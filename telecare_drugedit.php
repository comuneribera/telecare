<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_druginfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_drug_edit = NULL; // Initialize page object first

class ctelecare_drug_edit extends ctelecare_drug {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_drug';

	// Page object name
	var $PageObjName = 'telecare_drug_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_drug)
		if (!isset($GLOBALS["telecare_drug"]) || get_class($GLOBALS["telecare_drug"]) == "ctelecare_drug") {
			$GLOBALS["telecare_drug"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_drug"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_drug', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_druglist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->drug_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_drug;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_drug);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["drug_id"] <> "") {
			$this->drug_id->setQueryStringValue($_GET["drug_id"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->drug_id->CurrentValue == "")
			$this->Page_Terminate("telecare_druglist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_druglist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->drug_id->FldIsDetailKey)
			$this->drug_id->setFormValue($objForm->GetValue("x_drug_id"));
		if (!$this->drug_active_ingredient->FldIsDetailKey) {
			$this->drug_active_ingredient->setFormValue($objForm->GetValue("x_drug_active_ingredient"));
		}
		if (!$this->drug_package->FldIsDetailKey) {
			$this->drug_package->setFormValue($objForm->GetValue("x_drug_package"));
		}
		if (!$this->drug_ATC->FldIsDetailKey) {
			$this->drug_ATC->setFormValue($objForm->GetValue("x_drug_ATC"));
		}
		if (!$this->drug_AIC->FldIsDetailKey) {
			$this->drug_AIC->setFormValue($objForm->GetValue("x_drug_AIC"));
		}
		if (!$this->drug_name->FldIsDetailKey) {
			$this->drug_name->setFormValue($objForm->GetValue("x_drug_name"));
		}
		if (!$this->drug_company->FldIsDetailKey) {
			$this->drug_company->setFormValue($objForm->GetValue("x_drug_company"));
		}
		if (!$this->drug_price_b2b->FldIsDetailKey) {
			$this->drug_price_b2b->setFormValue($objForm->GetValue("x_drug_price_b2b"));
		}
		if (!$this->drug_price_b2c->FldIsDetailKey) {
			$this->drug_price_b2c->setFormValue($objForm->GetValue("x_drug_price_b2c"));
		}
		if (!$this->drug_note->FldIsDetailKey) {
			$this->drug_note->setFormValue($objForm->GetValue("x_drug_note"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->drug_id->CurrentValue = $this->drug_id->FormValue;
		$this->drug_active_ingredient->CurrentValue = $this->drug_active_ingredient->FormValue;
		$this->drug_package->CurrentValue = $this->drug_package->FormValue;
		$this->drug_ATC->CurrentValue = $this->drug_ATC->FormValue;
		$this->drug_AIC->CurrentValue = $this->drug_AIC->FormValue;
		$this->drug_name->CurrentValue = $this->drug_name->FormValue;
		$this->drug_company->CurrentValue = $this->drug_company->FormValue;
		$this->drug_price_b2b->CurrentValue = $this->drug_price_b2b->FormValue;
		$this->drug_price_b2c->CurrentValue = $this->drug_price_b2c->FormValue;
		$this->drug_note->CurrentValue = $this->drug_note->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->drug_id->setDbValue($rs->fields('drug_id'));
		$this->drug_active_ingredient->setDbValue($rs->fields('drug_active_ingredient'));
		$this->drug_package->setDbValue($rs->fields('drug_package'));
		$this->drug_ATC->setDbValue($rs->fields('drug_ATC'));
		$this->drug_AIC->setDbValue($rs->fields('drug_AIC'));
		$this->drug_name->setDbValue($rs->fields('drug_name'));
		$this->drug_company->setDbValue($rs->fields('drug_company'));
		$this->drug_price_b2b->setDbValue($rs->fields('drug_price_b2b'));
		$this->drug_price_b2c->setDbValue($rs->fields('drug_price_b2c'));
		$this->drug_note->setDbValue($rs->fields('drug_note'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->drug_id->DbValue = $row['drug_id'];
		$this->drug_active_ingredient->DbValue = $row['drug_active_ingredient'];
		$this->drug_package->DbValue = $row['drug_package'];
		$this->drug_ATC->DbValue = $row['drug_ATC'];
		$this->drug_AIC->DbValue = $row['drug_AIC'];
		$this->drug_name->DbValue = $row['drug_name'];
		$this->drug_company->DbValue = $row['drug_company'];
		$this->drug_price_b2b->DbValue = $row['drug_price_b2b'];
		$this->drug_price_b2c->DbValue = $row['drug_price_b2c'];
		$this->drug_note->DbValue = $row['drug_note'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// drug_id
		// drug_active_ingredient
		// drug_package
		// drug_ATC
		// drug_AIC
		// drug_name
		// drug_company
		// drug_price_b2b
		// drug_price_b2c
		// drug_note

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// drug_id
		$this->drug_id->ViewValue = $this->drug_id->CurrentValue;
		$this->drug_id->ViewCustomAttributes = "";

		// drug_active_ingredient
		$this->drug_active_ingredient->ViewValue = $this->drug_active_ingredient->CurrentValue;
		$this->drug_active_ingredient->ViewCustomAttributes = "";

		// drug_package
		$this->drug_package->ViewValue = $this->drug_package->CurrentValue;
		$this->drug_package->ViewCustomAttributes = "";

		// drug_ATC
		$this->drug_ATC->ViewValue = $this->drug_ATC->CurrentValue;
		$this->drug_ATC->ViewCustomAttributes = "";

		// drug_AIC
		$this->drug_AIC->ViewValue = $this->drug_AIC->CurrentValue;
		$this->drug_AIC->ViewCustomAttributes = "";

		// drug_name
		$this->drug_name->ViewValue = $this->drug_name->CurrentValue;
		$this->drug_name->ViewCustomAttributes = "";

		// drug_company
		$this->drug_company->ViewValue = $this->drug_company->CurrentValue;
		$this->drug_company->ViewCustomAttributes = "";

		// drug_price_b2b
		$this->drug_price_b2b->ViewValue = $this->drug_price_b2b->CurrentValue;
		$this->drug_price_b2b->ViewCustomAttributes = "";

		// drug_price_b2c
		$this->drug_price_b2c->ViewValue = $this->drug_price_b2c->CurrentValue;
		$this->drug_price_b2c->ViewCustomAttributes = "";

		// drug_note
		$this->drug_note->ViewValue = $this->drug_note->CurrentValue;
		$this->drug_note->ViewCustomAttributes = "";

			// drug_id
			$this->drug_id->LinkCustomAttributes = "";
			$this->drug_id->HrefValue = "";
			$this->drug_id->TooltipValue = "";

			// drug_active_ingredient
			$this->drug_active_ingredient->LinkCustomAttributes = "";
			$this->drug_active_ingredient->HrefValue = "";
			$this->drug_active_ingredient->TooltipValue = "";

			// drug_package
			$this->drug_package->LinkCustomAttributes = "";
			$this->drug_package->HrefValue = "";
			$this->drug_package->TooltipValue = "";

			// drug_ATC
			$this->drug_ATC->LinkCustomAttributes = "";
			$this->drug_ATC->HrefValue = "";
			$this->drug_ATC->TooltipValue = "";

			// drug_AIC
			$this->drug_AIC->LinkCustomAttributes = "";
			$this->drug_AIC->HrefValue = "";
			$this->drug_AIC->TooltipValue = "";

			// drug_name
			$this->drug_name->LinkCustomAttributes = "";
			$this->drug_name->HrefValue = "";
			$this->drug_name->TooltipValue = "";

			// drug_company
			$this->drug_company->LinkCustomAttributes = "";
			$this->drug_company->HrefValue = "";
			$this->drug_company->TooltipValue = "";

			// drug_price_b2b
			$this->drug_price_b2b->LinkCustomAttributes = "";
			$this->drug_price_b2b->HrefValue = "";
			$this->drug_price_b2b->TooltipValue = "";

			// drug_price_b2c
			$this->drug_price_b2c->LinkCustomAttributes = "";
			$this->drug_price_b2c->HrefValue = "";
			$this->drug_price_b2c->TooltipValue = "";

			// drug_note
			$this->drug_note->LinkCustomAttributes = "";
			$this->drug_note->HrefValue = "";
			$this->drug_note->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// drug_id
			$this->drug_id->EditAttrs["class"] = "form-control";
			$this->drug_id->EditCustomAttributes = "";
			$this->drug_id->EditValue = $this->drug_id->CurrentValue;
			$this->drug_id->ViewCustomAttributes = "";

			// drug_active_ingredient
			$this->drug_active_ingredient->EditAttrs["class"] = "form-control";
			$this->drug_active_ingredient->EditCustomAttributes = "";
			$this->drug_active_ingredient->EditValue = ew_HtmlEncode($this->drug_active_ingredient->CurrentValue);
			$this->drug_active_ingredient->PlaceHolder = ew_RemoveHtml($this->drug_active_ingredient->FldCaption());

			// drug_package
			$this->drug_package->EditAttrs["class"] = "form-control";
			$this->drug_package->EditCustomAttributes = "";
			$this->drug_package->EditValue = ew_HtmlEncode($this->drug_package->CurrentValue);
			$this->drug_package->PlaceHolder = ew_RemoveHtml($this->drug_package->FldCaption());

			// drug_ATC
			$this->drug_ATC->EditAttrs["class"] = "form-control";
			$this->drug_ATC->EditCustomAttributes = "";
			$this->drug_ATC->EditValue = ew_HtmlEncode($this->drug_ATC->CurrentValue);
			$this->drug_ATC->PlaceHolder = ew_RemoveHtml($this->drug_ATC->FldCaption());

			// drug_AIC
			$this->drug_AIC->EditAttrs["class"] = "form-control";
			$this->drug_AIC->EditCustomAttributes = "";
			$this->drug_AIC->EditValue = ew_HtmlEncode($this->drug_AIC->CurrentValue);
			$this->drug_AIC->PlaceHolder = ew_RemoveHtml($this->drug_AIC->FldCaption());

			// drug_name
			$this->drug_name->EditAttrs["class"] = "form-control";
			$this->drug_name->EditCustomAttributes = "";
			$this->drug_name->EditValue = ew_HtmlEncode($this->drug_name->CurrentValue);
			$this->drug_name->PlaceHolder = ew_RemoveHtml($this->drug_name->FldCaption());

			// drug_company
			$this->drug_company->EditAttrs["class"] = "form-control";
			$this->drug_company->EditCustomAttributes = "";
			$this->drug_company->EditValue = ew_HtmlEncode($this->drug_company->CurrentValue);
			$this->drug_company->PlaceHolder = ew_RemoveHtml($this->drug_company->FldCaption());

			// drug_price_b2b
			$this->drug_price_b2b->EditAttrs["class"] = "form-control";
			$this->drug_price_b2b->EditCustomAttributes = "";
			$this->drug_price_b2b->EditValue = ew_HtmlEncode($this->drug_price_b2b->CurrentValue);
			$this->drug_price_b2b->PlaceHolder = ew_RemoveHtml($this->drug_price_b2b->FldCaption());

			// drug_price_b2c
			$this->drug_price_b2c->EditAttrs["class"] = "form-control";
			$this->drug_price_b2c->EditCustomAttributes = "";
			$this->drug_price_b2c->EditValue = ew_HtmlEncode($this->drug_price_b2c->CurrentValue);
			$this->drug_price_b2c->PlaceHolder = ew_RemoveHtml($this->drug_price_b2c->FldCaption());

			// drug_note
			$this->drug_note->EditAttrs["class"] = "form-control";
			$this->drug_note->EditCustomAttributes = "";
			$this->drug_note->EditValue = ew_HtmlEncode($this->drug_note->CurrentValue);
			$this->drug_note->PlaceHolder = ew_RemoveHtml($this->drug_note->FldCaption());

			// Edit refer script
			// drug_id

			$this->drug_id->LinkCustomAttributes = "";
			$this->drug_id->HrefValue = "";

			// drug_active_ingredient
			$this->drug_active_ingredient->LinkCustomAttributes = "";
			$this->drug_active_ingredient->HrefValue = "";

			// drug_package
			$this->drug_package->LinkCustomAttributes = "";
			$this->drug_package->HrefValue = "";

			// drug_ATC
			$this->drug_ATC->LinkCustomAttributes = "";
			$this->drug_ATC->HrefValue = "";

			// drug_AIC
			$this->drug_AIC->LinkCustomAttributes = "";
			$this->drug_AIC->HrefValue = "";

			// drug_name
			$this->drug_name->LinkCustomAttributes = "";
			$this->drug_name->HrefValue = "";

			// drug_company
			$this->drug_company->LinkCustomAttributes = "";
			$this->drug_company->HrefValue = "";

			// drug_price_b2b
			$this->drug_price_b2b->LinkCustomAttributes = "";
			$this->drug_price_b2b->HrefValue = "";

			// drug_price_b2c
			$this->drug_price_b2c->LinkCustomAttributes = "";
			$this->drug_price_b2c->HrefValue = "";

			// drug_note
			$this->drug_note->LinkCustomAttributes = "";
			$this->drug_note->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// drug_active_ingredient
			$this->drug_active_ingredient->SetDbValueDef($rsnew, $this->drug_active_ingredient->CurrentValue, NULL, $this->drug_active_ingredient->ReadOnly);

			// drug_package
			$this->drug_package->SetDbValueDef($rsnew, $this->drug_package->CurrentValue, NULL, $this->drug_package->ReadOnly);

			// drug_ATC
			$this->drug_ATC->SetDbValueDef($rsnew, $this->drug_ATC->CurrentValue, NULL, $this->drug_ATC->ReadOnly);

			// drug_AIC
			$this->drug_AIC->SetDbValueDef($rsnew, $this->drug_AIC->CurrentValue, NULL, $this->drug_AIC->ReadOnly);

			// drug_name
			$this->drug_name->SetDbValueDef($rsnew, $this->drug_name->CurrentValue, NULL, $this->drug_name->ReadOnly);

			// drug_company
			$this->drug_company->SetDbValueDef($rsnew, $this->drug_company->CurrentValue, NULL, $this->drug_company->ReadOnly);

			// drug_price_b2b
			$this->drug_price_b2b->SetDbValueDef($rsnew, $this->drug_price_b2b->CurrentValue, NULL, $this->drug_price_b2b->ReadOnly);

			// drug_price_b2c
			$this->drug_price_b2c->SetDbValueDef($rsnew, $this->drug_price_b2c->CurrentValue, NULL, $this->drug_price_b2c->ReadOnly);

			// drug_note
			$this->drug_note->SetDbValueDef($rsnew, $this->drug_note->CurrentValue, NULL, $this->drug_note->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_druglist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_drug_edit)) $telecare_drug_edit = new ctelecare_drug_edit();

// Page init
$telecare_drug_edit->Page_Init();

// Page main
$telecare_drug_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_drug_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = ftelecare_drugedit = new ew_Form("ftelecare_drugedit", "edit");

// Validate form
ftelecare_drugedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_drugedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_drugedit.ValidateRequired = true;
<?php } else { ?>
ftelecare_drugedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_drug_edit->ShowPageHeader(); ?>
<?php
$telecare_drug_edit->ShowMessage();
?>
<form name="ftelecare_drugedit" id="ftelecare_drugedit" class="<?php echo $telecare_drug_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_drug_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_drug_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_drug">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<div>
<?php if ($telecare_drug->drug_id->Visible) { // drug_id ?>
	<div id="r_drug_id" class="form-group">
		<label id="elh_telecare_drug_drug_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_id->CellAttributes() ?>>
<span id="el_telecare_drug_drug_id">
<span<?php echo $telecare_drug->drug_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_drug->drug_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_drug" data-field="x_drug_id" name="x_drug_id" id="x_drug_id" value="<?php echo ew_HtmlEncode($telecare_drug->drug_id->CurrentValue) ?>">
<?php echo $telecare_drug->drug_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_active_ingredient->Visible) { // drug_active_ingredient ?>
	<div id="r_drug_active_ingredient" class="form-group">
		<label id="elh_telecare_drug_drug_active_ingredient" for="x_drug_active_ingredient" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_active_ingredient->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_active_ingredient->CellAttributes() ?>>
<span id="el_telecare_drug_drug_active_ingredient">
<input type="text" data-table="telecare_drug" data-field="x_drug_active_ingredient" name="x_drug_active_ingredient" id="x_drug_active_ingredient" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_active_ingredient->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_active_ingredient->EditValue ?>"<?php echo $telecare_drug->drug_active_ingredient->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_active_ingredient->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_package->Visible) { // drug_package ?>
	<div id="r_drug_package" class="form-group">
		<label id="elh_telecare_drug_drug_package" for="x_drug_package" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_package->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_package->CellAttributes() ?>>
<span id="el_telecare_drug_drug_package">
<input type="text" data-table="telecare_drug" data-field="x_drug_package" name="x_drug_package" id="x_drug_package" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_package->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_package->EditValue ?>"<?php echo $telecare_drug->drug_package->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_package->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_ATC->Visible) { // drug_ATC ?>
	<div id="r_drug_ATC" class="form-group">
		<label id="elh_telecare_drug_drug_ATC" for="x_drug_ATC" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_ATC->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_ATC->CellAttributes() ?>>
<span id="el_telecare_drug_drug_ATC">
<input type="text" data-table="telecare_drug" data-field="x_drug_ATC" name="x_drug_ATC" id="x_drug_ATC" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_ATC->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_ATC->EditValue ?>"<?php echo $telecare_drug->drug_ATC->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_ATC->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_AIC->Visible) { // drug_AIC ?>
	<div id="r_drug_AIC" class="form-group">
		<label id="elh_telecare_drug_drug_AIC" for="x_drug_AIC" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_AIC->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_AIC->CellAttributes() ?>>
<span id="el_telecare_drug_drug_AIC">
<input type="text" data-table="telecare_drug" data-field="x_drug_AIC" name="x_drug_AIC" id="x_drug_AIC" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_AIC->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_AIC->EditValue ?>"<?php echo $telecare_drug->drug_AIC->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_AIC->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_name->Visible) { // drug_name ?>
	<div id="r_drug_name" class="form-group">
		<label id="elh_telecare_drug_drug_name" for="x_drug_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_name->CellAttributes() ?>>
<span id="el_telecare_drug_drug_name">
<input type="text" data-table="telecare_drug" data-field="x_drug_name" name="x_drug_name" id="x_drug_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_name->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_name->EditValue ?>"<?php echo $telecare_drug->drug_name->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_company->Visible) { // drug_company ?>
	<div id="r_drug_company" class="form-group">
		<label id="elh_telecare_drug_drug_company" for="x_drug_company" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_company->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_company->CellAttributes() ?>>
<span id="el_telecare_drug_drug_company">
<input type="text" data-table="telecare_drug" data-field="x_drug_company" name="x_drug_company" id="x_drug_company" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_company->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_company->EditValue ?>"<?php echo $telecare_drug->drug_company->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_company->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_price_b2b->Visible) { // drug_price_b2b ?>
	<div id="r_drug_price_b2b" class="form-group">
		<label id="elh_telecare_drug_drug_price_b2b" for="x_drug_price_b2b" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_price_b2b->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_price_b2b->CellAttributes() ?>>
<span id="el_telecare_drug_drug_price_b2b">
<input type="text" data-table="telecare_drug" data-field="x_drug_price_b2b" name="x_drug_price_b2b" id="x_drug_price_b2b" size="30" maxlength="8" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_price_b2b->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_price_b2b->EditValue ?>"<?php echo $telecare_drug->drug_price_b2b->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_price_b2b->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_price_b2c->Visible) { // drug_price_b2c ?>
	<div id="r_drug_price_b2c" class="form-group">
		<label id="elh_telecare_drug_drug_price_b2c" for="x_drug_price_b2c" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_price_b2c->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_price_b2c->CellAttributes() ?>>
<span id="el_telecare_drug_drug_price_b2c">
<input type="text" data-table="telecare_drug" data-field="x_drug_price_b2c" name="x_drug_price_b2c" id="x_drug_price_b2c" size="30" maxlength="8" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_price_b2c->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_price_b2c->EditValue ?>"<?php echo $telecare_drug->drug_price_b2c->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_price_b2c->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_drug->drug_note->Visible) { // drug_note ?>
	<div id="r_drug_note" class="form-group">
		<label id="elh_telecare_drug_drug_note" for="x_drug_note" class="col-sm-2 control-label ewLabel"><?php echo $telecare_drug->drug_note->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_drug->drug_note->CellAttributes() ?>>
<span id="el_telecare_drug_drug_note">
<input type="text" data-table="telecare_drug" data-field="x_drug_note" name="x_drug_note" id="x_drug_note" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_drug->drug_note->getPlaceHolder()) ?>" value="<?php echo $telecare_drug->drug_note->EditValue ?>"<?php echo $telecare_drug->drug_note->EditAttributes() ?>>
</span>
<?php echo $telecare_drug->drug_note->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_drug_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_drugedit.Init();
</script>
<?php
$telecare_drug_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_drug_edit->Page_Terminate();
?>
