<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_admin_list = NULL; // Initialize page object first

class ctelecare_admin_list extends ctelecare_admin {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_admin';

	// Page object name
	var $PageObjName = 'telecare_admin_list';

	// Grid form hidden field names
	var $FormName = 'ftelecare_adminlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_admin)
		if (!isset($GLOBALS["telecare_admin"]) || get_class($GLOBALS["telecare_admin"]) == "ctelecare_admin") {
			$GLOBALS["telecare_admin"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_admin"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "telecare_adminadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "telecare_admindelete.php";
		$this->MultiUpdateUrl = "telecare_adminupdate.php";

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_admin', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption ftelecare_adminlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
				$this->Page_Terminate();
			}
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->admin_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_admin;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_admin);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Restore filter list
			$this->RestoreFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->admin_id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->admin_id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->admin_id->AdvancedSearch->ToJSON(), ","); // Field admin_id
		$sFilterList = ew_Concat($sFilterList, $this->admin_name->AdvancedSearch->ToJSON(), ","); // Field admin_name
		$sFilterList = ew_Concat($sFilterList, $this->admin_surname->AdvancedSearch->ToJSON(), ","); // Field admin_surname
		$sFilterList = ew_Concat($sFilterList, $this->admin_gender->AdvancedSearch->ToJSON(), ","); // Field admin_gender
		$sFilterList = ew_Concat($sFilterList, $this->admin_email->AdvancedSearch->ToJSON(), ","); // Field admin_email
		$sFilterList = ew_Concat($sFilterList, $this->admin_mobile->AdvancedSearch->ToJSON(), ","); // Field admin_mobile
		$sFilterList = ew_Concat($sFilterList, $this->admin_phone->AdvancedSearch->ToJSON(), ","); // Field admin_phone
		$sFilterList = ew_Concat($sFilterList, $this->admin_modules->AdvancedSearch->ToJSON(), ","); // Field admin_modules
		$sFilterList = ew_Concat($sFilterList, $this->admin_language->AdvancedSearch->ToJSON(), ","); // Field admin_language
		$sFilterList = ew_Concat($sFilterList, $this->admin_is_active->AdvancedSearch->ToJSON(), ","); // Field admin_is_active
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_name->AdvancedSearch->ToJSON(), ","); // Field admin_company_name
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_vat->AdvancedSearch->ToJSON(), ","); // Field admin_company_vat
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_logo_file->AdvancedSearch->ToJSON(), ","); // Field admin_company_logo_file
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_address->AdvancedSearch->ToJSON(), ","); // Field admin_company_address
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_region_id->AdvancedSearch->ToJSON(), ","); // Field admin_company_region_id
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_province_id->AdvancedSearch->ToJSON(), ","); // Field admin_company_province_id
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_city_id->AdvancedSearch->ToJSON(), ","); // Field admin_company_city_id
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_phone->AdvancedSearch->ToJSON(), ","); // Field admin_company_phone
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_email->AdvancedSearch->ToJSON(), ","); // Field admin_company_email
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_web->AdvancedSearch->ToJSON(), ","); // Field admin_company_web
		$sFilterList = ew_Concat($sFilterList, $this->admin_contract->AdvancedSearch->ToJSON(), ","); // Field admin_contract
		$sFilterList = ew_Concat($sFilterList, $this->admin_expire->AdvancedSearch->ToJSON(), ","); // Field admin_expire
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_facebook->AdvancedSearch->ToJSON(), ","); // Field admin_company_facebook
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_twitter->AdvancedSearch->ToJSON(), ","); // Field admin_company_twitter
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_googleplus->AdvancedSearch->ToJSON(), ","); // Field admin_company_googleplus
		$sFilterList = ew_Concat($sFilterList, $this->admin_company_linkedin->AdvancedSearch->ToJSON(), ","); // Field admin_company_linkedin
		$sFilterList = ew_Concat($sFilterList, $this->admin_link_playstore->AdvancedSearch->ToJSON(), ","); // Field admin_link_playstore
		$sFilterList = ew_Concat($sFilterList, $this->admin_link_appstore->AdvancedSearch->ToJSON(), ","); // Field admin_link_appstore
		$sFilterList = ew_Concat($sFilterList, $this->admin_link_windowsstore->AdvancedSearch->ToJSON(), ","); // Field admin_link_windowsstore
		$sFilterList = ew_Concat($sFilterList, $this->admin_level->AdvancedSearch->ToJSON(), ","); // Field admin_level
		$sFilterList = ew_Concat($sFilterList, $this->admin_username->AdvancedSearch->ToJSON(), ","); // Field admin_username
		$sFilterList = ew_Concat($sFilterList, $this->admin_password->AdvancedSearch->ToJSON(), ","); // Field admin_password
		$sFilterList = ew_Concat($sFilterList, $this->admin_last_update->AdvancedSearch->ToJSON(), ","); // Field admin_last_update
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field admin_id
		$this->admin_id->AdvancedSearch->SearchValue = @$filter["x_admin_id"];
		$this->admin_id->AdvancedSearch->SearchOperator = @$filter["z_admin_id"];
		$this->admin_id->AdvancedSearch->SearchCondition = @$filter["v_admin_id"];
		$this->admin_id->AdvancedSearch->SearchValue2 = @$filter["y_admin_id"];
		$this->admin_id->AdvancedSearch->SearchOperator2 = @$filter["w_admin_id"];
		$this->admin_id->AdvancedSearch->Save();

		// Field admin_name
		$this->admin_name->AdvancedSearch->SearchValue = @$filter["x_admin_name"];
		$this->admin_name->AdvancedSearch->SearchOperator = @$filter["z_admin_name"];
		$this->admin_name->AdvancedSearch->SearchCondition = @$filter["v_admin_name"];
		$this->admin_name->AdvancedSearch->SearchValue2 = @$filter["y_admin_name"];
		$this->admin_name->AdvancedSearch->SearchOperator2 = @$filter["w_admin_name"];
		$this->admin_name->AdvancedSearch->Save();

		// Field admin_surname
		$this->admin_surname->AdvancedSearch->SearchValue = @$filter["x_admin_surname"];
		$this->admin_surname->AdvancedSearch->SearchOperator = @$filter["z_admin_surname"];
		$this->admin_surname->AdvancedSearch->SearchCondition = @$filter["v_admin_surname"];
		$this->admin_surname->AdvancedSearch->SearchValue2 = @$filter["y_admin_surname"];
		$this->admin_surname->AdvancedSearch->SearchOperator2 = @$filter["w_admin_surname"];
		$this->admin_surname->AdvancedSearch->Save();

		// Field admin_gender
		$this->admin_gender->AdvancedSearch->SearchValue = @$filter["x_admin_gender"];
		$this->admin_gender->AdvancedSearch->SearchOperator = @$filter["z_admin_gender"];
		$this->admin_gender->AdvancedSearch->SearchCondition = @$filter["v_admin_gender"];
		$this->admin_gender->AdvancedSearch->SearchValue2 = @$filter["y_admin_gender"];
		$this->admin_gender->AdvancedSearch->SearchOperator2 = @$filter["w_admin_gender"];
		$this->admin_gender->AdvancedSearch->Save();

		// Field admin_email
		$this->admin_email->AdvancedSearch->SearchValue = @$filter["x_admin_email"];
		$this->admin_email->AdvancedSearch->SearchOperator = @$filter["z_admin_email"];
		$this->admin_email->AdvancedSearch->SearchCondition = @$filter["v_admin_email"];
		$this->admin_email->AdvancedSearch->SearchValue2 = @$filter["y_admin_email"];
		$this->admin_email->AdvancedSearch->SearchOperator2 = @$filter["w_admin_email"];
		$this->admin_email->AdvancedSearch->Save();

		// Field admin_mobile
		$this->admin_mobile->AdvancedSearch->SearchValue = @$filter["x_admin_mobile"];
		$this->admin_mobile->AdvancedSearch->SearchOperator = @$filter["z_admin_mobile"];
		$this->admin_mobile->AdvancedSearch->SearchCondition = @$filter["v_admin_mobile"];
		$this->admin_mobile->AdvancedSearch->SearchValue2 = @$filter["y_admin_mobile"];
		$this->admin_mobile->AdvancedSearch->SearchOperator2 = @$filter["w_admin_mobile"];
		$this->admin_mobile->AdvancedSearch->Save();

		// Field admin_phone
		$this->admin_phone->AdvancedSearch->SearchValue = @$filter["x_admin_phone"];
		$this->admin_phone->AdvancedSearch->SearchOperator = @$filter["z_admin_phone"];
		$this->admin_phone->AdvancedSearch->SearchCondition = @$filter["v_admin_phone"];
		$this->admin_phone->AdvancedSearch->SearchValue2 = @$filter["y_admin_phone"];
		$this->admin_phone->AdvancedSearch->SearchOperator2 = @$filter["w_admin_phone"];
		$this->admin_phone->AdvancedSearch->Save();

		// Field admin_modules
		$this->admin_modules->AdvancedSearch->SearchValue = @$filter["x_admin_modules"];
		$this->admin_modules->AdvancedSearch->SearchOperator = @$filter["z_admin_modules"];
		$this->admin_modules->AdvancedSearch->SearchCondition = @$filter["v_admin_modules"];
		$this->admin_modules->AdvancedSearch->SearchValue2 = @$filter["y_admin_modules"];
		$this->admin_modules->AdvancedSearch->SearchOperator2 = @$filter["w_admin_modules"];
		$this->admin_modules->AdvancedSearch->Save();

		// Field admin_language
		$this->admin_language->AdvancedSearch->SearchValue = @$filter["x_admin_language"];
		$this->admin_language->AdvancedSearch->SearchOperator = @$filter["z_admin_language"];
		$this->admin_language->AdvancedSearch->SearchCondition = @$filter["v_admin_language"];
		$this->admin_language->AdvancedSearch->SearchValue2 = @$filter["y_admin_language"];
		$this->admin_language->AdvancedSearch->SearchOperator2 = @$filter["w_admin_language"];
		$this->admin_language->AdvancedSearch->Save();

		// Field admin_is_active
		$this->admin_is_active->AdvancedSearch->SearchValue = @$filter["x_admin_is_active"];
		$this->admin_is_active->AdvancedSearch->SearchOperator = @$filter["z_admin_is_active"];
		$this->admin_is_active->AdvancedSearch->SearchCondition = @$filter["v_admin_is_active"];
		$this->admin_is_active->AdvancedSearch->SearchValue2 = @$filter["y_admin_is_active"];
		$this->admin_is_active->AdvancedSearch->SearchOperator2 = @$filter["w_admin_is_active"];
		$this->admin_is_active->AdvancedSearch->Save();

		// Field admin_company_name
		$this->admin_company_name->AdvancedSearch->SearchValue = @$filter["x_admin_company_name"];
		$this->admin_company_name->AdvancedSearch->SearchOperator = @$filter["z_admin_company_name"];
		$this->admin_company_name->AdvancedSearch->SearchCondition = @$filter["v_admin_company_name"];
		$this->admin_company_name->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_name"];
		$this->admin_company_name->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_name"];
		$this->admin_company_name->AdvancedSearch->Save();

		// Field admin_company_vat
		$this->admin_company_vat->AdvancedSearch->SearchValue = @$filter["x_admin_company_vat"];
		$this->admin_company_vat->AdvancedSearch->SearchOperator = @$filter["z_admin_company_vat"];
		$this->admin_company_vat->AdvancedSearch->SearchCondition = @$filter["v_admin_company_vat"];
		$this->admin_company_vat->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_vat"];
		$this->admin_company_vat->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_vat"];
		$this->admin_company_vat->AdvancedSearch->Save();

		// Field admin_company_logo_file
		$this->admin_company_logo_file->AdvancedSearch->SearchValue = @$filter["x_admin_company_logo_file"];
		$this->admin_company_logo_file->AdvancedSearch->SearchOperator = @$filter["z_admin_company_logo_file"];
		$this->admin_company_logo_file->AdvancedSearch->SearchCondition = @$filter["v_admin_company_logo_file"];
		$this->admin_company_logo_file->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_logo_file"];
		$this->admin_company_logo_file->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_logo_file"];
		$this->admin_company_logo_file->AdvancedSearch->Save();

		// Field admin_company_address
		$this->admin_company_address->AdvancedSearch->SearchValue = @$filter["x_admin_company_address"];
		$this->admin_company_address->AdvancedSearch->SearchOperator = @$filter["z_admin_company_address"];
		$this->admin_company_address->AdvancedSearch->SearchCondition = @$filter["v_admin_company_address"];
		$this->admin_company_address->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_address"];
		$this->admin_company_address->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_address"];
		$this->admin_company_address->AdvancedSearch->Save();

		// Field admin_company_region_id
		$this->admin_company_region_id->AdvancedSearch->SearchValue = @$filter["x_admin_company_region_id"];
		$this->admin_company_region_id->AdvancedSearch->SearchOperator = @$filter["z_admin_company_region_id"];
		$this->admin_company_region_id->AdvancedSearch->SearchCondition = @$filter["v_admin_company_region_id"];
		$this->admin_company_region_id->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_region_id"];
		$this->admin_company_region_id->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_region_id"];
		$this->admin_company_region_id->AdvancedSearch->Save();

		// Field admin_company_province_id
		$this->admin_company_province_id->AdvancedSearch->SearchValue = @$filter["x_admin_company_province_id"];
		$this->admin_company_province_id->AdvancedSearch->SearchOperator = @$filter["z_admin_company_province_id"];
		$this->admin_company_province_id->AdvancedSearch->SearchCondition = @$filter["v_admin_company_province_id"];
		$this->admin_company_province_id->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_province_id"];
		$this->admin_company_province_id->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_province_id"];
		$this->admin_company_province_id->AdvancedSearch->Save();

		// Field admin_company_city_id
		$this->admin_company_city_id->AdvancedSearch->SearchValue = @$filter["x_admin_company_city_id"];
		$this->admin_company_city_id->AdvancedSearch->SearchOperator = @$filter["z_admin_company_city_id"];
		$this->admin_company_city_id->AdvancedSearch->SearchCondition = @$filter["v_admin_company_city_id"];
		$this->admin_company_city_id->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_city_id"];
		$this->admin_company_city_id->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_city_id"];
		$this->admin_company_city_id->AdvancedSearch->Save();

		// Field admin_company_phone
		$this->admin_company_phone->AdvancedSearch->SearchValue = @$filter["x_admin_company_phone"];
		$this->admin_company_phone->AdvancedSearch->SearchOperator = @$filter["z_admin_company_phone"];
		$this->admin_company_phone->AdvancedSearch->SearchCondition = @$filter["v_admin_company_phone"];
		$this->admin_company_phone->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_phone"];
		$this->admin_company_phone->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_phone"];
		$this->admin_company_phone->AdvancedSearch->Save();

		// Field admin_company_email
		$this->admin_company_email->AdvancedSearch->SearchValue = @$filter["x_admin_company_email"];
		$this->admin_company_email->AdvancedSearch->SearchOperator = @$filter["z_admin_company_email"];
		$this->admin_company_email->AdvancedSearch->SearchCondition = @$filter["v_admin_company_email"];
		$this->admin_company_email->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_email"];
		$this->admin_company_email->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_email"];
		$this->admin_company_email->AdvancedSearch->Save();

		// Field admin_company_web
		$this->admin_company_web->AdvancedSearch->SearchValue = @$filter["x_admin_company_web"];
		$this->admin_company_web->AdvancedSearch->SearchOperator = @$filter["z_admin_company_web"];
		$this->admin_company_web->AdvancedSearch->SearchCondition = @$filter["v_admin_company_web"];
		$this->admin_company_web->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_web"];
		$this->admin_company_web->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_web"];
		$this->admin_company_web->AdvancedSearch->Save();

		// Field admin_contract
		$this->admin_contract->AdvancedSearch->SearchValue = @$filter["x_admin_contract"];
		$this->admin_contract->AdvancedSearch->SearchOperator = @$filter["z_admin_contract"];
		$this->admin_contract->AdvancedSearch->SearchCondition = @$filter["v_admin_contract"];
		$this->admin_contract->AdvancedSearch->SearchValue2 = @$filter["y_admin_contract"];
		$this->admin_contract->AdvancedSearch->SearchOperator2 = @$filter["w_admin_contract"];
		$this->admin_contract->AdvancedSearch->Save();

		// Field admin_expire
		$this->admin_expire->AdvancedSearch->SearchValue = @$filter["x_admin_expire"];
		$this->admin_expire->AdvancedSearch->SearchOperator = @$filter["z_admin_expire"];
		$this->admin_expire->AdvancedSearch->SearchCondition = @$filter["v_admin_expire"];
		$this->admin_expire->AdvancedSearch->SearchValue2 = @$filter["y_admin_expire"];
		$this->admin_expire->AdvancedSearch->SearchOperator2 = @$filter["w_admin_expire"];
		$this->admin_expire->AdvancedSearch->Save();

		// Field admin_company_facebook
		$this->admin_company_facebook->AdvancedSearch->SearchValue = @$filter["x_admin_company_facebook"];
		$this->admin_company_facebook->AdvancedSearch->SearchOperator = @$filter["z_admin_company_facebook"];
		$this->admin_company_facebook->AdvancedSearch->SearchCondition = @$filter["v_admin_company_facebook"];
		$this->admin_company_facebook->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_facebook"];
		$this->admin_company_facebook->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_facebook"];
		$this->admin_company_facebook->AdvancedSearch->Save();

		// Field admin_company_twitter
		$this->admin_company_twitter->AdvancedSearch->SearchValue = @$filter["x_admin_company_twitter"];
		$this->admin_company_twitter->AdvancedSearch->SearchOperator = @$filter["z_admin_company_twitter"];
		$this->admin_company_twitter->AdvancedSearch->SearchCondition = @$filter["v_admin_company_twitter"];
		$this->admin_company_twitter->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_twitter"];
		$this->admin_company_twitter->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_twitter"];
		$this->admin_company_twitter->AdvancedSearch->Save();

		// Field admin_company_googleplus
		$this->admin_company_googleplus->AdvancedSearch->SearchValue = @$filter["x_admin_company_googleplus"];
		$this->admin_company_googleplus->AdvancedSearch->SearchOperator = @$filter["z_admin_company_googleplus"];
		$this->admin_company_googleplus->AdvancedSearch->SearchCondition = @$filter["v_admin_company_googleplus"];
		$this->admin_company_googleplus->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_googleplus"];
		$this->admin_company_googleplus->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_googleplus"];
		$this->admin_company_googleplus->AdvancedSearch->Save();

		// Field admin_company_linkedin
		$this->admin_company_linkedin->AdvancedSearch->SearchValue = @$filter["x_admin_company_linkedin"];
		$this->admin_company_linkedin->AdvancedSearch->SearchOperator = @$filter["z_admin_company_linkedin"];
		$this->admin_company_linkedin->AdvancedSearch->SearchCondition = @$filter["v_admin_company_linkedin"];
		$this->admin_company_linkedin->AdvancedSearch->SearchValue2 = @$filter["y_admin_company_linkedin"];
		$this->admin_company_linkedin->AdvancedSearch->SearchOperator2 = @$filter["w_admin_company_linkedin"];
		$this->admin_company_linkedin->AdvancedSearch->Save();

		// Field admin_link_playstore
		$this->admin_link_playstore->AdvancedSearch->SearchValue = @$filter["x_admin_link_playstore"];
		$this->admin_link_playstore->AdvancedSearch->SearchOperator = @$filter["z_admin_link_playstore"];
		$this->admin_link_playstore->AdvancedSearch->SearchCondition = @$filter["v_admin_link_playstore"];
		$this->admin_link_playstore->AdvancedSearch->SearchValue2 = @$filter["y_admin_link_playstore"];
		$this->admin_link_playstore->AdvancedSearch->SearchOperator2 = @$filter["w_admin_link_playstore"];
		$this->admin_link_playstore->AdvancedSearch->Save();

		// Field admin_link_appstore
		$this->admin_link_appstore->AdvancedSearch->SearchValue = @$filter["x_admin_link_appstore"];
		$this->admin_link_appstore->AdvancedSearch->SearchOperator = @$filter["z_admin_link_appstore"];
		$this->admin_link_appstore->AdvancedSearch->SearchCondition = @$filter["v_admin_link_appstore"];
		$this->admin_link_appstore->AdvancedSearch->SearchValue2 = @$filter["y_admin_link_appstore"];
		$this->admin_link_appstore->AdvancedSearch->SearchOperator2 = @$filter["w_admin_link_appstore"];
		$this->admin_link_appstore->AdvancedSearch->Save();

		// Field admin_link_windowsstore
		$this->admin_link_windowsstore->AdvancedSearch->SearchValue = @$filter["x_admin_link_windowsstore"];
		$this->admin_link_windowsstore->AdvancedSearch->SearchOperator = @$filter["z_admin_link_windowsstore"];
		$this->admin_link_windowsstore->AdvancedSearch->SearchCondition = @$filter["v_admin_link_windowsstore"];
		$this->admin_link_windowsstore->AdvancedSearch->SearchValue2 = @$filter["y_admin_link_windowsstore"];
		$this->admin_link_windowsstore->AdvancedSearch->SearchOperator2 = @$filter["w_admin_link_windowsstore"];
		$this->admin_link_windowsstore->AdvancedSearch->Save();

		// Field admin_level
		$this->admin_level->AdvancedSearch->SearchValue = @$filter["x_admin_level"];
		$this->admin_level->AdvancedSearch->SearchOperator = @$filter["z_admin_level"];
		$this->admin_level->AdvancedSearch->SearchCondition = @$filter["v_admin_level"];
		$this->admin_level->AdvancedSearch->SearchValue2 = @$filter["y_admin_level"];
		$this->admin_level->AdvancedSearch->SearchOperator2 = @$filter["w_admin_level"];
		$this->admin_level->AdvancedSearch->Save();

		// Field admin_username
		$this->admin_username->AdvancedSearch->SearchValue = @$filter["x_admin_username"];
		$this->admin_username->AdvancedSearch->SearchOperator = @$filter["z_admin_username"];
		$this->admin_username->AdvancedSearch->SearchCondition = @$filter["v_admin_username"];
		$this->admin_username->AdvancedSearch->SearchValue2 = @$filter["y_admin_username"];
		$this->admin_username->AdvancedSearch->SearchOperator2 = @$filter["w_admin_username"];
		$this->admin_username->AdvancedSearch->Save();

		// Field admin_password
		$this->admin_password->AdvancedSearch->SearchValue = @$filter["x_admin_password"];
		$this->admin_password->AdvancedSearch->SearchOperator = @$filter["z_admin_password"];
		$this->admin_password->AdvancedSearch->SearchCondition = @$filter["v_admin_password"];
		$this->admin_password->AdvancedSearch->SearchValue2 = @$filter["y_admin_password"];
		$this->admin_password->AdvancedSearch->SearchOperator2 = @$filter["w_admin_password"];
		$this->admin_password->AdvancedSearch->Save();

		// Field admin_last_update
		$this->admin_last_update->AdvancedSearch->SearchValue = @$filter["x_admin_last_update"];
		$this->admin_last_update->AdvancedSearch->SearchOperator = @$filter["z_admin_last_update"];
		$this->admin_last_update->AdvancedSearch->SearchCondition = @$filter["v_admin_last_update"];
		$this->admin_last_update->AdvancedSearch->SearchValue2 = @$filter["y_admin_last_update"];
		$this->admin_last_update->AdvancedSearch->SearchOperator2 = @$filter["w_admin_last_update"];
		$this->admin_last_update->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->admin_id, $Default, FALSE); // admin_id
		$this->BuildSearchSql($sWhere, $this->admin_name, $Default, FALSE); // admin_name
		$this->BuildSearchSql($sWhere, $this->admin_surname, $Default, FALSE); // admin_surname
		$this->BuildSearchSql($sWhere, $this->admin_gender, $Default, FALSE); // admin_gender
		$this->BuildSearchSql($sWhere, $this->admin_email, $Default, FALSE); // admin_email
		$this->BuildSearchSql($sWhere, $this->admin_mobile, $Default, FALSE); // admin_mobile
		$this->BuildSearchSql($sWhere, $this->admin_phone, $Default, FALSE); // admin_phone
		$this->BuildSearchSql($sWhere, $this->admin_modules, $Default, TRUE); // admin_modules
		$this->BuildSearchSql($sWhere, $this->admin_language, $Default, FALSE); // admin_language
		$this->BuildSearchSql($sWhere, $this->admin_is_active, $Default, FALSE); // admin_is_active
		$this->BuildSearchSql($sWhere, $this->admin_company_name, $Default, FALSE); // admin_company_name
		$this->BuildSearchSql($sWhere, $this->admin_company_vat, $Default, FALSE); // admin_company_vat
		$this->BuildSearchSql($sWhere, $this->admin_company_logo_file, $Default, FALSE); // admin_company_logo_file
		$this->BuildSearchSql($sWhere, $this->admin_company_address, $Default, FALSE); // admin_company_address
		$this->BuildSearchSql($sWhere, $this->admin_company_region_id, $Default, FALSE); // admin_company_region_id
		$this->BuildSearchSql($sWhere, $this->admin_company_province_id, $Default, FALSE); // admin_company_province_id
		$this->BuildSearchSql($sWhere, $this->admin_company_city_id, $Default, FALSE); // admin_company_city_id
		$this->BuildSearchSql($sWhere, $this->admin_company_phone, $Default, FALSE); // admin_company_phone
		$this->BuildSearchSql($sWhere, $this->admin_company_email, $Default, FALSE); // admin_company_email
		$this->BuildSearchSql($sWhere, $this->admin_company_web, $Default, FALSE); // admin_company_web
		$this->BuildSearchSql($sWhere, $this->admin_contract, $Default, FALSE); // admin_contract
		$this->BuildSearchSql($sWhere, $this->admin_expire, $Default, FALSE); // admin_expire
		$this->BuildSearchSql($sWhere, $this->admin_company_facebook, $Default, FALSE); // admin_company_facebook
		$this->BuildSearchSql($sWhere, $this->admin_company_twitter, $Default, FALSE); // admin_company_twitter
		$this->BuildSearchSql($sWhere, $this->admin_company_googleplus, $Default, FALSE); // admin_company_googleplus
		$this->BuildSearchSql($sWhere, $this->admin_company_linkedin, $Default, FALSE); // admin_company_linkedin
		$this->BuildSearchSql($sWhere, $this->admin_link_playstore, $Default, FALSE); // admin_link_playstore
		$this->BuildSearchSql($sWhere, $this->admin_link_appstore, $Default, FALSE); // admin_link_appstore
		$this->BuildSearchSql($sWhere, $this->admin_link_windowsstore, $Default, FALSE); // admin_link_windowsstore
		$this->BuildSearchSql($sWhere, $this->admin_level, $Default, FALSE); // admin_level
		$this->BuildSearchSql($sWhere, $this->admin_username, $Default, FALSE); // admin_username
		$this->BuildSearchSql($sWhere, $this->admin_password, $Default, FALSE); // admin_password
		$this->BuildSearchSql($sWhere, $this->admin_last_update, $Default, FALSE); // admin_last_update

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->admin_id->AdvancedSearch->Save(); // admin_id
			$this->admin_name->AdvancedSearch->Save(); // admin_name
			$this->admin_surname->AdvancedSearch->Save(); // admin_surname
			$this->admin_gender->AdvancedSearch->Save(); // admin_gender
			$this->admin_email->AdvancedSearch->Save(); // admin_email
			$this->admin_mobile->AdvancedSearch->Save(); // admin_mobile
			$this->admin_phone->AdvancedSearch->Save(); // admin_phone
			$this->admin_modules->AdvancedSearch->Save(); // admin_modules
			$this->admin_language->AdvancedSearch->Save(); // admin_language
			$this->admin_is_active->AdvancedSearch->Save(); // admin_is_active
			$this->admin_company_name->AdvancedSearch->Save(); // admin_company_name
			$this->admin_company_vat->AdvancedSearch->Save(); // admin_company_vat
			$this->admin_company_logo_file->AdvancedSearch->Save(); // admin_company_logo_file
			$this->admin_company_address->AdvancedSearch->Save(); // admin_company_address
			$this->admin_company_region_id->AdvancedSearch->Save(); // admin_company_region_id
			$this->admin_company_province_id->AdvancedSearch->Save(); // admin_company_province_id
			$this->admin_company_city_id->AdvancedSearch->Save(); // admin_company_city_id
			$this->admin_company_phone->AdvancedSearch->Save(); // admin_company_phone
			$this->admin_company_email->AdvancedSearch->Save(); // admin_company_email
			$this->admin_company_web->AdvancedSearch->Save(); // admin_company_web
			$this->admin_contract->AdvancedSearch->Save(); // admin_contract
			$this->admin_expire->AdvancedSearch->Save(); // admin_expire
			$this->admin_company_facebook->AdvancedSearch->Save(); // admin_company_facebook
			$this->admin_company_twitter->AdvancedSearch->Save(); // admin_company_twitter
			$this->admin_company_googleplus->AdvancedSearch->Save(); // admin_company_googleplus
			$this->admin_company_linkedin->AdvancedSearch->Save(); // admin_company_linkedin
			$this->admin_link_playstore->AdvancedSearch->Save(); // admin_link_playstore
			$this->admin_link_appstore->AdvancedSearch->Save(); // admin_link_appstore
			$this->admin_link_windowsstore->AdvancedSearch->Save(); // admin_link_windowsstore
			$this->admin_level->AdvancedSearch->Save(); // admin_level
			$this->admin_username->AdvancedSearch->Save(); // admin_username
			$this->admin_password->AdvancedSearch->Save(); // admin_password
			$this->admin_last_update->AdvancedSearch->Save(); // admin_last_update
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1 || $FldOpr <> "LIKE" ||
			($FldOpr2 <> "LIKE" && $FldVal2 <> ""))
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->admin_name, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_surname, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_email, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_mobile, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_phone, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_modules, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_name, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_logo_file, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_address, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_city_id, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_phone, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_email, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_web, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_facebook, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_twitter, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_googleplus, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_company_linkedin, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_link_playstore, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_link_appstore, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_link_windowsstore, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_username, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->admin_password, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->admin_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_name->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_surname->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_gender->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_email->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_mobile->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_phone->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_modules->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_language->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_is_active->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_name->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_vat->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_logo_file->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_address->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_region_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_province_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_city_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_phone->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_email->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_web->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_contract->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_expire->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_facebook->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_twitter->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_googleplus->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_company_linkedin->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_link_playstore->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_link_appstore->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_link_windowsstore->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_level->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_username->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_password->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->admin_last_update->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->admin_id->AdvancedSearch->UnsetSession();
		$this->admin_name->AdvancedSearch->UnsetSession();
		$this->admin_surname->AdvancedSearch->UnsetSession();
		$this->admin_gender->AdvancedSearch->UnsetSession();
		$this->admin_email->AdvancedSearch->UnsetSession();
		$this->admin_mobile->AdvancedSearch->UnsetSession();
		$this->admin_phone->AdvancedSearch->UnsetSession();
		$this->admin_modules->AdvancedSearch->UnsetSession();
		$this->admin_language->AdvancedSearch->UnsetSession();
		$this->admin_is_active->AdvancedSearch->UnsetSession();
		$this->admin_company_name->AdvancedSearch->UnsetSession();
		$this->admin_company_vat->AdvancedSearch->UnsetSession();
		$this->admin_company_logo_file->AdvancedSearch->UnsetSession();
		$this->admin_company_address->AdvancedSearch->UnsetSession();
		$this->admin_company_region_id->AdvancedSearch->UnsetSession();
		$this->admin_company_province_id->AdvancedSearch->UnsetSession();
		$this->admin_company_city_id->AdvancedSearch->UnsetSession();
		$this->admin_company_phone->AdvancedSearch->UnsetSession();
		$this->admin_company_email->AdvancedSearch->UnsetSession();
		$this->admin_company_web->AdvancedSearch->UnsetSession();
		$this->admin_contract->AdvancedSearch->UnsetSession();
		$this->admin_expire->AdvancedSearch->UnsetSession();
		$this->admin_company_facebook->AdvancedSearch->UnsetSession();
		$this->admin_company_twitter->AdvancedSearch->UnsetSession();
		$this->admin_company_googleplus->AdvancedSearch->UnsetSession();
		$this->admin_company_linkedin->AdvancedSearch->UnsetSession();
		$this->admin_link_playstore->AdvancedSearch->UnsetSession();
		$this->admin_link_appstore->AdvancedSearch->UnsetSession();
		$this->admin_link_windowsstore->AdvancedSearch->UnsetSession();
		$this->admin_level->AdvancedSearch->UnsetSession();
		$this->admin_username->AdvancedSearch->UnsetSession();
		$this->admin_password->AdvancedSearch->UnsetSession();
		$this->admin_last_update->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->admin_id->AdvancedSearch->Load();
		$this->admin_name->AdvancedSearch->Load();
		$this->admin_surname->AdvancedSearch->Load();
		$this->admin_gender->AdvancedSearch->Load();
		$this->admin_email->AdvancedSearch->Load();
		$this->admin_mobile->AdvancedSearch->Load();
		$this->admin_phone->AdvancedSearch->Load();
		$this->admin_modules->AdvancedSearch->Load();
		$this->admin_language->AdvancedSearch->Load();
		$this->admin_is_active->AdvancedSearch->Load();
		$this->admin_company_name->AdvancedSearch->Load();
		$this->admin_company_vat->AdvancedSearch->Load();
		$this->admin_company_logo_file->AdvancedSearch->Load();
		$this->admin_company_address->AdvancedSearch->Load();
		$this->admin_company_region_id->AdvancedSearch->Load();
		$this->admin_company_province_id->AdvancedSearch->Load();
		$this->admin_company_city_id->AdvancedSearch->Load();
		$this->admin_company_phone->AdvancedSearch->Load();
		$this->admin_company_email->AdvancedSearch->Load();
		$this->admin_company_web->AdvancedSearch->Load();
		$this->admin_contract->AdvancedSearch->Load();
		$this->admin_expire->AdvancedSearch->Load();
		$this->admin_company_facebook->AdvancedSearch->Load();
		$this->admin_company_twitter->AdvancedSearch->Load();
		$this->admin_company_googleplus->AdvancedSearch->Load();
		$this->admin_company_linkedin->AdvancedSearch->Load();
		$this->admin_link_playstore->AdvancedSearch->Load();
		$this->admin_link_appstore->AdvancedSearch->Load();
		$this->admin_link_windowsstore->AdvancedSearch->Load();
		$this->admin_level->AdvancedSearch->Load();
		$this->admin_username->AdvancedSearch->Load();
		$this->admin_password->AdvancedSearch->Load();
		$this->admin_last_update->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->admin_id); // admin_id
			$this->UpdateSort($this->admin_name); // admin_name
			$this->UpdateSort($this->admin_surname); // admin_surname
			$this->UpdateSort($this->admin_company_name); // admin_company_name
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->admin_id->setSort("");
				$this->admin_name->setSort("");
				$this->admin_surname->setSort("");
				$this->admin_company_name->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = TRUE;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = TRUE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView() && $this->ShowOptionLink('view'))
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit() && $this->ShowOptionLink('edit')) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd() && $this->ShowOptionLink('add')) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete() && $this->ShowOptionLink('delete'))
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->admin_id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"ftelecare_adminlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"ftelecare_adminlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.ftelecare_adminlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$user = $row['admin_username'];
					if ($userlist <> "") $userlist .= ",";
					$userlist .= $user;
					if ($UserAction == "resendregisteremail")
						$Processed = FALSE;
					elseif ($UserAction == "resetconcurrentuser")
						$Processed = FALSE;
					elseif ($UserAction == "resetloginretry")
						$Processed = FALSE;
					elseif ($UserAction == "setpasswordexpired")
						$Processed = FALSE;
					else
						$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"ftelecare_adminlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Advanced search button
		$item = &$this->SearchOptions->Add("advancedsearch");
		if (ew_IsMobile())
			$item->Body = "<a class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" href=\"telecare_adminsrch.php\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		else
			$item->Body = "<button type=\"button\" class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" onclick=\"ew_SearchDialogShow({lnk:this,url:'telecare_adminsrch.php'});\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		$item->Visible = TRUE;

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;

		// Hide detail items for dropdown if necessary
		$this->ListOptions->HideDetailItemsForDropDown();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// admin_id

		$this->admin_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_id"]);
		if ($this->admin_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_id->AdvancedSearch->SearchOperator = @$_GET["z_admin_id"];

		// admin_name
		$this->admin_name->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_name"]);
		if ($this->admin_name->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_name->AdvancedSearch->SearchOperator = @$_GET["z_admin_name"];

		// admin_surname
		$this->admin_surname->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_surname"]);
		if ($this->admin_surname->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_surname->AdvancedSearch->SearchOperator = @$_GET["z_admin_surname"];

		// admin_gender
		$this->admin_gender->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_gender"]);
		if ($this->admin_gender->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_gender->AdvancedSearch->SearchOperator = @$_GET["z_admin_gender"];

		// admin_email
		$this->admin_email->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_email"]);
		if ($this->admin_email->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_email->AdvancedSearch->SearchOperator = @$_GET["z_admin_email"];

		// admin_mobile
		$this->admin_mobile->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_mobile"]);
		if ($this->admin_mobile->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_mobile->AdvancedSearch->SearchOperator = @$_GET["z_admin_mobile"];

		// admin_phone
		$this->admin_phone->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_phone"]);
		if ($this->admin_phone->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_phone->AdvancedSearch->SearchOperator = @$_GET["z_admin_phone"];

		// admin_modules
		$this->admin_modules->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_modules"]);
		if ($this->admin_modules->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_modules->AdvancedSearch->SearchOperator = @$_GET["z_admin_modules"];
		if (is_array($this->admin_modules->AdvancedSearch->SearchValue)) $this->admin_modules->AdvancedSearch->SearchValue = implode(",", $this->admin_modules->AdvancedSearch->SearchValue);
		if (is_array($this->admin_modules->AdvancedSearch->SearchValue2)) $this->admin_modules->AdvancedSearch->SearchValue2 = implode(",", $this->admin_modules->AdvancedSearch->SearchValue2);

		// admin_language
		$this->admin_language->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_language"]);
		if ($this->admin_language->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_language->AdvancedSearch->SearchOperator = @$_GET["z_admin_language"];

		// admin_is_active
		$this->admin_is_active->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_is_active"]);
		if ($this->admin_is_active->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_is_active->AdvancedSearch->SearchOperator = @$_GET["z_admin_is_active"];

		// admin_company_name
		$this->admin_company_name->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_name"]);
		if ($this->admin_company_name->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_name->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_name"];

		// admin_company_vat
		$this->admin_company_vat->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_vat"]);
		if ($this->admin_company_vat->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_vat->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_vat"];

		// admin_company_logo_file
		$this->admin_company_logo_file->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_logo_file"]);
		if ($this->admin_company_logo_file->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_logo_file->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_logo_file"];

		// admin_company_address
		$this->admin_company_address->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_address"]);
		if ($this->admin_company_address->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_address->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_address"];

		// admin_company_region_id
		$this->admin_company_region_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_region_id"]);
		if ($this->admin_company_region_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_region_id->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_region_id"];

		// admin_company_province_id
		$this->admin_company_province_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_province_id"]);
		if ($this->admin_company_province_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_province_id->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_province_id"];

		// admin_company_city_id
		$this->admin_company_city_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_city_id"]);
		if ($this->admin_company_city_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_city_id->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_city_id"];

		// admin_company_phone
		$this->admin_company_phone->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_phone"]);
		if ($this->admin_company_phone->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_phone->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_phone"];

		// admin_company_email
		$this->admin_company_email->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_email"]);
		if ($this->admin_company_email->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_email->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_email"];

		// admin_company_web
		$this->admin_company_web->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_web"]);
		if ($this->admin_company_web->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_web->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_web"];

		// admin_contract
		$this->admin_contract->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_contract"]);
		if ($this->admin_contract->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_contract->AdvancedSearch->SearchOperator = @$_GET["z_admin_contract"];

		// admin_expire
		$this->admin_expire->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_expire"]);
		if ($this->admin_expire->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_expire->AdvancedSearch->SearchOperator = @$_GET["z_admin_expire"];

		// admin_company_facebook
		$this->admin_company_facebook->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_facebook"]);
		if ($this->admin_company_facebook->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_facebook->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_facebook"];

		// admin_company_twitter
		$this->admin_company_twitter->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_twitter"]);
		if ($this->admin_company_twitter->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_twitter->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_twitter"];

		// admin_company_googleplus
		$this->admin_company_googleplus->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_googleplus"]);
		if ($this->admin_company_googleplus->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_googleplus->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_googleplus"];

		// admin_company_linkedin
		$this->admin_company_linkedin->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_company_linkedin"]);
		if ($this->admin_company_linkedin->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_company_linkedin->AdvancedSearch->SearchOperator = @$_GET["z_admin_company_linkedin"];

		// admin_link_playstore
		$this->admin_link_playstore->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_link_playstore"]);
		if ($this->admin_link_playstore->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_link_playstore->AdvancedSearch->SearchOperator = @$_GET["z_admin_link_playstore"];

		// admin_link_appstore
		$this->admin_link_appstore->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_link_appstore"]);
		if ($this->admin_link_appstore->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_link_appstore->AdvancedSearch->SearchOperator = @$_GET["z_admin_link_appstore"];

		// admin_link_windowsstore
		$this->admin_link_windowsstore->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_link_windowsstore"]);
		if ($this->admin_link_windowsstore->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_link_windowsstore->AdvancedSearch->SearchOperator = @$_GET["z_admin_link_windowsstore"];

		// admin_level
		$this->admin_level->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_level"]);
		if ($this->admin_level->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_level->AdvancedSearch->SearchOperator = @$_GET["z_admin_level"];

		// admin_username
		$this->admin_username->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_username"]);
		if ($this->admin_username->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_username->AdvancedSearch->SearchOperator = @$_GET["z_admin_username"];

		// admin_password
		$this->admin_password->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_password"]);
		if ($this->admin_password->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_password->AdvancedSearch->SearchOperator = @$_GET["z_admin_password"];

		// admin_last_update
		$this->admin_last_update->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_admin_last_update"]);
		if ($this->admin_last_update->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->admin_last_update->AdvancedSearch->SearchOperator = @$_GET["z_admin_last_update"];
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->admin_id->setDbValue($rs->fields('admin_id'));
		$this->admin_parent_id->setDbValue($rs->fields('admin_parent_id'));
		$this->admin_name->setDbValue($rs->fields('admin_name'));
		$this->admin_surname->setDbValue($rs->fields('admin_surname'));
		$this->admin_gender->setDbValue($rs->fields('admin_gender'));
		$this->admin_email->setDbValue($rs->fields('admin_email'));
		$this->admin_mobile->setDbValue($rs->fields('admin_mobile'));
		$this->admin_phone->setDbValue($rs->fields('admin_phone'));
		$this->admin_modules->setDbValue($rs->fields('admin_modules'));
		$this->admin_language->setDbValue($rs->fields('admin_language'));
		$this->admin_is_active->setDbValue($rs->fields('admin_is_active'));
		$this->admin_company_name->setDbValue($rs->fields('admin_company_name'));
		$this->admin_company_vat->setDbValue($rs->fields('admin_company_vat'));
		$this->admin_company_logo_file->Upload->DbValue = $rs->fields('admin_company_logo_file');
		$this->admin_company_logo_file->CurrentValue = $this->admin_company_logo_file->Upload->DbValue;
		$this->admin_company_logo_width->setDbValue($rs->fields('admin_company_logo_width'));
		$this->admin_company_logo_height->setDbValue($rs->fields('admin_company_logo_height'));
		$this->admin_company_logo_size->setDbValue($rs->fields('admin_company_logo_size'));
		$this->admin_company_address->setDbValue($rs->fields('admin_company_address'));
		$this->admin_company_region_id->setDbValue($rs->fields('admin_company_region_id'));
		$this->admin_company_province_id->setDbValue($rs->fields('admin_company_province_id'));
		$this->admin_company_city_id->setDbValue($rs->fields('admin_company_city_id'));
		$this->admin_company_phone->setDbValue($rs->fields('admin_company_phone'));
		$this->admin_company_email->setDbValue($rs->fields('admin_company_email'));
		$this->admin_company_web->setDbValue($rs->fields('admin_company_web'));
		$this->admin_contract->setDbValue($rs->fields('admin_contract'));
		$this->admin_expire->setDbValue($rs->fields('admin_expire'));
		$this->admin_company_facebook->setDbValue($rs->fields('admin_company_facebook'));
		$this->admin_company_twitter->setDbValue($rs->fields('admin_company_twitter'));
		$this->admin_company_googleplus->setDbValue($rs->fields('admin_company_googleplus'));
		$this->admin_company_linkedin->setDbValue($rs->fields('admin_company_linkedin'));
		$this->admin_link_playstore->setDbValue($rs->fields('admin_link_playstore'));
		$this->admin_link_appstore->setDbValue($rs->fields('admin_link_appstore'));
		$this->admin_link_windowsstore->setDbValue($rs->fields('admin_link_windowsstore'));
		$this->admin_level->setDbValue($rs->fields('admin_level'));
		$this->admin_username->setDbValue($rs->fields('admin_username'));
		$this->admin_password->setDbValue($rs->fields('admin_password'));
		$this->admin_company_autohority->setDbValue($rs->fields('admin_company_autohority'));
		$this->admin_auth_token->setDbValue($rs->fields('admin_auth_token'));
		$this->admin_last_update->setDbValue($rs->fields('admin_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->admin_id->DbValue = $row['admin_id'];
		$this->admin_parent_id->DbValue = $row['admin_parent_id'];
		$this->admin_name->DbValue = $row['admin_name'];
		$this->admin_surname->DbValue = $row['admin_surname'];
		$this->admin_gender->DbValue = $row['admin_gender'];
		$this->admin_email->DbValue = $row['admin_email'];
		$this->admin_mobile->DbValue = $row['admin_mobile'];
		$this->admin_phone->DbValue = $row['admin_phone'];
		$this->admin_modules->DbValue = $row['admin_modules'];
		$this->admin_language->DbValue = $row['admin_language'];
		$this->admin_is_active->DbValue = $row['admin_is_active'];
		$this->admin_company_name->DbValue = $row['admin_company_name'];
		$this->admin_company_vat->DbValue = $row['admin_company_vat'];
		$this->admin_company_logo_file->Upload->DbValue = $row['admin_company_logo_file'];
		$this->admin_company_logo_width->DbValue = $row['admin_company_logo_width'];
		$this->admin_company_logo_height->DbValue = $row['admin_company_logo_height'];
		$this->admin_company_logo_size->DbValue = $row['admin_company_logo_size'];
		$this->admin_company_address->DbValue = $row['admin_company_address'];
		$this->admin_company_region_id->DbValue = $row['admin_company_region_id'];
		$this->admin_company_province_id->DbValue = $row['admin_company_province_id'];
		$this->admin_company_city_id->DbValue = $row['admin_company_city_id'];
		$this->admin_company_phone->DbValue = $row['admin_company_phone'];
		$this->admin_company_email->DbValue = $row['admin_company_email'];
		$this->admin_company_web->DbValue = $row['admin_company_web'];
		$this->admin_contract->DbValue = $row['admin_contract'];
		$this->admin_expire->DbValue = $row['admin_expire'];
		$this->admin_company_facebook->DbValue = $row['admin_company_facebook'];
		$this->admin_company_twitter->DbValue = $row['admin_company_twitter'];
		$this->admin_company_googleplus->DbValue = $row['admin_company_googleplus'];
		$this->admin_company_linkedin->DbValue = $row['admin_company_linkedin'];
		$this->admin_link_playstore->DbValue = $row['admin_link_playstore'];
		$this->admin_link_appstore->DbValue = $row['admin_link_appstore'];
		$this->admin_link_windowsstore->DbValue = $row['admin_link_windowsstore'];
		$this->admin_level->DbValue = $row['admin_level'];
		$this->admin_username->DbValue = $row['admin_username'];
		$this->admin_password->DbValue = $row['admin_password'];
		$this->admin_company_autohority->DbValue = $row['admin_company_autohority'];
		$this->admin_auth_token->DbValue = $row['admin_auth_token'];
		$this->admin_last_update->DbValue = $row['admin_last_update'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("admin_id")) <> "")
			$this->admin_id->CurrentValue = $this->getKey("admin_id"); // admin_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// admin_id
		// admin_parent_id

		$this->admin_parent_id->CellCssStyle = "white-space: nowrap;";

		// admin_name
		// admin_surname
		// admin_gender
		// admin_email
		// admin_mobile
		// admin_phone
		// admin_modules
		// admin_language
		// admin_is_active
		// admin_company_name
		// admin_company_vat
		// admin_company_logo_file
		// admin_company_logo_width
		// admin_company_logo_height
		// admin_company_logo_size
		// admin_company_address
		// admin_company_region_id
		// admin_company_province_id
		// admin_company_city_id
		// admin_company_phone
		// admin_company_email
		// admin_company_web
		// admin_contract

		$this->admin_contract->CellCssStyle = "white-space: nowrap;";

		// admin_expire
		$this->admin_expire->CellCssStyle = "white-space: nowrap;";

		// admin_company_facebook
		// admin_company_twitter
		// admin_company_googleplus
		// admin_company_linkedin
		// admin_link_playstore
		// admin_link_appstore
		// admin_link_windowsstore
		// admin_level
		// admin_username
		// admin_password
		// admin_company_autohority

		$this->admin_company_autohority->CellCssStyle = "white-space: nowrap;";

		// admin_auth_token
		$this->admin_auth_token->CellCssStyle = "white-space: nowrap;";

		// admin_last_update
		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// admin_id
		$this->admin_id->ViewValue = $this->admin_id->CurrentValue;
		$this->admin_id->ViewCustomAttributes = "";

		// admin_name
		$this->admin_name->ViewValue = $this->admin_name->CurrentValue;
		$this->admin_name->ViewCustomAttributes = "";

		// admin_surname
		$this->admin_surname->ViewValue = $this->admin_surname->CurrentValue;
		$this->admin_surname->ViewCustomAttributes = "";

		// admin_gender
		if (strval($this->admin_gender->CurrentValue) <> "") {
			$this->admin_gender->ViewValue = $this->admin_gender->OptionCaption($this->admin_gender->CurrentValue);
		} else {
			$this->admin_gender->ViewValue = NULL;
		}
		$this->admin_gender->ViewCustomAttributes = "";

		// admin_email
		$this->admin_email->ViewValue = $this->admin_email->CurrentValue;
		$this->admin_email->ViewCustomAttributes = "";

		// admin_mobile
		$this->admin_mobile->ViewValue = $this->admin_mobile->CurrentValue;
		$this->admin_mobile->ViewCustomAttributes = "";

		// admin_phone
		$this->admin_phone->ViewValue = $this->admin_phone->CurrentValue;
		$this->admin_phone->ViewCustomAttributes = "";

		// admin_modules
		if (strval($this->admin_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->admin_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->admin_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->admin_modules->ViewValue .= $this->admin_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->admin_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->admin_modules->ViewValue = $this->admin_modules->CurrentValue;
			}
		} else {
			$this->admin_modules->ViewValue = NULL;
		}
		$this->admin_modules->ViewCustomAttributes = "";

		// admin_language
		if (strval($this->admin_language->CurrentValue) <> "") {
			$this->admin_language->ViewValue = $this->admin_language->OptionCaption($this->admin_language->CurrentValue);
		} else {
			$this->admin_language->ViewValue = NULL;
		}
		$this->admin_language->ViewCustomAttributes = "";

		// admin_is_active
		if (strval($this->admin_is_active->CurrentValue) <> "") {
			$this->admin_is_active->ViewValue = $this->admin_is_active->OptionCaption($this->admin_is_active->CurrentValue);
		} else {
			$this->admin_is_active->ViewValue = NULL;
		}
		$this->admin_is_active->ViewCustomAttributes = "";

		// admin_company_name
		$this->admin_company_name->ViewValue = $this->admin_company_name->CurrentValue;
		$this->admin_company_name->ViewCustomAttributes = "";

		// admin_company_vat
		$this->admin_company_vat->ViewValue = $this->admin_company_vat->CurrentValue;
		$this->admin_company_vat->ViewCustomAttributes = "";

		// admin_company_logo_file
		if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
			$this->admin_company_logo_file->ImageWidth = 100;
			$this->admin_company_logo_file->ImageHeight = 0;
			$this->admin_company_logo_file->ImageAlt = $this->admin_company_logo_file->FldAlt();
			$this->admin_company_logo_file->ViewValue = $this->admin_company_logo_file->Upload->DbValue;
		} else {
			$this->admin_company_logo_file->ViewValue = "";
		}
		$this->admin_company_logo_file->ViewCustomAttributes = "";

		// admin_company_address
		$this->admin_company_address->ViewValue = $this->admin_company_address->CurrentValue;
		$this->admin_company_address->ViewCustomAttributes = "";

		// admin_company_region_id
		if (strval($this->admin_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->admin_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->CurrentValue;
			}
		} else {
			$this->admin_company_region_id->ViewValue = NULL;
		}
		$this->admin_company_region_id->ViewCustomAttributes = "";

		// admin_company_province_id
		if (strval($this->admin_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->admin_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->CurrentValue;
			}
		} else {
			$this->admin_company_province_id->ViewValue = NULL;
		}
		$this->admin_company_province_id->ViewCustomAttributes = "";

		// admin_company_city_id
		if (strval($this->admin_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->admin_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->CurrentValue;
			}
		} else {
			$this->admin_company_city_id->ViewValue = NULL;
		}
		$this->admin_company_city_id->ViewCustomAttributes = "";

		// admin_company_phone
		$this->admin_company_phone->ViewValue = $this->admin_company_phone->CurrentValue;
		$this->admin_company_phone->ViewCustomAttributes = "";

		// admin_company_email
		$this->admin_company_email->ViewValue = $this->admin_company_email->CurrentValue;
		$this->admin_company_email->ViewCustomAttributes = "";

		// admin_company_web
		$this->admin_company_web->ViewValue = $this->admin_company_web->CurrentValue;
		$this->admin_company_web->ViewCustomAttributes = "";

		// admin_company_facebook
		$this->admin_company_facebook->ViewValue = $this->admin_company_facebook->CurrentValue;
		$this->admin_company_facebook->ViewCustomAttributes = "";

		// admin_company_twitter
		$this->admin_company_twitter->ViewValue = $this->admin_company_twitter->CurrentValue;
		$this->admin_company_twitter->ViewCustomAttributes = "";

		// admin_company_googleplus
		$this->admin_company_googleplus->ViewValue = $this->admin_company_googleplus->CurrentValue;
		$this->admin_company_googleplus->ViewCustomAttributes = "";

		// admin_company_linkedin
		$this->admin_company_linkedin->ViewValue = $this->admin_company_linkedin->CurrentValue;
		$this->admin_company_linkedin->ViewCustomAttributes = "";

		// admin_link_playstore
		$this->admin_link_playstore->ViewValue = $this->admin_link_playstore->CurrentValue;
		$this->admin_link_playstore->ViewCustomAttributes = "";

		// admin_link_appstore
		$this->admin_link_appstore->ViewValue = $this->admin_link_appstore->CurrentValue;
		$this->admin_link_appstore->ViewCustomAttributes = "";

		// admin_link_windowsstore
		$this->admin_link_windowsstore->ViewValue = $this->admin_link_windowsstore->CurrentValue;
		$this->admin_link_windowsstore->ViewCustomAttributes = "";

		// admin_level
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->admin_level->CurrentValue) <> "") {
			$this->admin_level->ViewValue = $this->admin_level->OptionCaption($this->admin_level->CurrentValue);
		} else {
			$this->admin_level->ViewValue = NULL;
		}
		} else {
			$this->admin_level->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->admin_level->ViewCustomAttributes = "";

		// admin_username
		$this->admin_username->ViewValue = $this->admin_username->CurrentValue;
		$this->admin_username->ViewCustomAttributes = "";

		// admin_password
		$this->admin_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->admin_password->ViewCustomAttributes = "";

		// admin_last_update
		$this->admin_last_update->ViewValue = $this->admin_last_update->CurrentValue;
		$this->admin_last_update->ViewValue = ew_FormatDateTime($this->admin_last_update->ViewValue, 7);
		$this->admin_last_update->ViewCustomAttributes = "";

			// admin_id
			$this->admin_id->LinkCustomAttributes = "";
			$this->admin_id->HrefValue = "";
			$this->admin_id->TooltipValue = "";

			// admin_name
			$this->admin_name->LinkCustomAttributes = "";
			$this->admin_name->HrefValue = "";
			$this->admin_name->TooltipValue = "";

			// admin_surname
			$this->admin_surname->LinkCustomAttributes = "";
			$this->admin_surname->HrefValue = "";
			$this->admin_surname->TooltipValue = "";

			// admin_company_name
			$this->admin_company_name->LinkCustomAttributes = "";
			$this->admin_company_name->HrefValue = "";
			$this->admin_company_name->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->admin_id->AdvancedSearch->Load();
		$this->admin_name->AdvancedSearch->Load();
		$this->admin_surname->AdvancedSearch->Load();
		$this->admin_gender->AdvancedSearch->Load();
		$this->admin_email->AdvancedSearch->Load();
		$this->admin_mobile->AdvancedSearch->Load();
		$this->admin_phone->AdvancedSearch->Load();
		$this->admin_modules->AdvancedSearch->Load();
		$this->admin_language->AdvancedSearch->Load();
		$this->admin_is_active->AdvancedSearch->Load();
		$this->admin_company_name->AdvancedSearch->Load();
		$this->admin_company_vat->AdvancedSearch->Load();
		$this->admin_company_logo_file->AdvancedSearch->Load();
		$this->admin_company_address->AdvancedSearch->Load();
		$this->admin_company_region_id->AdvancedSearch->Load();
		$this->admin_company_province_id->AdvancedSearch->Load();
		$this->admin_company_city_id->AdvancedSearch->Load();
		$this->admin_company_phone->AdvancedSearch->Load();
		$this->admin_company_email->AdvancedSearch->Load();
		$this->admin_company_web->AdvancedSearch->Load();
		$this->admin_contract->AdvancedSearch->Load();
		$this->admin_expire->AdvancedSearch->Load();
		$this->admin_company_facebook->AdvancedSearch->Load();
		$this->admin_company_twitter->AdvancedSearch->Load();
		$this->admin_company_googleplus->AdvancedSearch->Load();
		$this->admin_company_linkedin->AdvancedSearch->Load();
		$this->admin_link_playstore->AdvancedSearch->Load();
		$this->admin_link_appstore->AdvancedSearch->Load();
		$this->admin_link_windowsstore->AdvancedSearch->Load();
		$this->admin_level->AdvancedSearch->Load();
		$this->admin_username->AdvancedSearch->Load();
		$this->admin_password->AdvancedSearch->Load();
		$this->admin_last_update->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_telecare_admin\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_telecare_admin',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.ftelecare_adminlist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->admin_id->CurrentValue);
		return TRUE;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_admin_list)) $telecare_admin_list = new ctelecare_admin_list();

// Page init
$telecare_admin_list->Page_Init();

// Page main
$telecare_admin_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_admin_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($telecare_admin->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = ftelecare_adminlist = new ew_Form("ftelecare_adminlist", "list");
ftelecare_adminlist.FormKeyCountName = '<?php echo $telecare_admin_list->FormKeyCountName ?>';

// Form_CustomValidate event
ftelecare_adminlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_adminlist.ValidateRequired = true;
<?php } else { ?>
ftelecare_adminlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

var CurrentSearchForm = ftelecare_adminlistsrch = new ew_Form("ftelecare_adminlistsrch");
</script>
<style type="text/css">
.ewTablePreviewRow { /* main table preview row color */
	background-color: #FFFFFF; /* preview row color */
}
.ewTablePreviewRow .ewGrid {
	display: table;
}
.ewTablePreviewRow .ewGrid .ewTable {
	width: auto;
}
</style>
<div id="ewPreview" class="hide"><ul class="nav nav-tabs"></ul><div class="tab-content"><div class="tab-pane fade"></div></div></div>
<script type="text/javascript" src="phpjs/ewpreview.min.js"></script>
<script type="text/javascript">
var EW_PREVIEW_PLACEMENT = EW_CSS_FLIP ? "left" : "right";
var EW_PREVIEW_SINGLE_ROW = false;
var EW_PREVIEW_OVERLAY = false;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
<div class="ewToolbar">
<?php if ($telecare_admin->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($telecare_admin_list->TotalRecs > 0 && $telecare_admin_list->ExportOptions->Visible()) { ?>
<?php $telecare_admin_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_admin_list->SearchOptions->Visible()) { ?>
<?php $telecare_admin_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_admin_list->FilterOptions->Visible()) { ?>
<?php $telecare_admin_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
	$bSelectLimit = $telecare_admin_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($telecare_admin_list->TotalRecs <= 0)
			$telecare_admin_list->TotalRecs = $telecare_admin->SelectRecordCount();
	} else {
		if (!$telecare_admin_list->Recordset && ($telecare_admin_list->Recordset = $telecare_admin_list->LoadRecordset()))
			$telecare_admin_list->TotalRecs = $telecare_admin_list->Recordset->RecordCount();
	}
	$telecare_admin_list->StartRec = 1;
	if ($telecare_admin_list->DisplayRecs <= 0 || ($telecare_admin->Export <> "" && $telecare_admin->ExportAll)) // Display all records
		$telecare_admin_list->DisplayRecs = $telecare_admin_list->TotalRecs;
	if (!($telecare_admin->Export <> "" && $telecare_admin->ExportAll))
		$telecare_admin_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$telecare_admin_list->Recordset = $telecare_admin_list->LoadRecordset($telecare_admin_list->StartRec-1, $telecare_admin_list->DisplayRecs);

	// Set no record found message
	if ($telecare_admin->CurrentAction == "" && $telecare_admin_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$telecare_admin_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($telecare_admin_list->SearchWhere == "0=101")
			$telecare_admin_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$telecare_admin_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
$telecare_admin_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($telecare_admin->Export == "" && $telecare_admin->CurrentAction == "") { ?>
<form name="ftelecare_adminlistsrch" id="ftelecare_adminlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($telecare_admin_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="ftelecare_adminlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="telecare_admin">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($telecare_admin_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($telecare_admin_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $telecare_admin_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($telecare_admin_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($telecare_admin_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($telecare_admin_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($telecare_admin_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $telecare_admin_list->ShowPageHeader(); ?>
<?php
$telecare_admin_list->ShowMessage();
?>
<?php if ($telecare_admin_list->TotalRecs > 0 || $telecare_admin->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<?php if ($telecare_admin->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($telecare_admin->CurrentAction <> "gridadd" && $telecare_admin->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_admin_list->Pager)) $telecare_admin_list->Pager = new cPrevNextPager($telecare_admin_list->StartRec, $telecare_admin_list->DisplayRecs, $telecare_admin_list->TotalRecs) ?>
<?php if ($telecare_admin_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_admin_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_admin_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_admin_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_admin_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_admin_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_admin_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $telecare_admin_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $telecare_admin_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $telecare_admin_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_admin_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="ftelecare_adminlist" id="ftelecare_adminlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_admin_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_admin_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_admin">
<div id="gmp_telecare_admin" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($telecare_admin_list->TotalRecs > 0) { ?>
<table id="tbl_telecare_adminlist" class="table ewTable">
<?php echo $telecare_admin->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$telecare_admin_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$telecare_admin_list->RenderListOptions();

// Render list options (header, left)
$telecare_admin_list->ListOptions->Render("header", "left");
?>
<?php if ($telecare_admin->admin_id->Visible) { // admin_id ?>
	<?php if ($telecare_admin->SortUrl($telecare_admin->admin_id) == "") { ?>
		<th data-name="admin_id"><div id="elh_telecare_admin_admin_id" class="telecare_admin_admin_id"><div class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="admin_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_admin->SortUrl($telecare_admin->admin_id) ?>',1);"><div id="elh_telecare_admin_admin_id" class="telecare_admin_admin_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_admin->admin_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_admin->admin_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_admin->admin_name->Visible) { // admin_name ?>
	<?php if ($telecare_admin->SortUrl($telecare_admin->admin_name) == "") { ?>
		<th data-name="admin_name"><div id="elh_telecare_admin_admin_name" class="telecare_admin_admin_name"><div class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="admin_name"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_admin->SortUrl($telecare_admin->admin_name) ?>',1);"><div id="elh_telecare_admin_admin_name" class="telecare_admin_admin_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_name->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_admin->admin_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_admin->admin_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_admin->admin_surname->Visible) { // admin_surname ?>
	<?php if ($telecare_admin->SortUrl($telecare_admin->admin_surname) == "") { ?>
		<th data-name="admin_surname"><div id="elh_telecare_admin_admin_surname" class="telecare_admin_admin_surname"><div class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_surname->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="admin_surname"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_admin->SortUrl($telecare_admin->admin_surname) ?>',1);"><div id="elh_telecare_admin_admin_surname" class="telecare_admin_admin_surname">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_surname->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_admin->admin_surname->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_admin->admin_surname->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_admin->admin_company_name->Visible) { // admin_company_name ?>
	<?php if ($telecare_admin->SortUrl($telecare_admin->admin_company_name) == "") { ?>
		<th data-name="admin_company_name"><div id="elh_telecare_admin_admin_company_name" class="telecare_admin_admin_company_name"><div class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_company_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="admin_company_name"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_admin->SortUrl($telecare_admin->admin_company_name) ?>',1);"><div id="elh_telecare_admin_admin_company_name" class="telecare_admin_admin_company_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_admin->admin_company_name->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_admin->admin_company_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_admin->admin_company_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$telecare_admin_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($telecare_admin->ExportAll && $telecare_admin->Export <> "") {
	$telecare_admin_list->StopRec = $telecare_admin_list->TotalRecs;
} else {

	// Set the last record to display
	if ($telecare_admin_list->TotalRecs > $telecare_admin_list->StartRec + $telecare_admin_list->DisplayRecs - 1)
		$telecare_admin_list->StopRec = $telecare_admin_list->StartRec + $telecare_admin_list->DisplayRecs - 1;
	else
		$telecare_admin_list->StopRec = $telecare_admin_list->TotalRecs;
}
$telecare_admin_list->RecCnt = $telecare_admin_list->StartRec - 1;
if ($telecare_admin_list->Recordset && !$telecare_admin_list->Recordset->EOF) {
	$telecare_admin_list->Recordset->MoveFirst();
	$bSelectLimit = $telecare_admin_list->UseSelectLimit;
	if (!$bSelectLimit && $telecare_admin_list->StartRec > 1)
		$telecare_admin_list->Recordset->Move($telecare_admin_list->StartRec - 1);
} elseif (!$telecare_admin->AllowAddDeleteRow && $telecare_admin_list->StopRec == 0) {
	$telecare_admin_list->StopRec = $telecare_admin->GridAddRowCount;
}

// Initialize aggregate
$telecare_admin->RowType = EW_ROWTYPE_AGGREGATEINIT;
$telecare_admin->ResetAttrs();
$telecare_admin_list->RenderRow();
while ($telecare_admin_list->RecCnt < $telecare_admin_list->StopRec) {
	$telecare_admin_list->RecCnt++;
	if (intval($telecare_admin_list->RecCnt) >= intval($telecare_admin_list->StartRec)) {
		$telecare_admin_list->RowCnt++;

		// Set up key count
		$telecare_admin_list->KeyCount = $telecare_admin_list->RowIndex;

		// Init row class and style
		$telecare_admin->ResetAttrs();
		$telecare_admin->CssClass = "";
		if ($telecare_admin->CurrentAction == "gridadd") {
		} else {
			$telecare_admin_list->LoadRowValues($telecare_admin_list->Recordset); // Load row values
		}
		$telecare_admin->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$telecare_admin->RowAttrs = array_merge($telecare_admin->RowAttrs, array('data-rowindex'=>$telecare_admin_list->RowCnt, 'id'=>'r' . $telecare_admin_list->RowCnt . '_telecare_admin', 'data-rowtype'=>$telecare_admin->RowType));

		// Render row
		$telecare_admin_list->RenderRow();

		// Render list options
		$telecare_admin_list->RenderListOptions();
?>
	<tr<?php echo $telecare_admin->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_admin_list->ListOptions->Render("body", "left", $telecare_admin_list->RowCnt);
?>
	<?php if ($telecare_admin->admin_id->Visible) { // admin_id ?>
		<td data-name="admin_id"<?php echo $telecare_admin->admin_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_admin_list->RowCnt ?>_telecare_admin_admin_id" class="telecare_admin_admin_id">
<span<?php echo $telecare_admin->admin_id->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_id->ListViewValue() ?></span>
</span>
<a id="<?php echo $telecare_admin_list->PageObjName . "_row_" . $telecare_admin_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($telecare_admin->admin_name->Visible) { // admin_name ?>
		<td data-name="admin_name"<?php echo $telecare_admin->admin_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_admin_list->RowCnt ?>_telecare_admin_admin_name" class="telecare_admin_admin_name">
<span<?php echo $telecare_admin->admin_name->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_name->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_admin->admin_surname->Visible) { // admin_surname ?>
		<td data-name="admin_surname"<?php echo $telecare_admin->admin_surname->CellAttributes() ?>>
<span id="el<?php echo $telecare_admin_list->RowCnt ?>_telecare_admin_admin_surname" class="telecare_admin_admin_surname">
<span<?php echo $telecare_admin->admin_surname->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_surname->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_admin->admin_company_name->Visible) { // admin_company_name ?>
		<td data-name="admin_company_name"<?php echo $telecare_admin->admin_company_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_admin_list->RowCnt ?>_telecare_admin_admin_company_name" class="telecare_admin_admin_company_name">
<span<?php echo $telecare_admin->admin_company_name->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_name->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_admin_list->ListOptions->Render("body", "right", $telecare_admin_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($telecare_admin->CurrentAction <> "gridadd")
		$telecare_admin_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($telecare_admin->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($telecare_admin_list->Recordset)
	$telecare_admin_list->Recordset->Close();
?>
<?php if ($telecare_admin->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($telecare_admin->CurrentAction <> "gridadd" && $telecare_admin->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_admin_list->Pager)) $telecare_admin_list->Pager = new cPrevNextPager($telecare_admin_list->StartRec, $telecare_admin_list->DisplayRecs, $telecare_admin_list->TotalRecs) ?>
<?php if ($telecare_admin_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_admin_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_admin_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_admin_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_admin_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_admin_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_admin_list->PageUrl() ?>start=<?php echo $telecare_admin_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_admin_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $telecare_admin_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $telecare_admin_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $telecare_admin_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_admin_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($telecare_admin_list->TotalRecs == 0 && $telecare_admin->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_admin_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
<script type="text/javascript">
ftelecare_adminlistsrch.Init();
ftelecare_adminlistsrch.FilterList = <?php echo $telecare_admin_list->GetFilterList() ?>;
ftelecare_adminlist.Init();
</script>
<?php } ?>
<?php
$telecare_admin_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($telecare_admin->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$telecare_admin_list->Page_Terminate();
?>
