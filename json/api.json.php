<?php 
header('content-type: application/json; charset=utf-8');
	@ini_set("display_errors", "0"); // Display errors
	error_reporting(E_ALL ^ E_NOTICE); // Report all errors except E_NOTICE
require_once('class/Crud.class.php');
?>
<?php
$db = new Db();

$payload= array(
	"alarm_id",
	"alarm_user_id",
	"alarm_product_id",
	"alarm_datetime",
	"alarm_latitude",
	"alarm_longitude",
	"alarm_start_on",
	"alarm_close_on",
	"alarm_close",
	"alarm_admin_id",
	"alarm_type_id",
	"alarm_call_parents",
	"alarm_call_emergency",
	"alarm_note",
	"alarm_last_update"
);

$parameters = array(
	"serial",
	"type"
);

$start_on = new DateTime();

foreach ($parameters as $key) {
    if (!isset($_POST[$key])) {
        $json['description'] = "Access Denied, plese verify your token key and try again.";
        $json['message'] = "$key required";        
        $json['success'] = false;        
        echo json_encode($json);
        die();
    }
}

		$db->bind('phone',$_POST['serial']);
		$product_id = $db->row("SELECT product_id, product_user_id FROM telecare_product WHERE product_name LIKE '".$_POST['serial']."' ");
		$now = new DateTime();
if ($product_id['product_user_id']>0){
	if ($_POST['type'] == 'alarm'){
		$SQL = "INSERT INTO telecare_alarm (
			alarm_user_id,
			alarm_product_id,
			alarm_datetime,
			alarm_latitude,
			alarm_longitude,
			alarm_type_id
		) VALUES (
			".$product_id['product_user_id'].",
			".$product_id['product_id'].",
			'".$now->format('Y-m-d H:i:s')."',
			'".$_POST['latitude']."',
			'".$_POST['longitude']."',
			".$_POST['type_id']."
		)";
		//echo $SQL;
		$db->query($SQL);
	}

	if ($_POST['type'] == 'data'){



		$SQL = "INSERT INTO `telecare_data`
		(
			`data_product_id`,
			`data_value_pattern`
		) 
		VALUES 
		(
			".$product_id['product_id'].",
			'".$_POST['data']."'
		)";
		//echo $SQL;
		$db->query($SQL);
	}

	echo json_encode(array("success" => true));
	return;
}

echo json_encode(array("success" => false));
return;

?>
