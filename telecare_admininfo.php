<?php

// Global variable for table object
$telecare_admin = NULL;

//
// Table class for telecare_admin
//
class ctelecare_admin extends cTable {
	var $admin_id;
	var $admin_parent_id;
	var $admin_name;
	var $admin_surname;
	var $admin_gender;
	var $admin_email;
	var $admin_mobile;
	var $admin_phone;
	var $admin_modules;
	var $admin_language;
	var $admin_is_active;
	var $admin_company_name;
	var $admin_company_vat;
	var $admin_company_logo_file;
	var $admin_company_logo_width;
	var $admin_company_logo_height;
	var $admin_company_logo_size;
	var $admin_company_address;
	var $admin_company_region_id;
	var $admin_company_province_id;
	var $admin_company_city_id;
	var $admin_company_phone;
	var $admin_company_email;
	var $admin_company_web;
	var $admin_contract;
	var $admin_expire;
	var $admin_company_facebook;
	var $admin_company_twitter;
	var $admin_company_googleplus;
	var $admin_company_linkedin;
	var $admin_link_playstore;
	var $admin_link_appstore;
	var $admin_link_windowsstore;
	var $admin_level;
	var $admin_username;
	var $admin_password;
	var $admin_company_autohority;
	var $admin_auth_token;
	var $admin_last_update;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'telecare_admin';
		$this->TableName = 'telecare_admin';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`telecare_admin`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = PHPExcel_Worksheet_PageSetup::ORIENTATION_DEFAULT; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// admin_id
		$this->admin_id = new cField('telecare_admin', 'telecare_admin', 'x_admin_id', 'admin_id', '`admin_id`', '`admin_id`', 3, -1, FALSE, '`admin_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->admin_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_id'] = &$this->admin_id;

		// admin_parent_id
		$this->admin_parent_id = new cField('telecare_admin', 'telecare_admin', 'x_admin_parent_id', 'admin_parent_id', '`admin_parent_id`', '`admin_parent_id`', 3, -1, FALSE, '`admin_parent_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_parent_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_parent_id'] = &$this->admin_parent_id;

		// admin_name
		$this->admin_name = new cField('telecare_admin', 'telecare_admin', 'x_admin_name', 'admin_name', '`admin_name`', '`admin_name`', 200, -1, FALSE, '`admin_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_name'] = &$this->admin_name;

		// admin_surname
		$this->admin_surname = new cField('telecare_admin', 'telecare_admin', 'x_admin_surname', 'admin_surname', '`admin_surname`', '`admin_surname`', 200, -1, FALSE, '`admin_surname`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_surname'] = &$this->admin_surname;

		// admin_gender
		$this->admin_gender = new cField('telecare_admin', 'telecare_admin', 'x_admin_gender', 'admin_gender', '`admin_gender`', '`admin_gender`', 16, -1, FALSE, '`admin_gender`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->admin_gender->OptionCount = 2;
		$this->admin_gender->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_gender'] = &$this->admin_gender;

		// admin_email
		$this->admin_email = new cField('telecare_admin', 'telecare_admin', 'x_admin_email', 'admin_email', '`admin_email`', '`admin_email`', 200, -1, FALSE, '`admin_email`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_email->FldDefaultErrMsg = $Language->Phrase("IncorrectEmail");
		$this->fields['admin_email'] = &$this->admin_email;

		// admin_mobile
		$this->admin_mobile = new cField('telecare_admin', 'telecare_admin', 'x_admin_mobile', 'admin_mobile', '`admin_mobile`', '`admin_mobile`', 200, -1, FALSE, '`admin_mobile`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_mobile'] = &$this->admin_mobile;

		// admin_phone
		$this->admin_phone = new cField('telecare_admin', 'telecare_admin', 'x_admin_phone', 'admin_phone', '`admin_phone`', '`admin_phone`', 200, -1, FALSE, '`admin_phone`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_phone'] = &$this->admin_phone;

		// admin_modules
		$this->admin_modules = new cField('telecare_admin', 'telecare_admin', 'x_admin_modules', 'admin_modules', '`admin_modules`', '`admin_modules`', 200, -1, FALSE, '`admin_modules`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'CHECKBOX');
		$this->fields['admin_modules'] = &$this->admin_modules;

		// admin_language
		$this->admin_language = new cField('telecare_admin', 'telecare_admin', 'x_admin_language', 'admin_language', '`admin_language`', '`admin_language`', 3, -1, FALSE, '`admin_language`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->admin_language->OptionCount = 2;
		$this->admin_language->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_language'] = &$this->admin_language;

		// admin_is_active
		$this->admin_is_active = new cField('telecare_admin', 'telecare_admin', 'x_admin_is_active', 'admin_is_active', '`admin_is_active`', '`admin_is_active`', 200, -1, FALSE, '`admin_is_active`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->admin_is_active->OptionCount = 2;
		$this->admin_is_active->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_is_active'] = &$this->admin_is_active;

		// admin_company_name
		$this->admin_company_name = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_name', 'admin_company_name', '`admin_company_name`', '`admin_company_name`', 200, -1, FALSE, '`admin_company_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_name'] = &$this->admin_company_name;

		// admin_company_vat
		$this->admin_company_vat = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_vat', 'admin_company_vat', '`admin_company_vat`', '`admin_company_vat`', 200, -1, FALSE, '`admin_company_vat`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_company_vat->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_company_vat'] = &$this->admin_company_vat;

		// admin_company_logo_file
		$this->admin_company_logo_file = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_logo_file', 'admin_company_logo_file', '`admin_company_logo_file`', '`admin_company_logo_file`', 200, -1, TRUE, '`admin_company_logo_file`', FALSE, FALSE, FALSE, 'IMAGE', 'FILE');
		$this->admin_company_logo_file->ImageResize = TRUE;
		$this->fields['admin_company_logo_file'] = &$this->admin_company_logo_file;

		// admin_company_logo_width
		$this->admin_company_logo_width = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_logo_width', 'admin_company_logo_width', '`admin_company_logo_width`', '`admin_company_logo_width`', 3, -1, FALSE, '`admin_company_logo_width`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_company_logo_width->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_company_logo_width'] = &$this->admin_company_logo_width;

		// admin_company_logo_height
		$this->admin_company_logo_height = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_logo_height', 'admin_company_logo_height', '`admin_company_logo_height`', '`admin_company_logo_height`', 3, -1, FALSE, '`admin_company_logo_height`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_company_logo_height->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_company_logo_height'] = &$this->admin_company_logo_height;

		// admin_company_logo_size
		$this->admin_company_logo_size = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_logo_size', 'admin_company_logo_size', '`admin_company_logo_size`', '`admin_company_logo_size`', 3, -1, FALSE, '`admin_company_logo_size`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_company_logo_size->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_company_logo_size'] = &$this->admin_company_logo_size;

		// admin_company_address
		$this->admin_company_address = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_address', 'admin_company_address', '`admin_company_address`', '`admin_company_address`', 200, -1, FALSE, '`admin_company_address`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_address'] = &$this->admin_company_address;

		// admin_company_region_id
		$this->admin_company_region_id = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_region_id', 'admin_company_region_id', '`admin_company_region_id`', '`admin_company_region_id`', 3, -1, FALSE, '`admin_company_region_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->admin_company_region_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_company_region_id'] = &$this->admin_company_region_id;

		// admin_company_province_id
		$this->admin_company_province_id = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_province_id', 'admin_company_province_id', '`admin_company_province_id`', '`admin_company_province_id`', 3, -1, FALSE, '`admin_company_province_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->admin_company_province_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_company_province_id'] = &$this->admin_company_province_id;

		// admin_company_city_id
		$this->admin_company_city_id = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_city_id', 'admin_company_city_id', '`admin_company_city_id`', '`admin_company_city_id`', 3, -1, FALSE, '`admin_company_city_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->fields['admin_company_city_id'] = &$this->admin_company_city_id;

		// admin_company_phone
		$this->admin_company_phone = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_phone', 'admin_company_phone', '`admin_company_phone`', '`admin_company_phone`', 200, -1, FALSE, '`admin_company_phone`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_phone'] = &$this->admin_company_phone;

		// admin_company_email
		$this->admin_company_email = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_email', 'admin_company_email', '`admin_company_email`', '`admin_company_email`', 200, -1, FALSE, '`admin_company_email`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_company_email->FldDefaultErrMsg = $Language->Phrase("IncorrectEmail");
		$this->fields['admin_company_email'] = &$this->admin_company_email;

		// admin_company_web
		$this->admin_company_web = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_web', 'admin_company_web', '`admin_company_web`', '`admin_company_web`', 200, -1, FALSE, '`admin_company_web`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_web'] = &$this->admin_company_web;

		// admin_contract
		$this->admin_contract = new cField('telecare_admin', 'telecare_admin', 'x_admin_contract', 'admin_contract', '`admin_contract`', 'DATE_FORMAT(`admin_contract`, \'%d/%m/%Y\')', 133, 7, FALSE, '`admin_contract`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_contract->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['admin_contract'] = &$this->admin_contract;

		// admin_expire
		$this->admin_expire = new cField('telecare_admin', 'telecare_admin', 'x_admin_expire', 'admin_expire', '`admin_expire`', 'DATE_FORMAT(`admin_expire`, \'%d/%m/%Y\')', 133, 7, FALSE, '`admin_expire`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_expire->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['admin_expire'] = &$this->admin_expire;

		// admin_company_facebook
		$this->admin_company_facebook = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_facebook', 'admin_company_facebook', '`admin_company_facebook`', '`admin_company_facebook`', 200, -1, FALSE, '`admin_company_facebook`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_facebook'] = &$this->admin_company_facebook;

		// admin_company_twitter
		$this->admin_company_twitter = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_twitter', 'admin_company_twitter', '`admin_company_twitter`', '`admin_company_twitter`', 200, -1, FALSE, '`admin_company_twitter`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_twitter'] = &$this->admin_company_twitter;

		// admin_company_googleplus
		$this->admin_company_googleplus = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_googleplus', 'admin_company_googleplus', '`admin_company_googleplus`', '`admin_company_googleplus`', 200, -1, FALSE, '`admin_company_googleplus`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_googleplus'] = &$this->admin_company_googleplus;

		// admin_company_linkedin
		$this->admin_company_linkedin = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_linkedin', 'admin_company_linkedin', '`admin_company_linkedin`', '`admin_company_linkedin`', 200, -1, FALSE, '`admin_company_linkedin`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_linkedin'] = &$this->admin_company_linkedin;

		// admin_link_playstore
		$this->admin_link_playstore = new cField('telecare_admin', 'telecare_admin', 'x_admin_link_playstore', 'admin_link_playstore', '`admin_link_playstore`', '`admin_link_playstore`', 200, -1, FALSE, '`admin_link_playstore`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_link_playstore'] = &$this->admin_link_playstore;

		// admin_link_appstore
		$this->admin_link_appstore = new cField('telecare_admin', 'telecare_admin', 'x_admin_link_appstore', 'admin_link_appstore', '`admin_link_appstore`', '`admin_link_appstore`', 200, -1, FALSE, '`admin_link_appstore`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_link_appstore'] = &$this->admin_link_appstore;

		// admin_link_windowsstore
		$this->admin_link_windowsstore = new cField('telecare_admin', 'telecare_admin', 'x_admin_link_windowsstore', 'admin_link_windowsstore', '`admin_link_windowsstore`', '`admin_link_windowsstore`', 200, -1, FALSE, '`admin_link_windowsstore`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_link_windowsstore'] = &$this->admin_link_windowsstore;

		// admin_level
		$this->admin_level = new cField('telecare_admin', 'telecare_admin', 'x_admin_level', 'admin_level', '`admin_level`', '`admin_level`', 3, -1, FALSE, '`admin_level`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->admin_level->OptionCount = 7;
		$this->admin_level->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['admin_level'] = &$this->admin_level;

		// admin_username
		$this->admin_username = new cField('telecare_admin', 'telecare_admin', 'x_admin_username', 'admin_username', '`admin_username`', '`admin_username`', 200, -1, FALSE, '`admin_username`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_username'] = &$this->admin_username;

		// admin_password
		$this->admin_password = new cField('telecare_admin', 'telecare_admin', 'x_admin_password', 'admin_password', '`admin_password`', '`admin_password`', 200, -1, FALSE, '`admin_password`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'PASSWORD');
		$this->fields['admin_password'] = &$this->admin_password;

		// admin_company_autohority
		$this->admin_company_autohority = new cField('telecare_admin', 'telecare_admin', 'x_admin_company_autohority', 'admin_company_autohority', '`admin_company_autohority`', '`admin_company_autohority`', 200, -1, FALSE, '`admin_company_autohority`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_company_autohority'] = &$this->admin_company_autohority;

		// admin_auth_token
		$this->admin_auth_token = new cField('telecare_admin', 'telecare_admin', 'x_admin_auth_token', 'admin_auth_token', '`admin_auth_token`', '`admin_auth_token`', 200, -1, FALSE, '`admin_auth_token`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['admin_auth_token'] = &$this->admin_auth_token;

		// admin_last_update
		$this->admin_last_update = new cField('telecare_admin', 'telecare_admin', 'x_admin_last_update', 'admin_last_update', '`admin_last_update`', 'DATE_FORMAT(`admin_last_update`, \'%d/%m/%Y\')', 135, 7, FALSE, '`admin_last_update`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->admin_last_update->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['admin_last_update'] = &$this->admin_last_update;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`telecare_admin`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		global $Security;

		// Add User ID filter
		if ($Security->CurrentUserID() <> "" && !$Security->IsAdmin()) { // Non system admin
			$sFilter = $this->AddUserIDFilter($sFilter);
		}
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = $this->UserIDAllowSecurity;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			if (EW_ENCRYPTED_PASSWORD && $name == 'admin_password')
				$value = (EW_CASE_SENSITIVE_PASSWORD) ? ew_EncryptPassword($value) : ew_EncryptPassword(strtolower($value));
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			if (EW_ENCRYPTED_PASSWORD && $name == 'admin_password') {
				$value = (EW_CASE_SENSITIVE_PASSWORD) ? ew_EncryptPassword($value) : ew_EncryptPassword(strtolower($value));
			}
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('admin_id', $rs))
				ew_AddFilter($where, ew_QuotedName('admin_id', $this->DBID) . '=' . ew_QuotedValue($rs['admin_id'], $this->admin_id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`admin_id` = @admin_id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->admin_id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@admin_id@", ew_AdjustSql($this->admin_id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "telecare_adminlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "telecare_adminlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_adminview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_adminview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "telecare_adminadd.php?" . $this->UrlParm($parm);
		else
			$url = "telecare_adminadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("telecare_adminedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("telecare_adminadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("telecare_admindelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "admin_id:" . ew_VarToJson($this->admin_id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->admin_id->CurrentValue)) {
			$sUrl .= "admin_id=" . urlencode($this->admin_id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["admin_id"]) : ew_StripSlashes(@$_GET["admin_id"]); // admin_id

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->admin_id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->admin_id->setDbValue($rs->fields('admin_id'));
		$this->admin_parent_id->setDbValue($rs->fields('admin_parent_id'));
		$this->admin_name->setDbValue($rs->fields('admin_name'));
		$this->admin_surname->setDbValue($rs->fields('admin_surname'));
		$this->admin_gender->setDbValue($rs->fields('admin_gender'));
		$this->admin_email->setDbValue($rs->fields('admin_email'));
		$this->admin_mobile->setDbValue($rs->fields('admin_mobile'));
		$this->admin_phone->setDbValue($rs->fields('admin_phone'));
		$this->admin_modules->setDbValue($rs->fields('admin_modules'));
		$this->admin_language->setDbValue($rs->fields('admin_language'));
		$this->admin_is_active->setDbValue($rs->fields('admin_is_active'));
		$this->admin_company_name->setDbValue($rs->fields('admin_company_name'));
		$this->admin_company_vat->setDbValue($rs->fields('admin_company_vat'));
		$this->admin_company_logo_file->Upload->DbValue = $rs->fields('admin_company_logo_file');
		$this->admin_company_logo_width->setDbValue($rs->fields('admin_company_logo_width'));
		$this->admin_company_logo_height->setDbValue($rs->fields('admin_company_logo_height'));
		$this->admin_company_logo_size->setDbValue($rs->fields('admin_company_logo_size'));
		$this->admin_company_address->setDbValue($rs->fields('admin_company_address'));
		$this->admin_company_region_id->setDbValue($rs->fields('admin_company_region_id'));
		$this->admin_company_province_id->setDbValue($rs->fields('admin_company_province_id'));
		$this->admin_company_city_id->setDbValue($rs->fields('admin_company_city_id'));
		$this->admin_company_phone->setDbValue($rs->fields('admin_company_phone'));
		$this->admin_company_email->setDbValue($rs->fields('admin_company_email'));
		$this->admin_company_web->setDbValue($rs->fields('admin_company_web'));
		$this->admin_contract->setDbValue($rs->fields('admin_contract'));
		$this->admin_expire->setDbValue($rs->fields('admin_expire'));
		$this->admin_company_facebook->setDbValue($rs->fields('admin_company_facebook'));
		$this->admin_company_twitter->setDbValue($rs->fields('admin_company_twitter'));
		$this->admin_company_googleplus->setDbValue($rs->fields('admin_company_googleplus'));
		$this->admin_company_linkedin->setDbValue($rs->fields('admin_company_linkedin'));
		$this->admin_link_playstore->setDbValue($rs->fields('admin_link_playstore'));
		$this->admin_link_appstore->setDbValue($rs->fields('admin_link_appstore'));
		$this->admin_link_windowsstore->setDbValue($rs->fields('admin_link_windowsstore'));
		$this->admin_level->setDbValue($rs->fields('admin_level'));
		$this->admin_username->setDbValue($rs->fields('admin_username'));
		$this->admin_password->setDbValue($rs->fields('admin_password'));
		$this->admin_company_autohority->setDbValue($rs->fields('admin_company_autohority'));
		$this->admin_auth_token->setDbValue($rs->fields('admin_auth_token'));
		$this->admin_last_update->setDbValue($rs->fields('admin_last_update'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// admin_id
		// admin_parent_id

		$this->admin_parent_id->CellCssStyle = "white-space: nowrap;";

		// admin_name
		// admin_surname
		// admin_gender
		// admin_email
		// admin_mobile
		// admin_phone
		// admin_modules
		// admin_language
		// admin_is_active
		// admin_company_name
		// admin_company_vat
		// admin_company_logo_file
		// admin_company_logo_width
		// admin_company_logo_height
		// admin_company_logo_size
		// admin_company_address
		// admin_company_region_id
		// admin_company_province_id
		// admin_company_city_id
		// admin_company_phone
		// admin_company_email
		// admin_company_web
		// admin_contract

		$this->admin_contract->CellCssStyle = "white-space: nowrap;";

		// admin_expire
		$this->admin_expire->CellCssStyle = "white-space: nowrap;";

		// admin_company_facebook
		// admin_company_twitter
		// admin_company_googleplus
		// admin_company_linkedin
		// admin_link_playstore
		// admin_link_appstore
		// admin_link_windowsstore
		// admin_level
		// admin_username
		// admin_password
		// admin_company_autohority

		$this->admin_company_autohority->CellCssStyle = "white-space: nowrap;";

		// admin_auth_token
		$this->admin_auth_token->CellCssStyle = "white-space: nowrap;";

		// admin_last_update
		// admin_id

		$this->admin_id->ViewValue = $this->admin_id->CurrentValue;
		$this->admin_id->ViewCustomAttributes = "";

		// admin_parent_id
		$this->admin_parent_id->ViewValue = $this->admin_parent_id->CurrentValue;
		$this->admin_parent_id->ViewCustomAttributes = "";

		// admin_name
		$this->admin_name->ViewValue = $this->admin_name->CurrentValue;
		$this->admin_name->ViewCustomAttributes = "";

		// admin_surname
		$this->admin_surname->ViewValue = $this->admin_surname->CurrentValue;
		$this->admin_surname->ViewCustomAttributes = "";

		// admin_gender
		if (strval($this->admin_gender->CurrentValue) <> "") {
			$this->admin_gender->ViewValue = $this->admin_gender->OptionCaption($this->admin_gender->CurrentValue);
		} else {
			$this->admin_gender->ViewValue = NULL;
		}
		$this->admin_gender->ViewCustomAttributes = "";

		// admin_email
		$this->admin_email->ViewValue = $this->admin_email->CurrentValue;
		$this->admin_email->ViewCustomAttributes = "";

		// admin_mobile
		$this->admin_mobile->ViewValue = $this->admin_mobile->CurrentValue;
		$this->admin_mobile->ViewCustomAttributes = "";

		// admin_phone
		$this->admin_phone->ViewValue = $this->admin_phone->CurrentValue;
		$this->admin_phone->ViewCustomAttributes = "";

		// admin_modules
		if (strval($this->admin_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->admin_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->admin_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->admin_modules->ViewValue .= $this->admin_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->admin_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->admin_modules->ViewValue = $this->admin_modules->CurrentValue;
			}
		} else {
			$this->admin_modules->ViewValue = NULL;
		}
		$this->admin_modules->ViewCustomAttributes = "";

		// admin_language
		if (strval($this->admin_language->CurrentValue) <> "") {
			$this->admin_language->ViewValue = $this->admin_language->OptionCaption($this->admin_language->CurrentValue);
		} else {
			$this->admin_language->ViewValue = NULL;
		}
		$this->admin_language->ViewCustomAttributes = "";

		// admin_is_active
		if (strval($this->admin_is_active->CurrentValue) <> "") {
			$this->admin_is_active->ViewValue = $this->admin_is_active->OptionCaption($this->admin_is_active->CurrentValue);
		} else {
			$this->admin_is_active->ViewValue = NULL;
		}
		$this->admin_is_active->ViewCustomAttributes = "";

		// admin_company_name
		$this->admin_company_name->ViewValue = $this->admin_company_name->CurrentValue;
		$this->admin_company_name->ViewCustomAttributes = "";

		// admin_company_vat
		$this->admin_company_vat->ViewValue = $this->admin_company_vat->CurrentValue;
		$this->admin_company_vat->ViewCustomAttributes = "";

		// admin_company_logo_file
		if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
			$this->admin_company_logo_file->ImageWidth = 100;
			$this->admin_company_logo_file->ImageHeight = 0;
			$this->admin_company_logo_file->ImageAlt = $this->admin_company_logo_file->FldAlt();
			$this->admin_company_logo_file->ViewValue = $this->admin_company_logo_file->Upload->DbValue;
		} else {
			$this->admin_company_logo_file->ViewValue = "";
		}
		$this->admin_company_logo_file->ViewCustomAttributes = "";

		// admin_company_logo_width
		$this->admin_company_logo_width->ViewValue = $this->admin_company_logo_width->CurrentValue;
		$this->admin_company_logo_width->ViewCustomAttributes = "";

		// admin_company_logo_height
		$this->admin_company_logo_height->ViewValue = $this->admin_company_logo_height->CurrentValue;
		$this->admin_company_logo_height->ViewCustomAttributes = "";

		// admin_company_logo_size
		$this->admin_company_logo_size->ViewValue = $this->admin_company_logo_size->CurrentValue;
		$this->admin_company_logo_size->ViewCustomAttributes = "";

		// admin_company_address
		$this->admin_company_address->ViewValue = $this->admin_company_address->CurrentValue;
		$this->admin_company_address->ViewCustomAttributes = "";

		// admin_company_region_id
		if (strval($this->admin_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->admin_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->CurrentValue;
			}
		} else {
			$this->admin_company_region_id->ViewValue = NULL;
		}
		$this->admin_company_region_id->ViewCustomAttributes = "";

		// admin_company_province_id
		if (strval($this->admin_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->admin_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->CurrentValue;
			}
		} else {
			$this->admin_company_province_id->ViewValue = NULL;
		}
		$this->admin_company_province_id->ViewCustomAttributes = "";

		// admin_company_city_id
		if (strval($this->admin_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->admin_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->CurrentValue;
			}
		} else {
			$this->admin_company_city_id->ViewValue = NULL;
		}
		$this->admin_company_city_id->ViewCustomAttributes = "";

		// admin_company_phone
		$this->admin_company_phone->ViewValue = $this->admin_company_phone->CurrentValue;
		$this->admin_company_phone->ViewCustomAttributes = "";

		// admin_company_email
		$this->admin_company_email->ViewValue = $this->admin_company_email->CurrentValue;
		$this->admin_company_email->ViewCustomAttributes = "";

		// admin_company_web
		$this->admin_company_web->ViewValue = $this->admin_company_web->CurrentValue;
		$this->admin_company_web->ViewCustomAttributes = "";

		// admin_contract
		$this->admin_contract->ViewValue = $this->admin_contract->CurrentValue;
		$this->admin_contract->ViewValue = ew_FormatDateTime($this->admin_contract->ViewValue, 7);
		$this->admin_contract->ViewCustomAttributes = "";

		// admin_expire
		$this->admin_expire->ViewValue = $this->admin_expire->CurrentValue;
		$this->admin_expire->ViewValue = ew_FormatDateTime($this->admin_expire->ViewValue, 7);
		$this->admin_expire->ViewCustomAttributes = "";

		// admin_company_facebook
		$this->admin_company_facebook->ViewValue = $this->admin_company_facebook->CurrentValue;
		$this->admin_company_facebook->ViewCustomAttributes = "";

		// admin_company_twitter
		$this->admin_company_twitter->ViewValue = $this->admin_company_twitter->CurrentValue;
		$this->admin_company_twitter->ViewCustomAttributes = "";

		// admin_company_googleplus
		$this->admin_company_googleplus->ViewValue = $this->admin_company_googleplus->CurrentValue;
		$this->admin_company_googleplus->ViewCustomAttributes = "";

		// admin_company_linkedin
		$this->admin_company_linkedin->ViewValue = $this->admin_company_linkedin->CurrentValue;
		$this->admin_company_linkedin->ViewCustomAttributes = "";

		// admin_link_playstore
		$this->admin_link_playstore->ViewValue = $this->admin_link_playstore->CurrentValue;
		$this->admin_link_playstore->ViewCustomAttributes = "";

		// admin_link_appstore
		$this->admin_link_appstore->ViewValue = $this->admin_link_appstore->CurrentValue;
		$this->admin_link_appstore->ViewCustomAttributes = "";

		// admin_link_windowsstore
		$this->admin_link_windowsstore->ViewValue = $this->admin_link_windowsstore->CurrentValue;
		$this->admin_link_windowsstore->ViewCustomAttributes = "";

		// admin_level
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->admin_level->CurrentValue) <> "") {
			$this->admin_level->ViewValue = $this->admin_level->OptionCaption($this->admin_level->CurrentValue);
		} else {
			$this->admin_level->ViewValue = NULL;
		}
		} else {
			$this->admin_level->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->admin_level->ViewCustomAttributes = "";

		// admin_username
		$this->admin_username->ViewValue = $this->admin_username->CurrentValue;
		$this->admin_username->ViewCustomAttributes = "";

		// admin_password
		$this->admin_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->admin_password->ViewCustomAttributes = "";

		// admin_company_autohority
		$this->admin_company_autohority->ViewValue = $this->admin_company_autohority->CurrentValue;
		$this->admin_company_autohority->ViewCustomAttributes = "";

		// admin_auth_token
		$this->admin_auth_token->ViewValue = $this->admin_auth_token->CurrentValue;
		$this->admin_auth_token->ViewCustomAttributes = "";

		// admin_last_update
		$this->admin_last_update->ViewValue = $this->admin_last_update->CurrentValue;
		$this->admin_last_update->ViewValue = ew_FormatDateTime($this->admin_last_update->ViewValue, 7);
		$this->admin_last_update->ViewCustomAttributes = "";

		// admin_id
		$this->admin_id->LinkCustomAttributes = "";
		$this->admin_id->HrefValue = "";
		$this->admin_id->TooltipValue = "";

		// admin_parent_id
		$this->admin_parent_id->LinkCustomAttributes = "";
		$this->admin_parent_id->HrefValue = "";
		$this->admin_parent_id->TooltipValue = "";

		// admin_name
		$this->admin_name->LinkCustomAttributes = "";
		$this->admin_name->HrefValue = "";
		$this->admin_name->TooltipValue = "";

		// admin_surname
		$this->admin_surname->LinkCustomAttributes = "";
		$this->admin_surname->HrefValue = "";
		$this->admin_surname->TooltipValue = "";

		// admin_gender
		$this->admin_gender->LinkCustomAttributes = "";
		$this->admin_gender->HrefValue = "";
		$this->admin_gender->TooltipValue = "";

		// admin_email
		$this->admin_email->LinkCustomAttributes = "";
		$this->admin_email->HrefValue = "";
		$this->admin_email->TooltipValue = "";

		// admin_mobile
		$this->admin_mobile->LinkCustomAttributes = "";
		$this->admin_mobile->HrefValue = "";
		$this->admin_mobile->TooltipValue = "";

		// admin_phone
		$this->admin_phone->LinkCustomAttributes = "";
		$this->admin_phone->HrefValue = "";
		$this->admin_phone->TooltipValue = "";

		// admin_modules
		$this->admin_modules->LinkCustomAttributes = "";
		$this->admin_modules->HrefValue = "";
		$this->admin_modules->TooltipValue = "";

		// admin_language
		$this->admin_language->LinkCustomAttributes = "";
		$this->admin_language->HrefValue = "";
		$this->admin_language->TooltipValue = "";

		// admin_is_active
		$this->admin_is_active->LinkCustomAttributes = "";
		$this->admin_is_active->HrefValue = "";
		$this->admin_is_active->TooltipValue = "";

		// admin_company_name
		$this->admin_company_name->LinkCustomAttributes = "";
		$this->admin_company_name->HrefValue = "";
		$this->admin_company_name->TooltipValue = "";

		// admin_company_vat
		$this->admin_company_vat->LinkCustomAttributes = "";
		$this->admin_company_vat->HrefValue = "";
		$this->admin_company_vat->TooltipValue = "";

		// admin_company_logo_file
		$this->admin_company_logo_file->LinkCustomAttributes = "";
		if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
			$this->admin_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->admin_company_logo_file, $this->admin_company_logo_file->Upload->DbValue); // Add prefix/suffix
			$this->admin_company_logo_file->LinkAttrs["target"] = ""; // Add target
			if ($this->Export <> "") $this->admin_company_logo_file->HrefValue = ew_ConvertFullUrl($this->admin_company_logo_file->HrefValue);
		} else {
			$this->admin_company_logo_file->HrefValue = "";
		}
		$this->admin_company_logo_file->HrefValue2 = $this->admin_company_logo_file->UploadPath . $this->admin_company_logo_file->Upload->DbValue;
		$this->admin_company_logo_file->TooltipValue = "";
		if ($this->admin_company_logo_file->UseColorbox) {
			$this->admin_company_logo_file->LinkAttrs["title"] = $Language->Phrase("ViewImageGallery");
			$this->admin_company_logo_file->LinkAttrs["data-rel"] = "telecare_admin_x_admin_company_logo_file";

			//$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox ewTooltip img-thumbnail";
			//$this->admin_company_logo_file->LinkAttrs["data-placement"] = "bottom";
			//$this->admin_company_logo_file->LinkAttrs["data-container"] = "body";

			$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox img-thumbnail";
		}

		// admin_company_logo_width
		$this->admin_company_logo_width->LinkCustomAttributes = "";
		$this->admin_company_logo_width->HrefValue = "";
		$this->admin_company_logo_width->TooltipValue = "";

		// admin_company_logo_height
		$this->admin_company_logo_height->LinkCustomAttributes = "";
		$this->admin_company_logo_height->HrefValue = "";
		$this->admin_company_logo_height->TooltipValue = "";

		// admin_company_logo_size
		$this->admin_company_logo_size->LinkCustomAttributes = "";
		$this->admin_company_logo_size->HrefValue = "";
		$this->admin_company_logo_size->TooltipValue = "";

		// admin_company_address
		$this->admin_company_address->LinkCustomAttributes = "";
		$this->admin_company_address->HrefValue = "";
		$this->admin_company_address->TooltipValue = "";

		// admin_company_region_id
		$this->admin_company_region_id->LinkCustomAttributes = "";
		$this->admin_company_region_id->HrefValue = "";
		$this->admin_company_region_id->TooltipValue = "";

		// admin_company_province_id
		$this->admin_company_province_id->LinkCustomAttributes = "";
		$this->admin_company_province_id->HrefValue = "";
		$this->admin_company_province_id->TooltipValue = "";

		// admin_company_city_id
		$this->admin_company_city_id->LinkCustomAttributes = "";
		$this->admin_company_city_id->HrefValue = "";
		$this->admin_company_city_id->TooltipValue = "";

		// admin_company_phone
		$this->admin_company_phone->LinkCustomAttributes = "";
		$this->admin_company_phone->HrefValue = "";
		$this->admin_company_phone->TooltipValue = "";

		// admin_company_email
		$this->admin_company_email->LinkCustomAttributes = "";
		$this->admin_company_email->HrefValue = "";
		$this->admin_company_email->TooltipValue = "";

		// admin_company_web
		$this->admin_company_web->LinkCustomAttributes = "";
		$this->admin_company_web->HrefValue = "";
		$this->admin_company_web->TooltipValue = "";

		// admin_contract
		$this->admin_contract->LinkCustomAttributes = "";
		$this->admin_contract->HrefValue = "";
		$this->admin_contract->TooltipValue = "";

		// admin_expire
		$this->admin_expire->LinkCustomAttributes = "";
		$this->admin_expire->HrefValue = "";
		$this->admin_expire->TooltipValue = "";

		// admin_company_facebook
		$this->admin_company_facebook->LinkCustomAttributes = "";
		$this->admin_company_facebook->HrefValue = "";
		$this->admin_company_facebook->TooltipValue = "";

		// admin_company_twitter
		$this->admin_company_twitter->LinkCustomAttributes = "";
		$this->admin_company_twitter->HrefValue = "";
		$this->admin_company_twitter->TooltipValue = "";

		// admin_company_googleplus
		$this->admin_company_googleplus->LinkCustomAttributes = "";
		$this->admin_company_googleplus->HrefValue = "";
		$this->admin_company_googleplus->TooltipValue = "";

		// admin_company_linkedin
		$this->admin_company_linkedin->LinkCustomAttributes = "";
		$this->admin_company_linkedin->HrefValue = "";
		$this->admin_company_linkedin->TooltipValue = "";

		// admin_link_playstore
		$this->admin_link_playstore->LinkCustomAttributes = "";
		$this->admin_link_playstore->HrefValue = "";
		$this->admin_link_playstore->TooltipValue = "";

		// admin_link_appstore
		$this->admin_link_appstore->LinkCustomAttributes = "";
		$this->admin_link_appstore->HrefValue = "";
		$this->admin_link_appstore->TooltipValue = "";

		// admin_link_windowsstore
		$this->admin_link_windowsstore->LinkCustomAttributes = "";
		$this->admin_link_windowsstore->HrefValue = "";
		$this->admin_link_windowsstore->TooltipValue = "";

		// admin_level
		$this->admin_level->LinkCustomAttributes = "";
		$this->admin_level->HrefValue = "";
		$this->admin_level->TooltipValue = "";

		// admin_username
		$this->admin_username->LinkCustomAttributes = "";
		$this->admin_username->HrefValue = "";
		$this->admin_username->TooltipValue = "";

		// admin_password
		$this->admin_password->LinkCustomAttributes = "";
		$this->admin_password->HrefValue = "";
		$this->admin_password->TooltipValue = "";

		// admin_company_autohority
		$this->admin_company_autohority->LinkCustomAttributes = "";
		$this->admin_company_autohority->HrefValue = "";
		$this->admin_company_autohority->TooltipValue = "";

		// admin_auth_token
		$this->admin_auth_token->LinkCustomAttributes = "";
		$this->admin_auth_token->HrefValue = "";
		$this->admin_auth_token->TooltipValue = "";

		// admin_last_update
		$this->admin_last_update->LinkCustomAttributes = "";
		$this->admin_last_update->HrefValue = "";
		$this->admin_last_update->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// admin_id
		$this->admin_id->EditAttrs["class"] = "form-control";
		$this->admin_id->EditCustomAttributes = "";
		$this->admin_id->EditValue = $this->admin_id->CurrentValue;
		$this->admin_id->ViewCustomAttributes = "";

		// admin_parent_id
		// admin_name

		$this->admin_name->EditAttrs["class"] = "form-control";
		$this->admin_name->EditCustomAttributes = "";
		$this->admin_name->EditValue = $this->admin_name->CurrentValue;
		$this->admin_name->PlaceHolder = ew_RemoveHtml($this->admin_name->FldCaption());

		// admin_surname
		$this->admin_surname->EditAttrs["class"] = "form-control";
		$this->admin_surname->EditCustomAttributes = "";
		$this->admin_surname->EditValue = $this->admin_surname->CurrentValue;
		$this->admin_surname->PlaceHolder = ew_RemoveHtml($this->admin_surname->FldCaption());

		// admin_gender
		$this->admin_gender->EditCustomAttributes = "";
		$this->admin_gender->EditValue = $this->admin_gender->Options(FALSE);

		// admin_email
		$this->admin_email->EditAttrs["class"] = "form-control";
		$this->admin_email->EditCustomAttributes = "";
		$this->admin_email->EditValue = $this->admin_email->CurrentValue;
		$this->admin_email->PlaceHolder = ew_RemoveHtml($this->admin_email->FldCaption());

		// admin_mobile
		$this->admin_mobile->EditAttrs["class"] = "form-control";
		$this->admin_mobile->EditCustomAttributes = "";
		$this->admin_mobile->EditValue = $this->admin_mobile->CurrentValue;
		$this->admin_mobile->PlaceHolder = ew_RemoveHtml($this->admin_mobile->FldCaption());

		// admin_phone
		$this->admin_phone->EditAttrs["class"] = "form-control";
		$this->admin_phone->EditCustomAttributes = "";
		$this->admin_phone->EditValue = $this->admin_phone->CurrentValue;
		$this->admin_phone->PlaceHolder = ew_RemoveHtml($this->admin_phone->FldCaption());

		// admin_modules
		$this->admin_modules->EditCustomAttributes = "";

		// admin_language
		$this->admin_language->EditCustomAttributes = "";
		$this->admin_language->EditValue = $this->admin_language->Options(FALSE);

		// admin_is_active
		$this->admin_is_active->EditCustomAttributes = "";
		$this->admin_is_active->EditValue = $this->admin_is_active->Options(FALSE);

		// admin_company_name
		$this->admin_company_name->EditAttrs["class"] = "form-control";
		$this->admin_company_name->EditCustomAttributes = "";
		$this->admin_company_name->EditValue = $this->admin_company_name->CurrentValue;
		$this->admin_company_name->PlaceHolder = ew_RemoveHtml($this->admin_company_name->FldCaption());

		// admin_company_vat
		$this->admin_company_vat->EditAttrs["class"] = "form-control";
		$this->admin_company_vat->EditCustomAttributes = "";
		$this->admin_company_vat->EditValue = $this->admin_company_vat->CurrentValue;
		$this->admin_company_vat->PlaceHolder = ew_RemoveHtml($this->admin_company_vat->FldCaption());

		// admin_company_logo_file
		$this->admin_company_logo_file->EditAttrs["class"] = "form-control";
		$this->admin_company_logo_file->EditCustomAttributes = "";
		if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
			$this->admin_company_logo_file->ImageWidth = 100;
			$this->admin_company_logo_file->ImageHeight = 0;
			$this->admin_company_logo_file->ImageAlt = $this->admin_company_logo_file->FldAlt();
			$this->admin_company_logo_file->EditValue = $this->admin_company_logo_file->Upload->DbValue;
		} else {
			$this->admin_company_logo_file->EditValue = "";
		}
		if (!ew_Empty($this->admin_company_logo_file->CurrentValue))
			$this->admin_company_logo_file->Upload->FileName = $this->admin_company_logo_file->CurrentValue;

		// admin_company_logo_width
		$this->admin_company_logo_width->EditAttrs["class"] = "form-control";
		$this->admin_company_logo_width->EditCustomAttributes = "";
		$this->admin_company_logo_width->EditValue = $this->admin_company_logo_width->CurrentValue;
		$this->admin_company_logo_width->PlaceHolder = ew_RemoveHtml($this->admin_company_logo_width->FldCaption());

		// admin_company_logo_height
		$this->admin_company_logo_height->EditAttrs["class"] = "form-control";
		$this->admin_company_logo_height->EditCustomAttributes = "";
		$this->admin_company_logo_height->EditValue = $this->admin_company_logo_height->CurrentValue;
		$this->admin_company_logo_height->PlaceHolder = ew_RemoveHtml($this->admin_company_logo_height->FldCaption());

		// admin_company_logo_size
		$this->admin_company_logo_size->EditAttrs["class"] = "form-control";
		$this->admin_company_logo_size->EditCustomAttributes = "";
		$this->admin_company_logo_size->EditValue = $this->admin_company_logo_size->CurrentValue;
		$this->admin_company_logo_size->PlaceHolder = ew_RemoveHtml($this->admin_company_logo_size->FldCaption());

		// admin_company_address
		$this->admin_company_address->EditAttrs["class"] = "form-control";
		$this->admin_company_address->EditCustomAttributes = "";
		$this->admin_company_address->EditValue = $this->admin_company_address->CurrentValue;
		$this->admin_company_address->PlaceHolder = ew_RemoveHtml($this->admin_company_address->FldCaption());

		// admin_company_region_id
		$this->admin_company_region_id->EditAttrs["class"] = "form-control";
		$this->admin_company_region_id->EditCustomAttributes = "";

		// admin_company_province_id
		$this->admin_company_province_id->EditAttrs["class"] = "form-control";
		$this->admin_company_province_id->EditCustomAttributes = "";

		// admin_company_city_id
		$this->admin_company_city_id->EditAttrs["class"] = "form-control";
		$this->admin_company_city_id->EditCustomAttributes = "";

		// admin_company_phone
		$this->admin_company_phone->EditAttrs["class"] = "form-control";
		$this->admin_company_phone->EditCustomAttributes = "";
		$this->admin_company_phone->EditValue = $this->admin_company_phone->CurrentValue;
		$this->admin_company_phone->PlaceHolder = ew_RemoveHtml($this->admin_company_phone->FldCaption());

		// admin_company_email
		$this->admin_company_email->EditAttrs["class"] = "form-control";
		$this->admin_company_email->EditCustomAttributes = "";
		$this->admin_company_email->EditValue = $this->admin_company_email->CurrentValue;
		$this->admin_company_email->PlaceHolder = ew_RemoveHtml($this->admin_company_email->FldCaption());

		// admin_company_web
		$this->admin_company_web->EditAttrs["class"] = "form-control";
		$this->admin_company_web->EditCustomAttributes = "";
		$this->admin_company_web->EditValue = $this->admin_company_web->CurrentValue;
		$this->admin_company_web->PlaceHolder = ew_RemoveHtml($this->admin_company_web->FldCaption());

		// admin_contract
		$this->admin_contract->EditAttrs["class"] = "form-control";
		$this->admin_contract->EditCustomAttributes = "";
		$this->admin_contract->EditValue = ew_FormatDateTime($this->admin_contract->CurrentValue, 7);
		$this->admin_contract->PlaceHolder = ew_RemoveHtml($this->admin_contract->FldCaption());

		// admin_expire
		$this->admin_expire->EditAttrs["class"] = "form-control";
		$this->admin_expire->EditCustomAttributes = "";
		$this->admin_expire->EditValue = ew_FormatDateTime($this->admin_expire->CurrentValue, 7);
		$this->admin_expire->PlaceHolder = ew_RemoveHtml($this->admin_expire->FldCaption());

		// admin_company_facebook
		$this->admin_company_facebook->EditAttrs["class"] = "form-control";
		$this->admin_company_facebook->EditCustomAttributes = "";
		$this->admin_company_facebook->EditValue = $this->admin_company_facebook->CurrentValue;
		$this->admin_company_facebook->PlaceHolder = ew_RemoveHtml($this->admin_company_facebook->FldCaption());

		// admin_company_twitter
		$this->admin_company_twitter->EditAttrs["class"] = "form-control";
		$this->admin_company_twitter->EditCustomAttributes = "";
		$this->admin_company_twitter->EditValue = $this->admin_company_twitter->CurrentValue;
		$this->admin_company_twitter->PlaceHolder = ew_RemoveHtml($this->admin_company_twitter->FldCaption());

		// admin_company_googleplus
		$this->admin_company_googleplus->EditAttrs["class"] = "form-control";
		$this->admin_company_googleplus->EditCustomAttributes = "";
		$this->admin_company_googleplus->EditValue = $this->admin_company_googleplus->CurrentValue;
		$this->admin_company_googleplus->PlaceHolder = ew_RemoveHtml($this->admin_company_googleplus->FldCaption());

		// admin_company_linkedin
		$this->admin_company_linkedin->EditAttrs["class"] = "form-control";
		$this->admin_company_linkedin->EditCustomAttributes = "";
		$this->admin_company_linkedin->EditValue = $this->admin_company_linkedin->CurrentValue;
		$this->admin_company_linkedin->PlaceHolder = ew_RemoveHtml($this->admin_company_linkedin->FldCaption());

		// admin_link_playstore
		$this->admin_link_playstore->EditAttrs["class"] = "form-control";
		$this->admin_link_playstore->EditCustomAttributes = "";
		$this->admin_link_playstore->EditValue = $this->admin_link_playstore->CurrentValue;
		$this->admin_link_playstore->PlaceHolder = ew_RemoveHtml($this->admin_link_playstore->FldCaption());

		// admin_link_appstore
		$this->admin_link_appstore->EditAttrs["class"] = "form-control";
		$this->admin_link_appstore->EditCustomAttributes = "";
		$this->admin_link_appstore->EditValue = $this->admin_link_appstore->CurrentValue;
		$this->admin_link_appstore->PlaceHolder = ew_RemoveHtml($this->admin_link_appstore->FldCaption());

		// admin_link_windowsstore
		$this->admin_link_windowsstore->EditAttrs["class"] = "form-control";
		$this->admin_link_windowsstore->EditCustomAttributes = "";
		$this->admin_link_windowsstore->EditValue = $this->admin_link_windowsstore->CurrentValue;
		$this->admin_link_windowsstore->PlaceHolder = ew_RemoveHtml($this->admin_link_windowsstore->FldCaption());

		// admin_level
		$this->admin_level->EditAttrs["class"] = "form-control";
		$this->admin_level->EditCustomAttributes = "";
		if (!$Security->CanAdmin()) { // System admin
			$this->admin_level->EditValue = $Language->Phrase("PasswordMask");
		} else {
		$this->admin_level->EditValue = $this->admin_level->Options(TRUE);
		}

		// admin_username
		$this->admin_username->EditAttrs["class"] = "form-control";
		$this->admin_username->EditCustomAttributes = "";
		$this->admin_username->EditValue = $this->admin_username->CurrentValue;
		$this->admin_username->PlaceHolder = ew_RemoveHtml($this->admin_username->FldCaption());

		// admin_password
		$this->admin_password->EditAttrs["class"] = "form-control ewPasswordStrength";
		$this->admin_password->EditCustomAttributes = "";
		$this->admin_password->EditValue = $this->admin_password->CurrentValue;
		$this->admin_password->PlaceHolder = ew_RemoveHtml($this->admin_password->FldCaption());

		// admin_company_autohority
		$this->admin_company_autohority->EditAttrs["class"] = "form-control";
		$this->admin_company_autohority->EditCustomAttributes = "";
		$this->admin_company_autohority->EditValue = $this->admin_company_autohority->CurrentValue;
		$this->admin_company_autohority->PlaceHolder = ew_RemoveHtml($this->admin_company_autohority->FldCaption());

		// admin_auth_token
		$this->admin_auth_token->EditAttrs["class"] = "form-control";
		$this->admin_auth_token->EditCustomAttributes = "";
		$this->admin_auth_token->EditValue = $this->admin_auth_token->CurrentValue;
		$this->admin_auth_token->PlaceHolder = ew_RemoveHtml($this->admin_auth_token->FldCaption());

		// admin_last_update
		// Call Row Rendered event

		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->admin_id->Exportable) $Doc->ExportCaption($this->admin_id);
					if ($this->admin_name->Exportable) $Doc->ExportCaption($this->admin_name);
					if ($this->admin_surname->Exportable) $Doc->ExportCaption($this->admin_surname);
					if ($this->admin_gender->Exportable) $Doc->ExportCaption($this->admin_gender);
					if ($this->admin_email->Exportable) $Doc->ExportCaption($this->admin_email);
					if ($this->admin_mobile->Exportable) $Doc->ExportCaption($this->admin_mobile);
					if ($this->admin_phone->Exportable) $Doc->ExportCaption($this->admin_phone);
					if ($this->admin_modules->Exportable) $Doc->ExportCaption($this->admin_modules);
					if ($this->admin_language->Exportable) $Doc->ExportCaption($this->admin_language);
					if ($this->admin_is_active->Exportable) $Doc->ExportCaption($this->admin_is_active);
					if ($this->admin_company_name->Exportable) $Doc->ExportCaption($this->admin_company_name);
					if ($this->admin_company_vat->Exportable) $Doc->ExportCaption($this->admin_company_vat);
					if ($this->admin_company_logo_file->Exportable) $Doc->ExportCaption($this->admin_company_logo_file);
					if ($this->admin_company_address->Exportable) $Doc->ExportCaption($this->admin_company_address);
					if ($this->admin_company_region_id->Exportable) $Doc->ExportCaption($this->admin_company_region_id);
					if ($this->admin_company_province_id->Exportable) $Doc->ExportCaption($this->admin_company_province_id);
					if ($this->admin_company_city_id->Exportable) $Doc->ExportCaption($this->admin_company_city_id);
					if ($this->admin_company_phone->Exportable) $Doc->ExportCaption($this->admin_company_phone);
					if ($this->admin_company_email->Exportable) $Doc->ExportCaption($this->admin_company_email);
					if ($this->admin_company_web->Exportable) $Doc->ExportCaption($this->admin_company_web);
					if ($this->admin_contract->Exportable) $Doc->ExportCaption($this->admin_contract);
					if ($this->admin_expire->Exportable) $Doc->ExportCaption($this->admin_expire);
					if ($this->admin_company_facebook->Exportable) $Doc->ExportCaption($this->admin_company_facebook);
					if ($this->admin_company_twitter->Exportable) $Doc->ExportCaption($this->admin_company_twitter);
					if ($this->admin_company_googleplus->Exportable) $Doc->ExportCaption($this->admin_company_googleplus);
					if ($this->admin_company_linkedin->Exportable) $Doc->ExportCaption($this->admin_company_linkedin);
					if ($this->admin_link_playstore->Exportable) $Doc->ExportCaption($this->admin_link_playstore);
					if ($this->admin_link_appstore->Exportable) $Doc->ExportCaption($this->admin_link_appstore);
					if ($this->admin_link_windowsstore->Exportable) $Doc->ExportCaption($this->admin_link_windowsstore);
					if ($this->admin_level->Exportable) $Doc->ExportCaption($this->admin_level);
					if ($this->admin_username->Exportable) $Doc->ExportCaption($this->admin_username);
					if ($this->admin_password->Exportable) $Doc->ExportCaption($this->admin_password);
					if ($this->admin_last_update->Exportable) $Doc->ExportCaption($this->admin_last_update);
				} else {
					if ($this->admin_id->Exportable) $Doc->ExportCaption($this->admin_id);
					if ($this->admin_name->Exportable) $Doc->ExportCaption($this->admin_name);
					if ($this->admin_surname->Exportable) $Doc->ExportCaption($this->admin_surname);
					if ($this->admin_gender->Exportable) $Doc->ExportCaption($this->admin_gender);
					if ($this->admin_email->Exportable) $Doc->ExportCaption($this->admin_email);
					if ($this->admin_mobile->Exportable) $Doc->ExportCaption($this->admin_mobile);
					if ($this->admin_phone->Exportable) $Doc->ExportCaption($this->admin_phone);
					if ($this->admin_modules->Exportable) $Doc->ExportCaption($this->admin_modules);
					if ($this->admin_language->Exportable) $Doc->ExportCaption($this->admin_language);
					if ($this->admin_is_active->Exportable) $Doc->ExportCaption($this->admin_is_active);
					if ($this->admin_company_name->Exportable) $Doc->ExportCaption($this->admin_company_name);
					if ($this->admin_company_vat->Exportable) $Doc->ExportCaption($this->admin_company_vat);
					if ($this->admin_company_logo_file->Exportable) $Doc->ExportCaption($this->admin_company_logo_file);
					if ($this->admin_company_address->Exportable) $Doc->ExportCaption($this->admin_company_address);
					if ($this->admin_company_region_id->Exportable) $Doc->ExportCaption($this->admin_company_region_id);
					if ($this->admin_company_province_id->Exportable) $Doc->ExportCaption($this->admin_company_province_id);
					if ($this->admin_company_city_id->Exportable) $Doc->ExportCaption($this->admin_company_city_id);
					if ($this->admin_company_phone->Exportable) $Doc->ExportCaption($this->admin_company_phone);
					if ($this->admin_company_email->Exportable) $Doc->ExportCaption($this->admin_company_email);
					if ($this->admin_company_web->Exportable) $Doc->ExportCaption($this->admin_company_web);
					if ($this->admin_company_facebook->Exportable) $Doc->ExportCaption($this->admin_company_facebook);
					if ($this->admin_company_twitter->Exportable) $Doc->ExportCaption($this->admin_company_twitter);
					if ($this->admin_company_googleplus->Exportable) $Doc->ExportCaption($this->admin_company_googleplus);
					if ($this->admin_company_linkedin->Exportable) $Doc->ExportCaption($this->admin_company_linkedin);
					if ($this->admin_link_playstore->Exportable) $Doc->ExportCaption($this->admin_link_playstore);
					if ($this->admin_link_appstore->Exportable) $Doc->ExportCaption($this->admin_link_appstore);
					if ($this->admin_link_windowsstore->Exportable) $Doc->ExportCaption($this->admin_link_windowsstore);
					if ($this->admin_level->Exportable) $Doc->ExportCaption($this->admin_level);
					if ($this->admin_username->Exportable) $Doc->ExportCaption($this->admin_username);
					if ($this->admin_password->Exportable) $Doc->ExportCaption($this->admin_password);
					if ($this->admin_last_update->Exportable) $Doc->ExportCaption($this->admin_last_update);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->admin_id->Exportable) $Doc->ExportField($this->admin_id);
						if ($this->admin_name->Exportable) $Doc->ExportField($this->admin_name);
						if ($this->admin_surname->Exportable) $Doc->ExportField($this->admin_surname);
						if ($this->admin_gender->Exportable) $Doc->ExportField($this->admin_gender);
						if ($this->admin_email->Exportable) $Doc->ExportField($this->admin_email);
						if ($this->admin_mobile->Exportable) $Doc->ExportField($this->admin_mobile);
						if ($this->admin_phone->Exportable) $Doc->ExportField($this->admin_phone);
						if ($this->admin_modules->Exportable) $Doc->ExportField($this->admin_modules);
						if ($this->admin_language->Exportable) $Doc->ExportField($this->admin_language);
						if ($this->admin_is_active->Exportable) $Doc->ExportField($this->admin_is_active);
						if ($this->admin_company_name->Exportable) $Doc->ExportField($this->admin_company_name);
						if ($this->admin_company_vat->Exportable) $Doc->ExportField($this->admin_company_vat);
						if ($this->admin_company_logo_file->Exportable) $Doc->ExportField($this->admin_company_logo_file);
						if ($this->admin_company_address->Exportable) $Doc->ExportField($this->admin_company_address);
						if ($this->admin_company_region_id->Exportable) $Doc->ExportField($this->admin_company_region_id);
						if ($this->admin_company_province_id->Exportable) $Doc->ExportField($this->admin_company_province_id);
						if ($this->admin_company_city_id->Exportable) $Doc->ExportField($this->admin_company_city_id);
						if ($this->admin_company_phone->Exportable) $Doc->ExportField($this->admin_company_phone);
						if ($this->admin_company_email->Exportable) $Doc->ExportField($this->admin_company_email);
						if ($this->admin_company_web->Exportable) $Doc->ExportField($this->admin_company_web);
						if ($this->admin_contract->Exportable) $Doc->ExportField($this->admin_contract);
						if ($this->admin_expire->Exportable) $Doc->ExportField($this->admin_expire);
						if ($this->admin_company_facebook->Exportable) $Doc->ExportField($this->admin_company_facebook);
						if ($this->admin_company_twitter->Exportable) $Doc->ExportField($this->admin_company_twitter);
						if ($this->admin_company_googleplus->Exportable) $Doc->ExportField($this->admin_company_googleplus);
						if ($this->admin_company_linkedin->Exportable) $Doc->ExportField($this->admin_company_linkedin);
						if ($this->admin_link_playstore->Exportable) $Doc->ExportField($this->admin_link_playstore);
						if ($this->admin_link_appstore->Exportable) $Doc->ExportField($this->admin_link_appstore);
						if ($this->admin_link_windowsstore->Exportable) $Doc->ExportField($this->admin_link_windowsstore);
						if ($this->admin_level->Exportable) $Doc->ExportField($this->admin_level);
						if ($this->admin_username->Exportable) $Doc->ExportField($this->admin_username);
						if ($this->admin_password->Exportable) $Doc->ExportField($this->admin_password);
						if ($this->admin_last_update->Exportable) $Doc->ExportField($this->admin_last_update);
					} else {
						if ($this->admin_id->Exportable) $Doc->ExportField($this->admin_id);
						if ($this->admin_name->Exportable) $Doc->ExportField($this->admin_name);
						if ($this->admin_surname->Exportable) $Doc->ExportField($this->admin_surname);
						if ($this->admin_gender->Exportable) $Doc->ExportField($this->admin_gender);
						if ($this->admin_email->Exportable) $Doc->ExportField($this->admin_email);
						if ($this->admin_mobile->Exportable) $Doc->ExportField($this->admin_mobile);
						if ($this->admin_phone->Exportable) $Doc->ExportField($this->admin_phone);
						if ($this->admin_modules->Exportable) $Doc->ExportField($this->admin_modules);
						if ($this->admin_language->Exportable) $Doc->ExportField($this->admin_language);
						if ($this->admin_is_active->Exportable) $Doc->ExportField($this->admin_is_active);
						if ($this->admin_company_name->Exportable) $Doc->ExportField($this->admin_company_name);
						if ($this->admin_company_vat->Exportable) $Doc->ExportField($this->admin_company_vat);
						if ($this->admin_company_logo_file->Exportable) $Doc->ExportField($this->admin_company_logo_file);
						if ($this->admin_company_address->Exportable) $Doc->ExportField($this->admin_company_address);
						if ($this->admin_company_region_id->Exportable) $Doc->ExportField($this->admin_company_region_id);
						if ($this->admin_company_province_id->Exportable) $Doc->ExportField($this->admin_company_province_id);
						if ($this->admin_company_city_id->Exportable) $Doc->ExportField($this->admin_company_city_id);
						if ($this->admin_company_phone->Exportable) $Doc->ExportField($this->admin_company_phone);
						if ($this->admin_company_email->Exportable) $Doc->ExportField($this->admin_company_email);
						if ($this->admin_company_web->Exportable) $Doc->ExportField($this->admin_company_web);
						if ($this->admin_company_facebook->Exportable) $Doc->ExportField($this->admin_company_facebook);
						if ($this->admin_company_twitter->Exportable) $Doc->ExportField($this->admin_company_twitter);
						if ($this->admin_company_googleplus->Exportable) $Doc->ExportField($this->admin_company_googleplus);
						if ($this->admin_company_linkedin->Exportable) $Doc->ExportField($this->admin_company_linkedin);
						if ($this->admin_link_playstore->Exportable) $Doc->ExportField($this->admin_link_playstore);
						if ($this->admin_link_appstore->Exportable) $Doc->ExportField($this->admin_link_appstore);
						if ($this->admin_link_windowsstore->Exportable) $Doc->ExportField($this->admin_link_windowsstore);
						if ($this->admin_level->Exportable) $Doc->ExportField($this->admin_level);
						if ($this->admin_username->Exportable) $Doc->ExportField($this->admin_username);
						if ($this->admin_password->Exportable) $Doc->ExportField($this->admin_password);
						if ($this->admin_last_update->Exportable) $Doc->ExportField($this->admin_last_update);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// User ID filter
	function UserIDFilter($userid) {
		$sUserIDFilter = '`admin_id` = ' . ew_QuotedValue($userid, EW_DATATYPE_NUMBER, EW_USER_TABLE_DBID);
		$sParentUserIDFilter = '`admin_id` IN (SELECT `admin_id` FROM ' . "`telecare_admin`" . ' WHERE `admin_parent_id` = ' . ew_QuotedValue($userid, EW_DATATYPE_NUMBER, EW_USER_TABLE_DBID) . ')';
		$sUserIDFilter = "($sUserIDFilter) OR ($sParentUserIDFilter)";
		return $sUserIDFilter;
	}

	// Add User ID filter
	function AddUserIDFilter($sFilter) {
		global $Security;
		$sFilterWrk = "";
		$id = (CurrentPageID() == "list") ? $this->CurrentAction : CurrentPageID();
		if (!$this->UserIDAllow($id) && !$Security->IsAdmin()) {
			$sFilterWrk = $Security->UserIDList();
			if ($sFilterWrk <> "")
				$sFilterWrk = '`admin_id` IN (' . $sFilterWrk . ')';
		}

		// Call User ID Filtering event
		$this->UserID_Filtering($sFilterWrk);
		ew_AddFilter($sFilter, $sFilterWrk);
		return $sFilter;
	}

	// Add Parent User ID filter
	function AddParentUserIDFilter($sFilter, $userid) {
		global $Security;
		if (!$Security->IsAdmin()) {
			$result = $Security->ParentUserIDList($userid);
			if ($result <> "")
				$result = '`admin_id` IN (' . $result . ')';
			ew_AddFilter($result, $sFilter);
			return $result;
		} else {
			return $sFilter;
		}
	}

	// User ID subquery
	function GetUserIDSubquery(&$fld, &$masterfld) {
		global $UserTableConn;
		$sWrk = "";
		$sSql = "SELECT " . $masterfld->FldExpression . " FROM `telecare_admin`";
		$sFilter = $this->AddUserIDFilter("");
		if ($sFilter <> "") $sSql .= " WHERE " . $sFilter;

		// Use subquery
		if (EW_USE_SUBQUERY_FOR_MASTER_USER_ID) {
			$sWrk = $sSql;
		} else {

			// List all values
			if ($rs = $UserTableConn->Execute($sSql)) {
				while (!$rs->EOF) {
					if ($sWrk <> "") $sWrk .= ",";
					$sWrk .= ew_QuotedValue($rs->fields[0], $masterfld->FldDataType, EW_USER_TABLE_DBID);
					$rs->MoveNext();
				}
				$rs->Close();
			}
		}
		if ($sWrk <> "") {
			$sWrk = $fld->FldExpression . " IN (" . $sWrk . ")";
		}
		return $sWrk;
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
