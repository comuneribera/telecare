<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_druginfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_drug_delete = NULL; // Initialize page object first

class ctelecare_drug_delete extends ctelecare_drug {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_drug';

	// Page object name
	var $PageObjName = 'telecare_drug_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_drug)
		if (!isset($GLOBALS["telecare_drug"]) || get_class($GLOBALS["telecare_drug"]) == "ctelecare_drug") {
			$GLOBALS["telecare_drug"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_drug"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_drug', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_druglist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->drug_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_drug;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_drug);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("telecare_druglist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in telecare_drug class, telecare_druginfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->drug_id->setDbValue($rs->fields('drug_id'));
		$this->drug_active_ingredient->setDbValue($rs->fields('drug_active_ingredient'));
		$this->drug_package->setDbValue($rs->fields('drug_package'));
		$this->drug_ATC->setDbValue($rs->fields('drug_ATC'));
		$this->drug_AIC->setDbValue($rs->fields('drug_AIC'));
		$this->drug_name->setDbValue($rs->fields('drug_name'));
		$this->drug_company->setDbValue($rs->fields('drug_company'));
		$this->drug_price_b2b->setDbValue($rs->fields('drug_price_b2b'));
		$this->drug_price_b2c->setDbValue($rs->fields('drug_price_b2c'));
		$this->drug_note->setDbValue($rs->fields('drug_note'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->drug_id->DbValue = $row['drug_id'];
		$this->drug_active_ingredient->DbValue = $row['drug_active_ingredient'];
		$this->drug_package->DbValue = $row['drug_package'];
		$this->drug_ATC->DbValue = $row['drug_ATC'];
		$this->drug_AIC->DbValue = $row['drug_AIC'];
		$this->drug_name->DbValue = $row['drug_name'];
		$this->drug_company->DbValue = $row['drug_company'];
		$this->drug_price_b2b->DbValue = $row['drug_price_b2b'];
		$this->drug_price_b2c->DbValue = $row['drug_price_b2c'];
		$this->drug_note->DbValue = $row['drug_note'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// drug_id
		// drug_active_ingredient
		// drug_package
		// drug_ATC
		// drug_AIC
		// drug_name
		// drug_company
		// drug_price_b2b
		// drug_price_b2c
		// drug_note

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// drug_id
		$this->drug_id->ViewValue = $this->drug_id->CurrentValue;
		$this->drug_id->ViewCustomAttributes = "";

		// drug_active_ingredient
		$this->drug_active_ingredient->ViewValue = $this->drug_active_ingredient->CurrentValue;
		$this->drug_active_ingredient->ViewCustomAttributes = "";

		// drug_package
		$this->drug_package->ViewValue = $this->drug_package->CurrentValue;
		$this->drug_package->ViewCustomAttributes = "";

		// drug_ATC
		$this->drug_ATC->ViewValue = $this->drug_ATC->CurrentValue;
		$this->drug_ATC->ViewCustomAttributes = "";

		// drug_AIC
		$this->drug_AIC->ViewValue = $this->drug_AIC->CurrentValue;
		$this->drug_AIC->ViewCustomAttributes = "";

		// drug_name
		$this->drug_name->ViewValue = $this->drug_name->CurrentValue;
		$this->drug_name->ViewCustomAttributes = "";

		// drug_company
		$this->drug_company->ViewValue = $this->drug_company->CurrentValue;
		$this->drug_company->ViewCustomAttributes = "";

		// drug_price_b2b
		$this->drug_price_b2b->ViewValue = $this->drug_price_b2b->CurrentValue;
		$this->drug_price_b2b->ViewCustomAttributes = "";

		// drug_price_b2c
		$this->drug_price_b2c->ViewValue = $this->drug_price_b2c->CurrentValue;
		$this->drug_price_b2c->ViewCustomAttributes = "";

		// drug_note
		$this->drug_note->ViewValue = $this->drug_note->CurrentValue;
		$this->drug_note->ViewCustomAttributes = "";

			// drug_id
			$this->drug_id->LinkCustomAttributes = "";
			$this->drug_id->HrefValue = "";
			$this->drug_id->TooltipValue = "";

			// drug_active_ingredient
			$this->drug_active_ingredient->LinkCustomAttributes = "";
			$this->drug_active_ingredient->HrefValue = "";
			$this->drug_active_ingredient->TooltipValue = "";

			// drug_package
			$this->drug_package->LinkCustomAttributes = "";
			$this->drug_package->HrefValue = "";
			$this->drug_package->TooltipValue = "";

			// drug_ATC
			$this->drug_ATC->LinkCustomAttributes = "";
			$this->drug_ATC->HrefValue = "";
			$this->drug_ATC->TooltipValue = "";

			// drug_AIC
			$this->drug_AIC->LinkCustomAttributes = "";
			$this->drug_AIC->HrefValue = "";
			$this->drug_AIC->TooltipValue = "";

			// drug_name
			$this->drug_name->LinkCustomAttributes = "";
			$this->drug_name->HrefValue = "";
			$this->drug_name->TooltipValue = "";

			// drug_company
			$this->drug_company->LinkCustomAttributes = "";
			$this->drug_company->HrefValue = "";
			$this->drug_company->TooltipValue = "";

			// drug_price_b2b
			$this->drug_price_b2b->LinkCustomAttributes = "";
			$this->drug_price_b2b->HrefValue = "";
			$this->drug_price_b2b->TooltipValue = "";

			// drug_price_b2c
			$this->drug_price_b2c->LinkCustomAttributes = "";
			$this->drug_price_b2c->HrefValue = "";
			$this->drug_price_b2c->TooltipValue = "";

			// drug_note
			$this->drug_note->LinkCustomAttributes = "";
			$this->drug_note->HrefValue = "";
			$this->drug_note->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['drug_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_druglist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_drug_delete)) $telecare_drug_delete = new ctelecare_drug_delete();

// Page init
$telecare_drug_delete->Page_Init();

// Page main
$telecare_drug_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_drug_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftelecare_drugdelete = new ew_Form("ftelecare_drugdelete", "delete");

// Form_CustomValidate event
ftelecare_drugdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_drugdelete.ValidateRequired = true;
<?php } else { ?>
ftelecare_drugdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($telecare_drug_delete->Recordset = $telecare_drug_delete->LoadRecordset())
	$telecare_drug_deleteTotalRecs = $telecare_drug_delete->Recordset->RecordCount(); // Get record count
if ($telecare_drug_deleteTotalRecs <= 0) { // No record found, exit
	if ($telecare_drug_delete->Recordset)
		$telecare_drug_delete->Recordset->Close();
	$telecare_drug_delete->Page_Terminate("telecare_druglist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_drug_delete->ShowPageHeader(); ?>
<?php
$telecare_drug_delete->ShowMessage();
?>
<form name="ftelecare_drugdelete" id="ftelecare_drugdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_drug_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_drug_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_drug">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($telecare_drug_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $telecare_drug->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($telecare_drug->drug_id->Visible) { // drug_id ?>
		<th><span id="elh_telecare_drug_drug_id" class="telecare_drug_drug_id"><?php echo $telecare_drug->drug_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_active_ingredient->Visible) { // drug_active_ingredient ?>
		<th><span id="elh_telecare_drug_drug_active_ingredient" class="telecare_drug_drug_active_ingredient"><?php echo $telecare_drug->drug_active_ingredient->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_package->Visible) { // drug_package ?>
		<th><span id="elh_telecare_drug_drug_package" class="telecare_drug_drug_package"><?php echo $telecare_drug->drug_package->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_ATC->Visible) { // drug_ATC ?>
		<th><span id="elh_telecare_drug_drug_ATC" class="telecare_drug_drug_ATC"><?php echo $telecare_drug->drug_ATC->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_AIC->Visible) { // drug_AIC ?>
		<th><span id="elh_telecare_drug_drug_AIC" class="telecare_drug_drug_AIC"><?php echo $telecare_drug->drug_AIC->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_name->Visible) { // drug_name ?>
		<th><span id="elh_telecare_drug_drug_name" class="telecare_drug_drug_name"><?php echo $telecare_drug->drug_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_company->Visible) { // drug_company ?>
		<th><span id="elh_telecare_drug_drug_company" class="telecare_drug_drug_company"><?php echo $telecare_drug->drug_company->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_price_b2b->Visible) { // drug_price_b2b ?>
		<th><span id="elh_telecare_drug_drug_price_b2b" class="telecare_drug_drug_price_b2b"><?php echo $telecare_drug->drug_price_b2b->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_price_b2c->Visible) { // drug_price_b2c ?>
		<th><span id="elh_telecare_drug_drug_price_b2c" class="telecare_drug_drug_price_b2c"><?php echo $telecare_drug->drug_price_b2c->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_drug->drug_note->Visible) { // drug_note ?>
		<th><span id="elh_telecare_drug_drug_note" class="telecare_drug_drug_note"><?php echo $telecare_drug->drug_note->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$telecare_drug_delete->RecCnt = 0;
$i = 0;
while (!$telecare_drug_delete->Recordset->EOF) {
	$telecare_drug_delete->RecCnt++;
	$telecare_drug_delete->RowCnt++;

	// Set row properties
	$telecare_drug->ResetAttrs();
	$telecare_drug->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$telecare_drug_delete->LoadRowValues($telecare_drug_delete->Recordset);

	// Render row
	$telecare_drug_delete->RenderRow();
?>
	<tr<?php echo $telecare_drug->RowAttributes() ?>>
<?php if ($telecare_drug->drug_id->Visible) { // drug_id ?>
		<td<?php echo $telecare_drug->drug_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_id" class="telecare_drug_drug_id">
<span<?php echo $telecare_drug->drug_id->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_active_ingredient->Visible) { // drug_active_ingredient ?>
		<td<?php echo $telecare_drug->drug_active_ingredient->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_active_ingredient" class="telecare_drug_drug_active_ingredient">
<span<?php echo $telecare_drug->drug_active_ingredient->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_active_ingredient->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_package->Visible) { // drug_package ?>
		<td<?php echo $telecare_drug->drug_package->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_package" class="telecare_drug_drug_package">
<span<?php echo $telecare_drug->drug_package->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_package->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_ATC->Visible) { // drug_ATC ?>
		<td<?php echo $telecare_drug->drug_ATC->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_ATC" class="telecare_drug_drug_ATC">
<span<?php echo $telecare_drug->drug_ATC->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_ATC->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_AIC->Visible) { // drug_AIC ?>
		<td<?php echo $telecare_drug->drug_AIC->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_AIC" class="telecare_drug_drug_AIC">
<span<?php echo $telecare_drug->drug_AIC->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_AIC->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_name->Visible) { // drug_name ?>
		<td<?php echo $telecare_drug->drug_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_name" class="telecare_drug_drug_name">
<span<?php echo $telecare_drug->drug_name->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_company->Visible) { // drug_company ?>
		<td<?php echo $telecare_drug->drug_company->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_company" class="telecare_drug_drug_company">
<span<?php echo $telecare_drug->drug_company->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_company->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_price_b2b->Visible) { // drug_price_b2b ?>
		<td<?php echo $telecare_drug->drug_price_b2b->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_price_b2b" class="telecare_drug_drug_price_b2b">
<span<?php echo $telecare_drug->drug_price_b2b->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_price_b2b->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_price_b2c->Visible) { // drug_price_b2c ?>
		<td<?php echo $telecare_drug->drug_price_b2c->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_price_b2c" class="telecare_drug_drug_price_b2c">
<span<?php echo $telecare_drug->drug_price_b2c->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_price_b2c->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_drug->drug_note->Visible) { // drug_note ?>
		<td<?php echo $telecare_drug->drug_note->CellAttributes() ?>>
<span id="el<?php echo $telecare_drug_delete->RowCnt ?>_telecare_drug_drug_note" class="telecare_drug_drug_note">
<span<?php echo $telecare_drug->drug_note->ViewAttributes() ?>>
<?php echo $telecare_drug->drug_note->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$telecare_drug_delete->Recordset->MoveNext();
}
$telecare_drug_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_drug_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftelecare_drugdelete.Init();
</script>
<?php
$telecare_drug_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_drug_delete->Page_Terminate();
?>
