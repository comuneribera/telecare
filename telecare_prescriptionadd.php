<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_prescriptioninfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_apotechinfo.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "telecare_prescription_druggridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_prescription_add = NULL; // Initialize page object first

class ctelecare_prescription_add extends ctelecare_prescription {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_prescription';

	// Page object name
	var $PageObjName = 'telecare_prescription_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_prescription)
		if (!isset($GLOBALS["telecare_prescription"]) || get_class($GLOBALS["telecare_prescription"]) == "ctelecare_prescription") {
			$GLOBALS["telecare_prescription"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_prescription"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_apotech)
		if (!isset($GLOBALS['telecare_apotech'])) $GLOBALS['telecare_apotech'] = new ctelecare_apotech();

		// Table object (telecare_user)
		if (!isset($GLOBALS['telecare_user'])) $GLOBALS['telecare_user'] = new ctelecare_user();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_prescription', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_prescriptionlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'telecare_prescription_drug'
			if (@$_POST["grid"] == "ftelecare_prescription_druggrid") {
				if (!isset($GLOBALS["telecare_prescription_drug_grid"])) $GLOBALS["telecare_prescription_drug_grid"] = new ctelecare_prescription_drug_grid;
				$GLOBALS["telecare_prescription_drug_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_prescription;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_prescription);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["prescription_id"] != "") {
				$this->prescription_id->setQueryStringValue($_GET["prescription_id"]);
				$this->setKey("prescription_id", $this->prescription_id->CurrentValue); // Set up key
			} else {
				$this->setKey("prescription_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Set up detail parameters
		$this->SetUpDetailParms();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_prescriptionlist.php"); // No matching record, return to list
				}

				// Set up detail parameters
				$this->SetUpDetailParms();
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					if ($this->getCurrentDetailTable() <> "") // Master/detail add
						$sReturnUrl = $this->GetDetailUrl();
					else
						$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_prescriptionview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values

					// Set up detail parameters
					$this->SetUpDetailParms();
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->prescription_user_id->CurrentValue = NULL;
		$this->prescription_user_id->OldValue = $this->prescription_user_id->CurrentValue;
		$this->prescription_drugstore_id->CurrentValue = NULL;
		$this->prescription_drugstore_id->OldValue = $this->prescription_drugstore_id->CurrentValue;
		$this->prescription_doctor_id->CurrentValue = NULL;
		$this->prescription_doctor_id->OldValue = $this->prescription_doctor_id->CurrentValue;
		$this->prescription_admin_id->CurrentValue = NULL;
		$this->prescription_admin_id->OldValue = $this->prescription_admin_id->CurrentValue;
		$this->prescription_request_date->CurrentValue = NULL;
		$this->prescription_request_date->OldValue = $this->prescription_request_date->CurrentValue;
		$this->prescription_start_on->CurrentValue = NULL;
		$this->prescription_start_on->OldValue = $this->prescription_start_on->CurrentValue;
		$this->prescription_close_on->CurrentValue = NULL;
		$this->prescription_close_on->OldValue = $this->prescription_close_on->CurrentValue;
		$this->prescription_close->CurrentValue = 1;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->prescription_user_id->FldIsDetailKey) {
			$this->prescription_user_id->setFormValue($objForm->GetValue("x_prescription_user_id"));
		}
		if (!$this->prescription_drugstore_id->FldIsDetailKey) {
			$this->prescription_drugstore_id->setFormValue($objForm->GetValue("x_prescription_drugstore_id"));
		}
		if (!$this->prescription_doctor_id->FldIsDetailKey) {
			$this->prescription_doctor_id->setFormValue($objForm->GetValue("x_prescription_doctor_id"));
		}
		if (!$this->prescription_admin_id->FldIsDetailKey) {
			$this->prescription_admin_id->setFormValue($objForm->GetValue("x_prescription_admin_id"));
		}
		if (!$this->prescription_request_date->FldIsDetailKey) {
			$this->prescription_request_date->setFormValue($objForm->GetValue("x_prescription_request_date"));
			$this->prescription_request_date->CurrentValue = ew_UnFormatDateTime($this->prescription_request_date->CurrentValue, 7);
		}
		if (!$this->prescription_start_on->FldIsDetailKey) {
			$this->prescription_start_on->setFormValue($objForm->GetValue("x_prescription_start_on"));
			$this->prescription_start_on->CurrentValue = ew_UnFormatDateTime($this->prescription_start_on->CurrentValue, 7);
		}
		if (!$this->prescription_close_on->FldIsDetailKey) {
			$this->prescription_close_on->setFormValue($objForm->GetValue("x_prescription_close_on"));
			$this->prescription_close_on->CurrentValue = ew_UnFormatDateTime($this->prescription_close_on->CurrentValue, 7);
		}
		if (!$this->prescription_close->FldIsDetailKey) {
			$this->prescription_close->setFormValue($objForm->GetValue("x_prescription_close"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->prescription_user_id->CurrentValue = $this->prescription_user_id->FormValue;
		$this->prescription_drugstore_id->CurrentValue = $this->prescription_drugstore_id->FormValue;
		$this->prescription_doctor_id->CurrentValue = $this->prescription_doctor_id->FormValue;
		$this->prescription_admin_id->CurrentValue = $this->prescription_admin_id->FormValue;
		$this->prescription_request_date->CurrentValue = $this->prescription_request_date->FormValue;
		$this->prescription_request_date->CurrentValue = ew_UnFormatDateTime($this->prescription_request_date->CurrentValue, 7);
		$this->prescription_start_on->CurrentValue = $this->prescription_start_on->FormValue;
		$this->prescription_start_on->CurrentValue = ew_UnFormatDateTime($this->prescription_start_on->CurrentValue, 7);
		$this->prescription_close_on->CurrentValue = $this->prescription_close_on->FormValue;
		$this->prescription_close_on->CurrentValue = ew_UnFormatDateTime($this->prescription_close_on->CurrentValue, 7);
		$this->prescription_close->CurrentValue = $this->prescription_close->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->prescription_id->setDbValue($rs->fields('prescription_id'));
		$this->prescription_user_id->setDbValue($rs->fields('prescription_user_id'));
		$this->prescription_drugstore_id->setDbValue($rs->fields('prescription_drugstore_id'));
		$this->prescription_doctor_id->setDbValue($rs->fields('prescription_doctor_id'));
		$this->prescription_admin_id->setDbValue($rs->fields('prescription_admin_id'));
		$this->prescription_request_date->setDbValue($rs->fields('prescription_request_date'));
		$this->prescription_start_on->setDbValue($rs->fields('prescription_start_on'));
		$this->prescription_close_on->setDbValue($rs->fields('prescription_close_on'));
		$this->prescription_close->setDbValue($rs->fields('prescription_close'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->prescription_id->DbValue = $row['prescription_id'];
		$this->prescription_user_id->DbValue = $row['prescription_user_id'];
		$this->prescription_drugstore_id->DbValue = $row['prescription_drugstore_id'];
		$this->prescription_doctor_id->DbValue = $row['prescription_doctor_id'];
		$this->prescription_admin_id->DbValue = $row['prescription_admin_id'];
		$this->prescription_request_date->DbValue = $row['prescription_request_date'];
		$this->prescription_start_on->DbValue = $row['prescription_start_on'];
		$this->prescription_close_on->DbValue = $row['prescription_close_on'];
		$this->prescription_close->DbValue = $row['prescription_close'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("prescription_id")) <> "")
			$this->prescription_id->CurrentValue = $this->getKey("prescription_id"); // prescription_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// prescription_id
		// prescription_user_id
		// prescription_drugstore_id
		// prescription_doctor_id
		// prescription_admin_id
		// prescription_request_date
		// prescription_start_on
		// prescription_close_on
		// prescription_close

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// prescription_id
		$this->prescription_id->ViewValue = $this->prescription_id->CurrentValue;
		$this->prescription_id->ViewCustomAttributes = "";

		// prescription_user_id
		if (strval($this->prescription_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->prescription_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->CurrentValue;
			}
		} else {
			$this->prescription_user_id->ViewValue = NULL;
		}
		$this->prescription_user_id->ViewCustomAttributes = "";

		// prescription_drugstore_id
		if (strval($this->prescription_drugstore_id->CurrentValue) <> "") {
			$sFilterWrk = "`apotech_id`" . ew_SearchString("=", $this->prescription_drugstore_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->CurrentValue;
			}
		} else {
			$this->prescription_drugstore_id->ViewValue = NULL;
		}
		$this->prescription_drugstore_id->ViewCustomAttributes = "";

		// prescription_doctor_id
		if (strval($this->prescription_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->prescription_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = " `admin_level` = 2 ";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->prescription_doctor_id->ViewValue = $this->prescription_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_doctor_id->ViewValue = $this->prescription_doctor_id->CurrentValue;
			}
		} else {
			$this->prescription_doctor_id->ViewValue = NULL;
		}
		$this->prescription_doctor_id->ViewCustomAttributes = "";

		// prescription_admin_id
		if (strval($this->prescription_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->prescription_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->prescription_admin_id->ViewValue = $this->prescription_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_admin_id->ViewValue = $this->prescription_admin_id->CurrentValue;
			}
		} else {
			$this->prescription_admin_id->ViewValue = NULL;
		}
		$this->prescription_admin_id->ViewCustomAttributes = "";

		// prescription_request_date
		$this->prescription_request_date->ViewValue = $this->prescription_request_date->CurrentValue;
		$this->prescription_request_date->ViewValue = ew_FormatDateTime($this->prescription_request_date->ViewValue, 7);
		$this->prescription_request_date->ViewCustomAttributes = "";

		// prescription_start_on
		$this->prescription_start_on->ViewValue = $this->prescription_start_on->CurrentValue;
		$this->prescription_start_on->ViewValue = ew_FormatDateTime($this->prescription_start_on->ViewValue, 7);
		$this->prescription_start_on->ViewCustomAttributes = "";

		// prescription_close_on
		$this->prescription_close_on->ViewValue = $this->prescription_close_on->CurrentValue;
		$this->prescription_close_on->ViewValue = ew_FormatDateTime($this->prescription_close_on->ViewValue, 7);
		$this->prescription_close_on->ViewCustomAttributes = "";

		// prescription_close
		if (strval($this->prescription_close->CurrentValue) <> "") {
			$this->prescription_close->ViewValue = $this->prescription_close->OptionCaption($this->prescription_close->CurrentValue);
		} else {
			$this->prescription_close->ViewValue = NULL;
		}
		$this->prescription_close->ViewCustomAttributes = "";

			// prescription_user_id
			$this->prescription_user_id->LinkCustomAttributes = "";
			$this->prescription_user_id->HrefValue = "";
			$this->prescription_user_id->TooltipValue = "";

			// prescription_drugstore_id
			$this->prescription_drugstore_id->LinkCustomAttributes = "";
			$this->prescription_drugstore_id->HrefValue = "";
			$this->prescription_drugstore_id->TooltipValue = "";

			// prescription_doctor_id
			$this->prescription_doctor_id->LinkCustomAttributes = "";
			$this->prescription_doctor_id->HrefValue = "";
			$this->prescription_doctor_id->TooltipValue = "";

			// prescription_admin_id
			$this->prescription_admin_id->LinkCustomAttributes = "";
			$this->prescription_admin_id->HrefValue = "";
			$this->prescription_admin_id->TooltipValue = "";

			// prescription_request_date
			$this->prescription_request_date->LinkCustomAttributes = "";
			$this->prescription_request_date->HrefValue = "";
			$this->prescription_request_date->TooltipValue = "";

			// prescription_start_on
			$this->prescription_start_on->LinkCustomAttributes = "";
			$this->prescription_start_on->HrefValue = "";
			$this->prescription_start_on->TooltipValue = "";

			// prescription_close_on
			$this->prescription_close_on->LinkCustomAttributes = "";
			$this->prescription_close_on->HrefValue = "";
			$this->prescription_close_on->TooltipValue = "";

			// prescription_close
			$this->prescription_close->LinkCustomAttributes = "";
			$this->prescription_close->HrefValue = "";
			$this->prescription_close->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// prescription_user_id
			$this->prescription_user_id->EditAttrs["class"] = "form-control";
			$this->prescription_user_id->EditCustomAttributes = "";
			if ($this->prescription_user_id->getSessionValue() <> "") {
				$this->prescription_user_id->CurrentValue = $this->prescription_user_id->getSessionValue();
			if (strval($this->prescription_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->prescription_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->prescription_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `user_surname` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->prescription_user_id->ViewValue = $this->prescription_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->prescription_user_id->ViewValue = $this->prescription_user_id->CurrentValue;
				}
			} else {
				$this->prescription_user_id->ViewValue = NULL;
			}
			$this->prescription_user_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->prescription_user_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->prescription_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->prescription_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->prescription_user_id->EditValue = $arwrk;
			}

			// prescription_drugstore_id
			$this->prescription_drugstore_id->EditAttrs["class"] = "form-control";
			$this->prescription_drugstore_id->EditCustomAttributes = "";
			if ($this->prescription_drugstore_id->getSessionValue() <> "") {
				$this->prescription_drugstore_id->CurrentValue = $this->prescription_drugstore_id->getSessionValue();
			if (strval($this->prescription_drugstore_id->CurrentValue) <> "") {
				$sFilterWrk = "`apotech_id`" . ew_SearchString("=", $this->prescription_drugstore_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->CurrentValue;
				}
			} else {
				$this->prescription_drugstore_id->ViewValue = NULL;
			}
			$this->prescription_drugstore_id->ViewCustomAttributes = "";
			} else {
			if (trim(strval($this->prescription_drugstore_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`apotech_id`" . ew_SearchString("=", $this->prescription_drugstore_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_apotech`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_apotech`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->prescription_drugstore_id->EditValue = $arwrk;
			}

			// prescription_doctor_id
			$this->prescription_doctor_id->EditAttrs["class"] = "form-control";
			$this->prescription_doctor_id->EditCustomAttributes = "";
			if (trim(strval($this->prescription_doctor_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->prescription_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			$lookuptblfilter = " `admin_level` = 2 ";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_prescription"]->UserIDAllow("add")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->prescription_doctor_id->EditValue = $arwrk;

			// prescription_admin_id
			// prescription_request_date

			$this->prescription_request_date->EditAttrs["class"] = "form-control";
			$this->prescription_request_date->EditCustomAttributes = "";
			$this->prescription_request_date->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->prescription_request_date->CurrentValue, 7));
			$this->prescription_request_date->PlaceHolder = ew_RemoveHtml($this->prescription_request_date->FldCaption());

			// prescription_start_on
			$this->prescription_start_on->EditAttrs["class"] = "form-control";
			$this->prescription_start_on->EditCustomAttributes = "";
			$this->prescription_start_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->prescription_start_on->CurrentValue, 7));
			$this->prescription_start_on->PlaceHolder = ew_RemoveHtml($this->prescription_start_on->FldCaption());

			// prescription_close_on
			$this->prescription_close_on->EditAttrs["class"] = "form-control";
			$this->prescription_close_on->EditCustomAttributes = "";
			$this->prescription_close_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->prescription_close_on->CurrentValue, 7));
			$this->prescription_close_on->PlaceHolder = ew_RemoveHtml($this->prescription_close_on->FldCaption());

			// prescription_close
			$this->prescription_close->EditCustomAttributes = "";
			$this->prescription_close->EditValue = $this->prescription_close->Options(FALSE);

			// Edit refer script
			// prescription_user_id

			$this->prescription_user_id->LinkCustomAttributes = "";
			$this->prescription_user_id->HrefValue = "";

			// prescription_drugstore_id
			$this->prescription_drugstore_id->LinkCustomAttributes = "";
			$this->prescription_drugstore_id->HrefValue = "";

			// prescription_doctor_id
			$this->prescription_doctor_id->LinkCustomAttributes = "";
			$this->prescription_doctor_id->HrefValue = "";

			// prescription_admin_id
			$this->prescription_admin_id->LinkCustomAttributes = "";
			$this->prescription_admin_id->HrefValue = "";

			// prescription_request_date
			$this->prescription_request_date->LinkCustomAttributes = "";
			$this->prescription_request_date->HrefValue = "";

			// prescription_start_on
			$this->prescription_start_on->LinkCustomAttributes = "";
			$this->prescription_start_on->HrefValue = "";

			// prescription_close_on
			$this->prescription_close_on->LinkCustomAttributes = "";
			$this->prescription_close_on->HrefValue = "";

			// prescription_close
			$this->prescription_close->LinkCustomAttributes = "";
			$this->prescription_close->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckEuroDate($this->prescription_request_date->FormValue)) {
			ew_AddMessage($gsFormError, $this->prescription_request_date->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->prescription_start_on->FormValue)) {
			ew_AddMessage($gsFormError, $this->prescription_start_on->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->prescription_close_on->FormValue)) {
			ew_AddMessage($gsFormError, $this->prescription_close_on->FldErrMsg());
		}
		if ($this->prescription_close->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->prescription_close->FldCaption(), $this->prescription_close->ReqErrMsg));
		}

		// Validate detail grid
		$DetailTblVar = explode(",", $this->getCurrentDetailTable());
		if (in_array("telecare_prescription_drug", $DetailTblVar) && $GLOBALS["telecare_prescription_drug"]->DetailAdd) {
			if (!isset($GLOBALS["telecare_prescription_drug_grid"])) $GLOBALS["telecare_prescription_drug_grid"] = new ctelecare_prescription_drug_grid(); // get detail page object
			$GLOBALS["telecare_prescription_drug_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Begin transaction
		if ($this->getCurrentDetailTable() <> "")
			$conn->BeginTrans();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// prescription_user_id
		$this->prescription_user_id->SetDbValueDef($rsnew, $this->prescription_user_id->CurrentValue, NULL, FALSE);

		// prescription_drugstore_id
		$this->prescription_drugstore_id->SetDbValueDef($rsnew, $this->prescription_drugstore_id->CurrentValue, NULL, FALSE);

		// prescription_doctor_id
		$this->prescription_doctor_id->SetDbValueDef($rsnew, $this->prescription_doctor_id->CurrentValue, NULL, FALSE);

		// prescription_admin_id
		$this->prescription_admin_id->SetDbValueDef($rsnew, CurrentUserID(), 0);
		$rsnew['prescription_admin_id'] = &$this->prescription_admin_id->DbValue;

		// prescription_request_date
		$this->prescription_request_date->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->prescription_request_date->CurrentValue, 7), NULL, FALSE);

		// prescription_start_on
		$this->prescription_start_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->prescription_start_on->CurrentValue, 7), NULL, FALSE);

		// prescription_close_on
		$this->prescription_close_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->prescription_close_on->CurrentValue, 7), NULL, FALSE);

		// prescription_close
		$this->prescription_close->SetDbValueDef($rsnew, $this->prescription_close->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->prescription_id->setDbValue($conn->Insert_ID());
				$rsnew['prescription_id'] = $this->prescription_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}

		// Add detail records
		if ($AddRow) {
			$DetailTblVar = explode(",", $this->getCurrentDetailTable());
			if (in_array("telecare_prescription_drug", $DetailTblVar) && $GLOBALS["telecare_prescription_drug"]->DetailAdd) {
				$GLOBALS["telecare_prescription_drug"]->prescription_id->setSessionValue($this->prescription_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["telecare_prescription_drug_grid"])) $GLOBALS["telecare_prescription_drug_grid"] = new ctelecare_prescription_drug_grid(); // Get detail page object
				$AddRow = $GLOBALS["telecare_prescription_drug_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["telecare_prescription_drug"]->prescription_id->setSessionValue(""); // Clear master key if insert failed
			}
		}

		// Commit/Rollback transaction
		if ($this->getCurrentDetailTable() <> "") {
			if ($AddRow) {
				$conn->CommitTrans(); // Commit transaction
			} else {
				$conn->RollbackTrans(); // Rollback transaction
			}
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setQueryStringValue($_GET["fk_user_id"]);
					$this->prescription_user_id->setQueryStringValue($GLOBALS["telecare_user"]->user_id->QueryStringValue);
					$this->prescription_user_id->setSessionValue($this->prescription_user_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "telecare_apotech") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_apotech_id"] <> "") {
					$GLOBALS["telecare_apotech"]->apotech_id->setQueryStringValue($_GET["fk_apotech_id"]);
					$this->prescription_drugstore_id->setQueryStringValue($GLOBALS["telecare_apotech"]->apotech_id->QueryStringValue);
					$this->prescription_drugstore_id->setSessionValue($this->prescription_drugstore_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_apotech"]->apotech_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setFormValue($_POST["fk_user_id"]);
					$this->prescription_user_id->setFormValue($GLOBALS["telecare_user"]->user_id->FormValue);
					$this->prescription_user_id->setSessionValue($this->prescription_user_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "telecare_apotech") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_apotech_id"] <> "") {
					$GLOBALS["telecare_apotech"]->apotech_id->setFormValue($_POST["fk_apotech_id"]);
					$this->prescription_drugstore_id->setFormValue($GLOBALS["telecare_apotech"]->apotech_id->FormValue);
					$this->prescription_drugstore_id->setSessionValue($this->prescription_drugstore_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_apotech"]->apotech_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "telecare_user") {
				if ($this->prescription_user_id->QueryStringValue == "") $this->prescription_user_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "telecare_apotech") {
				if ($this->prescription_drugstore_id->QueryStringValue == "") $this->prescription_drugstore_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("telecare_prescription_drug", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_prescription_drug_grid"]))
					$GLOBALS["telecare_prescription_drug_grid"] = new ctelecare_prescription_drug_grid;
				if ($GLOBALS["telecare_prescription_drug_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["telecare_prescription_drug_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["telecare_prescription_drug_grid"]->CurrentMode = "add";
					$GLOBALS["telecare_prescription_drug_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["telecare_prescription_drug_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_prescription_drug_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_prescription_drug_grid"]->prescription_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_prescription_drug_grid"]->prescription_id->CurrentValue = $this->prescription_id->CurrentValue;
					$GLOBALS["telecare_prescription_drug_grid"]->prescription_id->setSessionValue($GLOBALS["telecare_prescription_drug_grid"]->prescription_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_prescriptionlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_prescription_add)) $telecare_prescription_add = new ctelecare_prescription_add();

// Page init
$telecare_prescription_add->Page_Init();

// Page main
$telecare_prescription_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_prescription_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_prescriptionadd = new ew_Form("ftelecare_prescriptionadd", "add");

// Validate form
ftelecare_prescriptionadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_prescription_request_date");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription->prescription_request_date->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_start_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription->prescription_start_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_close_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription->prescription_close_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_close");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_prescription->prescription_close->FldCaption(), $telecare_prescription->prescription_close->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_prescriptionadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_prescriptionadd.ValidateRequired = true;
<?php } else { ?>
ftelecare_prescriptionadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_prescriptionadd.Lists["x_prescription_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptionadd.Lists["x_prescription_drugstore_id"] = {"LinkField":"x_apotech_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_apotech_company_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptionadd.Lists["x_prescription_doctor_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptionadd.Lists["x_prescription_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptionadd.Lists["x_prescription_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptionadd.Lists["x_prescription_close"].Options = <?php echo json_encode($telecare_prescription->prescription_close->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_prescription_add->ShowPageHeader(); ?>
<?php
$telecare_prescription_add->ShowMessage();
?>
<form name="ftelecare_prescriptionadd" id="ftelecare_prescriptionadd" class="<?php echo $telecare_prescription_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_prescription_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_prescription_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_prescription">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($telecare_prescription->prescription_user_id->Visible) { // prescription_user_id ?>
	<div id="r_prescription_user_id" class="form-group">
		<label id="elh_telecare_prescription_prescription_user_id" for="x_prescription_user_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription->prescription_user_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription->prescription_user_id->CellAttributes() ?>>
<?php if ($telecare_prescription->prescription_user_id->getSessionValue() <> "") { ?>
<span id="el_telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_prescription_user_id" name="x_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_telecare_prescription_prescription_user_id">
<select data-table="telecare_prescription" data-field="x_prescription_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_user_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_user_id->DisplayValueSeparator) : $telecare_prescription->prescription_user_id->DisplayValueSeparator) ?>" id="x_prescription_user_id" name="x_prescription_user_id"<?php echo $telecare_prescription->prescription_user_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_user_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_user_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_user_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_user_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_user_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_user_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_user_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_user_id->LookupFilters += array("f0" => "`user_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `user_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_user_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_prescription_user_id" id="s_x_prescription_user_id" value="<?php echo $telecare_prescription->prescription_user_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $telecare_prescription->prescription_user_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription->prescription_drugstore_id->Visible) { // prescription_drugstore_id ?>
	<div id="r_prescription_drugstore_id" class="form-group">
		<label id="elh_telecare_prescription_prescription_drugstore_id" for="x_prescription_drugstore_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription->prescription_drugstore_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription->prescription_drugstore_id->CellAttributes() ?>>
<?php if ($telecare_prescription->prescription_drugstore_id->getSessionValue() <> "") { ?>
<span id="el_telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_drugstore_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_prescription_drugstore_id" name="x_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_telecare_prescription_prescription_drugstore_id">
<select data-table="telecare_prescription" data-field="x_prescription_drugstore_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) : $telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ?>" id="x_prescription_drugstore_id" name="x_prescription_drugstore_id"<?php echo $telecare_prescription->prescription_drugstore_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_drugstore_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_drugstore_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_drugstore_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_drugstore_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_drugstore_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_drugstore_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_drugstore_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_drugstore_id->LookupFilters += array("f0" => "`apotech_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_drugstore_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_prescription_drugstore_id" id="s_x_prescription_drugstore_id" value="<?php echo $telecare_prescription->prescription_drugstore_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php echo $telecare_prescription->prescription_drugstore_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription->prescription_doctor_id->Visible) { // prescription_doctor_id ?>
	<div id="r_prescription_doctor_id" class="form-group">
		<label id="elh_telecare_prescription_prescription_doctor_id" for="x_prescription_doctor_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription->prescription_doctor_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription->prescription_doctor_id->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_doctor_id">
<select data-table="telecare_prescription" data-field="x_prescription_doctor_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) : $telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ?>" id="x_prescription_doctor_id" name="x_prescription_doctor_id"<?php echo $telecare_prescription->prescription_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_doctor_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_doctor_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_doctor_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_doctor_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = " `admin_level` = 2 ";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_prescription"]->UserIDAllow("add")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_prescription->prescription_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_prescription_doctor_id" id="s_x_prescription_doctor_id" value="<?php echo $telecare_prescription->prescription_doctor_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_prescription->prescription_doctor_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription->prescription_request_date->Visible) { // prescription_request_date ?>
	<div id="r_prescription_request_date" class="form-group">
		<label id="elh_telecare_prescription_prescription_request_date" for="x_prescription_request_date" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription->prescription_request_date->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription->prescription_request_date->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_request_date">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_request_date" data-format="7" name="x_prescription_request_date" id="x_prescription_request_date" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_request_date->EditValue ?>"<?php echo $telecare_prescription->prescription_request_date->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_request_date->ReadOnly && !$telecare_prescription->prescription_request_date->Disabled && !isset($telecare_prescription->prescription_request_date->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_request_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptionadd", "x_prescription_request_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_prescription->prescription_request_date->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription->prescription_start_on->Visible) { // prescription_start_on ?>
	<div id="r_prescription_start_on" class="form-group">
		<label id="elh_telecare_prescription_prescription_start_on" for="x_prescription_start_on" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription->prescription_start_on->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription->prescription_start_on->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_start_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_start_on" data-format="7" name="x_prescription_start_on" id="x_prescription_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_start_on->EditValue ?>"<?php echo $telecare_prescription->prescription_start_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_start_on->ReadOnly && !$telecare_prescription->prescription_start_on->Disabled && !isset($telecare_prescription->prescription_start_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptionadd", "x_prescription_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_prescription->prescription_start_on->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription->prescription_close_on->Visible) { // prescription_close_on ?>
	<div id="r_prescription_close_on" class="form-group">
		<label id="elh_telecare_prescription_prescription_close_on" for="x_prescription_close_on" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription->prescription_close_on->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription->prescription_close_on->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_close_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_close_on" data-format="7" name="x_prescription_close_on" id="x_prescription_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_close_on->EditValue ?>"<?php echo $telecare_prescription->prescription_close_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_close_on->ReadOnly && !$telecare_prescription->prescription_close_on->Disabled && !isset($telecare_prescription->prescription_close_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptionadd", "x_prescription_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_prescription->prescription_close_on->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription->prescription_close->Visible) { // prescription_close ?>
	<div id="r_prescription_close" class="form-group">
		<label id="elh_telecare_prescription_prescription_close" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription->prescription_close->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription->prescription_close->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_close">
<div id="tp_x_prescription_close" class="ewTemplate"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_close->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_close->DisplayValueSeparator) : $telecare_prescription->prescription_close->DisplayValueSeparator) ?>" name="x_prescription_close" id="x_prescription_close" value="{value}"<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>></div>
<div id="dsl_x_prescription_close" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_prescription->prescription_close->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_prescription->prescription_close->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x_prescription_close" id="x_prescription_close_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_close->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x_prescription_close" id="x_prescription_close_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->CurrentValue) ?>" checked<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_prescription->prescription_close->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<?php
	if (in_array("telecare_prescription_drug", explode(",", $telecare_prescription->getCurrentDetailTable())) && $telecare_prescription_drug->DetailAdd) {
?>
<?php if ($telecare_prescription->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_prescription_drug", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_prescription_druggrid.php" ?>
<?php } ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_prescription_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_prescriptionadd.Init();
</script>
<?php
$telecare_prescription_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_prescription_add->Page_Terminate();
?>
