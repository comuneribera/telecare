<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_prescriptioninfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_apotechinfo.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_prescription_delete = NULL; // Initialize page object first

class ctelecare_prescription_delete extends ctelecare_prescription {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_prescription';

	// Page object name
	var $PageObjName = 'telecare_prescription_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_prescription)
		if (!isset($GLOBALS["telecare_prescription"]) || get_class($GLOBALS["telecare_prescription"]) == "ctelecare_prescription") {
			$GLOBALS["telecare_prescription"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_prescription"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_apotech)
		if (!isset($GLOBALS['telecare_apotech'])) $GLOBALS['telecare_apotech'] = new ctelecare_apotech();

		// Table object (telecare_user)
		if (!isset($GLOBALS['telecare_user'])) $GLOBALS['telecare_user'] = new ctelecare_user();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_prescription', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_prescriptionlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->prescription_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_prescription;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_prescription);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("telecare_prescriptionlist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in telecare_prescription class, telecare_prescriptioninfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->prescription_id->setDbValue($rs->fields('prescription_id'));
		$this->prescription_user_id->setDbValue($rs->fields('prescription_user_id'));
		$this->prescription_drugstore_id->setDbValue($rs->fields('prescription_drugstore_id'));
		$this->prescription_doctor_id->setDbValue($rs->fields('prescription_doctor_id'));
		$this->prescription_admin_id->setDbValue($rs->fields('prescription_admin_id'));
		$this->prescription_request_date->setDbValue($rs->fields('prescription_request_date'));
		$this->prescription_start_on->setDbValue($rs->fields('prescription_start_on'));
		$this->prescription_close_on->setDbValue($rs->fields('prescription_close_on'));
		$this->prescription_close->setDbValue($rs->fields('prescription_close'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->prescription_id->DbValue = $row['prescription_id'];
		$this->prescription_user_id->DbValue = $row['prescription_user_id'];
		$this->prescription_drugstore_id->DbValue = $row['prescription_drugstore_id'];
		$this->prescription_doctor_id->DbValue = $row['prescription_doctor_id'];
		$this->prescription_admin_id->DbValue = $row['prescription_admin_id'];
		$this->prescription_request_date->DbValue = $row['prescription_request_date'];
		$this->prescription_start_on->DbValue = $row['prescription_start_on'];
		$this->prescription_close_on->DbValue = $row['prescription_close_on'];
		$this->prescription_close->DbValue = $row['prescription_close'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// prescription_id
		// prescription_user_id
		// prescription_drugstore_id
		// prescription_doctor_id
		// prescription_admin_id
		// prescription_request_date
		// prescription_start_on
		// prescription_close_on
		// prescription_close

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// prescription_id
		$this->prescription_id->ViewValue = $this->prescription_id->CurrentValue;
		$this->prescription_id->ViewCustomAttributes = "";

		// prescription_user_id
		if (strval($this->prescription_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->prescription_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->CurrentValue;
			}
		} else {
			$this->prescription_user_id->ViewValue = NULL;
		}
		$this->prescription_user_id->ViewCustomAttributes = "";

		// prescription_drugstore_id
		if (strval($this->prescription_drugstore_id->CurrentValue) <> "") {
			$sFilterWrk = "`apotech_id`" . ew_SearchString("=", $this->prescription_drugstore_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->CurrentValue;
			}
		} else {
			$this->prescription_drugstore_id->ViewValue = NULL;
		}
		$this->prescription_drugstore_id->ViewCustomAttributes = "";

		// prescription_doctor_id
		if (strval($this->prescription_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->prescription_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = " `admin_level` = 2 ";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->prescription_doctor_id->ViewValue = $this->prescription_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_doctor_id->ViewValue = $this->prescription_doctor_id->CurrentValue;
			}
		} else {
			$this->prescription_doctor_id->ViewValue = NULL;
		}
		$this->prescription_doctor_id->ViewCustomAttributes = "";

		// prescription_admin_id
		if (strval($this->prescription_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->prescription_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->prescription_admin_id->ViewValue = $this->prescription_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_admin_id->ViewValue = $this->prescription_admin_id->CurrentValue;
			}
		} else {
			$this->prescription_admin_id->ViewValue = NULL;
		}
		$this->prescription_admin_id->ViewCustomAttributes = "";

		// prescription_request_date
		$this->prescription_request_date->ViewValue = $this->prescription_request_date->CurrentValue;
		$this->prescription_request_date->ViewValue = ew_FormatDateTime($this->prescription_request_date->ViewValue, 7);
		$this->prescription_request_date->ViewCustomAttributes = "";

		// prescription_start_on
		$this->prescription_start_on->ViewValue = $this->prescription_start_on->CurrentValue;
		$this->prescription_start_on->ViewValue = ew_FormatDateTime($this->prescription_start_on->ViewValue, 7);
		$this->prescription_start_on->ViewCustomAttributes = "";

		// prescription_close_on
		$this->prescription_close_on->ViewValue = $this->prescription_close_on->CurrentValue;
		$this->prescription_close_on->ViewValue = ew_FormatDateTime($this->prescription_close_on->ViewValue, 7);
		$this->prescription_close_on->ViewCustomAttributes = "";

		// prescription_close
		if (strval($this->prescription_close->CurrentValue) <> "") {
			$this->prescription_close->ViewValue = $this->prescription_close->OptionCaption($this->prescription_close->CurrentValue);
		} else {
			$this->prescription_close->ViewValue = NULL;
		}
		$this->prescription_close->ViewCustomAttributes = "";

			// prescription_id
			$this->prescription_id->LinkCustomAttributes = "";
			$this->prescription_id->HrefValue = "";
			$this->prescription_id->TooltipValue = "";

			// prescription_user_id
			$this->prescription_user_id->LinkCustomAttributes = "";
			$this->prescription_user_id->HrefValue = "";
			$this->prescription_user_id->TooltipValue = "";

			// prescription_drugstore_id
			$this->prescription_drugstore_id->LinkCustomAttributes = "";
			$this->prescription_drugstore_id->HrefValue = "";
			$this->prescription_drugstore_id->TooltipValue = "";

			// prescription_doctor_id
			$this->prescription_doctor_id->LinkCustomAttributes = "";
			$this->prescription_doctor_id->HrefValue = "";
			$this->prescription_doctor_id->TooltipValue = "";

			// prescription_admin_id
			$this->prescription_admin_id->LinkCustomAttributes = "";
			$this->prescription_admin_id->HrefValue = "";
			$this->prescription_admin_id->TooltipValue = "";

			// prescription_request_date
			$this->prescription_request_date->LinkCustomAttributes = "";
			$this->prescription_request_date->HrefValue = "";
			$this->prescription_request_date->TooltipValue = "";

			// prescription_start_on
			$this->prescription_start_on->LinkCustomAttributes = "";
			$this->prescription_start_on->HrefValue = "";
			$this->prescription_start_on->TooltipValue = "";

			// prescription_close_on
			$this->prescription_close_on->LinkCustomAttributes = "";
			$this->prescription_close_on->HrefValue = "";
			$this->prescription_close_on->TooltipValue = "";

			// prescription_close
			$this->prescription_close->LinkCustomAttributes = "";
			$this->prescription_close->HrefValue = "";
			$this->prescription_close->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['prescription_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setQueryStringValue($_GET["fk_user_id"]);
					$this->prescription_user_id->setQueryStringValue($GLOBALS["telecare_user"]->user_id->QueryStringValue);
					$this->prescription_user_id->setSessionValue($this->prescription_user_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "telecare_apotech") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_apotech_id"] <> "") {
					$GLOBALS["telecare_apotech"]->apotech_id->setQueryStringValue($_GET["fk_apotech_id"]);
					$this->prescription_drugstore_id->setQueryStringValue($GLOBALS["telecare_apotech"]->apotech_id->QueryStringValue);
					$this->prescription_drugstore_id->setSessionValue($this->prescription_drugstore_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_apotech"]->apotech_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setFormValue($_POST["fk_user_id"]);
					$this->prescription_user_id->setFormValue($GLOBALS["telecare_user"]->user_id->FormValue);
					$this->prescription_user_id->setSessionValue($this->prescription_user_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
			if ($sMasterTblVar == "telecare_apotech") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_apotech_id"] <> "") {
					$GLOBALS["telecare_apotech"]->apotech_id->setFormValue($_POST["fk_apotech_id"]);
					$this->prescription_drugstore_id->setFormValue($GLOBALS["telecare_apotech"]->apotech_id->FormValue);
					$this->prescription_drugstore_id->setSessionValue($this->prescription_drugstore_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_apotech"]->apotech_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "telecare_user") {
				if ($this->prescription_user_id->QueryStringValue == "") $this->prescription_user_id->setSessionValue("");
			}
			if ($sMasterTblVar <> "telecare_apotech") {
				if ($this->prescription_drugstore_id->QueryStringValue == "") $this->prescription_drugstore_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_prescriptionlist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_prescription_delete)) $telecare_prescription_delete = new ctelecare_prescription_delete();

// Page init
$telecare_prescription_delete->Page_Init();

// Page main
$telecare_prescription_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_prescription_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftelecare_prescriptiondelete = new ew_Form("ftelecare_prescriptiondelete", "delete");

// Form_CustomValidate event
ftelecare_prescriptiondelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_prescriptiondelete.ValidateRequired = true;
<?php } else { ?>
ftelecare_prescriptiondelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_prescriptiondelete.Lists["x_prescription_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiondelete.Lists["x_prescription_drugstore_id"] = {"LinkField":"x_apotech_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_apotech_company_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiondelete.Lists["x_prescription_doctor_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiondelete.Lists["x_prescription_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiondelete.Lists["x_prescription_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiondelete.Lists["x_prescription_close"].Options = <?php echo json_encode($telecare_prescription->prescription_close->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($telecare_prescription_delete->Recordset = $telecare_prescription_delete->LoadRecordset())
	$telecare_prescription_deleteTotalRecs = $telecare_prescription_delete->Recordset->RecordCount(); // Get record count
if ($telecare_prescription_deleteTotalRecs <= 0) { // No record found, exit
	if ($telecare_prescription_delete->Recordset)
		$telecare_prescription_delete->Recordset->Close();
	$telecare_prescription_delete->Page_Terminate("telecare_prescriptionlist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_prescription_delete->ShowPageHeader(); ?>
<?php
$telecare_prescription_delete->ShowMessage();
?>
<form name="ftelecare_prescriptiondelete" id="ftelecare_prescriptiondelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_prescription_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_prescription_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_prescription">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($telecare_prescription_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $telecare_prescription->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($telecare_prescription->prescription_id->Visible) { // prescription_id ?>
		<th><span id="elh_telecare_prescription_prescription_id" class="telecare_prescription_prescription_id"><?php echo $telecare_prescription->prescription_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_user_id->Visible) { // prescription_user_id ?>
		<th><span id="elh_telecare_prescription_prescription_user_id" class="telecare_prescription_prescription_user_id"><?php echo $telecare_prescription->prescription_user_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_drugstore_id->Visible) { // prescription_drugstore_id ?>
		<th><span id="elh_telecare_prescription_prescription_drugstore_id" class="telecare_prescription_prescription_drugstore_id"><?php echo $telecare_prescription->prescription_drugstore_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_doctor_id->Visible) { // prescription_doctor_id ?>
		<th><span id="elh_telecare_prescription_prescription_doctor_id" class="telecare_prescription_prescription_doctor_id"><?php echo $telecare_prescription->prescription_doctor_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_admin_id->Visible) { // prescription_admin_id ?>
		<th><span id="elh_telecare_prescription_prescription_admin_id" class="telecare_prescription_prescription_admin_id"><?php echo $telecare_prescription->prescription_admin_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_request_date->Visible) { // prescription_request_date ?>
		<th><span id="elh_telecare_prescription_prescription_request_date" class="telecare_prescription_prescription_request_date"><?php echo $telecare_prescription->prescription_request_date->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_start_on->Visible) { // prescription_start_on ?>
		<th><span id="elh_telecare_prescription_prescription_start_on" class="telecare_prescription_prescription_start_on"><?php echo $telecare_prescription->prescription_start_on->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_close_on->Visible) { // prescription_close_on ?>
		<th><span id="elh_telecare_prescription_prescription_close_on" class="telecare_prescription_prescription_close_on"><?php echo $telecare_prescription->prescription_close_on->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_prescription->prescription_close->Visible) { // prescription_close ?>
		<th><span id="elh_telecare_prescription_prescription_close" class="telecare_prescription_prescription_close"><?php echo $telecare_prescription->prescription_close->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$telecare_prescription_delete->RecCnt = 0;
$i = 0;
while (!$telecare_prescription_delete->Recordset->EOF) {
	$telecare_prescription_delete->RecCnt++;
	$telecare_prescription_delete->RowCnt++;

	// Set row properties
	$telecare_prescription->ResetAttrs();
	$telecare_prescription->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$telecare_prescription_delete->LoadRowValues($telecare_prescription_delete->Recordset);

	// Render row
	$telecare_prescription_delete->RenderRow();
?>
	<tr<?php echo $telecare_prescription->RowAttributes() ?>>
<?php if ($telecare_prescription->prescription_id->Visible) { // prescription_id ?>
		<td<?php echo $telecare_prescription->prescription_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_id" class="telecare_prescription_prescription_id">
<span<?php echo $telecare_prescription->prescription_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_user_id->Visible) { // prescription_user_id ?>
		<td<?php echo $telecare_prescription->prescription_user_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_user_id" class="telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_user_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_drugstore_id->Visible) { // prescription_drugstore_id ?>
		<td<?php echo $telecare_prescription->prescription_drugstore_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_drugstore_id" class="telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_drugstore_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_doctor_id->Visible) { // prescription_doctor_id ?>
		<td<?php echo $telecare_prescription->prescription_doctor_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_doctor_id" class="telecare_prescription_prescription_doctor_id">
<span<?php echo $telecare_prescription->prescription_doctor_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_doctor_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_admin_id->Visible) { // prescription_admin_id ?>
		<td<?php echo $telecare_prescription->prescription_admin_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_admin_id" class="telecare_prescription_prescription_admin_id">
<span<?php echo $telecare_prescription->prescription_admin_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_admin_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_request_date->Visible) { // prescription_request_date ?>
		<td<?php echo $telecare_prescription->prescription_request_date->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_request_date" class="telecare_prescription_prescription_request_date">
<span<?php echo $telecare_prescription->prescription_request_date->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_request_date->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_start_on->Visible) { // prescription_start_on ?>
		<td<?php echo $telecare_prescription->prescription_start_on->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_start_on" class="telecare_prescription_prescription_start_on">
<span<?php echo $telecare_prescription->prescription_start_on->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_start_on->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_close_on->Visible) { // prescription_close_on ?>
		<td<?php echo $telecare_prescription->prescription_close_on->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_close_on" class="telecare_prescription_prescription_close_on">
<span<?php echo $telecare_prescription->prescription_close_on->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_close_on->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_prescription->prescription_close->Visible) { // prescription_close ?>
		<td<?php echo $telecare_prescription->prescription_close->CellAttributes() ?>>
<span id="el<?php echo $telecare_prescription_delete->RowCnt ?>_telecare_prescription_prescription_close" class="telecare_prescription_prescription_close">
<span<?php echo $telecare_prescription->prescription_close->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_close->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$telecare_prescription_delete->Recordset->MoveNext();
}
$telecare_prescription_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_prescription_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftelecare_prescriptiondelete.Init();
</script>
<?php
$telecare_prescription_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_prescription_delete->Page_Terminate();
?>
