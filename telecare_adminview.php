<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_admin_view = NULL; // Initialize page object first

class ctelecare_admin_view extends ctelecare_admin {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_admin';

	// Page object name
	var $PageObjName = 'telecare_admin_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_admin)
		if (!isset($GLOBALS["telecare_admin"]) || get_class($GLOBALS["telecare_admin"]) == "ctelecare_admin") {
			$GLOBALS["telecare_admin"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_admin"];
		}
		$KeyUrl = "";
		if (@$_GET["admin_id"] <> "") {
			$this->RecKey["admin_id"] = $_GET["admin_id"];
			$KeyUrl .= "&amp;admin_id=" . urlencode($this->RecKey["admin_id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_admin', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_adminlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
				$this->Page_Terminate(ew_GetUrl("telecare_adminlist.php"));
			}
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header
		if (@$_GET["admin_id"] <> "") {
			if ($gsExportFile <> "") $gsExportFile .= "_";
			$gsExportFile .= ew_StripSlashes($_GET["admin_id"]);
		}

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Setup export options
		$this->SetupExportOptions();
		$this->admin_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_admin;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_admin);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;
	var $MultiPages; // Multi pages object

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["admin_id"] <> "") {
				$this->admin_id->setQueryStringValue($_GET["admin_id"]);
				$this->RecKey["admin_id"] = $this->admin_id->QueryStringValue;
			} elseif (@$_POST["admin_id"] <> "") {
				$this->admin_id->setFormValue($_POST["admin_id"]);
				$this->RecKey["admin_id"] = $this->admin_id->FormValue;
			} else {
				$bLoadCurrentRecord = TRUE;
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					$this->StartRec = 1; // Initialize start position
					if ($this->Recordset = $this->LoadRecordset()) // Load records
						$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
					if ($this->TotalRecs <= 0) { // No record found
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$this->Page_Terminate("telecare_adminlist.php"); // Return to list page
					} elseif ($bLoadCurrentRecord) { // Load current record position
						$this->SetUpStartRec(); // Set up start record position

						// Point to current record
						if (intval($this->StartRec) <= intval($this->TotalRecs)) {
							$bMatchRecord = TRUE;
							$this->Recordset->Move($this->StartRec-1);
						}
					} else { // Match key values
						while (!$this->Recordset->EOF) {
							if (strval($this->admin_id->CurrentValue) == strval($this->Recordset->fields('admin_id'))) {
								$this->setStartRecordNumber($this->StartRec); // Save record position
								$bMatchRecord = TRUE;
								break;
							} else {
								$this->StartRec++;
								$this->Recordset->MoveNext();
							}
						}
					}
					if (!$bMatchRecord) {
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "telecare_adminlist.php"; // No matching record, return to list
					} else {
						$this->LoadRowValues($this->Recordset); // Load row values
					}
			}

			// Export data only
			if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
				$this->ExportData();
				$this->Page_Terminate(); // Terminate response
				exit();
			}
		} else {
			$sReturnUrl = "telecare_adminlist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit()&& $this->ShowOptionLink('edit'));

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd() && $this->ShowOptionLink('add'));

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete() && $this->ShowOptionLink('delete'));

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->admin_id->setDbValue($rs->fields('admin_id'));
		$this->admin_parent_id->setDbValue($rs->fields('admin_parent_id'));
		$this->admin_name->setDbValue($rs->fields('admin_name'));
		$this->admin_surname->setDbValue($rs->fields('admin_surname'));
		$this->admin_gender->setDbValue($rs->fields('admin_gender'));
		$this->admin_email->setDbValue($rs->fields('admin_email'));
		$this->admin_mobile->setDbValue($rs->fields('admin_mobile'));
		$this->admin_phone->setDbValue($rs->fields('admin_phone'));
		$this->admin_modules->setDbValue($rs->fields('admin_modules'));
		$this->admin_language->setDbValue($rs->fields('admin_language'));
		$this->admin_is_active->setDbValue($rs->fields('admin_is_active'));
		$this->admin_company_name->setDbValue($rs->fields('admin_company_name'));
		$this->admin_company_vat->setDbValue($rs->fields('admin_company_vat'));
		$this->admin_company_logo_file->Upload->DbValue = $rs->fields('admin_company_logo_file');
		$this->admin_company_logo_file->CurrentValue = $this->admin_company_logo_file->Upload->DbValue;
		$this->admin_company_logo_width->setDbValue($rs->fields('admin_company_logo_width'));
		$this->admin_company_logo_height->setDbValue($rs->fields('admin_company_logo_height'));
		$this->admin_company_logo_size->setDbValue($rs->fields('admin_company_logo_size'));
		$this->admin_company_address->setDbValue($rs->fields('admin_company_address'));
		$this->admin_company_region_id->setDbValue($rs->fields('admin_company_region_id'));
		$this->admin_company_province_id->setDbValue($rs->fields('admin_company_province_id'));
		$this->admin_company_city_id->setDbValue($rs->fields('admin_company_city_id'));
		$this->admin_company_phone->setDbValue($rs->fields('admin_company_phone'));
		$this->admin_company_email->setDbValue($rs->fields('admin_company_email'));
		$this->admin_company_web->setDbValue($rs->fields('admin_company_web'));
		$this->admin_contract->setDbValue($rs->fields('admin_contract'));
		$this->admin_expire->setDbValue($rs->fields('admin_expire'));
		$this->admin_company_facebook->setDbValue($rs->fields('admin_company_facebook'));
		$this->admin_company_twitter->setDbValue($rs->fields('admin_company_twitter'));
		$this->admin_company_googleplus->setDbValue($rs->fields('admin_company_googleplus'));
		$this->admin_company_linkedin->setDbValue($rs->fields('admin_company_linkedin'));
		$this->admin_link_playstore->setDbValue($rs->fields('admin_link_playstore'));
		$this->admin_link_appstore->setDbValue($rs->fields('admin_link_appstore'));
		$this->admin_link_windowsstore->setDbValue($rs->fields('admin_link_windowsstore'));
		$this->admin_level->setDbValue($rs->fields('admin_level'));
		$this->admin_username->setDbValue($rs->fields('admin_username'));
		$this->admin_password->setDbValue($rs->fields('admin_password'));
		$this->admin_company_autohority->setDbValue($rs->fields('admin_company_autohority'));
		$this->admin_auth_token->setDbValue($rs->fields('admin_auth_token'));
		$this->admin_last_update->setDbValue($rs->fields('admin_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->admin_id->DbValue = $row['admin_id'];
		$this->admin_parent_id->DbValue = $row['admin_parent_id'];
		$this->admin_name->DbValue = $row['admin_name'];
		$this->admin_surname->DbValue = $row['admin_surname'];
		$this->admin_gender->DbValue = $row['admin_gender'];
		$this->admin_email->DbValue = $row['admin_email'];
		$this->admin_mobile->DbValue = $row['admin_mobile'];
		$this->admin_phone->DbValue = $row['admin_phone'];
		$this->admin_modules->DbValue = $row['admin_modules'];
		$this->admin_language->DbValue = $row['admin_language'];
		$this->admin_is_active->DbValue = $row['admin_is_active'];
		$this->admin_company_name->DbValue = $row['admin_company_name'];
		$this->admin_company_vat->DbValue = $row['admin_company_vat'];
		$this->admin_company_logo_file->Upload->DbValue = $row['admin_company_logo_file'];
		$this->admin_company_logo_width->DbValue = $row['admin_company_logo_width'];
		$this->admin_company_logo_height->DbValue = $row['admin_company_logo_height'];
		$this->admin_company_logo_size->DbValue = $row['admin_company_logo_size'];
		$this->admin_company_address->DbValue = $row['admin_company_address'];
		$this->admin_company_region_id->DbValue = $row['admin_company_region_id'];
		$this->admin_company_province_id->DbValue = $row['admin_company_province_id'];
		$this->admin_company_city_id->DbValue = $row['admin_company_city_id'];
		$this->admin_company_phone->DbValue = $row['admin_company_phone'];
		$this->admin_company_email->DbValue = $row['admin_company_email'];
		$this->admin_company_web->DbValue = $row['admin_company_web'];
		$this->admin_contract->DbValue = $row['admin_contract'];
		$this->admin_expire->DbValue = $row['admin_expire'];
		$this->admin_company_facebook->DbValue = $row['admin_company_facebook'];
		$this->admin_company_twitter->DbValue = $row['admin_company_twitter'];
		$this->admin_company_googleplus->DbValue = $row['admin_company_googleplus'];
		$this->admin_company_linkedin->DbValue = $row['admin_company_linkedin'];
		$this->admin_link_playstore->DbValue = $row['admin_link_playstore'];
		$this->admin_link_appstore->DbValue = $row['admin_link_appstore'];
		$this->admin_link_windowsstore->DbValue = $row['admin_link_windowsstore'];
		$this->admin_level->DbValue = $row['admin_level'];
		$this->admin_username->DbValue = $row['admin_username'];
		$this->admin_password->DbValue = $row['admin_password'];
		$this->admin_company_autohority->DbValue = $row['admin_company_autohority'];
		$this->admin_auth_token->DbValue = $row['admin_auth_token'];
		$this->admin_last_update->DbValue = $row['admin_last_update'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// admin_id
		// admin_parent_id
		// admin_name
		// admin_surname
		// admin_gender
		// admin_email
		// admin_mobile
		// admin_phone
		// admin_modules
		// admin_language
		// admin_is_active
		// admin_company_name
		// admin_company_vat
		// admin_company_logo_file
		// admin_company_logo_width
		// admin_company_logo_height
		// admin_company_logo_size
		// admin_company_address
		// admin_company_region_id
		// admin_company_province_id
		// admin_company_city_id
		// admin_company_phone
		// admin_company_email
		// admin_company_web
		// admin_contract
		// admin_expire
		// admin_company_facebook
		// admin_company_twitter
		// admin_company_googleplus
		// admin_company_linkedin
		// admin_link_playstore
		// admin_link_appstore
		// admin_link_windowsstore
		// admin_level
		// admin_username
		// admin_password
		// admin_company_autohority
		// admin_auth_token
		// admin_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// admin_id
		$this->admin_id->ViewValue = $this->admin_id->CurrentValue;
		$this->admin_id->ViewCustomAttributes = "";

		// admin_name
		$this->admin_name->ViewValue = $this->admin_name->CurrentValue;
		$this->admin_name->ViewCustomAttributes = "";

		// admin_surname
		$this->admin_surname->ViewValue = $this->admin_surname->CurrentValue;
		$this->admin_surname->ViewCustomAttributes = "";

		// admin_gender
		if (strval($this->admin_gender->CurrentValue) <> "") {
			$this->admin_gender->ViewValue = $this->admin_gender->OptionCaption($this->admin_gender->CurrentValue);
		} else {
			$this->admin_gender->ViewValue = NULL;
		}
		$this->admin_gender->ViewCustomAttributes = "";

		// admin_email
		$this->admin_email->ViewValue = $this->admin_email->CurrentValue;
		$this->admin_email->ViewCustomAttributes = "";

		// admin_mobile
		$this->admin_mobile->ViewValue = $this->admin_mobile->CurrentValue;
		$this->admin_mobile->ViewCustomAttributes = "";

		// admin_phone
		$this->admin_phone->ViewValue = $this->admin_phone->CurrentValue;
		$this->admin_phone->ViewCustomAttributes = "";

		// admin_modules
		if (strval($this->admin_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->admin_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->admin_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->admin_modules->ViewValue .= $this->admin_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->admin_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->admin_modules->ViewValue = $this->admin_modules->CurrentValue;
			}
		} else {
			$this->admin_modules->ViewValue = NULL;
		}
		$this->admin_modules->ViewCustomAttributes = "";

		// admin_language
		if (strval($this->admin_language->CurrentValue) <> "") {
			$this->admin_language->ViewValue = $this->admin_language->OptionCaption($this->admin_language->CurrentValue);
		} else {
			$this->admin_language->ViewValue = NULL;
		}
		$this->admin_language->ViewCustomAttributes = "";

		// admin_is_active
		if (strval($this->admin_is_active->CurrentValue) <> "") {
			$this->admin_is_active->ViewValue = $this->admin_is_active->OptionCaption($this->admin_is_active->CurrentValue);
		} else {
			$this->admin_is_active->ViewValue = NULL;
		}
		$this->admin_is_active->ViewCustomAttributes = "";

		// admin_company_name
		$this->admin_company_name->ViewValue = $this->admin_company_name->CurrentValue;
		$this->admin_company_name->ViewCustomAttributes = "";

		// admin_company_vat
		$this->admin_company_vat->ViewValue = $this->admin_company_vat->CurrentValue;
		$this->admin_company_vat->ViewCustomAttributes = "";

		// admin_company_logo_file
		if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
			$this->admin_company_logo_file->ImageWidth = 100;
			$this->admin_company_logo_file->ImageHeight = 0;
			$this->admin_company_logo_file->ImageAlt = $this->admin_company_logo_file->FldAlt();
			$this->admin_company_logo_file->ViewValue = $this->admin_company_logo_file->Upload->DbValue;
		} else {
			$this->admin_company_logo_file->ViewValue = "";
		}
		$this->admin_company_logo_file->ViewCustomAttributes = "";

		// admin_company_address
		$this->admin_company_address->ViewValue = $this->admin_company_address->CurrentValue;
		$this->admin_company_address->ViewCustomAttributes = "";

		// admin_company_region_id
		if (strval($this->admin_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->admin_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->CurrentValue;
			}
		} else {
			$this->admin_company_region_id->ViewValue = NULL;
		}
		$this->admin_company_region_id->ViewCustomAttributes = "";

		// admin_company_province_id
		if (strval($this->admin_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->admin_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->CurrentValue;
			}
		} else {
			$this->admin_company_province_id->ViewValue = NULL;
		}
		$this->admin_company_province_id->ViewCustomAttributes = "";

		// admin_company_city_id
		if (strval($this->admin_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->admin_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->CurrentValue;
			}
		} else {
			$this->admin_company_city_id->ViewValue = NULL;
		}
		$this->admin_company_city_id->ViewCustomAttributes = "";

		// admin_company_phone
		$this->admin_company_phone->ViewValue = $this->admin_company_phone->CurrentValue;
		$this->admin_company_phone->ViewCustomAttributes = "";

		// admin_company_email
		$this->admin_company_email->ViewValue = $this->admin_company_email->CurrentValue;
		$this->admin_company_email->ViewCustomAttributes = "";

		// admin_company_web
		$this->admin_company_web->ViewValue = $this->admin_company_web->CurrentValue;
		$this->admin_company_web->ViewCustomAttributes = "";

		// admin_contract
		$this->admin_contract->ViewValue = $this->admin_contract->CurrentValue;
		$this->admin_contract->ViewValue = ew_FormatDateTime($this->admin_contract->ViewValue, 7);
		$this->admin_contract->ViewCustomAttributes = "";

		// admin_expire
		$this->admin_expire->ViewValue = $this->admin_expire->CurrentValue;
		$this->admin_expire->ViewValue = ew_FormatDateTime($this->admin_expire->ViewValue, 7);
		$this->admin_expire->ViewCustomAttributes = "";

		// admin_company_facebook
		$this->admin_company_facebook->ViewValue = $this->admin_company_facebook->CurrentValue;
		$this->admin_company_facebook->ViewCustomAttributes = "";

		// admin_company_twitter
		$this->admin_company_twitter->ViewValue = $this->admin_company_twitter->CurrentValue;
		$this->admin_company_twitter->ViewCustomAttributes = "";

		// admin_company_googleplus
		$this->admin_company_googleplus->ViewValue = $this->admin_company_googleplus->CurrentValue;
		$this->admin_company_googleplus->ViewCustomAttributes = "";

		// admin_company_linkedin
		$this->admin_company_linkedin->ViewValue = $this->admin_company_linkedin->CurrentValue;
		$this->admin_company_linkedin->ViewCustomAttributes = "";

		// admin_link_playstore
		$this->admin_link_playstore->ViewValue = $this->admin_link_playstore->CurrentValue;
		$this->admin_link_playstore->ViewCustomAttributes = "";

		// admin_link_appstore
		$this->admin_link_appstore->ViewValue = $this->admin_link_appstore->CurrentValue;
		$this->admin_link_appstore->ViewCustomAttributes = "";

		// admin_link_windowsstore
		$this->admin_link_windowsstore->ViewValue = $this->admin_link_windowsstore->CurrentValue;
		$this->admin_link_windowsstore->ViewCustomAttributes = "";

		// admin_level
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->admin_level->CurrentValue) <> "") {
			$this->admin_level->ViewValue = $this->admin_level->OptionCaption($this->admin_level->CurrentValue);
		} else {
			$this->admin_level->ViewValue = NULL;
		}
		} else {
			$this->admin_level->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->admin_level->ViewCustomAttributes = "";

		// admin_username
		$this->admin_username->ViewValue = $this->admin_username->CurrentValue;
		$this->admin_username->ViewCustomAttributes = "";

		// admin_password
		$this->admin_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->admin_password->ViewCustomAttributes = "";

		// admin_last_update
		$this->admin_last_update->ViewValue = $this->admin_last_update->CurrentValue;
		$this->admin_last_update->ViewValue = ew_FormatDateTime($this->admin_last_update->ViewValue, 7);
		$this->admin_last_update->ViewCustomAttributes = "";

			// admin_id
			$this->admin_id->LinkCustomAttributes = "";
			$this->admin_id->HrefValue = "";
			$this->admin_id->TooltipValue = "";

			// admin_name
			$this->admin_name->LinkCustomAttributes = "";
			$this->admin_name->HrefValue = "";
			$this->admin_name->TooltipValue = "";

			// admin_surname
			$this->admin_surname->LinkCustomAttributes = "";
			$this->admin_surname->HrefValue = "";
			$this->admin_surname->TooltipValue = "";

			// admin_gender
			$this->admin_gender->LinkCustomAttributes = "";
			$this->admin_gender->HrefValue = "";
			$this->admin_gender->TooltipValue = "";

			// admin_email
			$this->admin_email->LinkCustomAttributes = "";
			$this->admin_email->HrefValue = "";
			$this->admin_email->TooltipValue = "";

			// admin_mobile
			$this->admin_mobile->LinkCustomAttributes = "";
			$this->admin_mobile->HrefValue = "";
			$this->admin_mobile->TooltipValue = "";

			// admin_phone
			$this->admin_phone->LinkCustomAttributes = "";
			$this->admin_phone->HrefValue = "";
			$this->admin_phone->TooltipValue = "";

			// admin_modules
			$this->admin_modules->LinkCustomAttributes = "";
			$this->admin_modules->HrefValue = "";
			$this->admin_modules->TooltipValue = "";

			// admin_language
			$this->admin_language->LinkCustomAttributes = "";
			$this->admin_language->HrefValue = "";
			$this->admin_language->TooltipValue = "";

			// admin_is_active
			$this->admin_is_active->LinkCustomAttributes = "";
			$this->admin_is_active->HrefValue = "";
			$this->admin_is_active->TooltipValue = "";

			// admin_company_name
			$this->admin_company_name->LinkCustomAttributes = "";
			$this->admin_company_name->HrefValue = "";
			$this->admin_company_name->TooltipValue = "";

			// admin_company_vat
			$this->admin_company_vat->LinkCustomAttributes = "";
			$this->admin_company_vat->HrefValue = "";
			$this->admin_company_vat->TooltipValue = "";

			// admin_company_logo_file
			$this->admin_company_logo_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
				$this->admin_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->admin_company_logo_file, $this->admin_company_logo_file->Upload->DbValue); // Add prefix/suffix
				$this->admin_company_logo_file->LinkAttrs["target"] = ""; // Add target
				if ($this->Export <> "") $this->admin_company_logo_file->HrefValue = ew_ConvertFullUrl($this->admin_company_logo_file->HrefValue);
			} else {
				$this->admin_company_logo_file->HrefValue = "";
			}
			$this->admin_company_logo_file->HrefValue2 = $this->admin_company_logo_file->UploadPath . $this->admin_company_logo_file->Upload->DbValue;
			$this->admin_company_logo_file->TooltipValue = "";
			if ($this->admin_company_logo_file->UseColorbox) {
				$this->admin_company_logo_file->LinkAttrs["title"] = $Language->Phrase("ViewImageGallery");
				$this->admin_company_logo_file->LinkAttrs["data-rel"] = "telecare_admin_x_admin_company_logo_file";

				//$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox ewTooltip img-thumbnail";
				//$this->admin_company_logo_file->LinkAttrs["data-placement"] = "bottom";
				//$this->admin_company_logo_file->LinkAttrs["data-container"] = "body";

				$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox img-thumbnail";
			}

			// admin_company_address
			$this->admin_company_address->LinkCustomAttributes = "";
			$this->admin_company_address->HrefValue = "";
			$this->admin_company_address->TooltipValue = "";

			// admin_company_region_id
			$this->admin_company_region_id->LinkCustomAttributes = "";
			$this->admin_company_region_id->HrefValue = "";
			$this->admin_company_region_id->TooltipValue = "";

			// admin_company_province_id
			$this->admin_company_province_id->LinkCustomAttributes = "";
			$this->admin_company_province_id->HrefValue = "";
			$this->admin_company_province_id->TooltipValue = "";

			// admin_company_city_id
			$this->admin_company_city_id->LinkCustomAttributes = "";
			$this->admin_company_city_id->HrefValue = "";
			$this->admin_company_city_id->TooltipValue = "";

			// admin_company_phone
			$this->admin_company_phone->LinkCustomAttributes = "";
			$this->admin_company_phone->HrefValue = "";
			$this->admin_company_phone->TooltipValue = "";

			// admin_company_email
			$this->admin_company_email->LinkCustomAttributes = "";
			$this->admin_company_email->HrefValue = "";
			$this->admin_company_email->TooltipValue = "";

			// admin_company_web
			$this->admin_company_web->LinkCustomAttributes = "";
			$this->admin_company_web->HrefValue = "";
			$this->admin_company_web->TooltipValue = "";

			// admin_contract
			$this->admin_contract->LinkCustomAttributes = "";
			$this->admin_contract->HrefValue = "";
			$this->admin_contract->TooltipValue = "";

			// admin_expire
			$this->admin_expire->LinkCustomAttributes = "";
			$this->admin_expire->HrefValue = "";
			$this->admin_expire->TooltipValue = "";

			// admin_company_facebook
			$this->admin_company_facebook->LinkCustomAttributes = "";
			$this->admin_company_facebook->HrefValue = "";
			$this->admin_company_facebook->TooltipValue = "";

			// admin_company_twitter
			$this->admin_company_twitter->LinkCustomAttributes = "";
			$this->admin_company_twitter->HrefValue = "";
			$this->admin_company_twitter->TooltipValue = "";

			// admin_company_googleplus
			$this->admin_company_googleplus->LinkCustomAttributes = "";
			$this->admin_company_googleplus->HrefValue = "";
			$this->admin_company_googleplus->TooltipValue = "";

			// admin_company_linkedin
			$this->admin_company_linkedin->LinkCustomAttributes = "";
			$this->admin_company_linkedin->HrefValue = "";
			$this->admin_company_linkedin->TooltipValue = "";

			// admin_link_playstore
			$this->admin_link_playstore->LinkCustomAttributes = "";
			$this->admin_link_playstore->HrefValue = "";
			$this->admin_link_playstore->TooltipValue = "";

			// admin_link_appstore
			$this->admin_link_appstore->LinkCustomAttributes = "";
			$this->admin_link_appstore->HrefValue = "";
			$this->admin_link_appstore->TooltipValue = "";

			// admin_link_windowsstore
			$this->admin_link_windowsstore->LinkCustomAttributes = "";
			$this->admin_link_windowsstore->HrefValue = "";
			$this->admin_link_windowsstore->TooltipValue = "";

			// admin_level
			$this->admin_level->LinkCustomAttributes = "";
			$this->admin_level->HrefValue = "";
			$this->admin_level->TooltipValue = "";

			// admin_username
			$this->admin_username->LinkCustomAttributes = "";
			$this->admin_username->HrefValue = "";
			$this->admin_username->TooltipValue = "";

			// admin_password
			$this->admin_password->LinkCustomAttributes = "";
			$this->admin_password->HrefValue = "";
			$this->admin_password->TooltipValue = "";

			// admin_last_update
			$this->admin_last_update->LinkCustomAttributes = "";
			$this->admin_last_update->HrefValue = "";
			$this->admin_last_update->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = TRUE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_telecare_admin\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_telecare_admin',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.ftelecare_adminview,key:" . ew_ArrayToJsonAttr($this->RecKey) . ",sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide options for export
		if ($this->Export <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = FALSE;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;
		$this->SetUpStartRec(); // Set up start record position

		// Set the last record to display
		if ($this->DisplayRecs <= 0) {
			$this->StopRec = $this->TotalRecs;
		} else {
			$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
		}
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "v");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "view");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->admin_id->CurrentValue);
		return TRUE;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_adminlist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$pages->Add(4);
		$this->MultiPages = $pages;
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_admin_view)) $telecare_admin_view = new ctelecare_admin_view();

// Page init
$telecare_admin_view->Page_Init();

// Page main
$telecare_admin_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_admin_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($telecare_admin->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = ftelecare_adminview = new ew_Form("ftelecare_adminview", "view");

// Form_CustomValidate event
ftelecare_adminview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_adminview.ValidateRequired = true;
<?php } else { ?>
ftelecare_adminview.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_adminview.MultiPage = new ew_MultiPage("ftelecare_adminview");

// Dynamic selection lists
ftelecare_adminview.Lists["x_admin_gender"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_gender"].Options = <?php echo json_encode($telecare_admin->admin_gender->Options()) ?>;
ftelecare_adminview.Lists["x_admin_modules[]"] = {"LinkField":"x_invalidity_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_invalidity_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_language"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_language"].Options = <?php echo json_encode($telecare_admin->admin_language->Options()) ?>;
ftelecare_adminview.Lists["x_admin_is_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_is_active"].Options = <?php echo json_encode($telecare_admin->admin_is_active->Options()) ?>;
ftelecare_adminview.Lists["x_admin_company_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_admin_company_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_company_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":[],"ChildFields":["x_admin_company_city_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_company_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_level"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminview.Lists["x_admin_level"].Options = <?php echo json_encode($telecare_admin->admin_level->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
<div class="ewToolbar">
<?php if ($telecare_admin->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php $telecare_admin_view->ExportOptions->Render("body") ?>
<?php
	foreach ($telecare_admin_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php if ($telecare_admin->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $telecare_admin_view->ShowPageHeader(); ?>
<?php
$telecare_admin_view->ShowMessage();
?>
<?php if ($telecare_admin->Export == "") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_admin_view->Pager)) $telecare_admin_view->Pager = new cPrevNextPager($telecare_admin_view->StartRec, $telecare_admin_view->DisplayRecs, $telecare_admin_view->TotalRecs) ?>
<?php if ($telecare_admin_view->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_admin_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_admin_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_admin_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_admin_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_admin_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_admin_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
</form>
<?php } ?>
<form name="ftelecare_adminview" id="ftelecare_adminview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_admin_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_admin_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_admin">
<?php if ($telecare_admin->Export == "") { ?>
<div class="ewMultiPage">
<div class="tabbable" id="telecare_admin_view">
	<ul class="nav<?php echo $telecare_admin_view->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_admin_view->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_admin1" data-toggle="tab"><?php echo $telecare_admin->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_admin_view->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_admin2" data-toggle="tab"><?php echo $telecare_admin->PageCaption(2) ?></a></li>
		<li<?php echo $telecare_admin_view->MultiPages->TabStyle("3") ?>><a href="#tab_telecare_admin3" data-toggle="tab"><?php echo $telecare_admin->PageCaption(3) ?></a></li>
		<li<?php echo $telecare_admin_view->MultiPages->TabStyle("4") ?>><a href="#tab_telecare_admin4" data-toggle="tab"><?php echo $telecare_admin->PageCaption(4) ?></a></li>
	</ul>
	<div class="tab-content">
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_admin_view->MultiPages->PageStyle("1") ?>" id="tab_telecare_admin1">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_admin->admin_id->Visible) { // admin_id ?>
	<tr id="r_admin_id">
		<td><span id="elh_telecare_admin_admin_id"><?php echo $telecare_admin->admin_id->FldCaption() ?></span></td>
		<td data-name="admin_id"<?php echo $telecare_admin->admin_id->CellAttributes() ?>>
<span id="el_telecare_admin_admin_id" data-page="1">
<span<?php echo $telecare_admin->admin_id->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_name->Visible) { // admin_name ?>
	<tr id="r_admin_name">
		<td><span id="elh_telecare_admin_admin_name"><?php echo $telecare_admin->admin_name->FldCaption() ?></span></td>
		<td data-name="admin_name"<?php echo $telecare_admin->admin_name->CellAttributes() ?>>
<span id="el_telecare_admin_admin_name" data-page="1">
<span<?php echo $telecare_admin->admin_name->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_name->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_surname->Visible) { // admin_surname ?>
	<tr id="r_admin_surname">
		<td><span id="elh_telecare_admin_admin_surname"><?php echo $telecare_admin->admin_surname->FldCaption() ?></span></td>
		<td data-name="admin_surname"<?php echo $telecare_admin->admin_surname->CellAttributes() ?>>
<span id="el_telecare_admin_admin_surname" data-page="1">
<span<?php echo $telecare_admin->admin_surname->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_surname->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_gender->Visible) { // admin_gender ?>
	<tr id="r_admin_gender">
		<td><span id="elh_telecare_admin_admin_gender"><?php echo $telecare_admin->admin_gender->FldCaption() ?></span></td>
		<td data-name="admin_gender"<?php echo $telecare_admin->admin_gender->CellAttributes() ?>>
<span id="el_telecare_admin_admin_gender" data-page="1">
<span<?php echo $telecare_admin->admin_gender->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_gender->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_email->Visible) { // admin_email ?>
	<tr id="r_admin_email">
		<td><span id="elh_telecare_admin_admin_email"><?php echo $telecare_admin->admin_email->FldCaption() ?></span></td>
		<td data-name="admin_email"<?php echo $telecare_admin->admin_email->CellAttributes() ?>>
<span id="el_telecare_admin_admin_email" data-page="1">
<span<?php echo $telecare_admin->admin_email->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_email->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_mobile->Visible) { // admin_mobile ?>
	<tr id="r_admin_mobile">
		<td><span id="elh_telecare_admin_admin_mobile"><?php echo $telecare_admin->admin_mobile->FldCaption() ?></span></td>
		<td data-name="admin_mobile"<?php echo $telecare_admin->admin_mobile->CellAttributes() ?>>
<span id="el_telecare_admin_admin_mobile" data-page="1">
<span<?php echo $telecare_admin->admin_mobile->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_mobile->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_phone->Visible) { // admin_phone ?>
	<tr id="r_admin_phone">
		<td><span id="elh_telecare_admin_admin_phone"><?php echo $telecare_admin->admin_phone->FldCaption() ?></span></td>
		<td data-name="admin_phone"<?php echo $telecare_admin->admin_phone->CellAttributes() ?>>
<span id="el_telecare_admin_admin_phone" data-page="1">
<span<?php echo $telecare_admin->admin_phone->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_phone->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_modules->Visible) { // admin_modules ?>
	<tr id="r_admin_modules">
		<td><span id="elh_telecare_admin_admin_modules"><?php echo $telecare_admin->admin_modules->FldCaption() ?></span></td>
		<td data-name="admin_modules"<?php echo $telecare_admin->admin_modules->CellAttributes() ?>>
<span id="el_telecare_admin_admin_modules" data-page="1">
<span<?php echo $telecare_admin->admin_modules->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_modules->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_language->Visible) { // admin_language ?>
	<tr id="r_admin_language">
		<td><span id="elh_telecare_admin_admin_language"><?php echo $telecare_admin->admin_language->FldCaption() ?></span></td>
		<td data-name="admin_language"<?php echo $telecare_admin->admin_language->CellAttributes() ?>>
<span id="el_telecare_admin_admin_language" data-page="1">
<span<?php echo $telecare_admin->admin_language->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_language->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_is_active->Visible) { // admin_is_active ?>
	<tr id="r_admin_is_active">
		<td><span id="elh_telecare_admin_admin_is_active"><?php echo $telecare_admin->admin_is_active->FldCaption() ?></span></td>
		<td data-name="admin_is_active"<?php echo $telecare_admin->admin_is_active->CellAttributes() ?>>
<span id="el_telecare_admin_admin_is_active" data-page="1">
<span<?php echo $telecare_admin->admin_is_active->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_is_active->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_last_update->Visible) { // admin_last_update ?>
	<tr id="r_admin_last_update">
		<td><span id="elh_telecare_admin_admin_last_update"><?php echo $telecare_admin->admin_last_update->FldCaption() ?></span></td>
		<td data-name="admin_last_update"<?php echo $telecare_admin->admin_last_update->CellAttributes() ?>>
<span id="el_telecare_admin_admin_last_update" data-page="1">
<span<?php echo $telecare_admin->admin_last_update->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_last_update->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_admin->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_admin_view->MultiPages->PageStyle("2") ?>" id="tab_telecare_admin2">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_admin->admin_company_name->Visible) { // admin_company_name ?>
	<tr id="r_admin_company_name">
		<td><span id="elh_telecare_admin_admin_company_name"><?php echo $telecare_admin->admin_company_name->FldCaption() ?></span></td>
		<td data-name="admin_company_name"<?php echo $telecare_admin->admin_company_name->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_name" data-page="2">
<span<?php echo $telecare_admin->admin_company_name->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_name->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_vat->Visible) { // admin_company_vat ?>
	<tr id="r_admin_company_vat">
		<td><span id="elh_telecare_admin_admin_company_vat"><?php echo $telecare_admin->admin_company_vat->FldCaption() ?></span></td>
		<td data-name="admin_company_vat"<?php echo $telecare_admin->admin_company_vat->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_vat" data-page="2">
<span<?php echo $telecare_admin->admin_company_vat->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_vat->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_logo_file->Visible) { // admin_company_logo_file ?>
	<tr id="r_admin_company_logo_file">
		<td><span id="elh_telecare_admin_admin_company_logo_file"><?php echo $telecare_admin->admin_company_logo_file->FldCaption() ?></span></td>
		<td data-name="admin_company_logo_file"<?php echo $telecare_admin->admin_company_logo_file->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_logo_file" data-page="2">
<span>
<?php echo ew_GetFileViewTag($telecare_admin->admin_company_logo_file, $telecare_admin->admin_company_logo_file->ViewValue) ?>
</span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_address->Visible) { // admin_company_address ?>
	<tr id="r_admin_company_address">
		<td><span id="elh_telecare_admin_admin_company_address"><?php echo $telecare_admin->admin_company_address->FldCaption() ?></span></td>
		<td data-name="admin_company_address"<?php echo $telecare_admin->admin_company_address->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_address" data-page="2">
<span<?php echo $telecare_admin->admin_company_address->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_address->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_region_id->Visible) { // admin_company_region_id ?>
	<tr id="r_admin_company_region_id">
		<td><span id="elh_telecare_admin_admin_company_region_id"><?php echo $telecare_admin->admin_company_region_id->FldCaption() ?></span></td>
		<td data-name="admin_company_region_id"<?php echo $telecare_admin->admin_company_region_id->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_region_id" data-page="2">
<span<?php echo $telecare_admin->admin_company_region_id->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_region_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_province_id->Visible) { // admin_company_province_id ?>
	<tr id="r_admin_company_province_id">
		<td><span id="elh_telecare_admin_admin_company_province_id"><?php echo $telecare_admin->admin_company_province_id->FldCaption() ?></span></td>
		<td data-name="admin_company_province_id"<?php echo $telecare_admin->admin_company_province_id->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_province_id" data-page="2">
<span<?php echo $telecare_admin->admin_company_province_id->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_province_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_city_id->Visible) { // admin_company_city_id ?>
	<tr id="r_admin_company_city_id">
		<td><span id="elh_telecare_admin_admin_company_city_id"><?php echo $telecare_admin->admin_company_city_id->FldCaption() ?></span></td>
		<td data-name="admin_company_city_id"<?php echo $telecare_admin->admin_company_city_id->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_city_id" data-page="2">
<span<?php echo $telecare_admin->admin_company_city_id->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_city_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_phone->Visible) { // admin_company_phone ?>
	<tr id="r_admin_company_phone">
		<td><span id="elh_telecare_admin_admin_company_phone"><?php echo $telecare_admin->admin_company_phone->FldCaption() ?></span></td>
		<td data-name="admin_company_phone"<?php echo $telecare_admin->admin_company_phone->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_phone" data-page="2">
<span<?php echo $telecare_admin->admin_company_phone->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_phone->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_email->Visible) { // admin_company_email ?>
	<tr id="r_admin_company_email">
		<td><span id="elh_telecare_admin_admin_company_email"><?php echo $telecare_admin->admin_company_email->FldCaption() ?></span></td>
		<td data-name="admin_company_email"<?php echo $telecare_admin->admin_company_email->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_email" data-page="2">
<span<?php echo $telecare_admin->admin_company_email->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_email->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_web->Visible) { // admin_company_web ?>
	<tr id="r_admin_company_web">
		<td><span id="elh_telecare_admin_admin_company_web"><?php echo $telecare_admin->admin_company_web->FldCaption() ?></span></td>
		<td data-name="admin_company_web"<?php echo $telecare_admin->admin_company_web->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_web" data-page="2">
<span<?php echo $telecare_admin->admin_company_web->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_web->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_contract->Visible) { // admin_contract ?>
	<tr id="r_admin_contract">
		<td><span id="elh_telecare_admin_admin_contract"><?php echo $telecare_admin->admin_contract->FldCaption() ?></span></td>
		<td data-name="admin_contract"<?php echo $telecare_admin->admin_contract->CellAttributes() ?>>
<span id="el_telecare_admin_admin_contract" data-page="2">
<span<?php echo $telecare_admin->admin_contract->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_contract->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_expire->Visible) { // admin_expire ?>
	<tr id="r_admin_expire">
		<td><span id="elh_telecare_admin_admin_expire"><?php echo $telecare_admin->admin_expire->FldCaption() ?></span></td>
		<td data-name="admin_expire"<?php echo $telecare_admin->admin_expire->CellAttributes() ?>>
<span id="el_telecare_admin_admin_expire" data-page="2">
<span<?php echo $telecare_admin->admin_expire->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_expire->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_admin->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_admin_view->MultiPages->PageStyle("3") ?>" id="tab_telecare_admin3">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_admin->admin_company_facebook->Visible) { // admin_company_facebook ?>
	<tr id="r_admin_company_facebook">
		<td><span id="elh_telecare_admin_admin_company_facebook"><?php echo $telecare_admin->admin_company_facebook->FldCaption() ?></span></td>
		<td data-name="admin_company_facebook"<?php echo $telecare_admin->admin_company_facebook->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_facebook" data-page="3">
<span<?php echo $telecare_admin->admin_company_facebook->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_facebook->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_twitter->Visible) { // admin_company_twitter ?>
	<tr id="r_admin_company_twitter">
		<td><span id="elh_telecare_admin_admin_company_twitter"><?php echo $telecare_admin->admin_company_twitter->FldCaption() ?></span></td>
		<td data-name="admin_company_twitter"<?php echo $telecare_admin->admin_company_twitter->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_twitter" data-page="3">
<span<?php echo $telecare_admin->admin_company_twitter->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_twitter->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_googleplus->Visible) { // admin_company_googleplus ?>
	<tr id="r_admin_company_googleplus">
		<td><span id="elh_telecare_admin_admin_company_googleplus"><?php echo $telecare_admin->admin_company_googleplus->FldCaption() ?></span></td>
		<td data-name="admin_company_googleplus"<?php echo $telecare_admin->admin_company_googleplus->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_googleplus" data-page="3">
<span<?php echo $telecare_admin->admin_company_googleplus->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_googleplus->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_company_linkedin->Visible) { // admin_company_linkedin ?>
	<tr id="r_admin_company_linkedin">
		<td><span id="elh_telecare_admin_admin_company_linkedin"><?php echo $telecare_admin->admin_company_linkedin->FldCaption() ?></span></td>
		<td data-name="admin_company_linkedin"<?php echo $telecare_admin->admin_company_linkedin->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_linkedin" data-page="3">
<span<?php echo $telecare_admin->admin_company_linkedin->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_company_linkedin->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_link_playstore->Visible) { // admin_link_playstore ?>
	<tr id="r_admin_link_playstore">
		<td><span id="elh_telecare_admin_admin_link_playstore"><?php echo $telecare_admin->admin_link_playstore->FldCaption() ?></span></td>
		<td data-name="admin_link_playstore"<?php echo $telecare_admin->admin_link_playstore->CellAttributes() ?>>
<span id="el_telecare_admin_admin_link_playstore" data-page="3">
<span<?php echo $telecare_admin->admin_link_playstore->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_link_playstore->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_link_appstore->Visible) { // admin_link_appstore ?>
	<tr id="r_admin_link_appstore">
		<td><span id="elh_telecare_admin_admin_link_appstore"><?php echo $telecare_admin->admin_link_appstore->FldCaption() ?></span></td>
		<td data-name="admin_link_appstore"<?php echo $telecare_admin->admin_link_appstore->CellAttributes() ?>>
<span id="el_telecare_admin_admin_link_appstore" data-page="3">
<span<?php echo $telecare_admin->admin_link_appstore->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_link_appstore->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_link_windowsstore->Visible) { // admin_link_windowsstore ?>
	<tr id="r_admin_link_windowsstore">
		<td><span id="elh_telecare_admin_admin_link_windowsstore"><?php echo $telecare_admin->admin_link_windowsstore->FldCaption() ?></span></td>
		<td data-name="admin_link_windowsstore"<?php echo $telecare_admin->admin_link_windowsstore->CellAttributes() ?>>
<span id="el_telecare_admin_admin_link_windowsstore" data-page="3">
<span<?php echo $telecare_admin->admin_link_windowsstore->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_link_windowsstore->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_admin->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_admin_view->MultiPages->PageStyle("4") ?>" id="tab_telecare_admin4">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_admin->admin_level->Visible) { // admin_level ?>
	<tr id="r_admin_level">
		<td><span id="elh_telecare_admin_admin_level"><?php echo $telecare_admin->admin_level->FldCaption() ?></span></td>
		<td data-name="admin_level"<?php echo $telecare_admin->admin_level->CellAttributes() ?>>
<span id="el_telecare_admin_admin_level" data-page="4">
<span<?php echo $telecare_admin->admin_level->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_level->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_username->Visible) { // admin_username ?>
	<tr id="r_admin_username">
		<td><span id="elh_telecare_admin_admin_username"><?php echo $telecare_admin->admin_username->FldCaption() ?></span></td>
		<td data-name="admin_username"<?php echo $telecare_admin->admin_username->CellAttributes() ?>>
<span id="el_telecare_admin_admin_username" data-page="4">
<span<?php echo $telecare_admin->admin_username->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_username->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_admin->admin_password->Visible) { // admin_password ?>
	<tr id="r_admin_password">
		<td><span id="elh_telecare_admin_admin_password"><?php echo $telecare_admin->admin_password->FldCaption() ?></span></td>
		<td data-name="admin_password"<?php echo $telecare_admin->admin_password->CellAttributes() ?>>
<span id="el_telecare_admin_admin_password" data-page="4">
<span<?php echo $telecare_admin->admin_password->ViewAttributes() ?>>
<?php echo $telecare_admin->admin_password->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_admin->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
	</div>
</div>
</div>
<?php } ?>
<?php if ($telecare_admin->Export == "") { ?>
<?php if (!isset($telecare_admin_view->Pager)) $telecare_admin_view->Pager = new cPrevNextPager($telecare_admin_view->StartRec, $telecare_admin_view->DisplayRecs, $telecare_admin_view->TotalRecs) ?>
<?php if ($telecare_admin_view->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_admin_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_admin_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_admin_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_admin_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_admin_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_admin_view->PageUrl() ?>start=<?php echo $telecare_admin_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_admin_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php } ?>
</form>
<script type="text/javascript">
ftelecare_adminview.Init();
</script>
<?php
$telecare_admin_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($telecare_admin->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$telecare_admin_view->Page_Terminate();
?>
