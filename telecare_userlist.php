<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_callgridcls.php" ?>
<?php include_once "telecare_alarmgridcls.php" ?>
<?php include_once "telecare_prescriptiongridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_user_list = NULL; // Initialize page object first

class ctelecare_user_list extends ctelecare_user {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_user';

	// Page object name
	var $PageObjName = 'telecare_user_list';

	// Grid form hidden field names
	var $FormName = 'ftelecare_userlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;
    var $AuditTrailOnAdd = TRUE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_user)
		if (!isset($GLOBALS["telecare_user"]) || get_class($GLOBALS["telecare_user"]) == "ctelecare_user") {
			$GLOBALS["telecare_user"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_user"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "telecare_useradd.php?" . EW_TABLE_SHOW_DETAIL . "=";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "telecare_userdelete.php";
		$this->MultiUpdateUrl = "telecare_userupdate.php";

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_user', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption ftelecare_userlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->user_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'telecare_call'
			if (@$_POST["grid"] == "ftelecare_callgrid") {
				if (!isset($GLOBALS["telecare_call_grid"])) $GLOBALS["telecare_call_grid"] = new ctelecare_call_grid;
				$GLOBALS["telecare_call_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'telecare_alarm'
			if (@$_POST["grid"] == "ftelecare_alarmgrid") {
				if (!isset($GLOBALS["telecare_alarm_grid"])) $GLOBALS["telecare_alarm_grid"] = new ctelecare_alarm_grid;
				$GLOBALS["telecare_alarm_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'telecare_prescription'
			if (@$_POST["grid"] == "ftelecare_prescriptiongrid") {
				if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;
				$GLOBALS["telecare_prescription_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_user;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_user);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to grid add mode
				if ($this->CurrentAction == "gridadd")
					$this->GridAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Grid Insert
					if ($this->CurrentAction == "gridinsert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "gridadd") {
						if ($this->ValidateGridForm()) {
							$bGridInsert = $this->GridInsert();
						} else {
							$bGridInsert = FALSE;
							$this->setFailureMessage($gsFormError);
						}
						if (!$bGridInsert) {
							$this->EventCancelled = TRUE;
							$this->CurrentAction = "gridadd"; // Stay in Grid Add mode
						}
					}
				}
			}

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Restore filter list
			$this->RestoreFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->user_id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->user_id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Begin transaction
		$conn->BeginTrans();

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		if ($this->AuditTrailOnAdd) $this->WriteAuditTrailDummy($Language->Phrase("BatchInsertBegin")); // Batch insert begin
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->user_id->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->setFailureMessage($Language->Phrase("NoAddRecord"));
			$bGridInsert = FALSE;
		}
		if ($bGridInsert) {
			$conn->CommitTrans(); // Commit transaction

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			if ($this->AuditTrailOnAdd) $this->WriteAuditTrailDummy($Language->Phrase("BatchInsertSuccess")); // Batch insert success
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("InsertSuccess")); // Set up insert success message
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			$conn->RollbackTrans(); // Rollback transaction
			if ($this->AuditTrailOnAdd) $this->WriteAuditTrailDummy($Language->Phrase("BatchInsertRollback")); // Batch insert rollback
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_user_surname") && $objForm->HasValue("o_user_surname") && $this->user_surname->CurrentValue <> $this->user_surname->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_user_name") && $objForm->HasValue("o_user_name") && $this->user_name->CurrentValue <> $this->user_name->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_user_gender") && $objForm->HasValue("o_user_gender") && $this->user_gender->CurrentValue <> $this->user_gender->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_user_born") && $objForm->HasValue("o_user_born") && $this->user_born->CurrentValue <> $this->user_born->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_user_doctor_id") && $objForm->HasValue("o_user_doctor_id") && $this->user_doctor_id->CurrentValue <> $this->user_doctor_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_user_city_id") && $objForm->HasValue("o_user_city_id") && $this->user_city_id->CurrentValue <> $this->user_city_id->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->user_id->AdvancedSearch->ToJSON(), ","); // Field user_id
		$sFilterList = ew_Concat($sFilterList, $this->user_surname->AdvancedSearch->ToJSON(), ","); // Field user_surname
		$sFilterList = ew_Concat($sFilterList, $this->user_name->AdvancedSearch->ToJSON(), ","); // Field user_name
		$sFilterList = ew_Concat($sFilterList, $this->user_gender->AdvancedSearch->ToJSON(), ","); // Field user_gender
		$sFilterList = ew_Concat($sFilterList, $this->user_born->AdvancedSearch->ToJSON(), ","); // Field user_born
		$sFilterList = ew_Concat($sFilterList, $this->user_modules->AdvancedSearch->ToJSON(), ","); // Field user_modules
		$sFilterList = ew_Concat($sFilterList, $this->user_doctor_id->AdvancedSearch->ToJSON(), ","); // Field user_doctor_id
		$sFilterList = ew_Concat($sFilterList, $this->user_is_active->AdvancedSearch->ToJSON(), ","); // Field user_is_active
		$sFilterList = ew_Concat($sFilterList, $this->user_medical_history->AdvancedSearch->ToJSON(), ","); // Field user_medical_history
		$sFilterList = ew_Concat($sFilterList, $this->user_address->AdvancedSearch->ToJSON(), ","); // Field user_address
		$sFilterList = ew_Concat($sFilterList, $this->user_region_id->AdvancedSearch->ToJSON(), ","); // Field user_region_id
		$sFilterList = ew_Concat($sFilterList, $this->user_province_id->AdvancedSearch->ToJSON(), ","); // Field user_province_id
		$sFilterList = ew_Concat($sFilterList, $this->user_city_id->AdvancedSearch->ToJSON(), ","); // Field user_city_id
		$sFilterList = ew_Concat($sFilterList, $this->user_email->AdvancedSearch->ToJSON(), ","); // Field user_email
		$sFilterList = ew_Concat($sFilterList, $this->user_phone->AdvancedSearch->ToJSON(), ","); // Field user_phone
		$sFilterList = ew_Concat($sFilterList, $this->user_language->AdvancedSearch->ToJSON(), ","); // Field user_language
		$sFilterList = ew_Concat($sFilterList, $this->user_username->AdvancedSearch->ToJSON(), ","); // Field user_username
		$sFilterList = ew_Concat($sFilterList, $this->user_password->AdvancedSearch->ToJSON(), ","); // Field user_password
		$sFilterList = ew_Concat($sFilterList, $this->user_latitude->AdvancedSearch->ToJSON(), ","); // Field user_latitude
		$sFilterList = ew_Concat($sFilterList, $this->user_longitude->AdvancedSearch->ToJSON(), ","); // Field user_longitude
		$sFilterList = ew_Concat($sFilterList, $this->user_last_update->AdvancedSearch->ToJSON(), ","); // Field user_last_update
		$sFilterList = ew_Concat($sFilterList, $this->user_admin_id->AdvancedSearch->ToJSON(), ","); // Field user_admin_id
		$sFilterList = ew_Concat($sFilterList, $this->user_level->AdvancedSearch->ToJSON(), ","); // Field user_level
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field user_id
		$this->user_id->AdvancedSearch->SearchValue = @$filter["x_user_id"];
		$this->user_id->AdvancedSearch->SearchOperator = @$filter["z_user_id"];
		$this->user_id->AdvancedSearch->SearchCondition = @$filter["v_user_id"];
		$this->user_id->AdvancedSearch->SearchValue2 = @$filter["y_user_id"];
		$this->user_id->AdvancedSearch->SearchOperator2 = @$filter["w_user_id"];
		$this->user_id->AdvancedSearch->Save();

		// Field user_surname
		$this->user_surname->AdvancedSearch->SearchValue = @$filter["x_user_surname"];
		$this->user_surname->AdvancedSearch->SearchOperator = @$filter["z_user_surname"];
		$this->user_surname->AdvancedSearch->SearchCondition = @$filter["v_user_surname"];
		$this->user_surname->AdvancedSearch->SearchValue2 = @$filter["y_user_surname"];
		$this->user_surname->AdvancedSearch->SearchOperator2 = @$filter["w_user_surname"];
		$this->user_surname->AdvancedSearch->Save();

		// Field user_name
		$this->user_name->AdvancedSearch->SearchValue = @$filter["x_user_name"];
		$this->user_name->AdvancedSearch->SearchOperator = @$filter["z_user_name"];
		$this->user_name->AdvancedSearch->SearchCondition = @$filter["v_user_name"];
		$this->user_name->AdvancedSearch->SearchValue2 = @$filter["y_user_name"];
		$this->user_name->AdvancedSearch->SearchOperator2 = @$filter["w_user_name"];
		$this->user_name->AdvancedSearch->Save();

		// Field user_gender
		$this->user_gender->AdvancedSearch->SearchValue = @$filter["x_user_gender"];
		$this->user_gender->AdvancedSearch->SearchOperator = @$filter["z_user_gender"];
		$this->user_gender->AdvancedSearch->SearchCondition = @$filter["v_user_gender"];
		$this->user_gender->AdvancedSearch->SearchValue2 = @$filter["y_user_gender"];
		$this->user_gender->AdvancedSearch->SearchOperator2 = @$filter["w_user_gender"];
		$this->user_gender->AdvancedSearch->Save();

		// Field user_born
		$this->user_born->AdvancedSearch->SearchValue = @$filter["x_user_born"];
		$this->user_born->AdvancedSearch->SearchOperator = @$filter["z_user_born"];
		$this->user_born->AdvancedSearch->SearchCondition = @$filter["v_user_born"];
		$this->user_born->AdvancedSearch->SearchValue2 = @$filter["y_user_born"];
		$this->user_born->AdvancedSearch->SearchOperator2 = @$filter["w_user_born"];
		$this->user_born->AdvancedSearch->Save();

		// Field user_modules
		$this->user_modules->AdvancedSearch->SearchValue = @$filter["x_user_modules"];
		$this->user_modules->AdvancedSearch->SearchOperator = @$filter["z_user_modules"];
		$this->user_modules->AdvancedSearch->SearchCondition = @$filter["v_user_modules"];
		$this->user_modules->AdvancedSearch->SearchValue2 = @$filter["y_user_modules"];
		$this->user_modules->AdvancedSearch->SearchOperator2 = @$filter["w_user_modules"];
		$this->user_modules->AdvancedSearch->Save();

		// Field user_doctor_id
		$this->user_doctor_id->AdvancedSearch->SearchValue = @$filter["x_user_doctor_id"];
		$this->user_doctor_id->AdvancedSearch->SearchOperator = @$filter["z_user_doctor_id"];
		$this->user_doctor_id->AdvancedSearch->SearchCondition = @$filter["v_user_doctor_id"];
		$this->user_doctor_id->AdvancedSearch->SearchValue2 = @$filter["y_user_doctor_id"];
		$this->user_doctor_id->AdvancedSearch->SearchOperator2 = @$filter["w_user_doctor_id"];
		$this->user_doctor_id->AdvancedSearch->Save();

		// Field user_is_active
		$this->user_is_active->AdvancedSearch->SearchValue = @$filter["x_user_is_active"];
		$this->user_is_active->AdvancedSearch->SearchOperator = @$filter["z_user_is_active"];
		$this->user_is_active->AdvancedSearch->SearchCondition = @$filter["v_user_is_active"];
		$this->user_is_active->AdvancedSearch->SearchValue2 = @$filter["y_user_is_active"];
		$this->user_is_active->AdvancedSearch->SearchOperator2 = @$filter["w_user_is_active"];
		$this->user_is_active->AdvancedSearch->Save();

		// Field user_medical_history
		$this->user_medical_history->AdvancedSearch->SearchValue = @$filter["x_user_medical_history"];
		$this->user_medical_history->AdvancedSearch->SearchOperator = @$filter["z_user_medical_history"];
		$this->user_medical_history->AdvancedSearch->SearchCondition = @$filter["v_user_medical_history"];
		$this->user_medical_history->AdvancedSearch->SearchValue2 = @$filter["y_user_medical_history"];
		$this->user_medical_history->AdvancedSearch->SearchOperator2 = @$filter["w_user_medical_history"];
		$this->user_medical_history->AdvancedSearch->Save();

		// Field user_address
		$this->user_address->AdvancedSearch->SearchValue = @$filter["x_user_address"];
		$this->user_address->AdvancedSearch->SearchOperator = @$filter["z_user_address"];
		$this->user_address->AdvancedSearch->SearchCondition = @$filter["v_user_address"];
		$this->user_address->AdvancedSearch->SearchValue2 = @$filter["y_user_address"];
		$this->user_address->AdvancedSearch->SearchOperator2 = @$filter["w_user_address"];
		$this->user_address->AdvancedSearch->Save();

		// Field user_region_id
		$this->user_region_id->AdvancedSearch->SearchValue = @$filter["x_user_region_id"];
		$this->user_region_id->AdvancedSearch->SearchOperator = @$filter["z_user_region_id"];
		$this->user_region_id->AdvancedSearch->SearchCondition = @$filter["v_user_region_id"];
		$this->user_region_id->AdvancedSearch->SearchValue2 = @$filter["y_user_region_id"];
		$this->user_region_id->AdvancedSearch->SearchOperator2 = @$filter["w_user_region_id"];
		$this->user_region_id->AdvancedSearch->Save();

		// Field user_province_id
		$this->user_province_id->AdvancedSearch->SearchValue = @$filter["x_user_province_id"];
		$this->user_province_id->AdvancedSearch->SearchOperator = @$filter["z_user_province_id"];
		$this->user_province_id->AdvancedSearch->SearchCondition = @$filter["v_user_province_id"];
		$this->user_province_id->AdvancedSearch->SearchValue2 = @$filter["y_user_province_id"];
		$this->user_province_id->AdvancedSearch->SearchOperator2 = @$filter["w_user_province_id"];
		$this->user_province_id->AdvancedSearch->Save();

		// Field user_city_id
		$this->user_city_id->AdvancedSearch->SearchValue = @$filter["x_user_city_id"];
		$this->user_city_id->AdvancedSearch->SearchOperator = @$filter["z_user_city_id"];
		$this->user_city_id->AdvancedSearch->SearchCondition = @$filter["v_user_city_id"];
		$this->user_city_id->AdvancedSearch->SearchValue2 = @$filter["y_user_city_id"];
		$this->user_city_id->AdvancedSearch->SearchOperator2 = @$filter["w_user_city_id"];
		$this->user_city_id->AdvancedSearch->Save();

		// Field user_email
		$this->user_email->AdvancedSearch->SearchValue = @$filter["x_user_email"];
		$this->user_email->AdvancedSearch->SearchOperator = @$filter["z_user_email"];
		$this->user_email->AdvancedSearch->SearchCondition = @$filter["v_user_email"];
		$this->user_email->AdvancedSearch->SearchValue2 = @$filter["y_user_email"];
		$this->user_email->AdvancedSearch->SearchOperator2 = @$filter["w_user_email"];
		$this->user_email->AdvancedSearch->Save();

		// Field user_phone
		$this->user_phone->AdvancedSearch->SearchValue = @$filter["x_user_phone"];
		$this->user_phone->AdvancedSearch->SearchOperator = @$filter["z_user_phone"];
		$this->user_phone->AdvancedSearch->SearchCondition = @$filter["v_user_phone"];
		$this->user_phone->AdvancedSearch->SearchValue2 = @$filter["y_user_phone"];
		$this->user_phone->AdvancedSearch->SearchOperator2 = @$filter["w_user_phone"];
		$this->user_phone->AdvancedSearch->Save();

		// Field user_language
		$this->user_language->AdvancedSearch->SearchValue = @$filter["x_user_language"];
		$this->user_language->AdvancedSearch->SearchOperator = @$filter["z_user_language"];
		$this->user_language->AdvancedSearch->SearchCondition = @$filter["v_user_language"];
		$this->user_language->AdvancedSearch->SearchValue2 = @$filter["y_user_language"];
		$this->user_language->AdvancedSearch->SearchOperator2 = @$filter["w_user_language"];
		$this->user_language->AdvancedSearch->Save();

		// Field user_username
		$this->user_username->AdvancedSearch->SearchValue = @$filter["x_user_username"];
		$this->user_username->AdvancedSearch->SearchOperator = @$filter["z_user_username"];
		$this->user_username->AdvancedSearch->SearchCondition = @$filter["v_user_username"];
		$this->user_username->AdvancedSearch->SearchValue2 = @$filter["y_user_username"];
		$this->user_username->AdvancedSearch->SearchOperator2 = @$filter["w_user_username"];
		$this->user_username->AdvancedSearch->Save();

		// Field user_password
		$this->user_password->AdvancedSearch->SearchValue = @$filter["x_user_password"];
		$this->user_password->AdvancedSearch->SearchOperator = @$filter["z_user_password"];
		$this->user_password->AdvancedSearch->SearchCondition = @$filter["v_user_password"];
		$this->user_password->AdvancedSearch->SearchValue2 = @$filter["y_user_password"];
		$this->user_password->AdvancedSearch->SearchOperator2 = @$filter["w_user_password"];
		$this->user_password->AdvancedSearch->Save();

		// Field user_latitude
		$this->user_latitude->AdvancedSearch->SearchValue = @$filter["x_user_latitude"];
		$this->user_latitude->AdvancedSearch->SearchOperator = @$filter["z_user_latitude"];
		$this->user_latitude->AdvancedSearch->SearchCondition = @$filter["v_user_latitude"];
		$this->user_latitude->AdvancedSearch->SearchValue2 = @$filter["y_user_latitude"];
		$this->user_latitude->AdvancedSearch->SearchOperator2 = @$filter["w_user_latitude"];
		$this->user_latitude->AdvancedSearch->Save();

		// Field user_longitude
		$this->user_longitude->AdvancedSearch->SearchValue = @$filter["x_user_longitude"];
		$this->user_longitude->AdvancedSearch->SearchOperator = @$filter["z_user_longitude"];
		$this->user_longitude->AdvancedSearch->SearchCondition = @$filter["v_user_longitude"];
		$this->user_longitude->AdvancedSearch->SearchValue2 = @$filter["y_user_longitude"];
		$this->user_longitude->AdvancedSearch->SearchOperator2 = @$filter["w_user_longitude"];
		$this->user_longitude->AdvancedSearch->Save();

		// Field user_last_update
		$this->user_last_update->AdvancedSearch->SearchValue = @$filter["x_user_last_update"];
		$this->user_last_update->AdvancedSearch->SearchOperator = @$filter["z_user_last_update"];
		$this->user_last_update->AdvancedSearch->SearchCondition = @$filter["v_user_last_update"];
		$this->user_last_update->AdvancedSearch->SearchValue2 = @$filter["y_user_last_update"];
		$this->user_last_update->AdvancedSearch->SearchOperator2 = @$filter["w_user_last_update"];
		$this->user_last_update->AdvancedSearch->Save();

		// Field user_admin_id
		$this->user_admin_id->AdvancedSearch->SearchValue = @$filter["x_user_admin_id"];
		$this->user_admin_id->AdvancedSearch->SearchOperator = @$filter["z_user_admin_id"];
		$this->user_admin_id->AdvancedSearch->SearchCondition = @$filter["v_user_admin_id"];
		$this->user_admin_id->AdvancedSearch->SearchValue2 = @$filter["y_user_admin_id"];
		$this->user_admin_id->AdvancedSearch->SearchOperator2 = @$filter["w_user_admin_id"];
		$this->user_admin_id->AdvancedSearch->Save();

		// Field user_level
		$this->user_level->AdvancedSearch->SearchValue = @$filter["x_user_level"];
		$this->user_level->AdvancedSearch->SearchOperator = @$filter["z_user_level"];
		$this->user_level->AdvancedSearch->SearchCondition = @$filter["v_user_level"];
		$this->user_level->AdvancedSearch->SearchValue2 = @$filter["y_user_level"];
		$this->user_level->AdvancedSearch->SearchOperator2 = @$filter["w_user_level"];
		$this->user_level->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->user_id, $Default, FALSE); // user_id
		$this->BuildSearchSql($sWhere, $this->user_surname, $Default, FALSE); // user_surname
		$this->BuildSearchSql($sWhere, $this->user_name, $Default, FALSE); // user_name
		$this->BuildSearchSql($sWhere, $this->user_gender, $Default, FALSE); // user_gender
		$this->BuildSearchSql($sWhere, $this->user_born, $Default, FALSE); // user_born
		$this->BuildSearchSql($sWhere, $this->user_modules, $Default, TRUE); // user_modules
		$this->BuildSearchSql($sWhere, $this->user_doctor_id, $Default, FALSE); // user_doctor_id
		$this->BuildSearchSql($sWhere, $this->user_is_active, $Default, FALSE); // user_is_active
		$this->BuildSearchSql($sWhere, $this->user_medical_history, $Default, FALSE); // user_medical_history
		$this->BuildSearchSql($sWhere, $this->user_address, $Default, FALSE); // user_address
		$this->BuildSearchSql($sWhere, $this->user_region_id, $Default, FALSE); // user_region_id
		$this->BuildSearchSql($sWhere, $this->user_province_id, $Default, FALSE); // user_province_id
		$this->BuildSearchSql($sWhere, $this->user_city_id, $Default, FALSE); // user_city_id
		$this->BuildSearchSql($sWhere, $this->user_email, $Default, FALSE); // user_email
		$this->BuildSearchSql($sWhere, $this->user_phone, $Default, FALSE); // user_phone
		$this->BuildSearchSql($sWhere, $this->user_language, $Default, FALSE); // user_language
		$this->BuildSearchSql($sWhere, $this->user_username, $Default, FALSE); // user_username
		$this->BuildSearchSql($sWhere, $this->user_password, $Default, FALSE); // user_password
		$this->BuildSearchSql($sWhere, $this->user_latitude, $Default, FALSE); // user_latitude
		$this->BuildSearchSql($sWhere, $this->user_longitude, $Default, FALSE); // user_longitude
		$this->BuildSearchSql($sWhere, $this->user_last_update, $Default, FALSE); // user_last_update
		$this->BuildSearchSql($sWhere, $this->user_admin_id, $Default, FALSE); // user_admin_id
		$this->BuildSearchSql($sWhere, $this->user_level, $Default, FALSE); // user_level

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->user_id->AdvancedSearch->Save(); // user_id
			$this->user_surname->AdvancedSearch->Save(); // user_surname
			$this->user_name->AdvancedSearch->Save(); // user_name
			$this->user_gender->AdvancedSearch->Save(); // user_gender
			$this->user_born->AdvancedSearch->Save(); // user_born
			$this->user_modules->AdvancedSearch->Save(); // user_modules
			$this->user_doctor_id->AdvancedSearch->Save(); // user_doctor_id
			$this->user_is_active->AdvancedSearch->Save(); // user_is_active
			$this->user_medical_history->AdvancedSearch->Save(); // user_medical_history
			$this->user_address->AdvancedSearch->Save(); // user_address
			$this->user_region_id->AdvancedSearch->Save(); // user_region_id
			$this->user_province_id->AdvancedSearch->Save(); // user_province_id
			$this->user_city_id->AdvancedSearch->Save(); // user_city_id
			$this->user_email->AdvancedSearch->Save(); // user_email
			$this->user_phone->AdvancedSearch->Save(); // user_phone
			$this->user_language->AdvancedSearch->Save(); // user_language
			$this->user_username->AdvancedSearch->Save(); // user_username
			$this->user_password->AdvancedSearch->Save(); // user_password
			$this->user_latitude->AdvancedSearch->Save(); // user_latitude
			$this->user_longitude->AdvancedSearch->Save(); // user_longitude
			$this->user_last_update->AdvancedSearch->Save(); // user_last_update
			$this->user_admin_id->AdvancedSearch->Save(); // user_admin_id
			$this->user_level->AdvancedSearch->Save(); // user_level
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1 || $FldOpr <> "LIKE" ||
			($FldOpr2 <> "LIKE" && $FldVal2 <> ""))
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->user_surname, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_name, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_modules, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_medical_history, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_address, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_city_id, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_email, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_phone, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_username, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_password, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_latitude, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->user_longitude, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->user_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_surname->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_name->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_gender->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_born->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_modules->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_doctor_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_is_active->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_medical_history->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_address->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_region_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_province_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_city_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_email->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_phone->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_language->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_username->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_password->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_latitude->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_longitude->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_last_update->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_admin_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->user_level->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->user_id->AdvancedSearch->UnsetSession();
		$this->user_surname->AdvancedSearch->UnsetSession();
		$this->user_name->AdvancedSearch->UnsetSession();
		$this->user_gender->AdvancedSearch->UnsetSession();
		$this->user_born->AdvancedSearch->UnsetSession();
		$this->user_modules->AdvancedSearch->UnsetSession();
		$this->user_doctor_id->AdvancedSearch->UnsetSession();
		$this->user_is_active->AdvancedSearch->UnsetSession();
		$this->user_medical_history->AdvancedSearch->UnsetSession();
		$this->user_address->AdvancedSearch->UnsetSession();
		$this->user_region_id->AdvancedSearch->UnsetSession();
		$this->user_province_id->AdvancedSearch->UnsetSession();
		$this->user_city_id->AdvancedSearch->UnsetSession();
		$this->user_email->AdvancedSearch->UnsetSession();
		$this->user_phone->AdvancedSearch->UnsetSession();
		$this->user_language->AdvancedSearch->UnsetSession();
		$this->user_username->AdvancedSearch->UnsetSession();
		$this->user_password->AdvancedSearch->UnsetSession();
		$this->user_latitude->AdvancedSearch->UnsetSession();
		$this->user_longitude->AdvancedSearch->UnsetSession();
		$this->user_last_update->AdvancedSearch->UnsetSession();
		$this->user_admin_id->AdvancedSearch->UnsetSession();
		$this->user_level->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->user_id->AdvancedSearch->Load();
		$this->user_surname->AdvancedSearch->Load();
		$this->user_name->AdvancedSearch->Load();
		$this->user_gender->AdvancedSearch->Load();
		$this->user_born->AdvancedSearch->Load();
		$this->user_modules->AdvancedSearch->Load();
		$this->user_doctor_id->AdvancedSearch->Load();
		$this->user_is_active->AdvancedSearch->Load();
		$this->user_medical_history->AdvancedSearch->Load();
		$this->user_address->AdvancedSearch->Load();
		$this->user_region_id->AdvancedSearch->Load();
		$this->user_province_id->AdvancedSearch->Load();
		$this->user_city_id->AdvancedSearch->Load();
		$this->user_email->AdvancedSearch->Load();
		$this->user_phone->AdvancedSearch->Load();
		$this->user_language->AdvancedSearch->Load();
		$this->user_username->AdvancedSearch->Load();
		$this->user_password->AdvancedSearch->Load();
		$this->user_latitude->AdvancedSearch->Load();
		$this->user_longitude->AdvancedSearch->Load();
		$this->user_last_update->AdvancedSearch->Load();
		$this->user_admin_id->AdvancedSearch->Load();
		$this->user_level->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->user_id); // user_id
			$this->UpdateSort($this->user_surname); // user_surname
			$this->UpdateSort($this->user_name); // user_name
			$this->UpdateSort($this->user_gender); // user_gender
			$this->UpdateSort($this->user_born); // user_born
			$this->UpdateSort($this->user_doctor_id); // user_doctor_id
			$this->UpdateSort($this->user_city_id); // user_city_id
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->user_id->setSort("");
				$this->user_surname->setSort("");
				$this->user_name->setSort("");
				$this->user_gender->setSort("");
				$this->user_born->setSort("");
				$this->user_doctor_id->setSort("");
				$this->user_city_id->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = TRUE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = TRUE;

		// "detail_telecare_call"
		$item = &$this->ListOptions->Add("detail_telecare_call");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'telecare_call') && !$this->ShowMultipleDetails;
		$item->OnLeft = TRUE;
		$item->ShowInButtonGroup = FALSE;
		if (!isset($GLOBALS["telecare_call_grid"])) $GLOBALS["telecare_call_grid"] = new ctelecare_call_grid;

		// "detail_telecare_alarm"
		$item = &$this->ListOptions->Add("detail_telecare_alarm");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'telecare_alarm') && !$this->ShowMultipleDetails;
		$item->OnLeft = TRUE;
		$item->ShowInButtonGroup = FALSE;
		if (!isset($GLOBALS["telecare_alarm_grid"])) $GLOBALS["telecare_alarm_grid"] = new ctelecare_alarm_grid;

		// "detail_telecare_prescription"
		$item = &$this->ListOptions->Add("detail_telecare_prescription");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'telecare_prescription') && !$this->ShowMultipleDetails;
		$item->OnLeft = TRUE;
		$item->ShowInButtonGroup = FALSE;
		if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;

		// Multiple details
		if ($this->ShowMultipleDetails) {
			$item = &$this->ListOptions->Add("details");
			$item->CssStyle = "white-space: nowrap;";
			$item->Visible = $this->ShowMultipleDetails;
			$item->OnLeft = TRUE;
			$item->ShowInButtonGroup = FALSE;
		}

		// Set up detail pages
		$pages = new cSubPages();
		$pages->Add("telecare_call");
		$pages->Add("telecare_alarm");
		$pages->Add("telecare_prescription");
		$this->DetailPages = $pages;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = TRUE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}
		$DetailViewTblVar = "";
		$DetailCopyTblVar = "";
		$DetailEditTblVar = "";

		// "detail_telecare_call"
		$oListOpt = &$this->ListOptions->Items["detail_telecare_call"];
		if ($Security->AllowList(CurrentProjectID() . 'telecare_call')) {
			$body = $Language->Phrase("DetailLink") . $Language->TablePhrase("telecare_call", "TblCaption");
			$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("telecare_calllist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "") . "\">" . $body . "</a>";
			$links = "";
			if ($GLOBALS["telecare_call_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_call')) {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
				if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
				$DetailViewTblVar .= "telecare_call";
			}
			if ($GLOBALS["telecare_call_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_call')) {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
				if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
				$DetailEditTblVar .= "telecare_call";
			}
			if ($GLOBALS["telecare_call_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_call')) {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
				if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
				$DetailCopyTblVar .= "telecare_call";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
			}
			$body = "<div class=\"btn-group\">" . $body . "</div>";
			$oListOpt->Body = $body;
			if ($this->ShowMultipleDetails) $oListOpt->Visible = FALSE;
		}

		// "detail_telecare_alarm"
		$oListOpt = &$this->ListOptions->Items["detail_telecare_alarm"];
		if ($Security->AllowList(CurrentProjectID() . 'telecare_alarm')) {
			$body = $Language->Phrase("DetailLink") . $Language->TablePhrase("telecare_alarm", "TblCaption");
			$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("telecare_alarmlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "") . "\">" . $body . "</a>";
			$links = "";
			if ($GLOBALS["telecare_alarm_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_alarm')) {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
				if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
				$DetailViewTblVar .= "telecare_alarm";
			}
			if ($GLOBALS["telecare_alarm_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_alarm')) {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
				if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
				$DetailEditTblVar .= "telecare_alarm";
			}
			if ($GLOBALS["telecare_alarm_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_alarm')) {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
				if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
				$DetailCopyTblVar .= "telecare_alarm";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
			}
			$body = "<div class=\"btn-group\">" . $body . "</div>";
			$oListOpt->Body = $body;
			if ($this->ShowMultipleDetails) $oListOpt->Visible = FALSE;
		}

		// "detail_telecare_prescription"
		$oListOpt = &$this->ListOptions->Items["detail_telecare_prescription"];
		if ($Security->AllowList(CurrentProjectID() . 'telecare_prescription')) {
			$body = $Language->Phrase("DetailLink") . $Language->TablePhrase("telecare_prescription", "TblCaption");
			$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("telecare_prescriptionlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "") . "\">" . $body . "</a>";
			$links = "";
			if ($GLOBALS["telecare_prescription_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_prescription')) {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
				if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
				$DetailViewTblVar .= "telecare_prescription";
			}
			if ($GLOBALS["telecare_prescription_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_prescription')) {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
				if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
				$DetailEditTblVar .= "telecare_prescription";
			}
			if ($GLOBALS["telecare_prescription_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_prescription')) {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
				if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
				$DetailCopyTblVar .= "telecare_prescription";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
			}
			$body = "<div class=\"btn-group\">" . $body . "</div>";
			$oListOpt->Body = $body;
			if ($this->ShowMultipleDetails) $oListOpt->Visible = FALSE;
		}
		if ($this->ShowMultipleDetails) {
			$body = $Language->Phrase("MultipleMasterDetails");
			$body = "<div class=\"btn-group\">";
			$links = "";
			if ($DetailViewTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailViewTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			}
			if ($DetailEditTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailEditTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			}
			if ($DetailCopyTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailCopyTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewMasterDetail\" title=\"" . ew_HtmlTitle($Language->Phrase("MultipleMasterDetails")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("MultipleMasterDetails") . "<b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu ewMenu\">". $links . "</ul>";
			}
			$body .= "</div>";

			// Multiple details
			$oListOpt = &$this->ListOptions->Items["details"];
			$oListOpt->Body = $body;
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->user_id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$item = &$option->Add("gridadd");
		$item->Body = "<a class=\"ewAddEdit ewGridAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridAddLink")) . "\" href=\"" . ew_HtmlEncode($this->GridAddUrl) . "\">" . $Language->Phrase("GridAddLink") . "</a>";
		$item->Visible = ($this->GridAddUrl <> "" && $Security->CanAdd());
		$option = $options["detail"];
		$DetailTableLink = "";
		$item = &$option->Add("detailadd_telecare_call");
		$caption = $Language->Phrase("Add") . "&nbsp;" . $this->TableCaption() . "/" . $GLOBALS["telecare_call"]->TableCaption();
		$item->Body = "<a class=\"ewDetailAddGroup ewDetailAdd\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"" . ew_HtmlEncode($this->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=telecare_call") . "\">" . $caption . "</a>";
		$item->Visible = ($GLOBALS["telecare_call"]->DetailAdd && $Security->AllowAdd(CurrentProjectID() . 'telecare_call') && $Security->CanAdd());
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "telecare_call";
		}
		$item = &$option->Add("detailadd_telecare_alarm");
		$caption = $Language->Phrase("Add") . "&nbsp;" . $this->TableCaption() . "/" . $GLOBALS["telecare_alarm"]->TableCaption();
		$item->Body = "<a class=\"ewDetailAddGroup ewDetailAdd\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"" . ew_HtmlEncode($this->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=telecare_alarm") . "\">" . $caption . "</a>";
		$item->Visible = ($GLOBALS["telecare_alarm"]->DetailAdd && $Security->AllowAdd(CurrentProjectID() . 'telecare_alarm') && $Security->CanAdd());
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "telecare_alarm";
		}
		$item = &$option->Add("detailadd_telecare_prescription");
		$caption = $Language->Phrase("Add") . "&nbsp;" . $this->TableCaption() . "/" . $GLOBALS["telecare_prescription"]->TableCaption();
		$item->Body = "<a class=\"ewDetailAddGroup ewDetailAdd\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"" . ew_HtmlEncode($this->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "\">" . $caption . "</a>";
		$item->Visible = ($GLOBALS["telecare_prescription"]->DetailAdd && $Security->AllowAdd(CurrentProjectID() . 'telecare_prescription') && $Security->CanAdd());
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "telecare_prescription";
		}

		// Add multiple details
		if ($this->ShowMultipleDetails) {
			$item = &$option->Add("detailsadd");
			$item->Body = "<a class=\"ewDetailAddGroup ewDetailAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddMasterDetailLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddMasterDetailLink")) . "\" href=\"" . ew_HtmlEncode($this->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=" . $DetailTableLink) . "\">" . $Language->Phrase("AddMasterDetailLink") . "</a>";
			$item->Visible = ($DetailTableLink <> "" && $Security->CanAdd());

			// Hide single master/detail items
			$ar = explode(",", $DetailTableLink);
			$cnt = count($ar);
			for ($i = 0; $i < $cnt; $i++) {
				if ($item = &$option->GetItem("detailadd_" . $ar[$i]))
					$item->Visible = FALSE;
			}
		}
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"ftelecare_userlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"ftelecare_userlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "gridedit") { // Not grid add/edit mode
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.ftelecare_userlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
		} else { // Grid add/edit mode

			// Hide all options first
			foreach ($options as &$option)
				$option->HideAllOptions();
			if ($this->CurrentAction == "gridadd") {
				if ($this->AllowAddDeleteRow) {

					// Add add blank row
					$option = &$options["addedit"];
					$option->UseDropDownButton = FALSE;
					$option->UseImageAndText = TRUE;
					$item = &$option->Add("addblankrow");
					$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
					$item->Visible = $Security->CanAdd();
				}
				$option = &$options["action"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;

				// Add grid insert
				$item = &$option->Add("gridinsert");
				$item->Body = "<a class=\"ewAction ewGridInsert\" title=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridInsertLink")) . "\" href=\"\" onclick=\"return ewForms(this).Submit('" . $this->AddMasterUrl($this->PageName()) . "');\">" . $Language->Phrase("GridInsertLink") . "</a>";

				// Add grid cancel
				$item = &$option->Add("gridcancel");
				$cancelurl = $this->AddMasterUrl($this->PageUrl() . "a=cancel");
				$item->Body = "<a class=\"ewAction ewGridCancel\" title=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("GridCancelLink")) . "\" href=\"" . $cancelurl . "\">" . $Language->Phrase("GridCancelLink") . "</a>";
			}
		}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"ftelecare_userlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Advanced search button
		$item = &$this->SearchOptions->Add("advancedsearch");
		if (ew_IsMobile())
			$item->Body = "<a class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" href=\"telecare_usersrch.php\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		else
			$item->Body = "<button type=\"button\" class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" onclick=\"ew_SearchDialogShow({lnk:this,url:'telecare_usersrch.php'});\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		$item->Visible = TRUE;

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;

		// Hide detail items for dropdown if necessary
		$this->ListOptions->HideDetailItemsForDropDown();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
		$links = "";
		$btngrps = "";
		$sSqlWrk = "`call_user_id`=" . ew_AdjustSql($this->user_id->CurrentValue, $this->DBID) . "";

		// Column "detail_telecare_call"
		if ($this->DetailPages->Items["telecare_call"]->Visible) {
			$link = "";
			$option = &$this->ListOptions->Items["detail_telecare_call"];
			$url = "telecare_callpreview.php?t=telecare_user&f=" . ew_Encrypt($sSqlWrk);
			$btngrp = "<div data-table=\"telecare_call\" data-url=\"" . $url . "\" class=\"btn-group\">";
			if ($Security->AllowList(CurrentProjectID() . 'telecare_call')) {			
				$label = $Language->TablePhrase("telecare_call", "TblCaption");
				$link = "<li><a href=\"#\" data-toggle=\"tab\" data-table=\"telecare_call\" data-url=\"" . $url . "\">" . $label . "</a></li>";			
				$links .= $link;
				$detaillnk = ew_JsEncode3("telecare_calllist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "");
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . $Language->TablePhrase("telecare_call", "TblCaption") . "\" onclick=\"window.location='" . $detaillnk . "'\">" . $Language->Phrase("MasterDetailListLink") . "</button>";
			}
			if ($GLOBALS["telecare_call_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_call'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" onclick=\"window.location='" . $this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call") . "'\">" . $Language->Phrase("MasterDetailViewLink") . "</button>";
			if ($GLOBALS["telecare_call_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_call'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" onclick=\"window.location='" . $this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call") . "'\">" . $Language->Phrase("MasterDetailEditLink") . "</button>";
			if ($GLOBALS["telecare_call_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_call'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" onclick=\"window.location='" . $this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call") . "'\">" . $Language->Phrase("MasterDetailCopyLink") . "</button>";
			$btngrp .= "</div>";
			if ($link <> "") {
				$btngrps .= $btngrp;
				$option->Body .= "<div class=\"hide ewPreview\">" . $link . $btngrp . "</div>";
			}
		}
		$sSqlWrk = "`alarm_user_id`=" . ew_AdjustSql($this->user_id->CurrentValue, $this->DBID) . "";

		// Column "detail_telecare_alarm"
		if ($this->DetailPages->Items["telecare_alarm"]->Visible) {
			$link = "";
			$option = &$this->ListOptions->Items["detail_telecare_alarm"];
			$url = "telecare_alarmpreview.php?t=telecare_user&f=" . ew_Encrypt($sSqlWrk);
			$btngrp = "<div data-table=\"telecare_alarm\" data-url=\"" . $url . "\" class=\"btn-group\">";
			if ($Security->AllowList(CurrentProjectID() . 'telecare_alarm')) {			
				$label = $Language->TablePhrase("telecare_alarm", "TblCaption");
				$link = "<li><a href=\"#\" data-toggle=\"tab\" data-table=\"telecare_alarm\" data-url=\"" . $url . "\">" . $label . "</a></li>";			
				$links .= $link;
				$detaillnk = ew_JsEncode3("telecare_alarmlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "");
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . $Language->TablePhrase("telecare_alarm", "TblCaption") . "\" onclick=\"window.location='" . $detaillnk . "'\">" . $Language->Phrase("MasterDetailListLink") . "</button>";
			}
			if ($GLOBALS["telecare_alarm_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_alarm'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" onclick=\"window.location='" . $this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm") . "'\">" . $Language->Phrase("MasterDetailViewLink") . "</button>";
			if ($GLOBALS["telecare_alarm_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_alarm'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" onclick=\"window.location='" . $this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm") . "'\">" . $Language->Phrase("MasterDetailEditLink") . "</button>";
			if ($GLOBALS["telecare_alarm_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_alarm'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" onclick=\"window.location='" . $this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm") . "'\">" . $Language->Phrase("MasterDetailCopyLink") . "</button>";
			$btngrp .= "</div>";
			if ($link <> "") {
				$btngrps .= $btngrp;
				$option->Body .= "<div class=\"hide ewPreview\">" . $link . $btngrp . "</div>";
			}
		}
		$sSqlWrk = "`prescription_user_id`=" . ew_AdjustSql($this->user_id->CurrentValue, $this->DBID) . "";

		// Column "detail_telecare_prescription"
		if ($this->DetailPages->Items["telecare_prescription"]->Visible) {
			$link = "";
			$option = &$this->ListOptions->Items["detail_telecare_prescription"];
			$url = "telecare_prescriptionpreview.php?t=telecare_user&f=" . ew_Encrypt($sSqlWrk);
			$btngrp = "<div data-table=\"telecare_prescription\" data-url=\"" . $url . "\" class=\"btn-group\">";
			if ($Security->AllowList(CurrentProjectID() . 'telecare_prescription')) {			
				$label = $Language->TablePhrase("telecare_prescription", "TblCaption");
				$link = "<li><a href=\"#\" data-toggle=\"tab\" data-table=\"telecare_prescription\" data-url=\"" . $url . "\">" . $label . "</a></li>";			
				$links .= $link;
				$detaillnk = ew_JsEncode3("telecare_prescriptionlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "");
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . $Language->TablePhrase("telecare_prescription", "TblCaption") . "\" onclick=\"window.location='" . $detaillnk . "'\">" . $Language->Phrase("MasterDetailListLink") . "</button>";
			}
			if ($GLOBALS["telecare_prescription_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_prescription'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" onclick=\"window.location='" . $this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "'\">" . $Language->Phrase("MasterDetailViewLink") . "</button>";
			if ($GLOBALS["telecare_prescription_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_prescription'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" onclick=\"window.location='" . $this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "'\">" . $Language->Phrase("MasterDetailEditLink") . "</button>";
			if ($GLOBALS["telecare_prescription_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_prescription'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" onclick=\"window.location='" . $this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "'\">" . $Language->Phrase("MasterDetailCopyLink") . "</button>";
			$btngrp .= "</div>";
			if ($link <> "") {
				$btngrps .= $btngrp;
				$option->Body .= "<div class=\"hide ewPreview\">" . $link . $btngrp . "</div>";
			}
		}

		// Hide detail items if necessary
		$this->ListOptions->HideDetailItemsForDropDown();

		// Column "preview"
		$option = &$this->ListOptions->GetItem("preview");
		if (!$option) { // Add preview column
			$option = &$this->ListOptions->Add("preview");
			$option->OnLeft = TRUE;
			if ($option->OnLeft) {
				$option->MoveTo($this->ListOptions->ItemPos("checkbox") + 1);
			} else {
				$option->MoveTo($this->ListOptions->ItemPos("checkbox"));
			}
			$option->Visible = !($this->Export <> "" || $this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit");
			$option->ShowInDropDown = FALSE;
			$option->ShowInButtonGroup = FALSE;
		}
		if ($option) {
			$option->Body = "<span class=\"ewPreviewRowBtn icon-expand\"></span>";
			$option->Body .= "<div class=\"hide ewPreview\">" . $links . $btngrps . "</div>";
			if ($option->Visible) $option->Visible = $link <> "";
		}

		// Column "details" (Multiple details)
		$option = &$this->ListOptions->GetItem("details");
		if ($option) {
			$option->Body .= "<div class=\"hide ewPreview\">" . $links . $btngrps . "</div>";
			if ($option->Visible) $option->Visible = $links <> "";
		}
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->user_id->CurrentValue = NULL;
		$this->user_id->OldValue = $this->user_id->CurrentValue;
		$this->user_surname->CurrentValue = NULL;
		$this->user_surname->OldValue = $this->user_surname->CurrentValue;
		$this->user_name->CurrentValue = NULL;
		$this->user_name->OldValue = $this->user_name->CurrentValue;
		$this->user_gender->CurrentValue = 1;
		$this->user_gender->OldValue = $this->user_gender->CurrentValue;
		$this->user_born->CurrentValue = NULL;
		$this->user_born->OldValue = $this->user_born->CurrentValue;
		$this->user_doctor_id->CurrentValue = NULL;
		$this->user_doctor_id->OldValue = $this->user_doctor_id->CurrentValue;
		$this->user_city_id->CurrentValue = NULL;
		$this->user_city_id->OldValue = $this->user_city_id->CurrentValue;
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// user_id

		$this->user_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_id"]);
		if ($this->user_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_id->AdvancedSearch->SearchOperator = @$_GET["z_user_id"];

		// user_surname
		$this->user_surname->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_surname"]);
		if ($this->user_surname->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_surname->AdvancedSearch->SearchOperator = @$_GET["z_user_surname"];

		// user_name
		$this->user_name->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_name"]);
		if ($this->user_name->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_name->AdvancedSearch->SearchOperator = @$_GET["z_user_name"];

		// user_gender
		$this->user_gender->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_gender"]);
		if ($this->user_gender->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_gender->AdvancedSearch->SearchOperator = @$_GET["z_user_gender"];

		// user_born
		$this->user_born->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_born"]);
		if ($this->user_born->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_born->AdvancedSearch->SearchOperator = @$_GET["z_user_born"];

		// user_modules
		$this->user_modules->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_modules"]);
		if ($this->user_modules->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_modules->AdvancedSearch->SearchOperator = @$_GET["z_user_modules"];
		if (is_array($this->user_modules->AdvancedSearch->SearchValue)) $this->user_modules->AdvancedSearch->SearchValue = implode(",", $this->user_modules->AdvancedSearch->SearchValue);
		if (is_array($this->user_modules->AdvancedSearch->SearchValue2)) $this->user_modules->AdvancedSearch->SearchValue2 = implode(",", $this->user_modules->AdvancedSearch->SearchValue2);

		// user_doctor_id
		$this->user_doctor_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_doctor_id"]);
		if ($this->user_doctor_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_doctor_id->AdvancedSearch->SearchOperator = @$_GET["z_user_doctor_id"];

		// user_is_active
		$this->user_is_active->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_is_active"]);
		if ($this->user_is_active->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_is_active->AdvancedSearch->SearchOperator = @$_GET["z_user_is_active"];

		// user_medical_history
		$this->user_medical_history->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_medical_history"]);
		if ($this->user_medical_history->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_medical_history->AdvancedSearch->SearchOperator = @$_GET["z_user_medical_history"];

		// user_address
		$this->user_address->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_address"]);
		if ($this->user_address->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_address->AdvancedSearch->SearchOperator = @$_GET["z_user_address"];

		// user_region_id
		$this->user_region_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_region_id"]);
		if ($this->user_region_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_region_id->AdvancedSearch->SearchOperator = @$_GET["z_user_region_id"];

		// user_province_id
		$this->user_province_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_province_id"]);
		if ($this->user_province_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_province_id->AdvancedSearch->SearchOperator = @$_GET["z_user_province_id"];

		// user_city_id
		$this->user_city_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_city_id"]);
		if ($this->user_city_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_city_id->AdvancedSearch->SearchOperator = @$_GET["z_user_city_id"];

		// user_email
		$this->user_email->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_email"]);
		if ($this->user_email->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_email->AdvancedSearch->SearchOperator = @$_GET["z_user_email"];

		// user_phone
		$this->user_phone->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_phone"]);
		if ($this->user_phone->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_phone->AdvancedSearch->SearchOperator = @$_GET["z_user_phone"];

		// user_language
		$this->user_language->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_language"]);
		if ($this->user_language->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_language->AdvancedSearch->SearchOperator = @$_GET["z_user_language"];

		// user_username
		$this->user_username->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_username"]);
		if ($this->user_username->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_username->AdvancedSearch->SearchOperator = @$_GET["z_user_username"];

		// user_password
		$this->user_password->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_password"]);
		if ($this->user_password->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_password->AdvancedSearch->SearchOperator = @$_GET["z_user_password"];

		// user_latitude
		$this->user_latitude->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_latitude"]);
		if ($this->user_latitude->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_latitude->AdvancedSearch->SearchOperator = @$_GET["z_user_latitude"];

		// user_longitude
		$this->user_longitude->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_longitude"]);
		if ($this->user_longitude->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_longitude->AdvancedSearch->SearchOperator = @$_GET["z_user_longitude"];

		// user_last_update
		$this->user_last_update->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_last_update"]);
		if ($this->user_last_update->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_last_update->AdvancedSearch->SearchOperator = @$_GET["z_user_last_update"];

		// user_admin_id
		$this->user_admin_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_admin_id"]);
		if ($this->user_admin_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_admin_id->AdvancedSearch->SearchOperator = @$_GET["z_user_admin_id"];

		// user_level
		$this->user_level->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_user_level"]);
		if ($this->user_level->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->user_level->AdvancedSearch->SearchOperator = @$_GET["z_user_level"];
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->user_id->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->user_id->setFormValue($objForm->GetValue("x_user_id"));
		if (!$this->user_surname->FldIsDetailKey) {
			$this->user_surname->setFormValue($objForm->GetValue("x_user_surname"));
		}
		$this->user_surname->setOldValue($objForm->GetValue("o_user_surname"));
		if (!$this->user_name->FldIsDetailKey) {
			$this->user_name->setFormValue($objForm->GetValue("x_user_name"));
		}
		$this->user_name->setOldValue($objForm->GetValue("o_user_name"));
		if (!$this->user_gender->FldIsDetailKey) {
			$this->user_gender->setFormValue($objForm->GetValue("x_user_gender"));
		}
		$this->user_gender->setOldValue($objForm->GetValue("o_user_gender"));
		if (!$this->user_born->FldIsDetailKey) {
			$this->user_born->setFormValue($objForm->GetValue("x_user_born"));
			$this->user_born->CurrentValue = ew_UnFormatDateTime($this->user_born->CurrentValue, 7);
		}
		$this->user_born->setOldValue($objForm->GetValue("o_user_born"));
		if (!$this->user_doctor_id->FldIsDetailKey) {
			$this->user_doctor_id->setFormValue($objForm->GetValue("x_user_doctor_id"));
		}
		$this->user_doctor_id->setOldValue($objForm->GetValue("o_user_doctor_id"));
		if (!$this->user_city_id->FldIsDetailKey) {
			$this->user_city_id->setFormValue($objForm->GetValue("x_user_city_id"));
		}
		$this->user_city_id->setOldValue($objForm->GetValue("o_user_city_id"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->user_id->CurrentValue = $this->user_id->FormValue;
		$this->user_surname->CurrentValue = $this->user_surname->FormValue;
		$this->user_name->CurrentValue = $this->user_name->FormValue;
		$this->user_gender->CurrentValue = $this->user_gender->FormValue;
		$this->user_born->CurrentValue = $this->user_born->FormValue;
		$this->user_born->CurrentValue = ew_UnFormatDateTime($this->user_born->CurrentValue, 7);
		$this->user_doctor_id->CurrentValue = $this->user_doctor_id->FormValue;
		$this->user_city_id->CurrentValue = $this->user_city_id->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->user_id->setDbValue($rs->fields('user_id'));
		$this->user_surname->setDbValue($rs->fields('user_surname'));
		$this->user_name->setDbValue($rs->fields('user_name'));
		$this->user_gender->setDbValue($rs->fields('user_gender'));
		$this->user_born->setDbValue($rs->fields('user_born'));
		$this->user_modules->setDbValue($rs->fields('user_modules'));
		$this->user_doctor_id->setDbValue($rs->fields('user_doctor_id'));
		$this->user_is_active->setDbValue($rs->fields('user_is_active'));
		$this->user_medical_history->setDbValue($rs->fields('user_medical_history'));
		$this->user_address->setDbValue($rs->fields('user_address'));
		$this->user_region_id->setDbValue($rs->fields('user_region_id'));
		$this->user_province_id->setDbValue($rs->fields('user_province_id'));
		$this->user_city_id->setDbValue($rs->fields('user_city_id'));
		$this->user_email->setDbValue($rs->fields('user_email'));
		$this->user_phone->setDbValue($rs->fields('user_phone'));
		$this->user_language->setDbValue($rs->fields('user_language'));
		$this->user_username->setDbValue($rs->fields('user_username'));
		$this->user_password->setDbValue($rs->fields('user_password'));
		$this->user_latitude->setDbValue($rs->fields('user_latitude'));
		$this->user_longitude->setDbValue($rs->fields('user_longitude'));
		$this->user_last_update->setDbValue($rs->fields('user_last_update'));
		$this->user_admin_id->setDbValue($rs->fields('user_admin_id'));
		$this->user_level->setDbValue($rs->fields('user_level'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->user_id->DbValue = $row['user_id'];
		$this->user_surname->DbValue = $row['user_surname'];
		$this->user_name->DbValue = $row['user_name'];
		$this->user_gender->DbValue = $row['user_gender'];
		$this->user_born->DbValue = $row['user_born'];
		$this->user_modules->DbValue = $row['user_modules'];
		$this->user_doctor_id->DbValue = $row['user_doctor_id'];
		$this->user_is_active->DbValue = $row['user_is_active'];
		$this->user_medical_history->DbValue = $row['user_medical_history'];
		$this->user_address->DbValue = $row['user_address'];
		$this->user_region_id->DbValue = $row['user_region_id'];
		$this->user_province_id->DbValue = $row['user_province_id'];
		$this->user_city_id->DbValue = $row['user_city_id'];
		$this->user_email->DbValue = $row['user_email'];
		$this->user_phone->DbValue = $row['user_phone'];
		$this->user_language->DbValue = $row['user_language'];
		$this->user_username->DbValue = $row['user_username'];
		$this->user_password->DbValue = $row['user_password'];
		$this->user_latitude->DbValue = $row['user_latitude'];
		$this->user_longitude->DbValue = $row['user_longitude'];
		$this->user_last_update->DbValue = $row['user_last_update'];
		$this->user_admin_id->DbValue = $row['user_admin_id'];
		$this->user_level->DbValue = $row['user_level'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("user_id")) <> "")
			$this->user_id->CurrentValue = $this->getKey("user_id"); // user_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// user_id
		// user_surname
		// user_name
		// user_gender
		// user_born
		// user_modules
		// user_doctor_id
		// user_is_active
		// user_medical_history
		// user_address
		// user_region_id
		// user_province_id
		// user_city_id
		// user_email
		// user_phone
		// user_language
		// user_username
		// user_password
		// user_latitude
		// user_longitude
		// user_last_update
		// user_admin_id

		$this->user_admin_id->CellCssStyle = "white-space: nowrap;";

		// user_level
		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// user_id
		$this->user_id->ViewValue = $this->user_id->CurrentValue;
		$this->user_id->ViewCustomAttributes = "";

		// user_surname
		$this->user_surname->ViewValue = $this->user_surname->CurrentValue;
		$this->user_surname->ViewCustomAttributes = "";

		// user_name
		$this->user_name->ViewValue = $this->user_name->CurrentValue;
		$this->user_name->ViewCustomAttributes = "";

		// user_gender
		if (strval($this->user_gender->CurrentValue) <> "") {
			$this->user_gender->ViewValue = $this->user_gender->OptionCaption($this->user_gender->CurrentValue);
		} else {
			$this->user_gender->ViewValue = NULL;
		}
		$this->user_gender->ViewCustomAttributes = "";

		// user_born
		$this->user_born->ViewValue = $this->user_born->CurrentValue;
		$this->user_born->ViewValue = ew_FormatDateTime($this->user_born->ViewValue, 7);
		$this->user_born->ViewCustomAttributes = "";

		// user_modules
		if (strval($this->user_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->user_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->user_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->user_modules->ViewValue .= $this->user_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->user_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->user_modules->ViewValue = $this->user_modules->CurrentValue;
			}
		} else {
			$this->user_modules->ViewValue = NULL;
		}
		$this->user_modules->ViewCustomAttributes = "";

		// user_doctor_id
		if (strval($this->user_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = " `admin_level` = 2 ";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->CurrentValue;
			}
		} else {
			$this->user_doctor_id->ViewValue = NULL;
		}
		$this->user_doctor_id->ViewCustomAttributes = "";

		// user_is_active
		$this->user_is_active->ViewValue = $this->user_is_active->CurrentValue;
		$this->user_is_active->ViewCustomAttributes = "";

		// user_medical_history
		$this->user_medical_history->ViewValue = $this->user_medical_history->CurrentValue;
		$this->user_medical_history->ViewCustomAttributes = "";

		// user_address
		$this->user_address->ViewValue = $this->user_address->CurrentValue;
		$this->user_address->ViewCustomAttributes = "";

		// user_region_id
		if (strval($this->user_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->user_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `region_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_region_id->ViewValue = $this->user_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_region_id->ViewValue = $this->user_region_id->CurrentValue;
			}
		} else {
			$this->user_region_id->ViewValue = NULL;
		}
		$this->user_region_id->ViewCustomAttributes = "";

		// user_province_id
		if (strval($this->user_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->user_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `province_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_province_id->ViewValue = $this->user_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_province_id->ViewValue = $this->user_province_id->CurrentValue;
			}
		} else {
			$this->user_province_id->ViewValue = NULL;
		}
		$this->user_province_id->ViewCustomAttributes = "";

		// user_city_id
		if (strval($this->user_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_city_id->ViewValue = $this->user_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_city_id->ViewValue = $this->user_city_id->CurrentValue;
			}
		} else {
			$this->user_city_id->ViewValue = NULL;
		}
		$this->user_city_id->ViewCustomAttributes = "";

		// user_email
		$this->user_email->ViewValue = $this->user_email->CurrentValue;
		$this->user_email->ViewCustomAttributes = "";

		// user_phone
		$this->user_phone->ViewValue = $this->user_phone->CurrentValue;
		$this->user_phone->ViewCustomAttributes = "";

		// user_language
		$this->user_language->ViewValue = $this->user_language->CurrentValue;
		$this->user_language->ViewCustomAttributes = "";

		// user_username
		$this->user_username->ViewValue = $this->user_username->CurrentValue;
		$this->user_username->ViewCustomAttributes = "";

		// user_password
		$this->user_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->user_password->ViewCustomAttributes = "";

		// user_latitude
		$this->user_latitude->ViewValue = $this->user_latitude->CurrentValue;
		$this->user_latitude->ViewCustomAttributes = "";

		// user_longitude
		$this->user_longitude->ViewValue = $this->user_longitude->CurrentValue;
		$this->user_longitude->ViewCustomAttributes = "";

		// user_last_update
		$this->user_last_update->ViewValue = $this->user_last_update->CurrentValue;
		$this->user_last_update->ViewValue = ew_FormatDateTime($this->user_last_update->ViewValue, 7);
		$this->user_last_update->ViewCustomAttributes = "";

		// user_level
		$this->user_level->ViewValue = $this->user_level->CurrentValue;
		$this->user_level->ViewCustomAttributes = "";

			// user_id
			$this->user_id->LinkCustomAttributes = "";
			$this->user_id->HrefValue = "";
			$this->user_id->TooltipValue = "";

			// user_surname
			$this->user_surname->LinkCustomAttributes = "";
			$this->user_surname->HrefValue = "";
			$this->user_surname->TooltipValue = "";

			// user_name
			$this->user_name->LinkCustomAttributes = "";
			$this->user_name->HrefValue = "";
			$this->user_name->TooltipValue = "";

			// user_gender
			$this->user_gender->LinkCustomAttributes = "";
			$this->user_gender->HrefValue = "";
			$this->user_gender->TooltipValue = "";

			// user_born
			$this->user_born->LinkCustomAttributes = "";
			$this->user_born->HrefValue = "";
			$this->user_born->TooltipValue = "";

			// user_doctor_id
			$this->user_doctor_id->LinkCustomAttributes = "";
			$this->user_doctor_id->HrefValue = "";
			$this->user_doctor_id->TooltipValue = "";

			// user_city_id
			$this->user_city_id->LinkCustomAttributes = "";
			$this->user_city_id->HrefValue = "";
			$this->user_city_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// user_id
			// user_surname

			$this->user_surname->EditAttrs["class"] = "form-control";
			$this->user_surname->EditCustomAttributes = "";
			$this->user_surname->EditValue = ew_HtmlEncode($this->user_surname->CurrentValue);
			$this->user_surname->PlaceHolder = ew_RemoveHtml($this->user_surname->FldCaption());

			// user_name
			$this->user_name->EditAttrs["class"] = "form-control";
			$this->user_name->EditCustomAttributes = "";
			$this->user_name->EditValue = ew_HtmlEncode($this->user_name->CurrentValue);
			$this->user_name->PlaceHolder = ew_RemoveHtml($this->user_name->FldCaption());

			// user_gender
			$this->user_gender->EditCustomAttributes = "";
			$this->user_gender->EditValue = $this->user_gender->Options(FALSE);

			// user_born
			$this->user_born->EditAttrs["class"] = "form-control";
			$this->user_born->EditCustomAttributes = "";
			$this->user_born->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->user_born->CurrentValue, 7));
			$this->user_born->PlaceHolder = ew_RemoveHtml($this->user_born->FldCaption());

			// user_doctor_id
			$this->user_doctor_id->EditAttrs["class"] = "form-control";
			$this->user_doctor_id->EditCustomAttributes = "";
			if (trim(strval($this->user_doctor_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			$lookuptblfilter = " `admin_level` = 2 ";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_user"]->UserIDAllow($GLOBALS["telecare_user"]->CurrentAction)) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_doctor_id->EditValue = $arwrk;

			// user_city_id
			$this->user_city_id->EditAttrs["class"] = "form-control";
			$this->user_city_id->EditCustomAttributes = "";
			if (trim(strval($this->user_city_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_city_id->EditValue = $arwrk;

			// Edit refer script
			// user_id

			$this->user_id->LinkCustomAttributes = "";
			$this->user_id->HrefValue = "";

			// user_surname
			$this->user_surname->LinkCustomAttributes = "";
			$this->user_surname->HrefValue = "";

			// user_name
			$this->user_name->LinkCustomAttributes = "";
			$this->user_name->HrefValue = "";

			// user_gender
			$this->user_gender->LinkCustomAttributes = "";
			$this->user_gender->HrefValue = "";

			// user_born
			$this->user_born->LinkCustomAttributes = "";
			$this->user_born->HrefValue = "";

			// user_doctor_id
			$this->user_doctor_id->LinkCustomAttributes = "";
			$this->user_doctor_id->HrefValue = "";

			// user_city_id
			$this->user_city_id->LinkCustomAttributes = "";
			$this->user_city_id->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->user_surname->FldIsDetailKey && !is_null($this->user_surname->FormValue) && $this->user_surname->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_surname->FldCaption(), $this->user_surname->ReqErrMsg));
		}
		if (!$this->user_name->FldIsDetailKey && !is_null($this->user_name->FormValue) && $this->user_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_name->FldCaption(), $this->user_name->ReqErrMsg));
		}
		if ($this->user_gender->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_gender->FldCaption(), $this->user_gender->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->user_born->FormValue)) {
			ew_AddMessage($gsFormError, $this->user_born->FldErrMsg());
		}
		if (!$this->user_doctor_id->FldIsDetailKey && !is_null($this->user_doctor_id->FormValue) && $this->user_doctor_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_doctor_id->FldCaption(), $this->user_doctor_id->ReqErrMsg));
		}
		if (!$this->user_city_id->FldIsDetailKey && !is_null($this->user_city_id->FormValue) && $this->user_city_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_city_id->FldCaption(), $this->user_city_id->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['user_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// user_surname
		$this->user_surname->SetDbValueDef($rsnew, $this->user_surname->CurrentValue, NULL, FALSE);

		// user_name
		$this->user_name->SetDbValueDef($rsnew, $this->user_name->CurrentValue, NULL, FALSE);

		// user_gender
		$this->user_gender->SetDbValueDef($rsnew, $this->user_gender->CurrentValue, NULL, strval($this->user_gender->CurrentValue) == "");

		// user_born
		$this->user_born->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->user_born->CurrentValue, 7), NULL, FALSE);

		// user_doctor_id
		$this->user_doctor_id->SetDbValueDef($rsnew, $this->user_doctor_id->CurrentValue, NULL, FALSE);

		// user_city_id
		$this->user_city_id->SetDbValueDef($rsnew, $this->user_city_id->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->user_id->setDbValue($conn->Insert_ID());
				$rsnew['user_id'] = $this->user_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
			$this->WriteAuditTrailOnAdd($rsnew);
		}
		return $AddRow;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->user_id->AdvancedSearch->Load();
		$this->user_surname->AdvancedSearch->Load();
		$this->user_name->AdvancedSearch->Load();
		$this->user_gender->AdvancedSearch->Load();
		$this->user_born->AdvancedSearch->Load();
		$this->user_modules->AdvancedSearch->Load();
		$this->user_doctor_id->AdvancedSearch->Load();
		$this->user_is_active->AdvancedSearch->Load();
		$this->user_medical_history->AdvancedSearch->Load();
		$this->user_address->AdvancedSearch->Load();
		$this->user_region_id->AdvancedSearch->Load();
		$this->user_province_id->AdvancedSearch->Load();
		$this->user_city_id->AdvancedSearch->Load();
		$this->user_email->AdvancedSearch->Load();
		$this->user_phone->AdvancedSearch->Load();
		$this->user_language->AdvancedSearch->Load();
		$this->user_username->AdvancedSearch->Load();
		$this->user_password->AdvancedSearch->Load();
		$this->user_latitude->AdvancedSearch->Load();
		$this->user_longitude->AdvancedSearch->Load();
		$this->user_last_update->AdvancedSearch->Load();
		$this->user_admin_id->AdvancedSearch->Load();
		$this->user_level->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_telecare_user\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_telecare_user',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.ftelecare_userlist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'telecare_user';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (add page)
	function WriteAuditTrailOnAdd(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnAdd) return;
		$table = 'telecare_user';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['user_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$newvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$newvalue = $rs[$fldname];
					else
						$newvalue = "[MEMO]"; // Memo Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$newvalue = "[XML]"; // XML Field
				} else {
					$newvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $usr, "A", $table, $fldname, $key, "", $newvalue);
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_user_list)) $telecare_user_list = new ctelecare_user_list();

// Page init
$telecare_user_list->Page_Init();

// Page main
$telecare_user_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_user_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($telecare_user->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = ftelecare_userlist = new ew_Form("ftelecare_userlist", "list");
ftelecare_userlist.FormKeyCountName = '<?php echo $telecare_user_list->FormKeyCountName ?>';

// Validate form
ftelecare_userlist.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_user_surname");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_surname->FldCaption(), $telecare_user->user_surname->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_name->FldCaption(), $telecare_user->user_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_gender");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_gender->FldCaption(), $telecare_user->user_gender->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_born");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_user->user_born->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_user_doctor_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_doctor_id->FldCaption(), $telecare_user->user_doctor_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_city_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_city_id->FldCaption(), $telecare_user->user_city_id->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	if (gridinsert && addcnt == 0) { // No row added
		ew_Alert(ewLanguage.Phrase("NoAddRecord"));
		return false;
	}
	return true;
}

// Check empty row
ftelecare_userlist.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "user_surname", false)) return false;
	if (ew_ValueChanged(fobj, infix, "user_name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "user_gender", false)) return false;
	if (ew_ValueChanged(fobj, infix, "user_born", false)) return false;
	if (ew_ValueChanged(fobj, infix, "user_doctor_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "user_city_id", false)) return false;
	return true;
}

// Form_CustomValidate event
ftelecare_userlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_userlist.ValidateRequired = true;
<?php } else { ?>
ftelecare_userlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_userlist.Lists["x_user_gender"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_userlist.Lists["x_user_gender"].Options = <?php echo json_encode($telecare_user->user_gender->Options()) ?>;
ftelecare_userlist.Lists["x_user_doctor_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_userlist.Lists["x_user_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = ftelecare_userlistsrch = new ew_Form("ftelecare_userlistsrch");
</script>
<style type="text/css">
.ewTablePreviewRow { /* main table preview row color */
	background-color: #FFFFFF; /* preview row color */
}
.ewTablePreviewRow .ewGrid {
	display: table;
}
.ewTablePreviewRow .ewGrid .ewTable {
	width: auto;
}
</style>
<div id="ewPreview" class="hide"><ul class="nav nav-tabs"></ul><div class="tab-content"><div class="tab-pane fade"></div></div></div>
<script type="text/javascript" src="phpjs/ewpreview.min.js"></script>
<script type="text/javascript">
var EW_PREVIEW_PLACEMENT = EW_CSS_FLIP ? "left" : "right";
var EW_PREVIEW_SINGLE_ROW = false;
var EW_PREVIEW_OVERLAY = false;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
<div class="ewToolbar">
<?php if ($telecare_user->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($telecare_user_list->TotalRecs > 0 && $telecare_user_list->ExportOptions->Visible()) { ?>
<?php $telecare_user_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_user_list->SearchOptions->Visible()) { ?>
<?php $telecare_user_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_user_list->FilterOptions->Visible()) { ?>
<?php $telecare_user_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
if ($telecare_user->CurrentAction == "gridadd") {
	$telecare_user->CurrentFilter = "0=1";
	$telecare_user_list->StartRec = 1;
	$telecare_user_list->DisplayRecs = $telecare_user->GridAddRowCount;
	$telecare_user_list->TotalRecs = $telecare_user_list->DisplayRecs;
	$telecare_user_list->StopRec = $telecare_user_list->DisplayRecs;
} else {
	$bSelectLimit = $telecare_user_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($telecare_user_list->TotalRecs <= 0)
			$telecare_user_list->TotalRecs = $telecare_user->SelectRecordCount();
	} else {
		if (!$telecare_user_list->Recordset && ($telecare_user_list->Recordset = $telecare_user_list->LoadRecordset()))
			$telecare_user_list->TotalRecs = $telecare_user_list->Recordset->RecordCount();
	}
	$telecare_user_list->StartRec = 1;
	if ($telecare_user_list->DisplayRecs <= 0 || ($telecare_user->Export <> "" && $telecare_user->ExportAll)) // Display all records
		$telecare_user_list->DisplayRecs = $telecare_user_list->TotalRecs;
	if (!($telecare_user->Export <> "" && $telecare_user->ExportAll))
		$telecare_user_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$telecare_user_list->Recordset = $telecare_user_list->LoadRecordset($telecare_user_list->StartRec-1, $telecare_user_list->DisplayRecs);

	// Set no record found message
	if ($telecare_user->CurrentAction == "" && $telecare_user_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$telecare_user_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($telecare_user_list->SearchWhere == "0=101")
			$telecare_user_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$telecare_user_list->setWarningMessage($Language->Phrase("NoRecord"));
	}

	// Audit trail on search
	if ($telecare_user_list->AuditTrailOnSearch && $telecare_user_list->Command == "search" && !$telecare_user_list->RestoreSearch) {
		$searchparm = ew_ServerVar("QUERY_STRING");
		$searchsql = $telecare_user_list->getSessionWhere();
		$telecare_user_list->WriteAuditTrailOnSearch($searchparm, $searchsql);
	}
}
$telecare_user_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($telecare_user->Export == "" && $telecare_user->CurrentAction == "") { ?>
<form name="ftelecare_userlistsrch" id="ftelecare_userlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($telecare_user_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="ftelecare_userlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="telecare_user">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($telecare_user_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($telecare_user_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $telecare_user_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($telecare_user_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($telecare_user_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($telecare_user_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($telecare_user_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $telecare_user_list->ShowPageHeader(); ?>
<?php
$telecare_user_list->ShowMessage();
?>
<?php if ($telecare_user_list->TotalRecs > 0 || $telecare_user->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<?php if ($telecare_user->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($telecare_user->CurrentAction <> "gridadd" && $telecare_user->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_user_list->Pager)) $telecare_user_list->Pager = new cPrevNextPager($telecare_user_list->StartRec, $telecare_user_list->DisplayRecs, $telecare_user_list->TotalRecs) ?>
<?php if ($telecare_user_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_user_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_user_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_user_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_user_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_user_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_user_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $telecare_user_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $telecare_user_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $telecare_user_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_user_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="ftelecare_userlist" id="ftelecare_userlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_user_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_user_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_user">
<div id="gmp_telecare_user" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($telecare_user_list->TotalRecs > 0) { ?>
<table id="tbl_telecare_userlist" class="table ewTable">
<?php echo $telecare_user->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$telecare_user_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$telecare_user_list->RenderListOptions();

// Render list options (header, left)
$telecare_user_list->ListOptions->Render("header", "left");
?>
<?php if ($telecare_user->user_id->Visible) { // user_id ?>
	<?php if ($telecare_user->SortUrl($telecare_user->user_id) == "") { ?>
		<th data-name="user_id"><div id="elh_telecare_user_user_id" class="telecare_user_user_id"><div class="ewTableHeaderCaption"><?php echo $telecare_user->user_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="user_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_user->SortUrl($telecare_user->user_id) ?>',1);"><div id="elh_telecare_user_user_id" class="telecare_user_user_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_user->user_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_user->user_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_user->user_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_user->user_surname->Visible) { // user_surname ?>
	<?php if ($telecare_user->SortUrl($telecare_user->user_surname) == "") { ?>
		<th data-name="user_surname"><div id="elh_telecare_user_user_surname" class="telecare_user_user_surname"><div class="ewTableHeaderCaption"><?php echo $telecare_user->user_surname->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="user_surname"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_user->SortUrl($telecare_user->user_surname) ?>',1);"><div id="elh_telecare_user_user_surname" class="telecare_user_user_surname">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_user->user_surname->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_user->user_surname->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_user->user_surname->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_user->user_name->Visible) { // user_name ?>
	<?php if ($telecare_user->SortUrl($telecare_user->user_name) == "") { ?>
		<th data-name="user_name"><div id="elh_telecare_user_user_name" class="telecare_user_user_name"><div class="ewTableHeaderCaption"><?php echo $telecare_user->user_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="user_name"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_user->SortUrl($telecare_user->user_name) ?>',1);"><div id="elh_telecare_user_user_name" class="telecare_user_user_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_user->user_name->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_user->user_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_user->user_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_user->user_gender->Visible) { // user_gender ?>
	<?php if ($telecare_user->SortUrl($telecare_user->user_gender) == "") { ?>
		<th data-name="user_gender"><div id="elh_telecare_user_user_gender" class="telecare_user_user_gender"><div class="ewTableHeaderCaption"><?php echo $telecare_user->user_gender->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="user_gender"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_user->SortUrl($telecare_user->user_gender) ?>',1);"><div id="elh_telecare_user_user_gender" class="telecare_user_user_gender">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_user->user_gender->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_user->user_gender->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_user->user_gender->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_user->user_born->Visible) { // user_born ?>
	<?php if ($telecare_user->SortUrl($telecare_user->user_born) == "") { ?>
		<th data-name="user_born"><div id="elh_telecare_user_user_born" class="telecare_user_user_born"><div class="ewTableHeaderCaption"><?php echo $telecare_user->user_born->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="user_born"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_user->SortUrl($telecare_user->user_born) ?>',1);"><div id="elh_telecare_user_user_born" class="telecare_user_user_born">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_user->user_born->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_user->user_born->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_user->user_born->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_user->user_doctor_id->Visible) { // user_doctor_id ?>
	<?php if ($telecare_user->SortUrl($telecare_user->user_doctor_id) == "") { ?>
		<th data-name="user_doctor_id"><div id="elh_telecare_user_user_doctor_id" class="telecare_user_user_doctor_id"><div class="ewTableHeaderCaption"><?php echo $telecare_user->user_doctor_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="user_doctor_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_user->SortUrl($telecare_user->user_doctor_id) ?>',1);"><div id="elh_telecare_user_user_doctor_id" class="telecare_user_user_doctor_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_user->user_doctor_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_user->user_doctor_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_user->user_doctor_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_user->user_city_id->Visible) { // user_city_id ?>
	<?php if ($telecare_user->SortUrl($telecare_user->user_city_id) == "") { ?>
		<th data-name="user_city_id"><div id="elh_telecare_user_user_city_id" class="telecare_user_user_city_id"><div class="ewTableHeaderCaption"><?php echo $telecare_user->user_city_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="user_city_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_user->SortUrl($telecare_user->user_city_id) ?>',1);"><div id="elh_telecare_user_user_city_id" class="telecare_user_user_city_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_user->user_city_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_user->user_city_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_user->user_city_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$telecare_user_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($telecare_user->ExportAll && $telecare_user->Export <> "") {
	$telecare_user_list->StopRec = $telecare_user_list->TotalRecs;
} else {

	// Set the last record to display
	if ($telecare_user_list->TotalRecs > $telecare_user_list->StartRec + $telecare_user_list->DisplayRecs - 1)
		$telecare_user_list->StopRec = $telecare_user_list->StartRec + $telecare_user_list->DisplayRecs - 1;
	else
		$telecare_user_list->StopRec = $telecare_user_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($telecare_user_list->FormKeyCountName) && ($telecare_user->CurrentAction == "gridadd" || $telecare_user->CurrentAction == "gridedit" || $telecare_user->CurrentAction == "F")) {
		$telecare_user_list->KeyCount = $objForm->GetValue($telecare_user_list->FormKeyCountName);
		$telecare_user_list->StopRec = $telecare_user_list->StartRec + $telecare_user_list->KeyCount - 1;
	}
}
$telecare_user_list->RecCnt = $telecare_user_list->StartRec - 1;
if ($telecare_user_list->Recordset && !$telecare_user_list->Recordset->EOF) {
	$telecare_user_list->Recordset->MoveFirst();
	$bSelectLimit = $telecare_user_list->UseSelectLimit;
	if (!$bSelectLimit && $telecare_user_list->StartRec > 1)
		$telecare_user_list->Recordset->Move($telecare_user_list->StartRec - 1);
} elseif (!$telecare_user->AllowAddDeleteRow && $telecare_user_list->StopRec == 0) {
	$telecare_user_list->StopRec = $telecare_user->GridAddRowCount;
}

// Initialize aggregate
$telecare_user->RowType = EW_ROWTYPE_AGGREGATEINIT;
$telecare_user->ResetAttrs();
$telecare_user_list->RenderRow();
if ($telecare_user->CurrentAction == "gridadd")
	$telecare_user_list->RowIndex = 0;
while ($telecare_user_list->RecCnt < $telecare_user_list->StopRec) {
	$telecare_user_list->RecCnt++;
	if (intval($telecare_user_list->RecCnt) >= intval($telecare_user_list->StartRec)) {
		$telecare_user_list->RowCnt++;
		if ($telecare_user->CurrentAction == "gridadd" || $telecare_user->CurrentAction == "gridedit" || $telecare_user->CurrentAction == "F") {
			$telecare_user_list->RowIndex++;
			$objForm->Index = $telecare_user_list->RowIndex;
			if ($objForm->HasValue($telecare_user_list->FormActionName))
				$telecare_user_list->RowAction = strval($objForm->GetValue($telecare_user_list->FormActionName));
			elseif ($telecare_user->CurrentAction == "gridadd")
				$telecare_user_list->RowAction = "insert";
			else
				$telecare_user_list->RowAction = "";
		}

		// Set up key count
		$telecare_user_list->KeyCount = $telecare_user_list->RowIndex;

		// Init row class and style
		$telecare_user->ResetAttrs();
		$telecare_user->CssClass = "";
		if ($telecare_user->CurrentAction == "gridadd") {
			$telecare_user_list->LoadDefaultValues(); // Load default values
		} else {
			$telecare_user_list->LoadRowValues($telecare_user_list->Recordset); // Load row values
		}
		$telecare_user->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($telecare_user->CurrentAction == "gridadd") // Grid add
			$telecare_user->RowType = EW_ROWTYPE_ADD; // Render add
		if ($telecare_user->CurrentAction == "gridadd" && $telecare_user->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$telecare_user_list->RestoreCurrentRowFormValues($telecare_user_list->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$telecare_user->RowAttrs = array_merge($telecare_user->RowAttrs, array('data-rowindex'=>$telecare_user_list->RowCnt, 'id'=>'r' . $telecare_user_list->RowCnt . '_telecare_user', 'data-rowtype'=>$telecare_user->RowType));

		// Render row
		$telecare_user_list->RenderRow();

		// Render list options
		$telecare_user_list->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($telecare_user_list->RowAction <> "delete" && $telecare_user_list->RowAction <> "insertdelete" && !($telecare_user_list->RowAction == "insert" && $telecare_user->CurrentAction == "F" && $telecare_user_list->EmptyRow())) {
?>
	<tr<?php echo $telecare_user->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_user_list->ListOptions->Render("body", "left", $telecare_user_list->RowCnt);
?>
	<?php if ($telecare_user->user_id->Visible) { // user_id ?>
		<td data-name="user_id"<?php echo $telecare_user->user_id->CellAttributes() ?>>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_user" data-field="x_user_id" name="o<?php echo $telecare_user_list->RowIndex ?>_user_id" id="o<?php echo $telecare_user_list->RowIndex ?>_user_id" value="<?php echo ew_HtmlEncode($telecare_user->user_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_user->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_id" class="telecare_user_user_id">
<span<?php echo $telecare_user->user_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_id->ListViewValue() ?></span>
</span>
<?php } ?>
<a id="<?php echo $telecare_user_list->PageObjName . "_row_" . $telecare_user_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($telecare_user->user_surname->Visible) { // user_surname ?>
		<td data-name="user_surname"<?php echo $telecare_user->user_surname->CellAttributes() ?>>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_surname" class="form-group telecare_user_user_surname">
<input type="text" data-table="telecare_user" data-field="x_user_surname" name="x<?php echo $telecare_user_list->RowIndex ?>_user_surname" id="x<?php echo $telecare_user_list->RowIndex ?>_user_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_surname->EditValue ?>"<?php echo $telecare_user->user_surname->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_surname" name="o<?php echo $telecare_user_list->RowIndex ?>_user_surname" id="o<?php echo $telecare_user_list->RowIndex ?>_user_surname" value="<?php echo ew_HtmlEncode($telecare_user->user_surname->OldValue) ?>">
<?php } ?>
<?php if ($telecare_user->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_surname" class="telecare_user_user_surname">
<span<?php echo $telecare_user->user_surname->ViewAttributes() ?>>
<?php echo $telecare_user->user_surname->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_user->user_name->Visible) { // user_name ?>
		<td data-name="user_name"<?php echo $telecare_user->user_name->CellAttributes() ?>>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_name" class="form-group telecare_user_user_name">
<input type="text" data-table="telecare_user" data-field="x_user_name" name="x<?php echo $telecare_user_list->RowIndex ?>_user_name" id="x<?php echo $telecare_user_list->RowIndex ?>_user_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_name->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_name->EditValue ?>"<?php echo $telecare_user->user_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_name" name="o<?php echo $telecare_user_list->RowIndex ?>_user_name" id="o<?php echo $telecare_user_list->RowIndex ?>_user_name" value="<?php echo ew_HtmlEncode($telecare_user->user_name->OldValue) ?>">
<?php } ?>
<?php if ($telecare_user->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_name" class="telecare_user_user_name">
<span<?php echo $telecare_user->user_name->ViewAttributes() ?>>
<?php echo $telecare_user->user_name->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_user->user_gender->Visible) { // user_gender ?>
		<td data-name="user_gender"<?php echo $telecare_user->user_gender->CellAttributes() ?>>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_gender" class="form-group telecare_user_user_gender">
<div id="tp_x<?php echo $telecare_user_list->RowIndex ?>_user_gender" class="ewTemplate"><input type="radio" data-table="telecare_user" data-field="x_user_gender" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_gender->DisplayValueSeparator) ? json_encode($telecare_user->user_gender->DisplayValueSeparator) : $telecare_user->user_gender->DisplayValueSeparator) ?>" name="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" value="{value}"<?php echo $telecare_user->user_gender->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_user_list->RowIndex ?>_user_gender" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_user->user_gender->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_user->user_gender->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_user" data-field="x_user_gender" name="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="x<?php echo $telecare_user_list->RowIndex ?>_user_gender_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_user->user_gender->EditAttributes() ?>><?php echo $telecare_user->user_gender->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_gender->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_user" data-field="x_user_gender" name="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="x<?php echo $telecare_user_list->RowIndex ?>_user_gender_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_user->user_gender->CurrentValue) ?>" checked<?php echo $telecare_user->user_gender->EditAttributes() ?>><?php echo $telecare_user->user_gender->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_user->user_gender->OldValue = "";
?>
</div></div>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_gender" name="o<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="o<?php echo $telecare_user_list->RowIndex ?>_user_gender" value="<?php echo ew_HtmlEncode($telecare_user->user_gender->OldValue) ?>">
<?php } ?>
<?php if ($telecare_user->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_gender" class="telecare_user_user_gender">
<span<?php echo $telecare_user->user_gender->ViewAttributes() ?>>
<?php echo $telecare_user->user_gender->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_user->user_born->Visible) { // user_born ?>
		<td data-name="user_born"<?php echo $telecare_user->user_born->CellAttributes() ?>>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_born" class="form-group telecare_user_user_born">
<input type="text" data-table="telecare_user" data-field="x_user_born" data-format="7" name="x<?php echo $telecare_user_list->RowIndex ?>_user_born" id="x<?php echo $telecare_user_list->RowIndex ?>_user_born" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_born->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_born->EditValue ?>"<?php echo $telecare_user->user_born->EditAttributes() ?>>
<?php if (!$telecare_user->user_born->ReadOnly && !$telecare_user->user_born->Disabled && !isset($telecare_user->user_born->EditAttrs["readonly"]) && !isset($telecare_user->user_born->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_userlist", "x<?php echo $telecare_user_list->RowIndex ?>_user_born", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_born" name="o<?php echo $telecare_user_list->RowIndex ?>_user_born" id="o<?php echo $telecare_user_list->RowIndex ?>_user_born" value="<?php echo ew_HtmlEncode($telecare_user->user_born->OldValue) ?>">
<?php } ?>
<?php if ($telecare_user->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_born" class="telecare_user_user_born">
<span<?php echo $telecare_user->user_born->ViewAttributes() ?>>
<?php echo $telecare_user->user_born->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_user->user_doctor_id->Visible) { // user_doctor_id ?>
		<td data-name="user_doctor_id"<?php echo $telecare_user->user_doctor_id->CellAttributes() ?>>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_doctor_id" class="form-group telecare_user_user_doctor_id">
<select data-table="telecare_user" data-field="x_user_doctor_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_doctor_id->DisplayValueSeparator) ? json_encode($telecare_user->user_doctor_id->DisplayValueSeparator) : $telecare_user->user_doctor_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" name="x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id"<?php echo $telecare_user->user_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_doctor_id->EditValue)) {
	$arwrk = $telecare_user->user_doctor_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_doctor_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_doctor_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_user->user_doctor_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = " `admin_level` = 2 ";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_user"]->UserIDAllow($GLOBALS["telecare_user"]->CurrentAction)) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_user->user_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_user->user_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" id="s_x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" value="<?php echo $telecare_user->user_doctor_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_doctor_id" name="o<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" id="o<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" value="<?php echo ew_HtmlEncode($telecare_user->user_doctor_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_user->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_doctor_id" class="telecare_user_user_doctor_id">
<span<?php echo $telecare_user->user_doctor_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_doctor_id->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_user->user_city_id->Visible) { // user_city_id ?>
		<td data-name="user_city_id"<?php echo $telecare_user->user_city_id->CellAttributes() ?>>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_city_id" class="form-group telecare_user_user_city_id">
<select data-table="telecare_user" data-field="x_user_city_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_city_id->DisplayValueSeparator) ? json_encode($telecare_user->user_city_id->DisplayValueSeparator) : $telecare_user->user_city_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_user_list->RowIndex ?>_user_city_id" name="x<?php echo $telecare_user_list->RowIndex ?>_user_city_id"<?php echo $telecare_user->user_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_city_id->EditValue)) {
	$arwrk = $telecare_user->user_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_city_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_city_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_city_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_user->user_city_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "";
		break;
}
$telecare_user->user_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `city_name` ASC";
if ($sSqlWrk <> "") $telecare_user->user_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_user_list->RowIndex ?>_user_city_id" id="s_x<?php echo $telecare_user_list->RowIndex ?>_user_city_id" value="<?php echo $telecare_user->user_city_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_city_id" name="o<?php echo $telecare_user_list->RowIndex ?>_user_city_id" id="o<?php echo $telecare_user_list->RowIndex ?>_user_city_id" value="<?php echo ew_HtmlEncode($telecare_user->user_city_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_user->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_user_list->RowCnt ?>_telecare_user_user_city_id" class="telecare_user_user_city_id">
<span<?php echo $telecare_user->user_city_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_city_id->ListViewValue() ?></span>
</span>
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_user_list->ListOptions->Render("body", "right", $telecare_user_list->RowCnt);
?>
	</tr>
<?php if ($telecare_user->RowType == EW_ROWTYPE_ADD || $telecare_user->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
ftelecare_userlist.UpdateOpts(<?php echo $telecare_user_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($telecare_user->CurrentAction <> "gridadd")
		if (!$telecare_user_list->Recordset->EOF) $telecare_user_list->Recordset->MoveNext();
}
?>
<?php
	if ($telecare_user->CurrentAction == "gridadd" || $telecare_user->CurrentAction == "gridedit") {
		$telecare_user_list->RowIndex = '$rowindex$';
		$telecare_user_list->LoadDefaultValues();

		// Set row properties
		$telecare_user->ResetAttrs();
		$telecare_user->RowAttrs = array_merge($telecare_user->RowAttrs, array('data-rowindex'=>$telecare_user_list->RowIndex, 'id'=>'r0_telecare_user', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($telecare_user->RowAttrs["class"], "ewTemplate");
		$telecare_user->RowType = EW_ROWTYPE_ADD;

		// Render row
		$telecare_user_list->RenderRow();

		// Render list options
		$telecare_user_list->RenderListOptions();
		$telecare_user_list->StartRowCnt = 0;
?>
	<tr<?php echo $telecare_user->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_user_list->ListOptions->Render("body", "left", $telecare_user_list->RowIndex);
?>
	<?php if ($telecare_user->user_id->Visible) { // user_id ?>
		<td data-name="user_id">
<input type="hidden" data-table="telecare_user" data-field="x_user_id" name="o<?php echo $telecare_user_list->RowIndex ?>_user_id" id="o<?php echo $telecare_user_list->RowIndex ?>_user_id" value="<?php echo ew_HtmlEncode($telecare_user->user_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_user->user_surname->Visible) { // user_surname ?>
		<td data-name="user_surname">
<span id="el$rowindex$_telecare_user_user_surname" class="form-group telecare_user_user_surname">
<input type="text" data-table="telecare_user" data-field="x_user_surname" name="x<?php echo $telecare_user_list->RowIndex ?>_user_surname" id="x<?php echo $telecare_user_list->RowIndex ?>_user_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_surname->EditValue ?>"<?php echo $telecare_user->user_surname->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_surname" name="o<?php echo $telecare_user_list->RowIndex ?>_user_surname" id="o<?php echo $telecare_user_list->RowIndex ?>_user_surname" value="<?php echo ew_HtmlEncode($telecare_user->user_surname->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_user->user_name->Visible) { // user_name ?>
		<td data-name="user_name">
<span id="el$rowindex$_telecare_user_user_name" class="form-group telecare_user_user_name">
<input type="text" data-table="telecare_user" data-field="x_user_name" name="x<?php echo $telecare_user_list->RowIndex ?>_user_name" id="x<?php echo $telecare_user_list->RowIndex ?>_user_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_name->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_name->EditValue ?>"<?php echo $telecare_user->user_name->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_name" name="o<?php echo $telecare_user_list->RowIndex ?>_user_name" id="o<?php echo $telecare_user_list->RowIndex ?>_user_name" value="<?php echo ew_HtmlEncode($telecare_user->user_name->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_user->user_gender->Visible) { // user_gender ?>
		<td data-name="user_gender">
<span id="el$rowindex$_telecare_user_user_gender" class="form-group telecare_user_user_gender">
<div id="tp_x<?php echo $telecare_user_list->RowIndex ?>_user_gender" class="ewTemplate"><input type="radio" data-table="telecare_user" data-field="x_user_gender" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_gender->DisplayValueSeparator) ? json_encode($telecare_user->user_gender->DisplayValueSeparator) : $telecare_user->user_gender->DisplayValueSeparator) ?>" name="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" value="{value}"<?php echo $telecare_user->user_gender->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_user_list->RowIndex ?>_user_gender" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_user->user_gender->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_user->user_gender->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_user" data-field="x_user_gender" name="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="x<?php echo $telecare_user_list->RowIndex ?>_user_gender_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_user->user_gender->EditAttributes() ?>><?php echo $telecare_user->user_gender->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_gender->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_user" data-field="x_user_gender" name="x<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="x<?php echo $telecare_user_list->RowIndex ?>_user_gender_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_user->user_gender->CurrentValue) ?>" checked<?php echo $telecare_user->user_gender->EditAttributes() ?>><?php echo $telecare_user->user_gender->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_user->user_gender->OldValue = "";
?>
</div></div>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_gender" name="o<?php echo $telecare_user_list->RowIndex ?>_user_gender" id="o<?php echo $telecare_user_list->RowIndex ?>_user_gender" value="<?php echo ew_HtmlEncode($telecare_user->user_gender->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_user->user_born->Visible) { // user_born ?>
		<td data-name="user_born">
<span id="el$rowindex$_telecare_user_user_born" class="form-group telecare_user_user_born">
<input type="text" data-table="telecare_user" data-field="x_user_born" data-format="7" name="x<?php echo $telecare_user_list->RowIndex ?>_user_born" id="x<?php echo $telecare_user_list->RowIndex ?>_user_born" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_born->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_born->EditValue ?>"<?php echo $telecare_user->user_born->EditAttributes() ?>>
<?php if (!$telecare_user->user_born->ReadOnly && !$telecare_user->user_born->Disabled && !isset($telecare_user->user_born->EditAttrs["readonly"]) && !isset($telecare_user->user_born->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_userlist", "x<?php echo $telecare_user_list->RowIndex ?>_user_born", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_born" name="o<?php echo $telecare_user_list->RowIndex ?>_user_born" id="o<?php echo $telecare_user_list->RowIndex ?>_user_born" value="<?php echo ew_HtmlEncode($telecare_user->user_born->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_user->user_doctor_id->Visible) { // user_doctor_id ?>
		<td data-name="user_doctor_id">
<span id="el$rowindex$_telecare_user_user_doctor_id" class="form-group telecare_user_user_doctor_id">
<select data-table="telecare_user" data-field="x_user_doctor_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_doctor_id->DisplayValueSeparator) ? json_encode($telecare_user->user_doctor_id->DisplayValueSeparator) : $telecare_user->user_doctor_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" name="x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id"<?php echo $telecare_user->user_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_doctor_id->EditValue)) {
	$arwrk = $telecare_user->user_doctor_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_doctor_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_doctor_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_user->user_doctor_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = " `admin_level` = 2 ";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_user"]->UserIDAllow($GLOBALS["telecare_user"]->CurrentAction)) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_user->user_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_user->user_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" id="s_x<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" value="<?php echo $telecare_user->user_doctor_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_doctor_id" name="o<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" id="o<?php echo $telecare_user_list->RowIndex ?>_user_doctor_id" value="<?php echo ew_HtmlEncode($telecare_user->user_doctor_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_user->user_city_id->Visible) { // user_city_id ?>
		<td data-name="user_city_id">
<span id="el$rowindex$_telecare_user_user_city_id" class="form-group telecare_user_user_city_id">
<select data-table="telecare_user" data-field="x_user_city_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_city_id->DisplayValueSeparator) ? json_encode($telecare_user->user_city_id->DisplayValueSeparator) : $telecare_user->user_city_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_user_list->RowIndex ?>_user_city_id" name="x<?php echo $telecare_user_list->RowIndex ?>_user_city_id"<?php echo $telecare_user->user_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_city_id->EditValue)) {
	$arwrk = $telecare_user->user_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_city_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_city_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_city_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_user->user_city_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "";
		break;
}
$telecare_user->user_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `city_name` ASC";
if ($sSqlWrk <> "") $telecare_user->user_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_user_list->RowIndex ?>_user_city_id" id="s_x<?php echo $telecare_user_list->RowIndex ?>_user_city_id" value="<?php echo $telecare_user->user_city_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_user" data-field="x_user_city_id" name="o<?php echo $telecare_user_list->RowIndex ?>_user_city_id" id="o<?php echo $telecare_user_list->RowIndex ?>_user_city_id" value="<?php echo ew_HtmlEncode($telecare_user->user_city_id->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_user_list->ListOptions->Render("body", "right", $telecare_user_list->RowCnt);
?>
<script type="text/javascript">
ftelecare_userlist.UpdateOpts(<?php echo $telecare_user_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($telecare_user->CurrentAction == "gridadd") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $telecare_user_list->FormKeyCountName ?>" id="<?php echo $telecare_user_list->FormKeyCountName ?>" value="<?php echo $telecare_user_list->KeyCount ?>">
<?php echo $telecare_user_list->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_user->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($telecare_user_list->Recordset)
	$telecare_user_list->Recordset->Close();
?>
<?php if ($telecare_user->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($telecare_user->CurrentAction <> "gridadd" && $telecare_user->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_user_list->Pager)) $telecare_user_list->Pager = new cPrevNextPager($telecare_user_list->StartRec, $telecare_user_list->DisplayRecs, $telecare_user_list->TotalRecs) ?>
<?php if ($telecare_user_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_user_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_user_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_user_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_user_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_user_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_user_list->PageUrl() ?>start=<?php echo $telecare_user_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_user_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $telecare_user_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $telecare_user_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $telecare_user_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_user_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($telecare_user_list->TotalRecs == 0 && $telecare_user->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_user_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
<script type="text/javascript">
ftelecare_userlistsrch.Init();
ftelecare_userlistsrch.FilterList = <?php echo $telecare_user_list->GetFilterList() ?>;
ftelecare_userlist.Init();
</script>
<?php } ?>
<?php
$telecare_user_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($telecare_user->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$telecare_user_list->Page_Terminate();
?>
