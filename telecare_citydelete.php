<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_cityinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_city_delete = NULL; // Initialize page object first

class ctelecare_city_delete extends ctelecare_city {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_city';

	// Page object name
	var $PageObjName = 'telecare_city_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_city)
		if (!isset($GLOBALS["telecare_city"]) || get_class($GLOBALS["telecare_city"]) == "ctelecare_city") {
			$GLOBALS["telecare_city"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_city"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_city', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_citylist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->city_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_city;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_city);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("telecare_citylist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in telecare_city class, telecare_cityinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->city_id->setDbValue($rs->fields('city_id'));
		$this->city_name->setDbValue($rs->fields('city_name'));
		$this->city_slug->setDbValue($rs->fields('city_slug'));
		$this->city_latitude->setDbValue($rs->fields('city_latitude'));
		$this->city_longitude->setDbValue($rs->fields('city_longitude'));
		$this->city_istat_code->setDbValue($rs->fields('city_istat_code'));
		$this->city_istat_alphanumeric_code->setDbValue($rs->fields('city_istat_alphanumeric_code'));
		$this->city_province_istat_code->setDbValue($rs->fields('city_province_istat_code'));
		$this->city_administrative_center->setDbValue($rs->fields('city_administrative_center'));
		$this->city_region_administrative_center->setDbValue($rs->fields('city_region_administrative_center'));
		$this->city_province_id->setDbValue($rs->fields('city_province_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->city_id->DbValue = $row['city_id'];
		$this->city_name->DbValue = $row['city_name'];
		$this->city_slug->DbValue = $row['city_slug'];
		$this->city_latitude->DbValue = $row['city_latitude'];
		$this->city_longitude->DbValue = $row['city_longitude'];
		$this->city_istat_code->DbValue = $row['city_istat_code'];
		$this->city_istat_alphanumeric_code->DbValue = $row['city_istat_alphanumeric_code'];
		$this->city_province_istat_code->DbValue = $row['city_province_istat_code'];
		$this->city_administrative_center->DbValue = $row['city_administrative_center'];
		$this->city_region_administrative_center->DbValue = $row['city_region_administrative_center'];
		$this->city_province_id->DbValue = $row['city_province_id'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// city_id
		// city_name
		// city_slug
		// city_latitude
		// city_longitude
		// city_istat_code
		// city_istat_alphanumeric_code
		// city_province_istat_code
		// city_administrative_center
		// city_region_administrative_center
		// city_province_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// city_id
		$this->city_id->ViewValue = $this->city_id->CurrentValue;
		$this->city_id->ViewCustomAttributes = "";

		// city_name
		$this->city_name->ViewValue = $this->city_name->CurrentValue;
		$this->city_name->ViewCustomAttributes = "";

		// city_slug
		$this->city_slug->ViewValue = $this->city_slug->CurrentValue;
		$this->city_slug->ViewCustomAttributes = "";

		// city_latitude
		$this->city_latitude->ViewValue = $this->city_latitude->CurrentValue;
		$this->city_latitude->ViewCustomAttributes = "";

		// city_longitude
		$this->city_longitude->ViewValue = $this->city_longitude->CurrentValue;
		$this->city_longitude->ViewCustomAttributes = "";

		// city_istat_code
		$this->city_istat_code->ViewValue = $this->city_istat_code->CurrentValue;
		$this->city_istat_code->ViewCustomAttributes = "";

		// city_istat_alphanumeric_code
		$this->city_istat_alphanumeric_code->ViewValue = $this->city_istat_alphanumeric_code->CurrentValue;
		$this->city_istat_alphanumeric_code->ViewCustomAttributes = "";

		// city_province_istat_code
		$this->city_province_istat_code->ViewValue = $this->city_province_istat_code->CurrentValue;
		$this->city_province_istat_code->ViewCustomAttributes = "";

		// city_administrative_center
		$this->city_administrative_center->ViewValue = $this->city_administrative_center->CurrentValue;
		$this->city_administrative_center->ViewCustomAttributes = "";

		// city_region_administrative_center
		$this->city_region_administrative_center->ViewValue = $this->city_region_administrative_center->CurrentValue;
		$this->city_region_administrative_center->ViewCustomAttributes = "";

		// city_province_id
		$this->city_province_id->ViewValue = $this->city_province_id->CurrentValue;
		$this->city_province_id->ViewCustomAttributes = "";

			// city_id
			$this->city_id->LinkCustomAttributes = "";
			$this->city_id->HrefValue = "";
			$this->city_id->TooltipValue = "";

			// city_name
			$this->city_name->LinkCustomAttributes = "";
			$this->city_name->HrefValue = "";
			$this->city_name->TooltipValue = "";

			// city_slug
			$this->city_slug->LinkCustomAttributes = "";
			$this->city_slug->HrefValue = "";
			$this->city_slug->TooltipValue = "";

			// city_latitude
			$this->city_latitude->LinkCustomAttributes = "";
			$this->city_latitude->HrefValue = "";
			$this->city_latitude->TooltipValue = "";

			// city_longitude
			$this->city_longitude->LinkCustomAttributes = "";
			$this->city_longitude->HrefValue = "";
			$this->city_longitude->TooltipValue = "";

			// city_istat_code
			$this->city_istat_code->LinkCustomAttributes = "";
			$this->city_istat_code->HrefValue = "";
			$this->city_istat_code->TooltipValue = "";

			// city_istat_alphanumeric_code
			$this->city_istat_alphanumeric_code->LinkCustomAttributes = "";
			$this->city_istat_alphanumeric_code->HrefValue = "";
			$this->city_istat_alphanumeric_code->TooltipValue = "";

			// city_province_istat_code
			$this->city_province_istat_code->LinkCustomAttributes = "";
			$this->city_province_istat_code->HrefValue = "";
			$this->city_province_istat_code->TooltipValue = "";

			// city_administrative_center
			$this->city_administrative_center->LinkCustomAttributes = "";
			$this->city_administrative_center->HrefValue = "";
			$this->city_administrative_center->TooltipValue = "";

			// city_region_administrative_center
			$this->city_region_administrative_center->LinkCustomAttributes = "";
			$this->city_region_administrative_center->HrefValue = "";
			$this->city_region_administrative_center->TooltipValue = "";

			// city_province_id
			$this->city_province_id->LinkCustomAttributes = "";
			$this->city_province_id->HrefValue = "";
			$this->city_province_id->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['city_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_citylist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_city_delete)) $telecare_city_delete = new ctelecare_city_delete();

// Page init
$telecare_city_delete->Page_Init();

// Page main
$telecare_city_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_city_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftelecare_citydelete = new ew_Form("ftelecare_citydelete", "delete");

// Form_CustomValidate event
ftelecare_citydelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_citydelete.ValidateRequired = true;
<?php } else { ?>
ftelecare_citydelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($telecare_city_delete->Recordset = $telecare_city_delete->LoadRecordset())
	$telecare_city_deleteTotalRecs = $telecare_city_delete->Recordset->RecordCount(); // Get record count
if ($telecare_city_deleteTotalRecs <= 0) { // No record found, exit
	if ($telecare_city_delete->Recordset)
		$telecare_city_delete->Recordset->Close();
	$telecare_city_delete->Page_Terminate("telecare_citylist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_city_delete->ShowPageHeader(); ?>
<?php
$telecare_city_delete->ShowMessage();
?>
<form name="ftelecare_citydelete" id="ftelecare_citydelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_city_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_city_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_city">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($telecare_city_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $telecare_city->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($telecare_city->city_id->Visible) { // city_id ?>
		<th><span id="elh_telecare_city_city_id" class="telecare_city_city_id"><?php echo $telecare_city->city_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_name->Visible) { // city_name ?>
		<th><span id="elh_telecare_city_city_name" class="telecare_city_city_name"><?php echo $telecare_city->city_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_slug->Visible) { // city_slug ?>
		<th><span id="elh_telecare_city_city_slug" class="telecare_city_city_slug"><?php echo $telecare_city->city_slug->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_latitude->Visible) { // city_latitude ?>
		<th><span id="elh_telecare_city_city_latitude" class="telecare_city_city_latitude"><?php echo $telecare_city->city_latitude->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_longitude->Visible) { // city_longitude ?>
		<th><span id="elh_telecare_city_city_longitude" class="telecare_city_city_longitude"><?php echo $telecare_city->city_longitude->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_istat_code->Visible) { // city_istat_code ?>
		<th><span id="elh_telecare_city_city_istat_code" class="telecare_city_city_istat_code"><?php echo $telecare_city->city_istat_code->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_istat_alphanumeric_code->Visible) { // city_istat_alphanumeric_code ?>
		<th><span id="elh_telecare_city_city_istat_alphanumeric_code" class="telecare_city_city_istat_alphanumeric_code"><?php echo $telecare_city->city_istat_alphanumeric_code->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_province_istat_code->Visible) { // city_province_istat_code ?>
		<th><span id="elh_telecare_city_city_province_istat_code" class="telecare_city_city_province_istat_code"><?php echo $telecare_city->city_province_istat_code->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_administrative_center->Visible) { // city_administrative_center ?>
		<th><span id="elh_telecare_city_city_administrative_center" class="telecare_city_city_administrative_center"><?php echo $telecare_city->city_administrative_center->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_region_administrative_center->Visible) { // city_region_administrative_center ?>
		<th><span id="elh_telecare_city_city_region_administrative_center" class="telecare_city_city_region_administrative_center"><?php echo $telecare_city->city_region_administrative_center->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_city->city_province_id->Visible) { // city_province_id ?>
		<th><span id="elh_telecare_city_city_province_id" class="telecare_city_city_province_id"><?php echo $telecare_city->city_province_id->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$telecare_city_delete->RecCnt = 0;
$i = 0;
while (!$telecare_city_delete->Recordset->EOF) {
	$telecare_city_delete->RecCnt++;
	$telecare_city_delete->RowCnt++;

	// Set row properties
	$telecare_city->ResetAttrs();
	$telecare_city->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$telecare_city_delete->LoadRowValues($telecare_city_delete->Recordset);

	// Render row
	$telecare_city_delete->RenderRow();
?>
	<tr<?php echo $telecare_city->RowAttributes() ?>>
<?php if ($telecare_city->city_id->Visible) { // city_id ?>
		<td<?php echo $telecare_city->city_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_id" class="telecare_city_city_id">
<span<?php echo $telecare_city->city_id->ViewAttributes() ?>>
<?php echo $telecare_city->city_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_name->Visible) { // city_name ?>
		<td<?php echo $telecare_city->city_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_name" class="telecare_city_city_name">
<span<?php echo $telecare_city->city_name->ViewAttributes() ?>>
<?php echo $telecare_city->city_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_slug->Visible) { // city_slug ?>
		<td<?php echo $telecare_city->city_slug->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_slug" class="telecare_city_city_slug">
<span<?php echo $telecare_city->city_slug->ViewAttributes() ?>>
<?php echo $telecare_city->city_slug->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_latitude->Visible) { // city_latitude ?>
		<td<?php echo $telecare_city->city_latitude->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_latitude" class="telecare_city_city_latitude">
<span<?php echo $telecare_city->city_latitude->ViewAttributes() ?>>
<?php echo $telecare_city->city_latitude->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_longitude->Visible) { // city_longitude ?>
		<td<?php echo $telecare_city->city_longitude->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_longitude" class="telecare_city_city_longitude">
<span<?php echo $telecare_city->city_longitude->ViewAttributes() ?>>
<?php echo $telecare_city->city_longitude->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_istat_code->Visible) { // city_istat_code ?>
		<td<?php echo $telecare_city->city_istat_code->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_istat_code" class="telecare_city_city_istat_code">
<span<?php echo $telecare_city->city_istat_code->ViewAttributes() ?>>
<?php echo $telecare_city->city_istat_code->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_istat_alphanumeric_code->Visible) { // city_istat_alphanumeric_code ?>
		<td<?php echo $telecare_city->city_istat_alphanumeric_code->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_istat_alphanumeric_code" class="telecare_city_city_istat_alphanumeric_code">
<span<?php echo $telecare_city->city_istat_alphanumeric_code->ViewAttributes() ?>>
<?php echo $telecare_city->city_istat_alphanumeric_code->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_province_istat_code->Visible) { // city_province_istat_code ?>
		<td<?php echo $telecare_city->city_province_istat_code->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_province_istat_code" class="telecare_city_city_province_istat_code">
<span<?php echo $telecare_city->city_province_istat_code->ViewAttributes() ?>>
<?php echo $telecare_city->city_province_istat_code->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_administrative_center->Visible) { // city_administrative_center ?>
		<td<?php echo $telecare_city->city_administrative_center->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_administrative_center" class="telecare_city_city_administrative_center">
<span<?php echo $telecare_city->city_administrative_center->ViewAttributes() ?>>
<?php echo $telecare_city->city_administrative_center->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_region_administrative_center->Visible) { // city_region_administrative_center ?>
		<td<?php echo $telecare_city->city_region_administrative_center->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_region_administrative_center" class="telecare_city_city_region_administrative_center">
<span<?php echo $telecare_city->city_region_administrative_center->ViewAttributes() ?>>
<?php echo $telecare_city->city_region_administrative_center->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_city->city_province_id->Visible) { // city_province_id ?>
		<td<?php echo $telecare_city->city_province_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_city_delete->RowCnt ?>_telecare_city_city_province_id" class="telecare_city_city_province_id">
<span<?php echo $telecare_city->city_province_id->ViewAttributes() ?>>
<?php echo $telecare_city->city_province_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$telecare_city_delete->Recordset->MoveNext();
}
$telecare_city_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_city_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftelecare_citydelete.Init();
</script>
<?php
$telecare_city_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_city_delete->Page_Terminate();
?>
