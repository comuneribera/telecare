<?php include_once "telecare_callinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php

//
// Page class
//

$telecare_call_grid = NULL; // Initialize page object first

class ctelecare_call_grid extends ctelecare_call {

	// Page ID
	var $PageID = 'grid';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_call';

	// Page object name
	var $PageObjName = 'telecare_call_grid';

	// Grid form hidden field names
	var $FormName = 'ftelecare_callgrid';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$this->FormActionName .= '_' . $this->FormName;
		$this->FormKeyName .= '_' . $this->FormName;
		$this->FormOldKeyName .= '_' . $this->FormName;
		$this->FormBlankRowName .= '_' . $this->FormName;
		$this->FormKeyCountName .= '_' . $this->FormName;
		$GLOBALS["Grid"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_call)
		if (!isset($GLOBALS["telecare_call"]) || get_class($GLOBALS["telecare_call"]) == "ctelecare_call") {
			$GLOBALS["telecare_call"] = &$this;

//			$GLOBALS["MasterTable"] = &$GLOBALS["Table"];
//			if (!isset($GLOBALS["Table"])) $GLOBALS["Table"] = &$GLOBALS["telecare_call"];

		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'grid', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_call', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();
		$this->call_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->call_admin_id->Visible = !$this->IsAddOrEdit();
		$this->call_last_update->Visible = !$this->IsAddOrEdit();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Set up master detail parameters
		$this->SetUpMasterParms();

		// Setup other options
		$this->SetupOtherOptions();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Export
		global $EW_EXPORT, $telecare_call;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_call);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}

//		$GLOBALS["Table"] = &$GLOBALS["MasterTable"];
		unset($GLOBALS["Grid"]);
		if ($url == "")
			return;
		$this->Page_Redirecting($url);

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $ShowOtherOptions = FALSE;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Handle reset command
			$this->ResetCmd();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Show grid delete link for grid add / grid edit
			if ($this->AllowAddDeleteRow) {
				if ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
					$item = $this->ListOptions->GetItem("griddelete");
					if ($item) $item->Visible = TRUE;
				}
			}

			// Set up sorting order
			$this->SetUpSortOrder();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records

		// Restore master/detail filter
		$this->DbMasterFilter = $this->GetMasterFilter(); // Restore master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Restore detail filter
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "telecare_user") {
			global $telecare_user;
			$rsmaster = $telecare_user->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("telecare_userlist.php"); // Return to master page
			} else {
				$telecare_user->LoadListRowValues($rsmaster);
				$telecare_user->RowType = EW_ROWTYPE_MASTER; // Master row
				$telecare_user->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Grid Add mode
	function GridAddMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridadd"; // Enabled grid add
	}

	// Switch to Grid Edit mode
	function GridEditMode() {
		$_SESSION[EW_SESSION_INLINE_MODE] = "gridedit"; // Enable grid edit
	}

	// Perform update to grid
	function GridUpdate() {
		global $Language, $objForm, $gsFormError;
		$bGridUpdate = TRUE;

		// Get old recordset
		$this->CurrentFilter = $this->BuildKeyFilter();
		if ($this->CurrentFilter == "")
			$this->CurrentFilter = "0=1";
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			$rsold = $rs->GetRows();
			$rs->Close();
		}

		// Call Grid Updating event
		if (!$this->Grid_Updating($rsold)) {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("GridEditCancelled")); // Set grid edit cancelled message
			return FALSE;
		}
		$sKey = "";

		// Update row index and get row key
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Update all rows based on key
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {
			$objForm->Index = $rowindex;
			$rowkey = strval($objForm->GetValue($this->FormKeyName));
			$rowaction = strval($objForm->GetValue($this->FormActionName));

			// Load all values and keys
			if ($rowaction <> "insertdelete") { // Skip insert then deleted rows
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "" || $rowaction == "edit" || $rowaction == "delete") {
					$bGridUpdate = $this->SetupKeyValues($rowkey); // Set up key values
				} else {
					$bGridUpdate = TRUE;
				}

				// Skip empty row
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// No action required
				// Validate form and insert/update/delete record

				} elseif ($bGridUpdate) {
					if ($rowaction == "delete") {
						$this->CurrentFilter = $this->KeyFilter();
						$bGridUpdate = $this->DeleteRows(); // Delete this row
					} else if (!$this->ValidateForm()) {
						$bGridUpdate = FALSE; // Form error, reset action
						$this->setFailureMessage($gsFormError);
					} else {
						if ($rowaction == "insert") {
							$bGridUpdate = $this->AddRow(); // Insert this row
						} else {
							if ($rowkey <> "") {
								$this->SendEmail = FALSE; // Do not send email on update success
								$bGridUpdate = $this->EditRow(); // Update this row
							}
						} // End update
					}
				}
				if ($bGridUpdate) {
					if ($sKey <> "") $sKey .= ", ";
					$sKey .= $rowkey;
				} else {
					break;
				}
			}
		}
		if ($bGridUpdate) {

			// Get new recordset
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Updated event
			$this->Grid_Updated($rsold, $rsnew);
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
		}
		return $bGridUpdate;
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->call_id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->call_id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Perform Grid Add
	function GridInsert() {
		global $Language, $objForm, $gsFormError;
		$rowindex = 1;
		$bGridInsert = FALSE;
		$conn = &$this->Connection();

		// Call Grid Inserting event
		if (!$this->Grid_Inserting()) {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("GridAddCancelled")); // Set grid add cancelled message
			}
			return FALSE;
		}

		// Init key filter
		$sWrkFilter = "";
		$addcnt = 0;
		$sKey = "";

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Insert all rows
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "" && $rowaction <> "insert")
				continue; // Skip
			if ($rowaction == "insert") {
				$this->RowOldKey = strval($objForm->GetValue($this->FormOldKeyName));
				$this->LoadOldRecord(); // Load old recordset
			}
			$this->LoadFormValues(); // Get form values
			if (!$this->EmptyRow()) {
				$addcnt++;
				$this->SendEmail = FALSE; // Do not send email on insert success

				// Validate form
				if (!$this->ValidateForm()) {
					$bGridInsert = FALSE; // Form error, reset action
					$this->setFailureMessage($gsFormError);
				} else {
					$bGridInsert = $this->AddRow($this->OldRecordset); // Insert this row
				}
				if ($bGridInsert) {
					if ($sKey <> "") $sKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
					$sKey .= $this->call_id->CurrentValue;

					// Add filter for this record
					$sFilter = $this->KeyFilter();
					if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
					$sWrkFilter .= $sFilter;
				} else {
					break;
				}
			}
		}
		if ($addcnt == 0) { // No record inserted
			$this->ClearInlineMode(); // Clear grid add mode and return
			return TRUE;
		}
		if ($bGridInsert) {

			// Get new recordset
			$this->CurrentFilter = $sWrkFilter;
			$sSql = $this->SQL();
			if ($rs = $conn->Execute($sSql)) {
				$rsnew = $rs->GetRows();
				$rs->Close();
			}

			// Call Grid_Inserted event
			$this->Grid_Inserted($rsnew);
			$this->ClearInlineMode(); // Clear grid add mode
		} else {
			if ($this->getFailureMessage() == "") {
				$this->setFailureMessage($Language->Phrase("InsertFailed")); // Set insert failed message
			}
		}
		return $bGridInsert;
	}

	// Check if empty row
	function EmptyRow() {
		global $objForm;
		if ($objForm->HasValue("x_call_user_id") && $objForm->HasValue("o_call_user_id") && $this->call_user_id->CurrentValue <> $this->call_user_id->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_call_requested_date") && $objForm->HasValue("o_call_requested_date") && $this->call_requested_date->CurrentValue <> $this->call_requested_date->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_call_start_on") && $objForm->HasValue("o_call_start_on") && $this->call_start_on->CurrentValue <> $this->call_start_on->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_call_close_on") && $objForm->HasValue("o_call_close_on") && $this->call_close_on->CurrentValue <> $this->call_close_on->OldValue)
			return FALSE;
		if ($objForm->HasValue("x_call_closed") && $objForm->HasValue("o_call_closed") && $this->call_closed->CurrentValue <> $this->call_closed->OldValue)
			return FALSE;
		return TRUE;
	}

	// Validate grid form
	function ValidateGridForm() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;

		// Validate all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else if (!$this->ValidateForm()) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	// Get all form values of the grid
	function GetGridFormValues() {
		global $objForm;

		// Get row count
		$objForm->Index = -1;
		$rowcnt = strval($objForm->GetValue($this->FormKeyCountName));
		if ($rowcnt == "" || !is_numeric($rowcnt))
			$rowcnt = 0;
		$rows = array();

		// Loop through all records
		for ($rowindex = 1; $rowindex <= $rowcnt; $rowindex++) {

			// Load current row values
			$objForm->Index = $rowindex;
			$rowaction = strval($objForm->GetValue($this->FormActionName));
			if ($rowaction <> "delete" && $rowaction <> "insertdelete") {
				$this->LoadFormValues(); // Get form values
				if ($rowaction == "insert" && $this->EmptyRow()) {

					// Ignore
				} else {
					$rows[] = $this->GetFieldValues("FormValue"); // Return row as array
				}
			}
		}
		return $rows; // Return as array of array
	}

	// Restore form values for current row
	function RestoreCurrentRowFormValues($idx) {
		global $objForm;

		// Get row based on current index
		$objForm->Index = $idx;
		$this->LoadFormValues(); // Load form values
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset master/detail keys
			if ($this->Command == "resetall") {
				$this->setCurrentMasterTable(""); // Clear master table
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
				$this->call_user_id->setSessionValue("");
			}

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "griddelete"
		if ($this->AllowAddDeleteRow) {
			$item = &$this->ListOptions->Add("griddelete");
			$item->CssStyle = "white-space: nowrap;";
			$item->OnLeft = TRUE;
			$item->Visible = FALSE; // Default hidden
		}

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex) && $this->CurrentMode <> "view") {
			$objForm->Index = $this->RowIndex;
			$ActionName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormActionName);
			$OldKeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormOldKeyName);
			$KeyName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormKeyName);
			$BlankRowName = str_replace("k_", "k" . $this->RowIndex . "_", $this->FormBlankRowName);
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $ActionName . "\" id=\"" . $ActionName . "\" value=\"" . $this->RowAction . "\">";
			if ($objForm->HasValue($this->FormOldKeyName))
				$this->RowOldKey = strval($objForm->GetValue($this->FormOldKeyName));
			if ($this->RowOldKey <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $OldKeyName . "\" id=\"" . $OldKeyName . "\" value=\"" . ew_HtmlEncode($this->RowOldKey) . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue($this->FormKeyName);
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $BlankRowName . "\" id=\"" . $BlankRowName . "\" value=\"1\">";
		}

		// "delete"
		if ($this->AllowAddDeleteRow) {
			if ($this->CurrentMode == "add" || $this->CurrentMode == "copy" || $this->CurrentMode == "edit") {
				$option = &$this->ListOptions;
				$option->UseButtonGroup = TRUE; // Use button group for grid delete button
				$option->UseImageAndText = TRUE; // Use image and text for grid delete button
				$oListOpt = &$option->Items["griddelete"];
				if (!$Security->CanDelete() && is_numeric($this->RowIndex) && ($this->RowAction == "" || $this->RowAction == "edit")) { // Do not allow delete existing record
					$oListOpt->Body = "&nbsp;";
				} else {
					$oListOpt->Body = "<a class=\"ewGridLink ewGridDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" onclick=\"return ew_DeleteGridRow(this, " . $this->RowIndex . ");\">" . $Language->Phrase("DeleteLink") . "</a>";
				}
			}
		}
		if ($this->CurrentMode == "edit" && is_numeric($this->RowIndex)) {
			$this->MultiSelectKey .= "<input type=\"hidden\" name=\"" . $KeyName . "\" id=\"" . $KeyName . "\" value=\"" . $this->call_id->CurrentValue . "\">";
		}
		$this->RenderListOptionsExt();
	}

	// Set record key
	function SetRecordKey(&$key, $rs) {
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs->fields('call_id');
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$option = &$this->OtherOptions["addedit"];
		$option->UseDropDownButton = FALSE;
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$option->UseButtonGroup = TRUE;
		$option->ButtonClass = "btn-sm"; // Class for button group
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		if (($this->CurrentMode == "add" || $this->CurrentMode == "copy" || $this->CurrentMode == "edit") && $this->CurrentAction != "F") { // Check add/copy/edit mode
			if ($this->AllowAddDeleteRow) {
				$option = &$options["addedit"];
				$option->UseDropDownButton = FALSE;
				$option->UseImageAndText = TRUE;
				$item = &$option->Add("addblankrow");
				$item->Body = "<a class=\"ewAddEdit ewAddBlankRow\" title=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddBlankRow")) . "\" href=\"javascript:void(0);\" onclick=\"ew_AddGridRow(this);\">" . $Language->Phrase("AddBlankRow") . "</a>";
				$item->Visible = $Security->CanAdd();
				$this->ShowOtherOptions = $item->Visible;
			}
		}
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->call_id->CurrentValue = NULL;
		$this->call_id->OldValue = $this->call_id->CurrentValue;
		$this->call_user_id->CurrentValue = NULL;
		$this->call_user_id->OldValue = $this->call_user_id->CurrentValue;
		$this->call_admin_id->CurrentValue = NULL;
		$this->call_admin_id->OldValue = $this->call_admin_id->CurrentValue;
		$this->call_requested_date->CurrentValue = NULL;
		$this->call_requested_date->OldValue = $this->call_requested_date->CurrentValue;
		$this->call_start_on->CurrentValue = NULL;
		$this->call_start_on->OldValue = $this->call_start_on->CurrentValue;
		$this->call_close_on->CurrentValue = NULL;
		$this->call_close_on->OldValue = $this->call_close_on->CurrentValue;
		$this->call_closed->CurrentValue = 0;
		$this->call_closed->OldValue = $this->call_closed->CurrentValue;
		$this->call_last_update->CurrentValue = NULL;
		$this->call_last_update->OldValue = $this->call_last_update->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$objForm->FormName = $this->FormName;
		if (!$this->call_id->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->call_id->setFormValue($objForm->GetValue("x_call_id"));
		if (!$this->call_user_id->FldIsDetailKey) {
			$this->call_user_id->setFormValue($objForm->GetValue("x_call_user_id"));
		}
		$this->call_user_id->setOldValue($objForm->GetValue("o_call_user_id"));
		if (!$this->call_admin_id->FldIsDetailKey) {
			$this->call_admin_id->setFormValue($objForm->GetValue("x_call_admin_id"));
		}
		$this->call_admin_id->setOldValue($objForm->GetValue("o_call_admin_id"));
		if (!$this->call_requested_date->FldIsDetailKey) {
			$this->call_requested_date->setFormValue($objForm->GetValue("x_call_requested_date"));
			$this->call_requested_date->CurrentValue = ew_UnFormatDateTime($this->call_requested_date->CurrentValue, 7);
		}
		$this->call_requested_date->setOldValue($objForm->GetValue("o_call_requested_date"));
		if (!$this->call_start_on->FldIsDetailKey) {
			$this->call_start_on->setFormValue($objForm->GetValue("x_call_start_on"));
			$this->call_start_on->CurrentValue = ew_UnFormatDateTime($this->call_start_on->CurrentValue, 7);
		}
		$this->call_start_on->setOldValue($objForm->GetValue("o_call_start_on"));
		if (!$this->call_close_on->FldIsDetailKey) {
			$this->call_close_on->setFormValue($objForm->GetValue("x_call_close_on"));
			$this->call_close_on->CurrentValue = ew_UnFormatDateTime($this->call_close_on->CurrentValue, 7);
		}
		$this->call_close_on->setOldValue($objForm->GetValue("o_call_close_on"));
		if (!$this->call_closed->FldIsDetailKey) {
			$this->call_closed->setFormValue($objForm->GetValue("x_call_closed"));
		}
		$this->call_closed->setOldValue($objForm->GetValue("o_call_closed"));
		if (!$this->call_last_update->FldIsDetailKey) {
			$this->call_last_update->setFormValue($objForm->GetValue("x_call_last_update"));
			$this->call_last_update->CurrentValue = ew_UnFormatDateTime($this->call_last_update->CurrentValue, 7);
		}
		$this->call_last_update->setOldValue($objForm->GetValue("o_call_last_update"));
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->call_id->CurrentValue = $this->call_id->FormValue;
		$this->call_user_id->CurrentValue = $this->call_user_id->FormValue;
		$this->call_admin_id->CurrentValue = $this->call_admin_id->FormValue;
		$this->call_requested_date->CurrentValue = $this->call_requested_date->FormValue;
		$this->call_requested_date->CurrentValue = ew_UnFormatDateTime($this->call_requested_date->CurrentValue, 7);
		$this->call_start_on->CurrentValue = $this->call_start_on->FormValue;
		$this->call_start_on->CurrentValue = ew_UnFormatDateTime($this->call_start_on->CurrentValue, 7);
		$this->call_close_on->CurrentValue = $this->call_close_on->FormValue;
		$this->call_close_on->CurrentValue = ew_UnFormatDateTime($this->call_close_on->CurrentValue, 7);
		$this->call_closed->CurrentValue = $this->call_closed->FormValue;
		$this->call_last_update->CurrentValue = $this->call_last_update->FormValue;
		$this->call_last_update->CurrentValue = ew_UnFormatDateTime($this->call_last_update->CurrentValue, 7);
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->call_id->setDbValue($rs->fields('call_id'));
		$this->call_user_id->setDbValue($rs->fields('call_user_id'));
		$this->call_admin_id->setDbValue($rs->fields('call_admin_id'));
		$this->call_requested_date->setDbValue($rs->fields('call_requested_date'));
		$this->call_start_on->setDbValue($rs->fields('call_start_on'));
		$this->call_close_on->setDbValue($rs->fields('call_close_on'));
		$this->call_user_note->setDbValue($rs->fields('call_user_note'));
		$this->call_admin_note->setDbValue($rs->fields('call_admin_note'));
		$this->call_closed->setDbValue($rs->fields('call_closed'));
		$this->call_last_update->setDbValue($rs->fields('call_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->call_id->DbValue = $row['call_id'];
		$this->call_user_id->DbValue = $row['call_user_id'];
		$this->call_admin_id->DbValue = $row['call_admin_id'];
		$this->call_requested_date->DbValue = $row['call_requested_date'];
		$this->call_start_on->DbValue = $row['call_start_on'];
		$this->call_close_on->DbValue = $row['call_close_on'];
		$this->call_user_note->DbValue = $row['call_user_note'];
		$this->call_admin_note->DbValue = $row['call_admin_note'];
		$this->call_closed->DbValue = $row['call_closed'];
		$this->call_last_update->DbValue = $row['call_last_update'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		$arKeys[] = $this->RowOldKey;
		$cnt = count($arKeys);
		if ($cnt >= 1) {
			if (strval($arKeys[0]) <> "")
				$this->call_id->CurrentValue = strval($arKeys[0]); // call_id
			else
				$bValidKey = FALSE;
		} else {
			$bValidKey = FALSE;
		}

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// call_id
		// call_user_id
		// call_admin_id
		// call_requested_date
		// call_start_on
		// call_close_on
		// call_user_note
		// call_admin_note
		// call_closed
		// call_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// call_id
		$this->call_id->ViewValue = $this->call_id->CurrentValue;
		$this->call_id->ViewCustomAttributes = "";

		// call_user_id
		$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
		if (strval($this->call_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->call_user_id->ViewValue = $this->call_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
			}
		} else {
			$this->call_user_id->ViewValue = NULL;
		}
		$this->call_user_id->ViewCustomAttributes = "";

		// call_admin_id
		if (strval($this->call_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->call_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->call_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->call_admin_id->ViewValue = $this->call_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->call_admin_id->ViewValue = $this->call_admin_id->CurrentValue;
			}
		} else {
			$this->call_admin_id->ViewValue = NULL;
		}
		$this->call_admin_id->ViewCustomAttributes = "";

		// call_requested_date
		$this->call_requested_date->ViewValue = $this->call_requested_date->CurrentValue;
		$this->call_requested_date->ViewValue = ew_FormatDateTime($this->call_requested_date->ViewValue, 7);
		$this->call_requested_date->ViewCustomAttributes = "";

		// call_start_on
		$this->call_start_on->ViewValue = $this->call_start_on->CurrentValue;
		$this->call_start_on->ViewValue = ew_FormatDateTime($this->call_start_on->ViewValue, 7);
		$this->call_start_on->ViewCustomAttributes = "";

		// call_close_on
		$this->call_close_on->ViewValue = $this->call_close_on->CurrentValue;
		$this->call_close_on->ViewValue = ew_FormatDateTime($this->call_close_on->ViewValue, 7);
		$this->call_close_on->ViewCustomAttributes = "";

		// call_closed
		if (strval($this->call_closed->CurrentValue) <> "") {
			$this->call_closed->ViewValue = $this->call_closed->OptionCaption($this->call_closed->CurrentValue);
		} else {
			$this->call_closed->ViewValue = NULL;
		}
		$this->call_closed->ViewCustomAttributes = "";

		// call_last_update
		$this->call_last_update->ViewValue = $this->call_last_update->CurrentValue;
		$this->call_last_update->ViewValue = ew_FormatDateTime($this->call_last_update->ViewValue, 7);
		$this->call_last_update->ViewCustomAttributes = "";

			// call_id
			$this->call_id->LinkCustomAttributes = "";
			$this->call_id->HrefValue = "";
			$this->call_id->TooltipValue = "";

			// call_user_id
			$this->call_user_id->LinkCustomAttributes = "";
			$this->call_user_id->HrefValue = "";
			$this->call_user_id->TooltipValue = "";

			// call_admin_id
			$this->call_admin_id->LinkCustomAttributes = "";
			$this->call_admin_id->HrefValue = "";
			$this->call_admin_id->TooltipValue = "";

			// call_requested_date
			$this->call_requested_date->LinkCustomAttributes = "";
			$this->call_requested_date->HrefValue = "";
			$this->call_requested_date->TooltipValue = "";

			// call_start_on
			$this->call_start_on->LinkCustomAttributes = "";
			$this->call_start_on->HrefValue = "";
			$this->call_start_on->TooltipValue = "";

			// call_close_on
			$this->call_close_on->LinkCustomAttributes = "";
			$this->call_close_on->HrefValue = "";
			$this->call_close_on->TooltipValue = "";

			// call_closed
			$this->call_closed->LinkCustomAttributes = "";
			$this->call_closed->HrefValue = "";
			$this->call_closed->TooltipValue = "";

			// call_last_update
			$this->call_last_update->LinkCustomAttributes = "";
			$this->call_last_update->HrefValue = "";
			$this->call_last_update->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// call_id
			// call_user_id

			$this->call_user_id->EditAttrs["class"] = "form-control";
			$this->call_user_id->EditCustomAttributes = "";
			if ($this->call_user_id->getSessionValue() <> "") {
				$this->call_user_id->CurrentValue = $this->call_user_id->getSessionValue();
				$this->call_user_id->OldValue = $this->call_user_id->CurrentValue;
			$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
			if (strval($this->call_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->call_user_id->ViewValue = $this->call_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
				}
			} else {
				$this->call_user_id->ViewValue = NULL;
			}
			$this->call_user_id->ViewCustomAttributes = "";
			} else {
			$this->call_user_id->EditValue = ew_HtmlEncode($this->call_user_id->CurrentValue);
			if (strval($this->call_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
					$arwrk[2] = ew_HtmlEncode($rswrk->fields('Disp2Fld'));
					$this->call_user_id->EditValue = $this->call_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->call_user_id->EditValue = ew_HtmlEncode($this->call_user_id->CurrentValue);
				}
			} else {
				$this->call_user_id->EditValue = NULL;
			}
			$this->call_user_id->PlaceHolder = ew_RemoveHtml($this->call_user_id->FldCaption());
			}

			// call_admin_id
			// call_requested_date

			$this->call_requested_date->EditAttrs["class"] = "form-control";
			$this->call_requested_date->EditCustomAttributes = "";
			$this->call_requested_date->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_requested_date->CurrentValue, 7));
			$this->call_requested_date->PlaceHolder = ew_RemoveHtml($this->call_requested_date->FldCaption());

			// call_start_on
			$this->call_start_on->EditAttrs["class"] = "form-control";
			$this->call_start_on->EditCustomAttributes = "";
			$this->call_start_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_start_on->CurrentValue, 7));
			$this->call_start_on->PlaceHolder = ew_RemoveHtml($this->call_start_on->FldCaption());

			// call_close_on
			$this->call_close_on->EditAttrs["class"] = "form-control";
			$this->call_close_on->EditCustomAttributes = "";
			$this->call_close_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_close_on->CurrentValue, 7));
			$this->call_close_on->PlaceHolder = ew_RemoveHtml($this->call_close_on->FldCaption());

			// call_closed
			$this->call_closed->EditCustomAttributes = "";
			$this->call_closed->EditValue = $this->call_closed->Options(FALSE);

			// call_last_update
			// Edit refer script
			// call_id

			$this->call_id->LinkCustomAttributes = "";
			$this->call_id->HrefValue = "";

			// call_user_id
			$this->call_user_id->LinkCustomAttributes = "";
			$this->call_user_id->HrefValue = "";

			// call_admin_id
			$this->call_admin_id->LinkCustomAttributes = "";
			$this->call_admin_id->HrefValue = "";

			// call_requested_date
			$this->call_requested_date->LinkCustomAttributes = "";
			$this->call_requested_date->HrefValue = "";

			// call_start_on
			$this->call_start_on->LinkCustomAttributes = "";
			$this->call_start_on->HrefValue = "";

			// call_close_on
			$this->call_close_on->LinkCustomAttributes = "";
			$this->call_close_on->HrefValue = "";

			// call_closed
			$this->call_closed->LinkCustomAttributes = "";
			$this->call_closed->HrefValue = "";

			// call_last_update
			$this->call_last_update->LinkCustomAttributes = "";
			$this->call_last_update->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// call_id
			$this->call_id->EditAttrs["class"] = "form-control";
			$this->call_id->EditCustomAttributes = "";
			$this->call_id->EditValue = $this->call_id->CurrentValue;
			$this->call_id->ViewCustomAttributes = "";

			// call_user_id
			$this->call_user_id->EditAttrs["class"] = "form-control";
			$this->call_user_id->EditCustomAttributes = "";
			if ($this->call_user_id->getSessionValue() <> "") {
				$this->call_user_id->CurrentValue = $this->call_user_id->getSessionValue();
				$this->call_user_id->OldValue = $this->call_user_id->CurrentValue;
			$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
			if (strval($this->call_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->call_user_id->ViewValue = $this->call_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
				}
			} else {
				$this->call_user_id->ViewValue = NULL;
			}
			$this->call_user_id->ViewCustomAttributes = "";
			} else {
			$this->call_user_id->EditValue = ew_HtmlEncode($this->call_user_id->CurrentValue);
			if (strval($this->call_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
					$arwrk[2] = ew_HtmlEncode($rswrk->fields('Disp2Fld'));
					$this->call_user_id->EditValue = $this->call_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->call_user_id->EditValue = ew_HtmlEncode($this->call_user_id->CurrentValue);
				}
			} else {
				$this->call_user_id->EditValue = NULL;
			}
			$this->call_user_id->PlaceHolder = ew_RemoveHtml($this->call_user_id->FldCaption());
			}

			// call_admin_id
			// call_requested_date

			$this->call_requested_date->EditAttrs["class"] = "form-control";
			$this->call_requested_date->EditCustomAttributes = "";
			$this->call_requested_date->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_requested_date->CurrentValue, 7));
			$this->call_requested_date->PlaceHolder = ew_RemoveHtml($this->call_requested_date->FldCaption());

			// call_start_on
			$this->call_start_on->EditAttrs["class"] = "form-control";
			$this->call_start_on->EditCustomAttributes = "";
			$this->call_start_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_start_on->CurrentValue, 7));
			$this->call_start_on->PlaceHolder = ew_RemoveHtml($this->call_start_on->FldCaption());

			// call_close_on
			$this->call_close_on->EditAttrs["class"] = "form-control";
			$this->call_close_on->EditCustomAttributes = "";
			$this->call_close_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_close_on->CurrentValue, 7));
			$this->call_close_on->PlaceHolder = ew_RemoveHtml($this->call_close_on->FldCaption());

			// call_closed
			$this->call_closed->EditCustomAttributes = "";
			$this->call_closed->EditValue = $this->call_closed->Options(FALSE);

			// call_last_update
			// Edit refer script
			// call_id

			$this->call_id->LinkCustomAttributes = "";
			$this->call_id->HrefValue = "";

			// call_user_id
			$this->call_user_id->LinkCustomAttributes = "";
			$this->call_user_id->HrefValue = "";

			// call_admin_id
			$this->call_admin_id->LinkCustomAttributes = "";
			$this->call_admin_id->HrefValue = "";

			// call_requested_date
			$this->call_requested_date->LinkCustomAttributes = "";
			$this->call_requested_date->HrefValue = "";

			// call_start_on
			$this->call_start_on->LinkCustomAttributes = "";
			$this->call_start_on->HrefValue = "";

			// call_close_on
			$this->call_close_on->LinkCustomAttributes = "";
			$this->call_close_on->HrefValue = "";

			// call_closed
			$this->call_closed->LinkCustomAttributes = "";
			$this->call_closed->HrefValue = "";

			// call_last_update
			$this->call_last_update->LinkCustomAttributes = "";
			$this->call_last_update->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckInteger($this->call_user_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_user_id->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->call_requested_date->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_requested_date->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->call_start_on->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_start_on->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->call_close_on->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_close_on->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['call_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
		} else {
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// call_user_id
			$this->call_user_id->SetDbValueDef($rsnew, $this->call_user_id->CurrentValue, NULL, $this->call_user_id->ReadOnly);

			// call_admin_id
			$this->call_admin_id->SetDbValueDef($rsnew, CurrentUserID(), NULL);
			$rsnew['call_admin_id'] = &$this->call_admin_id->DbValue;

			// call_requested_date
			$this->call_requested_date->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_requested_date->CurrentValue, 7), NULL, $this->call_requested_date->ReadOnly);

			// call_start_on
			$this->call_start_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_start_on->CurrentValue, 7), NULL, $this->call_start_on->ReadOnly);

			// call_close_on
			$this->call_close_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_close_on->CurrentValue, 7), NULL, $this->call_close_on->ReadOnly);

			// call_closed
			$this->call_closed->SetDbValueDef($rsnew, $this->call_closed->CurrentValue, NULL, $this->call_closed->ReadOnly);

			// call_last_update
			$this->call_last_update->SetDbValueDef($rsnew, ew_CurrentDateTime(), NULL);
			$rsnew['call_last_update'] = &$this->call_last_update->DbValue;

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;

		// Set up foreign key field value from Session
			if ($this->getCurrentMasterTable() == "telecare_user") {
				$this->call_user_id->CurrentValue = $this->call_user_id->getSessionValue();
			}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// call_user_id
		$this->call_user_id->SetDbValueDef($rsnew, $this->call_user_id->CurrentValue, NULL, FALSE);

		// call_admin_id
		$this->call_admin_id->SetDbValueDef($rsnew, CurrentUserID(), NULL);
		$rsnew['call_admin_id'] = &$this->call_admin_id->DbValue;

		// call_requested_date
		$this->call_requested_date->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_requested_date->CurrentValue, 7), NULL, FALSE);

		// call_start_on
		$this->call_start_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_start_on->CurrentValue, 7), NULL, FALSE);

		// call_close_on
		$this->call_close_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_close_on->CurrentValue, 7), NULL, FALSE);

		// call_closed
		$this->call_closed->SetDbValueDef($rsnew, $this->call_closed->CurrentValue, NULL, strval($this->call_closed->CurrentValue) == "");

		// call_last_update
		$this->call_last_update->SetDbValueDef($rsnew, ew_CurrentDateTime(), NULL);
		$rsnew['call_last_update'] = &$this->call_last_update->DbValue;

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->call_id->setDbValue($conn->Insert_ID());
				$rsnew['call_id'] = $this->call_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {

		// Hide foreign keys
		$sMasterTblVar = $this->getCurrentMasterTable();
		if ($sMasterTblVar == "telecare_user") {
			$this->call_user_id->Visible = FALSE;
			if ($GLOBALS["telecare_user"]->EventCancelled) $this->EventCancelled = TRUE;
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
