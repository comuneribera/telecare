<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_doctor_userinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_doctor_user_search = NULL; // Initialize page object first

class ctelecare_doctor_user_search extends ctelecare_doctor_user {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_doctor_user';

	// Page object name
	var $PageObjName = 'telecare_doctor_user_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_doctor_user)
		if (!isset($GLOBALS["telecare_doctor_user"]) || get_class($GLOBALS["telecare_doctor_user"]) == "ctelecare_doctor_user") {
			$GLOBALS["telecare_doctor_user"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_doctor_user"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_doctor_user', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_doctor_userlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
				$this->Page_Terminate(ew_GetUrl("telecare_doctor_userlist.php"));
			}
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->user_id->Visible = !$this->IsAddOrEdit();

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_doctor_user;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_doctor_user);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";
	var $MultiPages; // Multi pages object

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "telecare_doctor_userlist.php" . "?" . $sSrchStr;
						if ($this->IsModal) {
							$row = array();
							$row["url"] = $sSrchStr;
							echo ew_ArrayToJson(array($row));
							$this->Page_Terminate();
							exit();
						} else {
							$this->Page_Terminate($sSrchStr); // Go to list page
						}
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->user_id); // user_id
		$this->BuildSearchUrl($sSrchUrl, $this->user_admin_id); // user_admin_id
		$this->BuildSearchUrl($sSrchUrl, $this->user_doctor_id); // user_doctor_id
		$this->BuildSearchUrl($sSrchUrl, $this->user_username); // user_username
		$this->BuildSearchUrl($sSrchUrl, $this->user_password); // user_password
		$this->BuildSearchUrl($sSrchUrl, $this->user_name); // user_name
		$this->BuildSearchUrl($sSrchUrl, $this->user_surname); // user_surname
		$this->BuildSearchUrl($sSrchUrl, $this->user_born); // user_born
		$this->BuildSearchUrl($sSrchUrl, $this->user_gender); // user_gender
		$this->BuildSearchUrl($sSrchUrl, $this->user_email); // user_email
		$this->BuildSearchUrl($sSrchUrl, $this->user_phone); // user_phone
		$this->BuildSearchUrl($sSrchUrl, $this->user_address); // user_address
		$this->BuildSearchUrl($sSrchUrl, $this->user_region_id); // user_region_id
		$this->BuildSearchUrl($sSrchUrl, $this->user_province_id); // user_province_id
		$this->BuildSearchUrl($sSrchUrl, $this->user_city_id); // user_city_id
		$this->BuildSearchUrl($sSrchUrl, $this->user_latitude); // user_latitude
		$this->BuildSearchUrl($sSrchUrl, $this->user_longitude); // user_longitude
		$this->BuildSearchUrl($sSrchUrl, $this->user_modules); // user_modules
		$this->BuildSearchUrl($sSrchUrl, $this->user_medical_history); // user_medical_history
		$this->BuildSearchUrl($sSrchUrl, $this->user_level); // user_level
		$this->BuildSearchUrl($sSrchUrl, $this->user_language); // user_language
		$this->BuildSearchUrl($sSrchUrl, $this->user_last_update); // user_last_update
		$this->BuildSearchUrl($sSrchUrl, $this->user_is_active); // user_is_active
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// user_id

		$this->user_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_id"));
		$this->user_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_id");

		// user_admin_id
		$this->user_admin_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_admin_id"));
		$this->user_admin_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_admin_id");

		// user_doctor_id
		$this->user_doctor_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_doctor_id"));
		$this->user_doctor_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_doctor_id");

		// user_username
		$this->user_username->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_username"));
		$this->user_username->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_username");

		// user_password
		$this->user_password->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_password"));
		$this->user_password->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_password");

		// user_name
		$this->user_name->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_name"));
		$this->user_name->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_name");

		// user_surname
		$this->user_surname->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_surname"));
		$this->user_surname->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_surname");

		// user_born
		$this->user_born->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_born"));
		$this->user_born->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_born");

		// user_gender
		$this->user_gender->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_gender"));
		$this->user_gender->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_gender");

		// user_email
		$this->user_email->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_email"));
		$this->user_email->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_email");

		// user_phone
		$this->user_phone->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_phone"));
		$this->user_phone->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_phone");

		// user_address
		$this->user_address->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_address"));
		$this->user_address->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_address");

		// user_region_id
		$this->user_region_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_region_id"));
		$this->user_region_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_region_id");

		// user_province_id
		$this->user_province_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_province_id"));
		$this->user_province_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_province_id");

		// user_city_id
		$this->user_city_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_city_id"));
		$this->user_city_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_city_id");

		// user_latitude
		$this->user_latitude->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_latitude"));
		$this->user_latitude->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_latitude");

		// user_longitude
		$this->user_longitude->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_longitude"));
		$this->user_longitude->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_longitude");

		// user_modules
		$this->user_modules->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_modules"));
		$this->user_modules->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_modules");
		if (is_array($this->user_modules->AdvancedSearch->SearchValue)) $this->user_modules->AdvancedSearch->SearchValue = implode(",", $this->user_modules->AdvancedSearch->SearchValue);
		if (is_array($this->user_modules->AdvancedSearch->SearchValue2)) $this->user_modules->AdvancedSearch->SearchValue2 = implode(",", $this->user_modules->AdvancedSearch->SearchValue2);

		// user_medical_history
		$this->user_medical_history->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_medical_history"));
		$this->user_medical_history->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_medical_history");

		// user_level
		$this->user_level->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_level"));
		$this->user_level->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_level");

		// user_language
		$this->user_language->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_language"));
		$this->user_language->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_language");

		// user_last_update
		$this->user_last_update->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_last_update"));
		$this->user_last_update->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_last_update");

		// user_is_active
		$this->user_is_active->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_user_is_active"));
		$this->user_is_active->AdvancedSearch->SearchOperator = $objForm->GetValue("z_user_is_active");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// user_id
		// user_admin_id
		// user_doctor_id
		// user_username
		// user_password
		// user_name
		// user_surname
		// user_born
		// user_gender
		// user_email
		// user_phone
		// user_address
		// user_region_id
		// user_province_id
		// user_city_id
		// user_latitude
		// user_longitude
		// user_modules
		// user_medical_history
		// user_level
		// user_language
		// user_last_update
		// user_is_active

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// user_id
		$this->user_id->ViewValue = $this->user_id->CurrentValue;
		$this->user_id->ViewCustomAttributes = "";

		// user_admin_id
		$this->user_admin_id->ViewValue = $this->user_admin_id->CurrentValue;
		$this->user_admin_id->ViewCustomAttributes = "";

		// user_doctor_id
		if (strval($this->user_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = "`admin_level` = 3";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->CurrentValue;
			}
		} else {
			$this->user_doctor_id->ViewValue = NULL;
		}
		$this->user_doctor_id->ViewCustomAttributes = "";

		// user_username
		$this->user_username->ViewValue = $this->user_username->CurrentValue;
		$this->user_username->ViewCustomAttributes = "";

		// user_password
		$this->user_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->user_password->ViewCustomAttributes = "";

		// user_name
		$this->user_name->ViewValue = $this->user_name->CurrentValue;
		$this->user_name->ViewCustomAttributes = "";

		// user_surname
		$this->user_surname->ViewValue = $this->user_surname->CurrentValue;
		$this->user_surname->ViewCustomAttributes = "";

		// user_born
		$this->user_born->ViewValue = $this->user_born->CurrentValue;
		$this->user_born->ViewValue = ew_FormatDateTime($this->user_born->ViewValue, 7);
		$this->user_born->ViewCustomAttributes = "";

		// user_gender
		if (strval($this->user_gender->CurrentValue) <> "") {
			$this->user_gender->ViewValue = $this->user_gender->OptionCaption($this->user_gender->CurrentValue);
		} else {
			$this->user_gender->ViewValue = NULL;
		}
		$this->user_gender->ViewCustomAttributes = "";

		// user_email
		$this->user_email->ViewValue = $this->user_email->CurrentValue;
		$this->user_email->ViewCustomAttributes = "";

		// user_phone
		$this->user_phone->ViewValue = $this->user_phone->CurrentValue;
		$this->user_phone->ViewCustomAttributes = "";

		// user_address
		$this->user_address->ViewValue = $this->user_address->CurrentValue;
		$this->user_address->ViewCustomAttributes = "";

		// user_region_id
		if (strval($this->user_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->user_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `region_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_region_id->ViewValue = $this->user_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_region_id->ViewValue = $this->user_region_id->CurrentValue;
			}
		} else {
			$this->user_region_id->ViewValue = NULL;
		}
		$this->user_region_id->ViewCustomAttributes = "";

		// user_province_id
		if (strval($this->user_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->user_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `province_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_province_id->ViewValue = $this->user_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_province_id->ViewValue = $this->user_province_id->CurrentValue;
			}
		} else {
			$this->user_province_id->ViewValue = NULL;
		}
		$this->user_province_id->ViewCustomAttributes = "";

		// user_city_id
		if (strval($this->user_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_city_id->ViewValue = $this->user_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_city_id->ViewValue = $this->user_city_id->CurrentValue;
			}
		} else {
			$this->user_city_id->ViewValue = NULL;
		}
		$this->user_city_id->ViewCustomAttributes = "";

		// user_latitude
		$this->user_latitude->ViewValue = $this->user_latitude->CurrentValue;
		$this->user_latitude->ViewCustomAttributes = "";

		// user_longitude
		$this->user_longitude->ViewValue = $this->user_longitude->CurrentValue;
		$this->user_longitude->ViewCustomAttributes = "";

		// user_modules
		if (strval($this->user_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->user_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->user_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->user_modules->ViewValue .= $this->user_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->user_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->user_modules->ViewValue = $this->user_modules->CurrentValue;
			}
		} else {
			$this->user_modules->ViewValue = NULL;
		}
		$this->user_modules->ViewCustomAttributes = "";

		// user_medical_history
		$this->user_medical_history->ViewValue = $this->user_medical_history->CurrentValue;
		$this->user_medical_history->ViewCustomAttributes = "";

		// user_level
		$this->user_level->ViewValue = $this->user_level->CurrentValue;
		$this->user_level->ViewCustomAttributes = "";

		// user_language
		$this->user_language->ViewValue = $this->user_language->CurrentValue;
		$this->user_language->ViewCustomAttributes = "";

		// user_last_update
		$this->user_last_update->ViewValue = $this->user_last_update->CurrentValue;
		$this->user_last_update->ViewValue = ew_FormatDateTime($this->user_last_update->ViewValue, 7);
		$this->user_last_update->ViewCustomAttributes = "";

		// user_is_active
		$this->user_is_active->ViewValue = $this->user_is_active->CurrentValue;
		$this->user_is_active->ViewCustomAttributes = "";

			// user_id
			$this->user_id->LinkCustomAttributes = "";
			$this->user_id->HrefValue = "";
			$this->user_id->TooltipValue = "";

			// user_admin_id
			$this->user_admin_id->LinkCustomAttributes = "";
			$this->user_admin_id->HrefValue = "";
			$this->user_admin_id->TooltipValue = "";

			// user_doctor_id
			$this->user_doctor_id->LinkCustomAttributes = "";
			$this->user_doctor_id->HrefValue = "";
			$this->user_doctor_id->TooltipValue = "";

			// user_username
			$this->user_username->LinkCustomAttributes = "";
			$this->user_username->HrefValue = "";
			$this->user_username->TooltipValue = "";

			// user_password
			$this->user_password->LinkCustomAttributes = "";
			$this->user_password->HrefValue = "";
			$this->user_password->TooltipValue = "";

			// user_name
			$this->user_name->LinkCustomAttributes = "";
			$this->user_name->HrefValue = "";
			$this->user_name->TooltipValue = "";

			// user_surname
			$this->user_surname->LinkCustomAttributes = "";
			$this->user_surname->HrefValue = "";
			$this->user_surname->TooltipValue = "";

			// user_born
			$this->user_born->LinkCustomAttributes = "";
			$this->user_born->HrefValue = "";
			$this->user_born->TooltipValue = "";

			// user_gender
			$this->user_gender->LinkCustomAttributes = "";
			$this->user_gender->HrefValue = "";
			$this->user_gender->TooltipValue = "";

			// user_email
			$this->user_email->LinkCustomAttributes = "";
			$this->user_email->HrefValue = "";
			$this->user_email->TooltipValue = "";

			// user_phone
			$this->user_phone->LinkCustomAttributes = "";
			$this->user_phone->HrefValue = "";
			$this->user_phone->TooltipValue = "";

			// user_address
			$this->user_address->LinkCustomAttributes = "";
			$this->user_address->HrefValue = "";
			$this->user_address->TooltipValue = "";

			// user_region_id
			$this->user_region_id->LinkCustomAttributes = "";
			$this->user_region_id->HrefValue = "";
			$this->user_region_id->TooltipValue = "";

			// user_province_id
			$this->user_province_id->LinkCustomAttributes = "";
			$this->user_province_id->HrefValue = "";
			$this->user_province_id->TooltipValue = "";

			// user_city_id
			$this->user_city_id->LinkCustomAttributes = "";
			$this->user_city_id->HrefValue = "";
			$this->user_city_id->TooltipValue = "";

			// user_latitude
			$this->user_latitude->LinkCustomAttributes = "";
			$this->user_latitude->HrefValue = "";
			$this->user_latitude->TooltipValue = "";

			// user_longitude
			$this->user_longitude->LinkCustomAttributes = "";
			$this->user_longitude->HrefValue = "";
			$this->user_longitude->TooltipValue = "";

			// user_modules
			$this->user_modules->LinkCustomAttributes = "";
			$this->user_modules->HrefValue = "";
			$this->user_modules->TooltipValue = "";

			// user_medical_history
			$this->user_medical_history->LinkCustomAttributes = "";
			$this->user_medical_history->HrefValue = "";
			$this->user_medical_history->TooltipValue = "";

			// user_level
			$this->user_level->LinkCustomAttributes = "";
			$this->user_level->HrefValue = "";
			$this->user_level->TooltipValue = "";

			// user_language
			$this->user_language->LinkCustomAttributes = "";
			$this->user_language->HrefValue = "";
			$this->user_language->TooltipValue = "";

			// user_last_update
			$this->user_last_update->LinkCustomAttributes = "";
			$this->user_last_update->HrefValue = "";
			$this->user_last_update->TooltipValue = "";

			// user_is_active
			$this->user_is_active->LinkCustomAttributes = "";
			$this->user_is_active->HrefValue = "";
			$this->user_is_active->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// user_id
			$this->user_id->EditAttrs["class"] = "form-control";
			$this->user_id->EditCustomAttributes = "";
			$this->user_id->EditValue = ew_HtmlEncode($this->user_id->AdvancedSearch->SearchValue);
			$this->user_id->PlaceHolder = ew_RemoveHtml($this->user_id->FldCaption());

			// user_admin_id
			$this->user_admin_id->EditAttrs["class"] = "form-control";
			$this->user_admin_id->EditCustomAttributes = "";
			$this->user_admin_id->EditValue = ew_HtmlEncode($this->user_admin_id->AdvancedSearch->SearchValue);
			$this->user_admin_id->PlaceHolder = ew_RemoveHtml($this->user_admin_id->FldCaption());

			// user_doctor_id
			$this->user_doctor_id->EditAttrs["class"] = "form-control";
			$this->user_doctor_id->EditCustomAttributes = "";
			if (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$this->UserIDAllow("search")) { // Non system admin
			$sFilterWrk = "";
			$sFilterWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter("");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			$lookuptblfilter = "`admin_level` = 3";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_doctor_id->EditValue = $arwrk;
			} else {
			if (trim(strval($this->user_doctor_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			$lookuptblfilter = "`admin_level` = 3";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_doctor_user"]->UserIDAllow("search")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_doctor_id->EditValue = $arwrk;
			}

			// user_username
			$this->user_username->EditAttrs["class"] = "form-control";
			$this->user_username->EditCustomAttributes = "";
			$this->user_username->EditValue = ew_HtmlEncode($this->user_username->AdvancedSearch->SearchValue);
			$this->user_username->PlaceHolder = ew_RemoveHtml($this->user_username->FldCaption());

			// user_password
			$this->user_password->EditAttrs["class"] = "form-control";
			$this->user_password->EditCustomAttributes = "";
			$this->user_password->EditValue = ew_HtmlEncode($this->user_password->AdvancedSearch->SearchValue);
			$this->user_password->PlaceHolder = ew_RemoveHtml($this->user_password->FldCaption());

			// user_name
			$this->user_name->EditAttrs["class"] = "form-control";
			$this->user_name->EditCustomAttributes = "";
			$this->user_name->EditValue = ew_HtmlEncode($this->user_name->AdvancedSearch->SearchValue);
			$this->user_name->PlaceHolder = ew_RemoveHtml($this->user_name->FldCaption());

			// user_surname
			$this->user_surname->EditAttrs["class"] = "form-control";
			$this->user_surname->EditCustomAttributes = "";
			$this->user_surname->EditValue = ew_HtmlEncode($this->user_surname->AdvancedSearch->SearchValue);
			$this->user_surname->PlaceHolder = ew_RemoveHtml($this->user_surname->FldCaption());

			// user_born
			$this->user_born->EditAttrs["class"] = "form-control";
			$this->user_born->EditCustomAttributes = "";
			$this->user_born->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->user_born->AdvancedSearch->SearchValue, 7), 7));
			$this->user_born->PlaceHolder = ew_RemoveHtml($this->user_born->FldCaption());

			// user_gender
			$this->user_gender->EditCustomAttributes = "";
			$this->user_gender->EditValue = $this->user_gender->Options(FALSE);

			// user_email
			$this->user_email->EditAttrs["class"] = "form-control";
			$this->user_email->EditCustomAttributes = "";
			$this->user_email->EditValue = ew_HtmlEncode($this->user_email->AdvancedSearch->SearchValue);
			$this->user_email->PlaceHolder = ew_RemoveHtml($this->user_email->FldCaption());

			// user_phone
			$this->user_phone->EditAttrs["class"] = "form-control";
			$this->user_phone->EditCustomAttributes = "";
			$this->user_phone->EditValue = ew_HtmlEncode($this->user_phone->AdvancedSearch->SearchValue);
			$this->user_phone->PlaceHolder = ew_RemoveHtml($this->user_phone->FldCaption());

			// user_address
			$this->user_address->EditAttrs["class"] = "form-control";
			$this->user_address->EditCustomAttributes = "";
			$this->user_address->EditValue = ew_HtmlEncode($this->user_address->AdvancedSearch->SearchValue);
			$this->user_address->PlaceHolder = ew_RemoveHtml($this->user_address->FldCaption());

			// user_region_id
			$this->user_region_id->EditAttrs["class"] = "form-control";
			$this->user_region_id->EditCustomAttributes = "";
			if (trim(strval($this->user_region_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->user_region_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_region_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `region_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_region_id->EditValue = $arwrk;

			// user_province_id
			$this->user_province_id->EditAttrs["class"] = "form-control";
			$this->user_province_id->EditCustomAttributes = "";
			if (trim(strval($this->user_province_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->user_province_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_province_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `province_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_province_id->EditValue = $arwrk;

			// user_city_id
			$this->user_city_id->EditAttrs["class"] = "form-control";
			$this->user_city_id->EditCustomAttributes = "";
			if (trim(strval($this->user_city_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_city_id->EditValue = $arwrk;

			// user_latitude
			$this->user_latitude->EditAttrs["class"] = "form-control";
			$this->user_latitude->EditCustomAttributes = "";
			$this->user_latitude->EditValue = ew_HtmlEncode($this->user_latitude->AdvancedSearch->SearchValue);
			$this->user_latitude->PlaceHolder = ew_RemoveHtml($this->user_latitude->FldCaption());

			// user_longitude
			$this->user_longitude->EditAttrs["class"] = "form-control";
			$this->user_longitude->EditCustomAttributes = "";
			$this->user_longitude->EditValue = ew_HtmlEncode($this->user_longitude->AdvancedSearch->SearchValue);
			$this->user_longitude->PlaceHolder = ew_RemoveHtml($this->user_longitude->FldCaption());

			// user_modules
			$this->user_modules->EditCustomAttributes = "";
			if (trim(strval($this->user_modules->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$arwrk = explode(",", $this->user_modules->AdvancedSearch->SearchValue);
				$sFilterWrk = "";
				foreach ($arwrk as $wrk) {
					if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
					$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
				}
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_modules, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->user_modules->EditValue = $arwrk;

			// user_medical_history
			$this->user_medical_history->EditAttrs["class"] = "form-control";
			$this->user_medical_history->EditCustomAttributes = "";
			$this->user_medical_history->EditValue = ew_HtmlEncode($this->user_medical_history->AdvancedSearch->SearchValue);
			$this->user_medical_history->PlaceHolder = ew_RemoveHtml($this->user_medical_history->FldCaption());

			// user_level
			$this->user_level->EditAttrs["class"] = "form-control";
			$this->user_level->EditCustomAttributes = "";
			$this->user_level->EditValue = ew_HtmlEncode($this->user_level->AdvancedSearch->SearchValue);
			$this->user_level->PlaceHolder = ew_RemoveHtml($this->user_level->FldCaption());

			// user_language
			$this->user_language->EditAttrs["class"] = "form-control";
			$this->user_language->EditCustomAttributes = "";
			$this->user_language->EditValue = ew_HtmlEncode($this->user_language->AdvancedSearch->SearchValue);
			$this->user_language->PlaceHolder = ew_RemoveHtml($this->user_language->FldCaption());

			// user_last_update
			$this->user_last_update->EditAttrs["class"] = "form-control";
			$this->user_last_update->EditCustomAttributes = "";
			$this->user_last_update->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->user_last_update->AdvancedSearch->SearchValue, 7), 7));
			$this->user_last_update->PlaceHolder = ew_RemoveHtml($this->user_last_update->FldCaption());

			// user_is_active
			$this->user_is_active->EditAttrs["class"] = "form-control";
			$this->user_is_active->EditCustomAttributes = "";
			$this->user_is_active->EditValue = ew_HtmlEncode($this->user_is_active->AdvancedSearch->SearchValue);
			$this->user_is_active->PlaceHolder = ew_RemoveHtml($this->user_is_active->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->user_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->user_id->FldErrMsg());
		}
		if (!ew_CheckInteger($this->user_admin_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->user_admin_id->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->user_born->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->user_born->FldErrMsg());
		}
		if (!ew_CheckInteger($this->user_level->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->user_level->FldErrMsg());
		}
		if (!ew_CheckInteger($this->user_language->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->user_language->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->user_last_update->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->user_last_update->FldErrMsg());
		}
		if (!ew_CheckInteger($this->user_is_active->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->user_is_active->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->user_id->AdvancedSearch->Load();
		$this->user_admin_id->AdvancedSearch->Load();
		$this->user_doctor_id->AdvancedSearch->Load();
		$this->user_username->AdvancedSearch->Load();
		$this->user_password->AdvancedSearch->Load();
		$this->user_name->AdvancedSearch->Load();
		$this->user_surname->AdvancedSearch->Load();
		$this->user_born->AdvancedSearch->Load();
		$this->user_gender->AdvancedSearch->Load();
		$this->user_email->AdvancedSearch->Load();
		$this->user_phone->AdvancedSearch->Load();
		$this->user_address->AdvancedSearch->Load();
		$this->user_region_id->AdvancedSearch->Load();
		$this->user_province_id->AdvancedSearch->Load();
		$this->user_city_id->AdvancedSearch->Load();
		$this->user_latitude->AdvancedSearch->Load();
		$this->user_longitude->AdvancedSearch->Load();
		$this->user_modules->AdvancedSearch->Load();
		$this->user_medical_history->AdvancedSearch->Load();
		$this->user_level->AdvancedSearch->Load();
		$this->user_language->AdvancedSearch->Load();
		$this->user_last_update->AdvancedSearch->Load();
		$this->user_is_active->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_doctor_userlist.php", "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$pages->Add(4);
		$this->MultiPages = $pages;
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_doctor_user_search)) $telecare_doctor_user_search = new ctelecare_doctor_user_search();

// Page init
$telecare_doctor_user_search->Page_Init();

// Page main
$telecare_doctor_user_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_doctor_user_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($telecare_doctor_user_search->IsModal) { ?>
var CurrentAdvancedSearchForm = ftelecare_doctor_usersearch = new ew_Form("ftelecare_doctor_usersearch", "search");
<?php } else { ?>
var CurrentForm = ftelecare_doctor_usersearch = new ew_Form("ftelecare_doctor_usersearch", "search");
<?php } ?>

// Form_CustomValidate event
ftelecare_doctor_usersearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_doctor_usersearch.ValidateRequired = true;
<?php } else { ?>
ftelecare_doctor_usersearch.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_doctor_usersearch.MultiPage = new ew_MultiPage("ftelecare_doctor_usersearch");

// Dynamic selection lists
ftelecare_doctor_usersearch.Lists["x_user_doctor_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_doctor_usersearch.Lists["x_user_gender"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_doctor_usersearch.Lists["x_user_gender"].Options = <?php echo json_encode($telecare_doctor_user->user_gender->Options()) ?>;
ftelecare_doctor_usersearch.Lists["x_user_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_user_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_doctor_usersearch.Lists["x_user_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":["x_user_region_id"],"ChildFields":["x_user_city_id"],"FilterFields":["x_province_region_id"],"Options":[],"Template":""};
ftelecare_doctor_usersearch.Lists["x_user_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":["x_user_province_id"],"ChildFields":[],"FilterFields":["x_city_province_id"],"Options":[],"Template":""};
ftelecare_doctor_usersearch.Lists["x_user_modules[]"] = {"LinkField":"x_invalidity_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_invalidity_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
// Validate function for search

ftelecare_doctor_usersearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_user_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_doctor_user->user_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_user_admin_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_doctor_user->user_admin_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_user_born");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_doctor_user->user_born->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_user_level");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_doctor_user->user_level->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_user_language");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_doctor_user->user_language->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_user_last_update");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_doctor_user->user_last_update->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_user_is_active");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_doctor_user->user_is_active->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$telecare_doctor_user_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $telecare_doctor_user_search->ShowPageHeader(); ?>
<?php
$telecare_doctor_user_search->ShowMessage();
?>
<form name="ftelecare_doctor_usersearch" id="ftelecare_doctor_usersearch" class="<?php echo $telecare_doctor_user_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_doctor_user_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_doctor_user_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_doctor_user">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($telecare_doctor_user_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div class="ewMultiPage">
<div class="tabbable" id="telecare_doctor_user_search">
	<ul class="nav<?php echo $telecare_doctor_user_search->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_doctor_user_search->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_doctor_user1" data-toggle="tab"><?php echo $telecare_doctor_user->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_doctor_user_search->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_doctor_user2" data-toggle="tab"><?php echo $telecare_doctor_user->PageCaption(2) ?></a></li>
		<li<?php echo $telecare_doctor_user_search->MultiPages->TabStyle("3") ?>><a href="#tab_telecare_doctor_user3" data-toggle="tab"><?php echo $telecare_doctor_user->PageCaption(3) ?></a></li>
		<li<?php echo $telecare_doctor_user_search->MultiPages->TabStyle("4") ?>><a href="#tab_telecare_doctor_user4" data-toggle="tab"><?php echo $telecare_doctor_user->PageCaption(4) ?></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane<?php echo $telecare_doctor_user_search->MultiPages->PageStyle("1") ?>" id="tab_telecare_doctor_user1">
<div>
<?php if ($telecare_doctor_user->user_id->Visible) { // user_id ?>
	<div id="r_user_id" class="form-group">
		<label for="x_user_id" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_id"><?php echo $telecare_doctor_user->user_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_id" id="z_user_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_id->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_id">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_id" data-page="1" name="x_user_id" id="x_user_id" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_id->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_id->EditValue ?>"<?php echo $telecare_doctor_user->user_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_admin_id->Visible) { // user_admin_id ?>
	<div id="r_user_admin_id" class="form-group">
		<label for="x_user_admin_id" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_admin_id"><?php echo $telecare_doctor_user->user_admin_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_admin_id" id="z_user_admin_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_admin_id->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_admin_id">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_admin_id" data-page="1" name="x_user_admin_id" id="x_user_admin_id" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_admin_id->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_admin_id->EditValue ?>"<?php echo $telecare_doctor_user->user_admin_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_doctor_id->Visible) { // user_doctor_id ?>
	<div id="r_user_doctor_id" class="form-group">
		<label for="x_user_doctor_id" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_doctor_id"><?php echo $telecare_doctor_user->user_doctor_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_doctor_id" id="z_user_doctor_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_doctor_id->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_doctor_id">
<?php if (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$telecare_doctor_user->UserIDAllow("search")) { // Non system admin ?>
<select data-table="telecare_doctor_user" data-field="x_user_doctor_id" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_doctor_user->user_doctor_id->DisplayValueSeparator) ? json_encode($telecare_doctor_user->user_doctor_id->DisplayValueSeparator) : $telecare_doctor_user->user_doctor_id->DisplayValueSeparator) ?>" id="x_user_doctor_id" name="x_user_doctor_id"<?php echo $telecare_doctor_user->user_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_doctor_user->user_doctor_id->EditValue)) {
	$arwrk = $telecare_doctor_user->user_doctor_id->EditValue;
	if ($arwrk[0][0] <> "") echo "<option value=\"\">" . $Language->Phrase("PleaseSelect") . "</option>";
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_doctor_user->user_doctor_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_doctor_user->user_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_doctor_user->user_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_doctor_user->user_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_doctor_user->user_doctor_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = "`admin_level` = 3";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_doctor_user"]->UserIDAllow("search")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_doctor_user->user_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_doctor_user->user_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_doctor_user->Lookup_Selecting($telecare_doctor_user->user_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_doctor_user->user_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_doctor_id" id="s_x_user_doctor_id" value="<?php echo $telecare_doctor_user->user_doctor_id->LookupFilterQuery() ?>">
<?php } else { ?>
<select data-table="telecare_doctor_user" data-field="x_user_doctor_id" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_doctor_user->user_doctor_id->DisplayValueSeparator) ? json_encode($telecare_doctor_user->user_doctor_id->DisplayValueSeparator) : $telecare_doctor_user->user_doctor_id->DisplayValueSeparator) ?>" id="x_user_doctor_id" name="x_user_doctor_id"<?php echo $telecare_doctor_user->user_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_doctor_user->user_doctor_id->EditValue)) {
	$arwrk = $telecare_doctor_user->user_doctor_id->EditValue;
	if ($arwrk[0][0] <> "") echo "<option value=\"\">" . $Language->Phrase("PleaseSelect") . "</option>";
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_doctor_user->user_doctor_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_doctor_user->user_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_doctor_user->user_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_doctor_user->user_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_doctor_user->user_doctor_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = "`admin_level` = 3";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_doctor_user"]->UserIDAllow("search")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_doctor_user->user_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_doctor_user->user_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_doctor_user->Lookup_Selecting($telecare_doctor_user->user_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_doctor_user->user_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_doctor_id" id="s_x_user_doctor_id" value="<?php echo $telecare_doctor_user->user_doctor_id->LookupFilterQuery() ?>">
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_name->Visible) { // user_name ?>
	<div id="r_user_name" class="form-group">
		<label for="x_user_name" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_name"><?php echo $telecare_doctor_user->user_name->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_name" id="z_user_name" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_name->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_name">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_name" data-page="1" name="x_user_name" id="x_user_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_name->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_name->EditValue ?>"<?php echo $telecare_doctor_user->user_name->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_surname->Visible) { // user_surname ?>
	<div id="r_user_surname" class="form-group">
		<label for="x_user_surname" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_surname"><?php echo $telecare_doctor_user->user_surname->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_surname" id="z_user_surname" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_surname->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_surname">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_surname" data-page="1" name="x_user_surname" id="x_user_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_surname->EditValue ?>"<?php echo $telecare_doctor_user->user_surname->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_born->Visible) { // user_born ?>
	<div id="r_user_born" class="form-group">
		<label for="x_user_born" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_born"><?php echo $telecare_doctor_user->user_born->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_born" id="z_user_born" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_born->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_born">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_born" data-page="1" data-format="7" name="x_user_born" id="x_user_born" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_born->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_born->EditValue ?>"<?php echo $telecare_doctor_user->user_born->EditAttributes() ?>>
<?php if (!$telecare_doctor_user->user_born->ReadOnly && !$telecare_doctor_user->user_born->Disabled && !isset($telecare_doctor_user->user_born->EditAttrs["readonly"]) && !isset($telecare_doctor_user->user_born->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_doctor_usersearch", "x_user_born", "%d/%m/%Y");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_gender->Visible) { // user_gender ?>
	<div id="r_user_gender" class="form-group">
		<label class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_gender"><?php echo $telecare_doctor_user->user_gender->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_gender" id="z_user_gender" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_gender->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_gender">
<div id="tp_x_user_gender" class="ewTemplate"><input type="radio" data-table="telecare_doctor_user" data-field="x_user_gender" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_doctor_user->user_gender->DisplayValueSeparator) ? json_encode($telecare_doctor_user->user_gender->DisplayValueSeparator) : $telecare_doctor_user->user_gender->DisplayValueSeparator) ?>" name="x_user_gender" id="x_user_gender" value="{value}"<?php echo $telecare_doctor_user->user_gender->EditAttributes() ?>></div>
<div id="dsl_x_user_gender" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_doctor_user->user_gender->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_doctor_user->user_gender->AdvancedSearch->SearchValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_doctor_user" data-field="x_user_gender" data-page="1" name="x_user_gender" id="x_user_gender_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_doctor_user->user_gender->EditAttributes() ?>><?php echo $telecare_doctor_user->user_gender->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_doctor_user->user_gender->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_doctor_user" data-field="x_user_gender" data-page="1" name="x_user_gender" id="x_user_gender_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_doctor_user->user_gender->CurrentValue) ?>" checked<?php echo $telecare_doctor_user->user_gender->EditAttributes() ?>><?php echo $telecare_doctor_user->user_gender->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_modules->Visible) { // user_modules ?>
	<div id="r_user_modules" class="form-group">
		<label class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_modules"><?php echo $telecare_doctor_user->user_modules->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_modules" id="z_user_modules" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_modules->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_modules">
<div id="tp_x_user_modules" class="ewTemplate"><input type="checkbox" data-table="telecare_doctor_user" data-field="x_user_modules" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_doctor_user->user_modules->DisplayValueSeparator) ? json_encode($telecare_doctor_user->user_modules->DisplayValueSeparator) : $telecare_doctor_user->user_modules->DisplayValueSeparator) ?>" name="x_user_modules[]" id="x_user_modules[]" value="{value}"<?php echo $telecare_doctor_user->user_modules->EditAttributes() ?>></div>
<div id="dsl_x_user_modules" data-repeatcolumn="10" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_doctor_user->user_modules->EditValue;
if (is_array($arwrk)) {
	$armultiwrk = (strval($telecare_doctor_user->user_modules->AdvancedSearch->SearchValue) <> "") ? explode(",", strval($telecare_doctor_user->user_modules->AdvancedSearch->SearchValue)) : array();
	$cnt = count($armultiwrk);
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = "";
		for ($ari = 0; $ari < $cnt; $ari++) {
			if (ew_SameStr($arwrk[$rowcntwrk][0], $armultiwrk[$ari]) && !is_null($armultiwrk[$ari])) {
				$armultiwrk[$ari] = NULL; // Marked for removal
				$selwrk = " checked";
				if ($selwrk <> "") $emptywrk = FALSE;
				break;
			}
		}
		if ($selwrk <> "") {
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 10, 1) ?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_doctor_user" data-field="x_user_modules" data-page="1" name="x_user_modules[]" id="x_user_modules_<?php echo $rowcntwrk ?>[]" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_doctor_user->user_modules->EditAttributes() ?>><?php echo $telecare_doctor_user->user_modules->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 10, 2) ?>
<?php
		}
	}
	for ($ari = 0; $ari < $cnt; $ari++) {
		if (!is_null($armultiwrk[$ari])) {
?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_doctor_user" data-field="x_user_modules" data-page="1" name="x_user_modules[]" value="<?php echo ew_HtmlEncode($armultiwrk[$ari]) ?>" checked<?php echo $telecare_doctor_user->user_modules->EditAttributes() ?>><?php echo $armultiwrk[$ari] ?></label>
<?php
		}
	}
}
?>
</div></div>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
}
$telecare_doctor_user->user_modules->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_doctor_user->user_modules->LookupFilters += array("f0" => "`invalidity_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_doctor_user->Lookup_Selecting($telecare_doctor_user->user_modules, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
if ($sSqlWrk <> "") $telecare_doctor_user->user_modules->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_modules" id="s_x_user_modules" value="<?php echo $telecare_doctor_user->user_modules->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_medical_history->Visible) { // user_medical_history ?>
	<div id="r_user_medical_history" class="form-group">
		<label for="x_user_medical_history" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_medical_history"><?php echo $telecare_doctor_user->user_medical_history->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_medical_history" id="z_user_medical_history" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_medical_history->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_medical_history">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_medical_history" data-page="1" name="x_user_medical_history" id="x_user_medical_history" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_medical_history->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_medical_history->EditValue ?>"<?php echo $telecare_doctor_user->user_medical_history->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_level->Visible) { // user_level ?>
	<div id="r_user_level" class="form-group">
		<label for="x_user_level" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_level"><?php echo $telecare_doctor_user->user_level->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_level" id="z_user_level" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_level->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_level">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_level" data-page="1" name="x_user_level" id="x_user_level" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_level->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_level->EditValue ?>"<?php echo $telecare_doctor_user->user_level->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_is_active->Visible) { // user_is_active ?>
	<div id="r_user_is_active" class="form-group">
		<label for="x_user_is_active" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_is_active"><?php echo $telecare_doctor_user->user_is_active->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_is_active" id="z_user_is_active" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_is_active->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_is_active">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_is_active" data-page="1" name="x_user_is_active" id="x_user_is_active" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_is_active->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_is_active->EditValue ?>"<?php echo $telecare_doctor_user->user_is_active->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_doctor_user_search->MultiPages->PageStyle("2") ?>" id="tab_telecare_doctor_user2">
<div>
<?php if ($telecare_doctor_user->user_email->Visible) { // user_email ?>
	<div id="r_user_email" class="form-group">
		<label for="x_user_email" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_email"><?php echo $telecare_doctor_user->user_email->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_email" id="z_user_email" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_email->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_email">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_email" data-page="2" name="x_user_email" id="x_user_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_email->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_email->EditValue ?>"<?php echo $telecare_doctor_user->user_email->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_phone->Visible) { // user_phone ?>
	<div id="r_user_phone" class="form-group">
		<label for="x_user_phone" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_phone"><?php echo $telecare_doctor_user->user_phone->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_phone" id="z_user_phone" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_phone->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_phone">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_phone" data-page="2" name="x_user_phone" id="x_user_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_phone->EditValue ?>"<?php echo $telecare_doctor_user->user_phone->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_address->Visible) { // user_address ?>
	<div id="r_user_address" class="form-group">
		<label for="x_user_address" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_address"><?php echo $telecare_doctor_user->user_address->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_address" id="z_user_address" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_address->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_address">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_address" data-page="2" name="x_user_address" id="x_user_address" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_address->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_address->EditValue ?>"<?php echo $telecare_doctor_user->user_address->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_region_id->Visible) { // user_region_id ?>
	<div id="r_user_region_id" class="form-group">
		<label for="x_user_region_id" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_region_id"><?php echo $telecare_doctor_user->user_region_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_region_id" id="z_user_region_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_region_id->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_region_id">
<?php $telecare_doctor_user->user_region_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_doctor_user->user_region_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_doctor_user" data-field="x_user_region_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_doctor_user->user_region_id->DisplayValueSeparator) ? json_encode($telecare_doctor_user->user_region_id->DisplayValueSeparator) : $telecare_doctor_user->user_region_id->DisplayValueSeparator) ?>" id="x_user_region_id" name="x_user_region_id"<?php echo $telecare_doctor_user->user_region_id->EditAttributes() ?>>
<?php
if (is_array($telecare_doctor_user->user_region_id->EditValue)) {
	$arwrk = $telecare_doctor_user->user_region_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_doctor_user->user_region_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_doctor_user->user_region_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_doctor_user->user_region_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_doctor_user->user_region_id->CurrentValue) ?>" selected><?php echo $telecare_doctor_user->user_region_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
}
$telecare_doctor_user->user_region_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_doctor_user->user_region_id->LookupFilters += array("f0" => "`region_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_doctor_user->Lookup_Selecting($telecare_doctor_user->user_region_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `region_name` ASC";
if ($sSqlWrk <> "") $telecare_doctor_user->user_region_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_region_id" id="s_x_user_region_id" value="<?php echo $telecare_doctor_user->user_region_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_province_id->Visible) { // user_province_id ?>
	<div id="r_user_province_id" class="form-group">
		<label for="x_user_province_id" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_province_id"><?php echo $telecare_doctor_user->user_province_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_province_id" id="z_user_province_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_province_id->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_province_id">
<?php $telecare_doctor_user->user_province_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_doctor_user->user_province_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_doctor_user" data-field="x_user_province_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_doctor_user->user_province_id->DisplayValueSeparator) ? json_encode($telecare_doctor_user->user_province_id->DisplayValueSeparator) : $telecare_doctor_user->user_province_id->DisplayValueSeparator) ?>" id="x_user_province_id" name="x_user_province_id"<?php echo $telecare_doctor_user->user_province_id->EditAttributes() ?>>
<?php
if (is_array($telecare_doctor_user->user_province_id->EditValue)) {
	$arwrk = $telecare_doctor_user->user_province_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_doctor_user->user_province_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_doctor_user->user_province_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_doctor_user->user_province_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_doctor_user->user_province_id->CurrentValue) ?>" selected><?php echo $telecare_doctor_user->user_province_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_doctor_user->user_province_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_doctor_user->user_province_id->LookupFilters += array("f0" => "`province_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_doctor_user->user_province_id->LookupFilters += array("f1" => "`province_region_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_doctor_user->Lookup_Selecting($telecare_doctor_user->user_province_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `province_name` ASC";
if ($sSqlWrk <> "") $telecare_doctor_user->user_province_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_province_id" id="s_x_user_province_id" value="<?php echo $telecare_doctor_user->user_province_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_city_id->Visible) { // user_city_id ?>
	<div id="r_user_city_id" class="form-group">
		<label for="x_user_city_id" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_city_id"><?php echo $telecare_doctor_user->user_city_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_city_id" id="z_user_city_id" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_city_id->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_city_id">
<select data-table="telecare_doctor_user" data-field="x_user_city_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_doctor_user->user_city_id->DisplayValueSeparator) ? json_encode($telecare_doctor_user->user_city_id->DisplayValueSeparator) : $telecare_doctor_user->user_city_id->DisplayValueSeparator) ?>" id="x_user_city_id" name="x_user_city_id"<?php echo $telecare_doctor_user->user_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_doctor_user->user_city_id->EditValue)) {
	$arwrk = $telecare_doctor_user->user_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_doctor_user->user_city_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_doctor_user->user_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_doctor_user->user_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_doctor_user->user_city_id->CurrentValue) ?>" selected><?php echo $telecare_doctor_user->user_city_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_doctor_user->user_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_doctor_user->user_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_doctor_user->user_city_id->LookupFilters += array("f1" => "`city_province_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_doctor_user->Lookup_Selecting($telecare_doctor_user->user_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `city_name` ASC";
if ($sSqlWrk <> "") $telecare_doctor_user->user_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_city_id" id="s_x_user_city_id" value="<?php echo $telecare_doctor_user->user_city_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_language->Visible) { // user_language ?>
	<div id="r_user_language" class="form-group">
		<label for="x_user_language" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_language"><?php echo $telecare_doctor_user->user_language->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_language" id="z_user_language" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_language->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_language">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_language" data-page="2" name="x_user_language" id="x_user_language" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_language->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_language->EditValue ?>"<?php echo $telecare_doctor_user->user_language->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_doctor_user_search->MultiPages->PageStyle("3") ?>" id="tab_telecare_doctor_user3">
<div>
<?php if ($telecare_doctor_user->user_username->Visible) { // user_username ?>
	<div id="r_user_username" class="form-group">
		<label for="x_user_username" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_username"><?php echo $telecare_doctor_user->user_username->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_username" id="z_user_username" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_username->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_username">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_username" data-page="3" name="x_user_username" id="x_user_username" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_username->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_username->EditValue ?>"<?php echo $telecare_doctor_user->user_username->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_password->Visible) { // user_password ?>
	<div id="r_user_password" class="form-group">
		<label for="x_user_password" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_password"><?php echo $telecare_doctor_user->user_password->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_password" id="z_user_password" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_password->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_password">
<input type="password" data-field="x_user_password" name="x_user_password" id="x_user_password" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_password->getPlaceHolder()) ?>"<?php echo $telecare_doctor_user->user_password->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_doctor_user_search->MultiPages->PageStyle("4") ?>" id="tab_telecare_doctor_user4">
<div>
<?php if ($telecare_doctor_user->user_latitude->Visible) { // user_latitude ?>
	<div id="r_user_latitude" class="form-group">
		<label for="x_user_latitude" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_latitude"><?php echo $telecare_doctor_user->user_latitude->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_latitude" id="z_user_latitude" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_latitude->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_latitude">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_latitude" data-page="4" name="x_user_latitude" id="x_user_latitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_latitude->EditValue ?>"<?php echo $telecare_doctor_user->user_latitude->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_longitude->Visible) { // user_longitude ?>
	<div id="r_user_longitude" class="form-group">
		<label for="x_user_longitude" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_longitude"><?php echo $telecare_doctor_user->user_longitude->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_user_longitude" id="z_user_longitude" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_longitude->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_longitude">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_longitude" data-page="4" name="x_user_longitude" id="x_user_longitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_longitude->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_longitude->EditValue ?>"<?php echo $telecare_doctor_user->user_longitude->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_doctor_user->user_last_update->Visible) { // user_last_update ?>
	<div id="r_user_last_update" class="form-group">
		<label for="x_user_last_update" class="<?php echo $telecare_doctor_user_search->SearchLabelClass ?>"><span id="elh_telecare_doctor_user_user_last_update"><?php echo $telecare_doctor_user->user_last_update->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_user_last_update" id="z_user_last_update" value="="></p>
		</label>
		<div class="<?php echo $telecare_doctor_user_search->SearchRightColumnClass ?>"><div<?php echo $telecare_doctor_user->user_last_update->CellAttributes() ?>>
			<span id="el_telecare_doctor_user_user_last_update">
<input type="text" data-table="telecare_doctor_user" data-field="x_user_last_update" data-page="4" data-format="7" name="x_user_last_update" id="x_user_last_update" placeholder="<?php echo ew_HtmlEncode($telecare_doctor_user->user_last_update->getPlaceHolder()) ?>" value="<?php echo $telecare_doctor_user->user_last_update->EditValue ?>"<?php echo $telecare_doctor_user->user_last_update->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
	</div>
</div>
</div>
<?php if (!$telecare_doctor_user_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
ftelecare_doctor_usersearch.Init();
</script>
<?php
$telecare_doctor_user_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_doctor_user_search->Page_Terminate();
?>
