<?php include_once "telecare_admininfo.php" ?>
<?php

// Create page object
if (!isset($telecare_alarm_grid)) $telecare_alarm_grid = new ctelecare_alarm_grid();

// Page init
$telecare_alarm_grid->Page_Init();

// Page main
$telecare_alarm_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_alarm_grid->Page_Render();
?>
<?php if ($telecare_alarm->Export == "") { ?>
<script type="text/javascript">

// Form object
var ftelecare_alarmgrid = new ew_Form("ftelecare_alarmgrid", "grid");
ftelecare_alarmgrid.FormKeyCountName = '<?php echo $telecare_alarm_grid->FormKeyCountName ?>';

// Validate form
ftelecare_alarmgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_alarm_type_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_type_id->FldCaption(), $telecare_alarm->alarm_type_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_user_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_user_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_alarm_product_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_product_id->FldCaption(), $telecare_alarm->alarm_product_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_datetime");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_datetime->FldCaption(), $telecare_alarm->alarm_datetime->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_datetime");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_datetime->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_alarm_latitude");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_latitude->FldCaption(), $telecare_alarm->alarm_latitude->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_start_on");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_start_on->FldCaption(), $telecare_alarm->alarm_start_on->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_start_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_start_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_alarm_close");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_close->FldCaption(), $telecare_alarm->alarm_close->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_call_parents");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_call_parents->FldCaption(), $telecare_alarm->alarm_call_parents->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_call_emergency");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_call_emergency->FldCaption(), $telecare_alarm->alarm_call_emergency->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
ftelecare_alarmgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "alarm_type_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_user_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_product_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_datetime", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_latitude", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_start_on", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_close", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_call_parents", false)) return false;
	if (ew_ValueChanged(fobj, infix, "alarm_call_emergency", false)) return false;
	return true;
}

// Form_CustomValidate event
ftelecare_alarmgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_alarmgrid.ValidateRequired = true;
<?php } else { ?>
ftelecare_alarmgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_alarmgrid.Lists["x_alarm_type_id"] = {"LinkField":"x_alarm_type_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_alarm_type_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmgrid.Lists["x_alarm_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmgrid.Lists["x_alarm_product_id"] = {"LinkField":"x_product_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_product_type_name","x_product_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmgrid.Lists["x_alarm_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmgrid.Lists["x_alarm_close"].Options = <?php echo json_encode($telecare_alarm->alarm_close->Options()) ?>;
ftelecare_alarmgrid.Lists["x_alarm_call_parents"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmgrid.Lists["x_alarm_call_parents"].Options = <?php echo json_encode($telecare_alarm->alarm_call_parents->Options()) ?>;
ftelecare_alarmgrid.Lists["x_alarm_call_emergency"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmgrid.Lists["x_alarm_call_emergency"].Options = <?php echo json_encode($telecare_alarm->alarm_call_emergency->Options()) ?>;

// Form object for search
</script>
<?php } ?>
<?php
if ($telecare_alarm->CurrentAction == "gridadd") {
	if ($telecare_alarm->CurrentMode == "copy") {
		$bSelectLimit = $telecare_alarm_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$telecare_alarm_grid->TotalRecs = $telecare_alarm->SelectRecordCount();
			$telecare_alarm_grid->Recordset = $telecare_alarm_grid->LoadRecordset($telecare_alarm_grid->StartRec-1, $telecare_alarm_grid->DisplayRecs);
		} else {
			if ($telecare_alarm_grid->Recordset = $telecare_alarm_grid->LoadRecordset())
				$telecare_alarm_grid->TotalRecs = $telecare_alarm_grid->Recordset->RecordCount();
		}
		$telecare_alarm_grid->StartRec = 1;
		$telecare_alarm_grid->DisplayRecs = $telecare_alarm_grid->TotalRecs;
	} else {
		$telecare_alarm->CurrentFilter = "0=1";
		$telecare_alarm_grid->StartRec = 1;
		$telecare_alarm_grid->DisplayRecs = $telecare_alarm->GridAddRowCount;
	}
	$telecare_alarm_grid->TotalRecs = $telecare_alarm_grid->DisplayRecs;
	$telecare_alarm_grid->StopRec = $telecare_alarm_grid->DisplayRecs;
} else {
	$bSelectLimit = $telecare_alarm_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($telecare_alarm_grid->TotalRecs <= 0)
			$telecare_alarm_grid->TotalRecs = $telecare_alarm->SelectRecordCount();
	} else {
		if (!$telecare_alarm_grid->Recordset && ($telecare_alarm_grid->Recordset = $telecare_alarm_grid->LoadRecordset()))
			$telecare_alarm_grid->TotalRecs = $telecare_alarm_grid->Recordset->RecordCount();
	}
	$telecare_alarm_grid->StartRec = 1;
	$telecare_alarm_grid->DisplayRecs = $telecare_alarm_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$telecare_alarm_grid->Recordset = $telecare_alarm_grid->LoadRecordset($telecare_alarm_grid->StartRec-1, $telecare_alarm_grid->DisplayRecs);

	// Set no record found message
	if ($telecare_alarm->CurrentAction == "" && $telecare_alarm_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$telecare_alarm_grid->setWarningMessage($Language->Phrase("NoPermission"));
		if ($telecare_alarm_grid->SearchWhere == "0=101")
			$telecare_alarm_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$telecare_alarm_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$telecare_alarm_grid->RenderOtherOptions();
?>
<?php $telecare_alarm_grid->ShowPageHeader(); ?>
<?php
$telecare_alarm_grid->ShowMessage();
?>
<?php if ($telecare_alarm_grid->TotalRecs > 0 || $telecare_alarm->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="ftelecare_alarmgrid" class="ewForm form-inline">
<?php if ($telecare_alarm_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($telecare_alarm_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_telecare_alarm" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_telecare_alarmgrid" class="table ewTable">
<?php echo $telecare_alarm->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$telecare_alarm_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$telecare_alarm_grid->RenderListOptions();

// Render list options (header, left)
$telecare_alarm_grid->ListOptions->Render("header", "left");
?>
<?php if ($telecare_alarm->alarm_id->Visible) { // alarm_id ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_id) == "") { ?>
		<th data-name="alarm_id"><div id="elh_telecare_alarm_alarm_id" class="telecare_alarm_alarm_id"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_id"><div><div id="elh_telecare_alarm_alarm_id" class="telecare_alarm_alarm_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_type_id) == "") { ?>
		<th data-name="alarm_type_id"><div id="elh_telecare_alarm_alarm_type_id" class="telecare_alarm_alarm_type_id"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_type_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_type_id"><div><div id="elh_telecare_alarm_alarm_type_id" class="telecare_alarm_alarm_type_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_type_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_type_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_type_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_user_id) == "") { ?>
		<th data-name="alarm_user_id"><div id="elh_telecare_alarm_alarm_user_id" class="telecare_alarm_alarm_user_id"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_user_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_user_id"><div><div id="elh_telecare_alarm_alarm_user_id" class="telecare_alarm_alarm_user_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_user_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_user_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_user_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_product_id) == "") { ?>
		<th data-name="alarm_product_id"><div id="elh_telecare_alarm_alarm_product_id" class="telecare_alarm_alarm_product_id"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_product_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_product_id"><div><div id="elh_telecare_alarm_alarm_product_id" class="telecare_alarm_alarm_product_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_product_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_product_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_product_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_datetime) == "") { ?>
		<th data-name="alarm_datetime"><div id="elh_telecare_alarm_alarm_datetime" class="telecare_alarm_alarm_datetime"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_datetime->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_datetime"><div><div id="elh_telecare_alarm_alarm_datetime" class="telecare_alarm_alarm_datetime">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_datetime->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_datetime->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_datetime->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_latitude->Visible) { // alarm_latitude ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_latitude) == "") { ?>
		<th data-name="alarm_latitude"><div id="elh_telecare_alarm_alarm_latitude" class="telecare_alarm_alarm_latitude"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_latitude->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_latitude"><div><div id="elh_telecare_alarm_alarm_latitude" class="telecare_alarm_alarm_latitude">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_latitude->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_latitude->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_latitude->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_start_on) == "") { ?>
		<th data-name="alarm_start_on"><div id="elh_telecare_alarm_alarm_start_on" class="telecare_alarm_alarm_start_on"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_start_on->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_start_on"><div><div id="elh_telecare_alarm_alarm_start_on" class="telecare_alarm_alarm_start_on">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_start_on->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_start_on->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_start_on->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_close_on->Visible) { // alarm_close_on ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_close_on) == "") { ?>
		<th data-name="alarm_close_on"><div id="elh_telecare_alarm_alarm_close_on" class="telecare_alarm_alarm_close_on"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_close_on->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_close_on"><div><div id="elh_telecare_alarm_alarm_close_on" class="telecare_alarm_alarm_close_on">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_close_on->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_close_on->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_close_on->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_close) == "") { ?>
		<th data-name="alarm_close"><div id="elh_telecare_alarm_alarm_close" class="telecare_alarm_alarm_close"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_close->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_close"><div><div id="elh_telecare_alarm_alarm_close" class="telecare_alarm_alarm_close">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_close->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_close->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_close->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_call_parents) == "") { ?>
		<th data-name="alarm_call_parents"><div id="elh_telecare_alarm_alarm_call_parents" class="telecare_alarm_alarm_call_parents"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_call_parents->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_call_parents"><div><div id="elh_telecare_alarm_alarm_call_parents" class="telecare_alarm_alarm_call_parents">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_call_parents->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_call_parents->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_call_parents->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_call_emergency) == "") { ?>
		<th data-name="alarm_call_emergency"><div id="elh_telecare_alarm_alarm_call_emergency" class="telecare_alarm_alarm_call_emergency"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_call_emergency->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_call_emergency"><div><div id="elh_telecare_alarm_alarm_call_emergency" class="telecare_alarm_alarm_call_emergency">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_call_emergency->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_call_emergency->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_call_emergency->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_alarm->alarm_last_update->Visible) { // alarm_last_update ?>
	<?php if ($telecare_alarm->SortUrl($telecare_alarm->alarm_last_update) == "") { ?>
		<th data-name="alarm_last_update"><div id="elh_telecare_alarm_alarm_last_update" class="telecare_alarm_alarm_last_update"><div class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_last_update->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="alarm_last_update"><div><div id="elh_telecare_alarm_alarm_last_update" class="telecare_alarm_alarm_last_update">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_alarm->alarm_last_update->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_alarm->alarm_last_update->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_alarm->alarm_last_update->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$telecare_alarm_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$telecare_alarm_grid->StartRec = 1;
$telecare_alarm_grid->StopRec = $telecare_alarm_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($telecare_alarm_grid->FormKeyCountName) && ($telecare_alarm->CurrentAction == "gridadd" || $telecare_alarm->CurrentAction == "gridedit" || $telecare_alarm->CurrentAction == "F")) {
		$telecare_alarm_grid->KeyCount = $objForm->GetValue($telecare_alarm_grid->FormKeyCountName);
		$telecare_alarm_grid->StopRec = $telecare_alarm_grid->StartRec + $telecare_alarm_grid->KeyCount - 1;
	}
}
$telecare_alarm_grid->RecCnt = $telecare_alarm_grid->StartRec - 1;
if ($telecare_alarm_grid->Recordset && !$telecare_alarm_grid->Recordset->EOF) {
	$telecare_alarm_grid->Recordset->MoveFirst();
	$bSelectLimit = $telecare_alarm_grid->UseSelectLimit;
	if (!$bSelectLimit && $telecare_alarm_grid->StartRec > 1)
		$telecare_alarm_grid->Recordset->Move($telecare_alarm_grid->StartRec - 1);
} elseif (!$telecare_alarm->AllowAddDeleteRow && $telecare_alarm_grid->StopRec == 0) {
	$telecare_alarm_grid->StopRec = $telecare_alarm->GridAddRowCount;
}

// Initialize aggregate
$telecare_alarm->RowType = EW_ROWTYPE_AGGREGATEINIT;
$telecare_alarm->ResetAttrs();
$telecare_alarm_grid->RenderRow();
if ($telecare_alarm->CurrentAction == "gridadd")
	$telecare_alarm_grid->RowIndex = 0;
if ($telecare_alarm->CurrentAction == "gridedit")
	$telecare_alarm_grid->RowIndex = 0;
while ($telecare_alarm_grid->RecCnt < $telecare_alarm_grid->StopRec) {
	$telecare_alarm_grid->RecCnt++;
	if (intval($telecare_alarm_grid->RecCnt) >= intval($telecare_alarm_grid->StartRec)) {
		$telecare_alarm_grid->RowCnt++;
		if ($telecare_alarm->CurrentAction == "gridadd" || $telecare_alarm->CurrentAction == "gridedit" || $telecare_alarm->CurrentAction == "F") {
			$telecare_alarm_grid->RowIndex++;
			$objForm->Index = $telecare_alarm_grid->RowIndex;
			if ($objForm->HasValue($telecare_alarm_grid->FormActionName))
				$telecare_alarm_grid->RowAction = strval($objForm->GetValue($telecare_alarm_grid->FormActionName));
			elseif ($telecare_alarm->CurrentAction == "gridadd")
				$telecare_alarm_grid->RowAction = "insert";
			else
				$telecare_alarm_grid->RowAction = "";
		}

		// Set up key count
		$telecare_alarm_grid->KeyCount = $telecare_alarm_grid->RowIndex;

		// Init row class and style
		$telecare_alarm->ResetAttrs();
		$telecare_alarm->CssClass = "";
		if ($telecare_alarm->CurrentAction == "gridadd") {
			if ($telecare_alarm->CurrentMode == "copy") {
				$telecare_alarm_grid->LoadRowValues($telecare_alarm_grid->Recordset); // Load row values
				$telecare_alarm_grid->SetRecordKey($telecare_alarm_grid->RowOldKey, $telecare_alarm_grid->Recordset); // Set old record key
			} else {
				$telecare_alarm_grid->LoadDefaultValues(); // Load default values
				$telecare_alarm_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$telecare_alarm_grid->LoadRowValues($telecare_alarm_grid->Recordset); // Load row values
		}
		$telecare_alarm->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($telecare_alarm->CurrentAction == "gridadd") // Grid add
			$telecare_alarm->RowType = EW_ROWTYPE_ADD; // Render add
		if ($telecare_alarm->CurrentAction == "gridadd" && $telecare_alarm->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$telecare_alarm_grid->RestoreCurrentRowFormValues($telecare_alarm_grid->RowIndex); // Restore form values
		if ($telecare_alarm->CurrentAction == "gridedit") { // Grid edit
			if ($telecare_alarm->EventCancelled) {
				$telecare_alarm_grid->RestoreCurrentRowFormValues($telecare_alarm_grid->RowIndex); // Restore form values
			}
			if ($telecare_alarm_grid->RowAction == "insert")
				$telecare_alarm->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$telecare_alarm->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($telecare_alarm->CurrentAction == "gridedit" && ($telecare_alarm->RowType == EW_ROWTYPE_EDIT || $telecare_alarm->RowType == EW_ROWTYPE_ADD) && $telecare_alarm->EventCancelled) // Update failed
			$telecare_alarm_grid->RestoreCurrentRowFormValues($telecare_alarm_grid->RowIndex); // Restore form values
		if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) // Edit row
			$telecare_alarm_grid->EditRowCnt++;
		if ($telecare_alarm->CurrentAction == "F") // Confirm row
			$telecare_alarm_grid->RestoreCurrentRowFormValues($telecare_alarm_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$telecare_alarm->RowAttrs = array_merge($telecare_alarm->RowAttrs, array('data-rowindex'=>$telecare_alarm_grid->RowCnt, 'id'=>'r' . $telecare_alarm_grid->RowCnt . '_telecare_alarm', 'data-rowtype'=>$telecare_alarm->RowType));

		// Render row
		$telecare_alarm_grid->RenderRow();

		// Render list options
		$telecare_alarm_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($telecare_alarm_grid->RowAction <> "delete" && $telecare_alarm_grid->RowAction <> "insertdelete" && !($telecare_alarm_grid->RowAction == "insert" && $telecare_alarm->CurrentAction == "F" && $telecare_alarm_grid->EmptyRow())) {
?>
	<tr<?php echo $telecare_alarm->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_alarm_grid->ListOptions->Render("body", "left", $telecare_alarm_grid->RowCnt);
?>
	<?php if ($telecare_alarm->alarm_id->Visible) { // alarm_id ?>
		<td data-name="alarm_id"<?php echo $telecare_alarm->alarm_id->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_id" class="form-group telecare_alarm_alarm_id">
<span<?php echo $telecare_alarm->alarm_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_id->CurrentValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_id" class="telecare_alarm_alarm_id">
<span<?php echo $telecare_alarm->alarm_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_id->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $telecare_alarm_grid->PageObjName . "_row_" . $telecare_alarm_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
		<td data-name="alarm_type_id"<?php echo $telecare_alarm->alarm_type_id->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_type_id" class="form-group telecare_alarm_alarm_type_id">
<select data-table="telecare_alarm" data-field="x_alarm_type_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_type_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_type_id->DisplayValueSeparator) : $telecare_alarm->alarm_type_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id"<?php echo $telecare_alarm->alarm_type_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_type_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_type_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_type_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_type_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_type_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_type_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_type_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_type_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_type_id->LookupFilters += array("f0" => "`alarm_type_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_type_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_type_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo $telecare_alarm->alarm_type_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_type_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_type_id" class="form-group telecare_alarm_alarm_type_id">
<select data-table="telecare_alarm" data-field="x_alarm_type_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_type_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_type_id->DisplayValueSeparator) : $telecare_alarm->alarm_type_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id"<?php echo $telecare_alarm->alarm_type_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_type_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_type_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_type_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_type_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_type_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_type_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_type_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_type_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_type_id->LookupFilters += array("f0" => "`alarm_type_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_type_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_type_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo $telecare_alarm->alarm_type_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_type_id" class="telecare_alarm_alarm_type_id">
<span<?php echo $telecare_alarm->alarm_type_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_type_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_type_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_type_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
		<td data-name="alarm_user_id"<?php echo $telecare_alarm->alarm_user_id->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($telecare_alarm->alarm_user_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_user_id" class="form-group telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_user_id" class="form-group telecare_alarm_alarm_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_alarm->alarm_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_alarm->alarm_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" style="white-space: nowrap; z-index: <?php echo (9000 - $telecare_alarm_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="sv_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo $telecare_alarm->alarm_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>"<?php echo $telecare_alarm->alarm_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_user_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_user_id->DisplayValueSeparator) : $telecare_alarm->alarm_user_id->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="q_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_alarmgrid.CreateAutoSuggest({"id":"x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($telecare_alarm->alarm_user_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_user_id" class="form-group telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_user_id" class="form-group telecare_alarm_alarm_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_alarm->alarm_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_alarm->alarm_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" style="white-space: nowrap; z-index: <?php echo (9000 - $telecare_alarm_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="sv_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo $telecare_alarm->alarm_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>"<?php echo $telecare_alarm->alarm_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_user_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_user_id->DisplayValueSeparator) : $telecare_alarm->alarm_user_id->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="q_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_alarmgrid.CreateAutoSuggest({"id":"x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_user_id" class="telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_user_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
		<td data-name="alarm_product_id"<?php echo $telecare_alarm->alarm_product_id->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_product_id" class="form-group telecare_alarm_alarm_product_id">
<select data-table="telecare_alarm" data-field="x_alarm_product_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_product_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_product_id->DisplayValueSeparator) : $telecare_alarm->alarm_product_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id"<?php echo $telecare_alarm->alarm_product_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_product_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_product_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_product_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_product_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_product_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_product_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_product_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_product_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_product_id->LookupFilters += array("f0" => "telecare_product.product_id = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_product_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_product_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo $telecare_alarm->alarm_product_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_product_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_product_id" class="form-group telecare_alarm_alarm_product_id">
<select data-table="telecare_alarm" data-field="x_alarm_product_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_product_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_product_id->DisplayValueSeparator) : $telecare_alarm->alarm_product_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id"<?php echo $telecare_alarm->alarm_product_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_product_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_product_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_product_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_product_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_product_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_product_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_product_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_product_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_product_id->LookupFilters += array("f0" => "telecare_product.product_id = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_product_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_product_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo $telecare_alarm->alarm_product_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_product_id" class="telecare_alarm_alarm_product_id">
<span<?php echo $telecare_alarm->alarm_product_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_product_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_product_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_product_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
		<td data-name="alarm_datetime"<?php echo $telecare_alarm->alarm_datetime->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_datetime" class="form-group telecare_alarm_alarm_datetime">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_datetime" data-format="11" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_datetime->EditValue ?>"<?php echo $telecare_alarm->alarm_datetime->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_datetime->ReadOnly && !$telecare_alarm->alarm_datetime->Disabled && !isset($telecare_alarm->alarm_datetime->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_datetime->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmgrid", "x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_datetime" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_datetime" class="form-group telecare_alarm_alarm_datetime">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_datetime" data-format="11" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_datetime->EditValue ?>"<?php echo $telecare_alarm->alarm_datetime->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_datetime->ReadOnly && !$telecare_alarm->alarm_datetime->Disabled && !isset($telecare_alarm->alarm_datetime->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_datetime->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmgrid", "x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_datetime" class="telecare_alarm_alarm_datetime">
<span<?php echo $telecare_alarm->alarm_datetime->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_datetime->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_datetime" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_datetime" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_latitude->Visible) { // alarm_latitude ?>
		<td data-name="alarm_latitude"<?php echo $telecare_alarm->alarm_latitude->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_latitude" class="form-group telecare_alarm_alarm_latitude">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_latitude" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_latitude->EditValue ?>"<?php echo $telecare_alarm->alarm_latitude->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_latitude" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_latitude" class="form-group telecare_alarm_alarm_latitude">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_latitude" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_latitude->EditValue ?>"<?php echo $telecare_alarm->alarm_latitude->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<div id="orig<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_latitude" class="hide">
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_latitude" class="telecare_alarm_alarm_latitude">
<span<?php echo $telecare_alarm->alarm_latitude->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_latitude->ListViewValue() ?></span>
</span>
</div>
<div id="gmap_x<?php echo $telecare_alarm_grid->RowCnt ?>_alarm_latitude" class="ewGoogleMap" style="width: 200px; height: 200px;"></div>
<script type="text/javascript">
ewGoogleMaps[ewGoogleMaps.length] = ewGoogleMaps["gmap_x<?php echo $telecare_alarm_grid->RowCnt ?>_alarm_latitude"] = jQuery.extend({"id":"gmap_x<?php echo $telecare_alarm_grid->RowCnt ?>_alarm_latitude","width":200,"height":200,"latitude":null,"longitude":null,"address":null,"type":"ROADMAP","zoom":8,"title":null,"icon":null,"use_single_map":false,"single_map_width":400,"single_map_height":400,"show_map_on_top":true,"show_all_markers":true,"description":null}, {
	latitude: <?php echo ew_VarToJson($telecare_alarm->alarm_latitude->CurrentValue, "undefined") ?>,
	longitude: <?php echo ew_VarToJson($telecare_alarm->alarm_longitude->CurrentValue, "undefined") ?>
});
</script>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_latitude" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_latitude" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
		<td data-name="alarm_start_on"<?php echo $telecare_alarm->alarm_start_on->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_start_on" class="form-group telecare_alarm_alarm_start_on">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_start_on" data-format="11" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_start_on->EditValue ?>"<?php echo $telecare_alarm->alarm_start_on->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_start_on->ReadOnly && !$telecare_alarm->alarm_start_on->Disabled && !isset($telecare_alarm->alarm_start_on->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmgrid", "x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_start_on" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_start_on" class="form-group telecare_alarm_alarm_start_on">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_start_on" data-format="11" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_start_on->EditValue ?>"<?php echo $telecare_alarm->alarm_start_on->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_start_on->ReadOnly && !$telecare_alarm->alarm_start_on->Disabled && !isset($telecare_alarm->alarm_start_on->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmgrid", "x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_start_on" class="telecare_alarm_alarm_start_on">
<span<?php echo $telecare_alarm->alarm_start_on->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_start_on->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_start_on" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_start_on" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_close_on->Visible) { // alarm_close_on ?>
		<td data-name="alarm_close_on"<?php echo $telecare_alarm->alarm_close_on->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close_on" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close_on->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_close_on" class="telecare_alarm_alarm_close_on">
<span<?php echo $telecare_alarm->alarm_close_on->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_close_on->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close_on" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close_on->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close_on" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close_on->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
		<td data-name="alarm_close"<?php echo $telecare_alarm->alarm_close->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_close" class="form-group telecare_alarm_alarm_close">
<select data-table="telecare_alarm" data-field="x_alarm_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_close->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_close->DisplayValueSeparator) : $telecare_alarm->alarm_close->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close"<?php echo $telecare_alarm->alarm_close->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_close->EditValue)) {
	$arwrk = $telecare_alarm->alarm_close->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_close->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_close->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_close->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_close->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_close->OldValue = "";
?>
</select>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_close" class="form-group telecare_alarm_alarm_close">
<select data-table="telecare_alarm" data-field="x_alarm_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_close->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_close->DisplayValueSeparator) : $telecare_alarm->alarm_close->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close"<?php echo $telecare_alarm->alarm_close->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_close->EditValue)) {
	$arwrk = $telecare_alarm->alarm_close->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_close->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_close->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_close->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_close->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_close->OldValue = "";
?>
</select>
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_close" class="telecare_alarm_alarm_close">
<span<?php echo $telecare_alarm->alarm_close->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_close->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
		<td data-name="alarm_call_parents"<?php echo $telecare_alarm->alarm_call_parents->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_call_parents" class="form-group telecare_alarm_alarm_call_parents">
<div id="tp_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_parents->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_parents->DisplayValueSeparator) : $telecare_alarm->alarm_call_parents->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="{value}"<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_parents->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_parents->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_parents->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_call_parents->OldValue = "";
?>
</div></div>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_call_parents" class="form-group telecare_alarm_alarm_call_parents">
<div id="tp_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_parents->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_parents->DisplayValueSeparator) : $telecare_alarm->alarm_call_parents->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="{value}"<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_parents->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_parents->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_parents->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_call_parents->OldValue = "";
?>
</div></div>
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_call_parents" class="telecare_alarm_alarm_call_parents">
<span<?php echo $telecare_alarm->alarm_call_parents->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_call_parents->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
		<td data-name="alarm_call_emergency"<?php echo $telecare_alarm->alarm_call_emergency->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_call_emergency" class="form-group telecare_alarm_alarm_call_emergency">
<div id="tp_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) : $telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="{value}"<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_emergency->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_emergency->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_emergency->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_call_emergency->OldValue = "";
?>
</div></div>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_call_emergency" class="form-group telecare_alarm_alarm_call_emergency">
<div id="tp_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) : $telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="{value}"<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_emergency->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_emergency->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_emergency->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_call_emergency->OldValue = "";
?>
</div></div>
</span>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_call_emergency" class="telecare_alarm_alarm_call_emergency">
<span<?php echo $telecare_alarm->alarm_call_emergency->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_call_emergency->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_last_update->Visible) { // alarm_last_update ?>
		<td data-name="alarm_last_update"<?php echo $telecare_alarm->alarm_last_update->CellAttributes() ?>>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_last_update" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_last_update->OldValue) ?>">
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_last_update" class="telecare_alarm_alarm_last_update">
<span<?php echo $telecare_alarm->alarm_last_update->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_last_update->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_last_update" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_last_update->FormValue) ?>">
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_last_update" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_last_update->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_alarm_grid->ListOptions->Render("body", "right", $telecare_alarm_grid->RowCnt);
?>
	</tr>
<?php if ($telecare_alarm->RowType == EW_ROWTYPE_ADD || $telecare_alarm->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
ftelecare_alarmgrid.UpdateOpts(<?php echo $telecare_alarm_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($telecare_alarm->CurrentAction <> "gridadd" || $telecare_alarm->CurrentMode == "copy")
		if (!$telecare_alarm_grid->Recordset->EOF) $telecare_alarm_grid->Recordset->MoveNext();
}
?>
<?php
	if ($telecare_alarm->CurrentMode == "add" || $telecare_alarm->CurrentMode == "copy" || $telecare_alarm->CurrentMode == "edit") {
		$telecare_alarm_grid->RowIndex = '$rowindex$';
		$telecare_alarm_grid->LoadDefaultValues();

		// Set row properties
		$telecare_alarm->ResetAttrs();
		$telecare_alarm->RowAttrs = array_merge($telecare_alarm->RowAttrs, array('data-rowindex'=>$telecare_alarm_grid->RowIndex, 'id'=>'r0_telecare_alarm', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($telecare_alarm->RowAttrs["class"], "ewTemplate");
		$telecare_alarm->RowType = EW_ROWTYPE_ADD;

		// Render row
		$telecare_alarm_grid->RenderRow();

		// Render list options
		$telecare_alarm_grid->RenderListOptions();
		$telecare_alarm_grid->StartRowCnt = 0;
?>
	<tr<?php echo $telecare_alarm->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_alarm_grid->ListOptions->Render("body", "left", $telecare_alarm_grid->RowIndex);
?>
	<?php if ($telecare_alarm->alarm_id->Visible) { // alarm_id ?>
		<td data-name="alarm_id">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_id" class="form-group telecare_alarm_alarm_id">
<span<?php echo $telecare_alarm->alarm_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
		<td data-name="alarm_type_id">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_type_id" class="form-group telecare_alarm_alarm_type_id">
<select data-table="telecare_alarm" data-field="x_alarm_type_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_type_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_type_id->DisplayValueSeparator) : $telecare_alarm->alarm_type_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id"<?php echo $telecare_alarm->alarm_type_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_type_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_type_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_type_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_type_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_type_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_type_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_type_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_type_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_type_id->LookupFilters += array("f0" => "`alarm_type_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_type_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_type_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo $telecare_alarm->alarm_type_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_type_id" class="form-group telecare_alarm_alarm_type_id">
<span<?php echo $telecare_alarm->alarm_type_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_type_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_type_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_type_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_type_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
		<td data-name="alarm_user_id">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<?php if ($telecare_alarm->alarm_user_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_user_id" class="form-group telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_user_id" class="form-group telecare_alarm_alarm_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_alarm->alarm_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_alarm->alarm_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" style="white-space: nowrap; z-index: <?php echo (9000 - $telecare_alarm_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="sv_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo $telecare_alarm->alarm_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>"<?php echo $telecare_alarm->alarm_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_user_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_user_id->DisplayValueSeparator) : $telecare_alarm->alarm_user_id->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="q_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_alarmgrid.CreateAutoSuggest({"id":"x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_user_id" class="form-group telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
		<td data-name="alarm_product_id">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_product_id" class="form-group telecare_alarm_alarm_product_id">
<select data-table="telecare_alarm" data-field="x_alarm_product_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_product_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_product_id->DisplayValueSeparator) : $telecare_alarm->alarm_product_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id"<?php echo $telecare_alarm->alarm_product_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_product_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_product_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_product_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_product_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_product_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_product_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_product_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_product_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_product_id->LookupFilters += array("f0" => "telecare_product.product_id = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_product_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_product_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="s_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo $telecare_alarm->alarm_product_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_product_id" class="form-group telecare_alarm_alarm_product_id">
<span<?php echo $telecare_alarm->alarm_product_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_product_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_product_id" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_product_id" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_product_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
		<td data-name="alarm_datetime">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_datetime" class="form-group telecare_alarm_alarm_datetime">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_datetime" data-format="11" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_datetime->EditValue ?>"<?php echo $telecare_alarm->alarm_datetime->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_datetime->ReadOnly && !$telecare_alarm->alarm_datetime->Disabled && !isset($telecare_alarm->alarm_datetime->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_datetime->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmgrid", "x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_datetime" class="form-group telecare_alarm_alarm_datetime">
<span<?php echo $telecare_alarm->alarm_datetime->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_datetime->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_datetime" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_datetime" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_datetime" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_latitude->Visible) { // alarm_latitude ?>
		<td data-name="alarm_latitude">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_latitude" class="form-group telecare_alarm_alarm_latitude">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_latitude" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_latitude->EditValue ?>"<?php echo $telecare_alarm->alarm_latitude->EditAttributes() ?>>
</span>
<?php } else { ?>
<div id="orig<?php echo $telecare_alarm_grid->RowCnt ?>_telecare_alarm_alarm_latitude" class="hide">
<span id="el$rowindex$_telecare_alarm_alarm_latitude" class="form-group telecare_alarm_alarm_latitude">
<span<?php echo $telecare_alarm->alarm_latitude->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_latitude->ViewValue ?></p></span>
</span>
</div>
<div id="gmap_x<?php echo $telecare_alarm_grid->RowCnt ?>_alarm_latitude" class="ewGoogleMap" style="width: 200px; height: 200px;"></div>
<script type="text/javascript">
ewGoogleMaps[ewGoogleMaps.length] = ewGoogleMaps["gmap_x<?php echo $telecare_alarm_grid->RowCnt ?>_alarm_latitude"] = jQuery.extend({"id":"gmap_x<?php echo $telecare_alarm_grid->RowCnt ?>_alarm_latitude","width":200,"height":200,"latitude":null,"longitude":null,"address":null,"type":"ROADMAP","zoom":8,"title":null,"icon":null,"use_single_map":false,"single_map_width":400,"single_map_height":400,"show_map_on_top":true,"show_all_markers":true,"description":null}, {
	latitude: <?php echo ew_VarToJson($telecare_alarm->alarm_latitude->CurrentValue, "undefined") ?>,
	longitude: <?php echo ew_VarToJson($telecare_alarm->alarm_longitude->CurrentValue, "undefined") ?>
});
</script>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_latitude" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_latitude" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_latitude" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
		<td data-name="alarm_start_on">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_start_on" class="form-group telecare_alarm_alarm_start_on">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_start_on" data-format="11" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_start_on->EditValue ?>"<?php echo $telecare_alarm->alarm_start_on->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_start_on->ReadOnly && !$telecare_alarm->alarm_start_on->Disabled && !isset($telecare_alarm->alarm_start_on->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmgrid", "x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_start_on" class="form-group telecare_alarm_alarm_start_on">
<span<?php echo $telecare_alarm->alarm_start_on->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_start_on->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_start_on" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_start_on" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_start_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_close_on->Visible) { // alarm_close_on ?>
		<td data-name="alarm_close_on">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_close_on" class="form-group telecare_alarm_alarm_close_on">
<span<?php echo $telecare_alarm->alarm_close_on->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_close_on->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close_on" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close_on->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close_on" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close_on" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close_on->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
		<td data-name="alarm_close">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_close" class="form-group telecare_alarm_alarm_close">
<select data-table="telecare_alarm" data-field="x_alarm_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_close->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_close->DisplayValueSeparator) : $telecare_alarm->alarm_close->DisplayValueSeparator) ?>" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close"<?php echo $telecare_alarm->alarm_close->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_close->EditValue)) {
	$arwrk = $telecare_alarm->alarm_close->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_close->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_close->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_close->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_close->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_close->OldValue = "";
?>
</select>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_close" class="form-group telecare_alarm_alarm_close">
<span<?php echo $telecare_alarm->alarm_close->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_close->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_close" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_close" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
		<td data-name="alarm_call_parents">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_call_parents" class="form-group telecare_alarm_alarm_call_parents">
<div id="tp_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_parents->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_parents->DisplayValueSeparator) : $telecare_alarm->alarm_call_parents->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="{value}"<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_parents->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_parents->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_parents->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_call_parents->OldValue = "";
?>
</div></div>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_call_parents" class="form-group telecare_alarm_alarm_call_parents">
<span<?php echo $telecare_alarm->alarm_call_parents->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_call_parents->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_parents" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
		<td data-name="alarm_call_emergency">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_alarm_alarm_call_emergency" class="form-group telecare_alarm_alarm_call_emergency">
<div id="tp_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) : $telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ?>" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="{value}"<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_emergency->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_emergency->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_emergency->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_alarm->alarm_call_emergency->OldValue = "";
?>
</div></div>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_call_emergency" class="form-group telecare_alarm_alarm_call_emergency">
<span<?php echo $telecare_alarm->alarm_call_emergency->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_call_emergency->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_call_emergency" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_alarm->alarm_last_update->Visible) { // alarm_last_update ?>
		<td data-name="alarm_last_update">
<?php if ($telecare_alarm->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_alarm_alarm_last_update" class="form-group telecare_alarm_alarm_last_update">
<span<?php echo $telecare_alarm->alarm_last_update->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_last_update->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_last_update" name="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" id="x<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_last_update->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_last_update" name="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" id="o<?php echo $telecare_alarm_grid->RowIndex ?>_alarm_last_update" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_last_update->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_alarm_grid->ListOptions->Render("body", "right", $telecare_alarm_grid->RowCnt);
?>
<script type="text/javascript">
ftelecare_alarmgrid.UpdateOpts(<?php echo $telecare_alarm_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($telecare_alarm->CurrentMode == "add" || $telecare_alarm->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $telecare_alarm_grid->FormKeyCountName ?>" id="<?php echo $telecare_alarm_grid->FormKeyCountName ?>" value="<?php echo $telecare_alarm_grid->KeyCount ?>">
<?php echo $telecare_alarm_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_alarm->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $telecare_alarm_grid->FormKeyCountName ?>" id="<?php echo $telecare_alarm_grid->FormKeyCountName ?>" value="<?php echo $telecare_alarm_grid->KeyCount ?>">
<?php echo $telecare_alarm_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_alarm->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="ftelecare_alarmgrid">
</div>
<?php

// Close recordset
if ($telecare_alarm_grid->Recordset)
	$telecare_alarm_grid->Recordset->Close();
?>
<?php if ($telecare_alarm_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($telecare_alarm_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($telecare_alarm_grid->TotalRecs == 0 && $telecare_alarm->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_alarm_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($telecare_alarm->Export == "") { ?>
<script type="text/javascript">
ftelecare_alarmgrid.Init();
</script>
<?php } ?>
<?php
$telecare_alarm_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$telecare_alarm_grid->Page_Terminate();
?>
