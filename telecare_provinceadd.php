<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_provinceinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_province_add = NULL; // Initialize page object first

class ctelecare_province_add extends ctelecare_province {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_province';

	// Page object name
	var $PageObjName = 'telecare_province_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_province)
		if (!isset($GLOBALS["telecare_province"]) || get_class($GLOBALS["telecare_province"]) == "ctelecare_province") {
			$GLOBALS["telecare_province"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_province"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_province', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_provincelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_province;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_province);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["province_id"] != "") {
				$this->province_id->setQueryStringValue($_GET["province_id"]);
				$this->setKey("province_id", $this->province_id->CurrentValue); // Set up key
			} else {
				$this->setKey("province_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_provincelist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_provinceview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->province_name->CurrentValue = NULL;
		$this->province_name->OldValue = $this->province_name->CurrentValue;
		$this->province_slug->CurrentValue = NULL;
		$this->province_slug->OldValue = $this->province_slug->CurrentValue;
		$this->province_abbr->CurrentValue = NULL;
		$this->province_abbr->OldValue = $this->province_abbr->CurrentValue;
		$this->province_istat_code->CurrentValue = NULL;
		$this->province_istat_code->OldValue = $this->province_istat_code->CurrentValue;
		$this->province_region_istat_code->CurrentValue = NULL;
		$this->province_region_istat_code->OldValue = $this->province_region_istat_code->CurrentValue;
		$this->province_region_id->CurrentValue = NULL;
		$this->province_region_id->OldValue = $this->province_region_id->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->province_name->FldIsDetailKey) {
			$this->province_name->setFormValue($objForm->GetValue("x_province_name"));
		}
		if (!$this->province_slug->FldIsDetailKey) {
			$this->province_slug->setFormValue($objForm->GetValue("x_province_slug"));
		}
		if (!$this->province_abbr->FldIsDetailKey) {
			$this->province_abbr->setFormValue($objForm->GetValue("x_province_abbr"));
		}
		if (!$this->province_istat_code->FldIsDetailKey) {
			$this->province_istat_code->setFormValue($objForm->GetValue("x_province_istat_code"));
		}
		if (!$this->province_region_istat_code->FldIsDetailKey) {
			$this->province_region_istat_code->setFormValue($objForm->GetValue("x_province_region_istat_code"));
		}
		if (!$this->province_region_id->FldIsDetailKey) {
			$this->province_region_id->setFormValue($objForm->GetValue("x_province_region_id"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->province_name->CurrentValue = $this->province_name->FormValue;
		$this->province_slug->CurrentValue = $this->province_slug->FormValue;
		$this->province_abbr->CurrentValue = $this->province_abbr->FormValue;
		$this->province_istat_code->CurrentValue = $this->province_istat_code->FormValue;
		$this->province_region_istat_code->CurrentValue = $this->province_region_istat_code->FormValue;
		$this->province_region_id->CurrentValue = $this->province_region_id->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->province_id->setDbValue($rs->fields('province_id'));
		$this->province_name->setDbValue($rs->fields('province_name'));
		$this->province_slug->setDbValue($rs->fields('province_slug'));
		$this->province_abbr->setDbValue($rs->fields('province_abbr'));
		$this->province_istat_code->setDbValue($rs->fields('province_istat_code'));
		$this->province_region_istat_code->setDbValue($rs->fields('province_region_istat_code'));
		$this->province_region_id->setDbValue($rs->fields('province_region_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->province_id->DbValue = $row['province_id'];
		$this->province_name->DbValue = $row['province_name'];
		$this->province_slug->DbValue = $row['province_slug'];
		$this->province_abbr->DbValue = $row['province_abbr'];
		$this->province_istat_code->DbValue = $row['province_istat_code'];
		$this->province_region_istat_code->DbValue = $row['province_region_istat_code'];
		$this->province_region_id->DbValue = $row['province_region_id'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("province_id")) <> "")
			$this->province_id->CurrentValue = $this->getKey("province_id"); // province_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// province_id
		// province_name
		// province_slug
		// province_abbr
		// province_istat_code
		// province_region_istat_code
		// province_region_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// province_id
		$this->province_id->ViewValue = $this->province_id->CurrentValue;
		$this->province_id->ViewCustomAttributes = "";

		// province_name
		$this->province_name->ViewValue = $this->province_name->CurrentValue;
		$this->province_name->ViewCustomAttributes = "";

		// province_slug
		$this->province_slug->ViewValue = $this->province_slug->CurrentValue;
		$this->province_slug->ViewCustomAttributes = "";

		// province_abbr
		$this->province_abbr->ViewValue = $this->province_abbr->CurrentValue;
		$this->province_abbr->ViewCustomAttributes = "";

		// province_istat_code
		$this->province_istat_code->ViewValue = $this->province_istat_code->CurrentValue;
		$this->province_istat_code->ViewCustomAttributes = "";

		// province_region_istat_code
		$this->province_region_istat_code->ViewValue = $this->province_region_istat_code->CurrentValue;
		$this->province_region_istat_code->ViewCustomAttributes = "";

		// province_region_id
		$this->province_region_id->ViewValue = $this->province_region_id->CurrentValue;
		$this->province_region_id->ViewCustomAttributes = "";

			// province_name
			$this->province_name->LinkCustomAttributes = "";
			$this->province_name->HrefValue = "";
			$this->province_name->TooltipValue = "";

			// province_slug
			$this->province_slug->LinkCustomAttributes = "";
			$this->province_slug->HrefValue = "";
			$this->province_slug->TooltipValue = "";

			// province_abbr
			$this->province_abbr->LinkCustomAttributes = "";
			$this->province_abbr->HrefValue = "";
			$this->province_abbr->TooltipValue = "";

			// province_istat_code
			$this->province_istat_code->LinkCustomAttributes = "";
			$this->province_istat_code->HrefValue = "";
			$this->province_istat_code->TooltipValue = "";

			// province_region_istat_code
			$this->province_region_istat_code->LinkCustomAttributes = "";
			$this->province_region_istat_code->HrefValue = "";
			$this->province_region_istat_code->TooltipValue = "";

			// province_region_id
			$this->province_region_id->LinkCustomAttributes = "";
			$this->province_region_id->HrefValue = "";
			$this->province_region_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// province_name
			$this->province_name->EditAttrs["class"] = "form-control";
			$this->province_name->EditCustomAttributes = "";
			$this->province_name->EditValue = ew_HtmlEncode($this->province_name->CurrentValue);
			$this->province_name->PlaceHolder = ew_RemoveHtml($this->province_name->FldCaption());

			// province_slug
			$this->province_slug->EditAttrs["class"] = "form-control";
			$this->province_slug->EditCustomAttributes = "";
			$this->province_slug->EditValue = ew_HtmlEncode($this->province_slug->CurrentValue);
			$this->province_slug->PlaceHolder = ew_RemoveHtml($this->province_slug->FldCaption());

			// province_abbr
			$this->province_abbr->EditAttrs["class"] = "form-control";
			$this->province_abbr->EditCustomAttributes = "";
			$this->province_abbr->EditValue = ew_HtmlEncode($this->province_abbr->CurrentValue);
			$this->province_abbr->PlaceHolder = ew_RemoveHtml($this->province_abbr->FldCaption());

			// province_istat_code
			$this->province_istat_code->EditAttrs["class"] = "form-control";
			$this->province_istat_code->EditCustomAttributes = "";
			$this->province_istat_code->EditValue = ew_HtmlEncode($this->province_istat_code->CurrentValue);
			$this->province_istat_code->PlaceHolder = ew_RemoveHtml($this->province_istat_code->FldCaption());

			// province_region_istat_code
			$this->province_region_istat_code->EditAttrs["class"] = "form-control";
			$this->province_region_istat_code->EditCustomAttributes = "";
			$this->province_region_istat_code->EditValue = ew_HtmlEncode($this->province_region_istat_code->CurrentValue);
			$this->province_region_istat_code->PlaceHolder = ew_RemoveHtml($this->province_region_istat_code->FldCaption());

			// province_region_id
			$this->province_region_id->EditAttrs["class"] = "form-control";
			$this->province_region_id->EditCustomAttributes = "";
			$this->province_region_id->EditValue = ew_HtmlEncode($this->province_region_id->CurrentValue);
			$this->province_region_id->PlaceHolder = ew_RemoveHtml($this->province_region_id->FldCaption());

			// Edit refer script
			// province_name

			$this->province_name->LinkCustomAttributes = "";
			$this->province_name->HrefValue = "";

			// province_slug
			$this->province_slug->LinkCustomAttributes = "";
			$this->province_slug->HrefValue = "";

			// province_abbr
			$this->province_abbr->LinkCustomAttributes = "";
			$this->province_abbr->HrefValue = "";

			// province_istat_code
			$this->province_istat_code->LinkCustomAttributes = "";
			$this->province_istat_code->HrefValue = "";

			// province_region_istat_code
			$this->province_region_istat_code->LinkCustomAttributes = "";
			$this->province_region_istat_code->HrefValue = "";

			// province_region_id
			$this->province_region_id->LinkCustomAttributes = "";
			$this->province_region_id->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->province_name->FldIsDetailKey && !is_null($this->province_name->FormValue) && $this->province_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->province_name->FldCaption(), $this->province_name->ReqErrMsg));
		}
		if (!$this->province_slug->FldIsDetailKey && !is_null($this->province_slug->FormValue) && $this->province_slug->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->province_slug->FldCaption(), $this->province_slug->ReqErrMsg));
		}
		if (!$this->province_abbr->FldIsDetailKey && !is_null($this->province_abbr->FormValue) && $this->province_abbr->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->province_abbr->FldCaption(), $this->province_abbr->ReqErrMsg));
		}
		if (!$this->province_istat_code->FldIsDetailKey && !is_null($this->province_istat_code->FormValue) && $this->province_istat_code->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->province_istat_code->FldCaption(), $this->province_istat_code->ReqErrMsg));
		}
		if (!$this->province_region_istat_code->FldIsDetailKey && !is_null($this->province_region_istat_code->FormValue) && $this->province_region_istat_code->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->province_region_istat_code->FldCaption(), $this->province_region_istat_code->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->province_region_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->province_region_id->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// province_name
		$this->province_name->SetDbValueDef($rsnew, $this->province_name->CurrentValue, "", FALSE);

		// province_slug
		$this->province_slug->SetDbValueDef($rsnew, $this->province_slug->CurrentValue, "", FALSE);

		// province_abbr
		$this->province_abbr->SetDbValueDef($rsnew, $this->province_abbr->CurrentValue, "", FALSE);

		// province_istat_code
		$this->province_istat_code->SetDbValueDef($rsnew, $this->province_istat_code->CurrentValue, "", FALSE);

		// province_region_istat_code
		$this->province_region_istat_code->SetDbValueDef($rsnew, $this->province_region_istat_code->CurrentValue, "", FALSE);

		// province_region_id
		$this->province_region_id->SetDbValueDef($rsnew, $this->province_region_id->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->province_id->setDbValue($conn->Insert_ID());
				$rsnew['province_id'] = $this->province_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_provincelist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_province_add)) $telecare_province_add = new ctelecare_province_add();

// Page init
$telecare_province_add->Page_Init();

// Page main
$telecare_province_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_province_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_provinceadd = new ew_Form("ftelecare_provinceadd", "add");

// Validate form
ftelecare_provinceadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_province_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_province->province_name->FldCaption(), $telecare_province->province_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_province_slug");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_province->province_slug->FldCaption(), $telecare_province->province_slug->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_province_abbr");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_province->province_abbr->FldCaption(), $telecare_province->province_abbr->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_province_istat_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_province->province_istat_code->FldCaption(), $telecare_province->province_istat_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_province_region_istat_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_province->province_region_istat_code->FldCaption(), $telecare_province->province_region_istat_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_province_region_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_province->province_region_id->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_provinceadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_provinceadd.ValidateRequired = true;
<?php } else { ?>
ftelecare_provinceadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_province_add->ShowPageHeader(); ?>
<?php
$telecare_province_add->ShowMessage();
?>
<form name="ftelecare_provinceadd" id="ftelecare_provinceadd" class="<?php echo $telecare_province_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_province_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_province_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_province">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($telecare_province->province_name->Visible) { // province_name ?>
	<div id="r_province_name" class="form-group">
		<label id="elh_telecare_province_province_name" for="x_province_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_province->province_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_province->province_name->CellAttributes() ?>>
<span id="el_telecare_province_province_name">
<input type="text" data-table="telecare_province" data-field="x_province_name" name="x_province_name" id="x_province_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_province->province_name->getPlaceHolder()) ?>" value="<?php echo $telecare_province->province_name->EditValue ?>"<?php echo $telecare_province->province_name->EditAttributes() ?>>
</span>
<?php echo $telecare_province->province_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_province->province_slug->Visible) { // province_slug ?>
	<div id="r_province_slug" class="form-group">
		<label id="elh_telecare_province_province_slug" for="x_province_slug" class="col-sm-2 control-label ewLabel"><?php echo $telecare_province->province_slug->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_province->province_slug->CellAttributes() ?>>
<span id="el_telecare_province_province_slug">
<input type="text" data-table="telecare_province" data-field="x_province_slug" name="x_province_slug" id="x_province_slug" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_province->province_slug->getPlaceHolder()) ?>" value="<?php echo $telecare_province->province_slug->EditValue ?>"<?php echo $telecare_province->province_slug->EditAttributes() ?>>
</span>
<?php echo $telecare_province->province_slug->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_province->province_abbr->Visible) { // province_abbr ?>
	<div id="r_province_abbr" class="form-group">
		<label id="elh_telecare_province_province_abbr" for="x_province_abbr" class="col-sm-2 control-label ewLabel"><?php echo $telecare_province->province_abbr->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_province->province_abbr->CellAttributes() ?>>
<span id="el_telecare_province_province_abbr">
<input type="text" data-table="telecare_province" data-field="x_province_abbr" name="x_province_abbr" id="x_province_abbr" size="30" maxlength="21" placeholder="<?php echo ew_HtmlEncode($telecare_province->province_abbr->getPlaceHolder()) ?>" value="<?php echo $telecare_province->province_abbr->EditValue ?>"<?php echo $telecare_province->province_abbr->EditAttributes() ?>>
</span>
<?php echo $telecare_province->province_abbr->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_province->province_istat_code->Visible) { // province_istat_code ?>
	<div id="r_province_istat_code" class="form-group">
		<label id="elh_telecare_province_province_istat_code" for="x_province_istat_code" class="col-sm-2 control-label ewLabel"><?php echo $telecare_province->province_istat_code->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_province->province_istat_code->CellAttributes() ?>>
<span id="el_telecare_province_province_istat_code">
<input type="text" data-table="telecare_province" data-field="x_province_istat_code" name="x_province_istat_code" id="x_province_istat_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_province->province_istat_code->getPlaceHolder()) ?>" value="<?php echo $telecare_province->province_istat_code->EditValue ?>"<?php echo $telecare_province->province_istat_code->EditAttributes() ?>>
</span>
<?php echo $telecare_province->province_istat_code->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_province->province_region_istat_code->Visible) { // province_region_istat_code ?>
	<div id="r_province_region_istat_code" class="form-group">
		<label id="elh_telecare_province_province_region_istat_code" for="x_province_region_istat_code" class="col-sm-2 control-label ewLabel"><?php echo $telecare_province->province_region_istat_code->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_province->province_region_istat_code->CellAttributes() ?>>
<span id="el_telecare_province_province_region_istat_code">
<input type="text" data-table="telecare_province" data-field="x_province_region_istat_code" name="x_province_region_istat_code" id="x_province_region_istat_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_province->province_region_istat_code->getPlaceHolder()) ?>" value="<?php echo $telecare_province->province_region_istat_code->EditValue ?>"<?php echo $telecare_province->province_region_istat_code->EditAttributes() ?>>
</span>
<?php echo $telecare_province->province_region_istat_code->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_province->province_region_id->Visible) { // province_region_id ?>
	<div id="r_province_region_id" class="form-group">
		<label id="elh_telecare_province_province_region_id" for="x_province_region_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_province->province_region_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_province->province_region_id->CellAttributes() ?>>
<span id="el_telecare_province_province_region_id">
<input type="text" data-table="telecare_province" data-field="x_province_region_id" name="x_province_region_id" id="x_province_region_id" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_province->province_region_id->getPlaceHolder()) ?>" value="<?php echo $telecare_province->province_region_id->EditValue ?>"<?php echo $telecare_province->province_region_id->EditAttributes() ?>>
</span>
<?php echo $telecare_province->province_region_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_province_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_provinceadd.Init();
</script>
<?php
$telecare_province_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_province_add->Page_Terminate();
?>
