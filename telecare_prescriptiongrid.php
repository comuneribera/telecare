<?php include_once "telecare_admininfo.php" ?>
<?php

// Create page object
if (!isset($telecare_prescription_grid)) $telecare_prescription_grid = new ctelecare_prescription_grid();

// Page init
$telecare_prescription_grid->Page_Init();

// Page main
$telecare_prescription_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_prescription_grid->Page_Render();
?>
<?php if ($telecare_prescription->Export == "") { ?>
<script type="text/javascript">

// Form object
var ftelecare_prescriptiongrid = new ew_Form("ftelecare_prescriptiongrid", "grid");
ftelecare_prescriptiongrid.FormKeyCountName = '<?php echo $telecare_prescription_grid->FormKeyCountName ?>';

// Validate form
ftelecare_prescriptiongrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_prescription_request_date");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription->prescription_request_date->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_start_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription->prescription_start_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_close_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription->prescription_close_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_close");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_prescription->prescription_close->FldCaption(), $telecare_prescription->prescription_close->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
ftelecare_prescriptiongrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "prescription_user_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "prescription_drugstore_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "prescription_doctor_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "prescription_request_date", false)) return false;
	if (ew_ValueChanged(fobj, infix, "prescription_start_on", false)) return false;
	if (ew_ValueChanged(fobj, infix, "prescription_close_on", false)) return false;
	if (ew_ValueChanged(fobj, infix, "prescription_close", false)) return false;
	return true;
}

// Form_CustomValidate event
ftelecare_prescriptiongrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_prescriptiongrid.ValidateRequired = true;
<?php } else { ?>
ftelecare_prescriptiongrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_prescriptiongrid.Lists["x_prescription_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiongrid.Lists["x_prescription_drugstore_id"] = {"LinkField":"x_apotech_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_apotech_company_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiongrid.Lists["x_prescription_doctor_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiongrid.Lists["x_prescription_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiongrid.Lists["x_prescription_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_prescriptiongrid.Lists["x_prescription_close"].Options = <?php echo json_encode($telecare_prescription->prescription_close->Options()) ?>;

// Form object for search
</script>
<?php } ?>
<?php
if ($telecare_prescription->CurrentAction == "gridadd") {
	if ($telecare_prescription->CurrentMode == "copy") {
		$bSelectLimit = $telecare_prescription_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$telecare_prescription_grid->TotalRecs = $telecare_prescription->SelectRecordCount();
			$telecare_prescription_grid->Recordset = $telecare_prescription_grid->LoadRecordset($telecare_prescription_grid->StartRec-1, $telecare_prescription_grid->DisplayRecs);
		} else {
			if ($telecare_prescription_grid->Recordset = $telecare_prescription_grid->LoadRecordset())
				$telecare_prescription_grid->TotalRecs = $telecare_prescription_grid->Recordset->RecordCount();
		}
		$telecare_prescription_grid->StartRec = 1;
		$telecare_prescription_grid->DisplayRecs = $telecare_prescription_grid->TotalRecs;
	} else {
		$telecare_prescription->CurrentFilter = "0=1";
		$telecare_prescription_grid->StartRec = 1;
		$telecare_prescription_grid->DisplayRecs = $telecare_prescription->GridAddRowCount;
	}
	$telecare_prescription_grid->TotalRecs = $telecare_prescription_grid->DisplayRecs;
	$telecare_prescription_grid->StopRec = $telecare_prescription_grid->DisplayRecs;
} else {
	$bSelectLimit = $telecare_prescription_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($telecare_prescription_grid->TotalRecs <= 0)
			$telecare_prescription_grid->TotalRecs = $telecare_prescription->SelectRecordCount();
	} else {
		if (!$telecare_prescription_grid->Recordset && ($telecare_prescription_grid->Recordset = $telecare_prescription_grid->LoadRecordset()))
			$telecare_prescription_grid->TotalRecs = $telecare_prescription_grid->Recordset->RecordCount();
	}
	$telecare_prescription_grid->StartRec = 1;
	$telecare_prescription_grid->DisplayRecs = $telecare_prescription_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$telecare_prescription_grid->Recordset = $telecare_prescription_grid->LoadRecordset($telecare_prescription_grid->StartRec-1, $telecare_prescription_grid->DisplayRecs);

	// Set no record found message
	if ($telecare_prescription->CurrentAction == "" && $telecare_prescription_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$telecare_prescription_grid->setWarningMessage($Language->Phrase("NoPermission"));
		if ($telecare_prescription_grid->SearchWhere == "0=101")
			$telecare_prescription_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$telecare_prescription_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$telecare_prescription_grid->RenderOtherOptions();
?>
<?php $telecare_prescription_grid->ShowPageHeader(); ?>
<?php
$telecare_prescription_grid->ShowMessage();
?>
<?php if ($telecare_prescription_grid->TotalRecs > 0 || $telecare_prescription->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="ftelecare_prescriptiongrid" class="ewForm form-inline">
<?php if ($telecare_prescription_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($telecare_prescription_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_telecare_prescription" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_telecare_prescriptiongrid" class="table ewTable">
<?php echo $telecare_prescription->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$telecare_prescription_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$telecare_prescription_grid->RenderListOptions();

// Render list options (header, left)
$telecare_prescription_grid->ListOptions->Render("header", "left");
?>
<?php if ($telecare_prescription->prescription_id->Visible) { // prescription_id ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_id) == "") { ?>
		<th data-name="prescription_id"><div id="elh_telecare_prescription_prescription_id" class="telecare_prescription_prescription_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_id"><div><div id="elh_telecare_prescription_prescription_id" class="telecare_prescription_prescription_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_user_id->Visible) { // prescription_user_id ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_user_id) == "") { ?>
		<th data-name="prescription_user_id"><div id="elh_telecare_prescription_prescription_user_id" class="telecare_prescription_prescription_user_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_user_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_user_id"><div><div id="elh_telecare_prescription_prescription_user_id" class="telecare_prescription_prescription_user_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_user_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_user_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_user_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_drugstore_id->Visible) { // prescription_drugstore_id ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_drugstore_id) == "") { ?>
		<th data-name="prescription_drugstore_id"><div id="elh_telecare_prescription_prescription_drugstore_id" class="telecare_prescription_prescription_drugstore_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_drugstore_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_drugstore_id"><div><div id="elh_telecare_prescription_prescription_drugstore_id" class="telecare_prescription_prescription_drugstore_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_drugstore_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_drugstore_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_drugstore_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_doctor_id->Visible) { // prescription_doctor_id ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_doctor_id) == "") { ?>
		<th data-name="prescription_doctor_id"><div id="elh_telecare_prescription_prescription_doctor_id" class="telecare_prescription_prescription_doctor_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_doctor_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_doctor_id"><div><div id="elh_telecare_prescription_prescription_doctor_id" class="telecare_prescription_prescription_doctor_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_doctor_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_doctor_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_doctor_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_admin_id->Visible) { // prescription_admin_id ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_admin_id) == "") { ?>
		<th data-name="prescription_admin_id"><div id="elh_telecare_prescription_prescription_admin_id" class="telecare_prescription_prescription_admin_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_admin_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_admin_id"><div><div id="elh_telecare_prescription_prescription_admin_id" class="telecare_prescription_prescription_admin_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_admin_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_admin_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_admin_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_request_date->Visible) { // prescription_request_date ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_request_date) == "") { ?>
		<th data-name="prescription_request_date"><div id="elh_telecare_prescription_prescription_request_date" class="telecare_prescription_prescription_request_date"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_request_date->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_request_date"><div><div id="elh_telecare_prescription_prescription_request_date" class="telecare_prescription_prescription_request_date">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_request_date->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_request_date->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_request_date->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_start_on->Visible) { // prescription_start_on ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_start_on) == "") { ?>
		<th data-name="prescription_start_on"><div id="elh_telecare_prescription_prescription_start_on" class="telecare_prescription_prescription_start_on"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_start_on->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_start_on"><div><div id="elh_telecare_prescription_prescription_start_on" class="telecare_prescription_prescription_start_on">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_start_on->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_start_on->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_start_on->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_close_on->Visible) { // prescription_close_on ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_close_on) == "") { ?>
		<th data-name="prescription_close_on"><div id="elh_telecare_prescription_prescription_close_on" class="telecare_prescription_prescription_close_on"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_close_on->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_close_on"><div><div id="elh_telecare_prescription_prescription_close_on" class="telecare_prescription_prescription_close_on">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_close_on->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_close_on->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_close_on->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription->prescription_close->Visible) { // prescription_close ?>
	<?php if ($telecare_prescription->SortUrl($telecare_prescription->prescription_close) == "") { ?>
		<th data-name="prescription_close"><div id="elh_telecare_prescription_prescription_close" class="telecare_prescription_prescription_close"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_close->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_close"><div><div id="elh_telecare_prescription_prescription_close" class="telecare_prescription_prescription_close">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription->prescription_close->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription->prescription_close->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription->prescription_close->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$telecare_prescription_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$telecare_prescription_grid->StartRec = 1;
$telecare_prescription_grid->StopRec = $telecare_prescription_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($telecare_prescription_grid->FormKeyCountName) && ($telecare_prescription->CurrentAction == "gridadd" || $telecare_prescription->CurrentAction == "gridedit" || $telecare_prescription->CurrentAction == "F")) {
		$telecare_prescription_grid->KeyCount = $objForm->GetValue($telecare_prescription_grid->FormKeyCountName);
		$telecare_prescription_grid->StopRec = $telecare_prescription_grid->StartRec + $telecare_prescription_grid->KeyCount - 1;
	}
}
$telecare_prescription_grid->RecCnt = $telecare_prescription_grid->StartRec - 1;
if ($telecare_prescription_grid->Recordset && !$telecare_prescription_grid->Recordset->EOF) {
	$telecare_prescription_grid->Recordset->MoveFirst();
	$bSelectLimit = $telecare_prescription_grid->UseSelectLimit;
	if (!$bSelectLimit && $telecare_prescription_grid->StartRec > 1)
		$telecare_prescription_grid->Recordset->Move($telecare_prescription_grid->StartRec - 1);
} elseif (!$telecare_prescription->AllowAddDeleteRow && $telecare_prescription_grid->StopRec == 0) {
	$telecare_prescription_grid->StopRec = $telecare_prescription->GridAddRowCount;
}

// Initialize aggregate
$telecare_prescription->RowType = EW_ROWTYPE_AGGREGATEINIT;
$telecare_prescription->ResetAttrs();
$telecare_prescription_grid->RenderRow();
if ($telecare_prescription->CurrentAction == "gridadd")
	$telecare_prescription_grid->RowIndex = 0;
if ($telecare_prescription->CurrentAction == "gridedit")
	$telecare_prescription_grid->RowIndex = 0;
while ($telecare_prescription_grid->RecCnt < $telecare_prescription_grid->StopRec) {
	$telecare_prescription_grid->RecCnt++;
	if (intval($telecare_prescription_grid->RecCnt) >= intval($telecare_prescription_grid->StartRec)) {
		$telecare_prescription_grid->RowCnt++;
		if ($telecare_prescription->CurrentAction == "gridadd" || $telecare_prescription->CurrentAction == "gridedit" || $telecare_prescription->CurrentAction == "F") {
			$telecare_prescription_grid->RowIndex++;
			$objForm->Index = $telecare_prescription_grid->RowIndex;
			if ($objForm->HasValue($telecare_prescription_grid->FormActionName))
				$telecare_prescription_grid->RowAction = strval($objForm->GetValue($telecare_prescription_grid->FormActionName));
			elseif ($telecare_prescription->CurrentAction == "gridadd")
				$telecare_prescription_grid->RowAction = "insert";
			else
				$telecare_prescription_grid->RowAction = "";
		}

		// Set up key count
		$telecare_prescription_grid->KeyCount = $telecare_prescription_grid->RowIndex;

		// Init row class and style
		$telecare_prescription->ResetAttrs();
		$telecare_prescription->CssClass = "";
		if ($telecare_prescription->CurrentAction == "gridadd") {
			if ($telecare_prescription->CurrentMode == "copy") {
				$telecare_prescription_grid->LoadRowValues($telecare_prescription_grid->Recordset); // Load row values
				$telecare_prescription_grid->SetRecordKey($telecare_prescription_grid->RowOldKey, $telecare_prescription_grid->Recordset); // Set old record key
			} else {
				$telecare_prescription_grid->LoadDefaultValues(); // Load default values
				$telecare_prescription_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$telecare_prescription_grid->LoadRowValues($telecare_prescription_grid->Recordset); // Load row values
		}
		$telecare_prescription->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($telecare_prescription->CurrentAction == "gridadd") // Grid add
			$telecare_prescription->RowType = EW_ROWTYPE_ADD; // Render add
		if ($telecare_prescription->CurrentAction == "gridadd" && $telecare_prescription->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$telecare_prescription_grid->RestoreCurrentRowFormValues($telecare_prescription_grid->RowIndex); // Restore form values
		if ($telecare_prescription->CurrentAction == "gridedit") { // Grid edit
			if ($telecare_prescription->EventCancelled) {
				$telecare_prescription_grid->RestoreCurrentRowFormValues($telecare_prescription_grid->RowIndex); // Restore form values
			}
			if ($telecare_prescription_grid->RowAction == "insert")
				$telecare_prescription->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$telecare_prescription->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($telecare_prescription->CurrentAction == "gridedit" && ($telecare_prescription->RowType == EW_ROWTYPE_EDIT || $telecare_prescription->RowType == EW_ROWTYPE_ADD) && $telecare_prescription->EventCancelled) // Update failed
			$telecare_prescription_grid->RestoreCurrentRowFormValues($telecare_prescription_grid->RowIndex); // Restore form values
		if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) // Edit row
			$telecare_prescription_grid->EditRowCnt++;
		if ($telecare_prescription->CurrentAction == "F") // Confirm row
			$telecare_prescription_grid->RestoreCurrentRowFormValues($telecare_prescription_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$telecare_prescription->RowAttrs = array_merge($telecare_prescription->RowAttrs, array('data-rowindex'=>$telecare_prescription_grid->RowCnt, 'id'=>'r' . $telecare_prescription_grid->RowCnt . '_telecare_prescription', 'data-rowtype'=>$telecare_prescription->RowType));

		// Render row
		$telecare_prescription_grid->RenderRow();

		// Render list options
		$telecare_prescription_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($telecare_prescription_grid->RowAction <> "delete" && $telecare_prescription_grid->RowAction <> "insertdelete" && !($telecare_prescription_grid->RowAction == "insert" && $telecare_prescription->CurrentAction == "F" && $telecare_prescription_grid->EmptyRow())) {
?>
	<tr<?php echo $telecare_prescription->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_prescription_grid->ListOptions->Render("body", "left", $telecare_prescription_grid->RowCnt);
?>
	<?php if ($telecare_prescription->prescription_id->Visible) { // prescription_id ?>
		<td data-name="prescription_id"<?php echo $telecare_prescription->prescription_id->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_id" class="form-group telecare_prescription_prescription_id">
<span<?php echo $telecare_prescription->prescription_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_id->CurrentValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_id" class="telecare_prescription_prescription_id">
<span<?php echo $telecare_prescription->prescription_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $telecare_prescription_grid->PageObjName . "_row_" . $telecare_prescription_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_user_id->Visible) { // prescription_user_id ?>
		<td data-name="prescription_user_id"<?php echo $telecare_prescription->prescription_user_id->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($telecare_prescription->prescription_user_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_user_id" class="form-group telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_user_id" class="form-group telecare_prescription_prescription_user_id">
<select data-table="telecare_prescription" data-field="x_prescription_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_user_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_user_id->DisplayValueSeparator) : $telecare_prescription->prescription_user_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id"<?php echo $telecare_prescription->prescription_user_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_user_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_user_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_user_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_user_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_user_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_user_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_user_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_user_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_user_id->LookupFilters += array("f0" => "`user_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `user_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_user_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo $telecare_prescription->prescription_user_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_user_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($telecare_prescription->prescription_user_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_user_id" class="form-group telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_user_id" class="form-group telecare_prescription_prescription_user_id">
<select data-table="telecare_prescription" data-field="x_prescription_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_user_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_user_id->DisplayValueSeparator) : $telecare_prescription->prescription_user_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id"<?php echo $telecare_prescription->prescription_user_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_user_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_user_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_user_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_user_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_user_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_user_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_user_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_user_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_user_id->LookupFilters += array("f0" => "`user_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `user_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_user_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo $telecare_prescription->prescription_user_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_user_id" class="telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_user_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_user_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_drugstore_id->Visible) { // prescription_drugstore_id ?>
		<td data-name="prescription_drugstore_id"<?php echo $telecare_prescription->prescription_drugstore_id->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($telecare_prescription->prescription_drugstore_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_drugstore_id" class="form-group telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_drugstore_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_drugstore_id" class="form-group telecare_prescription_prescription_drugstore_id">
<select data-table="telecare_prescription" data-field="x_prescription_drugstore_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) : $telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id"<?php echo $telecare_prescription->prescription_drugstore_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_drugstore_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_drugstore_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_drugstore_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_drugstore_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_drugstore_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_drugstore_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_drugstore_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_drugstore_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_drugstore_id->LookupFilters += array("f0" => "`apotech_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_drugstore_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo $telecare_prescription->prescription_drugstore_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_drugstore_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($telecare_prescription->prescription_drugstore_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_drugstore_id" class="form-group telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_drugstore_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_drugstore_id" class="form-group telecare_prescription_prescription_drugstore_id">
<select data-table="telecare_prescription" data-field="x_prescription_drugstore_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) : $telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id"<?php echo $telecare_prescription->prescription_drugstore_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_drugstore_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_drugstore_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_drugstore_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_drugstore_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_drugstore_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_drugstore_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_drugstore_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_drugstore_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_drugstore_id->LookupFilters += array("f0" => "`apotech_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_drugstore_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo $telecare_prescription->prescription_drugstore_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_drugstore_id" class="telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_drugstore_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_drugstore_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_doctor_id->Visible) { // prescription_doctor_id ?>
		<td data-name="prescription_doctor_id"<?php echo $telecare_prescription->prescription_doctor_id->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_doctor_id" class="form-group telecare_prescription_prescription_doctor_id">
<select data-table="telecare_prescription" data-field="x_prescription_doctor_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) : $telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id"<?php echo $telecare_prescription->prescription_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_doctor_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_doctor_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_doctor_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_doctor_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_doctor_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = " `admin_level` = 2 ";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_prescription"]->UserIDAllow("grid")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_prescription->prescription_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo $telecare_prescription->prescription_doctor_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_doctor_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_doctor_id" class="form-group telecare_prescription_prescription_doctor_id">
<select data-table="telecare_prescription" data-field="x_prescription_doctor_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) : $telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id"<?php echo $telecare_prescription->prescription_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_doctor_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_doctor_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_doctor_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_doctor_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_doctor_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = " `admin_level` = 2 ";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_prescription"]->UserIDAllow("grid")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_prescription->prescription_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo $telecare_prescription->prescription_doctor_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_doctor_id" class="telecare_prescription_prescription_doctor_id">
<span<?php echo $telecare_prescription->prescription_doctor_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_doctor_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_doctor_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_doctor_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_admin_id->Visible) { // prescription_admin_id ?>
		<td data-name="prescription_admin_id"<?php echo $telecare_prescription->prescription_admin_id->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_admin_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_admin_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_admin_id" class="telecare_prescription_prescription_admin_id">
<span<?php echo $telecare_prescription->prescription_admin_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_admin_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_admin_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_admin_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_admin_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_admin_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_request_date->Visible) { // prescription_request_date ?>
		<td data-name="prescription_request_date"<?php echo $telecare_prescription->prescription_request_date->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_request_date" class="form-group telecare_prescription_prescription_request_date">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_request_date" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_request_date->EditValue ?>"<?php echo $telecare_prescription->prescription_request_date->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_request_date->ReadOnly && !$telecare_prescription->prescription_request_date->Disabled && !isset($telecare_prescription->prescription_request_date->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_request_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_request_date" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_request_date" class="form-group telecare_prescription_prescription_request_date">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_request_date" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_request_date->EditValue ?>"<?php echo $telecare_prescription->prescription_request_date->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_request_date->ReadOnly && !$telecare_prescription->prescription_request_date->Disabled && !isset($telecare_prescription->prescription_request_date->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_request_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_request_date" class="telecare_prescription_prescription_request_date">
<span<?php echo $telecare_prescription->prescription_request_date->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_request_date->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_request_date" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_request_date" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_start_on->Visible) { // prescription_start_on ?>
		<td data-name="prescription_start_on"<?php echo $telecare_prescription->prescription_start_on->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_start_on" class="form-group telecare_prescription_prescription_start_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_start_on" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_start_on->EditValue ?>"<?php echo $telecare_prescription->prescription_start_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_start_on->ReadOnly && !$telecare_prescription->prescription_start_on->Disabled && !isset($telecare_prescription->prescription_start_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_start_on" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_start_on" class="form-group telecare_prescription_prescription_start_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_start_on" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_start_on->EditValue ?>"<?php echo $telecare_prescription->prescription_start_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_start_on->ReadOnly && !$telecare_prescription->prescription_start_on->Disabled && !isset($telecare_prescription->prescription_start_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_start_on" class="telecare_prescription_prescription_start_on">
<span<?php echo $telecare_prescription->prescription_start_on->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_start_on->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_start_on" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_start_on" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_close_on->Visible) { // prescription_close_on ?>
		<td data-name="prescription_close_on"<?php echo $telecare_prescription->prescription_close_on->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_close_on" class="form-group telecare_prescription_prescription_close_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_close_on" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_close_on->EditValue ?>"<?php echo $telecare_prescription->prescription_close_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_close_on->ReadOnly && !$telecare_prescription->prescription_close_on->Disabled && !isset($telecare_prescription->prescription_close_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close_on" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_close_on" class="form-group telecare_prescription_prescription_close_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_close_on" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_close_on->EditValue ?>"<?php echo $telecare_prescription->prescription_close_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_close_on->ReadOnly && !$telecare_prescription->prescription_close_on->Disabled && !isset($telecare_prescription->prescription_close_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_close_on" class="telecare_prescription_prescription_close_on">
<span<?php echo $telecare_prescription->prescription_close_on->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_close_on->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close_on" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close_on" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_close->Visible) { // prescription_close ?>
		<td data-name="prescription_close"<?php echo $telecare_prescription->prescription_close->CellAttributes() ?>>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_close" class="form-group telecare_prescription_prescription_close">
<div id="tp_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" class="ewTemplate"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_close->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_close->DisplayValueSeparator) : $telecare_prescription->prescription_close->DisplayValueSeparator) ?>" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="{value}"<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_prescription->prescription_close->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_prescription->prescription_close->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_close->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->CurrentValue) ?>" checked<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_close->OldValue = "";
?>
</div></div>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_close" class="form-group telecare_prescription_prescription_close">
<div id="tp_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" class="ewTemplate"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_close->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_close->DisplayValueSeparator) : $telecare_prescription->prescription_close->DisplayValueSeparator) ?>" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="{value}"<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_prescription->prescription_close->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_prescription->prescription_close->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_close->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->CurrentValue) ?>" checked<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_close->OldValue = "";
?>
</div></div>
</span>
<?php } ?>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_grid->RowCnt ?>_telecare_prescription_prescription_close" class="telecare_prescription_prescription_close">
<span<?php echo $telecare_prescription->prescription_close->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_close->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_prescription_grid->ListOptions->Render("body", "right", $telecare_prescription_grid->RowCnt);
?>
	</tr>
<?php if ($telecare_prescription->RowType == EW_ROWTYPE_ADD || $telecare_prescription->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
ftelecare_prescriptiongrid.UpdateOpts(<?php echo $telecare_prescription_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($telecare_prescription->CurrentAction <> "gridadd" || $telecare_prescription->CurrentMode == "copy")
		if (!$telecare_prescription_grid->Recordset->EOF) $telecare_prescription_grid->Recordset->MoveNext();
}
?>
<?php
	if ($telecare_prescription->CurrentMode == "add" || $telecare_prescription->CurrentMode == "copy" || $telecare_prescription->CurrentMode == "edit") {
		$telecare_prescription_grid->RowIndex = '$rowindex$';
		$telecare_prescription_grid->LoadDefaultValues();

		// Set row properties
		$telecare_prescription->ResetAttrs();
		$telecare_prescription->RowAttrs = array_merge($telecare_prescription->RowAttrs, array('data-rowindex'=>$telecare_prescription_grid->RowIndex, 'id'=>'r0_telecare_prescription', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($telecare_prescription->RowAttrs["class"], "ewTemplate");
		$telecare_prescription->RowType = EW_ROWTYPE_ADD;

		// Render row
		$telecare_prescription_grid->RenderRow();

		// Render list options
		$telecare_prescription_grid->RenderListOptions();
		$telecare_prescription_grid->StartRowCnt = 0;
?>
	<tr<?php echo $telecare_prescription->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_prescription_grid->ListOptions->Render("body", "left", $telecare_prescription_grid->RowIndex);
?>
	<?php if ($telecare_prescription->prescription_id->Visible) { // prescription_id ?>
		<td data-name="prescription_id">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_id" class="form-group telecare_prescription_prescription_id">
<span<?php echo $telecare_prescription->prescription_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_user_id->Visible) { // prescription_user_id ?>
		<td data-name="prescription_user_id">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<?php if ($telecare_prescription->prescription_user_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_telecare_prescription_prescription_user_id" class="form-group telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_user_id" class="form-group telecare_prescription_prescription_user_id">
<select data-table="telecare_prescription" data-field="x_prescription_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_user_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_user_id->DisplayValueSeparator) : $telecare_prescription->prescription_user_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id"<?php echo $telecare_prescription->prescription_user_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_user_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_user_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_user_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_user_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_user_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_user_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_user_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_user_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_user_id->LookupFilters += array("f0" => "`user_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `user_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_user_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo $telecare_prescription->prescription_user_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_user_id" class="form-group telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_user_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_user_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_user_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_user_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_drugstore_id->Visible) { // prescription_drugstore_id ?>
		<td data-name="prescription_drugstore_id">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<?php if ($telecare_prescription->prescription_drugstore_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_telecare_prescription_prescription_drugstore_id" class="form-group telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_drugstore_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_drugstore_id" class="form-group telecare_prescription_prescription_drugstore_id">
<select data-table="telecare_prescription" data-field="x_prescription_drugstore_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) : $telecare_prescription->prescription_drugstore_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id"<?php echo $telecare_prescription->prescription_drugstore_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_drugstore_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_drugstore_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_drugstore_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_drugstore_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_drugstore_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_drugstore_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_drugstore_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription->prescription_drugstore_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_drugstore_id->LookupFilters += array("f0" => "`apotech_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_drugstore_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo $telecare_prescription->prescription_drugstore_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_drugstore_id" class="form-group telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_drugstore_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_drugstore_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_drugstore_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_drugstore_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_drugstore_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_doctor_id->Visible) { // prescription_doctor_id ?>
		<td data-name="prescription_doctor_id">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_prescription_prescription_doctor_id" class="form-group telecare_prescription_prescription_doctor_id">
<select data-table="telecare_prescription" data-field="x_prescription_doctor_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_doctor_id->DisplayValueSeparator) : $telecare_prescription->prescription_doctor_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id"<?php echo $telecare_prescription->prescription_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription->prescription_doctor_id->EditValue)) {
	$arwrk = $telecare_prescription->prescription_doctor_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription->prescription_doctor_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription->prescription_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_prescription->prescription_doctor_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_doctor_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = " `admin_level` = 2 ";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_prescription"]->UserIDAllow("grid")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_prescription->prescription_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription->prescription_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription->Lookup_Selecting($telecare_prescription->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_prescription->prescription_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="s_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo $telecare_prescription->prescription_doctor_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_doctor_id" class="form-group telecare_prescription_prescription_doctor_id">
<span<?php echo $telecare_prescription->prescription_doctor_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_doctor_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_doctor_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_doctor_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_doctor_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_doctor_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_admin_id->Visible) { // prescription_admin_id ?>
		<td data-name="prescription_admin_id">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_admin_id" class="form-group telecare_prescription_prescription_admin_id">
<span<?php echo $telecare_prescription->prescription_admin_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_admin_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_admin_id" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_admin_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_admin_id" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_admin_id" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_admin_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_request_date->Visible) { // prescription_request_date ?>
		<td data-name="prescription_request_date">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_prescription_prescription_request_date" class="form-group telecare_prescription_prescription_request_date">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_request_date" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_request_date->EditValue ?>"<?php echo $telecare_prescription->prescription_request_date->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_request_date->ReadOnly && !$telecare_prescription->prescription_request_date->Disabled && !isset($telecare_prescription->prescription_request_date->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_request_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_request_date" class="form-group telecare_prescription_prescription_request_date">
<span<?php echo $telecare_prescription->prescription_request_date->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_request_date->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_request_date" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_request_date" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_request_date" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_request_date->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_start_on->Visible) { // prescription_start_on ?>
		<td data-name="prescription_start_on">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_prescription_prescription_start_on" class="form-group telecare_prescription_prescription_start_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_start_on" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_start_on->EditValue ?>"<?php echo $telecare_prescription->prescription_start_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_start_on->ReadOnly && !$telecare_prescription->prescription_start_on->Disabled && !isset($telecare_prescription->prescription_start_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_start_on" class="form-group telecare_prescription_prescription_start_on">
<span<?php echo $telecare_prescription->prescription_start_on->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_start_on->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_start_on" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_start_on" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_start_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_start_on->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_close_on->Visible) { // prescription_close_on ?>
		<td data-name="prescription_close_on">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_prescription_prescription_close_on" class="form-group telecare_prescription_prescription_close_on">
<input type="text" data-table="telecare_prescription" data-field="x_prescription_close_on" data-format="7" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription->prescription_close_on->EditValue ?>"<?php echo $telecare_prescription->prescription_close_on->EditAttributes() ?>>
<?php if (!$telecare_prescription->prescription_close_on->ReadOnly && !$telecare_prescription->prescription_close_on->Disabled && !isset($telecare_prescription->prescription_close_on->EditAttrs["readonly"]) && !isset($telecare_prescription->prescription_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_prescriptiongrid", "x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_close_on" class="form-group telecare_prescription_prescription_close_on">
<span<?php echo $telecare_prescription->prescription_close_on->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_close_on->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close_on" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close_on" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_on" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close_on->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription->prescription_close->Visible) { // prescription_close ?>
		<td data-name="prescription_close">
<?php if ($telecare_prescription->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_prescription_prescription_close" class="form-group telecare_prescription_prescription_close">
<div id="tp_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" class="ewTemplate"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription->prescription_close->DisplayValueSeparator) ? json_encode($telecare_prescription->prescription_close->DisplayValueSeparator) : $telecare_prescription->prescription_close->DisplayValueSeparator) ?>" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="{value}"<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_prescription->prescription_close->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_prescription->prescription_close->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_prescription->prescription_close->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->CurrentValue) ?>" checked<?php echo $telecare_prescription->prescription_close->EditAttributes() ?>><?php echo $telecare_prescription->prescription_close->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_prescription->prescription_close->OldValue = "";
?>
</div></div>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_prescription_close" class="form-group telecare_prescription_prescription_close">
<span<?php echo $telecare_prescription->prescription_close->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription->prescription_close->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close" name="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="x<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription" data-field="x_prescription_close" name="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" id="o<?php echo $telecare_prescription_grid->RowIndex ?>_prescription_close" value="<?php echo ew_HtmlEncode($telecare_prescription->prescription_close->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_prescription_grid->ListOptions->Render("body", "right", $telecare_prescription_grid->RowCnt);
?>
<script type="text/javascript">
ftelecare_prescriptiongrid.UpdateOpts(<?php echo $telecare_prescription_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($telecare_prescription->CurrentMode == "add" || $telecare_prescription->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $telecare_prescription_grid->FormKeyCountName ?>" id="<?php echo $telecare_prescription_grid->FormKeyCountName ?>" value="<?php echo $telecare_prescription_grid->KeyCount ?>">
<?php echo $telecare_prescription_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_prescription->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $telecare_prescription_grid->FormKeyCountName ?>" id="<?php echo $telecare_prescription_grid->FormKeyCountName ?>" value="<?php echo $telecare_prescription_grid->KeyCount ?>">
<?php echo $telecare_prescription_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_prescription->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="ftelecare_prescriptiongrid">
</div>
<?php

// Close recordset
if ($telecare_prescription_grid->Recordset)
	$telecare_prescription_grid->Recordset->Close();
?>
<?php if ($telecare_prescription_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($telecare_prescription_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($telecare_prescription_grid->TotalRecs == 0 && $telecare_prescription->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_prescription_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($telecare_prescription->Export == "") { ?>
<script type="text/javascript">
ftelecare_prescriptiongrid.Init();
</script>
<?php } ?>
<?php
$telecare_prescription_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$telecare_prescription_grid->Page_Terminate();
?>
