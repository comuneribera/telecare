<?php

// Global variable for table object
$telecare_prescription = NULL;

//
// Table class for telecare_prescription
//
class ctelecare_prescription extends cTable {
	var $prescription_id;
	var $prescription_user_id;
	var $prescription_drugstore_id;
	var $prescription_doctor_id;
	var $prescription_admin_id;
	var $prescription_request_date;
	var $prescription_start_on;
	var $prescription_close_on;
	var $prescription_close;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'telecare_prescription';
		$this->TableName = 'telecare_prescription';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`telecare_prescription`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = PHPExcel_Worksheet_PageSetup::ORIENTATION_DEFAULT; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// prescription_id
		$this->prescription_id = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_id', 'prescription_id', '`prescription_id`', '`prescription_id`', 3, -1, FALSE, '`prescription_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->prescription_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['prescription_id'] = &$this->prescription_id;

		// prescription_user_id
		$this->prescription_user_id = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_user_id', 'prescription_user_id', '`prescription_user_id`', '`prescription_user_id`', 3, -1, FALSE, '`prescription_user_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->prescription_user_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['prescription_user_id'] = &$this->prescription_user_id;

		// prescription_drugstore_id
		$this->prescription_drugstore_id = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_drugstore_id', 'prescription_drugstore_id', '`prescription_drugstore_id`', '`prescription_drugstore_id`', 3, -1, FALSE, '`prescription_drugstore_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->prescription_drugstore_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['prescription_drugstore_id'] = &$this->prescription_drugstore_id;

		// prescription_doctor_id
		$this->prescription_doctor_id = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_doctor_id', 'prescription_doctor_id', '`prescription_doctor_id`', '`prescription_doctor_id`', 3, -1, FALSE, '`prescription_doctor_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->prescription_doctor_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['prescription_doctor_id'] = &$this->prescription_doctor_id;

		// prescription_admin_id
		$this->prescription_admin_id = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_admin_id', 'prescription_admin_id', '`prescription_admin_id`', '`prescription_admin_id`', 3, -1, FALSE, '`prescription_admin_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->prescription_admin_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['prescription_admin_id'] = &$this->prescription_admin_id;

		// prescription_request_date
		$this->prescription_request_date = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_request_date', 'prescription_request_date', '`prescription_request_date`', 'DATE_FORMAT(`prescription_request_date`, \'%d/%m/%Y\')', 135, 7, FALSE, '`prescription_request_date`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->prescription_request_date->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['prescription_request_date'] = &$this->prescription_request_date;

		// prescription_start_on
		$this->prescription_start_on = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_start_on', 'prescription_start_on', '`prescription_start_on`', 'DATE_FORMAT(`prescription_start_on`, \'%d/%m/%Y\')', 135, 7, FALSE, '`prescription_start_on`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->prescription_start_on->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['prescription_start_on'] = &$this->prescription_start_on;

		// prescription_close_on
		$this->prescription_close_on = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_close_on', 'prescription_close_on', '`prescription_close_on`', 'DATE_FORMAT(`prescription_close_on`, \'%d/%m/%Y\')', 135, 7, FALSE, '`prescription_close_on`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->prescription_close_on->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['prescription_close_on'] = &$this->prescription_close_on;

		// prescription_close
		$this->prescription_close = new cField('telecare_prescription', 'telecare_prescription', 'x_prescription_close', 'prescription_close', '`prescription_close`', '`prescription_close`', 16, -1, FALSE, '`prescription_close`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->prescription_close->OptionCount = 3;
		$this->prescription_close->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['prescription_close'] = &$this->prescription_close;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Current master table name
	function getCurrentMasterTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE];
	}

	function setCurrentMasterTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE] = $v;
	}

	// Session master WHERE clause
	function GetMasterFilter() {

		// Master filter
		$sMasterFilter = "";
		if ($this->getCurrentMasterTable() == "telecare_user") {
			if ($this->prescription_user_id->getSessionValue() <> "")
				$sMasterFilter .= "`user_id`=" . ew_QuotedValue($this->prescription_user_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "telecare_apotech") {
			if ($this->prescription_drugstore_id->getSessionValue() <> "")
				$sMasterFilter .= "`apotech_id`=" . ew_QuotedValue($this->prescription_drugstore_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sMasterFilter;
	}

	// Session detail WHERE clause
	function GetDetailFilter() {

		// Detail filter
		$sDetailFilter = "";
		if ($this->getCurrentMasterTable() == "telecare_user") {
			if ($this->prescription_user_id->getSessionValue() <> "")
				$sDetailFilter .= "`prescription_user_id`=" . ew_QuotedValue($this->prescription_user_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		if ($this->getCurrentMasterTable() == "telecare_apotech") {
			if ($this->prescription_drugstore_id->getSessionValue() <> "")
				$sDetailFilter .= "`prescription_drugstore_id`=" . ew_QuotedValue($this->prescription_drugstore_id->getSessionValue(), EW_DATATYPE_NUMBER, "DB");
			else
				return "";
		}
		return $sDetailFilter;
	}

	// Master filter
	function SqlMasterFilter_telecare_user() {
		return "`user_id`=@user_id@";
	}

	// Detail filter
	function SqlDetailFilter_telecare_user() {
		return "`prescription_user_id`=@prescription_user_id@";
	}

	// Master filter
	function SqlMasterFilter_telecare_apotech() {
		return "`apotech_id`=@apotech_id@";
	}

	// Detail filter
	function SqlDetailFilter_telecare_apotech() {
		return "`prescription_drugstore_id`=@prescription_drugstore_id@";
	}

	// Current detail table name
	function getCurrentDetailTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE];
	}

	function setCurrentDetailTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE] = $v;
	}

	// Get detail url
	function GetDetailUrl() {

		// Detail url
		$sDetailUrl = "";
		if ($this->getCurrentDetailTable() == "telecare_prescription_drug") {
			$sDetailUrl = $GLOBALS["telecare_prescription_drug"]->GetListUrl() . "?" . EW_TABLE_SHOW_MASTER . "=" . $this->TableVar;
			$sDetailUrl .= "&fk_prescription_id=" . urlencode($this->prescription_id->CurrentValue);
		}
		if ($sDetailUrl == "") {
			$sDetailUrl = "telecare_prescriptionlist.php";
		}
		return $sDetailUrl;
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`telecare_prescription`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('prescription_id', $rs))
				ew_AddFilter($where, ew_QuotedName('prescription_id', $this->DBID) . '=' . ew_QuotedValue($rs['prescription_id'], $this->prescription_id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`prescription_id` = @prescription_id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->prescription_id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@prescription_id@", ew_AdjustSql($this->prescription_id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "telecare_prescriptionlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "telecare_prescriptionlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_prescriptionview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_prescriptionview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "telecare_prescriptionadd.php?" . $this->UrlParm($parm);
		else
			$url = "telecare_prescriptionadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_prescriptionedit.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_prescriptionedit.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_prescriptionadd.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_prescriptionadd.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("telecare_prescriptiondelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		if ($this->getCurrentMasterTable() == "telecare_user" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_user_id=" . urlencode($this->prescription_user_id->CurrentValue);
		}
		if ($this->getCurrentMasterTable() == "telecare_apotech" && strpos($url, EW_TABLE_SHOW_MASTER . "=") === FALSE) {
			$url .= (strpos($url, "?") !== FALSE ? "&" : "?") . EW_TABLE_SHOW_MASTER . "=" . $this->getCurrentMasterTable();
			$url .= "&fk_apotech_id=" . urlencode($this->prescription_drugstore_id->CurrentValue);
		}
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "prescription_id:" . ew_VarToJson($this->prescription_id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->prescription_id->CurrentValue)) {
			$sUrl .= "prescription_id=" . urlencode($this->prescription_id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["prescription_id"]) : ew_StripSlashes(@$_GET["prescription_id"]); // prescription_id

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->prescription_id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->prescription_id->setDbValue($rs->fields('prescription_id'));
		$this->prescription_user_id->setDbValue($rs->fields('prescription_user_id'));
		$this->prescription_drugstore_id->setDbValue($rs->fields('prescription_drugstore_id'));
		$this->prescription_doctor_id->setDbValue($rs->fields('prescription_doctor_id'));
		$this->prescription_admin_id->setDbValue($rs->fields('prescription_admin_id'));
		$this->prescription_request_date->setDbValue($rs->fields('prescription_request_date'));
		$this->prescription_start_on->setDbValue($rs->fields('prescription_start_on'));
		$this->prescription_close_on->setDbValue($rs->fields('prescription_close_on'));
		$this->prescription_close->setDbValue($rs->fields('prescription_close'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// prescription_id
		// prescription_user_id
		// prescription_drugstore_id
		// prescription_doctor_id
		// prescription_admin_id
		// prescription_request_date
		// prescription_start_on
		// prescription_close_on
		// prescription_close
		// prescription_id

		$this->prescription_id->ViewValue = $this->prescription_id->CurrentValue;
		$this->prescription_id->ViewCustomAttributes = "";

		// prescription_user_id
		if (strval($this->prescription_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->prescription_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->CurrentValue;
			}
		} else {
			$this->prescription_user_id->ViewValue = NULL;
		}
		$this->prescription_user_id->ViewCustomAttributes = "";

		// prescription_drugstore_id
		if (strval($this->prescription_drugstore_id->CurrentValue) <> "") {
			$sFilterWrk = "`apotech_id`" . ew_SearchString("=", $this->prescription_drugstore_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->CurrentValue;
			}
		} else {
			$this->prescription_drugstore_id->ViewValue = NULL;
		}
		$this->prescription_drugstore_id->ViewCustomAttributes = "";

		// prescription_doctor_id
		if (strval($this->prescription_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->prescription_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = " `admin_level` = 2 ";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->prescription_doctor_id->ViewValue = $this->prescription_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_doctor_id->ViewValue = $this->prescription_doctor_id->CurrentValue;
			}
		} else {
			$this->prescription_doctor_id->ViewValue = NULL;
		}
		$this->prescription_doctor_id->ViewCustomAttributes = "";

		// prescription_admin_id
		if (strval($this->prescription_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->prescription_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->prescription_admin_id->ViewValue = $this->prescription_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_admin_id->ViewValue = $this->prescription_admin_id->CurrentValue;
			}
		} else {
			$this->prescription_admin_id->ViewValue = NULL;
		}
		$this->prescription_admin_id->ViewCustomAttributes = "";

		// prescription_request_date
		$this->prescription_request_date->ViewValue = $this->prescription_request_date->CurrentValue;
		$this->prescription_request_date->ViewValue = ew_FormatDateTime($this->prescription_request_date->ViewValue, 7);
		$this->prescription_request_date->ViewCustomAttributes = "";

		// prescription_start_on
		$this->prescription_start_on->ViewValue = $this->prescription_start_on->CurrentValue;
		$this->prescription_start_on->ViewValue = ew_FormatDateTime($this->prescription_start_on->ViewValue, 7);
		$this->prescription_start_on->ViewCustomAttributes = "";

		// prescription_close_on
		$this->prescription_close_on->ViewValue = $this->prescription_close_on->CurrentValue;
		$this->prescription_close_on->ViewValue = ew_FormatDateTime($this->prescription_close_on->ViewValue, 7);
		$this->prescription_close_on->ViewCustomAttributes = "";

		// prescription_close
		if (strval($this->prescription_close->CurrentValue) <> "") {
			$this->prescription_close->ViewValue = $this->prescription_close->OptionCaption($this->prescription_close->CurrentValue);
		} else {
			$this->prescription_close->ViewValue = NULL;
		}
		$this->prescription_close->ViewCustomAttributes = "";

		// prescription_id
		$this->prescription_id->LinkCustomAttributes = "";
		$this->prescription_id->HrefValue = "";
		$this->prescription_id->TooltipValue = "";

		// prescription_user_id
		$this->prescription_user_id->LinkCustomAttributes = "";
		$this->prescription_user_id->HrefValue = "";
		$this->prescription_user_id->TooltipValue = "";

		// prescription_drugstore_id
		$this->prescription_drugstore_id->LinkCustomAttributes = "";
		$this->prescription_drugstore_id->HrefValue = "";
		$this->prescription_drugstore_id->TooltipValue = "";

		// prescription_doctor_id
		$this->prescription_doctor_id->LinkCustomAttributes = "";
		$this->prescription_doctor_id->HrefValue = "";
		$this->prescription_doctor_id->TooltipValue = "";

		// prescription_admin_id
		$this->prescription_admin_id->LinkCustomAttributes = "";
		$this->prescription_admin_id->HrefValue = "";
		$this->prescription_admin_id->TooltipValue = "";

		// prescription_request_date
		$this->prescription_request_date->LinkCustomAttributes = "";
		$this->prescription_request_date->HrefValue = "";
		$this->prescription_request_date->TooltipValue = "";

		// prescription_start_on
		$this->prescription_start_on->LinkCustomAttributes = "";
		$this->prescription_start_on->HrefValue = "";
		$this->prescription_start_on->TooltipValue = "";

		// prescription_close_on
		$this->prescription_close_on->LinkCustomAttributes = "";
		$this->prescription_close_on->HrefValue = "";
		$this->prescription_close_on->TooltipValue = "";

		// prescription_close
		$this->prescription_close->LinkCustomAttributes = "";
		$this->prescription_close->HrefValue = "";
		$this->prescription_close->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// prescription_id
		$this->prescription_id->EditAttrs["class"] = "form-control";
		$this->prescription_id->EditCustomAttributes = "";
		$this->prescription_id->EditValue = $this->prescription_id->CurrentValue;
		$this->prescription_id->ViewCustomAttributes = "";

		// prescription_user_id
		$this->prescription_user_id->EditAttrs["class"] = "form-control";
		$this->prescription_user_id->EditCustomAttributes = "";
		if ($this->prescription_user_id->getSessionValue() <> "") {
			$this->prescription_user_id->CurrentValue = $this->prescription_user_id->getSessionValue();
		if (strval($this->prescription_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->prescription_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_user_id->ViewValue = $this->prescription_user_id->CurrentValue;
			}
		} else {
			$this->prescription_user_id->ViewValue = NULL;
		}
		$this->prescription_user_id->ViewCustomAttributes = "";
		} else {
		}

		// prescription_drugstore_id
		$this->prescription_drugstore_id->EditAttrs["class"] = "form-control";
		$this->prescription_drugstore_id->EditCustomAttributes = "";
		if ($this->prescription_drugstore_id->getSessionValue() <> "") {
			$this->prescription_drugstore_id->CurrentValue = $this->prescription_drugstore_id->getSessionValue();
		if (strval($this->prescription_drugstore_id->CurrentValue) <> "") {
			$sFilterWrk = "`apotech_id`" . ew_SearchString("=", $this->prescription_drugstore_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `apotech_id`, `apotech_company_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_apotech`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->prescription_drugstore_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `apotech_company_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->prescription_drugstore_id->ViewValue = $this->prescription_drugstore_id->CurrentValue;
			}
		} else {
			$this->prescription_drugstore_id->ViewValue = NULL;
		}
		$this->prescription_drugstore_id->ViewCustomAttributes = "";
		} else {
		}

		// prescription_doctor_id
		$this->prescription_doctor_id->EditAttrs["class"] = "form-control";
		$this->prescription_doctor_id->EditCustomAttributes = "";

		// prescription_admin_id
		// prescription_request_date

		$this->prescription_request_date->EditAttrs["class"] = "form-control";
		$this->prescription_request_date->EditCustomAttributes = "";
		$this->prescription_request_date->EditValue = ew_FormatDateTime($this->prescription_request_date->CurrentValue, 7);
		$this->prescription_request_date->PlaceHolder = ew_RemoveHtml($this->prescription_request_date->FldCaption());

		// prescription_start_on
		$this->prescription_start_on->EditAttrs["class"] = "form-control";
		$this->prescription_start_on->EditCustomAttributes = "";
		$this->prescription_start_on->EditValue = ew_FormatDateTime($this->prescription_start_on->CurrentValue, 7);
		$this->prescription_start_on->PlaceHolder = ew_RemoveHtml($this->prescription_start_on->FldCaption());

		// prescription_close_on
		$this->prescription_close_on->EditAttrs["class"] = "form-control";
		$this->prescription_close_on->EditCustomAttributes = "";
		$this->prescription_close_on->EditValue = ew_FormatDateTime($this->prescription_close_on->CurrentValue, 7);
		$this->prescription_close_on->PlaceHolder = ew_RemoveHtml($this->prescription_close_on->FldCaption());

		// prescription_close
		$this->prescription_close->EditCustomAttributes = "";
		$this->prescription_close->EditValue = $this->prescription_close->Options(FALSE);

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->prescription_id->Exportable) $Doc->ExportCaption($this->prescription_id);
					if ($this->prescription_user_id->Exportable) $Doc->ExportCaption($this->prescription_user_id);
					if ($this->prescription_drugstore_id->Exportable) $Doc->ExportCaption($this->prescription_drugstore_id);
					if ($this->prescription_doctor_id->Exportable) $Doc->ExportCaption($this->prescription_doctor_id);
					if ($this->prescription_admin_id->Exportable) $Doc->ExportCaption($this->prescription_admin_id);
					if ($this->prescription_request_date->Exportable) $Doc->ExportCaption($this->prescription_request_date);
					if ($this->prescription_start_on->Exportable) $Doc->ExportCaption($this->prescription_start_on);
					if ($this->prescription_close_on->Exportable) $Doc->ExportCaption($this->prescription_close_on);
					if ($this->prescription_close->Exportable) $Doc->ExportCaption($this->prescription_close);
				} else {
					if ($this->prescription_id->Exportable) $Doc->ExportCaption($this->prescription_id);
					if ($this->prescription_user_id->Exportable) $Doc->ExportCaption($this->prescription_user_id);
					if ($this->prescription_drugstore_id->Exportable) $Doc->ExportCaption($this->prescription_drugstore_id);
					if ($this->prescription_doctor_id->Exportable) $Doc->ExportCaption($this->prescription_doctor_id);
					if ($this->prescription_admin_id->Exportable) $Doc->ExportCaption($this->prescription_admin_id);
					if ($this->prescription_request_date->Exportable) $Doc->ExportCaption($this->prescription_request_date);
					if ($this->prescription_start_on->Exportable) $Doc->ExportCaption($this->prescription_start_on);
					if ($this->prescription_close_on->Exportable) $Doc->ExportCaption($this->prescription_close_on);
					if ($this->prescription_close->Exportable) $Doc->ExportCaption($this->prescription_close);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->prescription_id->Exportable) $Doc->ExportField($this->prescription_id);
						if ($this->prescription_user_id->Exportable) $Doc->ExportField($this->prescription_user_id);
						if ($this->prescription_drugstore_id->Exportable) $Doc->ExportField($this->prescription_drugstore_id);
						if ($this->prescription_doctor_id->Exportable) $Doc->ExportField($this->prescription_doctor_id);
						if ($this->prescription_admin_id->Exportable) $Doc->ExportField($this->prescription_admin_id);
						if ($this->prescription_request_date->Exportable) $Doc->ExportField($this->prescription_request_date);
						if ($this->prescription_start_on->Exportable) $Doc->ExportField($this->prescription_start_on);
						if ($this->prescription_close_on->Exportable) $Doc->ExportField($this->prescription_close_on);
						if ($this->prescription_close->Exportable) $Doc->ExportField($this->prescription_close);
					} else {
						if ($this->prescription_id->Exportable) $Doc->ExportField($this->prescription_id);
						if ($this->prescription_user_id->Exportable) $Doc->ExportField($this->prescription_user_id);
						if ($this->prescription_drugstore_id->Exportable) $Doc->ExportField($this->prescription_drugstore_id);
						if ($this->prescription_doctor_id->Exportable) $Doc->ExportField($this->prescription_doctor_id);
						if ($this->prescription_admin_id->Exportable) $Doc->ExportField($this->prescription_admin_id);
						if ($this->prescription_request_date->Exportable) $Doc->ExportField($this->prescription_request_date);
						if ($this->prescription_start_on->Exportable) $Doc->ExportField($this->prescription_start_on);
						if ($this->prescription_close_on->Exportable) $Doc->ExportField($this->prescription_close_on);
						if ($this->prescription_close->Exportable) $Doc->ExportField($this->prescription_close);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
