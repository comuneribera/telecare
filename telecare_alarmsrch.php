<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_alarminfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_alarm_search = NULL; // Initialize page object first

class ctelecare_alarm_search extends ctelecare_alarm {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_alarm';

	// Page object name
	var $PageObjName = 'telecare_alarm_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_alarm)
		if (!isset($GLOBALS["telecare_alarm"]) || get_class($GLOBALS["telecare_alarm"]) == "ctelecare_alarm") {
			$GLOBALS["telecare_alarm"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_alarm"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_user)
		if (!isset($GLOBALS['telecare_user'])) $GLOBALS['telecare_user'] = new ctelecare_user();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_alarm', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_alarmlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->alarm_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_alarm;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_alarm);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "telecare_alarmlist.php" . "?" . $sSrchStr;
						if ($this->IsModal) {
							$row = array();
							$row["url"] = $sSrchStr;
							echo ew_ArrayToJson(array($row));
							$this->Page_Terminate();
							exit();
						} else {
							$this->Page_Terminate($sSrchStr); // Go to list page
						}
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_id); // alarm_id
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_type_id); // alarm_type_id
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_user_id); // alarm_user_id
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_product_id); // alarm_product_id
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_datetime); // alarm_datetime
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_latitude); // alarm_latitude
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_longitude); // alarm_longitude
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_start_on); // alarm_start_on
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_close_on); // alarm_close_on
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_close); // alarm_close
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_admin_id); // alarm_admin_id
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_call_parents); // alarm_call_parents
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_call_emergency); // alarm_call_emergency
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_note); // alarm_note
		$this->BuildSearchUrl($sSrchUrl, $this->alarm_last_update); // alarm_last_update
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// alarm_id

		$this->alarm_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_id"));
		$this->alarm_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_id");

		// alarm_type_id
		$this->alarm_type_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_type_id"));
		$this->alarm_type_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_type_id");

		// alarm_user_id
		$this->alarm_user_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_user_id"));
		$this->alarm_user_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_user_id");

		// alarm_product_id
		$this->alarm_product_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_product_id"));
		$this->alarm_product_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_product_id");

		// alarm_datetime
		$this->alarm_datetime->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_datetime"));
		$this->alarm_datetime->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_datetime");

		// alarm_latitude
		$this->alarm_latitude->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_latitude"));
		$this->alarm_latitude->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_latitude");

		// alarm_longitude
		$this->alarm_longitude->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_longitude"));
		$this->alarm_longitude->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_longitude");

		// alarm_start_on
		$this->alarm_start_on->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_start_on"));
		$this->alarm_start_on->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_start_on");

		// alarm_close_on
		$this->alarm_close_on->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_close_on"));
		$this->alarm_close_on->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_close_on");

		// alarm_close
		$this->alarm_close->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_close"));
		$this->alarm_close->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_close");

		// alarm_admin_id
		$this->alarm_admin_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_admin_id"));
		$this->alarm_admin_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_admin_id");

		// alarm_call_parents
		$this->alarm_call_parents->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_call_parents"));
		$this->alarm_call_parents->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_call_parents");

		// alarm_call_emergency
		$this->alarm_call_emergency->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_call_emergency"));
		$this->alarm_call_emergency->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_call_emergency");

		// alarm_note
		$this->alarm_note->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_note"));
		$this->alarm_note->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_note");

		// alarm_last_update
		$this->alarm_last_update->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_alarm_last_update"));
		$this->alarm_last_update->AdvancedSearch->SearchOperator = $objForm->GetValue("z_alarm_last_update");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// alarm_id
		// alarm_type_id
		// alarm_user_id
		// alarm_product_id
		// alarm_datetime
		// alarm_latitude
		// alarm_longitude
		// alarm_start_on
		// alarm_close_on
		// alarm_close
		// alarm_admin_id
		// alarm_call_parents
		// alarm_call_emergency
		// alarm_note
		// alarm_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// alarm_id
		$this->alarm_id->ViewValue = $this->alarm_id->CurrentValue;
		$this->alarm_id->ViewCustomAttributes = "";

		// alarm_type_id
		if (strval($this->alarm_type_id->CurrentValue) <> "") {
			$sFilterWrk = "`alarm_type_id`" . ew_SearchString("=", $this->alarm_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_type_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->CurrentValue;
			}
		} else {
			$this->alarm_type_id->ViewValue = NULL;
		}
		$this->alarm_type_id->ViewCustomAttributes = "";

		// alarm_user_id
		$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
		if (strval($this->alarm_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->alarm_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
			}
		} else {
			$this->alarm_user_id->ViewValue = NULL;
		}
		$this->alarm_user_id->ViewCustomAttributes = "";

		// alarm_product_id
		if (strval($this->alarm_product_id->CurrentValue) <> "") {
			$sFilterWrk = "telecare_product.product_id" . ew_SearchString("=", $this->alarm_product_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_product_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->CurrentValue;
			}
		} else {
			$this->alarm_product_id->ViewValue = NULL;
		}
		$this->alarm_product_id->ViewCustomAttributes = "";

		// alarm_datetime
		$this->alarm_datetime->ViewValue = $this->alarm_datetime->CurrentValue;
		$this->alarm_datetime->ViewValue = ew_FormatDateTime($this->alarm_datetime->ViewValue, 11);
		$this->alarm_datetime->ViewCustomAttributes = "";

		// alarm_latitude
		$this->alarm_latitude->ViewValue = $this->alarm_latitude->CurrentValue;
		$this->alarm_latitude->ViewCustomAttributes = "";

		// alarm_longitude
		$this->alarm_longitude->ViewValue = $this->alarm_longitude->CurrentValue;
		$this->alarm_longitude->ViewCustomAttributes = "";

		// alarm_start_on
		$this->alarm_start_on->ViewValue = $this->alarm_start_on->CurrentValue;
		$this->alarm_start_on->ViewValue = ew_FormatDateTime($this->alarm_start_on->ViewValue, 11);
		$this->alarm_start_on->ViewCustomAttributes = "";

		// alarm_close_on
		$this->alarm_close_on->ViewValue = $this->alarm_close_on->CurrentValue;
		$this->alarm_close_on->ViewValue = ew_FormatDateTime($this->alarm_close_on->ViewValue, 11);
		$this->alarm_close_on->ViewCustomAttributes = "";

		// alarm_close
		if (strval($this->alarm_close->CurrentValue) <> "") {
			$this->alarm_close->ViewValue = $this->alarm_close->OptionCaption($this->alarm_close->CurrentValue);
		} else {
			$this->alarm_close->ViewValue = NULL;
		}
		$this->alarm_close->ViewCustomAttributes = "";

		// alarm_admin_id
		if (strval($this->alarm_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->alarm_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->CurrentValue;
			}
		} else {
			$this->alarm_admin_id->ViewValue = NULL;
		}
		$this->alarm_admin_id->ViewCustomAttributes = "";

		// alarm_call_parents
		if (strval($this->alarm_call_parents->CurrentValue) <> "") {
			$this->alarm_call_parents->ViewValue = $this->alarm_call_parents->OptionCaption($this->alarm_call_parents->CurrentValue);
		} else {
			$this->alarm_call_parents->ViewValue = NULL;
		}
		$this->alarm_call_parents->ViewCustomAttributes = "";

		// alarm_call_emergency
		if (strval($this->alarm_call_emergency->CurrentValue) <> "") {
			$this->alarm_call_emergency->ViewValue = $this->alarm_call_emergency->OptionCaption($this->alarm_call_emergency->CurrentValue);
		} else {
			$this->alarm_call_emergency->ViewValue = NULL;
		}
		$this->alarm_call_emergency->ViewCustomAttributes = "";

		// alarm_note
		$this->alarm_note->ViewValue = $this->alarm_note->CurrentValue;
		$this->alarm_note->ViewCustomAttributes = "";

		// alarm_last_update
		$this->alarm_last_update->ViewValue = $this->alarm_last_update->CurrentValue;
		$this->alarm_last_update->ViewValue = ew_FormatDateTime($this->alarm_last_update->ViewValue, 7);
		$this->alarm_last_update->ViewCustomAttributes = "";

			// alarm_id
			$this->alarm_id->LinkCustomAttributes = "";
			$this->alarm_id->HrefValue = "";
			$this->alarm_id->TooltipValue = "";

			// alarm_type_id
			$this->alarm_type_id->LinkCustomAttributes = "";
			$this->alarm_type_id->HrefValue = "";
			$this->alarm_type_id->TooltipValue = "";

			// alarm_user_id
			$this->alarm_user_id->LinkCustomAttributes = "";
			$this->alarm_user_id->HrefValue = "";
			$this->alarm_user_id->TooltipValue = "";

			// alarm_product_id
			$this->alarm_product_id->LinkCustomAttributes = "";
			$this->alarm_product_id->HrefValue = "";
			$this->alarm_product_id->TooltipValue = "";

			// alarm_datetime
			$this->alarm_datetime->LinkCustomAttributes = "";
			$this->alarm_datetime->HrefValue = "";
			$this->alarm_datetime->TooltipValue = "";

			// alarm_latitude
			$this->alarm_latitude->LinkCustomAttributes = "";
			$this->alarm_latitude->HrefValue = "";
			$this->alarm_latitude->TooltipValue = "";

			// alarm_longitude
			$this->alarm_longitude->LinkCustomAttributes = "";
			$this->alarm_longitude->HrefValue = "";
			$this->alarm_longitude->TooltipValue = "";

			// alarm_start_on
			$this->alarm_start_on->LinkCustomAttributes = "";
			$this->alarm_start_on->HrefValue = "";
			$this->alarm_start_on->TooltipValue = "";

			// alarm_close_on
			$this->alarm_close_on->LinkCustomAttributes = "";
			$this->alarm_close_on->HrefValue = "";
			$this->alarm_close_on->TooltipValue = "";

			// alarm_close
			$this->alarm_close->LinkCustomAttributes = "";
			$this->alarm_close->HrefValue = "";
			$this->alarm_close->TooltipValue = "";

			// alarm_admin_id
			$this->alarm_admin_id->LinkCustomAttributes = "";
			$this->alarm_admin_id->HrefValue = "";
			$this->alarm_admin_id->TooltipValue = "";

			// alarm_call_parents
			$this->alarm_call_parents->LinkCustomAttributes = "";
			$this->alarm_call_parents->HrefValue = "";
			$this->alarm_call_parents->TooltipValue = "";

			// alarm_call_emergency
			$this->alarm_call_emergency->LinkCustomAttributes = "";
			$this->alarm_call_emergency->HrefValue = "";
			$this->alarm_call_emergency->TooltipValue = "";

			// alarm_note
			$this->alarm_note->LinkCustomAttributes = "";
			$this->alarm_note->HrefValue = "";
			$this->alarm_note->TooltipValue = "";

			// alarm_last_update
			$this->alarm_last_update->LinkCustomAttributes = "";
			$this->alarm_last_update->HrefValue = "";
			$this->alarm_last_update->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// alarm_id
			$this->alarm_id->EditAttrs["class"] = "form-control";
			$this->alarm_id->EditCustomAttributes = "";
			$this->alarm_id->EditValue = ew_HtmlEncode($this->alarm_id->AdvancedSearch->SearchValue);
			$this->alarm_id->PlaceHolder = ew_RemoveHtml($this->alarm_id->FldCaption());

			// alarm_type_id
			$this->alarm_type_id->EditAttrs["class"] = "form-control";
			$this->alarm_type_id->EditCustomAttributes = "";
			if (trim(strval($this->alarm_type_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`alarm_type_id`" . ew_SearchString("=", $this->alarm_type_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_alarm_type`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_alarm_type`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->alarm_type_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->alarm_type_id->EditValue = $arwrk;

			// alarm_user_id
			$this->alarm_user_id->EditAttrs["class"] = "form-control";
			$this->alarm_user_id->EditCustomAttributes = "";
			$this->alarm_user_id->EditValue = ew_HtmlEncode($this->alarm_user_id->AdvancedSearch->SearchValue);
			if (strval($this->alarm_user_id->AdvancedSearch->SearchValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->alarm_user_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->alarm_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
					$arwrk[2] = ew_HtmlEncode($rswrk->fields('Disp2Fld'));
					$this->alarm_user_id->EditValue = $this->alarm_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->alarm_user_id->EditValue = ew_HtmlEncode($this->alarm_user_id->AdvancedSearch->SearchValue);
				}
			} else {
				$this->alarm_user_id->EditValue = NULL;
			}
			$this->alarm_user_id->PlaceHolder = ew_RemoveHtml($this->alarm_user_id->FldCaption());

			// alarm_product_id
			$this->alarm_product_id->EditAttrs["class"] = "form-control";
			$this->alarm_product_id->EditCustomAttributes = "";
			if (trim(strval($this->alarm_product_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "telecare_product.product_id" . ew_SearchString("=", $this->alarm_product_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->alarm_product_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->alarm_product_id->EditValue = $arwrk;

			// alarm_datetime
			$this->alarm_datetime->EditAttrs["class"] = "form-control";
			$this->alarm_datetime->EditCustomAttributes = "";
			$this->alarm_datetime->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->alarm_datetime->AdvancedSearch->SearchValue, 11), 11));
			$this->alarm_datetime->PlaceHolder = ew_RemoveHtml($this->alarm_datetime->FldCaption());

			// alarm_latitude
			$this->alarm_latitude->EditAttrs["class"] = "form-control";
			$this->alarm_latitude->EditCustomAttributes = "";
			$this->alarm_latitude->EditValue = ew_HtmlEncode($this->alarm_latitude->AdvancedSearch->SearchValue);
			$this->alarm_latitude->PlaceHolder = ew_RemoveHtml($this->alarm_latitude->FldCaption());

			// alarm_longitude
			$this->alarm_longitude->EditAttrs["class"] = "form-control";
			$this->alarm_longitude->EditCustomAttributes = "";
			$this->alarm_longitude->EditValue = ew_HtmlEncode($this->alarm_longitude->AdvancedSearch->SearchValue);
			$this->alarm_longitude->PlaceHolder = ew_RemoveHtml($this->alarm_longitude->FldCaption());

			// alarm_start_on
			$this->alarm_start_on->EditAttrs["class"] = "form-control";
			$this->alarm_start_on->EditCustomAttributes = "";
			$this->alarm_start_on->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->alarm_start_on->AdvancedSearch->SearchValue, 11), 11));
			$this->alarm_start_on->PlaceHolder = ew_RemoveHtml($this->alarm_start_on->FldCaption());

			// alarm_close_on
			$this->alarm_close_on->EditAttrs["class"] = "form-control";
			$this->alarm_close_on->EditCustomAttributes = "";
			$this->alarm_close_on->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->alarm_close_on->AdvancedSearch->SearchValue, 11), 11));
			$this->alarm_close_on->PlaceHolder = ew_RemoveHtml($this->alarm_close_on->FldCaption());

			// alarm_close
			$this->alarm_close->EditAttrs["class"] = "form-control";
			$this->alarm_close->EditCustomAttributes = "";
			$this->alarm_close->EditValue = $this->alarm_close->Options(TRUE);

			// alarm_admin_id
			$this->alarm_admin_id->EditAttrs["class"] = "form-control";
			$this->alarm_admin_id->EditCustomAttributes = "";
			if (trim(strval($this->alarm_admin_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->alarm_admin_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_alarm"]->UserIDAllow("search")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->alarm_admin_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->alarm_admin_id->EditValue = $arwrk;

			// alarm_call_parents
			$this->alarm_call_parents->EditCustomAttributes = "";
			$this->alarm_call_parents->EditValue = $this->alarm_call_parents->Options(FALSE);

			// alarm_call_emergency
			$this->alarm_call_emergency->EditCustomAttributes = "";
			$this->alarm_call_emergency->EditValue = $this->alarm_call_emergency->Options(FALSE);

			// alarm_note
			$this->alarm_note->EditAttrs["class"] = "form-control";
			$this->alarm_note->EditCustomAttributes = "";
			$this->alarm_note->EditValue = ew_HtmlEncode($this->alarm_note->AdvancedSearch->SearchValue);
			$this->alarm_note->PlaceHolder = ew_RemoveHtml($this->alarm_note->FldCaption());

			// alarm_last_update
			$this->alarm_last_update->EditAttrs["class"] = "form-control";
			$this->alarm_last_update->EditCustomAttributes = "";
			$this->alarm_last_update->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->alarm_last_update->AdvancedSearch->SearchValue, 7), 7));
			$this->alarm_last_update->PlaceHolder = ew_RemoveHtml($this->alarm_last_update->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->alarm_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->alarm_id->FldErrMsg());
		}
		if (!ew_CheckInteger($this->alarm_user_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->alarm_user_id->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->alarm_datetime->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->alarm_datetime->FldErrMsg());
		}
		if (!ew_CheckInteger($this->alarm_longitude->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->alarm_longitude->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->alarm_start_on->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->alarm_start_on->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->alarm_close_on->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->alarm_close_on->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->alarm_last_update->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->alarm_last_update->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->alarm_id->AdvancedSearch->Load();
		$this->alarm_type_id->AdvancedSearch->Load();
		$this->alarm_user_id->AdvancedSearch->Load();
		$this->alarm_product_id->AdvancedSearch->Load();
		$this->alarm_datetime->AdvancedSearch->Load();
		$this->alarm_latitude->AdvancedSearch->Load();
		$this->alarm_longitude->AdvancedSearch->Load();
		$this->alarm_start_on->AdvancedSearch->Load();
		$this->alarm_close_on->AdvancedSearch->Load();
		$this->alarm_close->AdvancedSearch->Load();
		$this->alarm_admin_id->AdvancedSearch->Load();
		$this->alarm_call_parents->AdvancedSearch->Load();
		$this->alarm_call_emergency->AdvancedSearch->Load();
		$this->alarm_note->AdvancedSearch->Load();
		$this->alarm_last_update->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_alarmlist.php", "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_alarm_search)) $telecare_alarm_search = new ctelecare_alarm_search();

// Page init
$telecare_alarm_search->Page_Init();

// Page main
$telecare_alarm_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_alarm_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($telecare_alarm_search->IsModal) { ?>
var CurrentAdvancedSearchForm = ftelecare_alarmsearch = new ew_Form("ftelecare_alarmsearch", "search");
<?php } else { ?>
var CurrentForm = ftelecare_alarmsearch = new ew_Form("ftelecare_alarmsearch", "search");
<?php } ?>

// Form_CustomValidate event
ftelecare_alarmsearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_alarmsearch.ValidateRequired = true;
<?php } else { ?>
ftelecare_alarmsearch.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_alarmsearch.Lists["x_alarm_type_id"] = {"LinkField":"x_alarm_type_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_alarm_type_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmsearch.Lists["x_alarm_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmsearch.Lists["x_alarm_product_id"] = {"LinkField":"x_product_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_product_type_name","x_product_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmsearch.Lists["x_alarm_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmsearch.Lists["x_alarm_close"].Options = <?php echo json_encode($telecare_alarm->alarm_close->Options()) ?>;
ftelecare_alarmsearch.Lists["x_alarm_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmsearch.Lists["x_alarm_call_parents"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmsearch.Lists["x_alarm_call_parents"].Options = <?php echo json_encode($telecare_alarm->alarm_call_parents->Options()) ?>;
ftelecare_alarmsearch.Lists["x_alarm_call_emergency"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmsearch.Lists["x_alarm_call_emergency"].Options = <?php echo json_encode($telecare_alarm->alarm_call_emergency->Options()) ?>;

// Form object for search
// Validate function for search

ftelecare_alarmsearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_alarm_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_alarm_user_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_user_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_alarm_datetime");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_datetime->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_alarm_longitude");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_longitude->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_alarm_start_on");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_start_on->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_alarm_close_on");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_close_on->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_alarm_last_update");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_last_update->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$telecare_alarm_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $telecare_alarm_search->ShowPageHeader(); ?>
<?php
$telecare_alarm_search->ShowMessage();
?>
<form name="ftelecare_alarmsearch" id="ftelecare_alarmsearch" class="<?php echo $telecare_alarm_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_alarm_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_alarm_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_alarm">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($telecare_alarm_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div>
<?php if ($telecare_alarm->alarm_id->Visible) { // alarm_id ?>
	<div id="r_alarm_id" class="form-group">
		<label for="x_alarm_id" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_id"><?php echo $telecare_alarm->alarm_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_id" id="z_alarm_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_id->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_id">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_id" name="x_alarm_id" id="x_alarm_id" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_id->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_id->EditValue ?>"<?php echo $telecare_alarm->alarm_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
	<div id="r_alarm_type_id" class="form-group">
		<label for="x_alarm_type_id" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_type_id"><?php echo $telecare_alarm->alarm_type_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_type_id" id="z_alarm_type_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_type_id->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_type_id">
<select data-table="telecare_alarm" data-field="x_alarm_type_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_type_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_type_id->DisplayValueSeparator) : $telecare_alarm->alarm_type_id->DisplayValueSeparator) ?>" id="x_alarm_type_id" name="x_alarm_type_id"<?php echo $telecare_alarm->alarm_type_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_type_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_type_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_type_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_type_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_type_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_type_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_type_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_type_id->LookupFilters += array("f0" => "`alarm_type_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_type_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_type_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_alarm_type_id" id="s_x_alarm_type_id" value="<?php echo $telecare_alarm->alarm_type_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
	<div id="r_alarm_user_id" class="form-group">
		<label class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_user_id"><?php echo $telecare_alarm->alarm_user_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_user_id" id="z_alarm_user_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_user_id->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_alarm->alarm_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_alarm->alarm_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x_alarm_user_id" style="white-space: nowrap; z-index: 8970">
	<input type="text" name="sv_x_alarm_user_id" id="sv_x_alarm_user_id" value="<?php echo $telecare_alarm->alarm_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>"<?php echo $telecare_alarm->alarm_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_user_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_user_id->DisplayValueSeparator) : $telecare_alarm->alarm_user_id->DisplayValueSeparator) ?>" name="x_alarm_user_id" id="x_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->AdvancedSearch->SearchValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x_alarm_user_id" id="q_x_alarm_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_alarmsearch.CreateAutoSuggest({"id":"x_alarm_user_id","forceSelect":false});
</script>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
	<div id="r_alarm_product_id" class="form-group">
		<label for="x_alarm_product_id" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_product_id"><?php echo $telecare_alarm->alarm_product_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_product_id" id="z_alarm_product_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_product_id->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_product_id">
<select data-table="telecare_alarm" data-field="x_alarm_product_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_product_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_product_id->DisplayValueSeparator) : $telecare_alarm->alarm_product_id->DisplayValueSeparator) ?>" id="x_alarm_product_id" name="x_alarm_product_id"<?php echo $telecare_alarm->alarm_product_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_product_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_product_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_product_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_product_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_product_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_product_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_product_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_product_id->LookupFilters += array("f0" => "telecare_product.product_id = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_product_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_product_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_alarm_product_id" id="s_x_alarm_product_id" value="<?php echo $telecare_alarm->alarm_product_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
	<div id="r_alarm_datetime" class="form-group">
		<label for="x_alarm_datetime" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_datetime"><?php echo $telecare_alarm->alarm_datetime->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_datetime" id="z_alarm_datetime" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_datetime->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_datetime">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_datetime" data-format="11" name="x_alarm_datetime" id="x_alarm_datetime" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_datetime->EditValue ?>"<?php echo $telecare_alarm->alarm_datetime->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_datetime->ReadOnly && !$telecare_alarm->alarm_datetime->Disabled && !isset($telecare_alarm->alarm_datetime->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_datetime->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmsearch", "x_alarm_datetime", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_latitude->Visible) { // alarm_latitude ?>
	<div id="r_alarm_latitude" class="form-group">
		<label for="x_alarm_latitude" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_latitude"><?php echo $telecare_alarm->alarm_latitude->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_latitude" id="z_alarm_latitude" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_latitude->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_latitude">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_latitude" name="x_alarm_latitude" id="x_alarm_latitude" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_latitude->EditValue ?>"<?php echo $telecare_alarm->alarm_latitude->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_longitude->Visible) { // alarm_longitude ?>
	<div id="r_alarm_longitude" class="form-group">
		<label for="x_alarm_longitude" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_longitude"><?php echo $telecare_alarm->alarm_longitude->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_longitude" id="z_alarm_longitude" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_longitude->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_longitude">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_longitude" name="x_alarm_longitude" id="x_alarm_longitude" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_longitude->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_longitude->EditValue ?>"<?php echo $telecare_alarm->alarm_longitude->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
	<div id="r_alarm_start_on" class="form-group">
		<label for="x_alarm_start_on" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_start_on"><?php echo $telecare_alarm->alarm_start_on->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_start_on" id="z_alarm_start_on" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_start_on->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_start_on">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_start_on" data-format="11" name="x_alarm_start_on" id="x_alarm_start_on" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_start_on->EditValue ?>"<?php echo $telecare_alarm->alarm_start_on->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_start_on->ReadOnly && !$telecare_alarm->alarm_start_on->Disabled && !isset($telecare_alarm->alarm_start_on->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmsearch", "x_alarm_start_on", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_close_on->Visible) { // alarm_close_on ?>
	<div id="r_alarm_close_on" class="form-group">
		<label for="x_alarm_close_on" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_close_on"><?php echo $telecare_alarm->alarm_close_on->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_close_on" id="z_alarm_close_on" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_close_on->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_close_on">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_close_on" data-format="11" name="x_alarm_close_on" id="x_alarm_close_on" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_close_on->EditValue ?>"<?php echo $telecare_alarm->alarm_close_on->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_close_on->ReadOnly && !$telecare_alarm->alarm_close_on->Disabled && !isset($telecare_alarm->alarm_close_on->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmsearch", "x_alarm_close_on", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
	<div id="r_alarm_close" class="form-group">
		<label for="x_alarm_close" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_close"><?php echo $telecare_alarm->alarm_close->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_close" id="z_alarm_close" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_close->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_close">
<select data-table="telecare_alarm" data-field="x_alarm_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_close->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_close->DisplayValueSeparator) : $telecare_alarm->alarm_close->DisplayValueSeparator) ?>" id="x_alarm_close" name="x_alarm_close"<?php echo $telecare_alarm->alarm_close->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_close->EditValue)) {
	$arwrk = $telecare_alarm->alarm_close->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_close->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_close->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_close->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_close->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_admin_id->Visible) { // alarm_admin_id ?>
	<div id="r_alarm_admin_id" class="form-group">
		<label for="x_alarm_admin_id" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_admin_id"><?php echo $telecare_alarm->alarm_admin_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_admin_id" id="z_alarm_admin_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_admin_id->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_admin_id">
<select data-table="telecare_alarm" data-field="x_alarm_admin_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_admin_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_admin_id->DisplayValueSeparator) : $telecare_alarm->alarm_admin_id->DisplayValueSeparator) ?>" id="x_alarm_admin_id" name="x_alarm_admin_id"<?php echo $telecare_alarm->alarm_admin_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_admin_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_admin_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_admin_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_admin_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_admin_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_admin_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_admin_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
if (!$GLOBALS["telecare_alarm"]->UserIDAllow("search")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_alarm->alarm_admin_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_admin_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_admin_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_admin_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_alarm_admin_id" id="s_x_alarm_admin_id" value="<?php echo $telecare_alarm->alarm_admin_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
	<div id="r_alarm_call_parents" class="form-group">
		<label class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_call_parents"><?php echo $telecare_alarm->alarm_call_parents->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_call_parents" id="z_alarm_call_parents" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_call_parents->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_call_parents">
<div id="tp_x_alarm_call_parents" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_parents->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_parents->DisplayValueSeparator) : $telecare_alarm->alarm_call_parents->DisplayValueSeparator) ?>" name="x_alarm_call_parents" id="x_alarm_call_parents" value="{value}"<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>></div>
<div id="dsl_x_alarm_call_parents" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_parents->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_parents->AdvancedSearch->SearchValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x_alarm_call_parents" id="x_alarm_call_parents_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_parents->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x_alarm_call_parents" id="x_alarm_call_parents_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
	<div id="r_alarm_call_emergency" class="form-group">
		<label class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_call_emergency"><?php echo $telecare_alarm->alarm_call_emergency->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_call_emergency" id="z_alarm_call_emergency" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_call_emergency->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_call_emergency">
<div id="tp_x_alarm_call_emergency" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) : $telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ?>" name="x_alarm_call_emergency" id="x_alarm_call_emergency" value="{value}"<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>></div>
<div id="dsl_x_alarm_call_emergency" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_emergency->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_emergency->AdvancedSearch->SearchValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x_alarm_call_emergency" id="x_alarm_call_emergency_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_emergency->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x_alarm_call_emergency" id="x_alarm_call_emergency_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_note->Visible) { // alarm_note ?>
	<div id="r_alarm_note" class="form-group">
		<label for="x_alarm_note" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_note"><?php echo $telecare_alarm->alarm_note->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_alarm_note" id="z_alarm_note" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_note->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_note">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_note" name="x_alarm_note" id="x_alarm_note" size="35" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_note->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_note->EditValue ?>"<?php echo $telecare_alarm->alarm_note->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_last_update->Visible) { // alarm_last_update ?>
	<div id="r_alarm_last_update" class="form-group">
		<label for="x_alarm_last_update" class="<?php echo $telecare_alarm_search->SearchLabelClass ?>"><span id="elh_telecare_alarm_alarm_last_update"><?php echo $telecare_alarm->alarm_last_update->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_alarm_last_update" id="z_alarm_last_update" value="="></p>
		</label>
		<div class="<?php echo $telecare_alarm_search->SearchRightColumnClass ?>"><div<?php echo $telecare_alarm->alarm_last_update->CellAttributes() ?>>
			<span id="el_telecare_alarm_alarm_last_update">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_last_update" data-format="7" name="x_alarm_last_update" id="x_alarm_last_update" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_last_update->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_last_update->EditValue ?>"<?php echo $telecare_alarm->alarm_last_update->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
<?php if (!$telecare_alarm_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
ftelecare_alarmsearch.Init();
</script>
<?php
$telecare_alarm_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_alarm_search->Page_Terminate();
?>
