<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_prescription_druginfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_prescriptioninfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_prescription_drug_add = NULL; // Initialize page object first

class ctelecare_prescription_drug_add extends ctelecare_prescription_drug {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_prescription_drug';

	// Page object name
	var $PageObjName = 'telecare_prescription_drug_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_prescription_drug)
		if (!isset($GLOBALS["telecare_prescription_drug"]) || get_class($GLOBALS["telecare_prescription_drug"]) == "ctelecare_prescription_drug") {
			$GLOBALS["telecare_prescription_drug"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_prescription_drug"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_prescription)
		if (!isset($GLOBALS['telecare_prescription'])) $GLOBALS['telecare_prescription'] = new ctelecare_prescription();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_prescription_drug', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_prescription_druglist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_prescription_drug;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_prescription_drug);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["prescription_drug_id"] != "") {
				$this->prescription_drug_id->setQueryStringValue($_GET["prescription_drug_id"]);
				$this->setKey("prescription_drug_id", $this->prescription_drug_id->CurrentValue); // Set up key
			} else {
				$this->setKey("prescription_drug_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_prescription_druglist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_prescription_drugview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->prescription_id->CurrentValue = NULL;
		$this->prescription_id->OldValue = $this->prescription_id->CurrentValue;
		$this->drug_id->CurrentValue = NULL;
		$this->drug_id->OldValue = $this->drug_id->CurrentValue;
		$this->prescription_number->CurrentValue = 1;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->prescription_id->FldIsDetailKey) {
			$this->prescription_id->setFormValue($objForm->GetValue("x_prescription_id"));
		}
		if (!$this->drug_id->FldIsDetailKey) {
			$this->drug_id->setFormValue($objForm->GetValue("x_drug_id"));
		}
		if (!$this->prescription_number->FldIsDetailKey) {
			$this->prescription_number->setFormValue($objForm->GetValue("x_prescription_number"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->prescription_id->CurrentValue = $this->prescription_id->FormValue;
		$this->drug_id->CurrentValue = $this->drug_id->FormValue;
		$this->prescription_number->CurrentValue = $this->prescription_number->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->prescription_id->setDbValue($rs->fields('prescription_id'));
		$this->drug_id->setDbValue($rs->fields('drug_id'));
		if (array_key_exists('EV__drug_id', $rs->fields)) {
			$this->drug_id->VirtualValue = $rs->fields('EV__drug_id'); // Set up virtual field value
		} else {
			$this->drug_id->VirtualValue = ""; // Clear value
		}
		$this->prescription_drug_id->setDbValue($rs->fields('prescription_drug_id'));
		$this->prescription_number->setDbValue($rs->fields('prescription_number'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->prescription_id->DbValue = $row['prescription_id'];
		$this->drug_id->DbValue = $row['drug_id'];
		$this->prescription_drug_id->DbValue = $row['prescription_drug_id'];
		$this->prescription_number->DbValue = $row['prescription_number'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("prescription_drug_id")) <> "")
			$this->prescription_drug_id->CurrentValue = $this->getKey("prescription_drug_id"); // prescription_drug_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// prescription_id
		// drug_id
		// prescription_drug_id
		// prescription_number

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// prescription_id
		$this->prescription_id->ViewValue = $this->prescription_id->CurrentValue;
		$this->prescription_id->ViewCustomAttributes = "";

		// drug_id
		if ($this->drug_id->VirtualValue <> "") {
			$this->drug_id->ViewValue = $this->drug_id->VirtualValue;
		} else {
		if (strval($this->drug_id->CurrentValue) <> "") {
			$sFilterWrk = "`drug_id`" . ew_SearchString("=", $this->drug_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->drug_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `drug_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->drug_id->ViewValue = $this->drug_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->drug_id->ViewValue = $this->drug_id->CurrentValue;
			}
		} else {
			$this->drug_id->ViewValue = NULL;
		}
		}
		$this->drug_id->ViewCustomAttributes = "";

		// prescription_drug_id
		$this->prescription_drug_id->ViewValue = $this->prescription_drug_id->CurrentValue;
		$this->prescription_drug_id->ViewCustomAttributes = "";

		// prescription_number
		$this->prescription_number->ViewValue = $this->prescription_number->CurrentValue;
		$this->prescription_number->ViewCustomAttributes = "";

			// prescription_id
			$this->prescription_id->LinkCustomAttributes = "";
			$this->prescription_id->HrefValue = "";
			$this->prescription_id->TooltipValue = "";

			// drug_id
			$this->drug_id->LinkCustomAttributes = "";
			$this->drug_id->HrefValue = "";
			$this->drug_id->TooltipValue = "";

			// prescription_number
			$this->prescription_number->LinkCustomAttributes = "";
			$this->prescription_number->HrefValue = "";
			$this->prescription_number->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// prescription_id
			$this->prescription_id->EditAttrs["class"] = "form-control";
			$this->prescription_id->EditCustomAttributes = "";
			if ($this->prescription_id->getSessionValue() <> "") {
				$this->prescription_id->CurrentValue = $this->prescription_id->getSessionValue();
			$this->prescription_id->ViewValue = $this->prescription_id->CurrentValue;
			$this->prescription_id->ViewCustomAttributes = "";
			} else {
			$this->prescription_id->EditValue = ew_HtmlEncode($this->prescription_id->CurrentValue);
			$this->prescription_id->PlaceHolder = ew_RemoveHtml($this->prescription_id->FldCaption());
			}

			// drug_id
			$this->drug_id->EditAttrs["class"] = "form-control";
			$this->drug_id->EditCustomAttributes = "";
			if (trim(strval($this->drug_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`drug_id`" . ew_SearchString("=", $this->drug_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_drug`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_drug`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->drug_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `drug_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->drug_id->EditValue = $arwrk;

			// prescription_number
			$this->prescription_number->EditAttrs["class"] = "form-control";
			$this->prescription_number->EditCustomAttributes = "";
			$this->prescription_number->EditValue = ew_HtmlEncode($this->prescription_number->CurrentValue);
			$this->prescription_number->PlaceHolder = ew_RemoveHtml($this->prescription_number->FldCaption());

			// Edit refer script
			// prescription_id

			$this->prescription_id->LinkCustomAttributes = "";
			$this->prescription_id->HrefValue = "";

			// drug_id
			$this->drug_id->LinkCustomAttributes = "";
			$this->drug_id->HrefValue = "";

			// prescription_number
			$this->prescription_number->LinkCustomAttributes = "";
			$this->prescription_number->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckInteger($this->prescription_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->prescription_id->FldErrMsg());
		}
		if (!ew_CheckInteger($this->prescription_number->FormValue)) {
			ew_AddMessage($gsFormError, $this->prescription_number->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// prescription_id
		$this->prescription_id->SetDbValueDef($rsnew, $this->prescription_id->CurrentValue, NULL, FALSE);

		// drug_id
		$this->drug_id->SetDbValueDef($rsnew, $this->drug_id->CurrentValue, NULL, FALSE);

		// prescription_number
		$this->prescription_number->SetDbValueDef($rsnew, $this->prescription_number->CurrentValue, NULL, strval($this->prescription_number->CurrentValue) == "");

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->prescription_drug_id->setDbValue($conn->Insert_ID());
				$rsnew['prescription_drug_id'] = $this->prescription_drug_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_prescription") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_prescription_id"] <> "") {
					$GLOBALS["telecare_prescription"]->prescription_id->setQueryStringValue($_GET["fk_prescription_id"]);
					$this->prescription_id->setQueryStringValue($GLOBALS["telecare_prescription"]->prescription_id->QueryStringValue);
					$this->prescription_id->setSessionValue($this->prescription_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_prescription"]->prescription_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_prescription") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_prescription_id"] <> "") {
					$GLOBALS["telecare_prescription"]->prescription_id->setFormValue($_POST["fk_prescription_id"]);
					$this->prescription_id->setFormValue($GLOBALS["telecare_prescription"]->prescription_id->FormValue);
					$this->prescription_id->setSessionValue($this->prescription_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_prescription"]->prescription_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "telecare_prescription") {
				if ($this->prescription_id->QueryStringValue == "") $this->prescription_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_prescription_druglist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_prescription_drug_add)) $telecare_prescription_drug_add = new ctelecare_prescription_drug_add();

// Page init
$telecare_prescription_drug_add->Page_Init();

// Page main
$telecare_prescription_drug_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_prescription_drug_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_prescription_drugadd = new ew_Form("ftelecare_prescription_drugadd", "add");

// Validate form
ftelecare_prescription_drugadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_prescription_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription_drug->prescription_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_number");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription_drug->prescription_number->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_prescription_drugadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_prescription_drugadd.ValidateRequired = true;
<?php } else { ?>
ftelecare_prescription_drugadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_prescription_drugadd.Lists["x_drug_id"] = {"LinkField":"x_drug_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_drug_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_prescription_drug_add->ShowPageHeader(); ?>
<?php
$telecare_prescription_drug_add->ShowMessage();
?>
<form name="ftelecare_prescription_drugadd" id="ftelecare_prescription_drugadd" class="<?php echo $telecare_prescription_drug_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_prescription_drug_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_prescription_drug_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_prescription_drug">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($telecare_prescription_drug->prescription_id->Visible) { // prescription_id ?>
	<div id="r_prescription_id" class="form-group">
		<label id="elh_telecare_prescription_drug_prescription_id" for="x_prescription_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription_drug->prescription_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription_drug->prescription_id->CellAttributes() ?>>
<?php if ($telecare_prescription_drug->prescription_id->getSessionValue() <> "") { ?>
<span id="el_telecare_prescription_drug_prescription_id">
<span<?php echo $telecare_prescription_drug->prescription_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_prescription_id" name="x_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_telecare_prescription_drug_prescription_id">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="x_prescription_id" id="x_prescription_id" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_id->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_id->EditAttributes() ?>>
</span>
<?php } ?>
<?php echo $telecare_prescription_drug->prescription_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription_drug->drug_id->Visible) { // drug_id ?>
	<div id="r_drug_id" class="form-group">
		<label id="elh_telecare_prescription_drug_drug_id" for="x_drug_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription_drug->drug_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription_drug->drug_id->CellAttributes() ?>>
<span id="el_telecare_prescription_drug_drug_id">
<select data-table="telecare_prescription_drug" data-field="x_drug_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription_drug->drug_id->DisplayValueSeparator) ? json_encode($telecare_prescription_drug->drug_id->DisplayValueSeparator) : $telecare_prescription_drug->drug_id->DisplayValueSeparator) ?>" id="x_drug_id" name="x_drug_id"<?php echo $telecare_prescription_drug->drug_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription_drug->drug_id->EditValue)) {
	$arwrk = $telecare_prescription_drug->drug_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription_drug->drug_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription_drug->drug_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription_drug->drug_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->CurrentValue) ?>" selected><?php echo $telecare_prescription_drug->drug_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription_drug->drug_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription_drug->drug_id->LookupFilters += array("f0" => "`drug_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription_drug->Lookup_Selecting($telecare_prescription_drug->drug_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `drug_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription_drug->drug_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_drug_id" id="s_x_drug_id" value="<?php echo $telecare_prescription_drug->drug_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_prescription_drug->drug_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_prescription_drug->prescription_number->Visible) { // prescription_number ?>
	<div id="r_prescription_number" class="form-group">
		<label id="elh_telecare_prescription_drug_prescription_number" for="x_prescription_number" class="col-sm-2 control-label ewLabel"><?php echo $telecare_prescription_drug->prescription_number->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_prescription_drug->prescription_number->CellAttributes() ?>>
<span id="el_telecare_prescription_drug_prescription_number">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="x_prescription_number" id="x_prescription_number" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_number->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_number->EditAttributes() ?>>
</span>
<?php echo $telecare_prescription_drug->prescription_number->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_prescription_drug_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_prescription_drugadd.Init();
</script>
<?php
$telecare_prescription_drug_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_prescription_drug_add->Page_Terminate();
?>
