<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_productinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_product_delete = NULL; // Initialize page object first

class ctelecare_product_delete extends ctelecare_product {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_product';

	// Page object name
	var $PageObjName = 'telecare_product_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_product)
		if (!isset($GLOBALS["telecare_product"]) || get_class($GLOBALS["telecare_product"]) == "ctelecare_product") {
			$GLOBALS["telecare_product"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_product"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_product', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_productlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->product_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_product;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_product);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("telecare_productlist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in telecare_product class, telecare_productinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->product_id->setDbValue($rs->fields('product_id'));
		$this->product_admin_id->setDbValue($rs->fields('product_admin_id'));
		$this->product_type_id->setDbValue($rs->fields('product_type_id'));
		$this->product_user_id->setDbValue($rs->fields('product_user_id'));
		$this->product_name->setDbValue($rs->fields('product_name'));
		$this->product_installation_date->setDbValue($rs->fields('product_installation_date'));
		$this->product_installation_account_id->setDbValue($rs->fields('product_installation_account_id'));
		$this->product_attach_file->Upload->DbValue = $rs->fields('product_attach_file');
		$this->product_attach_file->CurrentValue = $this->product_attach_file->Upload->DbValue;
		$this->product_attach_type->setDbValue($rs->fields('product_attach_type'));
		$this->product_attach_size->setDbValue($rs->fields('product_attach_size'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->product_id->DbValue = $row['product_id'];
		$this->product_admin_id->DbValue = $row['product_admin_id'];
		$this->product_type_id->DbValue = $row['product_type_id'];
		$this->product_user_id->DbValue = $row['product_user_id'];
		$this->product_name->DbValue = $row['product_name'];
		$this->product_installation_date->DbValue = $row['product_installation_date'];
		$this->product_installation_account_id->DbValue = $row['product_installation_account_id'];
		$this->product_attach_file->Upload->DbValue = $row['product_attach_file'];
		$this->product_attach_type->DbValue = $row['product_attach_type'];
		$this->product_attach_size->DbValue = $row['product_attach_size'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// product_id
		// product_admin_id
		// product_type_id
		// product_user_id
		// product_name
		// product_installation_date
		// product_installation_account_id
		// product_attach_file
		// product_attach_type

		$this->product_attach_type->CellCssStyle = "white-space: nowrap;";

		// product_attach_size
		$this->product_attach_size->CellCssStyle = "white-space: nowrap;";
		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// product_id
		$this->product_id->ViewValue = $this->product_id->CurrentValue;
		$this->product_id->ViewCustomAttributes = "";

		// product_admin_id
		$this->product_admin_id->ViewValue = $this->product_admin_id->CurrentValue;
		$this->product_admin_id->ViewCustomAttributes = "";

		// product_type_id
		if (strval($this->product_type_id->CurrentValue) <> "") {
			$sFilterWrk = "`product_type_id`" . ew_SearchString("=", $this->product_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_type_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `product_type_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->product_type_id->ViewValue = $this->product_type_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_type_id->ViewValue = $this->product_type_id->CurrentValue;
			}
		} else {
			$this->product_type_id->ViewValue = NULL;
		}
		$this->product_type_id->ViewCustomAttributes = "";

		// product_user_id
		if (strval($this->product_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->product_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->product_user_id->ViewValue = $this->product_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_user_id->ViewValue = $this->product_user_id->CurrentValue;
			}
		} else {
			$this->product_user_id->ViewValue = NULL;
		}
		$this->product_user_id->ViewCustomAttributes = "";

		// product_name
		$this->product_name->ViewValue = $this->product_name->CurrentValue;
		$this->product_name->ViewCustomAttributes = "";

		// product_installation_date
		$this->product_installation_date->ViewValue = $this->product_installation_date->CurrentValue;
		$this->product_installation_date->ViewValue = ew_FormatDateTime($this->product_installation_date->ViewValue, 7);
		$this->product_installation_date->ViewCustomAttributes = "";

		// product_installation_account_id
		if (strval($this->product_installation_account_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->product_installation_account_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = "`admin_level` = 4";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_installation_account_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->product_installation_account_id->ViewValue = $this->product_installation_account_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_installation_account_id->ViewValue = $this->product_installation_account_id->CurrentValue;
			}
		} else {
			$this->product_installation_account_id->ViewValue = NULL;
		}
		$this->product_installation_account_id->ViewCustomAttributes = "";

		// product_attach_file
		if (!ew_Empty($this->product_attach_file->Upload->DbValue)) {
			$this->product_attach_file->ViewValue = $this->product_attach_file->Upload->DbValue;
		} else {
			$this->product_attach_file->ViewValue = "";
		}
		$this->product_attach_file->ViewCustomAttributes = "";

			// product_id
			$this->product_id->LinkCustomAttributes = "";
			$this->product_id->HrefValue = "";
			$this->product_id->TooltipValue = "";

			// product_admin_id
			$this->product_admin_id->LinkCustomAttributes = "";
			$this->product_admin_id->HrefValue = "";
			$this->product_admin_id->TooltipValue = "";

			// product_type_id
			$this->product_type_id->LinkCustomAttributes = "";
			$this->product_type_id->HrefValue = "";
			$this->product_type_id->TooltipValue = "";

			// product_user_id
			$this->product_user_id->LinkCustomAttributes = "";
			$this->product_user_id->HrefValue = "";
			$this->product_user_id->TooltipValue = "";

			// product_name
			$this->product_name->LinkCustomAttributes = "";
			$this->product_name->HrefValue = "";
			$this->product_name->TooltipValue = "";

			// product_installation_date
			$this->product_installation_date->LinkCustomAttributes = "";
			$this->product_installation_date->HrefValue = "";
			$this->product_installation_date->TooltipValue = "";

			// product_installation_account_id
			$this->product_installation_account_id->LinkCustomAttributes = "";
			$this->product_installation_account_id->HrefValue = "";
			$this->product_installation_account_id->TooltipValue = "";

			// product_attach_file
			$this->product_attach_file->LinkCustomAttributes = "";
			$this->product_attach_file->HrefValue = "";
			$this->product_attach_file->HrefValue2 = $this->product_attach_file->UploadPath . $this->product_attach_file->Upload->DbValue;
			$this->product_attach_file->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['product_id'];
				$this->LoadDbValues($row);
				$OldFiles = explode(EW_MULTIPLE_UPLOAD_SEPARATOR, $row['product_attach_file']);
				$FileCount = count($OldFiles);
				for ($i = 0; $i < $FileCount; $i++) {
					@unlink(ew_UploadPathEx(TRUE, $this->product_attach_file->OldUploadPath) . $OldFiles[$i]);
				}
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_productlist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_product_delete)) $telecare_product_delete = new ctelecare_product_delete();

// Page init
$telecare_product_delete->Page_Init();

// Page main
$telecare_product_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_product_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftelecare_productdelete = new ew_Form("ftelecare_productdelete", "delete");

// Form_CustomValidate event
ftelecare_productdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_productdelete.ValidateRequired = true;
<?php } else { ?>
ftelecare_productdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_productdelete.Lists["x_product_type_id"] = {"LinkField":"x_product_type_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_product_type_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_productdelete.Lists["x_product_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_productdelete.Lists["x_product_installation_account_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($telecare_product_delete->Recordset = $telecare_product_delete->LoadRecordset())
	$telecare_product_deleteTotalRecs = $telecare_product_delete->Recordset->RecordCount(); // Get record count
if ($telecare_product_deleteTotalRecs <= 0) { // No record found, exit
	if ($telecare_product_delete->Recordset)
		$telecare_product_delete->Recordset->Close();
	$telecare_product_delete->Page_Terminate("telecare_productlist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_product_delete->ShowPageHeader(); ?>
<?php
$telecare_product_delete->ShowMessage();
?>
<form name="ftelecare_productdelete" id="ftelecare_productdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_product_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_product_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_product">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($telecare_product_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $telecare_product->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($telecare_product->product_id->Visible) { // product_id ?>
		<th><span id="elh_telecare_product_product_id" class="telecare_product_product_id"><?php echo $telecare_product->product_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_product->product_admin_id->Visible) { // product_admin_id ?>
		<th><span id="elh_telecare_product_product_admin_id" class="telecare_product_product_admin_id"><?php echo $telecare_product->product_admin_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_product->product_type_id->Visible) { // product_type_id ?>
		<th><span id="elh_telecare_product_product_type_id" class="telecare_product_product_type_id"><?php echo $telecare_product->product_type_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_product->product_user_id->Visible) { // product_user_id ?>
		<th><span id="elh_telecare_product_product_user_id" class="telecare_product_product_user_id"><?php echo $telecare_product->product_user_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_product->product_name->Visible) { // product_name ?>
		<th><span id="elh_telecare_product_product_name" class="telecare_product_product_name"><?php echo $telecare_product->product_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_product->product_installation_date->Visible) { // product_installation_date ?>
		<th><span id="elh_telecare_product_product_installation_date" class="telecare_product_product_installation_date"><?php echo $telecare_product->product_installation_date->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_product->product_installation_account_id->Visible) { // product_installation_account_id ?>
		<th><span id="elh_telecare_product_product_installation_account_id" class="telecare_product_product_installation_account_id"><?php echo $telecare_product->product_installation_account_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_product->product_attach_file->Visible) { // product_attach_file ?>
		<th><span id="elh_telecare_product_product_attach_file" class="telecare_product_product_attach_file"><?php echo $telecare_product->product_attach_file->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$telecare_product_delete->RecCnt = 0;
$i = 0;
while (!$telecare_product_delete->Recordset->EOF) {
	$telecare_product_delete->RecCnt++;
	$telecare_product_delete->RowCnt++;

	// Set row properties
	$telecare_product->ResetAttrs();
	$telecare_product->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$telecare_product_delete->LoadRowValues($telecare_product_delete->Recordset);

	// Render row
	$telecare_product_delete->RenderRow();
?>
	<tr<?php echo $telecare_product->RowAttributes() ?>>
<?php if ($telecare_product->product_id->Visible) { // product_id ?>
		<td<?php echo $telecare_product->product_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_id" class="telecare_product_product_id">
<span<?php echo $telecare_product->product_id->ViewAttributes() ?>>
<?php echo $telecare_product->product_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_product->product_admin_id->Visible) { // product_admin_id ?>
		<td<?php echo $telecare_product->product_admin_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_admin_id" class="telecare_product_product_admin_id">
<span<?php echo $telecare_product->product_admin_id->ViewAttributes() ?>>
<?php echo $telecare_product->product_admin_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_product->product_type_id->Visible) { // product_type_id ?>
		<td<?php echo $telecare_product->product_type_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_type_id" class="telecare_product_product_type_id">
<span<?php echo $telecare_product->product_type_id->ViewAttributes() ?>>
<?php echo $telecare_product->product_type_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_product->product_user_id->Visible) { // product_user_id ?>
		<td<?php echo $telecare_product->product_user_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_user_id" class="telecare_product_product_user_id">
<span<?php echo $telecare_product->product_user_id->ViewAttributes() ?>>
<?php echo $telecare_product->product_user_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_product->product_name->Visible) { // product_name ?>
		<td<?php echo $telecare_product->product_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_name" class="telecare_product_product_name">
<span<?php echo $telecare_product->product_name->ViewAttributes() ?>>
<?php echo $telecare_product->product_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_product->product_installation_date->Visible) { // product_installation_date ?>
		<td<?php echo $telecare_product->product_installation_date->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_installation_date" class="telecare_product_product_installation_date">
<span<?php echo $telecare_product->product_installation_date->ViewAttributes() ?>>
<?php echo $telecare_product->product_installation_date->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_product->product_installation_account_id->Visible) { // product_installation_account_id ?>
		<td<?php echo $telecare_product->product_installation_account_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_installation_account_id" class="telecare_product_product_installation_account_id">
<span<?php echo $telecare_product->product_installation_account_id->ViewAttributes() ?>>
<?php echo $telecare_product->product_installation_account_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_product->product_attach_file->Visible) { // product_attach_file ?>
		<td<?php echo $telecare_product->product_attach_file->CellAttributes() ?>>
<span id="el<?php echo $telecare_product_delete->RowCnt ?>_telecare_product_product_attach_file" class="telecare_product_product_attach_file">
<span<?php echo $telecare_product->product_attach_file->ViewAttributes() ?>>
<?php echo ew_GetFileViewTag($telecare_product->product_attach_file, $telecare_product->product_attach_file->ListViewValue()) ?>
</span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$telecare_product_delete->Recordset->MoveNext();
}
$telecare_product_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_product_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftelecare_productdelete.Init();
</script>
<?php
$telecare_product_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_product_delete->Page_Terminate();
?>
