<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_alarminfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_alarm_delete = NULL; // Initialize page object first

class ctelecare_alarm_delete extends ctelecare_alarm {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_alarm';

	// Page object name
	var $PageObjName = 'telecare_alarm_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_alarm)
		if (!isset($GLOBALS["telecare_alarm"]) || get_class($GLOBALS["telecare_alarm"]) == "ctelecare_alarm") {
			$GLOBALS["telecare_alarm"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_alarm"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_user)
		if (!isset($GLOBALS['telecare_user'])) $GLOBALS['telecare_user'] = new ctelecare_user();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_alarm', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_alarmlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->alarm_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_alarm;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_alarm);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("telecare_alarmlist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in telecare_alarm class, telecare_alarminfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->alarm_id->setDbValue($rs->fields('alarm_id'));
		$this->alarm_type_id->setDbValue($rs->fields('alarm_type_id'));
		$this->alarm_user_id->setDbValue($rs->fields('alarm_user_id'));
		$this->alarm_product_id->setDbValue($rs->fields('alarm_product_id'));
		$this->alarm_datetime->setDbValue($rs->fields('alarm_datetime'));
		$this->alarm_latitude->setDbValue($rs->fields('alarm_latitude'));
		$this->alarm_longitude->setDbValue($rs->fields('alarm_longitude'));
		$this->alarm_start_on->setDbValue($rs->fields('alarm_start_on'));
		$this->alarm_close_on->setDbValue($rs->fields('alarm_close_on'));
		$this->alarm_close->setDbValue($rs->fields('alarm_close'));
		$this->alarm_admin_id->setDbValue($rs->fields('alarm_admin_id'));
		$this->alarm_call_parents->setDbValue($rs->fields('alarm_call_parents'));
		$this->alarm_call_emergency->setDbValue($rs->fields('alarm_call_emergency'));
		$this->alarm_note->setDbValue($rs->fields('alarm_note'));
		$this->alarm_last_update->setDbValue($rs->fields('alarm_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->alarm_id->DbValue = $row['alarm_id'];
		$this->alarm_type_id->DbValue = $row['alarm_type_id'];
		$this->alarm_user_id->DbValue = $row['alarm_user_id'];
		$this->alarm_product_id->DbValue = $row['alarm_product_id'];
		$this->alarm_datetime->DbValue = $row['alarm_datetime'];
		$this->alarm_latitude->DbValue = $row['alarm_latitude'];
		$this->alarm_longitude->DbValue = $row['alarm_longitude'];
		$this->alarm_start_on->DbValue = $row['alarm_start_on'];
		$this->alarm_close_on->DbValue = $row['alarm_close_on'];
		$this->alarm_close->DbValue = $row['alarm_close'];
		$this->alarm_admin_id->DbValue = $row['alarm_admin_id'];
		$this->alarm_call_parents->DbValue = $row['alarm_call_parents'];
		$this->alarm_call_emergency->DbValue = $row['alarm_call_emergency'];
		$this->alarm_note->DbValue = $row['alarm_note'];
		$this->alarm_last_update->DbValue = $row['alarm_last_update'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// alarm_id
		// alarm_type_id
		// alarm_user_id
		// alarm_product_id
		// alarm_datetime
		// alarm_latitude
		// alarm_longitude
		// alarm_start_on
		// alarm_close_on
		// alarm_close
		// alarm_admin_id
		// alarm_call_parents
		// alarm_call_emergency
		// alarm_note
		// alarm_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// alarm_id
		$this->alarm_id->ViewValue = $this->alarm_id->CurrentValue;
		$this->alarm_id->ViewCustomAttributes = "";

		// alarm_type_id
		if (strval($this->alarm_type_id->CurrentValue) <> "") {
			$sFilterWrk = "`alarm_type_id`" . ew_SearchString("=", $this->alarm_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_type_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->CurrentValue;
			}
		} else {
			$this->alarm_type_id->ViewValue = NULL;
		}
		$this->alarm_type_id->ViewCustomAttributes = "";

		// alarm_user_id
		$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
		if (strval($this->alarm_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->alarm_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
			}
		} else {
			$this->alarm_user_id->ViewValue = NULL;
		}
		$this->alarm_user_id->ViewCustomAttributes = "";

		// alarm_product_id
		if (strval($this->alarm_product_id->CurrentValue) <> "") {
			$sFilterWrk = "telecare_product.product_id" . ew_SearchString("=", $this->alarm_product_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_product_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->CurrentValue;
			}
		} else {
			$this->alarm_product_id->ViewValue = NULL;
		}
		$this->alarm_product_id->ViewCustomAttributes = "";

		// alarm_datetime
		$this->alarm_datetime->ViewValue = $this->alarm_datetime->CurrentValue;
		$this->alarm_datetime->ViewValue = ew_FormatDateTime($this->alarm_datetime->ViewValue, 11);
		$this->alarm_datetime->ViewCustomAttributes = "";

		// alarm_latitude
		$this->alarm_latitude->ViewValue = $this->alarm_latitude->CurrentValue;
		$this->alarm_latitude->ViewCustomAttributes = "";

		// alarm_longitude
		$this->alarm_longitude->ViewValue = $this->alarm_longitude->CurrentValue;
		$this->alarm_longitude->ViewCustomAttributes = "";

		// alarm_start_on
		$this->alarm_start_on->ViewValue = $this->alarm_start_on->CurrentValue;
		$this->alarm_start_on->ViewValue = ew_FormatDateTime($this->alarm_start_on->ViewValue, 11);
		$this->alarm_start_on->ViewCustomAttributes = "";

		// alarm_close_on
		$this->alarm_close_on->ViewValue = $this->alarm_close_on->CurrentValue;
		$this->alarm_close_on->ViewValue = ew_FormatDateTime($this->alarm_close_on->ViewValue, 11);
		$this->alarm_close_on->ViewCustomAttributes = "";

		// alarm_close
		if (strval($this->alarm_close->CurrentValue) <> "") {
			$this->alarm_close->ViewValue = $this->alarm_close->OptionCaption($this->alarm_close->CurrentValue);
		} else {
			$this->alarm_close->ViewValue = NULL;
		}
		$this->alarm_close->ViewCustomAttributes = "";

		// alarm_admin_id
		if (strval($this->alarm_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->alarm_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->CurrentValue;
			}
		} else {
			$this->alarm_admin_id->ViewValue = NULL;
		}
		$this->alarm_admin_id->ViewCustomAttributes = "";

		// alarm_call_parents
		if (strval($this->alarm_call_parents->CurrentValue) <> "") {
			$this->alarm_call_parents->ViewValue = $this->alarm_call_parents->OptionCaption($this->alarm_call_parents->CurrentValue);
		} else {
			$this->alarm_call_parents->ViewValue = NULL;
		}
		$this->alarm_call_parents->ViewCustomAttributes = "";

		// alarm_call_emergency
		if (strval($this->alarm_call_emergency->CurrentValue) <> "") {
			$this->alarm_call_emergency->ViewValue = $this->alarm_call_emergency->OptionCaption($this->alarm_call_emergency->CurrentValue);
		} else {
			$this->alarm_call_emergency->ViewValue = NULL;
		}
		$this->alarm_call_emergency->ViewCustomAttributes = "";

		// alarm_last_update
		$this->alarm_last_update->ViewValue = $this->alarm_last_update->CurrentValue;
		$this->alarm_last_update->ViewValue = ew_FormatDateTime($this->alarm_last_update->ViewValue, 7);
		$this->alarm_last_update->ViewCustomAttributes = "";

			// alarm_id
			$this->alarm_id->LinkCustomAttributes = "";
			$this->alarm_id->HrefValue = "";
			$this->alarm_id->TooltipValue = "";

			// alarm_type_id
			$this->alarm_type_id->LinkCustomAttributes = "";
			$this->alarm_type_id->HrefValue = "";
			$this->alarm_type_id->TooltipValue = "";

			// alarm_user_id
			$this->alarm_user_id->LinkCustomAttributes = "";
			$this->alarm_user_id->HrefValue = "";
			$this->alarm_user_id->TooltipValue = "";

			// alarm_product_id
			$this->alarm_product_id->LinkCustomAttributes = "";
			$this->alarm_product_id->HrefValue = "";
			$this->alarm_product_id->TooltipValue = "";

			// alarm_datetime
			$this->alarm_datetime->LinkCustomAttributes = "";
			$this->alarm_datetime->HrefValue = "";
			$this->alarm_datetime->TooltipValue = "";

			// alarm_latitude
			$this->alarm_latitude->LinkCustomAttributes = "";
			$this->alarm_latitude->HrefValue = "";
			$this->alarm_latitude->TooltipValue = "";

			// alarm_start_on
			$this->alarm_start_on->LinkCustomAttributes = "";
			$this->alarm_start_on->HrefValue = "";
			$this->alarm_start_on->TooltipValue = "";

			// alarm_close_on
			$this->alarm_close_on->LinkCustomAttributes = "";
			$this->alarm_close_on->HrefValue = "";
			$this->alarm_close_on->TooltipValue = "";

			// alarm_close
			$this->alarm_close->LinkCustomAttributes = "";
			$this->alarm_close->HrefValue = "";
			$this->alarm_close->TooltipValue = "";

			// alarm_call_parents
			$this->alarm_call_parents->LinkCustomAttributes = "";
			$this->alarm_call_parents->HrefValue = "";
			$this->alarm_call_parents->TooltipValue = "";

			// alarm_call_emergency
			$this->alarm_call_emergency->LinkCustomAttributes = "";
			$this->alarm_call_emergency->HrefValue = "";
			$this->alarm_call_emergency->TooltipValue = "";

			// alarm_last_update
			$this->alarm_last_update->LinkCustomAttributes = "";
			$this->alarm_last_update->HrefValue = "";
			$this->alarm_last_update->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['alarm_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setQueryStringValue($_GET["fk_user_id"]);
					$this->alarm_user_id->setQueryStringValue($GLOBALS["telecare_user"]->user_id->QueryStringValue);
					$this->alarm_user_id->setSessionValue($this->alarm_user_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setFormValue($_POST["fk_user_id"]);
					$this->alarm_user_id->setFormValue($GLOBALS["telecare_user"]->user_id->FormValue);
					$this->alarm_user_id->setSessionValue($this->alarm_user_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "telecare_user") {
				if ($this->alarm_user_id->QueryStringValue == "") $this->alarm_user_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_alarmlist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_alarm_delete)) $telecare_alarm_delete = new ctelecare_alarm_delete();

// Page init
$telecare_alarm_delete->Page_Init();

// Page main
$telecare_alarm_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_alarm_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftelecare_alarmdelete = new ew_Form("ftelecare_alarmdelete", "delete");

// Form_CustomValidate event
ftelecare_alarmdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_alarmdelete.ValidateRequired = true;
<?php } else { ?>
ftelecare_alarmdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_alarmdelete.Lists["x_alarm_type_id"] = {"LinkField":"x_alarm_type_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_alarm_type_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmdelete.Lists["x_alarm_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmdelete.Lists["x_alarm_product_id"] = {"LinkField":"x_product_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_product_type_name","x_product_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmdelete.Lists["x_alarm_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmdelete.Lists["x_alarm_close"].Options = <?php echo json_encode($telecare_alarm->alarm_close->Options()) ?>;
ftelecare_alarmdelete.Lists["x_alarm_call_parents"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmdelete.Lists["x_alarm_call_parents"].Options = <?php echo json_encode($telecare_alarm->alarm_call_parents->Options()) ?>;
ftelecare_alarmdelete.Lists["x_alarm_call_emergency"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmdelete.Lists["x_alarm_call_emergency"].Options = <?php echo json_encode($telecare_alarm->alarm_call_emergency->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($telecare_alarm_delete->Recordset = $telecare_alarm_delete->LoadRecordset())
	$telecare_alarm_deleteTotalRecs = $telecare_alarm_delete->Recordset->RecordCount(); // Get record count
if ($telecare_alarm_deleteTotalRecs <= 0) { // No record found, exit
	if ($telecare_alarm_delete->Recordset)
		$telecare_alarm_delete->Recordset->Close();
	$telecare_alarm_delete->Page_Terminate("telecare_alarmlist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_alarm_delete->ShowPageHeader(); ?>
<?php
$telecare_alarm_delete->ShowMessage();
?>
<form name="ftelecare_alarmdelete" id="ftelecare_alarmdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_alarm_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_alarm_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_alarm">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($telecare_alarm_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $telecare_alarm->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($telecare_alarm->alarm_id->Visible) { // alarm_id ?>
		<th><span id="elh_telecare_alarm_alarm_id" class="telecare_alarm_alarm_id"><?php echo $telecare_alarm->alarm_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
		<th><span id="elh_telecare_alarm_alarm_type_id" class="telecare_alarm_alarm_type_id"><?php echo $telecare_alarm->alarm_type_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
		<th><span id="elh_telecare_alarm_alarm_user_id" class="telecare_alarm_alarm_user_id"><?php echo $telecare_alarm->alarm_user_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
		<th><span id="elh_telecare_alarm_alarm_product_id" class="telecare_alarm_alarm_product_id"><?php echo $telecare_alarm->alarm_product_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
		<th><span id="elh_telecare_alarm_alarm_datetime" class="telecare_alarm_alarm_datetime"><?php echo $telecare_alarm->alarm_datetime->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_latitude->Visible) { // alarm_latitude ?>
		<th><span id="elh_telecare_alarm_alarm_latitude" class="telecare_alarm_alarm_latitude"><?php echo $telecare_alarm->alarm_latitude->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
		<th><span id="elh_telecare_alarm_alarm_start_on" class="telecare_alarm_alarm_start_on"><?php echo $telecare_alarm->alarm_start_on->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_close_on->Visible) { // alarm_close_on ?>
		<th><span id="elh_telecare_alarm_alarm_close_on" class="telecare_alarm_alarm_close_on"><?php echo $telecare_alarm->alarm_close_on->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
		<th><span id="elh_telecare_alarm_alarm_close" class="telecare_alarm_alarm_close"><?php echo $telecare_alarm->alarm_close->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
		<th><span id="elh_telecare_alarm_alarm_call_parents" class="telecare_alarm_alarm_call_parents"><?php echo $telecare_alarm->alarm_call_parents->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
		<th><span id="elh_telecare_alarm_alarm_call_emergency" class="telecare_alarm_alarm_call_emergency"><?php echo $telecare_alarm->alarm_call_emergency->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_alarm->alarm_last_update->Visible) { // alarm_last_update ?>
		<th><span id="elh_telecare_alarm_alarm_last_update" class="telecare_alarm_alarm_last_update"><?php echo $telecare_alarm->alarm_last_update->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$telecare_alarm_delete->RecCnt = 0;
$i = 0;
while (!$telecare_alarm_delete->Recordset->EOF) {
	$telecare_alarm_delete->RecCnt++;
	$telecare_alarm_delete->RowCnt++;

	// Set row properties
	$telecare_alarm->ResetAttrs();
	$telecare_alarm->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$telecare_alarm_delete->LoadRowValues($telecare_alarm_delete->Recordset);

	// Render row
	$telecare_alarm_delete->RenderRow();
?>
	<tr<?php echo $telecare_alarm->RowAttributes() ?>>
<?php if ($telecare_alarm->alarm_id->Visible) { // alarm_id ?>
		<td<?php echo $telecare_alarm->alarm_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_id" class="telecare_alarm_alarm_id">
<span<?php echo $telecare_alarm->alarm_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
		<td<?php echo $telecare_alarm->alarm_type_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_type_id" class="telecare_alarm_alarm_type_id">
<span<?php echo $telecare_alarm->alarm_type_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_type_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
		<td<?php echo $telecare_alarm->alarm_user_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_user_id" class="telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_user_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
		<td<?php echo $telecare_alarm->alarm_product_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_product_id" class="telecare_alarm_alarm_product_id">
<span<?php echo $telecare_alarm->alarm_product_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_product_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
		<td<?php echo $telecare_alarm->alarm_datetime->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_datetime" class="telecare_alarm_alarm_datetime">
<span<?php echo $telecare_alarm->alarm_datetime->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_datetime->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_latitude->Visible) { // alarm_latitude ?>
		<td<?php echo $telecare_alarm->alarm_latitude->CellAttributes() ?>>
<div id="orig<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_latitude" class="hide">
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_latitude" class="telecare_alarm_alarm_latitude">
<span<?php echo $telecare_alarm->alarm_latitude->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_latitude->ListViewValue() ?></span>
</span>
</div>
<div id="gmap_x<?php echo $telecare_alarm_delete->RowCnt ?>_alarm_latitude" class="ewGoogleMap" style="width: 200px; height: 200px;"></div>
<script type="text/javascript">
ewGoogleMaps[ewGoogleMaps.length] = ewGoogleMaps["gmap_x<?php echo $telecare_alarm_delete->RowCnt ?>_alarm_latitude"] = jQuery.extend({"id":"gmap_x<?php echo $telecare_alarm_delete->RowCnt ?>_alarm_latitude","width":200,"height":200,"latitude":null,"longitude":null,"address":null,"type":"ROADMAP","zoom":8,"title":null,"icon":null,"use_single_map":false,"single_map_width":400,"single_map_height":400,"show_map_on_top":true,"show_all_markers":true,"description":null}, {
	latitude: <?php echo ew_VarToJson($telecare_alarm->alarm_latitude->CurrentValue, "undefined") ?>,
	longitude: <?php echo ew_VarToJson($telecare_alarm->alarm_longitude->CurrentValue, "undefined") ?>
});
</script>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
		<td<?php echo $telecare_alarm->alarm_start_on->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_start_on" class="telecare_alarm_alarm_start_on">
<span<?php echo $telecare_alarm->alarm_start_on->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_start_on->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_close_on->Visible) { // alarm_close_on ?>
		<td<?php echo $telecare_alarm->alarm_close_on->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_close_on" class="telecare_alarm_alarm_close_on">
<span<?php echo $telecare_alarm->alarm_close_on->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_close_on->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
		<td<?php echo $telecare_alarm->alarm_close->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_close" class="telecare_alarm_alarm_close">
<span<?php echo $telecare_alarm->alarm_close->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_close->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
		<td<?php echo $telecare_alarm->alarm_call_parents->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_call_parents" class="telecare_alarm_alarm_call_parents">
<span<?php echo $telecare_alarm->alarm_call_parents->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_call_parents->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
		<td<?php echo $telecare_alarm->alarm_call_emergency->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_call_emergency" class="telecare_alarm_alarm_call_emergency">
<span<?php echo $telecare_alarm->alarm_call_emergency->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_call_emergency->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_alarm->alarm_last_update->Visible) { // alarm_last_update ?>
		<td<?php echo $telecare_alarm->alarm_last_update->CellAttributes() ?>>
<span id="el<?php echo $telecare_alarm_delete->RowCnt ?>_telecare_alarm_alarm_last_update" class="telecare_alarm_alarm_last_update">
<span<?php echo $telecare_alarm->alarm_last_update->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_last_update->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$telecare_alarm_delete->Recordset->MoveNext();
}
$telecare_alarm_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_alarm_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftelecare_alarmdelete.Init();
</script>
<?php
$telecare_alarm_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_alarm_delete->Page_Terminate();
?>
