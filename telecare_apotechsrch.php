<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_apotechinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_apotech_search = NULL; // Initialize page object first

class ctelecare_apotech_search extends ctelecare_apotech {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_apotech';

	// Page object name
	var $PageObjName = 'telecare_apotech_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_apotech)
		if (!isset($GLOBALS["telecare_apotech"]) || get_class($GLOBALS["telecare_apotech"]) == "ctelecare_apotech") {
			$GLOBALS["telecare_apotech"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_apotech"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_apotech', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_apotechlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->apotech_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_apotech;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_apotech);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";
	var $MultiPages; // Multi pages object

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "telecare_apotechlist.php" . "?" . $sSrchStr;
						if ($this->IsModal) {
							$row = array();
							$row["url"] = $sSrchStr;
							echo ew_ArrayToJson(array($row));
							$this->Page_Terminate();
							exit();
						} else {
							$this->Page_Terminate($sSrchStr); // Go to list page
						}
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_id); // apotech_id
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_admin_id); // apotech_admin_id
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_surname); // apotech_surname
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_name); // apotech_name
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_email); // apotech_email
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_mobile); // apotech_mobile
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_phone); // apotech_phone
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_is_active); // apotech_is_active
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_last_update); // apotech_last_update
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_name); // apotech_company_name
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_logo_file); // apotech_company_logo_file
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_address); // apotech_company_address
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_phone); // apotech_company_phone
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_email); // apotech_company_email
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_web); // apotech_company_web
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_latitude); // apotech_latitude
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_longitude); // apotech_longitude
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_region_id); // apotech_company_region_id
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_province_id); // apotech_company_province_id
		$this->BuildSearchUrl($sSrchUrl, $this->apotech_company_city_id); // apotech_company_city_id
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// apotech_id

		$this->apotech_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_id"));
		$this->apotech_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_id");

		// apotech_admin_id
		$this->apotech_admin_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_admin_id"));
		$this->apotech_admin_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_admin_id");

		// apotech_surname
		$this->apotech_surname->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_surname"));
		$this->apotech_surname->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_surname");

		// apotech_name
		$this->apotech_name->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_name"));
		$this->apotech_name->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_name");

		// apotech_email
		$this->apotech_email->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_email"));
		$this->apotech_email->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_email");

		// apotech_mobile
		$this->apotech_mobile->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_mobile"));
		$this->apotech_mobile->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_mobile");

		// apotech_phone
		$this->apotech_phone->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_phone"));
		$this->apotech_phone->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_phone");

		// apotech_is_active
		$this->apotech_is_active->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_is_active"));
		$this->apotech_is_active->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_is_active");

		// apotech_last_update
		$this->apotech_last_update->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_last_update"));
		$this->apotech_last_update->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_last_update");

		// apotech_company_name
		$this->apotech_company_name->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_name"));
		$this->apotech_company_name->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_name");

		// apotech_company_logo_file
		$this->apotech_company_logo_file->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_logo_file"));
		$this->apotech_company_logo_file->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_logo_file");

		// apotech_company_address
		$this->apotech_company_address->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_address"));
		$this->apotech_company_address->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_address");

		// apotech_company_phone
		$this->apotech_company_phone->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_phone"));
		$this->apotech_company_phone->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_phone");

		// apotech_company_email
		$this->apotech_company_email->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_email"));
		$this->apotech_company_email->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_email");

		// apotech_company_web
		$this->apotech_company_web->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_web"));
		$this->apotech_company_web->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_web");

		// apotech_latitude
		$this->apotech_latitude->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_latitude"));
		$this->apotech_latitude->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_latitude");

		// apotech_longitude
		$this->apotech_longitude->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_longitude"));
		$this->apotech_longitude->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_longitude");

		// apotech_company_region_id
		$this->apotech_company_region_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_region_id"));
		$this->apotech_company_region_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_region_id");

		// apotech_company_province_id
		$this->apotech_company_province_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_province_id"));
		$this->apotech_company_province_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_province_id");

		// apotech_company_city_id
		$this->apotech_company_city_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_apotech_company_city_id"));
		$this->apotech_company_city_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_apotech_company_city_id");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// apotech_id
		// apotech_admin_id
		// apotech_surname
		// apotech_name
		// apotech_email
		// apotech_mobile
		// apotech_phone
		// apotech_is_active
		// apotech_last_update
		// apotech_company_name
		// apotech_company_logo_file
		// apotech_company_logo_width
		// apotech_company_logo_height
		// apotech_company_logo_size
		// apotech_company_address
		// apotech_company_phone
		// apotech_company_email
		// apotech_company_web
		// apotech_latitude
		// apotech_longitude
		// apotech_company_region_id
		// apotech_company_province_id
		// apotech_company_city_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// apotech_id
		$this->apotech_id->ViewValue = $this->apotech_id->CurrentValue;
		$this->apotech_id->ViewCustomAttributes = "";

		// apotech_admin_id
		if (strval($this->apotech_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->apotech_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->CurrentValue;
			}
		} else {
			$this->apotech_admin_id->ViewValue = NULL;
		}
		$this->apotech_admin_id->ViewCustomAttributes = "";

		// apotech_surname
		$this->apotech_surname->ViewValue = $this->apotech_surname->CurrentValue;
		$this->apotech_surname->ViewCustomAttributes = "";

		// apotech_name
		$this->apotech_name->ViewValue = $this->apotech_name->CurrentValue;
		$this->apotech_name->ViewCustomAttributes = "";

		// apotech_email
		$this->apotech_email->ViewValue = $this->apotech_email->CurrentValue;
		$this->apotech_email->ViewCustomAttributes = "";

		// apotech_mobile
		$this->apotech_mobile->ViewValue = $this->apotech_mobile->CurrentValue;
		$this->apotech_mobile->ViewCustomAttributes = "";

		// apotech_phone
		$this->apotech_phone->ViewValue = $this->apotech_phone->CurrentValue;
		$this->apotech_phone->ViewCustomAttributes = "";

		// apotech_is_active
		if (strval($this->apotech_is_active->CurrentValue) <> "") {
			$this->apotech_is_active->ViewValue = $this->apotech_is_active->OptionCaption($this->apotech_is_active->CurrentValue);
		} else {
			$this->apotech_is_active->ViewValue = NULL;
		}
		$this->apotech_is_active->ViewCustomAttributes = "";

		// apotech_last_update
		$this->apotech_last_update->ViewValue = $this->apotech_last_update->CurrentValue;
		$this->apotech_last_update->ViewValue = ew_FormatDateTime($this->apotech_last_update->ViewValue, 7);
		$this->apotech_last_update->ViewCustomAttributes = "";

		// apotech_company_name
		$this->apotech_company_name->ViewValue = $this->apotech_company_name->CurrentValue;
		$this->apotech_company_name->ViewCustomAttributes = "";

		// apotech_company_logo_file
		if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
			$this->apotech_company_logo_file->ImageWidth = 100;
			$this->apotech_company_logo_file->ImageHeight = 0;
			$this->apotech_company_logo_file->ImageAlt = $this->apotech_company_logo_file->FldAlt();
			$this->apotech_company_logo_file->ViewValue = $this->apotech_company_logo_file->Upload->DbValue;
		} else {
			$this->apotech_company_logo_file->ViewValue = "";
		}
		$this->apotech_company_logo_file->ViewCustomAttributes = "";

		// apotech_company_logo_width
		$this->apotech_company_logo_width->ViewValue = $this->apotech_company_logo_width->CurrentValue;
		$this->apotech_company_logo_width->ViewCustomAttributes = "";

		// apotech_company_logo_height
		$this->apotech_company_logo_height->ViewValue = $this->apotech_company_logo_height->CurrentValue;
		$this->apotech_company_logo_height->ViewCustomAttributes = "";

		// apotech_company_logo_size
		$this->apotech_company_logo_size->ViewValue = $this->apotech_company_logo_size->CurrentValue;
		$this->apotech_company_logo_size->ViewCustomAttributes = "";

		// apotech_company_address
		$this->apotech_company_address->ViewValue = $this->apotech_company_address->CurrentValue;
		$this->apotech_company_address->ViewCustomAttributes = "";

		// apotech_company_phone
		$this->apotech_company_phone->ViewValue = $this->apotech_company_phone->CurrentValue;
		$this->apotech_company_phone->ViewCustomAttributes = "";

		// apotech_company_email
		$this->apotech_company_email->ViewValue = $this->apotech_company_email->CurrentValue;
		$this->apotech_company_email->ViewCustomAttributes = "";

		// apotech_company_web
		$this->apotech_company_web->ViewValue = $this->apotech_company_web->CurrentValue;
		$this->apotech_company_web->ViewCustomAttributes = "";

		// apotech_latitude
		$this->apotech_latitude->ViewValue = $this->apotech_latitude->CurrentValue;
		$this->apotech_latitude->ViewCustomAttributes = "";

		// apotech_longitude
		$this->apotech_longitude->ViewValue = $this->apotech_longitude->CurrentValue;
		$this->apotech_longitude->ViewCustomAttributes = "";

		// apotech_company_region_id
		if (strval($this->apotech_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->apotech_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->CurrentValue;
			}
		} else {
			$this->apotech_company_region_id->ViewValue = NULL;
		}
		$this->apotech_company_region_id->ViewCustomAttributes = "";

		// apotech_company_province_id
		if (strval($this->apotech_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->apotech_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->CurrentValue;
			}
		} else {
			$this->apotech_company_province_id->ViewValue = NULL;
		}
		$this->apotech_company_province_id->ViewCustomAttributes = "";

		// apotech_company_city_id
		if (strval($this->apotech_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->apotech_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->CurrentValue;
			}
		} else {
			$this->apotech_company_city_id->ViewValue = NULL;
		}
		$this->apotech_company_city_id->ViewCustomAttributes = "";

			// apotech_id
			$this->apotech_id->LinkCustomAttributes = "";
			$this->apotech_id->HrefValue = "";
			$this->apotech_id->TooltipValue = "";

			// apotech_admin_id
			$this->apotech_admin_id->LinkCustomAttributes = "";
			$this->apotech_admin_id->HrefValue = "";
			$this->apotech_admin_id->TooltipValue = "";

			// apotech_surname
			$this->apotech_surname->LinkCustomAttributes = "";
			$this->apotech_surname->HrefValue = "";
			$this->apotech_surname->TooltipValue = "";

			// apotech_name
			$this->apotech_name->LinkCustomAttributes = "";
			$this->apotech_name->HrefValue = "";
			$this->apotech_name->TooltipValue = "";

			// apotech_email
			$this->apotech_email->LinkCustomAttributes = "";
			$this->apotech_email->HrefValue = "";
			$this->apotech_email->TooltipValue = "";

			// apotech_mobile
			$this->apotech_mobile->LinkCustomAttributes = "";
			$this->apotech_mobile->HrefValue = "";
			$this->apotech_mobile->TooltipValue = "";

			// apotech_phone
			$this->apotech_phone->LinkCustomAttributes = "";
			$this->apotech_phone->HrefValue = "";
			$this->apotech_phone->TooltipValue = "";

			// apotech_is_active
			$this->apotech_is_active->LinkCustomAttributes = "";
			$this->apotech_is_active->HrefValue = "";
			$this->apotech_is_active->TooltipValue = "";

			// apotech_last_update
			$this->apotech_last_update->LinkCustomAttributes = "";
			$this->apotech_last_update->HrefValue = "";
			$this->apotech_last_update->TooltipValue = "";

			// apotech_company_name
			$this->apotech_company_name->LinkCustomAttributes = "";
			$this->apotech_company_name->HrefValue = "";
			$this->apotech_company_name->TooltipValue = "";

			// apotech_company_logo_file
			$this->apotech_company_logo_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
				$this->apotech_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->apotech_company_logo_file, $this->apotech_company_logo_file->Upload->DbValue); // Add prefix/suffix
				$this->apotech_company_logo_file->LinkAttrs["target"] = ""; // Add target
				if ($this->Export <> "") $this->apotech_company_logo_file->HrefValue = ew_ConvertFullUrl($this->apotech_company_logo_file->HrefValue);
			} else {
				$this->apotech_company_logo_file->HrefValue = "";
			}
			$this->apotech_company_logo_file->HrefValue2 = $this->apotech_company_logo_file->UploadPath . $this->apotech_company_logo_file->Upload->DbValue;
			$this->apotech_company_logo_file->TooltipValue = "";
			if ($this->apotech_company_logo_file->UseColorbox) {
				$this->apotech_company_logo_file->LinkAttrs["title"] = $Language->Phrase("ViewImageGallery");
				$this->apotech_company_logo_file->LinkAttrs["data-rel"] = "telecare_apotech_x_apotech_company_logo_file";

				//$this->apotech_company_logo_file->LinkAttrs["class"] = "ewLightbox ewTooltip img-thumbnail";
				//$this->apotech_company_logo_file->LinkAttrs["data-placement"] = "bottom";
				//$this->apotech_company_logo_file->LinkAttrs["data-container"] = "body";

				$this->apotech_company_logo_file->LinkAttrs["class"] = "ewLightbox img-thumbnail";
			}

			// apotech_company_address
			$this->apotech_company_address->LinkCustomAttributes = "";
			$this->apotech_company_address->HrefValue = "";
			$this->apotech_company_address->TooltipValue = "";

			// apotech_company_phone
			$this->apotech_company_phone->LinkCustomAttributes = "";
			$this->apotech_company_phone->HrefValue = "";
			$this->apotech_company_phone->TooltipValue = "";

			// apotech_company_email
			$this->apotech_company_email->LinkCustomAttributes = "";
			$this->apotech_company_email->HrefValue = "";
			$this->apotech_company_email->TooltipValue = "";

			// apotech_company_web
			$this->apotech_company_web->LinkCustomAttributes = "";
			$this->apotech_company_web->HrefValue = "";
			$this->apotech_company_web->TooltipValue = "";

			// apotech_latitude
			$this->apotech_latitude->LinkCustomAttributes = "";
			$this->apotech_latitude->HrefValue = "";
			$this->apotech_latitude->TooltipValue = "";

			// apotech_longitude
			$this->apotech_longitude->LinkCustomAttributes = "";
			$this->apotech_longitude->HrefValue = "";
			$this->apotech_longitude->TooltipValue = "";

			// apotech_company_region_id
			$this->apotech_company_region_id->LinkCustomAttributes = "";
			$this->apotech_company_region_id->HrefValue = "";
			$this->apotech_company_region_id->TooltipValue = "";

			// apotech_company_province_id
			$this->apotech_company_province_id->LinkCustomAttributes = "";
			$this->apotech_company_province_id->HrefValue = "";
			$this->apotech_company_province_id->TooltipValue = "";

			// apotech_company_city_id
			$this->apotech_company_city_id->LinkCustomAttributes = "";
			$this->apotech_company_city_id->HrefValue = "";
			$this->apotech_company_city_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// apotech_id
			$this->apotech_id->EditAttrs["class"] = "form-control";
			$this->apotech_id->EditCustomAttributes = "";
			$this->apotech_id->EditValue = ew_HtmlEncode($this->apotech_id->AdvancedSearch->SearchValue);
			$this->apotech_id->PlaceHolder = ew_RemoveHtml($this->apotech_id->FldCaption());

			// apotech_admin_id
			$this->apotech_admin_id->EditAttrs["class"] = "form-control";
			$this->apotech_admin_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_admin_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->apotech_admin_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_apotech"]->UserIDAllow("search")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_admin_id->EditValue = $arwrk;

			// apotech_surname
			$this->apotech_surname->EditAttrs["class"] = "form-control";
			$this->apotech_surname->EditCustomAttributes = "";
			$this->apotech_surname->EditValue = ew_HtmlEncode($this->apotech_surname->AdvancedSearch->SearchValue);
			$this->apotech_surname->PlaceHolder = ew_RemoveHtml($this->apotech_surname->FldCaption());

			// apotech_name
			$this->apotech_name->EditAttrs["class"] = "form-control";
			$this->apotech_name->EditCustomAttributes = "";
			$this->apotech_name->EditValue = ew_HtmlEncode($this->apotech_name->AdvancedSearch->SearchValue);
			$this->apotech_name->PlaceHolder = ew_RemoveHtml($this->apotech_name->FldCaption());

			// apotech_email
			$this->apotech_email->EditAttrs["class"] = "form-control";
			$this->apotech_email->EditCustomAttributes = "";
			$this->apotech_email->EditValue = ew_HtmlEncode($this->apotech_email->AdvancedSearch->SearchValue);
			$this->apotech_email->PlaceHolder = ew_RemoveHtml($this->apotech_email->FldCaption());

			// apotech_mobile
			$this->apotech_mobile->EditAttrs["class"] = "form-control";
			$this->apotech_mobile->EditCustomAttributes = "";
			$this->apotech_mobile->EditValue = ew_HtmlEncode($this->apotech_mobile->AdvancedSearch->SearchValue);
			$this->apotech_mobile->PlaceHolder = ew_RemoveHtml($this->apotech_mobile->FldCaption());

			// apotech_phone
			$this->apotech_phone->EditAttrs["class"] = "form-control";
			$this->apotech_phone->EditCustomAttributes = "";
			$this->apotech_phone->EditValue = ew_HtmlEncode($this->apotech_phone->AdvancedSearch->SearchValue);
			$this->apotech_phone->PlaceHolder = ew_RemoveHtml($this->apotech_phone->FldCaption());

			// apotech_is_active
			$this->apotech_is_active->EditCustomAttributes = "";
			$this->apotech_is_active->EditValue = $this->apotech_is_active->Options(FALSE);

			// apotech_last_update
			$this->apotech_last_update->EditAttrs["class"] = "form-control";
			$this->apotech_last_update->EditCustomAttributes = "";
			$this->apotech_last_update->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->apotech_last_update->AdvancedSearch->SearchValue, 7), 7));
			$this->apotech_last_update->PlaceHolder = ew_RemoveHtml($this->apotech_last_update->FldCaption());

			// apotech_company_name
			$this->apotech_company_name->EditAttrs["class"] = "form-control";
			$this->apotech_company_name->EditCustomAttributes = "";
			$this->apotech_company_name->EditValue = ew_HtmlEncode($this->apotech_company_name->AdvancedSearch->SearchValue);
			$this->apotech_company_name->PlaceHolder = ew_RemoveHtml($this->apotech_company_name->FldCaption());

			// apotech_company_logo_file
			$this->apotech_company_logo_file->EditAttrs["class"] = "form-control";
			$this->apotech_company_logo_file->EditCustomAttributes = "";
			$this->apotech_company_logo_file->EditValue = ew_HtmlEncode($this->apotech_company_logo_file->AdvancedSearch->SearchValue);
			$this->apotech_company_logo_file->PlaceHolder = ew_RemoveHtml($this->apotech_company_logo_file->FldCaption());

			// apotech_company_address
			$this->apotech_company_address->EditAttrs["class"] = "form-control";
			$this->apotech_company_address->EditCustomAttributes = "";
			$this->apotech_company_address->EditValue = ew_HtmlEncode($this->apotech_company_address->AdvancedSearch->SearchValue);
			$this->apotech_company_address->PlaceHolder = ew_RemoveHtml($this->apotech_company_address->FldCaption());

			// apotech_company_phone
			$this->apotech_company_phone->EditAttrs["class"] = "form-control";
			$this->apotech_company_phone->EditCustomAttributes = "";
			$this->apotech_company_phone->EditValue = ew_HtmlEncode($this->apotech_company_phone->AdvancedSearch->SearchValue);
			$this->apotech_company_phone->PlaceHolder = ew_RemoveHtml($this->apotech_company_phone->FldCaption());

			// apotech_company_email
			$this->apotech_company_email->EditAttrs["class"] = "form-control";
			$this->apotech_company_email->EditCustomAttributes = "";
			$this->apotech_company_email->EditValue = ew_HtmlEncode($this->apotech_company_email->AdvancedSearch->SearchValue);
			$this->apotech_company_email->PlaceHolder = ew_RemoveHtml($this->apotech_company_email->FldCaption());

			// apotech_company_web
			$this->apotech_company_web->EditAttrs["class"] = "form-control";
			$this->apotech_company_web->EditCustomAttributes = "";
			$this->apotech_company_web->EditValue = ew_HtmlEncode($this->apotech_company_web->AdvancedSearch->SearchValue);
			$this->apotech_company_web->PlaceHolder = ew_RemoveHtml($this->apotech_company_web->FldCaption());

			// apotech_latitude
			$this->apotech_latitude->EditAttrs["class"] = "form-control";
			$this->apotech_latitude->EditCustomAttributes = "";
			$this->apotech_latitude->EditValue = ew_HtmlEncode($this->apotech_latitude->AdvancedSearch->SearchValue);
			$this->apotech_latitude->PlaceHolder = ew_RemoveHtml($this->apotech_latitude->FldCaption());

			// apotech_longitude
			$this->apotech_longitude->EditAttrs["class"] = "form-control";
			$this->apotech_longitude->EditCustomAttributes = "";
			$this->apotech_longitude->EditValue = ew_HtmlEncode($this->apotech_longitude->AdvancedSearch->SearchValue);
			$this->apotech_longitude->PlaceHolder = ew_RemoveHtml($this->apotech_longitude->FldCaption());

			// apotech_company_region_id
			$this->apotech_company_region_id->EditAttrs["class"] = "form-control";
			$this->apotech_company_region_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_company_region_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->apotech_company_region_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_company_region_id->EditValue = $arwrk;

			// apotech_company_province_id
			$this->apotech_company_province_id->EditAttrs["class"] = "form-control";
			$this->apotech_company_province_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_company_province_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->apotech_company_province_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_company_province_id->EditValue = $arwrk;

			// apotech_company_city_id
			$this->apotech_company_city_id->EditAttrs["class"] = "form-control";
			$this->apotech_company_city_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_company_city_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->apotech_company_city_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_company_city_id->EditValue = $arwrk;
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->apotech_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->apotech_id->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->apotech_last_update->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->apotech_last_update->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->apotech_id->AdvancedSearch->Load();
		$this->apotech_admin_id->AdvancedSearch->Load();
		$this->apotech_surname->AdvancedSearch->Load();
		$this->apotech_name->AdvancedSearch->Load();
		$this->apotech_email->AdvancedSearch->Load();
		$this->apotech_mobile->AdvancedSearch->Load();
		$this->apotech_phone->AdvancedSearch->Load();
		$this->apotech_is_active->AdvancedSearch->Load();
		$this->apotech_last_update->AdvancedSearch->Load();
		$this->apotech_company_name->AdvancedSearch->Load();
		$this->apotech_company_logo_file->AdvancedSearch->Load();
		$this->apotech_company_address->AdvancedSearch->Load();
		$this->apotech_company_phone->AdvancedSearch->Load();
		$this->apotech_company_email->AdvancedSearch->Load();
		$this->apotech_company_web->AdvancedSearch->Load();
		$this->apotech_latitude->AdvancedSearch->Load();
		$this->apotech_longitude->AdvancedSearch->Load();
		$this->apotech_company_region_id->AdvancedSearch->Load();
		$this->apotech_company_province_id->AdvancedSearch->Load();
		$this->apotech_company_city_id->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_apotechlist.php", "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$this->MultiPages = $pages;
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_apotech_search)) $telecare_apotech_search = new ctelecare_apotech_search();

// Page init
$telecare_apotech_search->Page_Init();

// Page main
$telecare_apotech_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_apotech_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($telecare_apotech_search->IsModal) { ?>
var CurrentAdvancedSearchForm = ftelecare_apotechsearch = new ew_Form("ftelecare_apotechsearch", "search");
<?php } else { ?>
var CurrentForm = ftelecare_apotechsearch = new ew_Form("ftelecare_apotechsearch", "search");
<?php } ?>

// Form_CustomValidate event
ftelecare_apotechsearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_apotechsearch.ValidateRequired = true;
<?php } else { ?>
ftelecare_apotechsearch.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_apotechsearch.MultiPage = new ew_MultiPage("ftelecare_apotechsearch");

// Dynamic selection lists
ftelecare_apotechsearch.Lists["x_apotech_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_apotechsearch.Lists["x_apotech_is_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_apotechsearch.Lists["x_apotech_is_active"].Options = <?php echo json_encode($telecare_apotech->apotech_is_active->Options()) ?>;
ftelecare_apotechsearch.Lists["x_apotech_company_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_apotech_company_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_apotechsearch.Lists["x_apotech_company_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":["x_apotech_company_region_id"],"ChildFields":["x_apotech_company_city_id"],"FilterFields":["x_province_region_id"],"Options":[],"Template":""};
ftelecare_apotechsearch.Lists["x_apotech_company_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":["x_apotech_company_province_id"],"ChildFields":[],"FilterFields":["x_city_province_id"],"Options":[],"Template":""};

// Form object for search
// Validate function for search

ftelecare_apotechsearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_apotech_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_apotech->apotech_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_apotech_last_update");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_apotech->apotech_last_update->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$telecare_apotech_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $telecare_apotech_search->ShowPageHeader(); ?>
<?php
$telecare_apotech_search->ShowMessage();
?>
<form name="ftelecare_apotechsearch" id="ftelecare_apotechsearch" class="<?php echo $telecare_apotech_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_apotech_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_apotech_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_apotech">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($telecare_apotech_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div class="ewMultiPage">
<div class="tabbable" id="telecare_apotech_search">
	<ul class="nav<?php echo $telecare_apotech_search->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_apotech_search->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_apotech1" data-toggle="tab"><?php echo $telecare_apotech->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_apotech_search->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_apotech2" data-toggle="tab"><?php echo $telecare_apotech->PageCaption(2) ?></a></li>
		<li<?php echo $telecare_apotech_search->MultiPages->TabStyle("3") ?>><a href="#tab_telecare_apotech3" data-toggle="tab"><?php echo $telecare_apotech->PageCaption(3) ?></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane<?php echo $telecare_apotech_search->MultiPages->PageStyle("1") ?>" id="tab_telecare_apotech1">
<div>
<?php if ($telecare_apotech->apotech_id->Visible) { // apotech_id ?>
	<div id="r_apotech_id" class="form-group">
		<label for="x_apotech_id" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_id"><?php echo $telecare_apotech->apotech_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_apotech_id" id="z_apotech_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_id->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_id">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_id" data-page="1" name="x_apotech_id" id="x_apotech_id" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_id->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_id->EditValue ?>"<?php echo $telecare_apotech->apotech_id->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_admin_id->Visible) { // apotech_admin_id ?>
	<div id="r_apotech_admin_id" class="form-group">
		<label for="x_apotech_admin_id" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_admin_id"><?php echo $telecare_apotech->apotech_admin_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_apotech_admin_id" id="z_apotech_admin_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_admin_id->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_admin_id">
<select data-table="telecare_apotech" data-field="x_apotech_admin_id" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_admin_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_admin_id->DisplayValueSeparator) : $telecare_apotech->apotech_admin_id->DisplayValueSeparator) ?>" id="x_apotech_admin_id" name="x_apotech_admin_id"<?php echo $telecare_apotech->apotech_admin_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_admin_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_admin_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_admin_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_admin_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_admin_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_admin_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_admin_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
if (!$GLOBALS["telecare_apotech"]->UserIDAllow("search")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_apotech->apotech_admin_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_admin_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_admin_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_admin_id" id="s_x_apotech_admin_id" value="<?php echo $telecare_apotech->apotech_admin_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_surname->Visible) { // apotech_surname ?>
	<div id="r_apotech_surname" class="form-group">
		<label for="x_apotech_surname" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_surname"><?php echo $telecare_apotech->apotech_surname->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_surname" id="z_apotech_surname" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_surname->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_surname">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_surname" data-page="1" name="x_apotech_surname" id="x_apotech_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_surname->EditValue ?>"<?php echo $telecare_apotech->apotech_surname->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_name->Visible) { // apotech_name ?>
	<div id="r_apotech_name" class="form-group">
		<label for="x_apotech_name" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_name"><?php echo $telecare_apotech->apotech_name->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_name" id="z_apotech_name" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_name->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_name">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_name" data-page="1" name="x_apotech_name" id="x_apotech_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_name->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_name->EditValue ?>"<?php echo $telecare_apotech->apotech_name->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_email->Visible) { // apotech_email ?>
	<div id="r_apotech_email" class="form-group">
		<label for="x_apotech_email" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_email"><?php echo $telecare_apotech->apotech_email->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_email" id="z_apotech_email" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_email->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_email">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_email" data-page="1" name="x_apotech_email" id="x_apotech_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_email->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_email->EditValue ?>"<?php echo $telecare_apotech->apotech_email->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_mobile->Visible) { // apotech_mobile ?>
	<div id="r_apotech_mobile" class="form-group">
		<label for="x_apotech_mobile" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_mobile"><?php echo $telecare_apotech->apotech_mobile->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_mobile" id="z_apotech_mobile" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_mobile->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_mobile">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_mobile" data-page="1" name="x_apotech_mobile" id="x_apotech_mobile" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_mobile->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_mobile->EditValue ?>"<?php echo $telecare_apotech->apotech_mobile->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_phone->Visible) { // apotech_phone ?>
	<div id="r_apotech_phone" class="form-group">
		<label for="x_apotech_phone" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_phone"><?php echo $telecare_apotech->apotech_phone->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_phone" id="z_apotech_phone" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_phone->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_phone">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_phone" data-page="1" name="x_apotech_phone" id="x_apotech_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_phone->EditValue ?>"<?php echo $telecare_apotech->apotech_phone->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_is_active->Visible) { // apotech_is_active ?>
	<div id="r_apotech_is_active" class="form-group">
		<label class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_is_active"><?php echo $telecare_apotech->apotech_is_active->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_is_active" id="z_apotech_is_active" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_is_active->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_is_active">
<div id="tp_x_apotech_is_active" class="ewTemplate"><input type="radio" data-table="telecare_apotech" data-field="x_apotech_is_active" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_is_active->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_is_active->DisplayValueSeparator) : $telecare_apotech->apotech_is_active->DisplayValueSeparator) ?>" name="x_apotech_is_active" id="x_apotech_is_active" value="{value}"<?php echo $telecare_apotech->apotech_is_active->EditAttributes() ?>></div>
<div id="dsl_x_apotech_is_active" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_apotech->apotech_is_active->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_apotech->apotech_is_active->AdvancedSearch->SearchValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_apotech" data-field="x_apotech_is_active" data-page="1" name="x_apotech_is_active" id="x_apotech_is_active_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_apotech->apotech_is_active->EditAttributes() ?>><?php echo $telecare_apotech->apotech_is_active->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_is_active->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_apotech" data-field="x_apotech_is_active" data-page="1" name="x_apotech_is_active" id="x_apotech_is_active_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_is_active->CurrentValue) ?>" checked<?php echo $telecare_apotech->apotech_is_active->EditAttributes() ?>><?php echo $telecare_apotech->apotech_is_active->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_last_update->Visible) { // apotech_last_update ?>
	<div id="r_apotech_last_update" class="form-group">
		<label for="x_apotech_last_update" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_last_update"><?php echo $telecare_apotech->apotech_last_update->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_apotech_last_update" id="z_apotech_last_update" value="="></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_last_update->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_last_update">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_last_update" data-page="1" data-format="7" name="x_apotech_last_update" id="x_apotech_last_update" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_last_update->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_last_update->EditValue ?>"<?php echo $telecare_apotech->apotech_last_update->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_apotech_search->MultiPages->PageStyle("2") ?>" id="tab_telecare_apotech2">
<div>
<?php if ($telecare_apotech->apotech_company_name->Visible) { // apotech_company_name ?>
	<div id="r_apotech_company_name" class="form-group">
		<label for="x_apotech_company_name" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_name"><?php echo $telecare_apotech->apotech_company_name->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_company_name" id="z_apotech_company_name" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_name->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_name">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_name" data-page="2" name="x_apotech_company_name" id="x_apotech_company_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_name->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_name->EditValue ?>"<?php echo $telecare_apotech->apotech_company_name->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_logo_file->Visible) { // apotech_company_logo_file ?>
	<div id="r_apotech_company_logo_file" class="form-group">
		<label class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_logo_file"><?php echo $telecare_apotech->apotech_company_logo_file->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_company_logo_file" id="z_apotech_company_logo_file" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_logo_file->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_logo_file">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_logo_file" data-page="2" name="x_apotech_company_logo_file" id="x_apotech_company_logo_file" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_logo_file->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_logo_file->EditValue ?>"<?php echo $telecare_apotech->apotech_company_logo_file->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_address->Visible) { // apotech_company_address ?>
	<div id="r_apotech_company_address" class="form-group">
		<label for="x_apotech_company_address" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_address"><?php echo $telecare_apotech->apotech_company_address->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_company_address" id="z_apotech_company_address" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_address->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_address">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_address" data-page="2" name="x_apotech_company_address" id="x_apotech_company_address" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_address->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_address->EditValue ?>"<?php echo $telecare_apotech->apotech_company_address->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_phone->Visible) { // apotech_company_phone ?>
	<div id="r_apotech_company_phone" class="form-group">
		<label for="x_apotech_company_phone" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_phone"><?php echo $telecare_apotech->apotech_company_phone->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_company_phone" id="z_apotech_company_phone" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_phone->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_phone">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_phone" data-page="2" name="x_apotech_company_phone" id="x_apotech_company_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_phone->EditValue ?>"<?php echo $telecare_apotech->apotech_company_phone->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_email->Visible) { // apotech_company_email ?>
	<div id="r_apotech_company_email" class="form-group">
		<label for="x_apotech_company_email" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_email"><?php echo $telecare_apotech->apotech_company_email->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_company_email" id="z_apotech_company_email" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_email->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_email">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_email" data-page="2" name="x_apotech_company_email" id="x_apotech_company_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_email->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_email->EditValue ?>"<?php echo $telecare_apotech->apotech_company_email->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_web->Visible) { // apotech_company_web ?>
	<div id="r_apotech_company_web" class="form-group">
		<label for="x_apotech_company_web" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_web"><?php echo $telecare_apotech->apotech_company_web->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_company_web" id="z_apotech_company_web" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_web->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_web">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_web" data-page="2" name="x_apotech_company_web" id="x_apotech_company_web" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_web->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_web->EditValue ?>"<?php echo $telecare_apotech->apotech_company_web->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_apotech_search->MultiPages->PageStyle("3") ?>" id="tab_telecare_apotech3">
<div>
<?php if ($telecare_apotech->apotech_latitude->Visible) { // apotech_latitude ?>
	<div id="r_apotech_latitude" class="form-group">
		<label for="x_apotech_latitude" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_latitude"><?php echo $telecare_apotech->apotech_latitude->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_latitude" id="z_apotech_latitude" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_latitude->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_latitude">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_latitude" data-page="3" name="x_apotech_latitude" id="x_apotech_latitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_latitude->EditValue ?>"<?php echo $telecare_apotech->apotech_latitude->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_longitude->Visible) { // apotech_longitude ?>
	<div id="r_apotech_longitude" class="form-group">
		<label for="x_apotech_longitude" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_longitude"><?php echo $telecare_apotech->apotech_longitude->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_longitude" id="z_apotech_longitude" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_longitude->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_longitude">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_longitude" data-page="3" name="x_apotech_longitude" id="x_apotech_longitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_longitude->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_longitude->EditValue ?>"<?php echo $telecare_apotech->apotech_longitude->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_region_id->Visible) { // apotech_company_region_id ?>
	<div id="r_apotech_company_region_id" class="form-group">
		<label for="x_apotech_company_region_id" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_region_id"><?php echo $telecare_apotech->apotech_company_region_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_apotech_company_region_id" id="z_apotech_company_region_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_region_id->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_region_id">
<?php $telecare_apotech->apotech_company_region_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_apotech->apotech_company_region_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_apotech" data-field="x_apotech_company_region_id" data-page="3" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_company_region_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_company_region_id->DisplayValueSeparator) : $telecare_apotech->apotech_company_region_id->DisplayValueSeparator) ?>" id="x_apotech_company_region_id" name="x_apotech_company_region_id"<?php echo $telecare_apotech->apotech_company_region_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_company_region_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_company_region_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_company_region_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_company_region_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_company_region_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_region_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_company_region_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
}
$telecare_apotech->apotech_company_region_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_company_region_id->LookupFilters += array("f0" => "`region_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_company_region_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_company_region_id" id="s_x_apotech_company_region_id" value="<?php echo $telecare_apotech->apotech_company_region_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_province_id->Visible) { // apotech_company_province_id ?>
	<div id="r_apotech_company_province_id" class="form-group">
		<label for="x_apotech_company_province_id" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_province_id"><?php echo $telecare_apotech->apotech_company_province_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_apotech_company_province_id" id="z_apotech_company_province_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_province_id->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_province_id">
<?php $telecare_apotech->apotech_company_province_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_apotech->apotech_company_province_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_apotech" data-field="x_apotech_company_province_id" data-page="3" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_company_province_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_company_province_id->DisplayValueSeparator) : $telecare_apotech->apotech_company_province_id->DisplayValueSeparator) ?>" id="x_apotech_company_province_id" name="x_apotech_company_province_id"<?php echo $telecare_apotech->apotech_company_province_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_company_province_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_company_province_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_company_province_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_company_province_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_company_province_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_province_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_company_province_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_apotech->apotech_company_province_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_company_province_id->LookupFilters += array("f0" => "`province_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_apotech->apotech_company_province_id->LookupFilters += array("f1" => "`province_region_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_company_province_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_company_province_id" id="s_x_apotech_company_province_id" value="<?php echo $telecare_apotech->apotech_company_province_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_city_id->Visible) { // apotech_company_city_id ?>
	<div id="r_apotech_company_city_id" class="form-group">
		<label for="x_apotech_company_city_id" class="<?php echo $telecare_apotech_search->SearchLabelClass ?>"><span id="elh_telecare_apotech_apotech_company_city_id"><?php echo $telecare_apotech->apotech_company_city_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_apotech_company_city_id" id="z_apotech_company_city_id" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_apotech_search->SearchRightColumnClass ?>"><div<?php echo $telecare_apotech->apotech_company_city_id->CellAttributes() ?>>
			<span id="el_telecare_apotech_apotech_company_city_id">
<select data-table="telecare_apotech" data-field="x_apotech_company_city_id" data-page="3" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_company_city_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_company_city_id->DisplayValueSeparator) : $telecare_apotech->apotech_company_city_id->DisplayValueSeparator) ?>" id="x_apotech_company_city_id" name="x_apotech_company_city_id"<?php echo $telecare_apotech->apotech_company_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_company_city_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_company_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_company_city_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_company_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_company_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_city_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_company_city_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_apotech->apotech_company_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_company_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_apotech->apotech_company_city_id->LookupFilters += array("f1" => "`city_province_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_company_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_company_city_id" id="s_x_apotech_company_city_id" value="<?php echo $telecare_apotech->apotech_company_city_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
	</div>
</div>
</div>
<?php if (!$telecare_apotech_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
ftelecare_apotechsearch.Init();
</script>
<?php
$telecare_apotech_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_apotech_search->Page_Terminate();
?>
