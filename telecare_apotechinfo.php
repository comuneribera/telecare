<?php

// Global variable for table object
$telecare_apotech = NULL;

//
// Table class for telecare_apotech
//
class ctelecare_apotech extends cTable {
	var $apotech_id;
	var $apotech_admin_id;
	var $apotech_surname;
	var $apotech_name;
	var $apotech_email;
	var $apotech_mobile;
	var $apotech_phone;
	var $apotech_is_active;
	var $apotech_last_update;
	var $apotech_company_name;
	var $apotech_company_logo_file;
	var $apotech_company_logo_width;
	var $apotech_company_logo_height;
	var $apotech_company_logo_size;
	var $apotech_company_address;
	var $apotech_company_phone;
	var $apotech_company_email;
	var $apotech_company_web;
	var $apotech_latitude;
	var $apotech_longitude;
	var $apotech_company_region_id;
	var $apotech_company_province_id;
	var $apotech_company_city_id;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'telecare_apotech';
		$this->TableName = 'telecare_apotech';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`telecare_apotech`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = PHPExcel_Worksheet_PageSetup::ORIENTATION_DEFAULT; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// apotech_id
		$this->apotech_id = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_id', 'apotech_id', '`apotech_id`', '`apotech_id`', 3, -1, FALSE, '`apotech_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->apotech_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['apotech_id'] = &$this->apotech_id;

		// apotech_admin_id
		$this->apotech_admin_id = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_admin_id', 'apotech_admin_id', '`apotech_admin_id`', '`apotech_admin_id`', 3, -1, FALSE, '`apotech_admin_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->apotech_admin_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['apotech_admin_id'] = &$this->apotech_admin_id;

		// apotech_surname
		$this->apotech_surname = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_surname', 'apotech_surname', '`apotech_surname`', '`apotech_surname`', 200, -1, FALSE, '`apotech_surname`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_surname'] = &$this->apotech_surname;

		// apotech_name
		$this->apotech_name = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_name', 'apotech_name', '`apotech_name`', '`apotech_name`', 200, -1, FALSE, '`apotech_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_name'] = &$this->apotech_name;

		// apotech_email
		$this->apotech_email = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_email', 'apotech_email', '`apotech_email`', '`apotech_email`', 200, -1, FALSE, '`apotech_email`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->apotech_email->FldDefaultErrMsg = $Language->Phrase("IncorrectEmail");
		$this->fields['apotech_email'] = &$this->apotech_email;

		// apotech_mobile
		$this->apotech_mobile = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_mobile', 'apotech_mobile', '`apotech_mobile`', '`apotech_mobile`', 200, -1, FALSE, '`apotech_mobile`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_mobile'] = &$this->apotech_mobile;

		// apotech_phone
		$this->apotech_phone = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_phone', 'apotech_phone', '`apotech_phone`', '`apotech_phone`', 200, -1, FALSE, '`apotech_phone`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_phone'] = &$this->apotech_phone;

		// apotech_is_active
		$this->apotech_is_active = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_is_active', 'apotech_is_active', '`apotech_is_active`', '`apotech_is_active`', 200, -1, FALSE, '`apotech_is_active`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->apotech_is_active->OptionCount = 2;
		$this->fields['apotech_is_active'] = &$this->apotech_is_active;

		// apotech_last_update
		$this->apotech_last_update = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_last_update', 'apotech_last_update', '`apotech_last_update`', 'DATE_FORMAT(`apotech_last_update`, \'%d/%m/%Y\')', 135, 7, FALSE, '`apotech_last_update`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->apotech_last_update->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['apotech_last_update'] = &$this->apotech_last_update;

		// apotech_company_name
		$this->apotech_company_name = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_name', 'apotech_company_name', '`apotech_company_name`', '`apotech_company_name`', 200, -1, FALSE, '`apotech_company_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_company_name'] = &$this->apotech_company_name;

		// apotech_company_logo_file
		$this->apotech_company_logo_file = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_logo_file', 'apotech_company_logo_file', '`apotech_company_logo_file`', '`apotech_company_logo_file`', 200, -1, TRUE, '`apotech_company_logo_file`', FALSE, FALSE, FALSE, 'IMAGE', 'FILE');
		$this->apotech_company_logo_file->ImageResize = TRUE;
		$this->fields['apotech_company_logo_file'] = &$this->apotech_company_logo_file;

		// apotech_company_logo_width
		$this->apotech_company_logo_width = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_logo_width', 'apotech_company_logo_width', '`apotech_company_logo_width`', '`apotech_company_logo_width`', 3, -1, FALSE, '`apotech_company_logo_width`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->apotech_company_logo_width->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['apotech_company_logo_width'] = &$this->apotech_company_logo_width;

		// apotech_company_logo_height
		$this->apotech_company_logo_height = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_logo_height', 'apotech_company_logo_height', '`apotech_company_logo_height`', '`apotech_company_logo_height`', 3, -1, FALSE, '`apotech_company_logo_height`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->apotech_company_logo_height->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['apotech_company_logo_height'] = &$this->apotech_company_logo_height;

		// apotech_company_logo_size
		$this->apotech_company_logo_size = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_logo_size', 'apotech_company_logo_size', '`apotech_company_logo_size`', '`apotech_company_logo_size`', 3, -1, FALSE, '`apotech_company_logo_size`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->apotech_company_logo_size->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['apotech_company_logo_size'] = &$this->apotech_company_logo_size;

		// apotech_company_address
		$this->apotech_company_address = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_address', 'apotech_company_address', '`apotech_company_address`', '`apotech_company_address`', 200, -1, FALSE, '`apotech_company_address`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_company_address'] = &$this->apotech_company_address;

		// apotech_company_phone
		$this->apotech_company_phone = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_phone', 'apotech_company_phone', '`apotech_company_phone`', '`apotech_company_phone`', 200, -1, FALSE, '`apotech_company_phone`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_company_phone'] = &$this->apotech_company_phone;

		// apotech_company_email
		$this->apotech_company_email = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_email', 'apotech_company_email', '`apotech_company_email`', '`apotech_company_email`', 200, -1, FALSE, '`apotech_company_email`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_company_email'] = &$this->apotech_company_email;

		// apotech_company_web
		$this->apotech_company_web = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_web', 'apotech_company_web', '`apotech_company_web`', '`apotech_company_web`', 200, -1, FALSE, '`apotech_company_web`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_company_web'] = &$this->apotech_company_web;

		// apotech_latitude
		$this->apotech_latitude = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_latitude', 'apotech_latitude', '`apotech_latitude`', '`apotech_latitude`', 200, -1, FALSE, '`apotech_latitude`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_latitude'] = &$this->apotech_latitude;

		// apotech_longitude
		$this->apotech_longitude = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_longitude', 'apotech_longitude', '`apotech_longitude`', '`apotech_longitude`', 200, -1, FALSE, '`apotech_longitude`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['apotech_longitude'] = &$this->apotech_longitude;

		// apotech_company_region_id
		$this->apotech_company_region_id = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_region_id', 'apotech_company_region_id', '`apotech_company_region_id`', '`apotech_company_region_id`', 3, -1, FALSE, '`apotech_company_region_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->apotech_company_region_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['apotech_company_region_id'] = &$this->apotech_company_region_id;

		// apotech_company_province_id
		$this->apotech_company_province_id = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_province_id', 'apotech_company_province_id', '`apotech_company_province_id`', '`apotech_company_province_id`', 3, -1, FALSE, '`apotech_company_province_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->apotech_company_province_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['apotech_company_province_id'] = &$this->apotech_company_province_id;

		// apotech_company_city_id
		$this->apotech_company_city_id = new cField('telecare_apotech', 'telecare_apotech', 'x_apotech_company_city_id', 'apotech_company_city_id', '`apotech_company_city_id`', '`apotech_company_city_id`', 3, -1, FALSE, '`apotech_company_city_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->fields['apotech_company_city_id'] = &$this->apotech_company_city_id;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Current detail table name
	function getCurrentDetailTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE];
	}

	function setCurrentDetailTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE] = $v;
	}

	// Get detail url
	function GetDetailUrl() {

		// Detail url
		$sDetailUrl = "";
		if ($this->getCurrentDetailTable() == "telecare_prescription") {
			$sDetailUrl = $GLOBALS["telecare_prescription"]->GetListUrl() . "?" . EW_TABLE_SHOW_MASTER . "=" . $this->TableVar;
			$sDetailUrl .= "&fk_apotech_id=" . urlencode($this->apotech_id->CurrentValue);
		}
		if ($sDetailUrl == "") {
			$sDetailUrl = "telecare_apotechlist.php";
		}
		return $sDetailUrl;
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`telecare_apotech`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('apotech_id', $rs))
				ew_AddFilter($where, ew_QuotedName('apotech_id', $this->DBID) . '=' . ew_QuotedValue($rs['apotech_id'], $this->apotech_id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`apotech_id` = @apotech_id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->apotech_id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@apotech_id@", ew_AdjustSql($this->apotech_id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "telecare_apotechlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "telecare_apotechlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_apotechview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_apotechview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "telecare_apotechadd.php?" . $this->UrlParm($parm);
		else
			$url = "telecare_apotechadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_apotechedit.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_apotechedit.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_apotechadd.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_apotechadd.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("telecare_apotechdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "apotech_id:" . ew_VarToJson($this->apotech_id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->apotech_id->CurrentValue)) {
			$sUrl .= "apotech_id=" . urlencode($this->apotech_id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["apotech_id"]) : ew_StripSlashes(@$_GET["apotech_id"]); // apotech_id

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->apotech_id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->apotech_id->setDbValue($rs->fields('apotech_id'));
		$this->apotech_admin_id->setDbValue($rs->fields('apotech_admin_id'));
		$this->apotech_surname->setDbValue($rs->fields('apotech_surname'));
		$this->apotech_name->setDbValue($rs->fields('apotech_name'));
		$this->apotech_email->setDbValue($rs->fields('apotech_email'));
		$this->apotech_mobile->setDbValue($rs->fields('apotech_mobile'));
		$this->apotech_phone->setDbValue($rs->fields('apotech_phone'));
		$this->apotech_is_active->setDbValue($rs->fields('apotech_is_active'));
		$this->apotech_last_update->setDbValue($rs->fields('apotech_last_update'));
		$this->apotech_company_name->setDbValue($rs->fields('apotech_company_name'));
		$this->apotech_company_logo_file->Upload->DbValue = $rs->fields('apotech_company_logo_file');
		$this->apotech_company_logo_width->setDbValue($rs->fields('apotech_company_logo_width'));
		$this->apotech_company_logo_height->setDbValue($rs->fields('apotech_company_logo_height'));
		$this->apotech_company_logo_size->setDbValue($rs->fields('apotech_company_logo_size'));
		$this->apotech_company_address->setDbValue($rs->fields('apotech_company_address'));
		$this->apotech_company_phone->setDbValue($rs->fields('apotech_company_phone'));
		$this->apotech_company_email->setDbValue($rs->fields('apotech_company_email'));
		$this->apotech_company_web->setDbValue($rs->fields('apotech_company_web'));
		$this->apotech_latitude->setDbValue($rs->fields('apotech_latitude'));
		$this->apotech_longitude->setDbValue($rs->fields('apotech_longitude'));
		$this->apotech_company_region_id->setDbValue($rs->fields('apotech_company_region_id'));
		$this->apotech_company_province_id->setDbValue($rs->fields('apotech_company_province_id'));
		$this->apotech_company_city_id->setDbValue($rs->fields('apotech_company_city_id'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// apotech_id
		// apotech_admin_id
		// apotech_surname
		// apotech_name
		// apotech_email
		// apotech_mobile
		// apotech_phone
		// apotech_is_active
		// apotech_last_update
		// apotech_company_name
		// apotech_company_logo_file
		// apotech_company_logo_width
		// apotech_company_logo_height
		// apotech_company_logo_size
		// apotech_company_address
		// apotech_company_phone
		// apotech_company_email
		// apotech_company_web
		// apotech_latitude
		// apotech_longitude
		// apotech_company_region_id
		// apotech_company_province_id
		// apotech_company_city_id
		// apotech_id

		$this->apotech_id->ViewValue = $this->apotech_id->CurrentValue;
		$this->apotech_id->ViewCustomAttributes = "";

		// apotech_admin_id
		if (strval($this->apotech_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->apotech_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->CurrentValue;
			}
		} else {
			$this->apotech_admin_id->ViewValue = NULL;
		}
		$this->apotech_admin_id->ViewCustomAttributes = "";

		// apotech_surname
		$this->apotech_surname->ViewValue = $this->apotech_surname->CurrentValue;
		$this->apotech_surname->ViewCustomAttributes = "";

		// apotech_name
		$this->apotech_name->ViewValue = $this->apotech_name->CurrentValue;
		$this->apotech_name->ViewCustomAttributes = "";

		// apotech_email
		$this->apotech_email->ViewValue = $this->apotech_email->CurrentValue;
		$this->apotech_email->ViewCustomAttributes = "";

		// apotech_mobile
		$this->apotech_mobile->ViewValue = $this->apotech_mobile->CurrentValue;
		$this->apotech_mobile->ViewCustomAttributes = "";

		// apotech_phone
		$this->apotech_phone->ViewValue = $this->apotech_phone->CurrentValue;
		$this->apotech_phone->ViewCustomAttributes = "";

		// apotech_is_active
		if (strval($this->apotech_is_active->CurrentValue) <> "") {
			$this->apotech_is_active->ViewValue = $this->apotech_is_active->OptionCaption($this->apotech_is_active->CurrentValue);
		} else {
			$this->apotech_is_active->ViewValue = NULL;
		}
		$this->apotech_is_active->ViewCustomAttributes = "";

		// apotech_last_update
		$this->apotech_last_update->ViewValue = $this->apotech_last_update->CurrentValue;
		$this->apotech_last_update->ViewValue = ew_FormatDateTime($this->apotech_last_update->ViewValue, 7);
		$this->apotech_last_update->ViewCustomAttributes = "";

		// apotech_company_name
		$this->apotech_company_name->ViewValue = $this->apotech_company_name->CurrentValue;
		$this->apotech_company_name->ViewCustomAttributes = "";

		// apotech_company_logo_file
		if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
			$this->apotech_company_logo_file->ImageWidth = 100;
			$this->apotech_company_logo_file->ImageHeight = 0;
			$this->apotech_company_logo_file->ImageAlt = $this->apotech_company_logo_file->FldAlt();
			$this->apotech_company_logo_file->ViewValue = $this->apotech_company_logo_file->Upload->DbValue;
		} else {
			$this->apotech_company_logo_file->ViewValue = "";
		}
		$this->apotech_company_logo_file->ViewCustomAttributes = "";

		// apotech_company_logo_width
		$this->apotech_company_logo_width->ViewValue = $this->apotech_company_logo_width->CurrentValue;
		$this->apotech_company_logo_width->ViewCustomAttributes = "";

		// apotech_company_logo_height
		$this->apotech_company_logo_height->ViewValue = $this->apotech_company_logo_height->CurrentValue;
		$this->apotech_company_logo_height->ViewCustomAttributes = "";

		// apotech_company_logo_size
		$this->apotech_company_logo_size->ViewValue = $this->apotech_company_logo_size->CurrentValue;
		$this->apotech_company_logo_size->ViewCustomAttributes = "";

		// apotech_company_address
		$this->apotech_company_address->ViewValue = $this->apotech_company_address->CurrentValue;
		$this->apotech_company_address->ViewCustomAttributes = "";

		// apotech_company_phone
		$this->apotech_company_phone->ViewValue = $this->apotech_company_phone->CurrentValue;
		$this->apotech_company_phone->ViewCustomAttributes = "";

		// apotech_company_email
		$this->apotech_company_email->ViewValue = $this->apotech_company_email->CurrentValue;
		$this->apotech_company_email->ViewCustomAttributes = "";

		// apotech_company_web
		$this->apotech_company_web->ViewValue = $this->apotech_company_web->CurrentValue;
		$this->apotech_company_web->ViewCustomAttributes = "";

		// apotech_latitude
		$this->apotech_latitude->ViewValue = $this->apotech_latitude->CurrentValue;
		$this->apotech_latitude->ViewCustomAttributes = "";

		// apotech_longitude
		$this->apotech_longitude->ViewValue = $this->apotech_longitude->CurrentValue;
		$this->apotech_longitude->ViewCustomAttributes = "";

		// apotech_company_region_id
		if (strval($this->apotech_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->apotech_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->CurrentValue;
			}
		} else {
			$this->apotech_company_region_id->ViewValue = NULL;
		}
		$this->apotech_company_region_id->ViewCustomAttributes = "";

		// apotech_company_province_id
		if (strval($this->apotech_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->apotech_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->CurrentValue;
			}
		} else {
			$this->apotech_company_province_id->ViewValue = NULL;
		}
		$this->apotech_company_province_id->ViewCustomAttributes = "";

		// apotech_company_city_id
		if (strval($this->apotech_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->apotech_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->CurrentValue;
			}
		} else {
			$this->apotech_company_city_id->ViewValue = NULL;
		}
		$this->apotech_company_city_id->ViewCustomAttributes = "";

		// apotech_id
		$this->apotech_id->LinkCustomAttributes = "";
		$this->apotech_id->HrefValue = "";
		$this->apotech_id->TooltipValue = "";

		// apotech_admin_id
		$this->apotech_admin_id->LinkCustomAttributes = "";
		$this->apotech_admin_id->HrefValue = "";
		$this->apotech_admin_id->TooltipValue = "";

		// apotech_surname
		$this->apotech_surname->LinkCustomAttributes = "";
		$this->apotech_surname->HrefValue = "";
		$this->apotech_surname->TooltipValue = "";

		// apotech_name
		$this->apotech_name->LinkCustomAttributes = "";
		$this->apotech_name->HrefValue = "";
		$this->apotech_name->TooltipValue = "";

		// apotech_email
		$this->apotech_email->LinkCustomAttributes = "";
		$this->apotech_email->HrefValue = "";
		$this->apotech_email->TooltipValue = "";

		// apotech_mobile
		$this->apotech_mobile->LinkCustomAttributes = "";
		$this->apotech_mobile->HrefValue = "";
		$this->apotech_mobile->TooltipValue = "";

		// apotech_phone
		$this->apotech_phone->LinkCustomAttributes = "";
		$this->apotech_phone->HrefValue = "";
		$this->apotech_phone->TooltipValue = "";

		// apotech_is_active
		$this->apotech_is_active->LinkCustomAttributes = "";
		$this->apotech_is_active->HrefValue = "";
		$this->apotech_is_active->TooltipValue = "";

		// apotech_last_update
		$this->apotech_last_update->LinkCustomAttributes = "";
		$this->apotech_last_update->HrefValue = "";
		$this->apotech_last_update->TooltipValue = "";

		// apotech_company_name
		$this->apotech_company_name->LinkCustomAttributes = "";
		$this->apotech_company_name->HrefValue = "";
		$this->apotech_company_name->TooltipValue = "";

		// apotech_company_logo_file
		$this->apotech_company_logo_file->LinkCustomAttributes = "";
		if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
			$this->apotech_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->apotech_company_logo_file, $this->apotech_company_logo_file->Upload->DbValue); // Add prefix/suffix
			$this->apotech_company_logo_file->LinkAttrs["target"] = ""; // Add target
			if ($this->Export <> "") $this->apotech_company_logo_file->HrefValue = ew_ConvertFullUrl($this->apotech_company_logo_file->HrefValue);
		} else {
			$this->apotech_company_logo_file->HrefValue = "";
		}
		$this->apotech_company_logo_file->HrefValue2 = $this->apotech_company_logo_file->UploadPath . $this->apotech_company_logo_file->Upload->DbValue;
		$this->apotech_company_logo_file->TooltipValue = "";
		if ($this->apotech_company_logo_file->UseColorbox) {
			$this->apotech_company_logo_file->LinkAttrs["title"] = $Language->Phrase("ViewImageGallery");
			$this->apotech_company_logo_file->LinkAttrs["data-rel"] = "telecare_apotech_x_apotech_company_logo_file";

			//$this->apotech_company_logo_file->LinkAttrs["class"] = "ewLightbox ewTooltip img-thumbnail";
			//$this->apotech_company_logo_file->LinkAttrs["data-placement"] = "bottom";
			//$this->apotech_company_logo_file->LinkAttrs["data-container"] = "body";

			$this->apotech_company_logo_file->LinkAttrs["class"] = "ewLightbox img-thumbnail";
		}

		// apotech_company_logo_width
		$this->apotech_company_logo_width->LinkCustomAttributes = "";
		$this->apotech_company_logo_width->HrefValue = "";
		$this->apotech_company_logo_width->TooltipValue = "";

		// apotech_company_logo_height
		$this->apotech_company_logo_height->LinkCustomAttributes = "";
		$this->apotech_company_logo_height->HrefValue = "";
		$this->apotech_company_logo_height->TooltipValue = "";

		// apotech_company_logo_size
		$this->apotech_company_logo_size->LinkCustomAttributes = "";
		$this->apotech_company_logo_size->HrefValue = "";
		$this->apotech_company_logo_size->TooltipValue = "";

		// apotech_company_address
		$this->apotech_company_address->LinkCustomAttributes = "";
		$this->apotech_company_address->HrefValue = "";
		$this->apotech_company_address->TooltipValue = "";

		// apotech_company_phone
		$this->apotech_company_phone->LinkCustomAttributes = "";
		$this->apotech_company_phone->HrefValue = "";
		$this->apotech_company_phone->TooltipValue = "";

		// apotech_company_email
		$this->apotech_company_email->LinkCustomAttributes = "";
		$this->apotech_company_email->HrefValue = "";
		$this->apotech_company_email->TooltipValue = "";

		// apotech_company_web
		$this->apotech_company_web->LinkCustomAttributes = "";
		$this->apotech_company_web->HrefValue = "";
		$this->apotech_company_web->TooltipValue = "";

		// apotech_latitude
		$this->apotech_latitude->LinkCustomAttributes = "";
		$this->apotech_latitude->HrefValue = "";
		$this->apotech_latitude->TooltipValue = "";

		// apotech_longitude
		$this->apotech_longitude->LinkCustomAttributes = "";
		$this->apotech_longitude->HrefValue = "";
		$this->apotech_longitude->TooltipValue = "";

		// apotech_company_region_id
		$this->apotech_company_region_id->LinkCustomAttributes = "";
		$this->apotech_company_region_id->HrefValue = "";
		$this->apotech_company_region_id->TooltipValue = "";

		// apotech_company_province_id
		$this->apotech_company_province_id->LinkCustomAttributes = "";
		$this->apotech_company_province_id->HrefValue = "";
		$this->apotech_company_province_id->TooltipValue = "";

		// apotech_company_city_id
		$this->apotech_company_city_id->LinkCustomAttributes = "";
		$this->apotech_company_city_id->HrefValue = "";
		$this->apotech_company_city_id->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// apotech_id
		$this->apotech_id->EditAttrs["class"] = "form-control";
		$this->apotech_id->EditCustomAttributes = "";
		$this->apotech_id->EditValue = $this->apotech_id->CurrentValue;
		$this->apotech_id->ViewCustomAttributes = "";

		// apotech_admin_id
		$this->apotech_admin_id->EditAttrs["class"] = "form-control";
		$this->apotech_admin_id->EditCustomAttributes = "";

		// apotech_surname
		$this->apotech_surname->EditAttrs["class"] = "form-control";
		$this->apotech_surname->EditCustomAttributes = "";
		$this->apotech_surname->EditValue = $this->apotech_surname->CurrentValue;
		$this->apotech_surname->PlaceHolder = ew_RemoveHtml($this->apotech_surname->FldCaption());

		// apotech_name
		$this->apotech_name->EditAttrs["class"] = "form-control";
		$this->apotech_name->EditCustomAttributes = "";
		$this->apotech_name->EditValue = $this->apotech_name->CurrentValue;
		$this->apotech_name->PlaceHolder = ew_RemoveHtml($this->apotech_name->FldCaption());

		// apotech_email
		$this->apotech_email->EditAttrs["class"] = "form-control";
		$this->apotech_email->EditCustomAttributes = "";
		$this->apotech_email->EditValue = $this->apotech_email->CurrentValue;
		$this->apotech_email->PlaceHolder = ew_RemoveHtml($this->apotech_email->FldCaption());

		// apotech_mobile
		$this->apotech_mobile->EditAttrs["class"] = "form-control";
		$this->apotech_mobile->EditCustomAttributes = "";
		$this->apotech_mobile->EditValue = $this->apotech_mobile->CurrentValue;
		$this->apotech_mobile->PlaceHolder = ew_RemoveHtml($this->apotech_mobile->FldCaption());

		// apotech_phone
		$this->apotech_phone->EditAttrs["class"] = "form-control";
		$this->apotech_phone->EditCustomAttributes = "";
		$this->apotech_phone->EditValue = $this->apotech_phone->CurrentValue;
		$this->apotech_phone->PlaceHolder = ew_RemoveHtml($this->apotech_phone->FldCaption());

		// apotech_is_active
		$this->apotech_is_active->EditCustomAttributes = "";
		$this->apotech_is_active->EditValue = $this->apotech_is_active->Options(FALSE);

		// apotech_last_update
		// apotech_company_name

		$this->apotech_company_name->EditAttrs["class"] = "form-control";
		$this->apotech_company_name->EditCustomAttributes = "";
		$this->apotech_company_name->EditValue = $this->apotech_company_name->CurrentValue;
		$this->apotech_company_name->PlaceHolder = ew_RemoveHtml($this->apotech_company_name->FldCaption());

		// apotech_company_logo_file
		$this->apotech_company_logo_file->EditAttrs["class"] = "form-control";
		$this->apotech_company_logo_file->EditCustomAttributes = "";
		if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
			$this->apotech_company_logo_file->ImageWidth = 100;
			$this->apotech_company_logo_file->ImageHeight = 0;
			$this->apotech_company_logo_file->ImageAlt = $this->apotech_company_logo_file->FldAlt();
			$this->apotech_company_logo_file->EditValue = $this->apotech_company_logo_file->Upload->DbValue;
		} else {
			$this->apotech_company_logo_file->EditValue = "";
		}
		if (!ew_Empty($this->apotech_company_logo_file->CurrentValue))
			$this->apotech_company_logo_file->Upload->FileName = $this->apotech_company_logo_file->CurrentValue;

		// apotech_company_logo_width
		$this->apotech_company_logo_width->EditAttrs["class"] = "form-control";
		$this->apotech_company_logo_width->EditCustomAttributes = "";
		$this->apotech_company_logo_width->EditValue = $this->apotech_company_logo_width->CurrentValue;
		$this->apotech_company_logo_width->PlaceHolder = ew_RemoveHtml($this->apotech_company_logo_width->FldCaption());

		// apotech_company_logo_height
		$this->apotech_company_logo_height->EditAttrs["class"] = "form-control";
		$this->apotech_company_logo_height->EditCustomAttributes = "";
		$this->apotech_company_logo_height->EditValue = $this->apotech_company_logo_height->CurrentValue;
		$this->apotech_company_logo_height->PlaceHolder = ew_RemoveHtml($this->apotech_company_logo_height->FldCaption());

		// apotech_company_logo_size
		$this->apotech_company_logo_size->EditAttrs["class"] = "form-control";
		$this->apotech_company_logo_size->EditCustomAttributes = "";
		$this->apotech_company_logo_size->EditValue = $this->apotech_company_logo_size->CurrentValue;
		$this->apotech_company_logo_size->PlaceHolder = ew_RemoveHtml($this->apotech_company_logo_size->FldCaption());

		// apotech_company_address
		$this->apotech_company_address->EditAttrs["class"] = "form-control";
		$this->apotech_company_address->EditCustomAttributes = "";
		$this->apotech_company_address->EditValue = $this->apotech_company_address->CurrentValue;
		$this->apotech_company_address->PlaceHolder = ew_RemoveHtml($this->apotech_company_address->FldCaption());

		// apotech_company_phone
		$this->apotech_company_phone->EditAttrs["class"] = "form-control";
		$this->apotech_company_phone->EditCustomAttributes = "";
		$this->apotech_company_phone->EditValue = $this->apotech_company_phone->CurrentValue;
		$this->apotech_company_phone->PlaceHolder = ew_RemoveHtml($this->apotech_company_phone->FldCaption());

		// apotech_company_email
		$this->apotech_company_email->EditAttrs["class"] = "form-control";
		$this->apotech_company_email->EditCustomAttributes = "";
		$this->apotech_company_email->EditValue = $this->apotech_company_email->CurrentValue;
		$this->apotech_company_email->PlaceHolder = ew_RemoveHtml($this->apotech_company_email->FldCaption());

		// apotech_company_web
		$this->apotech_company_web->EditAttrs["class"] = "form-control";
		$this->apotech_company_web->EditCustomAttributes = "";
		$this->apotech_company_web->EditValue = $this->apotech_company_web->CurrentValue;
		$this->apotech_company_web->PlaceHolder = ew_RemoveHtml($this->apotech_company_web->FldCaption());

		// apotech_latitude
		$this->apotech_latitude->EditAttrs["class"] = "form-control";
		$this->apotech_latitude->EditCustomAttributes = "";
		$this->apotech_latitude->EditValue = $this->apotech_latitude->CurrentValue;
		$this->apotech_latitude->PlaceHolder = ew_RemoveHtml($this->apotech_latitude->FldCaption());

		// apotech_longitude
		$this->apotech_longitude->EditAttrs["class"] = "form-control";
		$this->apotech_longitude->EditCustomAttributes = "";
		$this->apotech_longitude->EditValue = $this->apotech_longitude->CurrentValue;
		$this->apotech_longitude->PlaceHolder = ew_RemoveHtml($this->apotech_longitude->FldCaption());

		// apotech_company_region_id
		$this->apotech_company_region_id->EditAttrs["class"] = "form-control";
		$this->apotech_company_region_id->EditCustomAttributes = "";

		// apotech_company_province_id
		$this->apotech_company_province_id->EditAttrs["class"] = "form-control";
		$this->apotech_company_province_id->EditCustomAttributes = "";

		// apotech_company_city_id
		$this->apotech_company_city_id->EditAttrs["class"] = "form-control";
		$this->apotech_company_city_id->EditCustomAttributes = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->apotech_id->Exportable) $Doc->ExportCaption($this->apotech_id);
					if ($this->apotech_admin_id->Exportable) $Doc->ExportCaption($this->apotech_admin_id);
					if ($this->apotech_surname->Exportable) $Doc->ExportCaption($this->apotech_surname);
					if ($this->apotech_name->Exportable) $Doc->ExportCaption($this->apotech_name);
					if ($this->apotech_email->Exportable) $Doc->ExportCaption($this->apotech_email);
					if ($this->apotech_mobile->Exportable) $Doc->ExportCaption($this->apotech_mobile);
					if ($this->apotech_phone->Exportable) $Doc->ExportCaption($this->apotech_phone);
					if ($this->apotech_is_active->Exportable) $Doc->ExportCaption($this->apotech_is_active);
					if ($this->apotech_last_update->Exportable) $Doc->ExportCaption($this->apotech_last_update);
					if ($this->apotech_company_name->Exportable) $Doc->ExportCaption($this->apotech_company_name);
					if ($this->apotech_company_logo_file->Exportable) $Doc->ExportCaption($this->apotech_company_logo_file);
					if ($this->apotech_company_address->Exportable) $Doc->ExportCaption($this->apotech_company_address);
					if ($this->apotech_company_phone->Exportable) $Doc->ExportCaption($this->apotech_company_phone);
					if ($this->apotech_company_email->Exportable) $Doc->ExportCaption($this->apotech_company_email);
					if ($this->apotech_company_web->Exportable) $Doc->ExportCaption($this->apotech_company_web);
					if ($this->apotech_latitude->Exportable) $Doc->ExportCaption($this->apotech_latitude);
					if ($this->apotech_longitude->Exportable) $Doc->ExportCaption($this->apotech_longitude);
					if ($this->apotech_company_region_id->Exportable) $Doc->ExportCaption($this->apotech_company_region_id);
					if ($this->apotech_company_province_id->Exportable) $Doc->ExportCaption($this->apotech_company_province_id);
					if ($this->apotech_company_city_id->Exportable) $Doc->ExportCaption($this->apotech_company_city_id);
				} else {
					if ($this->apotech_id->Exportable) $Doc->ExportCaption($this->apotech_id);
					if ($this->apotech_admin_id->Exportable) $Doc->ExportCaption($this->apotech_admin_id);
					if ($this->apotech_surname->Exportable) $Doc->ExportCaption($this->apotech_surname);
					if ($this->apotech_name->Exportable) $Doc->ExportCaption($this->apotech_name);
					if ($this->apotech_email->Exportable) $Doc->ExportCaption($this->apotech_email);
					if ($this->apotech_mobile->Exportable) $Doc->ExportCaption($this->apotech_mobile);
					if ($this->apotech_phone->Exportable) $Doc->ExportCaption($this->apotech_phone);
					if ($this->apotech_is_active->Exportable) $Doc->ExportCaption($this->apotech_is_active);
					if ($this->apotech_last_update->Exportable) $Doc->ExportCaption($this->apotech_last_update);
					if ($this->apotech_company_name->Exportable) $Doc->ExportCaption($this->apotech_company_name);
					if ($this->apotech_company_logo_file->Exportable) $Doc->ExportCaption($this->apotech_company_logo_file);
					if ($this->apotech_company_logo_width->Exportable) $Doc->ExportCaption($this->apotech_company_logo_width);
					if ($this->apotech_company_logo_height->Exportable) $Doc->ExportCaption($this->apotech_company_logo_height);
					if ($this->apotech_company_logo_size->Exportable) $Doc->ExportCaption($this->apotech_company_logo_size);
					if ($this->apotech_company_address->Exportable) $Doc->ExportCaption($this->apotech_company_address);
					if ($this->apotech_company_phone->Exportable) $Doc->ExportCaption($this->apotech_company_phone);
					if ($this->apotech_company_email->Exportable) $Doc->ExportCaption($this->apotech_company_email);
					if ($this->apotech_company_web->Exportable) $Doc->ExportCaption($this->apotech_company_web);
					if ($this->apotech_latitude->Exportable) $Doc->ExportCaption($this->apotech_latitude);
					if ($this->apotech_longitude->Exportable) $Doc->ExportCaption($this->apotech_longitude);
					if ($this->apotech_company_region_id->Exportable) $Doc->ExportCaption($this->apotech_company_region_id);
					if ($this->apotech_company_province_id->Exportable) $Doc->ExportCaption($this->apotech_company_province_id);
					if ($this->apotech_company_city_id->Exportable) $Doc->ExportCaption($this->apotech_company_city_id);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->apotech_id->Exportable) $Doc->ExportField($this->apotech_id);
						if ($this->apotech_admin_id->Exportable) $Doc->ExportField($this->apotech_admin_id);
						if ($this->apotech_surname->Exportable) $Doc->ExportField($this->apotech_surname);
						if ($this->apotech_name->Exportable) $Doc->ExportField($this->apotech_name);
						if ($this->apotech_email->Exportable) $Doc->ExportField($this->apotech_email);
						if ($this->apotech_mobile->Exportable) $Doc->ExportField($this->apotech_mobile);
						if ($this->apotech_phone->Exportable) $Doc->ExportField($this->apotech_phone);
						if ($this->apotech_is_active->Exportable) $Doc->ExportField($this->apotech_is_active);
						if ($this->apotech_last_update->Exportable) $Doc->ExportField($this->apotech_last_update);
						if ($this->apotech_company_name->Exportable) $Doc->ExportField($this->apotech_company_name);
						if ($this->apotech_company_logo_file->Exportable) $Doc->ExportField($this->apotech_company_logo_file);
						if ($this->apotech_company_address->Exportable) $Doc->ExportField($this->apotech_company_address);
						if ($this->apotech_company_phone->Exportable) $Doc->ExportField($this->apotech_company_phone);
						if ($this->apotech_company_email->Exportable) $Doc->ExportField($this->apotech_company_email);
						if ($this->apotech_company_web->Exportable) $Doc->ExportField($this->apotech_company_web);
						if ($this->apotech_latitude->Exportable) $Doc->ExportField($this->apotech_latitude);
						if ($this->apotech_longitude->Exportable) $Doc->ExportField($this->apotech_longitude);
						if ($this->apotech_company_region_id->Exportable) $Doc->ExportField($this->apotech_company_region_id);
						if ($this->apotech_company_province_id->Exportable) $Doc->ExportField($this->apotech_company_province_id);
						if ($this->apotech_company_city_id->Exportable) $Doc->ExportField($this->apotech_company_city_id);
					} else {
						if ($this->apotech_id->Exportable) $Doc->ExportField($this->apotech_id);
						if ($this->apotech_admin_id->Exportable) $Doc->ExportField($this->apotech_admin_id);
						if ($this->apotech_surname->Exportable) $Doc->ExportField($this->apotech_surname);
						if ($this->apotech_name->Exportable) $Doc->ExportField($this->apotech_name);
						if ($this->apotech_email->Exportable) $Doc->ExportField($this->apotech_email);
						if ($this->apotech_mobile->Exportable) $Doc->ExportField($this->apotech_mobile);
						if ($this->apotech_phone->Exportable) $Doc->ExportField($this->apotech_phone);
						if ($this->apotech_is_active->Exportable) $Doc->ExportField($this->apotech_is_active);
						if ($this->apotech_last_update->Exportable) $Doc->ExportField($this->apotech_last_update);
						if ($this->apotech_company_name->Exportable) $Doc->ExportField($this->apotech_company_name);
						if ($this->apotech_company_logo_file->Exportable) $Doc->ExportField($this->apotech_company_logo_file);
						if ($this->apotech_company_logo_width->Exportable) $Doc->ExportField($this->apotech_company_logo_width);
						if ($this->apotech_company_logo_height->Exportable) $Doc->ExportField($this->apotech_company_logo_height);
						if ($this->apotech_company_logo_size->Exportable) $Doc->ExportField($this->apotech_company_logo_size);
						if ($this->apotech_company_address->Exportable) $Doc->ExportField($this->apotech_company_address);
						if ($this->apotech_company_phone->Exportable) $Doc->ExportField($this->apotech_company_phone);
						if ($this->apotech_company_email->Exportable) $Doc->ExportField($this->apotech_company_email);
						if ($this->apotech_company_web->Exportable) $Doc->ExportField($this->apotech_company_web);
						if ($this->apotech_latitude->Exportable) $Doc->ExportField($this->apotech_latitude);
						if ($this->apotech_longitude->Exportable) $Doc->ExportField($this->apotech_longitude);
						if ($this->apotech_company_region_id->Exportable) $Doc->ExportField($this->apotech_company_region_id);
						if ($this->apotech_company_province_id->Exportable) $Doc->ExportField($this->apotech_company_province_id);
						if ($this->apotech_company_city_id->Exportable) $Doc->ExportField($this->apotech_company_city_id);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
