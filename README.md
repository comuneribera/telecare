# Telecare

**Progetto di Servizi Socio-Assistenziali e Socio-Sanitari**

## INTRODUZIONE
Il progetto si propone di affrontare le sfide che i Comuni devono affrontare nell'erogazione di servizi socio-assistenziali e socio-sanitari alla popolazione, in particolare a coloro che sono più deboli o vulnerabili. Nonostante le risorse limitate e la complessità della domanda di servizi sociali, insieme alle difficoltà nell'erogazione degli stessi, è necessario garantire l'efficienza in termini di qualità e copertura del maggior numero possibile di individui bisognosi.
## Licenza
Il software è rilasciato con licenza aperta ai sensi dell'art. 69 comma 1 del Codice dell’[Amministrazione Digitale](https://www.agid.gov.it/it/design-servizi/riuso-open-source/linee-guida-acquisizione-riuso-software-pa) con una licenza [AGPL-3.0 or later](https://spdx.org/licenses/AGPL-3.0-or-later.html)

## INFORMAZIONI GENERALI
Il nostro progetto si propone come strumento per attivare azioni integrate, comprese l'acquisizione di beni e servizi innovativi, al fine di:

Tradurre i diversi servizi socio-assistenziali e socio-sanitari in procedure e protocolli sviluppabili online, migliorando così le condizioni di erogazione, consentendo l'accesso online, la teleassistenza e la fruizione autonoma in termini di spazio e tempo per gli utenti finali.
Centralizzare alcuni servizi socio-assistenziali e socio-sanitari attraverso un sistema di intermediazione costituito da operatori del settore e automatizzare sia il processo di erogazione che di utilizzo da parte dei destinatari finali, sfruttando le tecnologie informatiche e telematiche.
Consentire una gestione adeguata dell'intero processo di erogazione di ciascun servizio, dalla fase di implementazione all'operatività, al monitoraggio e al controllo dell'efficacia.
Integrare l'offerta di servizi socio-assistenziali e socio-sanitari in modo da raggiungere il maggior numero possibile di destinatari finali, garantendo alti standard qualitativi.

### OBIETTIVI DEL PROGETTO
I principali obiettivi del nostro progetto sono:

Migliorare l'efficienza e la qualità nell'erogazione dei servizi socio-assistenziali e socio-sanitari.
Favorire l'accesso online ai servizi, consentendo la teleassistenza e la fruizione autonoma da parte degli utenti finali.
Centralizzare alcuni servizi attraverso operatori del settore, semplificando il processo di erogazione e utilizzo dei servizi.
Implementare un sistema di gestione completo per il monitoraggio e il controllo dell'efficacia dei servizi erogati.
Ampliare l'offerta di servizi socio-assistenziali e socio-sanitari, raggiungendo il maggior numero possibile di individui bisognosi, mantenendo alti standard qualitativi. 
Interfacciamento a dispositivi Biomedicali per il monitoraggio di:
- Frequenza respiratoria
- Temperatura pelle
- Attività in VMU ( fermo, passeggiata, corsa)
- Accelerazione ( accelerometro 3 assi 100Hz, 16g)
- Posizione/postura (+/-180°) 
- Misurazione della pressione sanguigna
- Saturimetro

### Benefici attesi  
I maggiori benefici attesi, macroscopicamente, sono: 
- Migliorare l’efficienza dei servizi socio-sanitari erogati;
- Sperimentare nuove forme di servizi di tipo domiciliare e/o personalizzato attraverso nuove modalità di erogazione;
- Territorializzare e realizzare una nuova domiciliarità dei servizi socio-sanitari nell’ottica della maggio-re e migliore distribuzione degli stessi;
- Assicurare la raggiungibilità del maggior numero possibile di destinatari finali target;
- Diminuire le necessità di mobilità di alcuni specifici target della popolazione con difficoltà motorie (anziani, diversamente abili);
- Contribuire, quindi, a diminuire le cause di inquinamento urbano;
- Mettere in campo effettive azioni di inclusione sociale dei differenti target svantaggiati di popolazio-ne;
- Realizzare la decongestione in termini di mobilità e sovraffollamento di uffici e gabinetti medico-sanitari;
- Rendere capillare la diffusione di tali servizi, attraverso l’uso di tecnologie che permetteranno di rag-giungere anche le fasce di popolazione allocate in ambiti territoriali periferici e/o distanti dai centri urbani;
- Ottimizzare i costi di implementazione ed erogazione di tali servizi, con diminuzione dei costi per ri-sorse umane e con possibilità di riallocazione dei diversi budget annuali a seguito delle economie conseguite;

### Destinatari del servizio 
Anziani, malati, diversamente abili (tutti quali destinatari direttamente coinvolti)

