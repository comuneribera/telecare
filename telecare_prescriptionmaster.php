<?php

// prescription_id
// prescription_user_id
// prescription_drugstore_id
// prescription_doctor_id
// prescription_admin_id
// prescription_request_date
// prescription_start_on
// prescription_close_on
// prescription_close

?>
<?php if ($telecare_prescription->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $telecare_prescription->TableCaption() ?></h4> -->
<table id="tbl_telecare_prescriptionmaster" class="table table-bordered table-striped ewViewTable">
	<tbody>
<?php if ($telecare_prescription->prescription_id->Visible) { // prescription_id ?>
		<tr id="r_prescription_id">
			<td><?php echo $telecare_prescription->prescription_id->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_id->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_id">
<span<?php echo $telecare_prescription->prescription_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_user_id->Visible) { // prescription_user_id ?>
		<tr id="r_prescription_user_id">
			<td><?php echo $telecare_prescription->prescription_user_id->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_user_id->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_user_id">
<span<?php echo $telecare_prescription->prescription_user_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_user_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_drugstore_id->Visible) { // prescription_drugstore_id ?>
		<tr id="r_prescription_drugstore_id">
			<td><?php echo $telecare_prescription->prescription_drugstore_id->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_drugstore_id->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_drugstore_id">
<span<?php echo $telecare_prescription->prescription_drugstore_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_drugstore_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_doctor_id->Visible) { // prescription_doctor_id ?>
		<tr id="r_prescription_doctor_id">
			<td><?php echo $telecare_prescription->prescription_doctor_id->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_doctor_id->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_doctor_id">
<span<?php echo $telecare_prescription->prescription_doctor_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_doctor_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_admin_id->Visible) { // prescription_admin_id ?>
		<tr id="r_prescription_admin_id">
			<td><?php echo $telecare_prescription->prescription_admin_id->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_admin_id->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_admin_id">
<span<?php echo $telecare_prescription->prescription_admin_id->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_admin_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_request_date->Visible) { // prescription_request_date ?>
		<tr id="r_prescription_request_date">
			<td><?php echo $telecare_prescription->prescription_request_date->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_request_date->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_request_date">
<span<?php echo $telecare_prescription->prescription_request_date->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_request_date->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_start_on->Visible) { // prescription_start_on ?>
		<tr id="r_prescription_start_on">
			<td><?php echo $telecare_prescription->prescription_start_on->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_start_on->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_start_on">
<span<?php echo $telecare_prescription->prescription_start_on->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_start_on->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_close_on->Visible) { // prescription_close_on ?>
		<tr id="r_prescription_close_on">
			<td><?php echo $telecare_prescription->prescription_close_on->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_close_on->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_close_on">
<span<?php echo $telecare_prescription->prescription_close_on->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_close_on->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_prescription->prescription_close->Visible) { // prescription_close ?>
		<tr id="r_prescription_close">
			<td><?php echo $telecare_prescription->prescription_close->FldCaption() ?></td>
			<td<?php echo $telecare_prescription->prescription_close->CellAttributes() ?>>
<span id="el_telecare_prescription_prescription_close">
<span<?php echo $telecare_prescription->prescription_close->ViewAttributes() ?>>
<?php echo $telecare_prescription->prescription_close->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
