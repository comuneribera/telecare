<?php

// Global variable for table object
$telecare_doctor_user = NULL;

//
// Table class for telecare_doctor_user
//
class ctelecare_doctor_user extends cTable {
	var $user_id;
	var $user_admin_id;
	var $user_doctor_id;
	var $user_username;
	var $user_password;
	var $user_name;
	var $user_surname;
	var $user_born;
	var $user_gender;
	var $user_email;
	var $user_phone;
	var $user_address;
	var $user_region_id;
	var $user_province_id;
	var $user_city_id;
	var $user_latitude;
	var $user_longitude;
	var $user_modules;
	var $user_medical_history;
	var $user_level;
	var $user_language;
	var $user_last_update;
	var $user_is_active;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'telecare_doctor_user';
		$this->TableName = 'telecare_doctor_user';
		$this->TableType = 'CUSTOMVIEW';

		// Update Table
		$this->UpdateTable = "telecare_user";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = PHPExcel_Worksheet_PageSetup::ORIENTATION_DEFAULT; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// user_id
		$this->user_id = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_id', 'user_id', '`user_id`', '`user_id`', 3, -1, FALSE, '`user_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->user_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_id'] = &$this->user_id;

		// user_admin_id
		$this->user_admin_id = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_admin_id', 'user_admin_id', '`user_admin_id`', '`user_admin_id`', 3, -1, FALSE, '`user_admin_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->user_admin_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_admin_id'] = &$this->user_admin_id;

		// user_doctor_id
		$this->user_doctor_id = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_doctor_id', 'user_doctor_id', '`user_doctor_id`', '`user_doctor_id`', 3, -1, FALSE, '`user_doctor_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->user_doctor_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_doctor_id'] = &$this->user_doctor_id;

		// user_username
		$this->user_username = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_username', 'user_username', '`user_username`', '`user_username`', 200, -1, FALSE, '`user_username`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['user_username'] = &$this->user_username;

		// user_password
		$this->user_password = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_password', 'user_password', '`user_password`', '`user_password`', 200, -1, FALSE, '`user_password`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'PASSWORD');
		$this->fields['user_password'] = &$this->user_password;

		// user_name
		$this->user_name = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_name', 'user_name', '`user_name`', '`user_name`', 200, -1, FALSE, '`user_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['user_name'] = &$this->user_name;

		// user_surname
		$this->user_surname = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_surname', 'user_surname', '`user_surname`', '`user_surname`', 200, -1, FALSE, '`user_surname`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['user_surname'] = &$this->user_surname;

		// user_born
		$this->user_born = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_born', 'user_born', '`user_born`', 'DATE_FORMAT(`user_born`, \'%d/%m/%Y\')', 133, 7, FALSE, '`user_born`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->user_born->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['user_born'] = &$this->user_born;

		// user_gender
		$this->user_gender = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_gender', 'user_gender', '`user_gender`', '`user_gender`', 16, -1, FALSE, '`user_gender`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'RADIO');
		$this->user_gender->OptionCount = 2;
		$this->user_gender->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_gender'] = &$this->user_gender;

		// user_email
		$this->user_email = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_email', 'user_email', '`user_email`', '`user_email`', 200, -1, FALSE, '`user_email`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->user_email->FldDefaultErrMsg = $Language->Phrase("IncorrectEmail");
		$this->fields['user_email'] = &$this->user_email;

		// user_phone
		$this->user_phone = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_phone', 'user_phone', '`user_phone`', '`user_phone`', 200, -1, FALSE, '`user_phone`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['user_phone'] = &$this->user_phone;

		// user_address
		$this->user_address = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_address', 'user_address', '`user_address`', '`user_address`', 200, -1, FALSE, '`user_address`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['user_address'] = &$this->user_address;

		// user_region_id
		$this->user_region_id = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_region_id', 'user_region_id', '`user_region_id`', '`user_region_id`', 3, -1, FALSE, '`user_region_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->user_region_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_region_id'] = &$this->user_region_id;

		// user_province_id
		$this->user_province_id = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_province_id', 'user_province_id', '`user_province_id`', '`user_province_id`', 3, -1, FALSE, '`user_province_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->user_province_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_province_id'] = &$this->user_province_id;

		// user_city_id
		$this->user_city_id = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_city_id', 'user_city_id', '`user_city_id`', '`user_city_id`', 3, -1, FALSE, '`user_city_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->fields['user_city_id'] = &$this->user_city_id;

		// user_latitude
		$this->user_latitude = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_latitude', 'user_latitude', '`user_latitude`', '`user_latitude`', 200, -1, FALSE, '`user_latitude`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['user_latitude'] = &$this->user_latitude;

		// user_longitude
		$this->user_longitude = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_longitude', 'user_longitude', '`user_longitude`', '`user_longitude`', 200, -1, FALSE, '`user_longitude`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['user_longitude'] = &$this->user_longitude;

		// user_modules
		$this->user_modules = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_modules', 'user_modules', '`user_modules`', '`user_modules`', 200, -1, FALSE, '`user_modules`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'CHECKBOX');
		$this->fields['user_modules'] = &$this->user_modules;

		// user_medical_history
		$this->user_medical_history = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_medical_history', 'user_medical_history', '`user_medical_history`', '`user_medical_history`', 200, -1, FALSE, '`user_medical_history`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXTAREA');
		$this->fields['user_medical_history'] = &$this->user_medical_history;

		// user_level
		$this->user_level = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_level', 'user_level', '`user_level`', '`user_level`', 3, -1, FALSE, '`user_level`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->user_level->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_level'] = &$this->user_level;

		// user_language
		$this->user_language = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_language', 'user_language', '`user_language`', '`user_language`', 3, -1, FALSE, '`user_language`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->user_language->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_language'] = &$this->user_language;

		// user_last_update
		$this->user_last_update = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_last_update', 'user_last_update', '`user_last_update`', 'DATE_FORMAT(`user_last_update`, \'%d/%m/%Y\')', 135, 7, FALSE, '`user_last_update`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->user_last_update->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['user_last_update'] = &$this->user_last_update;

		// user_is_active
		$this->user_is_active = new cField('telecare_doctor_user', 'telecare_doctor_user', 'x_user_is_active', 'user_is_active', '`user_is_active`', '`user_is_active`', 16, -1, FALSE, '`user_is_active`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->user_is_active->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['user_is_active'] = &$this->user_is_active;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "telecare_user";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT telecare_user.* FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "user_doctor_id";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		global $Security;

		// Add User ID filter
		if ($Security->CurrentUserID() <> "" && !$Security->IsAdmin()) { // Non system admin
			$sFilter = $this->AddUserIDFilter($sFilter);
		}
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = $this->UserIDAllowSecurity;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "telecare_doctor_userlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "telecare_doctor_userlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_doctor_userview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_doctor_userview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "telecare_doctor_useradd.php?" . $this->UrlParm($parm);
		else
			$url = "telecare_doctor_useradd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("telecare_doctor_useredit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("telecare_doctor_useradd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("telecare_doctor_userdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->user_id->setDbValue($rs->fields('user_id'));
		$this->user_admin_id->setDbValue($rs->fields('user_admin_id'));
		$this->user_doctor_id->setDbValue($rs->fields('user_doctor_id'));
		$this->user_username->setDbValue($rs->fields('user_username'));
		$this->user_password->setDbValue($rs->fields('user_password'));
		$this->user_name->setDbValue($rs->fields('user_name'));
		$this->user_surname->setDbValue($rs->fields('user_surname'));
		$this->user_born->setDbValue($rs->fields('user_born'));
		$this->user_gender->setDbValue($rs->fields('user_gender'));
		$this->user_email->setDbValue($rs->fields('user_email'));
		$this->user_phone->setDbValue($rs->fields('user_phone'));
		$this->user_address->setDbValue($rs->fields('user_address'));
		$this->user_region_id->setDbValue($rs->fields('user_region_id'));
		$this->user_province_id->setDbValue($rs->fields('user_province_id'));
		$this->user_city_id->setDbValue($rs->fields('user_city_id'));
		$this->user_latitude->setDbValue($rs->fields('user_latitude'));
		$this->user_longitude->setDbValue($rs->fields('user_longitude'));
		$this->user_modules->setDbValue($rs->fields('user_modules'));
		$this->user_medical_history->setDbValue($rs->fields('user_medical_history'));
		$this->user_level->setDbValue($rs->fields('user_level'));
		$this->user_language->setDbValue($rs->fields('user_language'));
		$this->user_last_update->setDbValue($rs->fields('user_last_update'));
		$this->user_is_active->setDbValue($rs->fields('user_is_active'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// user_id
		// user_admin_id

		$this->user_admin_id->CellCssStyle = "white-space: nowrap;";

		// user_doctor_id
		// user_username
		// user_password
		// user_name
		// user_surname
		// user_born
		// user_gender
		// user_email
		// user_phone
		// user_address
		// user_region_id
		// user_province_id
		// user_city_id
		// user_latitude
		// user_longitude
		// user_modules
		// user_medical_history
		// user_level
		// user_language
		// user_last_update
		// user_is_active
		// user_id

		$this->user_id->ViewValue = $this->user_id->CurrentValue;
		$this->user_id->ViewCustomAttributes = "";

		// user_admin_id
		$this->user_admin_id->ViewValue = $this->user_admin_id->CurrentValue;
		$this->user_admin_id->ViewCustomAttributes = "";

		// user_doctor_id
		if (strval($this->user_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = "`admin_level` = 3";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->CurrentValue;
			}
		} else {
			$this->user_doctor_id->ViewValue = NULL;
		}
		$this->user_doctor_id->ViewCustomAttributes = "";

		// user_username
		$this->user_username->ViewValue = $this->user_username->CurrentValue;
		$this->user_username->ViewCustomAttributes = "";

		// user_password
		$this->user_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->user_password->ViewCustomAttributes = "";

		// user_name
		$this->user_name->ViewValue = $this->user_name->CurrentValue;
		$this->user_name->ViewCustomAttributes = "";

		// user_surname
		$this->user_surname->ViewValue = $this->user_surname->CurrentValue;
		$this->user_surname->ViewCustomAttributes = "";

		// user_born
		$this->user_born->ViewValue = $this->user_born->CurrentValue;
		$this->user_born->ViewValue = ew_FormatDateTime($this->user_born->ViewValue, 7);
		$this->user_born->ViewCustomAttributes = "";

		// user_gender
		if (strval($this->user_gender->CurrentValue) <> "") {
			$this->user_gender->ViewValue = $this->user_gender->OptionCaption($this->user_gender->CurrentValue);
		} else {
			$this->user_gender->ViewValue = NULL;
		}
		$this->user_gender->ViewCustomAttributes = "";

		// user_email
		$this->user_email->ViewValue = $this->user_email->CurrentValue;
		$this->user_email->ViewCustomAttributes = "";

		// user_phone
		$this->user_phone->ViewValue = $this->user_phone->CurrentValue;
		$this->user_phone->ViewCustomAttributes = "";

		// user_address
		$this->user_address->ViewValue = $this->user_address->CurrentValue;
		$this->user_address->ViewCustomAttributes = "";

		// user_region_id
		if (strval($this->user_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->user_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `region_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_region_id->ViewValue = $this->user_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_region_id->ViewValue = $this->user_region_id->CurrentValue;
			}
		} else {
			$this->user_region_id->ViewValue = NULL;
		}
		$this->user_region_id->ViewCustomAttributes = "";

		// user_province_id
		if (strval($this->user_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->user_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `province_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_province_id->ViewValue = $this->user_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_province_id->ViewValue = $this->user_province_id->CurrentValue;
			}
		} else {
			$this->user_province_id->ViewValue = NULL;
		}
		$this->user_province_id->ViewCustomAttributes = "";

		// user_city_id
		if (strval($this->user_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_city_id->ViewValue = $this->user_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_city_id->ViewValue = $this->user_city_id->CurrentValue;
			}
		} else {
			$this->user_city_id->ViewValue = NULL;
		}
		$this->user_city_id->ViewCustomAttributes = "";

		// user_latitude
		$this->user_latitude->ViewValue = $this->user_latitude->CurrentValue;
		$this->user_latitude->ViewCustomAttributes = "";

		// user_longitude
		$this->user_longitude->ViewValue = $this->user_longitude->CurrentValue;
		$this->user_longitude->ViewCustomAttributes = "";

		// user_modules
		if (strval($this->user_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->user_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->user_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->user_modules->ViewValue .= $this->user_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->user_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->user_modules->ViewValue = $this->user_modules->CurrentValue;
			}
		} else {
			$this->user_modules->ViewValue = NULL;
		}
		$this->user_modules->ViewCustomAttributes = "";

		// user_medical_history
		$this->user_medical_history->ViewValue = $this->user_medical_history->CurrentValue;
		$this->user_medical_history->ViewCustomAttributes = "";

		// user_level
		$this->user_level->ViewValue = $this->user_level->CurrentValue;
		$this->user_level->ViewCustomAttributes = "";

		// user_language
		$this->user_language->ViewValue = $this->user_language->CurrentValue;
		$this->user_language->ViewCustomAttributes = "";

		// user_last_update
		$this->user_last_update->ViewValue = $this->user_last_update->CurrentValue;
		$this->user_last_update->ViewValue = ew_FormatDateTime($this->user_last_update->ViewValue, 7);
		$this->user_last_update->ViewCustomAttributes = "";

		// user_is_active
		$this->user_is_active->ViewValue = $this->user_is_active->CurrentValue;
		$this->user_is_active->ViewCustomAttributes = "";

		// user_id
		$this->user_id->LinkCustomAttributes = "";
		$this->user_id->HrefValue = "";
		$this->user_id->TooltipValue = "";

		// user_admin_id
		$this->user_admin_id->LinkCustomAttributes = "";
		$this->user_admin_id->HrefValue = "";
		$this->user_admin_id->TooltipValue = "";

		// user_doctor_id
		$this->user_doctor_id->LinkCustomAttributes = "";
		$this->user_doctor_id->HrefValue = "";
		$this->user_doctor_id->TooltipValue = "";

		// user_username
		$this->user_username->LinkCustomAttributes = "";
		$this->user_username->HrefValue = "";
		$this->user_username->TooltipValue = "";

		// user_password
		$this->user_password->LinkCustomAttributes = "";
		$this->user_password->HrefValue = "";
		$this->user_password->TooltipValue = "";

		// user_name
		$this->user_name->LinkCustomAttributes = "";
		$this->user_name->HrefValue = "";
		$this->user_name->TooltipValue = "";

		// user_surname
		$this->user_surname->LinkCustomAttributes = "";
		$this->user_surname->HrefValue = "";
		$this->user_surname->TooltipValue = "";

		// user_born
		$this->user_born->LinkCustomAttributes = "";
		$this->user_born->HrefValue = "";
		$this->user_born->TooltipValue = "";

		// user_gender
		$this->user_gender->LinkCustomAttributes = "";
		$this->user_gender->HrefValue = "";
		$this->user_gender->TooltipValue = "";

		// user_email
		$this->user_email->LinkCustomAttributes = "";
		$this->user_email->HrefValue = "";
		$this->user_email->TooltipValue = "";

		// user_phone
		$this->user_phone->LinkCustomAttributes = "";
		$this->user_phone->HrefValue = "";
		$this->user_phone->TooltipValue = "";

		// user_address
		$this->user_address->LinkCustomAttributes = "";
		$this->user_address->HrefValue = "";
		$this->user_address->TooltipValue = "";

		// user_region_id
		$this->user_region_id->LinkCustomAttributes = "";
		$this->user_region_id->HrefValue = "";
		$this->user_region_id->TooltipValue = "";

		// user_province_id
		$this->user_province_id->LinkCustomAttributes = "";
		$this->user_province_id->HrefValue = "";
		$this->user_province_id->TooltipValue = "";

		// user_city_id
		$this->user_city_id->LinkCustomAttributes = "";
		$this->user_city_id->HrefValue = "";
		$this->user_city_id->TooltipValue = "";

		// user_latitude
		$this->user_latitude->LinkCustomAttributes = "";
		$this->user_latitude->HrefValue = "";
		$this->user_latitude->TooltipValue = "";

		// user_longitude
		$this->user_longitude->LinkCustomAttributes = "";
		$this->user_longitude->HrefValue = "";
		$this->user_longitude->TooltipValue = "";

		// user_modules
		$this->user_modules->LinkCustomAttributes = "";
		$this->user_modules->HrefValue = "";
		$this->user_modules->TooltipValue = "";

		// user_medical_history
		$this->user_medical_history->LinkCustomAttributes = "";
		$this->user_medical_history->HrefValue = "";
		$this->user_medical_history->TooltipValue = "";

		// user_level
		$this->user_level->LinkCustomAttributes = "";
		$this->user_level->HrefValue = "";
		$this->user_level->TooltipValue = "";

		// user_language
		$this->user_language->LinkCustomAttributes = "";
		$this->user_language->HrefValue = "";
		$this->user_language->TooltipValue = "";

		// user_last_update
		$this->user_last_update->LinkCustomAttributes = "";
		$this->user_last_update->HrefValue = "";
		$this->user_last_update->TooltipValue = "";

		// user_is_active
		$this->user_is_active->LinkCustomAttributes = "";
		$this->user_is_active->HrefValue = "";
		$this->user_is_active->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// user_id
		$this->user_id->EditAttrs["class"] = "form-control";
		$this->user_id->EditCustomAttributes = "";
		$this->user_id->EditValue = $this->user_id->CurrentValue;
		$this->user_id->PlaceHolder = ew_RemoveHtml($this->user_id->FldCaption());

		// user_admin_id
		// user_doctor_id

		$this->user_doctor_id->EditAttrs["class"] = "form-control";
		$this->user_doctor_id->EditCustomAttributes = "";
		if (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$this->UserIDAllow("info")) { // Non system admin
		$sFilterWrk = "";
		$sFilterWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter("");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = "`admin_level` = 3";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
		$rswrk = Conn()->Execute($sSqlWrk);
		$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
		if ($rswrk) $rswrk->Close();
		array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
		$this->user_doctor_id->EditValue = $arwrk;
		} else {
		}

		// user_username
		$this->user_username->EditAttrs["class"] = "form-control";
		$this->user_username->EditCustomAttributes = "";
		$this->user_username->EditValue = $this->user_username->CurrentValue;
		$this->user_username->PlaceHolder = ew_RemoveHtml($this->user_username->FldCaption());

		// user_password
		$this->user_password->EditAttrs["class"] = "form-control";
		$this->user_password->EditCustomAttributes = "";
		$this->user_password->EditValue = $this->user_password->CurrentValue;
		$this->user_password->PlaceHolder = ew_RemoveHtml($this->user_password->FldCaption());

		// user_name
		$this->user_name->EditAttrs["class"] = "form-control";
		$this->user_name->EditCustomAttributes = "";
		$this->user_name->EditValue = $this->user_name->CurrentValue;
		$this->user_name->PlaceHolder = ew_RemoveHtml($this->user_name->FldCaption());

		// user_surname
		$this->user_surname->EditAttrs["class"] = "form-control";
		$this->user_surname->EditCustomAttributes = "";
		$this->user_surname->EditValue = $this->user_surname->CurrentValue;
		$this->user_surname->PlaceHolder = ew_RemoveHtml($this->user_surname->FldCaption());

		// user_born
		$this->user_born->EditAttrs["class"] = "form-control";
		$this->user_born->EditCustomAttributes = "";
		$this->user_born->EditValue = ew_FormatDateTime($this->user_born->CurrentValue, 7);
		$this->user_born->PlaceHolder = ew_RemoveHtml($this->user_born->FldCaption());

		// user_gender
		$this->user_gender->EditCustomAttributes = "";
		$this->user_gender->EditValue = $this->user_gender->Options(FALSE);

		// user_email
		$this->user_email->EditAttrs["class"] = "form-control";
		$this->user_email->EditCustomAttributes = "";
		$this->user_email->EditValue = $this->user_email->CurrentValue;
		$this->user_email->PlaceHolder = ew_RemoveHtml($this->user_email->FldCaption());

		// user_phone
		$this->user_phone->EditAttrs["class"] = "form-control";
		$this->user_phone->EditCustomAttributes = "";
		$this->user_phone->EditValue = $this->user_phone->CurrentValue;
		$this->user_phone->PlaceHolder = ew_RemoveHtml($this->user_phone->FldCaption());

		// user_address
		$this->user_address->EditAttrs["class"] = "form-control";
		$this->user_address->EditCustomAttributes = "";
		$this->user_address->EditValue = $this->user_address->CurrentValue;
		$this->user_address->PlaceHolder = ew_RemoveHtml($this->user_address->FldCaption());

		// user_region_id
		$this->user_region_id->EditAttrs["class"] = "form-control";
		$this->user_region_id->EditCustomAttributes = "";

		// user_province_id
		$this->user_province_id->EditAttrs["class"] = "form-control";
		$this->user_province_id->EditCustomAttributes = "";

		// user_city_id
		$this->user_city_id->EditAttrs["class"] = "form-control";
		$this->user_city_id->EditCustomAttributes = "";

		// user_latitude
		$this->user_latitude->EditAttrs["class"] = "form-control";
		$this->user_latitude->EditCustomAttributes = "";
		$this->user_latitude->EditValue = $this->user_latitude->CurrentValue;
		$this->user_latitude->PlaceHolder = ew_RemoveHtml($this->user_latitude->FldCaption());

		// user_longitude
		$this->user_longitude->EditAttrs["class"] = "form-control";
		$this->user_longitude->EditCustomAttributes = "";
		$this->user_longitude->EditValue = $this->user_longitude->CurrentValue;
		$this->user_longitude->PlaceHolder = ew_RemoveHtml($this->user_longitude->FldCaption());

		// user_modules
		$this->user_modules->EditCustomAttributes = "";

		// user_medical_history
		$this->user_medical_history->EditAttrs["class"] = "form-control";
		$this->user_medical_history->EditCustomAttributes = "";
		$this->user_medical_history->EditValue = $this->user_medical_history->CurrentValue;
		$this->user_medical_history->PlaceHolder = ew_RemoveHtml($this->user_medical_history->FldCaption());

		// user_level
		$this->user_level->EditAttrs["class"] = "form-control";
		$this->user_level->EditCustomAttributes = "";
		$this->user_level->EditValue = $this->user_level->CurrentValue;
		$this->user_level->PlaceHolder = ew_RemoveHtml($this->user_level->FldCaption());

		// user_language
		$this->user_language->EditAttrs["class"] = "form-control";
		$this->user_language->EditCustomAttributes = "";
		$this->user_language->EditValue = $this->user_language->CurrentValue;
		$this->user_language->PlaceHolder = ew_RemoveHtml($this->user_language->FldCaption());

		// user_last_update
		$this->user_last_update->EditAttrs["class"] = "form-control";
		$this->user_last_update->EditCustomAttributes = "";
		$this->user_last_update->EditValue = ew_FormatDateTime($this->user_last_update->CurrentValue, 7);
		$this->user_last_update->PlaceHolder = ew_RemoveHtml($this->user_last_update->FldCaption());

		// user_is_active
		$this->user_is_active->EditAttrs["class"] = "form-control";
		$this->user_is_active->EditCustomAttributes = "";
		$this->user_is_active->EditValue = $this->user_is_active->CurrentValue;
		$this->user_is_active->PlaceHolder = ew_RemoveHtml($this->user_is_active->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->user_id->Exportable) $Doc->ExportCaption($this->user_id);
					if ($this->user_admin_id->Exportable) $Doc->ExportCaption($this->user_admin_id);
					if ($this->user_doctor_id->Exportable) $Doc->ExportCaption($this->user_doctor_id);
					if ($this->user_name->Exportable) $Doc->ExportCaption($this->user_name);
					if ($this->user_surname->Exportable) $Doc->ExportCaption($this->user_surname);
					if ($this->user_born->Exportable) $Doc->ExportCaption($this->user_born);
					if ($this->user_gender->Exportable) $Doc->ExportCaption($this->user_gender);
					if ($this->user_email->Exportable) $Doc->ExportCaption($this->user_email);
					if ($this->user_phone->Exportable) $Doc->ExportCaption($this->user_phone);
					if ($this->user_address->Exportable) $Doc->ExportCaption($this->user_address);
					if ($this->user_region_id->Exportable) $Doc->ExportCaption($this->user_region_id);
					if ($this->user_province_id->Exportable) $Doc->ExportCaption($this->user_province_id);
					if ($this->user_city_id->Exportable) $Doc->ExportCaption($this->user_city_id);
					if ($this->user_latitude->Exportable) $Doc->ExportCaption($this->user_latitude);
					if ($this->user_longitude->Exportable) $Doc->ExportCaption($this->user_longitude);
					if ($this->user_modules->Exportable) $Doc->ExportCaption($this->user_modules);
					if ($this->user_medical_history->Exportable) $Doc->ExportCaption($this->user_medical_history);
					if ($this->user_level->Exportable) $Doc->ExportCaption($this->user_level);
					if ($this->user_language->Exportable) $Doc->ExportCaption($this->user_language);
					if ($this->user_last_update->Exportable) $Doc->ExportCaption($this->user_last_update);
					if ($this->user_is_active->Exportable) $Doc->ExportCaption($this->user_is_active);
				} else {
					if ($this->user_id->Exportable) $Doc->ExportCaption($this->user_id);
					if ($this->user_doctor_id->Exportable) $Doc->ExportCaption($this->user_doctor_id);
					if ($this->user_username->Exportable) $Doc->ExportCaption($this->user_username);
					if ($this->user_password->Exportable) $Doc->ExportCaption($this->user_password);
					if ($this->user_name->Exportable) $Doc->ExportCaption($this->user_name);
					if ($this->user_surname->Exportable) $Doc->ExportCaption($this->user_surname);
					if ($this->user_born->Exportable) $Doc->ExportCaption($this->user_born);
					if ($this->user_gender->Exportable) $Doc->ExportCaption($this->user_gender);
					if ($this->user_email->Exportable) $Doc->ExportCaption($this->user_email);
					if ($this->user_phone->Exportable) $Doc->ExportCaption($this->user_phone);
					if ($this->user_address->Exportable) $Doc->ExportCaption($this->user_address);
					if ($this->user_region_id->Exportable) $Doc->ExportCaption($this->user_region_id);
					if ($this->user_province_id->Exportable) $Doc->ExportCaption($this->user_province_id);
					if ($this->user_city_id->Exportable) $Doc->ExportCaption($this->user_city_id);
					if ($this->user_latitude->Exportable) $Doc->ExportCaption($this->user_latitude);
					if ($this->user_longitude->Exportable) $Doc->ExportCaption($this->user_longitude);
					if ($this->user_modules->Exportable) $Doc->ExportCaption($this->user_modules);
					if ($this->user_medical_history->Exportable) $Doc->ExportCaption($this->user_medical_history);
					if ($this->user_level->Exportable) $Doc->ExportCaption($this->user_level);
					if ($this->user_language->Exportable) $Doc->ExportCaption($this->user_language);
					if ($this->user_last_update->Exportable) $Doc->ExportCaption($this->user_last_update);
					if ($this->user_is_active->Exportable) $Doc->ExportCaption($this->user_is_active);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->user_id->Exportable) $Doc->ExportField($this->user_id);
						if ($this->user_admin_id->Exportable) $Doc->ExportField($this->user_admin_id);
						if ($this->user_doctor_id->Exportable) $Doc->ExportField($this->user_doctor_id);
						if ($this->user_name->Exportable) $Doc->ExportField($this->user_name);
						if ($this->user_surname->Exportable) $Doc->ExportField($this->user_surname);
						if ($this->user_born->Exportable) $Doc->ExportField($this->user_born);
						if ($this->user_gender->Exportable) $Doc->ExportField($this->user_gender);
						if ($this->user_email->Exportable) $Doc->ExportField($this->user_email);
						if ($this->user_phone->Exportable) $Doc->ExportField($this->user_phone);
						if ($this->user_address->Exportable) $Doc->ExportField($this->user_address);
						if ($this->user_region_id->Exportable) $Doc->ExportField($this->user_region_id);
						if ($this->user_province_id->Exportable) $Doc->ExportField($this->user_province_id);
						if ($this->user_city_id->Exportable) $Doc->ExportField($this->user_city_id);
						if ($this->user_latitude->Exportable) $Doc->ExportField($this->user_latitude);
						if ($this->user_longitude->Exportable) $Doc->ExportField($this->user_longitude);
						if ($this->user_modules->Exportable) $Doc->ExportField($this->user_modules);
						if ($this->user_medical_history->Exportable) $Doc->ExportField($this->user_medical_history);
						if ($this->user_level->Exportable) $Doc->ExportField($this->user_level);
						if ($this->user_language->Exportable) $Doc->ExportField($this->user_language);
						if ($this->user_last_update->Exportable) $Doc->ExportField($this->user_last_update);
						if ($this->user_is_active->Exportable) $Doc->ExportField($this->user_is_active);
					} else {
						if ($this->user_id->Exportable) $Doc->ExportField($this->user_id);
						if ($this->user_doctor_id->Exportable) $Doc->ExportField($this->user_doctor_id);
						if ($this->user_username->Exportable) $Doc->ExportField($this->user_username);
						if ($this->user_password->Exportable) $Doc->ExportField($this->user_password);
						if ($this->user_name->Exportable) $Doc->ExportField($this->user_name);
						if ($this->user_surname->Exportable) $Doc->ExportField($this->user_surname);
						if ($this->user_born->Exportable) $Doc->ExportField($this->user_born);
						if ($this->user_gender->Exportable) $Doc->ExportField($this->user_gender);
						if ($this->user_email->Exportable) $Doc->ExportField($this->user_email);
						if ($this->user_phone->Exportable) $Doc->ExportField($this->user_phone);
						if ($this->user_address->Exportable) $Doc->ExportField($this->user_address);
						if ($this->user_region_id->Exportable) $Doc->ExportField($this->user_region_id);
						if ($this->user_province_id->Exportable) $Doc->ExportField($this->user_province_id);
						if ($this->user_city_id->Exportable) $Doc->ExportField($this->user_city_id);
						if ($this->user_latitude->Exportable) $Doc->ExportField($this->user_latitude);
						if ($this->user_longitude->Exportable) $Doc->ExportField($this->user_longitude);
						if ($this->user_modules->Exportable) $Doc->ExportField($this->user_modules);
						if ($this->user_medical_history->Exportable) $Doc->ExportField($this->user_medical_history);
						if ($this->user_level->Exportable) $Doc->ExportField($this->user_level);
						if ($this->user_language->Exportable) $Doc->ExportField($this->user_language);
						if ($this->user_last_update->Exportable) $Doc->ExportField($this->user_last_update);
						if ($this->user_is_active->Exportable) $Doc->ExportField($this->user_is_active);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Add User ID filter
	function AddUserIDFilter($sFilter) {
		global $Security;
		$sFilterWrk = "";
		$id = (CurrentPageID() == "list") ? $this->CurrentAction : CurrentPageID();
		if (!$this->UserIDAllow($id) && !$Security->IsAdmin()) {
			$sFilterWrk = $Security->UserIDList();
			if ($sFilterWrk <> "")
				$sFilterWrk = '`user_doctor_id` IN (' . $sFilterWrk . ')';
		}

		// Call User ID Filtering event
		$this->UserID_Filtering($sFilterWrk);
		ew_AddFilter($sFilter, $sFilterWrk);
		return $sFilter;
	}

	// User ID subquery
	function GetUserIDSubquery(&$fld, &$masterfld) {
		global $UserTableConn;
		$sWrk = "";
		$sSql = "SELECT " . $masterfld->FldExpression . " FROM telecare_user";
		$sFilter = $this->AddUserIDFilter("");
		if ($sFilter <> "") $sSql .= " WHERE " . $sFilter;

		// Use subquery
		if (EW_USE_SUBQUERY_FOR_MASTER_USER_ID) {
			$sWrk = $sSql;
		} else {

			// List all values
			if ($rs = $UserTableConn->Execute($sSql)) {
				while (!$rs->EOF) {
					if ($sWrk <> "") $sWrk .= ",";
					$sWrk .= ew_QuotedValue($rs->fields[0], $masterfld->FldDataType, EW_USER_TABLE_DBID);
					$rs->MoveNext();
				}
				$rs->Close();
			}
		}
		if ($sWrk <> "") {
			$sWrk = $fld->FldExpression . " IN (" . $sWrk . ")";
		}
		return $sWrk;
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
