<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_provinceinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_province_delete = NULL; // Initialize page object first

class ctelecare_province_delete extends ctelecare_province {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_province';

	// Page object name
	var $PageObjName = 'telecare_province_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_province)
		if (!isset($GLOBALS["telecare_province"]) || get_class($GLOBALS["telecare_province"]) == "ctelecare_province") {
			$GLOBALS["telecare_province"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_province"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_province', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_provincelist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->province_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_province;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_province);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("telecare_provincelist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in telecare_province class, telecare_provinceinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->province_id->setDbValue($rs->fields('province_id'));
		$this->province_name->setDbValue($rs->fields('province_name'));
		$this->province_slug->setDbValue($rs->fields('province_slug'));
		$this->province_abbr->setDbValue($rs->fields('province_abbr'));
		$this->province_istat_code->setDbValue($rs->fields('province_istat_code'));
		$this->province_region_istat_code->setDbValue($rs->fields('province_region_istat_code'));
		$this->province_region_id->setDbValue($rs->fields('province_region_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->province_id->DbValue = $row['province_id'];
		$this->province_name->DbValue = $row['province_name'];
		$this->province_slug->DbValue = $row['province_slug'];
		$this->province_abbr->DbValue = $row['province_abbr'];
		$this->province_istat_code->DbValue = $row['province_istat_code'];
		$this->province_region_istat_code->DbValue = $row['province_region_istat_code'];
		$this->province_region_id->DbValue = $row['province_region_id'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// province_id
		// province_name
		// province_slug
		// province_abbr
		// province_istat_code
		// province_region_istat_code
		// province_region_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// province_id
		$this->province_id->ViewValue = $this->province_id->CurrentValue;
		$this->province_id->ViewCustomAttributes = "";

		// province_name
		$this->province_name->ViewValue = $this->province_name->CurrentValue;
		$this->province_name->ViewCustomAttributes = "";

		// province_slug
		$this->province_slug->ViewValue = $this->province_slug->CurrentValue;
		$this->province_slug->ViewCustomAttributes = "";

		// province_abbr
		$this->province_abbr->ViewValue = $this->province_abbr->CurrentValue;
		$this->province_abbr->ViewCustomAttributes = "";

		// province_istat_code
		$this->province_istat_code->ViewValue = $this->province_istat_code->CurrentValue;
		$this->province_istat_code->ViewCustomAttributes = "";

		// province_region_istat_code
		$this->province_region_istat_code->ViewValue = $this->province_region_istat_code->CurrentValue;
		$this->province_region_istat_code->ViewCustomAttributes = "";

		// province_region_id
		$this->province_region_id->ViewValue = $this->province_region_id->CurrentValue;
		$this->province_region_id->ViewCustomAttributes = "";

			// province_id
			$this->province_id->LinkCustomAttributes = "";
			$this->province_id->HrefValue = "";
			$this->province_id->TooltipValue = "";

			// province_name
			$this->province_name->LinkCustomAttributes = "";
			$this->province_name->HrefValue = "";
			$this->province_name->TooltipValue = "";

			// province_slug
			$this->province_slug->LinkCustomAttributes = "";
			$this->province_slug->HrefValue = "";
			$this->province_slug->TooltipValue = "";

			// province_abbr
			$this->province_abbr->LinkCustomAttributes = "";
			$this->province_abbr->HrefValue = "";
			$this->province_abbr->TooltipValue = "";

			// province_istat_code
			$this->province_istat_code->LinkCustomAttributes = "";
			$this->province_istat_code->HrefValue = "";
			$this->province_istat_code->TooltipValue = "";

			// province_region_istat_code
			$this->province_region_istat_code->LinkCustomAttributes = "";
			$this->province_region_istat_code->HrefValue = "";
			$this->province_region_istat_code->TooltipValue = "";

			// province_region_id
			$this->province_region_id->LinkCustomAttributes = "";
			$this->province_region_id->HrefValue = "";
			$this->province_region_id->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['province_id'];
				$this->LoadDbValues($row);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_provincelist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_province_delete)) $telecare_province_delete = new ctelecare_province_delete();

// Page init
$telecare_province_delete->Page_Init();

// Page main
$telecare_province_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_province_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftelecare_provincedelete = new ew_Form("ftelecare_provincedelete", "delete");

// Form_CustomValidate event
ftelecare_provincedelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_provincedelete.ValidateRequired = true;
<?php } else { ?>
ftelecare_provincedelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($telecare_province_delete->Recordset = $telecare_province_delete->LoadRecordset())
	$telecare_province_deleteTotalRecs = $telecare_province_delete->Recordset->RecordCount(); // Get record count
if ($telecare_province_deleteTotalRecs <= 0) { // No record found, exit
	if ($telecare_province_delete->Recordset)
		$telecare_province_delete->Recordset->Close();
	$telecare_province_delete->Page_Terminate("telecare_provincelist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_province_delete->ShowPageHeader(); ?>
<?php
$telecare_province_delete->ShowMessage();
?>
<form name="ftelecare_provincedelete" id="ftelecare_provincedelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_province_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_province_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_province">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($telecare_province_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $telecare_province->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($telecare_province->province_id->Visible) { // province_id ?>
		<th><span id="elh_telecare_province_province_id" class="telecare_province_province_id"><?php echo $telecare_province->province_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_province->province_name->Visible) { // province_name ?>
		<th><span id="elh_telecare_province_province_name" class="telecare_province_province_name"><?php echo $telecare_province->province_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_province->province_slug->Visible) { // province_slug ?>
		<th><span id="elh_telecare_province_province_slug" class="telecare_province_province_slug"><?php echo $telecare_province->province_slug->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_province->province_abbr->Visible) { // province_abbr ?>
		<th><span id="elh_telecare_province_province_abbr" class="telecare_province_province_abbr"><?php echo $telecare_province->province_abbr->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_province->province_istat_code->Visible) { // province_istat_code ?>
		<th><span id="elh_telecare_province_province_istat_code" class="telecare_province_province_istat_code"><?php echo $telecare_province->province_istat_code->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_province->province_region_istat_code->Visible) { // province_region_istat_code ?>
		<th><span id="elh_telecare_province_province_region_istat_code" class="telecare_province_province_region_istat_code"><?php echo $telecare_province->province_region_istat_code->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_province->province_region_id->Visible) { // province_region_id ?>
		<th><span id="elh_telecare_province_province_region_id" class="telecare_province_province_region_id"><?php echo $telecare_province->province_region_id->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$telecare_province_delete->RecCnt = 0;
$i = 0;
while (!$telecare_province_delete->Recordset->EOF) {
	$telecare_province_delete->RecCnt++;
	$telecare_province_delete->RowCnt++;

	// Set row properties
	$telecare_province->ResetAttrs();
	$telecare_province->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$telecare_province_delete->LoadRowValues($telecare_province_delete->Recordset);

	// Render row
	$telecare_province_delete->RenderRow();
?>
	<tr<?php echo $telecare_province->RowAttributes() ?>>
<?php if ($telecare_province->province_id->Visible) { // province_id ?>
		<td<?php echo $telecare_province->province_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_province_delete->RowCnt ?>_telecare_province_province_id" class="telecare_province_province_id">
<span<?php echo $telecare_province->province_id->ViewAttributes() ?>>
<?php echo $telecare_province->province_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_province->province_name->Visible) { // province_name ?>
		<td<?php echo $telecare_province->province_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_province_delete->RowCnt ?>_telecare_province_province_name" class="telecare_province_province_name">
<span<?php echo $telecare_province->province_name->ViewAttributes() ?>>
<?php echo $telecare_province->province_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_province->province_slug->Visible) { // province_slug ?>
		<td<?php echo $telecare_province->province_slug->CellAttributes() ?>>
<span id="el<?php echo $telecare_province_delete->RowCnt ?>_telecare_province_province_slug" class="telecare_province_province_slug">
<span<?php echo $telecare_province->province_slug->ViewAttributes() ?>>
<?php echo $telecare_province->province_slug->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_province->province_abbr->Visible) { // province_abbr ?>
		<td<?php echo $telecare_province->province_abbr->CellAttributes() ?>>
<span id="el<?php echo $telecare_province_delete->RowCnt ?>_telecare_province_province_abbr" class="telecare_province_province_abbr">
<span<?php echo $telecare_province->province_abbr->ViewAttributes() ?>>
<?php echo $telecare_province->province_abbr->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_province->province_istat_code->Visible) { // province_istat_code ?>
		<td<?php echo $telecare_province->province_istat_code->CellAttributes() ?>>
<span id="el<?php echo $telecare_province_delete->RowCnt ?>_telecare_province_province_istat_code" class="telecare_province_province_istat_code">
<span<?php echo $telecare_province->province_istat_code->ViewAttributes() ?>>
<?php echo $telecare_province->province_istat_code->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_province->province_region_istat_code->Visible) { // province_region_istat_code ?>
		<td<?php echo $telecare_province->province_region_istat_code->CellAttributes() ?>>
<span id="el<?php echo $telecare_province_delete->RowCnt ?>_telecare_province_province_region_istat_code" class="telecare_province_province_region_istat_code">
<span<?php echo $telecare_province->province_region_istat_code->ViewAttributes() ?>>
<?php echo $telecare_province->province_region_istat_code->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_province->province_region_id->Visible) { // province_region_id ?>
		<td<?php echo $telecare_province->province_region_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_province_delete->RowCnt ?>_telecare_province_province_region_id" class="telecare_province_province_region_id">
<span<?php echo $telecare_province->province_region_id->ViewAttributes() ?>>
<?php echo $telecare_province->province_region_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$telecare_province_delete->Recordset->MoveNext();
}
$telecare_province_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_province_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftelecare_provincedelete.Init();
</script>
<?php
$telecare_province_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_province_delete->Page_Terminate();
?>
