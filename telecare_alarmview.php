<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_alarminfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_alarm_view = NULL; // Initialize page object first

class ctelecare_alarm_view extends ctelecare_alarm {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_alarm';

	// Page object name
	var $PageObjName = 'telecare_alarm_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_alarm)
		if (!isset($GLOBALS["telecare_alarm"]) || get_class($GLOBALS["telecare_alarm"]) == "ctelecare_alarm") {
			$GLOBALS["telecare_alarm"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_alarm"];
		}
		$KeyUrl = "";
		if (@$_GET["alarm_id"] <> "") {
			$this->RecKey["alarm_id"] = $_GET["alarm_id"];
			$KeyUrl .= "&amp;alarm_id=" . urlencode($this->RecKey["alarm_id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_user)
		if (!isset($GLOBALS['telecare_user'])) $GLOBALS['telecare_user'] = new ctelecare_user();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_alarm', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_alarmlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header
		if (@$_GET["alarm_id"] <> "") {
			if ($gsExportFile <> "") $gsExportFile .= "_";
			$gsExportFile .= ew_StripSlashes($_GET["alarm_id"]);
		}

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Setup export options
		$this->SetupExportOptions();
		$this->alarm_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_alarm;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_alarm);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["alarm_id"] <> "") {
				$this->alarm_id->setQueryStringValue($_GET["alarm_id"]);
				$this->RecKey["alarm_id"] = $this->alarm_id->QueryStringValue;
			} elseif (@$_POST["alarm_id"] <> "") {
				$this->alarm_id->setFormValue($_POST["alarm_id"]);
				$this->RecKey["alarm_id"] = $this->alarm_id->FormValue;
			} else {
				$bLoadCurrentRecord = TRUE;
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					$this->StartRec = 1; // Initialize start position
					if ($this->Recordset = $this->LoadRecordset()) // Load records
						$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
					if ($this->TotalRecs <= 0) { // No record found
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$this->Page_Terminate("telecare_alarmlist.php"); // Return to list page
					} elseif ($bLoadCurrentRecord) { // Load current record position
						$this->SetUpStartRec(); // Set up start record position

						// Point to current record
						if (intval($this->StartRec) <= intval($this->TotalRecs)) {
							$bMatchRecord = TRUE;
							$this->Recordset->Move($this->StartRec-1);
						}
					} else { // Match key values
						while (!$this->Recordset->EOF) {
							if (strval($this->alarm_id->CurrentValue) == strval($this->Recordset->fields('alarm_id'))) {
								$this->setStartRecordNumber($this->StartRec); // Save record position
								$bMatchRecord = TRUE;
								break;
							} else {
								$this->StartRec++;
								$this->Recordset->MoveNext();
							}
						}
					}
					if (!$bMatchRecord) {
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "telecare_alarmlist.php"; // No matching record, return to list
					} else {
						$this->LoadRowValues($this->Recordset); // Load row values
					}
			}

			// Export data only
			if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
				$this->ExportData();
				$this->Page_Terminate(); // Terminate response
				exit();
			}
		} else {
			$sReturnUrl = "telecare_alarmlist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->alarm_id->setDbValue($rs->fields('alarm_id'));
		$this->alarm_type_id->setDbValue($rs->fields('alarm_type_id'));
		$this->alarm_user_id->setDbValue($rs->fields('alarm_user_id'));
		$this->alarm_product_id->setDbValue($rs->fields('alarm_product_id'));
		$this->alarm_datetime->setDbValue($rs->fields('alarm_datetime'));
		$this->alarm_latitude->setDbValue($rs->fields('alarm_latitude'));
		$this->alarm_longitude->setDbValue($rs->fields('alarm_longitude'));
		$this->alarm_start_on->setDbValue($rs->fields('alarm_start_on'));
		$this->alarm_close_on->setDbValue($rs->fields('alarm_close_on'));
		$this->alarm_close->setDbValue($rs->fields('alarm_close'));
		$this->alarm_admin_id->setDbValue($rs->fields('alarm_admin_id'));
		$this->alarm_call_parents->setDbValue($rs->fields('alarm_call_parents'));
		$this->alarm_call_emergency->setDbValue($rs->fields('alarm_call_emergency'));
		$this->alarm_note->setDbValue($rs->fields('alarm_note'));
		$this->alarm_last_update->setDbValue($rs->fields('alarm_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->alarm_id->DbValue = $row['alarm_id'];
		$this->alarm_type_id->DbValue = $row['alarm_type_id'];
		$this->alarm_user_id->DbValue = $row['alarm_user_id'];
		$this->alarm_product_id->DbValue = $row['alarm_product_id'];
		$this->alarm_datetime->DbValue = $row['alarm_datetime'];
		$this->alarm_latitude->DbValue = $row['alarm_latitude'];
		$this->alarm_longitude->DbValue = $row['alarm_longitude'];
		$this->alarm_start_on->DbValue = $row['alarm_start_on'];
		$this->alarm_close_on->DbValue = $row['alarm_close_on'];
		$this->alarm_close->DbValue = $row['alarm_close'];
		$this->alarm_admin_id->DbValue = $row['alarm_admin_id'];
		$this->alarm_call_parents->DbValue = $row['alarm_call_parents'];
		$this->alarm_call_emergency->DbValue = $row['alarm_call_emergency'];
		$this->alarm_note->DbValue = $row['alarm_note'];
		$this->alarm_last_update->DbValue = $row['alarm_last_update'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// alarm_id
		// alarm_type_id
		// alarm_user_id
		// alarm_product_id
		// alarm_datetime
		// alarm_latitude
		// alarm_longitude
		// alarm_start_on
		// alarm_close_on
		// alarm_close
		// alarm_admin_id
		// alarm_call_parents
		// alarm_call_emergency
		// alarm_note
		// alarm_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// alarm_id
		$this->alarm_id->ViewValue = $this->alarm_id->CurrentValue;
		$this->alarm_id->ViewCustomAttributes = "";

		// alarm_type_id
		if (strval($this->alarm_type_id->CurrentValue) <> "") {
			$sFilterWrk = "`alarm_type_id`" . ew_SearchString("=", $this->alarm_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_type_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->CurrentValue;
			}
		} else {
			$this->alarm_type_id->ViewValue = NULL;
		}
		$this->alarm_type_id->ViewCustomAttributes = "";

		// alarm_user_id
		$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
		if (strval($this->alarm_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->alarm_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
			}
		} else {
			$this->alarm_user_id->ViewValue = NULL;
		}
		$this->alarm_user_id->ViewCustomAttributes = "";

		// alarm_product_id
		if (strval($this->alarm_product_id->CurrentValue) <> "") {
			$sFilterWrk = "telecare_product.product_id" . ew_SearchString("=", $this->alarm_product_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_product_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->CurrentValue;
			}
		} else {
			$this->alarm_product_id->ViewValue = NULL;
		}
		$this->alarm_product_id->ViewCustomAttributes = "";

		// alarm_datetime
		$this->alarm_datetime->ViewValue = $this->alarm_datetime->CurrentValue;
		$this->alarm_datetime->ViewValue = ew_FormatDateTime($this->alarm_datetime->ViewValue, 11);
		$this->alarm_datetime->ViewCustomAttributes = "";

		// alarm_latitude
		$this->alarm_latitude->ViewValue = $this->alarm_latitude->CurrentValue;
		$this->alarm_latitude->ViewCustomAttributes = "";

		// alarm_longitude
		$this->alarm_longitude->ViewValue = $this->alarm_longitude->CurrentValue;
		$this->alarm_longitude->ViewCustomAttributes = "";

		// alarm_start_on
		$this->alarm_start_on->ViewValue = $this->alarm_start_on->CurrentValue;
		$this->alarm_start_on->ViewValue = ew_FormatDateTime($this->alarm_start_on->ViewValue, 11);
		$this->alarm_start_on->ViewCustomAttributes = "";

		// alarm_close_on
		$this->alarm_close_on->ViewValue = $this->alarm_close_on->CurrentValue;
		$this->alarm_close_on->ViewValue = ew_FormatDateTime($this->alarm_close_on->ViewValue, 11);
		$this->alarm_close_on->ViewCustomAttributes = "";

		// alarm_close
		if (strval($this->alarm_close->CurrentValue) <> "") {
			$this->alarm_close->ViewValue = $this->alarm_close->OptionCaption($this->alarm_close->CurrentValue);
		} else {
			$this->alarm_close->ViewValue = NULL;
		}
		$this->alarm_close->ViewCustomAttributes = "";

		// alarm_admin_id
		if (strval($this->alarm_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->alarm_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->CurrentValue;
			}
		} else {
			$this->alarm_admin_id->ViewValue = NULL;
		}
		$this->alarm_admin_id->ViewCustomAttributes = "";

		// alarm_call_parents
		if (strval($this->alarm_call_parents->CurrentValue) <> "") {
			$this->alarm_call_parents->ViewValue = $this->alarm_call_parents->OptionCaption($this->alarm_call_parents->CurrentValue);
		} else {
			$this->alarm_call_parents->ViewValue = NULL;
		}
		$this->alarm_call_parents->ViewCustomAttributes = "";

		// alarm_call_emergency
		if (strval($this->alarm_call_emergency->CurrentValue) <> "") {
			$this->alarm_call_emergency->ViewValue = $this->alarm_call_emergency->OptionCaption($this->alarm_call_emergency->CurrentValue);
		} else {
			$this->alarm_call_emergency->ViewValue = NULL;
		}
		$this->alarm_call_emergency->ViewCustomAttributes = "";

		// alarm_note
		$this->alarm_note->ViewValue = $this->alarm_note->CurrentValue;
		$this->alarm_note->ViewCustomAttributes = "";

		// alarm_last_update
		$this->alarm_last_update->ViewValue = $this->alarm_last_update->CurrentValue;
		$this->alarm_last_update->ViewValue = ew_FormatDateTime($this->alarm_last_update->ViewValue, 7);
		$this->alarm_last_update->ViewCustomAttributes = "";

			// alarm_id
			$this->alarm_id->LinkCustomAttributes = "";
			$this->alarm_id->HrefValue = "";
			$this->alarm_id->TooltipValue = "";

			// alarm_type_id
			$this->alarm_type_id->LinkCustomAttributes = "";
			$this->alarm_type_id->HrefValue = "";
			$this->alarm_type_id->TooltipValue = "";

			// alarm_user_id
			$this->alarm_user_id->LinkCustomAttributes = "";
			$this->alarm_user_id->HrefValue = "";
			$this->alarm_user_id->TooltipValue = "";

			// alarm_product_id
			$this->alarm_product_id->LinkCustomAttributes = "";
			$this->alarm_product_id->HrefValue = "";
			$this->alarm_product_id->TooltipValue = "";

			// alarm_datetime
			$this->alarm_datetime->LinkCustomAttributes = "";
			$this->alarm_datetime->HrefValue = "";
			$this->alarm_datetime->TooltipValue = "";

			// alarm_latitude
			$this->alarm_latitude->LinkCustomAttributes = "";
			$this->alarm_latitude->HrefValue = "";
			$this->alarm_latitude->TooltipValue = "";

			// alarm_start_on
			$this->alarm_start_on->LinkCustomAttributes = "";
			$this->alarm_start_on->HrefValue = "";
			$this->alarm_start_on->TooltipValue = "";

			// alarm_close_on
			$this->alarm_close_on->LinkCustomAttributes = "";
			$this->alarm_close_on->HrefValue = "";
			$this->alarm_close_on->TooltipValue = "";

			// alarm_close
			$this->alarm_close->LinkCustomAttributes = "";
			$this->alarm_close->HrefValue = "";
			$this->alarm_close->TooltipValue = "";

			// alarm_admin_id
			$this->alarm_admin_id->LinkCustomAttributes = "";
			$this->alarm_admin_id->HrefValue = "";
			$this->alarm_admin_id->TooltipValue = "";

			// alarm_call_parents
			$this->alarm_call_parents->LinkCustomAttributes = "";
			$this->alarm_call_parents->HrefValue = "";
			$this->alarm_call_parents->TooltipValue = "";

			// alarm_call_emergency
			$this->alarm_call_emergency->LinkCustomAttributes = "";
			$this->alarm_call_emergency->HrefValue = "";
			$this->alarm_call_emergency->TooltipValue = "";

			// alarm_note
			$this->alarm_note->LinkCustomAttributes = "";
			$this->alarm_note->HrefValue = "";
			$this->alarm_note->TooltipValue = "";

			// alarm_last_update
			$this->alarm_last_update->LinkCustomAttributes = "";
			$this->alarm_last_update->HrefValue = "";
			$this->alarm_last_update->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = TRUE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_telecare_alarm\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_telecare_alarm',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.ftelecare_alarmview,key:" . ew_ArrayToJsonAttr($this->RecKey) . ",sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide options for export
		if ($this->Export <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = FALSE;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;
		$this->SetUpStartRec(); // Set up start record position

		// Set the last record to display
		if ($this->DisplayRecs <= 0) {
			$this->StopRec = $this->TotalRecs;
		} else {
			$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
		}
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "v");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "view");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setQueryStringValue($_GET["fk_user_id"]);
					$this->alarm_user_id->setQueryStringValue($GLOBALS["telecare_user"]->user_id->QueryStringValue);
					$this->alarm_user_id->setSessionValue($this->alarm_user_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setFormValue($_POST["fk_user_id"]);
					$this->alarm_user_id->setFormValue($GLOBALS["telecare_user"]->user_id->FormValue);
					$this->alarm_user_id->setSessionValue($this->alarm_user_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);
			$this->setSessionWhere($this->GetDetailFilter());

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "telecare_user") {
				if ($this->alarm_user_id->QueryStringValue == "") $this->alarm_user_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_alarmlist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_alarm_view)) $telecare_alarm_view = new ctelecare_alarm_view();

// Page init
$telecare_alarm_view->Page_Init();

// Page main
$telecare_alarm_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_alarm_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($telecare_alarm->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = ftelecare_alarmview = new ew_Form("ftelecare_alarmview", "view");

// Form_CustomValidate event
ftelecare_alarmview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_alarmview.ValidateRequired = true;
<?php } else { ?>
ftelecare_alarmview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_alarmview.Lists["x_alarm_type_id"] = {"LinkField":"x_alarm_type_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_alarm_type_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmview.Lists["x_alarm_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmview.Lists["x_alarm_product_id"] = {"LinkField":"x_product_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_product_type_name","x_product_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmview.Lists["x_alarm_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmview.Lists["x_alarm_close"].Options = <?php echo json_encode($telecare_alarm->alarm_close->Options()) ?>;
ftelecare_alarmview.Lists["x_alarm_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmview.Lists["x_alarm_call_parents"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmview.Lists["x_alarm_call_parents"].Options = <?php echo json_encode($telecare_alarm->alarm_call_parents->Options()) ?>;
ftelecare_alarmview.Lists["x_alarm_call_emergency"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmview.Lists["x_alarm_call_emergency"].Options = <?php echo json_encode($telecare_alarm->alarm_call_emergency->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($telecare_alarm->Export == "") { ?>
<div class="ewToolbar">
<?php if ($telecare_alarm->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php $telecare_alarm_view->ExportOptions->Render("body") ?>
<?php
	foreach ($telecare_alarm_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php if ($telecare_alarm->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $telecare_alarm_view->ShowPageHeader(); ?>
<?php
$telecare_alarm_view->ShowMessage();
?>
<?php if ($telecare_alarm->Export == "") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_alarm_view->Pager)) $telecare_alarm_view->Pager = new cPrevNextPager($telecare_alarm_view->StartRec, $telecare_alarm_view->DisplayRecs, $telecare_alarm_view->TotalRecs) ?>
<?php if ($telecare_alarm_view->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_alarm_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_alarm_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_alarm_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_alarm_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_alarm_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_alarm_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
</form>
<?php } ?>
<form name="ftelecare_alarmview" id="ftelecare_alarmview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_alarm_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_alarm_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_alarm">
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_alarm->alarm_id->Visible) { // alarm_id ?>
	<tr id="r_alarm_id">
		<td><span id="elh_telecare_alarm_alarm_id"><?php echo $telecare_alarm->alarm_id->FldCaption() ?></span></td>
		<td data-name="alarm_id"<?php echo $telecare_alarm->alarm_id->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_id">
<span<?php echo $telecare_alarm->alarm_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
	<tr id="r_alarm_type_id">
		<td><span id="elh_telecare_alarm_alarm_type_id"><?php echo $telecare_alarm->alarm_type_id->FldCaption() ?></span></td>
		<td data-name="alarm_type_id"<?php echo $telecare_alarm->alarm_type_id->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_type_id">
<span<?php echo $telecare_alarm->alarm_type_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_type_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
	<tr id="r_alarm_user_id">
		<td><span id="elh_telecare_alarm_alarm_user_id"><?php echo $telecare_alarm->alarm_user_id->FldCaption() ?></span></td>
		<td data-name="alarm_user_id"<?php echo $telecare_alarm->alarm_user_id->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_user_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
	<tr id="r_alarm_product_id">
		<td><span id="elh_telecare_alarm_alarm_product_id"><?php echo $telecare_alarm->alarm_product_id->FldCaption() ?></span></td>
		<td data-name="alarm_product_id"<?php echo $telecare_alarm->alarm_product_id->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_product_id">
<span<?php echo $telecare_alarm->alarm_product_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_product_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
	<tr id="r_alarm_datetime">
		<td><span id="elh_telecare_alarm_alarm_datetime"><?php echo $telecare_alarm->alarm_datetime->FldCaption() ?></span></td>
		<td data-name="alarm_datetime"<?php echo $telecare_alarm->alarm_datetime->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_datetime">
<span<?php echo $telecare_alarm->alarm_datetime->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_datetime->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_latitude->Visible) { // alarm_latitude ?>
	<tr id="r_alarm_latitude">
		<td><span id="elh_telecare_alarm_alarm_latitude"><?php echo $telecare_alarm->alarm_latitude->FldCaption() ?></span></td>
		<td data-name="alarm_latitude"<?php echo $telecare_alarm->alarm_latitude->CellAttributes() ?>>
<div id="orig_telecare_alarm_alarm_latitude" class="hide">
<span id="el_telecare_alarm_alarm_latitude">
<span<?php echo $telecare_alarm->alarm_latitude->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_latitude->ViewValue ?></span>
</span>
</div>
<div id="gmap_x_alarm_latitude" class="ewGoogleMap" style="width: 200px; height: 200px;"></div>
<script type="text/javascript">
ewGoogleMaps[ewGoogleMaps.length] = ewGoogleMaps["gmap_x_alarm_latitude"] = jQuery.extend({"id":"gmap_x_alarm_latitude","width":200,"height":200,"latitude":null,"longitude":null,"address":null,"type":"ROADMAP","zoom":8,"title":null,"icon":null,"use_single_map":false,"single_map_width":400,"single_map_height":400,"show_map_on_top":true,"show_all_markers":true,"description":null}, {
	latitude: <?php echo ew_VarToJson($telecare_alarm->alarm_latitude->CurrentValue, "undefined") ?>,
	longitude: <?php echo ew_VarToJson($telecare_alarm->alarm_longitude->CurrentValue, "undefined") ?>
});
</script>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
	<tr id="r_alarm_start_on">
		<td><span id="elh_telecare_alarm_alarm_start_on"><?php echo $telecare_alarm->alarm_start_on->FldCaption() ?></span></td>
		<td data-name="alarm_start_on"<?php echo $telecare_alarm->alarm_start_on->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_start_on">
<span<?php echo $telecare_alarm->alarm_start_on->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_start_on->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_close_on->Visible) { // alarm_close_on ?>
	<tr id="r_alarm_close_on">
		<td><span id="elh_telecare_alarm_alarm_close_on"><?php echo $telecare_alarm->alarm_close_on->FldCaption() ?></span></td>
		<td data-name="alarm_close_on"<?php echo $telecare_alarm->alarm_close_on->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_close_on">
<span<?php echo $telecare_alarm->alarm_close_on->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_close_on->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
	<tr id="r_alarm_close">
		<td><span id="elh_telecare_alarm_alarm_close"><?php echo $telecare_alarm->alarm_close->FldCaption() ?></span></td>
		<td data-name="alarm_close"<?php echo $telecare_alarm->alarm_close->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_close">
<span<?php echo $telecare_alarm->alarm_close->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_close->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_admin_id->Visible) { // alarm_admin_id ?>
	<tr id="r_alarm_admin_id">
		<td><span id="elh_telecare_alarm_alarm_admin_id"><?php echo $telecare_alarm->alarm_admin_id->FldCaption() ?></span></td>
		<td data-name="alarm_admin_id"<?php echo $telecare_alarm->alarm_admin_id->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_admin_id">
<span<?php echo $telecare_alarm->alarm_admin_id->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_admin_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
	<tr id="r_alarm_call_parents">
		<td><span id="elh_telecare_alarm_alarm_call_parents"><?php echo $telecare_alarm->alarm_call_parents->FldCaption() ?></span></td>
		<td data-name="alarm_call_parents"<?php echo $telecare_alarm->alarm_call_parents->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_call_parents">
<span<?php echo $telecare_alarm->alarm_call_parents->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_call_parents->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
	<tr id="r_alarm_call_emergency">
		<td><span id="elh_telecare_alarm_alarm_call_emergency"><?php echo $telecare_alarm->alarm_call_emergency->FldCaption() ?></span></td>
		<td data-name="alarm_call_emergency"<?php echo $telecare_alarm->alarm_call_emergency->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_call_emergency">
<span<?php echo $telecare_alarm->alarm_call_emergency->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_call_emergency->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_note->Visible) { // alarm_note ?>
	<tr id="r_alarm_note">
		<td><span id="elh_telecare_alarm_alarm_note"><?php echo $telecare_alarm->alarm_note->FldCaption() ?></span></td>
		<td data-name="alarm_note"<?php echo $telecare_alarm->alarm_note->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_note">
<span<?php echo $telecare_alarm->alarm_note->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_note->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_alarm->alarm_last_update->Visible) { // alarm_last_update ?>
	<tr id="r_alarm_last_update">
		<td><span id="elh_telecare_alarm_alarm_last_update"><?php echo $telecare_alarm->alarm_last_update->FldCaption() ?></span></td>
		<td data-name="alarm_last_update"<?php echo $telecare_alarm->alarm_last_update->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_last_update">
<span<?php echo $telecare_alarm->alarm_last_update->ViewAttributes() ?>>
<?php echo $telecare_alarm->alarm_last_update->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_alarm->Export == "") { ?>
<?php if (!isset($telecare_alarm_view->Pager)) $telecare_alarm_view->Pager = new cPrevNextPager($telecare_alarm_view->StartRec, $telecare_alarm_view->DisplayRecs, $telecare_alarm_view->TotalRecs) ?>
<?php if ($telecare_alarm_view->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_alarm_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_alarm_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_alarm_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_alarm_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_alarm_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_alarm_view->PageUrl() ?>start=<?php echo $telecare_alarm_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_alarm_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php } ?>
</form>
<script type="text/javascript">
ftelecare_alarmview.Init();
</script>
<?php
$telecare_alarm_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($telecare_alarm->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$telecare_alarm_view->Page_Terminate();
?>
