<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_admin_search = NULL; // Initialize page object first

class ctelecare_admin_search extends ctelecare_admin {

	// Page ID
	var $PageID = 'search';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_admin';

	// Page object name
	var $PageObjName = 'telecare_admin_search';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_admin)
		if (!isset($GLOBALS["telecare_admin"]) || get_class($GLOBALS["telecare_admin"]) == "ctelecare_admin") {
			$GLOBALS["telecare_admin"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_admin"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'search', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_admin', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanSearch()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_adminlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
				$this->Page_Terminate(ew_GetUrl("telecare_adminlist.php"));
			}
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->admin_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_admin;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_admin);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewSearchForm";
	var $IsModal = FALSE;
	var $SearchLabelClass = "col-sm-3 control-label ewLabel";
	var $SearchRightColumnClass = "col-sm-9";
	var $MultiPages; // Multi pages object

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsSearchError;
		global $gbSkipHeaderFooter;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Check modal
		$this->IsModal = (@$_GET["modal"] == "1" || @$_POST["modal"] == "1");
		if ($this->IsModal)
			$gbSkipHeaderFooter = TRUE;
		if ($this->IsPageRequest()) { // Validate request

			// Get action
			$this->CurrentAction = $objForm->GetValue("a_search");
			switch ($this->CurrentAction) {
				case "S": // Get search criteria

					// Build search string for advanced search, remove blank field
					$this->LoadSearchValues(); // Get search values
					if ($this->ValidateSearch()) {
						$sSrchStr = $this->BuildAdvancedSearch();
					} else {
						$sSrchStr = "";
						$this->setFailureMessage($gsSearchError);
					}
					if ($sSrchStr <> "") {
						$sSrchStr = $this->UrlParm($sSrchStr);
						$sSrchStr = "telecare_adminlist.php" . "?" . $sSrchStr;
						if ($this->IsModal) {
							$row = array();
							$row["url"] = $sSrchStr;
							echo ew_ArrayToJson(array($row));
							$this->Page_Terminate();
							exit();
						} else {
							$this->Page_Terminate($sSrchStr); // Go to list page
						}
					}
			}
		}

		// Restore search settings from Session
		if ($gsSearchError == "")
			$this->LoadAdvancedSearch();

		// Render row for search
		$this->RowType = EW_ROWTYPE_SEARCH;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Build advanced search
	function BuildAdvancedSearch() {
		$sSrchUrl = "";
		$this->BuildSearchUrl($sSrchUrl, $this->admin_id); // admin_id
		$this->BuildSearchUrl($sSrchUrl, $this->admin_name); // admin_name
		$this->BuildSearchUrl($sSrchUrl, $this->admin_surname); // admin_surname
		$this->BuildSearchUrl($sSrchUrl, $this->admin_gender); // admin_gender
		$this->BuildSearchUrl($sSrchUrl, $this->admin_email); // admin_email
		$this->BuildSearchUrl($sSrchUrl, $this->admin_mobile); // admin_mobile
		$this->BuildSearchUrl($sSrchUrl, $this->admin_phone); // admin_phone
		$this->BuildSearchUrl($sSrchUrl, $this->admin_modules); // admin_modules
		$this->BuildSearchUrl($sSrchUrl, $this->admin_language); // admin_language
		$this->BuildSearchUrl($sSrchUrl, $this->admin_is_active); // admin_is_active
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_name); // admin_company_name
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_vat); // admin_company_vat
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_logo_file); // admin_company_logo_file
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_address); // admin_company_address
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_region_id); // admin_company_region_id
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_province_id); // admin_company_province_id
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_city_id); // admin_company_city_id
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_phone); // admin_company_phone
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_email); // admin_company_email
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_web); // admin_company_web
		$this->BuildSearchUrl($sSrchUrl, $this->admin_contract); // admin_contract
		$this->BuildSearchUrl($sSrchUrl, $this->admin_expire); // admin_expire
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_facebook); // admin_company_facebook
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_twitter); // admin_company_twitter
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_googleplus); // admin_company_googleplus
		$this->BuildSearchUrl($sSrchUrl, $this->admin_company_linkedin); // admin_company_linkedin
		$this->BuildSearchUrl($sSrchUrl, $this->admin_link_playstore); // admin_link_playstore
		$this->BuildSearchUrl($sSrchUrl, $this->admin_link_appstore); // admin_link_appstore
		$this->BuildSearchUrl($sSrchUrl, $this->admin_link_windowsstore); // admin_link_windowsstore
		$this->BuildSearchUrl($sSrchUrl, $this->admin_level); // admin_level
		$this->BuildSearchUrl($sSrchUrl, $this->admin_username); // admin_username
		$this->BuildSearchUrl($sSrchUrl, $this->admin_password); // admin_password
		$this->BuildSearchUrl($sSrchUrl, $this->admin_last_update); // admin_last_update
		if ($sSrchUrl <> "") $sSrchUrl .= "&";
		$sSrchUrl .= "cmd=search";
		return $sSrchUrl;
	}

	// Build search URL
	function BuildSearchUrl(&$Url, &$Fld, $OprOnly=FALSE) {
		global $objForm;
		$sWrk = "";
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $objForm->GetValue("x_$FldParm");
		$FldOpr = $objForm->GetValue("z_$FldParm");
		$FldCond = $objForm->GetValue("v_$FldParm");
		$FldVal2 = $objForm->GetValue("y_$FldParm");
		$FldOpr2 = $objForm->GetValue("w_$FldParm");
		$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);
		$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		$lFldDataType = ($Fld->FldIsVirtual) ? EW_DATATYPE_STRING : $Fld->FldDataType;
		if ($FldOpr == "BETWEEN") {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal) && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal <> "" && $FldVal2 <> "" && $IsValidValue) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			}
		} else {
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal));
			if ($FldVal <> "" && $IsValidValue && ew_IsValidOpr($FldOpr, $lFldDataType)) {
				$sWrk = "x_" . $FldParm . "=" . urlencode($FldVal) .
					"&z_" . $FldParm . "=" . urlencode($FldOpr);
			} elseif ($FldOpr == "IS NULL" || $FldOpr == "IS NOT NULL" || ($FldOpr <> "" && $OprOnly && ew_IsValidOpr($FldOpr, $lFldDataType))) {
				$sWrk = "z_" . $FldParm . "=" . urlencode($FldOpr);
			}
			$IsValidValue = ($lFldDataType <> EW_DATATYPE_NUMBER) ||
				($lFldDataType == EW_DATATYPE_NUMBER && $this->SearchValueIsNumeric($Fld, $FldVal2));
			if ($FldVal2 <> "" && $IsValidValue && ew_IsValidOpr($FldOpr2, $lFldDataType)) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "y_" . $FldParm . "=" . urlencode($FldVal2) .
					"&w_" . $FldParm . "=" . urlencode($FldOpr2);
			} elseif ($FldOpr2 == "IS NULL" || $FldOpr2 == "IS NOT NULL" || ($FldOpr2 <> "" && $OprOnly && ew_IsValidOpr($FldOpr2, $lFldDataType))) {
				if ($sWrk <> "") $sWrk .= "&v_" . $FldParm . "=" . urlencode($FldCond) . "&";
				$sWrk .= "w_" . $FldParm . "=" . urlencode($FldOpr2);
			}
		}
		if ($sWrk <> "") {
			if ($Url <> "") $Url .= "&";
			$Url .= $sWrk;
		}
	}

	function SearchValueIsNumeric($Fld, $Value) {
		if (ew_IsFloatFormat($Fld->FldType)) $Value = ew_StrToFloat($Value);
		return is_numeric($Value);
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// admin_id

		$this->admin_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_id"));
		$this->admin_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_id");

		// admin_name
		$this->admin_name->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_name"));
		$this->admin_name->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_name");

		// admin_surname
		$this->admin_surname->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_surname"));
		$this->admin_surname->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_surname");

		// admin_gender
		$this->admin_gender->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_gender"));
		$this->admin_gender->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_gender");

		// admin_email
		$this->admin_email->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_email"));
		$this->admin_email->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_email");

		// admin_mobile
		$this->admin_mobile->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_mobile"));
		$this->admin_mobile->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_mobile");

		// admin_phone
		$this->admin_phone->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_phone"));
		$this->admin_phone->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_phone");

		// admin_modules
		$this->admin_modules->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_modules"));
		$this->admin_modules->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_modules");
		if (is_array($this->admin_modules->AdvancedSearch->SearchValue)) $this->admin_modules->AdvancedSearch->SearchValue = implode(",", $this->admin_modules->AdvancedSearch->SearchValue);
		if (is_array($this->admin_modules->AdvancedSearch->SearchValue2)) $this->admin_modules->AdvancedSearch->SearchValue2 = implode(",", $this->admin_modules->AdvancedSearch->SearchValue2);

		// admin_language
		$this->admin_language->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_language"));
		$this->admin_language->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_language");

		// admin_is_active
		$this->admin_is_active->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_is_active"));
		$this->admin_is_active->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_is_active");

		// admin_company_name
		$this->admin_company_name->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_name"));
		$this->admin_company_name->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_name");

		// admin_company_vat
		$this->admin_company_vat->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_vat"));
		$this->admin_company_vat->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_vat");

		// admin_company_logo_file
		$this->admin_company_logo_file->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_logo_file"));
		$this->admin_company_logo_file->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_logo_file");

		// admin_company_address
		$this->admin_company_address->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_address"));
		$this->admin_company_address->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_address");

		// admin_company_region_id
		$this->admin_company_region_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_region_id"));
		$this->admin_company_region_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_region_id");

		// admin_company_province_id
		$this->admin_company_province_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_province_id"));
		$this->admin_company_province_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_province_id");

		// admin_company_city_id
		$this->admin_company_city_id->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_city_id"));
		$this->admin_company_city_id->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_city_id");

		// admin_company_phone
		$this->admin_company_phone->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_phone"));
		$this->admin_company_phone->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_phone");

		// admin_company_email
		$this->admin_company_email->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_email"));
		$this->admin_company_email->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_email");

		// admin_company_web
		$this->admin_company_web->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_web"));
		$this->admin_company_web->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_web");

		// admin_contract
		$this->admin_contract->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_contract"));
		$this->admin_contract->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_contract");

		// admin_expire
		$this->admin_expire->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_expire"));
		$this->admin_expire->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_expire");

		// admin_company_facebook
		$this->admin_company_facebook->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_facebook"));
		$this->admin_company_facebook->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_facebook");

		// admin_company_twitter
		$this->admin_company_twitter->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_twitter"));
		$this->admin_company_twitter->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_twitter");

		// admin_company_googleplus
		$this->admin_company_googleplus->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_googleplus"));
		$this->admin_company_googleplus->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_googleplus");

		// admin_company_linkedin
		$this->admin_company_linkedin->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_company_linkedin"));
		$this->admin_company_linkedin->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_company_linkedin");

		// admin_link_playstore
		$this->admin_link_playstore->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_link_playstore"));
		$this->admin_link_playstore->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_link_playstore");

		// admin_link_appstore
		$this->admin_link_appstore->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_link_appstore"));
		$this->admin_link_appstore->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_link_appstore");

		// admin_link_windowsstore
		$this->admin_link_windowsstore->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_link_windowsstore"));
		$this->admin_link_windowsstore->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_link_windowsstore");

		// admin_level
		$this->admin_level->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_level"));
		$this->admin_level->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_level");

		// admin_username
		$this->admin_username->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_username"));
		$this->admin_username->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_username");

		// admin_password
		$this->admin_password->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_password"));
		$this->admin_password->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_password");

		// admin_last_update
		$this->admin_last_update->AdvancedSearch->SearchValue = ew_StripSlashes($objForm->GetValue("x_admin_last_update"));
		$this->admin_last_update->AdvancedSearch->SearchOperator = $objForm->GetValue("z_admin_last_update");
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// admin_id
		// admin_parent_id
		// admin_name
		// admin_surname
		// admin_gender
		// admin_email
		// admin_mobile
		// admin_phone
		// admin_modules
		// admin_language
		// admin_is_active
		// admin_company_name
		// admin_company_vat
		// admin_company_logo_file
		// admin_company_logo_width
		// admin_company_logo_height
		// admin_company_logo_size
		// admin_company_address
		// admin_company_region_id
		// admin_company_province_id
		// admin_company_city_id
		// admin_company_phone
		// admin_company_email
		// admin_company_web
		// admin_contract
		// admin_expire
		// admin_company_facebook
		// admin_company_twitter
		// admin_company_googleplus
		// admin_company_linkedin
		// admin_link_playstore
		// admin_link_appstore
		// admin_link_windowsstore
		// admin_level
		// admin_username
		// admin_password
		// admin_company_autohority
		// admin_auth_token
		// admin_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// admin_id
		$this->admin_id->ViewValue = $this->admin_id->CurrentValue;
		$this->admin_id->ViewCustomAttributes = "";

		// admin_name
		$this->admin_name->ViewValue = $this->admin_name->CurrentValue;
		$this->admin_name->ViewCustomAttributes = "";

		// admin_surname
		$this->admin_surname->ViewValue = $this->admin_surname->CurrentValue;
		$this->admin_surname->ViewCustomAttributes = "";

		// admin_gender
		if (strval($this->admin_gender->CurrentValue) <> "") {
			$this->admin_gender->ViewValue = $this->admin_gender->OptionCaption($this->admin_gender->CurrentValue);
		} else {
			$this->admin_gender->ViewValue = NULL;
		}
		$this->admin_gender->ViewCustomAttributes = "";

		// admin_email
		$this->admin_email->ViewValue = $this->admin_email->CurrentValue;
		$this->admin_email->ViewCustomAttributes = "";

		// admin_mobile
		$this->admin_mobile->ViewValue = $this->admin_mobile->CurrentValue;
		$this->admin_mobile->ViewCustomAttributes = "";

		// admin_phone
		$this->admin_phone->ViewValue = $this->admin_phone->CurrentValue;
		$this->admin_phone->ViewCustomAttributes = "";

		// admin_modules
		if (strval($this->admin_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->admin_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->admin_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->admin_modules->ViewValue .= $this->admin_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->admin_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->admin_modules->ViewValue = $this->admin_modules->CurrentValue;
			}
		} else {
			$this->admin_modules->ViewValue = NULL;
		}
		$this->admin_modules->ViewCustomAttributes = "";

		// admin_language
		if (strval($this->admin_language->CurrentValue) <> "") {
			$this->admin_language->ViewValue = $this->admin_language->OptionCaption($this->admin_language->CurrentValue);
		} else {
			$this->admin_language->ViewValue = NULL;
		}
		$this->admin_language->ViewCustomAttributes = "";

		// admin_is_active
		if (strval($this->admin_is_active->CurrentValue) <> "") {
			$this->admin_is_active->ViewValue = $this->admin_is_active->OptionCaption($this->admin_is_active->CurrentValue);
		} else {
			$this->admin_is_active->ViewValue = NULL;
		}
		$this->admin_is_active->ViewCustomAttributes = "";

		// admin_company_name
		$this->admin_company_name->ViewValue = $this->admin_company_name->CurrentValue;
		$this->admin_company_name->ViewCustomAttributes = "";

		// admin_company_vat
		$this->admin_company_vat->ViewValue = $this->admin_company_vat->CurrentValue;
		$this->admin_company_vat->ViewCustomAttributes = "";

		// admin_company_logo_file
		if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
			$this->admin_company_logo_file->ImageWidth = 100;
			$this->admin_company_logo_file->ImageHeight = 0;
			$this->admin_company_logo_file->ImageAlt = $this->admin_company_logo_file->FldAlt();
			$this->admin_company_logo_file->ViewValue = $this->admin_company_logo_file->Upload->DbValue;
		} else {
			$this->admin_company_logo_file->ViewValue = "";
		}
		$this->admin_company_logo_file->ViewCustomAttributes = "";

		// admin_company_address
		$this->admin_company_address->ViewValue = $this->admin_company_address->CurrentValue;
		$this->admin_company_address->ViewCustomAttributes = "";

		// admin_company_region_id
		if (strval($this->admin_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->admin_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->CurrentValue;
			}
		} else {
			$this->admin_company_region_id->ViewValue = NULL;
		}
		$this->admin_company_region_id->ViewCustomAttributes = "";

		// admin_company_province_id
		if (strval($this->admin_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->admin_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->CurrentValue;
			}
		} else {
			$this->admin_company_province_id->ViewValue = NULL;
		}
		$this->admin_company_province_id->ViewCustomAttributes = "";

		// admin_company_city_id
		if (strval($this->admin_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->admin_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->CurrentValue;
			}
		} else {
			$this->admin_company_city_id->ViewValue = NULL;
		}
		$this->admin_company_city_id->ViewCustomAttributes = "";

		// admin_company_phone
		$this->admin_company_phone->ViewValue = $this->admin_company_phone->CurrentValue;
		$this->admin_company_phone->ViewCustomAttributes = "";

		// admin_company_email
		$this->admin_company_email->ViewValue = $this->admin_company_email->CurrentValue;
		$this->admin_company_email->ViewCustomAttributes = "";

		// admin_company_web
		$this->admin_company_web->ViewValue = $this->admin_company_web->CurrentValue;
		$this->admin_company_web->ViewCustomAttributes = "";

		// admin_contract
		$this->admin_contract->ViewValue = $this->admin_contract->CurrentValue;
		$this->admin_contract->ViewValue = ew_FormatDateTime($this->admin_contract->ViewValue, 7);
		$this->admin_contract->ViewCustomAttributes = "";

		// admin_expire
		$this->admin_expire->ViewValue = $this->admin_expire->CurrentValue;
		$this->admin_expire->ViewValue = ew_FormatDateTime($this->admin_expire->ViewValue, 7);
		$this->admin_expire->ViewCustomAttributes = "";

		// admin_company_facebook
		$this->admin_company_facebook->ViewValue = $this->admin_company_facebook->CurrentValue;
		$this->admin_company_facebook->ViewCustomAttributes = "";

		// admin_company_twitter
		$this->admin_company_twitter->ViewValue = $this->admin_company_twitter->CurrentValue;
		$this->admin_company_twitter->ViewCustomAttributes = "";

		// admin_company_googleplus
		$this->admin_company_googleplus->ViewValue = $this->admin_company_googleplus->CurrentValue;
		$this->admin_company_googleplus->ViewCustomAttributes = "";

		// admin_company_linkedin
		$this->admin_company_linkedin->ViewValue = $this->admin_company_linkedin->CurrentValue;
		$this->admin_company_linkedin->ViewCustomAttributes = "";

		// admin_link_playstore
		$this->admin_link_playstore->ViewValue = $this->admin_link_playstore->CurrentValue;
		$this->admin_link_playstore->ViewCustomAttributes = "";

		// admin_link_appstore
		$this->admin_link_appstore->ViewValue = $this->admin_link_appstore->CurrentValue;
		$this->admin_link_appstore->ViewCustomAttributes = "";

		// admin_link_windowsstore
		$this->admin_link_windowsstore->ViewValue = $this->admin_link_windowsstore->CurrentValue;
		$this->admin_link_windowsstore->ViewCustomAttributes = "";

		// admin_level
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->admin_level->CurrentValue) <> "") {
			$this->admin_level->ViewValue = $this->admin_level->OptionCaption($this->admin_level->CurrentValue);
		} else {
			$this->admin_level->ViewValue = NULL;
		}
		} else {
			$this->admin_level->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->admin_level->ViewCustomAttributes = "";

		// admin_username
		$this->admin_username->ViewValue = $this->admin_username->CurrentValue;
		$this->admin_username->ViewCustomAttributes = "";

		// admin_password
		$this->admin_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->admin_password->ViewCustomAttributes = "";

		// admin_last_update
		$this->admin_last_update->ViewValue = $this->admin_last_update->CurrentValue;
		$this->admin_last_update->ViewValue = ew_FormatDateTime($this->admin_last_update->ViewValue, 7);
		$this->admin_last_update->ViewCustomAttributes = "";

			// admin_id
			$this->admin_id->LinkCustomAttributes = "";
			$this->admin_id->HrefValue = "";
			$this->admin_id->TooltipValue = "";

			// admin_name
			$this->admin_name->LinkCustomAttributes = "";
			$this->admin_name->HrefValue = "";
			$this->admin_name->TooltipValue = "";

			// admin_surname
			$this->admin_surname->LinkCustomAttributes = "";
			$this->admin_surname->HrefValue = "";
			$this->admin_surname->TooltipValue = "";

			// admin_gender
			$this->admin_gender->LinkCustomAttributes = "";
			$this->admin_gender->HrefValue = "";
			$this->admin_gender->TooltipValue = "";

			// admin_email
			$this->admin_email->LinkCustomAttributes = "";
			$this->admin_email->HrefValue = "";
			$this->admin_email->TooltipValue = "";

			// admin_mobile
			$this->admin_mobile->LinkCustomAttributes = "";
			$this->admin_mobile->HrefValue = "";
			$this->admin_mobile->TooltipValue = "";

			// admin_phone
			$this->admin_phone->LinkCustomAttributes = "";
			$this->admin_phone->HrefValue = "";
			$this->admin_phone->TooltipValue = "";

			// admin_modules
			$this->admin_modules->LinkCustomAttributes = "";
			$this->admin_modules->HrefValue = "";
			$this->admin_modules->TooltipValue = "";

			// admin_language
			$this->admin_language->LinkCustomAttributes = "";
			$this->admin_language->HrefValue = "";
			$this->admin_language->TooltipValue = "";

			// admin_is_active
			$this->admin_is_active->LinkCustomAttributes = "";
			$this->admin_is_active->HrefValue = "";
			$this->admin_is_active->TooltipValue = "";

			// admin_company_name
			$this->admin_company_name->LinkCustomAttributes = "";
			$this->admin_company_name->HrefValue = "";
			$this->admin_company_name->TooltipValue = "";

			// admin_company_vat
			$this->admin_company_vat->LinkCustomAttributes = "";
			$this->admin_company_vat->HrefValue = "";
			$this->admin_company_vat->TooltipValue = "";

			// admin_company_logo_file
			$this->admin_company_logo_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
				$this->admin_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->admin_company_logo_file, $this->admin_company_logo_file->Upload->DbValue); // Add prefix/suffix
				$this->admin_company_logo_file->LinkAttrs["target"] = ""; // Add target
				if ($this->Export <> "") $this->admin_company_logo_file->HrefValue = ew_ConvertFullUrl($this->admin_company_logo_file->HrefValue);
			} else {
				$this->admin_company_logo_file->HrefValue = "";
			}
			$this->admin_company_logo_file->HrefValue2 = $this->admin_company_logo_file->UploadPath . $this->admin_company_logo_file->Upload->DbValue;
			$this->admin_company_logo_file->TooltipValue = "";
			if ($this->admin_company_logo_file->UseColorbox) {
				$this->admin_company_logo_file->LinkAttrs["title"] = $Language->Phrase("ViewImageGallery");
				$this->admin_company_logo_file->LinkAttrs["data-rel"] = "telecare_admin_x_admin_company_logo_file";

				//$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox ewTooltip img-thumbnail";
				//$this->admin_company_logo_file->LinkAttrs["data-placement"] = "bottom";
				//$this->admin_company_logo_file->LinkAttrs["data-container"] = "body";

				$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox img-thumbnail";
			}

			// admin_company_address
			$this->admin_company_address->LinkCustomAttributes = "";
			$this->admin_company_address->HrefValue = "";
			$this->admin_company_address->TooltipValue = "";

			// admin_company_region_id
			$this->admin_company_region_id->LinkCustomAttributes = "";
			$this->admin_company_region_id->HrefValue = "";
			$this->admin_company_region_id->TooltipValue = "";

			// admin_company_province_id
			$this->admin_company_province_id->LinkCustomAttributes = "";
			$this->admin_company_province_id->HrefValue = "";
			$this->admin_company_province_id->TooltipValue = "";

			// admin_company_city_id
			$this->admin_company_city_id->LinkCustomAttributes = "";
			$this->admin_company_city_id->HrefValue = "";
			$this->admin_company_city_id->TooltipValue = "";

			// admin_company_phone
			$this->admin_company_phone->LinkCustomAttributes = "";
			$this->admin_company_phone->HrefValue = "";
			$this->admin_company_phone->TooltipValue = "";

			// admin_company_email
			$this->admin_company_email->LinkCustomAttributes = "";
			$this->admin_company_email->HrefValue = "";
			$this->admin_company_email->TooltipValue = "";

			// admin_company_web
			$this->admin_company_web->LinkCustomAttributes = "";
			$this->admin_company_web->HrefValue = "";
			$this->admin_company_web->TooltipValue = "";

			// admin_contract
			$this->admin_contract->LinkCustomAttributes = "";
			$this->admin_contract->HrefValue = "";
			$this->admin_contract->TooltipValue = "";

			// admin_expire
			$this->admin_expire->LinkCustomAttributes = "";
			$this->admin_expire->HrefValue = "";
			$this->admin_expire->TooltipValue = "";

			// admin_company_facebook
			$this->admin_company_facebook->LinkCustomAttributes = "";
			$this->admin_company_facebook->HrefValue = "";
			$this->admin_company_facebook->TooltipValue = "";

			// admin_company_twitter
			$this->admin_company_twitter->LinkCustomAttributes = "";
			$this->admin_company_twitter->HrefValue = "";
			$this->admin_company_twitter->TooltipValue = "";

			// admin_company_googleplus
			$this->admin_company_googleplus->LinkCustomAttributes = "";
			$this->admin_company_googleplus->HrefValue = "";
			$this->admin_company_googleplus->TooltipValue = "";

			// admin_company_linkedin
			$this->admin_company_linkedin->LinkCustomAttributes = "";
			$this->admin_company_linkedin->HrefValue = "";
			$this->admin_company_linkedin->TooltipValue = "";

			// admin_link_playstore
			$this->admin_link_playstore->LinkCustomAttributes = "";
			$this->admin_link_playstore->HrefValue = "";
			$this->admin_link_playstore->TooltipValue = "";

			// admin_link_appstore
			$this->admin_link_appstore->LinkCustomAttributes = "";
			$this->admin_link_appstore->HrefValue = "";
			$this->admin_link_appstore->TooltipValue = "";

			// admin_link_windowsstore
			$this->admin_link_windowsstore->LinkCustomAttributes = "";
			$this->admin_link_windowsstore->HrefValue = "";
			$this->admin_link_windowsstore->TooltipValue = "";

			// admin_level
			$this->admin_level->LinkCustomAttributes = "";
			$this->admin_level->HrefValue = "";
			$this->admin_level->TooltipValue = "";

			// admin_username
			$this->admin_username->LinkCustomAttributes = "";
			$this->admin_username->HrefValue = "";
			$this->admin_username->TooltipValue = "";

			// admin_password
			$this->admin_password->LinkCustomAttributes = "";
			$this->admin_password->HrefValue = "";
			$this->admin_password->TooltipValue = "";

			// admin_last_update
			$this->admin_last_update->LinkCustomAttributes = "";
			$this->admin_last_update->HrefValue = "";
			$this->admin_last_update->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_SEARCH) { // Search row

			// admin_id
			$this->admin_id->EditAttrs["class"] = "form-control";
			$this->admin_id->EditCustomAttributes = "";
			if (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$this->UserIDAllow("search")) { // Non system admin
			$sFilterWrk = "";
			$sFilterWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter("");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_id` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_id` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->admin_id->EditValue = $arwrk;
			} else {
			$this->admin_id->EditValue = ew_HtmlEncode($this->admin_id->AdvancedSearch->SearchValue);
			$this->admin_id->PlaceHolder = ew_RemoveHtml($this->admin_id->FldCaption());
			}

			// admin_name
			$this->admin_name->EditAttrs["class"] = "form-control";
			$this->admin_name->EditCustomAttributes = "";
			$this->admin_name->EditValue = ew_HtmlEncode($this->admin_name->AdvancedSearch->SearchValue);
			$this->admin_name->PlaceHolder = ew_RemoveHtml($this->admin_name->FldCaption());

			// admin_surname
			$this->admin_surname->EditAttrs["class"] = "form-control";
			$this->admin_surname->EditCustomAttributes = "";
			$this->admin_surname->EditValue = ew_HtmlEncode($this->admin_surname->AdvancedSearch->SearchValue);
			$this->admin_surname->PlaceHolder = ew_RemoveHtml($this->admin_surname->FldCaption());

			// admin_gender
			$this->admin_gender->EditCustomAttributes = "";
			$this->admin_gender->EditValue = $this->admin_gender->Options(FALSE);

			// admin_email
			$this->admin_email->EditAttrs["class"] = "form-control";
			$this->admin_email->EditCustomAttributes = "";
			$this->admin_email->EditValue = ew_HtmlEncode($this->admin_email->AdvancedSearch->SearchValue);
			$this->admin_email->PlaceHolder = ew_RemoveHtml($this->admin_email->FldCaption());

			// admin_mobile
			$this->admin_mobile->EditAttrs["class"] = "form-control";
			$this->admin_mobile->EditCustomAttributes = "";
			$this->admin_mobile->EditValue = ew_HtmlEncode($this->admin_mobile->AdvancedSearch->SearchValue);
			$this->admin_mobile->PlaceHolder = ew_RemoveHtml($this->admin_mobile->FldCaption());

			// admin_phone
			$this->admin_phone->EditAttrs["class"] = "form-control";
			$this->admin_phone->EditCustomAttributes = "";
			$this->admin_phone->EditValue = ew_HtmlEncode($this->admin_phone->AdvancedSearch->SearchValue);
			$this->admin_phone->PlaceHolder = ew_RemoveHtml($this->admin_phone->FldCaption());

			// admin_modules
			$this->admin_modules->EditCustomAttributes = "";
			if (trim(strval($this->admin_modules->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$arwrk = explode(",", $this->admin_modules->AdvancedSearch->SearchValue);
				$sFilterWrk = "";
				foreach ($arwrk as $wrk) {
					if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
					$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
				}
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_modules, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->admin_modules->EditValue = $arwrk;

			// admin_language
			$this->admin_language->EditCustomAttributes = "";
			$this->admin_language->EditValue = $this->admin_language->Options(FALSE);

			// admin_is_active
			$this->admin_is_active->EditCustomAttributes = "";
			$this->admin_is_active->EditValue = $this->admin_is_active->Options(FALSE);

			// admin_company_name
			$this->admin_company_name->EditAttrs["class"] = "form-control";
			$this->admin_company_name->EditCustomAttributes = "";
			$this->admin_company_name->EditValue = ew_HtmlEncode($this->admin_company_name->AdvancedSearch->SearchValue);
			$this->admin_company_name->PlaceHolder = ew_RemoveHtml($this->admin_company_name->FldCaption());

			// admin_company_vat
			$this->admin_company_vat->EditAttrs["class"] = "form-control";
			$this->admin_company_vat->EditCustomAttributes = "";
			$this->admin_company_vat->EditValue = ew_HtmlEncode($this->admin_company_vat->AdvancedSearch->SearchValue);
			$this->admin_company_vat->PlaceHolder = ew_RemoveHtml($this->admin_company_vat->FldCaption());

			// admin_company_logo_file
			$this->admin_company_logo_file->EditAttrs["class"] = "form-control";
			$this->admin_company_logo_file->EditCustomAttributes = "";
			$this->admin_company_logo_file->EditValue = ew_HtmlEncode($this->admin_company_logo_file->AdvancedSearch->SearchValue);
			$this->admin_company_logo_file->PlaceHolder = ew_RemoveHtml($this->admin_company_logo_file->FldCaption());

			// admin_company_address
			$this->admin_company_address->EditAttrs["class"] = "form-control";
			$this->admin_company_address->EditCustomAttributes = "";
			$this->admin_company_address->EditValue = ew_HtmlEncode($this->admin_company_address->AdvancedSearch->SearchValue);
			$this->admin_company_address->PlaceHolder = ew_RemoveHtml($this->admin_company_address->FldCaption());

			// admin_company_region_id
			$this->admin_company_region_id->EditAttrs["class"] = "form-control";
			$this->admin_company_region_id->EditCustomAttributes = "";
			if (trim(strval($this->admin_company_region_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->admin_company_region_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->admin_company_region_id->EditValue = $arwrk;

			// admin_company_province_id
			$this->admin_company_province_id->EditAttrs["class"] = "form-control";
			$this->admin_company_province_id->EditCustomAttributes = "";
			if (trim(strval($this->admin_company_province_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->admin_company_province_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->admin_company_province_id->EditValue = $arwrk;

			// admin_company_city_id
			$this->admin_company_city_id->EditAttrs["class"] = "form-control";
			$this->admin_company_city_id->EditCustomAttributes = "";
			if (trim(strval($this->admin_company_city_id->AdvancedSearch->SearchValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->admin_company_city_id->AdvancedSearch->SearchValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->admin_company_city_id->EditValue = $arwrk;

			// admin_company_phone
			$this->admin_company_phone->EditAttrs["class"] = "form-control";
			$this->admin_company_phone->EditCustomAttributes = "";
			$this->admin_company_phone->EditValue = ew_HtmlEncode($this->admin_company_phone->AdvancedSearch->SearchValue);
			$this->admin_company_phone->PlaceHolder = ew_RemoveHtml($this->admin_company_phone->FldCaption());

			// admin_company_email
			$this->admin_company_email->EditAttrs["class"] = "form-control";
			$this->admin_company_email->EditCustomAttributes = "";
			$this->admin_company_email->EditValue = ew_HtmlEncode($this->admin_company_email->AdvancedSearch->SearchValue);
			$this->admin_company_email->PlaceHolder = ew_RemoveHtml($this->admin_company_email->FldCaption());

			// admin_company_web
			$this->admin_company_web->EditAttrs["class"] = "form-control";
			$this->admin_company_web->EditCustomAttributes = "";
			$this->admin_company_web->EditValue = ew_HtmlEncode($this->admin_company_web->AdvancedSearch->SearchValue);
			$this->admin_company_web->PlaceHolder = ew_RemoveHtml($this->admin_company_web->FldCaption());

			// admin_contract
			$this->admin_contract->EditAttrs["class"] = "form-control";
			$this->admin_contract->EditCustomAttributes = "";
			$this->admin_contract->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->admin_contract->AdvancedSearch->SearchValue, 7), 7));
			$this->admin_contract->PlaceHolder = ew_RemoveHtml($this->admin_contract->FldCaption());

			// admin_expire
			$this->admin_expire->EditAttrs["class"] = "form-control";
			$this->admin_expire->EditCustomAttributes = "";
			$this->admin_expire->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->admin_expire->AdvancedSearch->SearchValue, 7), 7));
			$this->admin_expire->PlaceHolder = ew_RemoveHtml($this->admin_expire->FldCaption());

			// admin_company_facebook
			$this->admin_company_facebook->EditAttrs["class"] = "form-control";
			$this->admin_company_facebook->EditCustomAttributes = "";
			$this->admin_company_facebook->EditValue = ew_HtmlEncode($this->admin_company_facebook->AdvancedSearch->SearchValue);
			$this->admin_company_facebook->PlaceHolder = ew_RemoveHtml($this->admin_company_facebook->FldCaption());

			// admin_company_twitter
			$this->admin_company_twitter->EditAttrs["class"] = "form-control";
			$this->admin_company_twitter->EditCustomAttributes = "";
			$this->admin_company_twitter->EditValue = ew_HtmlEncode($this->admin_company_twitter->AdvancedSearch->SearchValue);
			$this->admin_company_twitter->PlaceHolder = ew_RemoveHtml($this->admin_company_twitter->FldCaption());

			// admin_company_googleplus
			$this->admin_company_googleplus->EditAttrs["class"] = "form-control";
			$this->admin_company_googleplus->EditCustomAttributes = "";
			$this->admin_company_googleplus->EditValue = ew_HtmlEncode($this->admin_company_googleplus->AdvancedSearch->SearchValue);
			$this->admin_company_googleplus->PlaceHolder = ew_RemoveHtml($this->admin_company_googleplus->FldCaption());

			// admin_company_linkedin
			$this->admin_company_linkedin->EditAttrs["class"] = "form-control";
			$this->admin_company_linkedin->EditCustomAttributes = "";
			$this->admin_company_linkedin->EditValue = ew_HtmlEncode($this->admin_company_linkedin->AdvancedSearch->SearchValue);
			$this->admin_company_linkedin->PlaceHolder = ew_RemoveHtml($this->admin_company_linkedin->FldCaption());

			// admin_link_playstore
			$this->admin_link_playstore->EditAttrs["class"] = "form-control";
			$this->admin_link_playstore->EditCustomAttributes = "";
			$this->admin_link_playstore->EditValue = ew_HtmlEncode($this->admin_link_playstore->AdvancedSearch->SearchValue);
			$this->admin_link_playstore->PlaceHolder = ew_RemoveHtml($this->admin_link_playstore->FldCaption());

			// admin_link_appstore
			$this->admin_link_appstore->EditAttrs["class"] = "form-control";
			$this->admin_link_appstore->EditCustomAttributes = "";
			$this->admin_link_appstore->EditValue = ew_HtmlEncode($this->admin_link_appstore->AdvancedSearch->SearchValue);
			$this->admin_link_appstore->PlaceHolder = ew_RemoveHtml($this->admin_link_appstore->FldCaption());

			// admin_link_windowsstore
			$this->admin_link_windowsstore->EditAttrs["class"] = "form-control";
			$this->admin_link_windowsstore->EditCustomAttributes = "";
			$this->admin_link_windowsstore->EditValue = ew_HtmlEncode($this->admin_link_windowsstore->AdvancedSearch->SearchValue);
			$this->admin_link_windowsstore->PlaceHolder = ew_RemoveHtml($this->admin_link_windowsstore->FldCaption());

			// admin_level
			$this->admin_level->EditAttrs["class"] = "form-control";
			$this->admin_level->EditCustomAttributes = "";
			if (!$Security->CanAdmin()) { // System admin
				$this->admin_level->EditValue = $Language->Phrase("PasswordMask");
			} else {
			$this->admin_level->EditValue = $this->admin_level->Options(TRUE);
			}

			// admin_username
			$this->admin_username->EditAttrs["class"] = "form-control";
			$this->admin_username->EditCustomAttributes = "";
			$this->admin_username->EditValue = ew_HtmlEncode($this->admin_username->AdvancedSearch->SearchValue);
			$this->admin_username->PlaceHolder = ew_RemoveHtml($this->admin_username->FldCaption());

			// admin_password
			$this->admin_password->EditAttrs["class"] = "form-control ewPasswordStrength";
			$this->admin_password->EditCustomAttributes = "";
			$this->admin_password->EditValue = ew_HtmlEncode($this->admin_password->AdvancedSearch->SearchValue);
			$this->admin_password->PlaceHolder = ew_RemoveHtml($this->admin_password->FldCaption());

			// admin_last_update
			$this->admin_last_update->EditAttrs["class"] = "form-control";
			$this->admin_last_update->EditCustomAttributes = "";
			$this->admin_last_update->EditValue = ew_HtmlEncode(ew_FormatDateTime(ew_UnFormatDateTime($this->admin_last_update->AdvancedSearch->SearchValue, 7), 7));
			$this->admin_last_update->PlaceHolder = ew_RemoveHtml($this->admin_last_update->FldCaption());
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;
		if (!ew_CheckInteger($this->admin_id->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->admin_id->FldErrMsg());
		}
		if (!ew_CheckInteger($this->admin_company_vat->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->admin_company_vat->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->admin_contract->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->admin_contract->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->admin_expire->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->admin_expire->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->admin_last_update->AdvancedSearch->SearchValue)) {
			ew_AddMessage($gsSearchError, $this->admin_last_update->FldErrMsg());
		}

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->admin_id->AdvancedSearch->Load();
		$this->admin_name->AdvancedSearch->Load();
		$this->admin_surname->AdvancedSearch->Load();
		$this->admin_gender->AdvancedSearch->Load();
		$this->admin_email->AdvancedSearch->Load();
		$this->admin_mobile->AdvancedSearch->Load();
		$this->admin_phone->AdvancedSearch->Load();
		$this->admin_modules->AdvancedSearch->Load();
		$this->admin_language->AdvancedSearch->Load();
		$this->admin_is_active->AdvancedSearch->Load();
		$this->admin_company_name->AdvancedSearch->Load();
		$this->admin_company_vat->AdvancedSearch->Load();
		$this->admin_company_logo_file->AdvancedSearch->Load();
		$this->admin_company_address->AdvancedSearch->Load();
		$this->admin_company_region_id->AdvancedSearch->Load();
		$this->admin_company_province_id->AdvancedSearch->Load();
		$this->admin_company_city_id->AdvancedSearch->Load();
		$this->admin_company_phone->AdvancedSearch->Load();
		$this->admin_company_email->AdvancedSearch->Load();
		$this->admin_company_web->AdvancedSearch->Load();
		$this->admin_contract->AdvancedSearch->Load();
		$this->admin_expire->AdvancedSearch->Load();
		$this->admin_company_facebook->AdvancedSearch->Load();
		$this->admin_company_twitter->AdvancedSearch->Load();
		$this->admin_company_googleplus->AdvancedSearch->Load();
		$this->admin_company_linkedin->AdvancedSearch->Load();
		$this->admin_link_playstore->AdvancedSearch->Load();
		$this->admin_link_appstore->AdvancedSearch->Load();
		$this->admin_link_windowsstore->AdvancedSearch->Load();
		$this->admin_level->AdvancedSearch->Load();
		$this->admin_username->AdvancedSearch->Load();
		$this->admin_password->AdvancedSearch->Load();
		$this->admin_last_update->AdvancedSearch->Load();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_adminlist.php", "", $this->TableVar, TRUE);
		$PageId = "search";
		$Breadcrumb->Add("search", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$pages->Add(4);
		$this->MultiPages = $pages;
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_admin_search)) $telecare_admin_search = new ctelecare_admin_search();

// Page init
$telecare_admin_search->Page_Init();

// Page main
$telecare_admin_search->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_admin_search->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "search";
<?php if ($telecare_admin_search->IsModal) { ?>
var CurrentAdvancedSearchForm = ftelecare_adminsearch = new ew_Form("ftelecare_adminsearch", "search");
<?php } else { ?>
var CurrentForm = ftelecare_adminsearch = new ew_Form("ftelecare_adminsearch", "search");
<?php } ?>

// Form_CustomValidate event
ftelecare_adminsearch.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_adminsearch.ValidateRequired = true;
<?php } else { ?>
ftelecare_adminsearch.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_adminsearch.MultiPage = new ew_MultiPage("ftelecare_adminsearch");

// Dynamic selection lists
ftelecare_adminsearch.Lists["x_admin_gender"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_gender"].Options = <?php echo json_encode($telecare_admin->admin_gender->Options()) ?>;
ftelecare_adminsearch.Lists["x_admin_modules[]"] = {"LinkField":"x_invalidity_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_invalidity_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_language"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_language"].Options = <?php echo json_encode($telecare_admin->admin_language->Options()) ?>;
ftelecare_adminsearch.Lists["x_admin_is_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_is_active"].Options = <?php echo json_encode($telecare_admin->admin_is_active->Options()) ?>;
ftelecare_adminsearch.Lists["x_admin_company_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_admin_company_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_company_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":["x_admin_company_region_id"],"ChildFields":["x_admin_company_city_id"],"FilterFields":["x_province_region_id"],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_company_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":["x_admin_company_province_id"],"ChildFields":[],"FilterFields":["x_city_province_id"],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_level"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminsearch.Lists["x_admin_level"].Options = <?php echo json_encode($telecare_admin->admin_level->Options()) ?>;

// Form object for search
// Validate function for search

ftelecare_adminsearch.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	var infix = "";
	elm = this.GetElements("x" + infix + "_admin_id");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_id->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_admin_company_vat");
	if (elm && !ew_CheckInteger(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_company_vat->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_admin_contract");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_contract->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_admin_expire");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_expire->FldErrMsg()) ?>");
	elm = this.GetElements("x" + infix + "_admin_last_update");
	if (elm && !ew_CheckEuroDate(elm.value))
		return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_last_update->FldErrMsg()) ?>");

	// Fire Form_CustomValidate event
	if (!this.Form_CustomValidate(fobj))
		return false;
	return true;
}
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php if (!$telecare_admin_search->IsModal) { ?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $telecare_admin_search->ShowPageHeader(); ?>
<?php
$telecare_admin_search->ShowMessage();
?>
<form name="ftelecare_adminsearch" id="ftelecare_adminsearch" class="<?php echo $telecare_admin_search->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_admin_search->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_admin_search->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_admin">
<input type="hidden" name="a_search" id="a_search" value="S">
<?php if ($telecare_admin_search->IsModal) { ?>
<input type="hidden" name="modal" value="1">
<?php } ?>
<div class="ewMultiPage">
<div class="tabbable" id="telecare_admin_search">
	<ul class="nav<?php echo $telecare_admin_search->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_admin_search->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_admin1" data-toggle="tab"><?php echo $telecare_admin->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_admin_search->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_admin2" data-toggle="tab"><?php echo $telecare_admin->PageCaption(2) ?></a></li>
		<li<?php echo $telecare_admin_search->MultiPages->TabStyle("3") ?>><a href="#tab_telecare_admin3" data-toggle="tab"><?php echo $telecare_admin->PageCaption(3) ?></a></li>
		<li<?php echo $telecare_admin_search->MultiPages->TabStyle("4") ?>><a href="#tab_telecare_admin4" data-toggle="tab"><?php echo $telecare_admin->PageCaption(4) ?></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane<?php echo $telecare_admin_search->MultiPages->PageStyle("1") ?>" id="tab_telecare_admin1">
<div>
<?php if ($telecare_admin->admin_id->Visible) { // admin_id ?>
	<div id="r_admin_id" class="form-group">
		<label for="x_admin_id" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_id"><?php echo $telecare_admin->admin_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_id" id="z_admin_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_id->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_id">
<?php if (!$Security->IsAdmin() && $Security->IsLoggedIn() && !$telecare_admin->UserIDAllow("search")) { // Non system admin ?>
<select data-table="telecare_admin" data-field="x_admin_id" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_id->DisplayValueSeparator) ? json_encode($telecare_admin->admin_id->DisplayValueSeparator) : $telecare_admin->admin_id->DisplayValueSeparator) ?>" id="x_admin_id" name="x_admin_id"<?php echo $telecare_admin->admin_id->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_id->EditValue)) {
	$arwrk = $telecare_admin->admin_id->EditValue;
	if ($arwrk[0][0] <> "") echo "<option value=\"\">" . $Language->Phrase("PleaseSelect") . "</option>";
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_id->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php } else { ?>
<input type="text" data-table="telecare_admin" data-field="x_admin_id" data-page="1" name="x_admin_id" id="x_admin_id" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_id->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_id->EditValue ?>"<?php echo $telecare_admin->admin_id->EditAttributes() ?>>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_name->Visible) { // admin_name ?>
	<div id="r_admin_name" class="form-group">
		<label for="x_admin_name" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_name"><?php echo $telecare_admin->admin_name->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_name" id="z_admin_name" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_name->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_name">
<input type="text" data-table="telecare_admin" data-field="x_admin_name" data-page="1" name="x_admin_name" id="x_admin_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_name->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_name->EditValue ?>"<?php echo $telecare_admin->admin_name->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_surname->Visible) { // admin_surname ?>
	<div id="r_admin_surname" class="form-group">
		<label for="x_admin_surname" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_surname"><?php echo $telecare_admin->admin_surname->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_surname" id="z_admin_surname" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_surname->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_surname">
<input type="text" data-table="telecare_admin" data-field="x_admin_surname" data-page="1" name="x_admin_surname" id="x_admin_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_surname->EditValue ?>"<?php echo $telecare_admin->admin_surname->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_gender->Visible) { // admin_gender ?>
	<div id="r_admin_gender" class="form-group">
		<label class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_gender"><?php echo $telecare_admin->admin_gender->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_gender" id="z_admin_gender" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_gender->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_gender">
<div id="tp_x_admin_gender" class="ewTemplate"><input type="radio" data-table="telecare_admin" data-field="x_admin_gender" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_gender->DisplayValueSeparator) ? json_encode($telecare_admin->admin_gender->DisplayValueSeparator) : $telecare_admin->admin_gender->DisplayValueSeparator) ?>" name="x_admin_gender" id="x_admin_gender" value="{value}"<?php echo $telecare_admin->admin_gender->EditAttributes() ?>></div>
<div id="dsl_x_admin_gender" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_gender->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_admin->admin_gender->AdvancedSearch->SearchValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_gender" data-page="1" name="x_admin_gender" id="x_admin_gender_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_gender->EditAttributes() ?>><?php echo $telecare_admin->admin_gender->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_gender->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_gender" data-page="1" name="x_admin_gender" id="x_admin_gender_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_admin->admin_gender->CurrentValue) ?>" checked<?php echo $telecare_admin->admin_gender->EditAttributes() ?>><?php echo $telecare_admin->admin_gender->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_email->Visible) { // admin_email ?>
	<div id="r_admin_email" class="form-group">
		<label for="x_admin_email" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_email"><?php echo $telecare_admin->admin_email->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_email" id="z_admin_email" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_email->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_email">
<input type="text" data-table="telecare_admin" data-field="x_admin_email" data-page="1" name="x_admin_email" id="x_admin_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_email->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_email->EditValue ?>"<?php echo $telecare_admin->admin_email->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_mobile->Visible) { // admin_mobile ?>
	<div id="r_admin_mobile" class="form-group">
		<label for="x_admin_mobile" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_mobile"><?php echo $telecare_admin->admin_mobile->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_mobile" id="z_admin_mobile" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_mobile->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_mobile">
<input type="text" data-table="telecare_admin" data-field="x_admin_mobile" data-page="1" name="x_admin_mobile" id="x_admin_mobile" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_mobile->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_mobile->EditValue ?>"<?php echo $telecare_admin->admin_mobile->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_phone->Visible) { // admin_phone ?>
	<div id="r_admin_phone" class="form-group">
		<label for="x_admin_phone" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_phone"><?php echo $telecare_admin->admin_phone->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_phone" id="z_admin_phone" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_phone->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_phone">
<input type="text" data-table="telecare_admin" data-field="x_admin_phone" data-page="1" name="x_admin_phone" id="x_admin_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_phone->EditValue ?>"<?php echo $telecare_admin->admin_phone->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_modules->Visible) { // admin_modules ?>
	<div id="r_admin_modules" class="form-group">
		<label class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_modules"><?php echo $telecare_admin->admin_modules->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_modules" id="z_admin_modules" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_modules->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_modules">
<div id="tp_x_admin_modules" class="ewTemplate"><input type="checkbox" data-table="telecare_admin" data-field="x_admin_modules" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_modules->DisplayValueSeparator) ? json_encode($telecare_admin->admin_modules->DisplayValueSeparator) : $telecare_admin->admin_modules->DisplayValueSeparator) ?>" name="x_admin_modules[]" id="x_admin_modules[]" value="{value}"<?php echo $telecare_admin->admin_modules->EditAttributes() ?>></div>
<div id="dsl_x_admin_modules" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_modules->EditValue;
if (is_array($arwrk)) {
	$armultiwrk = (strval($telecare_admin->admin_modules->AdvancedSearch->SearchValue) <> "") ? explode(",", strval($telecare_admin->admin_modules->AdvancedSearch->SearchValue)) : array();
	$cnt = count($armultiwrk);
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = "";
		for ($ari = 0; $ari < $cnt; $ari++) {
			if (ew_SameStr($arwrk[$rowcntwrk][0], $armultiwrk[$ari]) && !is_null($armultiwrk[$ari])) {
				$armultiwrk[$ari] = NULL; // Marked for removal
				$selwrk = " checked";
				if ($selwrk <> "") $emptywrk = FALSE;
				break;
			}
		}
		if ($selwrk <> "") {
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_admin" data-field="x_admin_modules" data-page="1" name="x_admin_modules[]" id="x_admin_modules_<?php echo $rowcntwrk ?>[]" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_modules->EditAttributes() ?>><?php echo $telecare_admin->admin_modules->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
		}
	}
	for ($ari = 0; $ari < $cnt; $ari++) {
		if (!is_null($armultiwrk[$ari])) {
?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_admin" data-field="x_admin_modules" data-page="1" name="x_admin_modules[]" value="<?php echo ew_HtmlEncode($armultiwrk[$ari]) ?>" checked<?php echo $telecare_admin->admin_modules->EditAttributes() ?>><?php echo $armultiwrk[$ari] ?></label>
<?php
		}
	}
}
?>
</div></div>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
}
$telecare_admin->admin_modules->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_modules->LookupFilters += array("f0" => "`invalidity_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_modules, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
if ($sSqlWrk <> "") $telecare_admin->admin_modules->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_modules" id="s_x_admin_modules" value="<?php echo $telecare_admin->admin_modules->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_language->Visible) { // admin_language ?>
	<div id="r_admin_language" class="form-group">
		<label class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_language"><?php echo $telecare_admin->admin_language->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_language" id="z_admin_language" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_language->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_language">
<div id="tp_x_admin_language" class="ewTemplate"><input type="radio" data-table="telecare_admin" data-field="x_admin_language" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_language->DisplayValueSeparator) ? json_encode($telecare_admin->admin_language->DisplayValueSeparator) : $telecare_admin->admin_language->DisplayValueSeparator) ?>" name="x_admin_language" id="x_admin_language" value="{value}"<?php echo $telecare_admin->admin_language->EditAttributes() ?>></div>
<div id="dsl_x_admin_language" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_language->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_admin->admin_language->AdvancedSearch->SearchValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_language" data-page="1" name="x_admin_language" id="x_admin_language_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_language->EditAttributes() ?>><?php echo $telecare_admin->admin_language->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_language->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_language" data-page="1" name="x_admin_language" id="x_admin_language_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_admin->admin_language->CurrentValue) ?>" checked<?php echo $telecare_admin->admin_language->EditAttributes() ?>><?php echo $telecare_admin->admin_language->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_is_active->Visible) { // admin_is_active ?>
	<div id="r_admin_is_active" class="form-group">
		<label class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_is_active"><?php echo $telecare_admin->admin_is_active->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_is_active" id="z_admin_is_active" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_is_active->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_is_active">
<div id="tp_x_admin_is_active" class="ewTemplate"><input type="radio" data-table="telecare_admin" data-field="x_admin_is_active" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_is_active->DisplayValueSeparator) ? json_encode($telecare_admin->admin_is_active->DisplayValueSeparator) : $telecare_admin->admin_is_active->DisplayValueSeparator) ?>" name="x_admin_is_active" id="x_admin_is_active" value="{value}"<?php echo $telecare_admin->admin_is_active->EditAttributes() ?>></div>
<div id="dsl_x_admin_is_active" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_is_active->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_admin->admin_is_active->AdvancedSearch->SearchValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_is_active" data-page="1" name="x_admin_is_active" id="x_admin_is_active_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_is_active->EditAttributes() ?>><?php echo $telecare_admin->admin_is_active->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_is_active->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_is_active" data-page="1" name="x_admin_is_active" id="x_admin_is_active_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_admin->admin_is_active->CurrentValue) ?>" checked<?php echo $telecare_admin->admin_is_active->EditAttributes() ?>><?php echo $telecare_admin->admin_is_active->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_last_update->Visible) { // admin_last_update ?>
	<div id="r_admin_last_update" class="form-group">
		<label for="x_admin_last_update" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_last_update"><?php echo $telecare_admin->admin_last_update->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_last_update" id="z_admin_last_update" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_last_update->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_last_update">
<input type="text" data-table="telecare_admin" data-field="x_admin_last_update" data-page="1" data-format="7" name="x_admin_last_update" id="x_admin_last_update" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_last_update->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_last_update->EditValue ?>"<?php echo $telecare_admin->admin_last_update->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_admin_search->MultiPages->PageStyle("2") ?>" id="tab_telecare_admin2">
<div>
<?php if ($telecare_admin->admin_company_name->Visible) { // admin_company_name ?>
	<div id="r_admin_company_name" class="form-group">
		<label for="x_admin_company_name" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_name"><?php echo $telecare_admin->admin_company_name->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_name" id="z_admin_company_name" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_name->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_name">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_name" data-page="2" name="x_admin_company_name" id="x_admin_company_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_name->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_name->EditValue ?>"<?php echo $telecare_admin->admin_company_name->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_vat->Visible) { // admin_company_vat ?>
	<div id="r_admin_company_vat" class="form-group">
		<label for="x_admin_company_vat" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_vat"><?php echo $telecare_admin->admin_company_vat->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_company_vat" id="z_admin_company_vat" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_vat->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_vat">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_vat" data-page="2" name="x_admin_company_vat" id="x_admin_company_vat" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_vat->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_vat->EditValue ?>"<?php echo $telecare_admin->admin_company_vat->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_logo_file->Visible) { // admin_company_logo_file ?>
	<div id="r_admin_company_logo_file" class="form-group">
		<label class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_logo_file"><?php echo $telecare_admin->admin_company_logo_file->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_logo_file" id="z_admin_company_logo_file" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_logo_file->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_logo_file">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_logo_file" data-page="2" name="x_admin_company_logo_file" id="x_admin_company_logo_file" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_logo_file->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_logo_file->EditValue ?>"<?php echo $telecare_admin->admin_company_logo_file->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_address->Visible) { // admin_company_address ?>
	<div id="r_admin_company_address" class="form-group">
		<label for="x_admin_company_address" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_address"><?php echo $telecare_admin->admin_company_address->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_address" id="z_admin_company_address" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_address->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_address">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_address" data-page="2" name="x_admin_company_address" id="x_admin_company_address" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_address->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_address->EditValue ?>"<?php echo $telecare_admin->admin_company_address->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_region_id->Visible) { // admin_company_region_id ?>
	<div id="r_admin_company_region_id" class="form-group">
		<label for="x_admin_company_region_id" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_region_id"><?php echo $telecare_admin->admin_company_region_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_company_region_id" id="z_admin_company_region_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_region_id->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_region_id">
<?php $telecare_admin->admin_company_region_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_admin->admin_company_region_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_admin" data-field="x_admin_company_region_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_company_region_id->DisplayValueSeparator) ? json_encode($telecare_admin->admin_company_region_id->DisplayValueSeparator) : $telecare_admin->admin_company_region_id->DisplayValueSeparator) ?>" id="x_admin_company_region_id" name="x_admin_company_region_id"<?php echo $telecare_admin->admin_company_region_id->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_company_region_id->EditValue)) {
	$arwrk = $telecare_admin->admin_company_region_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_company_region_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_company_region_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_company_region_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_company_region_id->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_company_region_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
}
$telecare_admin->admin_company_region_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_company_region_id->LookupFilters += array("f0" => "`region_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_admin->admin_company_region_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_company_region_id" id="s_x_admin_company_region_id" value="<?php echo $telecare_admin->admin_company_region_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_province_id->Visible) { // admin_company_province_id ?>
	<div id="r_admin_company_province_id" class="form-group">
		<label for="x_admin_company_province_id" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_province_id"><?php echo $telecare_admin->admin_company_province_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_company_province_id" id="z_admin_company_province_id" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_province_id->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_province_id">
<?php $telecare_admin->admin_company_province_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_admin->admin_company_province_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_admin" data-field="x_admin_company_province_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_company_province_id->DisplayValueSeparator) ? json_encode($telecare_admin->admin_company_province_id->DisplayValueSeparator) : $telecare_admin->admin_company_province_id->DisplayValueSeparator) ?>" id="x_admin_company_province_id" name="x_admin_company_province_id"<?php echo $telecare_admin->admin_company_province_id->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_company_province_id->EditValue)) {
	$arwrk = $telecare_admin->admin_company_province_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_company_province_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_company_province_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_company_province_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_company_province_id->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_company_province_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_admin->admin_company_province_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_company_province_id->LookupFilters += array("f0" => "`province_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_admin->admin_company_province_id->LookupFilters += array("f1" => "`province_region_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_admin->admin_company_province_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_company_province_id" id="s_x_admin_company_province_id" value="<?php echo $telecare_admin->admin_company_province_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_city_id->Visible) { // admin_company_city_id ?>
	<div id="r_admin_company_city_id" class="form-group">
		<label for="x_admin_company_city_id" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_city_id"><?php echo $telecare_admin->admin_company_city_id->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_city_id" id="z_admin_company_city_id" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_city_id->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_city_id">
<select data-table="telecare_admin" data-field="x_admin_company_city_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_company_city_id->DisplayValueSeparator) ? json_encode($telecare_admin->admin_company_city_id->DisplayValueSeparator) : $telecare_admin->admin_company_city_id->DisplayValueSeparator) ?>" id="x_admin_company_city_id" name="x_admin_company_city_id"<?php echo $telecare_admin->admin_company_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_company_city_id->EditValue)) {
	$arwrk = $telecare_admin->admin_company_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_company_city_id->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_company_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_company_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_company_city_id->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_company_city_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_admin->admin_company_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_company_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_admin->admin_company_city_id->LookupFilters += array("f1" => "`city_province_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_admin->admin_company_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_company_city_id" id="s_x_admin_company_city_id" value="<?php echo $telecare_admin->admin_company_city_id->LookupFilterQuery() ?>">
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_phone->Visible) { // admin_company_phone ?>
	<div id="r_admin_company_phone" class="form-group">
		<label for="x_admin_company_phone" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_phone"><?php echo $telecare_admin->admin_company_phone->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_phone" id="z_admin_company_phone" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_phone->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_phone">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_phone" data-page="2" name="x_admin_company_phone" id="x_admin_company_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_phone->EditValue ?>"<?php echo $telecare_admin->admin_company_phone->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_email->Visible) { // admin_company_email ?>
	<div id="r_admin_company_email" class="form-group">
		<label for="x_admin_company_email" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_email"><?php echo $telecare_admin->admin_company_email->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_email" id="z_admin_company_email" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_email->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_email">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_email" data-page="2" name="x_admin_company_email" id="x_admin_company_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_email->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_email->EditValue ?>"<?php echo $telecare_admin->admin_company_email->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_web->Visible) { // admin_company_web ?>
	<div id="r_admin_company_web" class="form-group">
		<label for="x_admin_company_web" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_web"><?php echo $telecare_admin->admin_company_web->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_web" id="z_admin_company_web" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_web->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_web">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_web" data-page="2" name="x_admin_company_web" id="x_admin_company_web" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_web->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_web->EditValue ?>"<?php echo $telecare_admin->admin_company_web->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_contract->Visible) { // admin_contract ?>
	<div id="r_admin_contract" class="form-group">
		<label for="x_admin_contract" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_contract"><?php echo $telecare_admin->admin_contract->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_contract" id="z_admin_contract" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_contract->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_contract">
<input type="text" data-table="telecare_admin" data-field="x_admin_contract" data-page="2" data-format="7" name="x_admin_contract" id="x_admin_contract" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_contract->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_contract->EditValue ?>"<?php echo $telecare_admin->admin_contract->EditAttributes() ?>>
<?php if (!$telecare_admin->admin_contract->ReadOnly && !$telecare_admin->admin_contract->Disabled && !isset($telecare_admin->admin_contract->EditAttrs["readonly"]) && !isset($telecare_admin->admin_contract->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_adminsearch", "x_admin_contract", "%d/%m/%Y");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_expire->Visible) { // admin_expire ?>
	<div id="r_admin_expire" class="form-group">
		<label for="x_admin_expire" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_expire"><?php echo $telecare_admin->admin_expire->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_expire" id="z_admin_expire" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_expire->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_expire">
<input type="text" data-table="telecare_admin" data-field="x_admin_expire" data-page="2" data-format="7" name="x_admin_expire" id="x_admin_expire" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_expire->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_expire->EditValue ?>"<?php echo $telecare_admin->admin_expire->EditAttributes() ?>>
<?php if (!$telecare_admin->admin_expire->ReadOnly && !$telecare_admin->admin_expire->Disabled && !isset($telecare_admin->admin_expire->EditAttrs["readonly"]) && !isset($telecare_admin->admin_expire->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_adminsearch", "x_admin_expire", "%d/%m/%Y");
</script>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_admin_search->MultiPages->PageStyle("3") ?>" id="tab_telecare_admin3">
<div>
<?php if ($telecare_admin->admin_company_facebook->Visible) { // admin_company_facebook ?>
	<div id="r_admin_company_facebook" class="form-group">
		<label for="x_admin_company_facebook" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_facebook"><?php echo $telecare_admin->admin_company_facebook->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_facebook" id="z_admin_company_facebook" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_facebook->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_facebook">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_facebook" data-page="3" name="x_admin_company_facebook" id="x_admin_company_facebook" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_facebook->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_facebook->EditValue ?>"<?php echo $telecare_admin->admin_company_facebook->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_twitter->Visible) { // admin_company_twitter ?>
	<div id="r_admin_company_twitter" class="form-group">
		<label for="x_admin_company_twitter" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_twitter"><?php echo $telecare_admin->admin_company_twitter->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_twitter" id="z_admin_company_twitter" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_twitter->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_twitter">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_twitter" data-page="3" name="x_admin_company_twitter" id="x_admin_company_twitter" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_twitter->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_twitter->EditValue ?>"<?php echo $telecare_admin->admin_company_twitter->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_googleplus->Visible) { // admin_company_googleplus ?>
	<div id="r_admin_company_googleplus" class="form-group">
		<label for="x_admin_company_googleplus" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_googleplus"><?php echo $telecare_admin->admin_company_googleplus->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_googleplus" id="z_admin_company_googleplus" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_googleplus->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_googleplus">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_googleplus" data-page="3" name="x_admin_company_googleplus" id="x_admin_company_googleplus" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_googleplus->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_googleplus->EditValue ?>"<?php echo $telecare_admin->admin_company_googleplus->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_linkedin->Visible) { // admin_company_linkedin ?>
	<div id="r_admin_company_linkedin" class="form-group">
		<label for="x_admin_company_linkedin" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_company_linkedin"><?php echo $telecare_admin->admin_company_linkedin->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_company_linkedin" id="z_admin_company_linkedin" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_company_linkedin->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_company_linkedin">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_linkedin" data-page="3" name="x_admin_company_linkedin" id="x_admin_company_linkedin" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_linkedin->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_linkedin->EditValue ?>"<?php echo $telecare_admin->admin_company_linkedin->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_link_playstore->Visible) { // admin_link_playstore ?>
	<div id="r_admin_link_playstore" class="form-group">
		<label for="x_admin_link_playstore" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_link_playstore"><?php echo $telecare_admin->admin_link_playstore->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_link_playstore" id="z_admin_link_playstore" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_link_playstore->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_link_playstore">
<input type="text" data-table="telecare_admin" data-field="x_admin_link_playstore" data-page="3" name="x_admin_link_playstore" id="x_admin_link_playstore" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_link_playstore->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_link_playstore->EditValue ?>"<?php echo $telecare_admin->admin_link_playstore->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_link_appstore->Visible) { // admin_link_appstore ?>
	<div id="r_admin_link_appstore" class="form-group">
		<label for="x_admin_link_appstore" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_link_appstore"><?php echo $telecare_admin->admin_link_appstore->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_link_appstore" id="z_admin_link_appstore" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_link_appstore->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_link_appstore">
<input type="text" data-table="telecare_admin" data-field="x_admin_link_appstore" data-page="3" name="x_admin_link_appstore" id="x_admin_link_appstore" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_link_appstore->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_link_appstore->EditValue ?>"<?php echo $telecare_admin->admin_link_appstore->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_link_windowsstore->Visible) { // admin_link_windowsstore ?>
	<div id="r_admin_link_windowsstore" class="form-group">
		<label for="x_admin_link_windowsstore" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_link_windowsstore"><?php echo $telecare_admin->admin_link_windowsstore->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_link_windowsstore" id="z_admin_link_windowsstore" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_link_windowsstore->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_link_windowsstore">
<input type="text" data-table="telecare_admin" data-field="x_admin_link_windowsstore" data-page="3" name="x_admin_link_windowsstore" id="x_admin_link_windowsstore" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_link_windowsstore->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_link_windowsstore->EditValue ?>"<?php echo $telecare_admin->admin_link_windowsstore->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_admin_search->MultiPages->PageStyle("4") ?>" id="tab_telecare_admin4">
<div>
<?php if ($telecare_admin->admin_level->Visible) { // admin_level ?>
	<div id="r_admin_level" class="form-group">
		<label for="x_admin_level" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_level"><?php echo $telecare_admin->admin_level->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("=") ?><input type="hidden" name="z_admin_level" id="z_admin_level" value="="></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_level->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_level">
<?php if (!$Security->IsAdmin() && $Security->IsLoggedIn()) { // Non system admin ?>
<p class="form-control-static"><?php echo $telecare_admin->admin_level->EditValue ?></p>
<?php } else { ?>
<select data-table="telecare_admin" data-field="x_admin_level" data-page="4" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_level->DisplayValueSeparator) ? json_encode($telecare_admin->admin_level->DisplayValueSeparator) : $telecare_admin->admin_level->DisplayValueSeparator) ?>" id="x_admin_level" name="x_admin_level"<?php echo $telecare_admin->admin_level->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_level->EditValue)) {
	$arwrk = $telecare_admin->admin_level->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_level->AdvancedSearch->SearchValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_level->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_level->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_level->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_level->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php } ?>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_username->Visible) { // admin_username ?>
	<div id="r_admin_username" class="form-group">
		<label for="x_admin_username" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_username"><?php echo $telecare_admin->admin_username->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_username" id="z_admin_username" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_username->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_username">
<input type="text" data-table="telecare_admin" data-field="x_admin_username" data-page="4" name="x_admin_username" id="x_admin_username" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_username->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_username->EditValue ?>"<?php echo $telecare_admin->admin_username->EditAttributes() ?>>
</span>
		</div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_password->Visible) { // admin_password ?>
	<div id="r_admin_password" class="form-group">
		<label for="x_admin_password" class="<?php echo $telecare_admin_search->SearchLabelClass ?>"><span id="elh_telecare_admin_admin_password"><?php echo $telecare_admin->admin_password->FldCaption() ?></span>	
		<p class="form-control-static ewSearchOperator"><?php echo $Language->Phrase("LIKE") ?><input type="hidden" name="z_admin_password" id="z_admin_password" value="LIKE"></p>
		</label>
		<div class="<?php echo $telecare_admin_search->SearchRightColumnClass ?>"><div<?php echo $telecare_admin->admin_password->CellAttributes() ?>>
			<span id="el_telecare_admin_admin_password">
<div class="input-group" id="ig_x_admin_password">
<input type="password" data-password-strength="pst_x_admin_password" data-password-generated="pgt_x_admin_password" data-table="telecare_admin" data-field="x_admin_password" data-page="4" name="x_admin_password" id="x_admin_password" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_password->getPlaceHolder()) ?>"<?php echo $telecare_admin->admin_password->EditAttributes() ?>>
<span class="input-group-btn">
	<button type="button" class="btn btn-default ewPasswordGenerator" title="<?php echo ew_HtmlTitle($Language->Phrase("GeneratePassword")) ?>" data-password-field="x_admin_password" data-password-confirm="c_admin_password" data-password-strength="pst_x_admin_password" data-password-generated="pgt_x_admin_password"><?php echo $Language->Phrase("GeneratePassword") ?></button>
</span>
</div>
<span class="help-block" id="pgt_x_admin_password" style="display: none;"></span>
<div class="progress ewPasswordStrengthBar" id="pst_x_admin_password" style="display: none;">
	<div class="progress-bar" role="progressbar"></div>
</div>
</span>
		</div></div>
	</div>
<?php } ?>
</div>
		</div>
	</div>
</div>
</div>
<?php if (!$telecare_admin_search->IsModal) { ?>
<div class="form-group">
	<div class="col-sm-offset-3 col-sm-9">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("Search") ?></button>
<button class="btn btn-default ewButton" name="btnReset" id="btnReset" type="button" onclick="ew_ClearForm(this.form);"><?php echo $Language->Phrase("Reset") ?></button>
	</div>
</div>
<?php } ?>
</form>
<script type="text/javascript">
ftelecare_adminsearch.Init();
</script>
<?php
$telecare_admin_search->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_admin_search->Page_Terminate();
?>
