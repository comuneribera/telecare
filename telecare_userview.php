<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_callgridcls.php" ?>
<?php include_once "telecare_alarmgridcls.php" ?>
<?php include_once "telecare_prescriptiongridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_user_view = NULL; // Initialize page object first

class ctelecare_user_view extends ctelecare_user {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_user';

	// Page object name
	var $PageObjName = 'telecare_user_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;
    var $AuditTrailOnAdd = FALSE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_user)
		if (!isset($GLOBALS["telecare_user"]) || get_class($GLOBALS["telecare_user"]) == "ctelecare_user") {
			$GLOBALS["telecare_user"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_user"];
		}
		$KeyUrl = "";
		if (@$_GET["user_id"] <> "") {
			$this->RecKey["user_id"] = $_GET["user_id"];
			$KeyUrl .= "&amp;user_id=" . urlencode($this->RecKey["user_id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_user', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanView()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_userlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header
		if (@$_GET["user_id"] <> "") {
			if ($gsExportFile <> "") $gsExportFile .= "_";
			$gsExportFile .= ew_StripSlashes($_GET["user_id"]);
		}

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Setup export options
		$this->SetupExportOptions();
		$this->user_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_user;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_user);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;
	var $MultiPages; // Multi pages object

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		if ($this->Export == "")
			$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["user_id"] <> "") {
				$this->user_id->setQueryStringValue($_GET["user_id"]);
				$this->RecKey["user_id"] = $this->user_id->QueryStringValue;
			} elseif (@$_POST["user_id"] <> "") {
				$this->user_id->setFormValue($_POST["user_id"]);
				$this->RecKey["user_id"] = $this->user_id->FormValue;
			} else {
				$bLoadCurrentRecord = TRUE;
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					$this->StartRec = 1; // Initialize start position
					if ($this->Recordset = $this->LoadRecordset()) // Load records
						$this->TotalRecs = $this->Recordset->RecordCount(); // Get record count
					if ($this->TotalRecs <= 0) { // No record found
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$this->Page_Terminate("telecare_userlist.php"); // Return to list page
					} elseif ($bLoadCurrentRecord) { // Load current record position
						$this->SetUpStartRec(); // Set up start record position

						// Point to current record
						if (intval($this->StartRec) <= intval($this->TotalRecs)) {
							$bMatchRecord = TRUE;
							$this->Recordset->Move($this->StartRec-1);
						}
					} else { // Match key values
						while (!$this->Recordset->EOF) {
							if (strval($this->user_id->CurrentValue) == strval($this->Recordset->fields('user_id'))) {
								$this->setStartRecordNumber($this->StartRec); // Save record position
								$bMatchRecord = TRUE;
								break;
							} else {
								$this->StartRec++;
								$this->Recordset->MoveNext();
							}
						}
					}
					if (!$bMatchRecord) {
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "telecare_userlist.php"; // No matching record, return to list
					} else {
						$this->LoadRowValues($this->Recordset); // Load row values
					}
			}

			// Export data only
			if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
				$this->ExportData();
				$this->Page_Terminate(); // Terminate response
				exit();
			}
		} else {
			$sReturnUrl = "telecare_userlist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();

		// Set up detail parameters
		$this->SetUpDetailParms();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageAddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageEditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "" && $Security->CanEdit());

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "" && $Security->CanAdd());

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewPageDeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "" && $Security->CanDelete());
		$option = &$options["detail"];
		$DetailTableLink = "";
		$DetailViewTblVar = "";
		$DetailCopyTblVar = "";
		$DetailEditTblVar = "";

		// "detail_telecare_call"
		$item = &$option->Add("detail_telecare_call");
		$body = $Language->Phrase("ViewPageDetailLink") . $Language->TablePhrase("telecare_call", "TblCaption");
		$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("telecare_calllist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "") . "\">" . $body . "</a>";
		$links = "";
		if ($GLOBALS["telecare_call_grid"] && $GLOBALS["telecare_call_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_call')) {
			$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
			$DetailViewTblVar .= "telecare_call";
		}
		if ($GLOBALS["telecare_call_grid"] && $GLOBALS["telecare_call_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_call')) {
			$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
			$DetailEditTblVar .= "telecare_call";
		}
		if ($GLOBALS["telecare_call_grid"] && $GLOBALS["telecare_call_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_call')) {
			$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_call")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
			$DetailCopyTblVar .= "telecare_call";
		}
		if ($links <> "") {
			$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
			$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
		}
		$body = "<div class=\"btn-group\">" . $body . "</div>";
		$item->Body = $body;
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'telecare_call');
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "telecare_call";
		}
		if ($this->ShowMultipleDetails) $item->Visible = FALSE;

		// "detail_telecare_alarm"
		$item = &$option->Add("detail_telecare_alarm");
		$body = $Language->Phrase("ViewPageDetailLink") . $Language->TablePhrase("telecare_alarm", "TblCaption");
		$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("telecare_alarmlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "") . "\">" . $body . "</a>";
		$links = "";
		if ($GLOBALS["telecare_alarm_grid"] && $GLOBALS["telecare_alarm_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_alarm')) {
			$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
			$DetailViewTblVar .= "telecare_alarm";
		}
		if ($GLOBALS["telecare_alarm_grid"] && $GLOBALS["telecare_alarm_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_alarm')) {
			$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
			$DetailEditTblVar .= "telecare_alarm";
		}
		if ($GLOBALS["telecare_alarm_grid"] && $GLOBALS["telecare_alarm_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_alarm')) {
			$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_alarm")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
			$DetailCopyTblVar .= "telecare_alarm";
		}
		if ($links <> "") {
			$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
			$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
		}
		$body = "<div class=\"btn-group\">" . $body . "</div>";
		$item->Body = $body;
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'telecare_alarm');
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "telecare_alarm";
		}
		if ($this->ShowMultipleDetails) $item->Visible = FALSE;

		// "detail_telecare_prescription"
		$item = &$option->Add("detail_telecare_prescription");
		$body = $Language->Phrase("ViewPageDetailLink") . $Language->TablePhrase("telecare_prescription", "TblCaption");
		$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("telecare_prescriptionlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_user&fk_user_id=" . urlencode(strval($this->user_id->CurrentValue)) . "") . "\">" . $body . "</a>";
		$links = "";
		if ($GLOBALS["telecare_prescription_grid"] && $GLOBALS["telecare_prescription_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_prescription')) {
			$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
			$DetailViewTblVar .= "telecare_prescription";
		}
		if ($GLOBALS["telecare_prescription_grid"] && $GLOBALS["telecare_prescription_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_prescription')) {
			$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
			$DetailEditTblVar .= "telecare_prescription";
		}
		if ($GLOBALS["telecare_prescription_grid"] && $GLOBALS["telecare_prescription_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_prescription')) {
			$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
			$DetailCopyTblVar .= "telecare_prescription";
		}
		if ($links <> "") {
			$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
			$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
		}
		$body = "<div class=\"btn-group\">" . $body . "</div>";
		$item->Body = $body;
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'telecare_prescription');
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "telecare_prescription";
		}
		if ($this->ShowMultipleDetails) $item->Visible = FALSE;

		// Multiple details
		if ($this->ShowMultipleDetails) {
			$body = $Language->Phrase("MultipleMasterDetails");
			$body = "<div class=\"btn-group\">";
			$links = "";
			if ($DetailViewTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailViewTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			}
			if ($DetailEditTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailEditTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			}
			if ($DetailCopyTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailCopyTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewMasterDetail\" title=\"" . ew_HtmlTitle($Language->Phrase("MultipleMasterDetails")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("MultipleMasterDetails") . "<b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu ewMenu\">". $links . "</ul>";
			}
			$body .= "</div>";

			// Multiple details
			$oListOpt = &$option->Add("details");
			$oListOpt->Body = $body;
		}

		// Set up detail default
		$option = &$options["detail"];
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$option->UseImageAndText = TRUE;
		$ar = explode(",", $DetailTableLink);
		$cnt = count($ar);
		$option->UseDropDownButton = ($cnt > 1);
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Set up action default
		$option = &$options["action"];
		$option->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
		$option->UseImageAndText = TRUE;
		$option->UseDropDownButton = FALSE;
		$option->UseButtonGroup = TRUE;
		$item = &$option->Add($option->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		if ($this->AuditTrailOnView) $this->WriteAuditTrailOnView($row);
		$this->user_id->setDbValue($rs->fields('user_id'));
		$this->user_surname->setDbValue($rs->fields('user_surname'));
		$this->user_name->setDbValue($rs->fields('user_name'));
		$this->user_gender->setDbValue($rs->fields('user_gender'));
		$this->user_born->setDbValue($rs->fields('user_born'));
		$this->user_modules->setDbValue($rs->fields('user_modules'));
		$this->user_doctor_id->setDbValue($rs->fields('user_doctor_id'));
		$this->user_is_active->setDbValue($rs->fields('user_is_active'));
		$this->user_medical_history->setDbValue($rs->fields('user_medical_history'));
		$this->user_address->setDbValue($rs->fields('user_address'));
		$this->user_region_id->setDbValue($rs->fields('user_region_id'));
		$this->user_province_id->setDbValue($rs->fields('user_province_id'));
		$this->user_city_id->setDbValue($rs->fields('user_city_id'));
		$this->user_email->setDbValue($rs->fields('user_email'));
		$this->user_phone->setDbValue($rs->fields('user_phone'));
		$this->user_language->setDbValue($rs->fields('user_language'));
		$this->user_username->setDbValue($rs->fields('user_username'));
		$this->user_password->setDbValue($rs->fields('user_password'));
		$this->user_latitude->setDbValue($rs->fields('user_latitude'));
		$this->user_longitude->setDbValue($rs->fields('user_longitude'));
		$this->user_last_update->setDbValue($rs->fields('user_last_update'));
		$this->user_admin_id->setDbValue($rs->fields('user_admin_id'));
		$this->user_level->setDbValue($rs->fields('user_level'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->user_id->DbValue = $row['user_id'];
		$this->user_surname->DbValue = $row['user_surname'];
		$this->user_name->DbValue = $row['user_name'];
		$this->user_gender->DbValue = $row['user_gender'];
		$this->user_born->DbValue = $row['user_born'];
		$this->user_modules->DbValue = $row['user_modules'];
		$this->user_doctor_id->DbValue = $row['user_doctor_id'];
		$this->user_is_active->DbValue = $row['user_is_active'];
		$this->user_medical_history->DbValue = $row['user_medical_history'];
		$this->user_address->DbValue = $row['user_address'];
		$this->user_region_id->DbValue = $row['user_region_id'];
		$this->user_province_id->DbValue = $row['user_province_id'];
		$this->user_city_id->DbValue = $row['user_city_id'];
		$this->user_email->DbValue = $row['user_email'];
		$this->user_phone->DbValue = $row['user_phone'];
		$this->user_language->DbValue = $row['user_language'];
		$this->user_username->DbValue = $row['user_username'];
		$this->user_password->DbValue = $row['user_password'];
		$this->user_latitude->DbValue = $row['user_latitude'];
		$this->user_longitude->DbValue = $row['user_longitude'];
		$this->user_last_update->DbValue = $row['user_last_update'];
		$this->user_admin_id->DbValue = $row['user_admin_id'];
		$this->user_level->DbValue = $row['user_level'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// user_id
		// user_surname
		// user_name
		// user_gender
		// user_born
		// user_modules
		// user_doctor_id
		// user_is_active
		// user_medical_history
		// user_address
		// user_region_id
		// user_province_id
		// user_city_id
		// user_email
		// user_phone
		// user_language
		// user_username
		// user_password
		// user_latitude
		// user_longitude
		// user_last_update
		// user_admin_id
		// user_level

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// user_id
		$this->user_id->ViewValue = $this->user_id->CurrentValue;
		$this->user_id->ViewCustomAttributes = "";

		// user_surname
		$this->user_surname->ViewValue = $this->user_surname->CurrentValue;
		$this->user_surname->ViewCustomAttributes = "";

		// user_name
		$this->user_name->ViewValue = $this->user_name->CurrentValue;
		$this->user_name->ViewCustomAttributes = "";

		// user_gender
		if (strval($this->user_gender->CurrentValue) <> "") {
			$this->user_gender->ViewValue = $this->user_gender->OptionCaption($this->user_gender->CurrentValue);
		} else {
			$this->user_gender->ViewValue = NULL;
		}
		$this->user_gender->ViewCustomAttributes = "";

		// user_born
		$this->user_born->ViewValue = $this->user_born->CurrentValue;
		$this->user_born->ViewValue = ew_FormatDateTime($this->user_born->ViewValue, 7);
		$this->user_born->ViewCustomAttributes = "";

		// user_modules
		if (strval($this->user_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->user_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->user_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->user_modules->ViewValue .= $this->user_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->user_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->user_modules->ViewValue = $this->user_modules->CurrentValue;
			}
		} else {
			$this->user_modules->ViewValue = NULL;
		}
		$this->user_modules->ViewCustomAttributes = "";

		// user_doctor_id
		if (strval($this->user_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = " `admin_level` = 2 ";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->CurrentValue;
			}
		} else {
			$this->user_doctor_id->ViewValue = NULL;
		}
		$this->user_doctor_id->ViewCustomAttributes = "";

		// user_is_active
		$this->user_is_active->ViewValue = $this->user_is_active->CurrentValue;
		$this->user_is_active->ViewCustomAttributes = "";

		// user_medical_history
		$this->user_medical_history->ViewValue = $this->user_medical_history->CurrentValue;
		$this->user_medical_history->ViewCustomAttributes = "";

		// user_address
		$this->user_address->ViewValue = $this->user_address->CurrentValue;
		$this->user_address->ViewCustomAttributes = "";

		// user_region_id
		if (strval($this->user_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->user_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `region_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_region_id->ViewValue = $this->user_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_region_id->ViewValue = $this->user_region_id->CurrentValue;
			}
		} else {
			$this->user_region_id->ViewValue = NULL;
		}
		$this->user_region_id->ViewCustomAttributes = "";

		// user_province_id
		if (strval($this->user_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->user_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `province_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_province_id->ViewValue = $this->user_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_province_id->ViewValue = $this->user_province_id->CurrentValue;
			}
		} else {
			$this->user_province_id->ViewValue = NULL;
		}
		$this->user_province_id->ViewCustomAttributes = "";

		// user_city_id
		if (strval($this->user_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_city_id->ViewValue = $this->user_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_city_id->ViewValue = $this->user_city_id->CurrentValue;
			}
		} else {
			$this->user_city_id->ViewValue = NULL;
		}
		$this->user_city_id->ViewCustomAttributes = "";

		// user_email
		$this->user_email->ViewValue = $this->user_email->CurrentValue;
		$this->user_email->ViewCustomAttributes = "";

		// user_phone
		$this->user_phone->ViewValue = $this->user_phone->CurrentValue;
		$this->user_phone->ViewCustomAttributes = "";

		// user_language
		$this->user_language->ViewValue = $this->user_language->CurrentValue;
		$this->user_language->ViewCustomAttributes = "";

		// user_username
		$this->user_username->ViewValue = $this->user_username->CurrentValue;
		$this->user_username->ViewCustomAttributes = "";

		// user_password
		$this->user_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->user_password->ViewCustomAttributes = "";

		// user_latitude
		$this->user_latitude->ViewValue = $this->user_latitude->CurrentValue;
		$this->user_latitude->ViewCustomAttributes = "";

		// user_longitude
		$this->user_longitude->ViewValue = $this->user_longitude->CurrentValue;
		$this->user_longitude->ViewCustomAttributes = "";

		// user_last_update
		$this->user_last_update->ViewValue = $this->user_last_update->CurrentValue;
		$this->user_last_update->ViewValue = ew_FormatDateTime($this->user_last_update->ViewValue, 7);
		$this->user_last_update->ViewCustomAttributes = "";

		// user_admin_id
		$this->user_admin_id->ViewValue = $this->user_admin_id->CurrentValue;
		$this->user_admin_id->ViewCustomAttributes = "";

		// user_level
		$this->user_level->ViewValue = $this->user_level->CurrentValue;
		$this->user_level->ViewCustomAttributes = "";

			// user_id
			$this->user_id->LinkCustomAttributes = "";
			$this->user_id->HrefValue = "";
			$this->user_id->TooltipValue = "";

			// user_surname
			$this->user_surname->LinkCustomAttributes = "";
			$this->user_surname->HrefValue = "";
			$this->user_surname->TooltipValue = "";

			// user_name
			$this->user_name->LinkCustomAttributes = "";
			$this->user_name->HrefValue = "";
			$this->user_name->TooltipValue = "";

			// user_gender
			$this->user_gender->LinkCustomAttributes = "";
			$this->user_gender->HrefValue = "";
			$this->user_gender->TooltipValue = "";

			// user_born
			$this->user_born->LinkCustomAttributes = "";
			$this->user_born->HrefValue = "";
			$this->user_born->TooltipValue = "";

			// user_modules
			$this->user_modules->LinkCustomAttributes = "";
			$this->user_modules->HrefValue = "";
			$this->user_modules->TooltipValue = "";

			// user_doctor_id
			$this->user_doctor_id->LinkCustomAttributes = "";
			$this->user_doctor_id->HrefValue = "";
			$this->user_doctor_id->TooltipValue = "";

			// user_is_active
			$this->user_is_active->LinkCustomAttributes = "";
			$this->user_is_active->HrefValue = "";
			$this->user_is_active->TooltipValue = "";

			// user_medical_history
			$this->user_medical_history->LinkCustomAttributes = "";
			$this->user_medical_history->HrefValue = "";
			$this->user_medical_history->TooltipValue = "";

			// user_address
			$this->user_address->LinkCustomAttributes = "";
			$this->user_address->HrefValue = "";
			$this->user_address->TooltipValue = "";

			// user_region_id
			$this->user_region_id->LinkCustomAttributes = "";
			$this->user_region_id->HrefValue = "";
			$this->user_region_id->TooltipValue = "";

			// user_province_id
			$this->user_province_id->LinkCustomAttributes = "";
			$this->user_province_id->HrefValue = "";
			$this->user_province_id->TooltipValue = "";

			// user_city_id
			$this->user_city_id->LinkCustomAttributes = "";
			$this->user_city_id->HrefValue = "";
			$this->user_city_id->TooltipValue = "";

			// user_email
			$this->user_email->LinkCustomAttributes = "";
			$this->user_email->HrefValue = "";
			$this->user_email->TooltipValue = "";

			// user_phone
			$this->user_phone->LinkCustomAttributes = "";
			$this->user_phone->HrefValue = "";
			$this->user_phone->TooltipValue = "";

			// user_language
			$this->user_language->LinkCustomAttributes = "";
			$this->user_language->HrefValue = "";
			$this->user_language->TooltipValue = "";

			// user_latitude
			$this->user_latitude->LinkCustomAttributes = "";
			$this->user_latitude->HrefValue = "";
			$this->user_latitude->TooltipValue = "";

			// user_longitude
			$this->user_longitude->LinkCustomAttributes = "";
			$this->user_longitude->HrefValue = "";
			$this->user_longitude->TooltipValue = "";

			// user_last_update
			$this->user_last_update->LinkCustomAttributes = "";
			$this->user_last_update->HrefValue = "";
			$this->user_last_update->TooltipValue = "";

			// user_admin_id
			$this->user_admin_id->LinkCustomAttributes = "";
			$this->user_admin_id->HrefValue = "";
			$this->user_admin_id->TooltipValue = "";

			// user_level
			$this->user_level->LinkCustomAttributes = "";
			$this->user_level->HrefValue = "";
			$this->user_level->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = TRUE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_telecare_user\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_telecare_user',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.ftelecare_userview,key:" . ew_ArrayToJsonAttr($this->RecKey) . ",sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide options for export
		if ($this->Export <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = FALSE;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;
		$this->SetUpStartRec(); // Set up start record position

		// Set the last record to display
		if ($this->DisplayRecs <= 0) {
			$this->StopRec = $this->TotalRecs;
		} else {
			$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
		}
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "v");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "view");

		// Export detail records (telecare_call)
		if (EW_EXPORT_DETAIL_RECORDS && in_array("telecare_call", explode(",", $this->getCurrentDetailTable()))) {
			global $telecare_call;
			if (!isset($telecare_call)) $telecare_call = new ctelecare_call;
			$rsdetail = $telecare_call->LoadRs($telecare_call->GetDetailFilter()); // Load detail records
			if ($rsdetail && !$rsdetail->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("h"); // Change to horizontal
				if ($this->Export <> "csv" || EW_EXPORT_DETAIL_RECORDS_FOR_CSV) {
					$Doc->ExportEmptyRow();
					$detailcnt = $rsdetail->RecordCount();
					$telecare_call->ExportDocument($Doc, $rsdetail, 1, $detailcnt);
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsdetail->Close();
			}
		}

		// Export detail records (telecare_alarm)
		if (EW_EXPORT_DETAIL_RECORDS && in_array("telecare_alarm", explode(",", $this->getCurrentDetailTable()))) {
			global $telecare_alarm;
			if (!isset($telecare_alarm)) $telecare_alarm = new ctelecare_alarm;
			$rsdetail = $telecare_alarm->LoadRs($telecare_alarm->GetDetailFilter()); // Load detail records
			if ($rsdetail && !$rsdetail->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("h"); // Change to horizontal
				if ($this->Export <> "csv" || EW_EXPORT_DETAIL_RECORDS_FOR_CSV) {
					$Doc->ExportEmptyRow();
					$detailcnt = $rsdetail->RecordCount();
					$telecare_alarm->ExportDocument($Doc, $rsdetail, 1, $detailcnt);
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsdetail->Close();
			}
		}

		// Export detail records (telecare_prescription)
		if (EW_EXPORT_DETAIL_RECORDS && in_array("telecare_prescription", explode(",", $this->getCurrentDetailTable()))) {
			global $telecare_prescription;
			if (!isset($telecare_prescription)) $telecare_prescription = new ctelecare_prescription;
			$rsdetail = $telecare_prescription->LoadRs($telecare_prescription->GetDetailFilter()); // Load detail records
			if ($rsdetail && !$rsdetail->EOF) {
				$ExportStyle = $Doc->Style;
				$Doc->SetStyle("h"); // Change to horizontal
				if ($this->Export <> "csv" || EW_EXPORT_DETAIL_RECORDS_FOR_CSV) {
					$Doc->ExportEmptyRow();
					$detailcnt = $rsdetail->RecordCount();
					$telecare_prescription->ExportDocument($Doc, $rsdetail, 1, $detailcnt);
				}
				$Doc->SetStyle($ExportStyle); // Restore
				$rsdetail->Close();
			}
		}
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("telecare_call", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_call_grid"]))
					$GLOBALS["telecare_call_grid"] = new ctelecare_call_grid;
				if ($GLOBALS["telecare_call_grid"]->DetailView) {
					$GLOBALS["telecare_call_grid"]->CurrentMode = "view";

					// Save current master table to detail table
					$GLOBALS["telecare_call_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_call_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_call_grid"]->call_user_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_call_grid"]->call_user_id->CurrentValue = $this->user_id->CurrentValue;
					$GLOBALS["telecare_call_grid"]->call_user_id->setSessionValue($GLOBALS["telecare_call_grid"]->call_user_id->CurrentValue);
				}
			}
			if (in_array("telecare_alarm", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_alarm_grid"]))
					$GLOBALS["telecare_alarm_grid"] = new ctelecare_alarm_grid;
				if ($GLOBALS["telecare_alarm_grid"]->DetailView) {
					$GLOBALS["telecare_alarm_grid"]->CurrentMode = "view";

					// Save current master table to detail table
					$GLOBALS["telecare_alarm_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_alarm_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_alarm_grid"]->alarm_user_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_alarm_grid"]->alarm_user_id->CurrentValue = $this->user_id->CurrentValue;
					$GLOBALS["telecare_alarm_grid"]->alarm_user_id->setSessionValue($GLOBALS["telecare_alarm_grid"]->alarm_user_id->CurrentValue);
				}
			}
			if (in_array("telecare_prescription", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_prescription_grid"]))
					$GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;
				if ($GLOBALS["telecare_prescription_grid"]->DetailView) {
					$GLOBALS["telecare_prescription_grid"]->CurrentMode = "view";

					// Save current master table to detail table
					$GLOBALS["telecare_prescription_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_prescription_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_prescription_grid"]->prescription_user_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_prescription_grid"]->prescription_user_id->CurrentValue = $this->user_id->CurrentValue;
					$GLOBALS["telecare_prescription_grid"]->prescription_user_id->setSessionValue($GLOBALS["telecare_prescription_grid"]->prescription_user_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_userlist.php", "", $this->TableVar, TRUE);
		$PageId = "view";
		$Breadcrumb->Add("view", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$pages->Add(4);
		$this->MultiPages = $pages;
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'telecare_user';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_user_view)) $telecare_user_view = new ctelecare_user_view();

// Page init
$telecare_user_view->Page_Init();

// Page main
$telecare_user_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_user_view->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($telecare_user->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "view";
var CurrentForm = ftelecare_userview = new ew_Form("ftelecare_userview", "view");

// Form_CustomValidate event
ftelecare_userview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_userview.ValidateRequired = true;
<?php } else { ?>
ftelecare_userview.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_userview.MultiPage = new ew_MultiPage("ftelecare_userview");

// Dynamic selection lists
ftelecare_userview.Lists["x_user_gender"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_userview.Lists["x_user_gender"].Options = <?php echo json_encode($telecare_user->user_gender->Options()) ?>;
ftelecare_userview.Lists["x_user_modules[]"] = {"LinkField":"x_invalidity_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_invalidity_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_userview.Lists["x_user_doctor_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_userview.Lists["x_user_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_user_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_userview.Lists["x_user_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":[],"ChildFields":["x_user_city_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_userview.Lists["x_user_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
<div class="ewToolbar">
<?php if ($telecare_user->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php $telecare_user_view->ExportOptions->Render("body") ?>
<?php
	foreach ($telecare_user_view->OtherOptions as &$option)
		$option->Render("body");
?>
<?php if ($telecare_user->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php $telecare_user_view->ShowPageHeader(); ?>
<?php
$telecare_user_view->ShowMessage();
?>
<?php if ($telecare_user->Export == "") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_user_view->Pager)) $telecare_user_view->Pager = new cPrevNextPager($telecare_user_view->StartRec, $telecare_user_view->DisplayRecs, $telecare_user_view->TotalRecs) ?>
<?php if ($telecare_user_view->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_user_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_user_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_user_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_user_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_user_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_user_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
</form>
<?php } ?>
<form name="ftelecare_userview" id="ftelecare_userview" class="form-inline ewForm ewViewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_user_view->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_user_view->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_user">
<?php if ($telecare_user->Export == "") { ?>
<div class="ewMultiPage">
<div class="tabbable" id="telecare_user_view">
	<ul class="nav<?php echo $telecare_user_view->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_user_view->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_user1" data-toggle="tab"><?php echo $telecare_user->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_user_view->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_user2" data-toggle="tab"><?php echo $telecare_user->PageCaption(2) ?></a></li>
		<li style="display: none"><a href="#tab_telecare_user3" data-toggle="tab"></a></li>
		<li<?php echo $telecare_user_view->MultiPages->TabStyle("4") ?>><a href="#tab_telecare_user4" data-toggle="tab"><?php echo $telecare_user->PageCaption(4) ?></a></li>
	</ul>
	<div class="tab-content">
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_user_view->MultiPages->PageStyle("1") ?>" id="tab_telecare_user1">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_user->user_id->Visible) { // user_id ?>
	<tr id="r_user_id">
		<td><span id="elh_telecare_user_user_id"><?php echo $telecare_user->user_id->FldCaption() ?></span></td>
		<td data-name="user_id"<?php echo $telecare_user->user_id->CellAttributes() ?>>
<span id="el_telecare_user_user_id" data-page="1">
<span<?php echo $telecare_user->user_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_surname->Visible) { // user_surname ?>
	<tr id="r_user_surname">
		<td><span id="elh_telecare_user_user_surname"><?php echo $telecare_user->user_surname->FldCaption() ?></span></td>
		<td data-name="user_surname"<?php echo $telecare_user->user_surname->CellAttributes() ?>>
<span id="el_telecare_user_user_surname" data-page="1">
<span<?php echo $telecare_user->user_surname->ViewAttributes() ?>>
<?php echo $telecare_user->user_surname->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_name->Visible) { // user_name ?>
	<tr id="r_user_name">
		<td><span id="elh_telecare_user_user_name"><?php echo $telecare_user->user_name->FldCaption() ?></span></td>
		<td data-name="user_name"<?php echo $telecare_user->user_name->CellAttributes() ?>>
<span id="el_telecare_user_user_name" data-page="1">
<span<?php echo $telecare_user->user_name->ViewAttributes() ?>>
<?php echo $telecare_user->user_name->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_gender->Visible) { // user_gender ?>
	<tr id="r_user_gender">
		<td><span id="elh_telecare_user_user_gender"><?php echo $telecare_user->user_gender->FldCaption() ?></span></td>
		<td data-name="user_gender"<?php echo $telecare_user->user_gender->CellAttributes() ?>>
<span id="el_telecare_user_user_gender" data-page="1">
<span<?php echo $telecare_user->user_gender->ViewAttributes() ?>>
<?php echo $telecare_user->user_gender->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_born->Visible) { // user_born ?>
	<tr id="r_user_born">
		<td><span id="elh_telecare_user_user_born"><?php echo $telecare_user->user_born->FldCaption() ?></span></td>
		<td data-name="user_born"<?php echo $telecare_user->user_born->CellAttributes() ?>>
<span id="el_telecare_user_user_born" data-page="1">
<span<?php echo $telecare_user->user_born->ViewAttributes() ?>>
<?php echo $telecare_user->user_born->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_modules->Visible) { // user_modules ?>
	<tr id="r_user_modules">
		<td><span id="elh_telecare_user_user_modules"><?php echo $telecare_user->user_modules->FldCaption() ?></span></td>
		<td data-name="user_modules"<?php echo $telecare_user->user_modules->CellAttributes() ?>>
<span id="el_telecare_user_user_modules" data-page="1">
<span<?php echo $telecare_user->user_modules->ViewAttributes() ?>>
<?php echo $telecare_user->user_modules->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_doctor_id->Visible) { // user_doctor_id ?>
	<tr id="r_user_doctor_id">
		<td><span id="elh_telecare_user_user_doctor_id"><?php echo $telecare_user->user_doctor_id->FldCaption() ?></span></td>
		<td data-name="user_doctor_id"<?php echo $telecare_user->user_doctor_id->CellAttributes() ?>>
<span id="el_telecare_user_user_doctor_id" data-page="1">
<span<?php echo $telecare_user->user_doctor_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_doctor_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_is_active->Visible) { // user_is_active ?>
	<tr id="r_user_is_active">
		<td><span id="elh_telecare_user_user_is_active"><?php echo $telecare_user->user_is_active->FldCaption() ?></span></td>
		<td data-name="user_is_active"<?php echo $telecare_user->user_is_active->CellAttributes() ?>>
<span id="el_telecare_user_user_is_active" data-page="1">
<span<?php echo $telecare_user->user_is_active->ViewAttributes() ?>>
<?php echo $telecare_user->user_is_active->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_medical_history->Visible) { // user_medical_history ?>
	<tr id="r_user_medical_history">
		<td><span id="elh_telecare_user_user_medical_history"><?php echo $telecare_user->user_medical_history->FldCaption() ?></span></td>
		<td data-name="user_medical_history"<?php echo $telecare_user->user_medical_history->CellAttributes() ?>>
<span id="el_telecare_user_user_medical_history" data-page="1">
<span<?php echo $telecare_user->user_medical_history->ViewAttributes() ?>>
<?php echo $telecare_user->user_medical_history->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_admin_id->Visible) { // user_admin_id ?>
	<tr id="r_user_admin_id">
		<td><span id="elh_telecare_user_user_admin_id"><?php echo $telecare_user->user_admin_id->FldCaption() ?></span></td>
		<td data-name="user_admin_id"<?php echo $telecare_user->user_admin_id->CellAttributes() ?>>
<span id="el_telecare_user_user_admin_id" data-page="1">
<span<?php echo $telecare_user->user_admin_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_admin_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_level->Visible) { // user_level ?>
	<tr id="r_user_level">
		<td><span id="elh_telecare_user_user_level"><?php echo $telecare_user->user_level->FldCaption() ?></span></td>
		<td data-name="user_level"<?php echo $telecare_user->user_level->CellAttributes() ?>>
<span id="el_telecare_user_user_level" data-page="1">
<span<?php echo $telecare_user->user_level->ViewAttributes() ?>>
<?php echo $telecare_user->user_level->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_user->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_user_view->MultiPages->PageStyle("2") ?>" id="tab_telecare_user2">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_user->user_address->Visible) { // user_address ?>
	<tr id="r_user_address">
		<td><span id="elh_telecare_user_user_address"><?php echo $telecare_user->user_address->FldCaption() ?></span></td>
		<td data-name="user_address"<?php echo $telecare_user->user_address->CellAttributes() ?>>
<span id="el_telecare_user_user_address" data-page="2">
<span<?php echo $telecare_user->user_address->ViewAttributes() ?>>
<?php echo $telecare_user->user_address->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_region_id->Visible) { // user_region_id ?>
	<tr id="r_user_region_id">
		<td><span id="elh_telecare_user_user_region_id"><?php echo $telecare_user->user_region_id->FldCaption() ?></span></td>
		<td data-name="user_region_id"<?php echo $telecare_user->user_region_id->CellAttributes() ?>>
<span id="el_telecare_user_user_region_id" data-page="2">
<span<?php echo $telecare_user->user_region_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_region_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_province_id->Visible) { // user_province_id ?>
	<tr id="r_user_province_id">
		<td><span id="elh_telecare_user_user_province_id"><?php echo $telecare_user->user_province_id->FldCaption() ?></span></td>
		<td data-name="user_province_id"<?php echo $telecare_user->user_province_id->CellAttributes() ?>>
<span id="el_telecare_user_user_province_id" data-page="2">
<span<?php echo $telecare_user->user_province_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_province_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_city_id->Visible) { // user_city_id ?>
	<tr id="r_user_city_id">
		<td><span id="elh_telecare_user_user_city_id"><?php echo $telecare_user->user_city_id->FldCaption() ?></span></td>
		<td data-name="user_city_id"<?php echo $telecare_user->user_city_id->CellAttributes() ?>>
<span id="el_telecare_user_user_city_id" data-page="2">
<span<?php echo $telecare_user->user_city_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_city_id->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_email->Visible) { // user_email ?>
	<tr id="r_user_email">
		<td><span id="elh_telecare_user_user_email"><?php echo $telecare_user->user_email->FldCaption() ?></span></td>
		<td data-name="user_email"<?php echo $telecare_user->user_email->CellAttributes() ?>>
<span id="el_telecare_user_user_email" data-page="2">
<span<?php echo $telecare_user->user_email->ViewAttributes() ?>>
<?php echo $telecare_user->user_email->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_phone->Visible) { // user_phone ?>
	<tr id="r_user_phone">
		<td><span id="elh_telecare_user_user_phone"><?php echo $telecare_user->user_phone->FldCaption() ?></span></td>
		<td data-name="user_phone"<?php echo $telecare_user->user_phone->CellAttributes() ?>>
<span id="el_telecare_user_user_phone" data-page="2">
<span<?php echo $telecare_user->user_phone->ViewAttributes() ?>>
<?php echo $telecare_user->user_phone->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_language->Visible) { // user_language ?>
	<tr id="r_user_language">
		<td><span id="elh_telecare_user_user_language"><?php echo $telecare_user->user_language->FldCaption() ?></span></td>
		<td data-name="user_language"<?php echo $telecare_user->user_language->CellAttributes() ?>>
<span id="el_telecare_user_user_language" data-page="2">
<span<?php echo $telecare_user->user_language->ViewAttributes() ?>>
<?php echo $telecare_user->user_language->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_user->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_user_view->MultiPages->PageStyle("3") ?>" id="tab_telecare_user3">
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
		<div class="tab-pane<?php echo $telecare_user_view->MultiPages->PageStyle("4") ?>" id="tab_telecare_user4">
<?php } ?>
<table class="table table-bordered table-striped ewViewTable">
<?php if ($telecare_user->user_latitude->Visible) { // user_latitude ?>
	<tr id="r_user_latitude">
		<td><span id="elh_telecare_user_user_latitude"><?php echo $telecare_user->user_latitude->FldCaption() ?></span></td>
		<td data-name="user_latitude"<?php echo $telecare_user->user_latitude->CellAttributes() ?>>
<span id="el_telecare_user_user_latitude" data-page="4">
<span<?php echo $telecare_user->user_latitude->ViewAttributes() ?>>
<?php echo $telecare_user->user_latitude->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_longitude->Visible) { // user_longitude ?>
	<tr id="r_user_longitude">
		<td><span id="elh_telecare_user_user_longitude"><?php echo $telecare_user->user_longitude->FldCaption() ?></span></td>
		<td data-name="user_longitude"<?php echo $telecare_user->user_longitude->CellAttributes() ?>>
<span id="el_telecare_user_user_longitude" data-page="4">
<span<?php echo $telecare_user->user_longitude->ViewAttributes() ?>>
<?php echo $telecare_user->user_longitude->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($telecare_user->user_last_update->Visible) { // user_last_update ?>
	<tr id="r_user_last_update">
		<td><span id="elh_telecare_user_user_last_update"><?php echo $telecare_user->user_last_update->FldCaption() ?></span></td>
		<td data-name="user_last_update"<?php echo $telecare_user->user_last_update->CellAttributes() ?>>
<span id="el_telecare_user_user_last_update" data-page="4">
<span<?php echo $telecare_user->user_last_update->ViewAttributes() ?>>
<?php echo $telecare_user->user_last_update->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
<?php if ($telecare_user->Export == "") { ?>
		</div>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
	</div>
</div>
</div>
<?php } ?>
<?php if ($telecare_user->Export == "") { ?>
<?php if (!isset($telecare_user_view->Pager)) $telecare_user_view->Pager = new cPrevNextPager($telecare_user_view->StartRec, $telecare_user_view->DisplayRecs, $telecare_user_view->TotalRecs) ?>
<?php if ($telecare_user_view->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_user_view->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_user_view->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_user_view->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_user_view->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_user_view->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_user_view->PageUrl() ?>start=<?php echo $telecare_user_view->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_user_view->Pager->PageCount ?></span>
</div>
<?php } ?>
<div class="clearfix"></div>
<?php } ?>
<?php
	if (in_array("telecare_call", explode(",", $telecare_user->getCurrentDetailTable())) && $telecare_call->DetailView) {
?>
<?php if ($telecare_user->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_call", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_callgrid.php" ?>
<?php } ?>
<?php
	if (in_array("telecare_alarm", explode(",", $telecare_user->getCurrentDetailTable())) && $telecare_alarm->DetailView) {
?>
<?php if ($telecare_user->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_alarm", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_alarmgrid.php" ?>
<?php } ?>
<?php
	if (in_array("telecare_prescription", explode(",", $telecare_user->getCurrentDetailTable())) && $telecare_prescription->DetailView) {
?>
<?php if ($telecare_user->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_prescription", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_prescriptiongrid.php" ?>
<?php } ?>
</form>
<script type="text/javascript">
ftelecare_userview.Init();
</script>
<?php
$telecare_user_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($telecare_user->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$telecare_user_view->Page_Terminate();
?>
