<?php

// user_id
// user_surname
// user_name
// user_gender
// user_born
// user_doctor_id
// user_city_id

?>
<?php if ($telecare_user->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $telecare_user->TableCaption() ?></h4> -->
<table id="tbl_telecare_usermaster" class="table table-bordered table-striped ewViewTable">
	<tbody>
<?php if ($telecare_user->user_id->Visible) { // user_id ?>
		<tr id="r_user_id">
			<td><?php echo $telecare_user->user_id->FldCaption() ?></td>
			<td<?php echo $telecare_user->user_id->CellAttributes() ?>>
<span id="el_telecare_user_user_id">
<span<?php echo $telecare_user->user_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_user->user_surname->Visible) { // user_surname ?>
		<tr id="r_user_surname">
			<td><?php echo $telecare_user->user_surname->FldCaption() ?></td>
			<td<?php echo $telecare_user->user_surname->CellAttributes() ?>>
<span id="el_telecare_user_user_surname">
<span<?php echo $telecare_user->user_surname->ViewAttributes() ?>>
<?php echo $telecare_user->user_surname->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_user->user_name->Visible) { // user_name ?>
		<tr id="r_user_name">
			<td><?php echo $telecare_user->user_name->FldCaption() ?></td>
			<td<?php echo $telecare_user->user_name->CellAttributes() ?>>
<span id="el_telecare_user_user_name">
<span<?php echo $telecare_user->user_name->ViewAttributes() ?>>
<?php echo $telecare_user->user_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_user->user_gender->Visible) { // user_gender ?>
		<tr id="r_user_gender">
			<td><?php echo $telecare_user->user_gender->FldCaption() ?></td>
			<td<?php echo $telecare_user->user_gender->CellAttributes() ?>>
<span id="el_telecare_user_user_gender">
<span<?php echo $telecare_user->user_gender->ViewAttributes() ?>>
<?php echo $telecare_user->user_gender->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_user->user_born->Visible) { // user_born ?>
		<tr id="r_user_born">
			<td><?php echo $telecare_user->user_born->FldCaption() ?></td>
			<td<?php echo $telecare_user->user_born->CellAttributes() ?>>
<span id="el_telecare_user_user_born">
<span<?php echo $telecare_user->user_born->ViewAttributes() ?>>
<?php echo $telecare_user->user_born->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_user->user_doctor_id->Visible) { // user_doctor_id ?>
		<tr id="r_user_doctor_id">
			<td><?php echo $telecare_user->user_doctor_id->FldCaption() ?></td>
			<td<?php echo $telecare_user->user_doctor_id->CellAttributes() ?>>
<span id="el_telecare_user_user_doctor_id">
<span<?php echo $telecare_user->user_doctor_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_doctor_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_user->user_city_id->Visible) { // user_city_id ?>
		<tr id="r_user_city_id">
			<td><?php echo $telecare_user->user_city_id->FldCaption() ?></td>
			<td<?php echo $telecare_user->user_city_id->CellAttributes() ?>>
<span id="el_telecare_user_user_city_id">
<span<?php echo $telecare_user->user_city_id->ViewAttributes() ?>>
<?php echo $telecare_user->user_city_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
