<?php

// Global variable for table object
$telecare_product = NULL;

//
// Table class for telecare_product
//
class ctelecare_product extends cTable {
	var $product_id;
	var $product_admin_id;
	var $product_type_id;
	var $product_user_id;
	var $product_name;
	var $product_installation_date;
	var $product_installation_account_id;
	var $product_attach_file;
	var $product_attach_type;
	var $product_attach_size;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'telecare_product';
		$this->TableName = 'telecare_product';
		$this->TableType = 'TABLE';

		// Update Table
		$this->UpdateTable = "`telecare_product`";
		$this->DBID = 'DB';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->ExportExcelPageOrientation = PHPExcel_Worksheet_PageSetup::ORIENTATION_DEFAULT; // Page orientation (PHPExcel only)
		$this->ExportExcelPageSize = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4; // Page size (PHPExcel only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// product_id
		$this->product_id = new cField('telecare_product', 'telecare_product', 'x_product_id', 'product_id', '`product_id`', '`product_id`', 3, -1, FALSE, '`product_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'NO');
		$this->product_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['product_id'] = &$this->product_id;

		// product_admin_id
		$this->product_admin_id = new cField('telecare_product', 'telecare_product', 'x_product_admin_id', 'product_admin_id', '`product_admin_id`', '`product_admin_id`', 3, -1, FALSE, '`product_admin_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->product_admin_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['product_admin_id'] = &$this->product_admin_id;

		// product_type_id
		$this->product_type_id = new cField('telecare_product', 'telecare_product', 'x_product_type_id', 'product_type_id', '`product_type_id`', '`product_type_id`', 3, -1, FALSE, '`product_type_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->product_type_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['product_type_id'] = &$this->product_type_id;

		// product_user_id
		$this->product_user_id = new cField('telecare_product', 'telecare_product', 'x_product_user_id', 'product_user_id', '`product_user_id`', '`product_user_id`', 3, -1, FALSE, '`product_user_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->product_user_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['product_user_id'] = &$this->product_user_id;

		// product_name
		$this->product_name = new cField('telecare_product', 'telecare_product', 'x_product_name', 'product_name', '`product_name`', '`product_name`', 200, -1, FALSE, '`product_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['product_name'] = &$this->product_name;

		// product_installation_date
		$this->product_installation_date = new cField('telecare_product', 'telecare_product', 'x_product_installation_date', 'product_installation_date', '`product_installation_date`', 'DATE_FORMAT(`product_installation_date`, \'%d/%m/%Y\')', 133, 7, FALSE, '`product_installation_date`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->product_installation_date->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['product_installation_date'] = &$this->product_installation_date;

		// product_installation_account_id
		$this->product_installation_account_id = new cField('telecare_product', 'telecare_product', 'x_product_installation_account_id', 'product_installation_account_id', '`product_installation_account_id`', '`product_installation_account_id`', 3, -1, FALSE, '`product_installation_account_id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'SELECT');
		$this->product_installation_account_id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['product_installation_account_id'] = &$this->product_installation_account_id;

		// product_attach_file
		$this->product_attach_file = new cField('telecare_product', 'telecare_product', 'x_product_attach_file', 'product_attach_file', '`product_attach_file`', '`product_attach_file`', 200, -1, TRUE, '`product_attach_file`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'FILE');
		$this->product_attach_file->UploadMultiple = TRUE;
		$this->product_attach_file->Upload->UploadMultiple = TRUE;
		$this->fields['product_attach_file'] = &$this->product_attach_file;

		// product_attach_type
		$this->product_attach_type = new cField('telecare_product', 'telecare_product', 'x_product_attach_type', 'product_attach_type', '`product_attach_type`', '`product_attach_type`', 200, -1, FALSE, '`product_attach_type`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->fields['product_attach_type'] = &$this->product_attach_type;

		// product_attach_size
		$this->product_attach_size = new cField('telecare_product', 'telecare_product', 'x_product_attach_size', 'product_attach_size', '`product_attach_size`', '`product_attach_size`', 3, -1, FALSE, '`product_attach_size`', FALSE, FALSE, FALSE, 'FORMATTED TEXT', 'TEXT');
		$this->product_attach_size->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['product_attach_size'] = &$this->product_attach_size;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	var $_SqlFrom = "";

	function getSqlFrom() { // From
		return ($this->_SqlFrom <> "") ? $this->_SqlFrom : "`telecare_product`";
	}

	function SqlFrom() { // For backward compatibility
    	return $this->getSqlFrom();
	}

	function setSqlFrom($v) {
    	$this->_SqlFrom = $v;
	}
	var $_SqlSelect = "";

	function getSqlSelect() { // Select
		return ($this->_SqlSelect <> "") ? $this->_SqlSelect : "SELECT * FROM " . $this->getSqlFrom();
	}

	function SqlSelect() { // For backward compatibility
    	return $this->getSqlSelect();
	}

	function setSqlSelect($v) {
    	$this->_SqlSelect = $v;
	}
	var $_SqlWhere = "";

	function getSqlWhere() { // Where
		$sWhere = ($this->_SqlWhere <> "") ? $this->_SqlWhere : "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlWhere() { // For backward compatibility
    	return $this->getSqlWhere();
	}

	function setSqlWhere($v) {
    	$this->_SqlWhere = $v;
	}
	var $_SqlGroupBy = "";

	function getSqlGroupBy() { // Group By
		return ($this->_SqlGroupBy <> "") ? $this->_SqlGroupBy : "";
	}

	function SqlGroupBy() { // For backward compatibility
    	return $this->getSqlGroupBy();
	}

	function setSqlGroupBy($v) {
    	$this->_SqlGroupBy = $v;
	}
	var $_SqlHaving = "";

	function getSqlHaving() { // Having
		return ($this->_SqlHaving <> "") ? $this->_SqlHaving : "";
	}

	function SqlHaving() { // For backward compatibility
    	return $this->getSqlHaving();
	}

	function setSqlHaving($v) {
    	$this->_SqlHaving = $v;
	}
	var $_SqlOrderBy = "";

	function getSqlOrderBy() { // Order By
		return ($this->_SqlOrderBy <> "") ? $this->_SqlOrderBy : "";
	}

	function SqlOrderBy() { // For backward compatibility
    	return $this->getSqlOrderBy();
	}

	function setSqlOrderBy($v) {
    	$this->_SqlOrderBy = $v;
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(),
			$this->getSqlGroupBy(), $this->getSqlHaving(), $this->getSqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$this->Recordset_Selecting($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->getSqlSelect(), $this->getSqlWhere(), $this->getSqlGroupBy(),
			$this->getSqlHaving(), $this->getSqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->getSqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		$cnt = -1;
		if (($this->TableType == 'TABLE' || $this->TableType == 'VIEW' || $this->TableType == 'LINKTABLE') && preg_match("/^SELECT \* FROM/i", $sSql)) {
			$sSql = "SELECT COUNT(*) FROM" . preg_replace('/^SELECT\s([\s\S]+)?\*\sFROM/i', "", $sSql);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		$conn = &$this->Connection();
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			$conn = &$this->Connection();
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// INSERT statement
	function InsertSQL(&$rs) {
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		$conn = &$this->Connection();
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]) || $this->fields[$name]->FldIsCustom)
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType, $this->DBID) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL, $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->UpdateSQL($rs, $where, $curfilter));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "", $curfilter = TRUE) {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if (is_array($where))
			$where = $this->ArrayToFilter($where);
		if ($rs) {
			if (array_key_exists('product_id', $rs))
				ew_AddFilter($where, ew_QuotedName('product_id', $this->DBID) . '=' . ew_QuotedValue($rs['product_id'], $this->product_id->FldDataType, $this->DBID));
		}
		$filter = ($curfilter) ? $this->CurrentFilter : "";
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "", $curfilter = TRUE) {
		$conn = &$this->Connection();
		return $conn->Execute($this->DeleteSQL($rs, $where, $curfilter));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`product_id` = @product_id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->product_id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@product_id@", ew_AdjustSql($this->product_id->CurrentValue, $this->DBID), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "telecare_productlist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "telecare_productlist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			$url = $this->KeyUrl("telecare_productview.php", $this->UrlParm($parm));
		else
			$url = $this->KeyUrl("telecare_productview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
		return $this->AddMasterUrl($url);
	}

	// Add URL
	function GetAddUrl($parm = "") {
		if ($parm <> "")
			$url = "telecare_productadd.php?" . $this->UrlParm($parm);
		else
			$url = "telecare_productadd.php";
		return $this->AddMasterUrl($url);
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		$url = $this->KeyUrl("telecare_productedit.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
		return $this->AddMasterUrl($url);
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		$url = $this->KeyUrl("telecare_productadd.php", $this->UrlParm($parm));
		return $this->AddMasterUrl($url);
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		$url = $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
		return $this->AddMasterUrl($url);
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("telecare_productdelete.php", $this->UrlParm());
	}

	// Add master url
	function AddMasterUrl($url) {
		return $url;
	}

	function KeyToJson() {
		$json = "";
		$json .= "product_id:" . ew_VarToJson($this->product_id->CurrentValue, "number", "'");
		return "{" . $json . "}";
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->product_id->CurrentValue)) {
			$sUrl .= "product_id=" . urlencode($this->product_id->CurrentValue);
		} else {
			return "javascript:ew_Alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&amp;ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (!empty($_GET) || !empty($_POST)) {
			$isPost = ew_IsHttpPost();
			$arKeys[] = $isPost ? ew_StripSlashes(@$_POST["product_id"]) : ew_StripSlashes(@$_GET["product_id"]); // product_id

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->product_id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$conn = &$this->Connection();
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->product_id->setDbValue($rs->fields('product_id'));
		$this->product_admin_id->setDbValue($rs->fields('product_admin_id'));
		$this->product_type_id->setDbValue($rs->fields('product_type_id'));
		$this->product_user_id->setDbValue($rs->fields('product_user_id'));
		$this->product_name->setDbValue($rs->fields('product_name'));
		$this->product_installation_date->setDbValue($rs->fields('product_installation_date'));
		$this->product_installation_account_id->setDbValue($rs->fields('product_installation_account_id'));
		$this->product_attach_file->Upload->DbValue = $rs->fields('product_attach_file');
		$this->product_attach_type->setDbValue($rs->fields('product_attach_type'));
		$this->product_attach_size->setDbValue($rs->fields('product_attach_size'));
	}

	// Render list row values
	function RenderListRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// product_id
		// product_admin_id
		// product_type_id
		// product_user_id
		// product_name
		// product_installation_date
		// product_installation_account_id
		// product_attach_file
		// product_attach_type

		$this->product_attach_type->CellCssStyle = "white-space: nowrap;";

		// product_attach_size
		$this->product_attach_size->CellCssStyle = "white-space: nowrap;";

		// product_id
		$this->product_id->ViewValue = $this->product_id->CurrentValue;
		$this->product_id->ViewCustomAttributes = "";

		// product_admin_id
		$this->product_admin_id->ViewValue = $this->product_admin_id->CurrentValue;
		$this->product_admin_id->ViewCustomAttributes = "";

		// product_type_id
		if (strval($this->product_type_id->CurrentValue) <> "") {
			$sFilterWrk = "`product_type_id`" . ew_SearchString("=", $this->product_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_type_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `product_type_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->product_type_id->ViewValue = $this->product_type_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_type_id->ViewValue = $this->product_type_id->CurrentValue;
			}
		} else {
			$this->product_type_id->ViewValue = NULL;
		}
		$this->product_type_id->ViewCustomAttributes = "";

		// product_user_id
		if (strval($this->product_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->product_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->product_user_id->ViewValue = $this->product_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_user_id->ViewValue = $this->product_user_id->CurrentValue;
			}
		} else {
			$this->product_user_id->ViewValue = NULL;
		}
		$this->product_user_id->ViewCustomAttributes = "";

		// product_name
		$this->product_name->ViewValue = $this->product_name->CurrentValue;
		$this->product_name->ViewCustomAttributes = "";

		// product_installation_date
		$this->product_installation_date->ViewValue = $this->product_installation_date->CurrentValue;
		$this->product_installation_date->ViewValue = ew_FormatDateTime($this->product_installation_date->ViewValue, 7);
		$this->product_installation_date->ViewCustomAttributes = "";

		// product_installation_account_id
		if (strval($this->product_installation_account_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->product_installation_account_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = "`admin_level` = 4";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_installation_account_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->product_installation_account_id->ViewValue = $this->product_installation_account_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_installation_account_id->ViewValue = $this->product_installation_account_id->CurrentValue;
			}
		} else {
			$this->product_installation_account_id->ViewValue = NULL;
		}
		$this->product_installation_account_id->ViewCustomAttributes = "";

		// product_attach_file
		if (!ew_Empty($this->product_attach_file->Upload->DbValue)) {
			$this->product_attach_file->ViewValue = $this->product_attach_file->Upload->DbValue;
		} else {
			$this->product_attach_file->ViewValue = "";
		}
		$this->product_attach_file->ViewCustomAttributes = "";

		// product_attach_type
		$this->product_attach_type->ViewValue = $this->product_attach_type->CurrentValue;
		$this->product_attach_type->ViewCustomAttributes = "";

		// product_attach_size
		$this->product_attach_size->ViewValue = $this->product_attach_size->CurrentValue;
		$this->product_attach_size->ViewCustomAttributes = "";

		// product_id
		$this->product_id->LinkCustomAttributes = "";
		$this->product_id->HrefValue = "";
		$this->product_id->TooltipValue = "";

		// product_admin_id
		$this->product_admin_id->LinkCustomAttributes = "";
		$this->product_admin_id->HrefValue = "";
		$this->product_admin_id->TooltipValue = "";

		// product_type_id
		$this->product_type_id->LinkCustomAttributes = "";
		$this->product_type_id->HrefValue = "";
		$this->product_type_id->TooltipValue = "";

		// product_user_id
		$this->product_user_id->LinkCustomAttributes = "";
		$this->product_user_id->HrefValue = "";
		$this->product_user_id->TooltipValue = "";

		// product_name
		$this->product_name->LinkCustomAttributes = "";
		$this->product_name->HrefValue = "";
		$this->product_name->TooltipValue = "";

		// product_installation_date
		$this->product_installation_date->LinkCustomAttributes = "";
		$this->product_installation_date->HrefValue = "";
		$this->product_installation_date->TooltipValue = "";

		// product_installation_account_id
		$this->product_installation_account_id->LinkCustomAttributes = "";
		$this->product_installation_account_id->HrefValue = "";
		$this->product_installation_account_id->TooltipValue = "";

		// product_attach_file
		$this->product_attach_file->LinkCustomAttributes = "";
		$this->product_attach_file->HrefValue = "";
		$this->product_attach_file->HrefValue2 = $this->product_attach_file->UploadPath . $this->product_attach_file->Upload->DbValue;
		$this->product_attach_file->TooltipValue = "";

		// product_attach_type
		$this->product_attach_type->LinkCustomAttributes = "";
		$this->product_attach_type->HrefValue = "";
		$this->product_attach_type->TooltipValue = "";

		// product_attach_size
		$this->product_attach_size->LinkCustomAttributes = "";
		$this->product_attach_size->HrefValue = "";
		$this->product_attach_size->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Render edit row values
	function RenderEditRow() {
		global $Security, $gsLanguage, $Language;

		// Call Row Rendering event
		$this->Row_Rendering();

		// product_id
		$this->product_id->EditAttrs["class"] = "form-control";
		$this->product_id->EditCustomAttributes = "";
		$this->product_id->EditValue = $this->product_id->CurrentValue;
		$this->product_id->ViewCustomAttributes = "";

		// product_admin_id
		// product_type_id

		$this->product_type_id->EditAttrs["class"] = "form-control";
		$this->product_type_id->EditCustomAttributes = "";

		// product_user_id
		$this->product_user_id->EditAttrs["class"] = "form-control";
		$this->product_user_id->EditCustomAttributes = "";

		// product_name
		$this->product_name->EditAttrs["class"] = "form-control";
		$this->product_name->EditCustomAttributes = "";
		$this->product_name->EditValue = $this->product_name->CurrentValue;
		$this->product_name->PlaceHolder = ew_RemoveHtml($this->product_name->FldCaption());

		// product_installation_date
		$this->product_installation_date->EditAttrs["class"] = "form-control";
		$this->product_installation_date->EditCustomAttributes = "";
		$this->product_installation_date->EditValue = ew_FormatDateTime($this->product_installation_date->CurrentValue, 7);
		$this->product_installation_date->PlaceHolder = ew_RemoveHtml($this->product_installation_date->FldCaption());

		// product_installation_account_id
		$this->product_installation_account_id->EditAttrs["class"] = "form-control";
		$this->product_installation_account_id->EditCustomAttributes = "";

		// product_attach_file
		$this->product_attach_file->EditAttrs["class"] = "form-control";
		$this->product_attach_file->EditCustomAttributes = "";
		if (!ew_Empty($this->product_attach_file->Upload->DbValue)) {
			$this->product_attach_file->EditValue = $this->product_attach_file->Upload->DbValue;
		} else {
			$this->product_attach_file->EditValue = "";
		}
		if (!ew_Empty($this->product_attach_file->CurrentValue))
			$this->product_attach_file->Upload->FileName = $this->product_attach_file->CurrentValue;

		// product_attach_type
		$this->product_attach_type->EditAttrs["class"] = "form-control";
		$this->product_attach_type->EditCustomAttributes = "";
		$this->product_attach_type->EditValue = $this->product_attach_type->CurrentValue;
		$this->product_attach_type->PlaceHolder = ew_RemoveHtml($this->product_attach_type->FldCaption());

		// product_attach_size
		$this->product_attach_size->EditAttrs["class"] = "form-control";
		$this->product_attach_size->EditCustomAttributes = "";
		$this->product_attach_size->EditValue = $this->product_attach_size->CurrentValue;
		$this->product_attach_size->PlaceHolder = ew_RemoveHtml($this->product_attach_size->FldCaption());

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {

		// Call Row Rendered event
		$this->Row_Rendered();
	}
	var $ExportDoc;

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;
		if (!$Doc->ExportCustom) {

			// Write header
			$Doc->ExportTableHeader();
			if ($Doc->Horizontal) { // Horizontal format, write header
				$Doc->BeginExportRow();
				if ($ExportPageType == "view") {
					if ($this->product_id->Exportable) $Doc->ExportCaption($this->product_id);
					if ($this->product_admin_id->Exportable) $Doc->ExportCaption($this->product_admin_id);
					if ($this->product_type_id->Exportable) $Doc->ExportCaption($this->product_type_id);
					if ($this->product_user_id->Exportable) $Doc->ExportCaption($this->product_user_id);
					if ($this->product_name->Exportable) $Doc->ExportCaption($this->product_name);
					if ($this->product_installation_date->Exportable) $Doc->ExportCaption($this->product_installation_date);
					if ($this->product_installation_account_id->Exportable) $Doc->ExportCaption($this->product_installation_account_id);
					if ($this->product_attach_file->Exportable) $Doc->ExportCaption($this->product_attach_file);
				} else {
					if ($this->product_id->Exportable) $Doc->ExportCaption($this->product_id);
					if ($this->product_admin_id->Exportable) $Doc->ExportCaption($this->product_admin_id);
					if ($this->product_type_id->Exportable) $Doc->ExportCaption($this->product_type_id);
					if ($this->product_user_id->Exportable) $Doc->ExportCaption($this->product_user_id);
					if ($this->product_name->Exportable) $Doc->ExportCaption($this->product_name);
					if ($this->product_installation_date->Exportable) $Doc->ExportCaption($this->product_installation_date);
					if ($this->product_installation_account_id->Exportable) $Doc->ExportCaption($this->product_installation_account_id);
					if ($this->product_attach_file->Exportable) $Doc->ExportCaption($this->product_attach_file);
				}
				$Doc->EndExportRow();
			}
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				if (!$Doc->ExportCustom) {
					$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
					if ($ExportPageType == "view") {
						if ($this->product_id->Exportable) $Doc->ExportField($this->product_id);
						if ($this->product_admin_id->Exportable) $Doc->ExportField($this->product_admin_id);
						if ($this->product_type_id->Exportable) $Doc->ExportField($this->product_type_id);
						if ($this->product_user_id->Exportable) $Doc->ExportField($this->product_user_id);
						if ($this->product_name->Exportable) $Doc->ExportField($this->product_name);
						if ($this->product_installation_date->Exportable) $Doc->ExportField($this->product_installation_date);
						if ($this->product_installation_account_id->Exportable) $Doc->ExportField($this->product_installation_account_id);
						if ($this->product_attach_file->Exportable) $Doc->ExportField($this->product_attach_file);
					} else {
						if ($this->product_id->Exportable) $Doc->ExportField($this->product_id);
						if ($this->product_admin_id->Exportable) $Doc->ExportField($this->product_admin_id);
						if ($this->product_type_id->Exportable) $Doc->ExportField($this->product_type_id);
						if ($this->product_user_id->Exportable) $Doc->ExportField($this->product_user_id);
						if ($this->product_name->Exportable) $Doc->ExportField($this->product_name);
						if ($this->product_installation_date->Exportable) $Doc->ExportField($this->product_installation_date);
						if ($this->product_installation_account_id->Exportable) $Doc->ExportField($this->product_installation_account_id);
						if ($this->product_attach_file->Exportable) $Doc->ExportField($this->product_attach_file);
					}
					$Doc->EndExportRow();
				}
			}

			// Call Row Export server event
			if ($Doc->ExportCustom)
				$this->Row_Export($Recordset->fields);
			$Recordset->MoveNext();
		}
		if (!$Doc->ExportCustom) {
			$Doc->ExportTableFooter();
		}
	}

	// Get auto fill value
	function GetAutoFill($id, $val) {
		$rsarr = array();
		$rowcnt = 0;

		// Output
		if (is_array($rsarr) && $rowcnt > 0) {
			$fldcnt = count($rsarr[0]);
			for ($i = 0; $i < $rowcnt; $i++) {
				for ($j = 0; $j < $fldcnt; $j++) {
					$str = strval($rsarr[$i][$j]);
					$str = ew_ConvertToUtf8($str);
					if (isset($post["keepCRLF"])) {
						$str = str_replace(array("\r", "\n"), array("\\r", "\\n"), $str);
					} else {
						$str = str_replace(array("\r", "\n"), array(" ", " "), $str);
					}
					$rsarr[$i][$j] = $str;
				}
			}
			return ew_ArrayToJson($rsarr);
		} else {
			return FALSE;
		}
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Grid Inserting event
	function Grid_Inserting() {

		// Enter your code here
		// To reject grid insert, set return value to FALSE

		return TRUE;
	}

	// Grid Inserted event
	function Grid_Inserted($rsnew) {

		//echo "Grid Inserted";
	}

	// Grid Updating event
	function Grid_Updating($rsold) {

		// Enter your code here
		// To reject grid update, set return value to FALSE

		return TRUE;
	}

	// Grid Updated event
	function Grid_Updated($rsold, $rsnew) {

		//echo "Grid Updated";
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		//var_dump($fld->FldName, $fld->LookupFilters, $filter); // Uncomment to view the filter
		// Enter your code here

	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
