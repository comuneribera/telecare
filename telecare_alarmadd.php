<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_alarminfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_alarm_add = NULL; // Initialize page object first

class ctelecare_alarm_add extends ctelecare_alarm {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_alarm';

	// Page object name
	var $PageObjName = 'telecare_alarm_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_alarm)
		if (!isset($GLOBALS["telecare_alarm"]) || get_class($GLOBALS["telecare_alarm"]) == "ctelecare_alarm") {
			$GLOBALS["telecare_alarm"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_alarm"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_user)
		if (!isset($GLOBALS['telecare_user'])) $GLOBALS['telecare_user'] = new ctelecare_user();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_alarm', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_alarmlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_alarm;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_alarm);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["alarm_id"] != "") {
				$this->alarm_id->setQueryStringValue($_GET["alarm_id"]);
				$this->setKey("alarm_id", $this->alarm_id->CurrentValue); // Set up key
			} else {
				$this->setKey("alarm_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_alarmlist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_alarmview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->alarm_type_id->CurrentValue = NULL;
		$this->alarm_type_id->OldValue = $this->alarm_type_id->CurrentValue;
		$this->alarm_user_id->CurrentValue = NULL;
		$this->alarm_user_id->OldValue = $this->alarm_user_id->CurrentValue;
		$this->alarm_product_id->CurrentValue = NULL;
		$this->alarm_product_id->OldValue = $this->alarm_product_id->CurrentValue;
		$this->alarm_datetime->CurrentValue = NULL;
		$this->alarm_datetime->OldValue = $this->alarm_datetime->CurrentValue;
		$this->alarm_start_on->CurrentValue = NULL;
		$this->alarm_start_on->OldValue = $this->alarm_start_on->CurrentValue;
		$this->alarm_close_on->CurrentValue = NULL;
		$this->alarm_close_on->OldValue = $this->alarm_close_on->CurrentValue;
		$this->alarm_close->CurrentValue = NULL;
		$this->alarm_close->OldValue = $this->alarm_close->CurrentValue;
		$this->alarm_admin_id->CurrentValue = NULL;
		$this->alarm_admin_id->OldValue = $this->alarm_admin_id->CurrentValue;
		$this->alarm_call_parents->CurrentValue = NULL;
		$this->alarm_call_parents->OldValue = $this->alarm_call_parents->CurrentValue;
		$this->alarm_call_emergency->CurrentValue = NULL;
		$this->alarm_call_emergency->OldValue = $this->alarm_call_emergency->CurrentValue;
		$this->alarm_note->CurrentValue = NULL;
		$this->alarm_note->OldValue = $this->alarm_note->CurrentValue;
		$this->alarm_last_update->CurrentValue = NULL;
		$this->alarm_last_update->OldValue = $this->alarm_last_update->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->alarm_type_id->FldIsDetailKey) {
			$this->alarm_type_id->setFormValue($objForm->GetValue("x_alarm_type_id"));
		}
		if (!$this->alarm_user_id->FldIsDetailKey) {
			$this->alarm_user_id->setFormValue($objForm->GetValue("x_alarm_user_id"));
		}
		if (!$this->alarm_product_id->FldIsDetailKey) {
			$this->alarm_product_id->setFormValue($objForm->GetValue("x_alarm_product_id"));
		}
		if (!$this->alarm_datetime->FldIsDetailKey) {
			$this->alarm_datetime->setFormValue($objForm->GetValue("x_alarm_datetime"));
			$this->alarm_datetime->CurrentValue = ew_UnFormatDateTime($this->alarm_datetime->CurrentValue, 11);
		}
		if (!$this->alarm_start_on->FldIsDetailKey) {
			$this->alarm_start_on->setFormValue($objForm->GetValue("x_alarm_start_on"));
			$this->alarm_start_on->CurrentValue = ew_UnFormatDateTime($this->alarm_start_on->CurrentValue, 11);
		}
		if (!$this->alarm_close_on->FldIsDetailKey) {
			$this->alarm_close_on->setFormValue($objForm->GetValue("x_alarm_close_on"));
			$this->alarm_close_on->CurrentValue = ew_UnFormatDateTime($this->alarm_close_on->CurrentValue, 11);
		}
		if (!$this->alarm_close->FldIsDetailKey) {
			$this->alarm_close->setFormValue($objForm->GetValue("x_alarm_close"));
		}
		if (!$this->alarm_admin_id->FldIsDetailKey) {
			$this->alarm_admin_id->setFormValue($objForm->GetValue("x_alarm_admin_id"));
		}
		if (!$this->alarm_call_parents->FldIsDetailKey) {
			$this->alarm_call_parents->setFormValue($objForm->GetValue("x_alarm_call_parents"));
		}
		if (!$this->alarm_call_emergency->FldIsDetailKey) {
			$this->alarm_call_emergency->setFormValue($objForm->GetValue("x_alarm_call_emergency"));
		}
		if (!$this->alarm_note->FldIsDetailKey) {
			$this->alarm_note->setFormValue($objForm->GetValue("x_alarm_note"));
		}
		if (!$this->alarm_last_update->FldIsDetailKey) {
			$this->alarm_last_update->setFormValue($objForm->GetValue("x_alarm_last_update"));
			$this->alarm_last_update->CurrentValue = ew_UnFormatDateTime($this->alarm_last_update->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->alarm_type_id->CurrentValue = $this->alarm_type_id->FormValue;
		$this->alarm_user_id->CurrentValue = $this->alarm_user_id->FormValue;
		$this->alarm_product_id->CurrentValue = $this->alarm_product_id->FormValue;
		$this->alarm_datetime->CurrentValue = $this->alarm_datetime->FormValue;
		$this->alarm_datetime->CurrentValue = ew_UnFormatDateTime($this->alarm_datetime->CurrentValue, 11);
		$this->alarm_start_on->CurrentValue = $this->alarm_start_on->FormValue;
		$this->alarm_start_on->CurrentValue = ew_UnFormatDateTime($this->alarm_start_on->CurrentValue, 11);
		$this->alarm_close_on->CurrentValue = $this->alarm_close_on->FormValue;
		$this->alarm_close_on->CurrentValue = ew_UnFormatDateTime($this->alarm_close_on->CurrentValue, 11);
		$this->alarm_close->CurrentValue = $this->alarm_close->FormValue;
		$this->alarm_admin_id->CurrentValue = $this->alarm_admin_id->FormValue;
		$this->alarm_call_parents->CurrentValue = $this->alarm_call_parents->FormValue;
		$this->alarm_call_emergency->CurrentValue = $this->alarm_call_emergency->FormValue;
		$this->alarm_note->CurrentValue = $this->alarm_note->FormValue;
		$this->alarm_last_update->CurrentValue = $this->alarm_last_update->FormValue;
		$this->alarm_last_update->CurrentValue = ew_UnFormatDateTime($this->alarm_last_update->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->alarm_id->setDbValue($rs->fields('alarm_id'));
		$this->alarm_type_id->setDbValue($rs->fields('alarm_type_id'));
		$this->alarm_user_id->setDbValue($rs->fields('alarm_user_id'));
		$this->alarm_product_id->setDbValue($rs->fields('alarm_product_id'));
		$this->alarm_datetime->setDbValue($rs->fields('alarm_datetime'));
		$this->alarm_latitude->setDbValue($rs->fields('alarm_latitude'));
		$this->alarm_longitude->setDbValue($rs->fields('alarm_longitude'));
		$this->alarm_start_on->setDbValue($rs->fields('alarm_start_on'));
		$this->alarm_close_on->setDbValue($rs->fields('alarm_close_on'));
		$this->alarm_close->setDbValue($rs->fields('alarm_close'));
		$this->alarm_admin_id->setDbValue($rs->fields('alarm_admin_id'));
		$this->alarm_call_parents->setDbValue($rs->fields('alarm_call_parents'));
		$this->alarm_call_emergency->setDbValue($rs->fields('alarm_call_emergency'));
		$this->alarm_note->setDbValue($rs->fields('alarm_note'));
		$this->alarm_last_update->setDbValue($rs->fields('alarm_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->alarm_id->DbValue = $row['alarm_id'];
		$this->alarm_type_id->DbValue = $row['alarm_type_id'];
		$this->alarm_user_id->DbValue = $row['alarm_user_id'];
		$this->alarm_product_id->DbValue = $row['alarm_product_id'];
		$this->alarm_datetime->DbValue = $row['alarm_datetime'];
		$this->alarm_latitude->DbValue = $row['alarm_latitude'];
		$this->alarm_longitude->DbValue = $row['alarm_longitude'];
		$this->alarm_start_on->DbValue = $row['alarm_start_on'];
		$this->alarm_close_on->DbValue = $row['alarm_close_on'];
		$this->alarm_close->DbValue = $row['alarm_close'];
		$this->alarm_admin_id->DbValue = $row['alarm_admin_id'];
		$this->alarm_call_parents->DbValue = $row['alarm_call_parents'];
		$this->alarm_call_emergency->DbValue = $row['alarm_call_emergency'];
		$this->alarm_note->DbValue = $row['alarm_note'];
		$this->alarm_last_update->DbValue = $row['alarm_last_update'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("alarm_id")) <> "")
			$this->alarm_id->CurrentValue = $this->getKey("alarm_id"); // alarm_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// alarm_id
		// alarm_type_id
		// alarm_user_id
		// alarm_product_id
		// alarm_datetime
		// alarm_latitude
		// alarm_longitude
		// alarm_start_on
		// alarm_close_on
		// alarm_close
		// alarm_admin_id
		// alarm_call_parents
		// alarm_call_emergency
		// alarm_note
		// alarm_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// alarm_id
		$this->alarm_id->ViewValue = $this->alarm_id->CurrentValue;
		$this->alarm_id->ViewCustomAttributes = "";

		// alarm_type_id
		if (strval($this->alarm_type_id->CurrentValue) <> "") {
			$sFilterWrk = "`alarm_type_id`" . ew_SearchString("=", $this->alarm_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_type_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_type_id->ViewValue = $this->alarm_type_id->CurrentValue;
			}
		} else {
			$this->alarm_type_id->ViewValue = NULL;
		}
		$this->alarm_type_id->ViewCustomAttributes = "";

		// alarm_user_id
		$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
		if (strval($this->alarm_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->alarm_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
			}
		} else {
			$this->alarm_user_id->ViewValue = NULL;
		}
		$this->alarm_user_id->ViewCustomAttributes = "";

		// alarm_product_id
		if (strval($this->alarm_product_id->CurrentValue) <> "") {
			$sFilterWrk = "telecare_product.product_id" . ew_SearchString("=", $this->alarm_product_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_product_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_product_id->ViewValue = $this->alarm_product_id->CurrentValue;
			}
		} else {
			$this->alarm_product_id->ViewValue = NULL;
		}
		$this->alarm_product_id->ViewCustomAttributes = "";

		// alarm_datetime
		$this->alarm_datetime->ViewValue = $this->alarm_datetime->CurrentValue;
		$this->alarm_datetime->ViewValue = ew_FormatDateTime($this->alarm_datetime->ViewValue, 11);
		$this->alarm_datetime->ViewCustomAttributes = "";

		// alarm_latitude
		$this->alarm_latitude->ViewValue = $this->alarm_latitude->CurrentValue;
		$this->alarm_latitude->ViewCustomAttributes = "";

		// alarm_longitude
		$this->alarm_longitude->ViewValue = $this->alarm_longitude->CurrentValue;
		$this->alarm_longitude->ViewCustomAttributes = "";

		// alarm_start_on
		$this->alarm_start_on->ViewValue = $this->alarm_start_on->CurrentValue;
		$this->alarm_start_on->ViewValue = ew_FormatDateTime($this->alarm_start_on->ViewValue, 11);
		$this->alarm_start_on->ViewCustomAttributes = "";

		// alarm_close_on
		$this->alarm_close_on->ViewValue = $this->alarm_close_on->CurrentValue;
		$this->alarm_close_on->ViewValue = ew_FormatDateTime($this->alarm_close_on->ViewValue, 11);
		$this->alarm_close_on->ViewCustomAttributes = "";

		// alarm_close
		if (strval($this->alarm_close->CurrentValue) <> "") {
			$this->alarm_close->ViewValue = $this->alarm_close->OptionCaption($this->alarm_close->CurrentValue);
		} else {
			$this->alarm_close->ViewValue = NULL;
		}
		$this->alarm_close->ViewCustomAttributes = "";

		// alarm_admin_id
		if (strval($this->alarm_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->alarm_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->alarm_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->alarm_admin_id->ViewValue = $this->alarm_admin_id->CurrentValue;
			}
		} else {
			$this->alarm_admin_id->ViewValue = NULL;
		}
		$this->alarm_admin_id->ViewCustomAttributes = "";

		// alarm_call_parents
		if (strval($this->alarm_call_parents->CurrentValue) <> "") {
			$this->alarm_call_parents->ViewValue = $this->alarm_call_parents->OptionCaption($this->alarm_call_parents->CurrentValue);
		} else {
			$this->alarm_call_parents->ViewValue = NULL;
		}
		$this->alarm_call_parents->ViewCustomAttributes = "";

		// alarm_call_emergency
		if (strval($this->alarm_call_emergency->CurrentValue) <> "") {
			$this->alarm_call_emergency->ViewValue = $this->alarm_call_emergency->OptionCaption($this->alarm_call_emergency->CurrentValue);
		} else {
			$this->alarm_call_emergency->ViewValue = NULL;
		}
		$this->alarm_call_emergency->ViewCustomAttributes = "";

		// alarm_note
		$this->alarm_note->ViewValue = $this->alarm_note->CurrentValue;
		$this->alarm_note->ViewCustomAttributes = "";

		// alarm_last_update
		$this->alarm_last_update->ViewValue = $this->alarm_last_update->CurrentValue;
		$this->alarm_last_update->ViewValue = ew_FormatDateTime($this->alarm_last_update->ViewValue, 7);
		$this->alarm_last_update->ViewCustomAttributes = "";

			// alarm_type_id
			$this->alarm_type_id->LinkCustomAttributes = "";
			$this->alarm_type_id->HrefValue = "";
			$this->alarm_type_id->TooltipValue = "";

			// alarm_user_id
			$this->alarm_user_id->LinkCustomAttributes = "";
			$this->alarm_user_id->HrefValue = "";
			$this->alarm_user_id->TooltipValue = "";

			// alarm_product_id
			$this->alarm_product_id->LinkCustomAttributes = "";
			$this->alarm_product_id->HrefValue = "";
			$this->alarm_product_id->TooltipValue = "";

			// alarm_datetime
			$this->alarm_datetime->LinkCustomAttributes = "";
			$this->alarm_datetime->HrefValue = "";
			$this->alarm_datetime->TooltipValue = "";

			// alarm_start_on
			$this->alarm_start_on->LinkCustomAttributes = "";
			$this->alarm_start_on->HrefValue = "";
			$this->alarm_start_on->TooltipValue = "";

			// alarm_close_on
			$this->alarm_close_on->LinkCustomAttributes = "";
			$this->alarm_close_on->HrefValue = "";
			$this->alarm_close_on->TooltipValue = "";

			// alarm_close
			$this->alarm_close->LinkCustomAttributes = "";
			$this->alarm_close->HrefValue = "";
			$this->alarm_close->TooltipValue = "";

			// alarm_admin_id
			$this->alarm_admin_id->LinkCustomAttributes = "";
			$this->alarm_admin_id->HrefValue = "";
			$this->alarm_admin_id->TooltipValue = "";

			// alarm_call_parents
			$this->alarm_call_parents->LinkCustomAttributes = "";
			$this->alarm_call_parents->HrefValue = "";
			$this->alarm_call_parents->TooltipValue = "";

			// alarm_call_emergency
			$this->alarm_call_emergency->LinkCustomAttributes = "";
			$this->alarm_call_emergency->HrefValue = "";
			$this->alarm_call_emergency->TooltipValue = "";

			// alarm_note
			$this->alarm_note->LinkCustomAttributes = "";
			$this->alarm_note->HrefValue = "";
			$this->alarm_note->TooltipValue = "";

			// alarm_last_update
			$this->alarm_last_update->LinkCustomAttributes = "";
			$this->alarm_last_update->HrefValue = "";
			$this->alarm_last_update->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// alarm_type_id
			$this->alarm_type_id->EditAttrs["class"] = "form-control";
			$this->alarm_type_id->EditCustomAttributes = "";
			if (trim(strval($this->alarm_type_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`alarm_type_id`" . ew_SearchString("=", $this->alarm_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_alarm_type`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_alarm_type`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->alarm_type_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->alarm_type_id->EditValue = $arwrk;

			// alarm_user_id
			$this->alarm_user_id->EditAttrs["class"] = "form-control";
			$this->alarm_user_id->EditCustomAttributes = "";
			if ($this->alarm_user_id->getSessionValue() <> "") {
				$this->alarm_user_id->CurrentValue = $this->alarm_user_id->getSessionValue();
			$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
			if (strval($this->alarm_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->alarm_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->alarm_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->alarm_user_id->ViewValue = $this->alarm_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->alarm_user_id->ViewValue = $this->alarm_user_id->CurrentValue;
				}
			} else {
				$this->alarm_user_id->ViewValue = NULL;
			}
			$this->alarm_user_id->ViewCustomAttributes = "";
			} else {
			$this->alarm_user_id->EditValue = ew_HtmlEncode($this->alarm_user_id->CurrentValue);
			if (strval($this->alarm_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->alarm_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->alarm_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
					$arwrk[2] = ew_HtmlEncode($rswrk->fields('Disp2Fld'));
					$this->alarm_user_id->EditValue = $this->alarm_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->alarm_user_id->EditValue = ew_HtmlEncode($this->alarm_user_id->CurrentValue);
				}
			} else {
				$this->alarm_user_id->EditValue = NULL;
			}
			$this->alarm_user_id->PlaceHolder = ew_RemoveHtml($this->alarm_user_id->FldCaption());
			}

			// alarm_product_id
			$this->alarm_product_id->EditAttrs["class"] = "form-control";
			$this->alarm_product_id->EditCustomAttributes = "";
			if (trim(strval($this->alarm_product_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "telecare_product.product_id" . ew_SearchString("=", $this->alarm_product_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->alarm_product_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->alarm_product_id->EditValue = $arwrk;

			// alarm_datetime
			$this->alarm_datetime->EditAttrs["class"] = "form-control";
			$this->alarm_datetime->EditCustomAttributes = "";
			$this->alarm_datetime->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->alarm_datetime->CurrentValue, 11));
			$this->alarm_datetime->PlaceHolder = ew_RemoveHtml($this->alarm_datetime->FldCaption());

			// alarm_start_on
			$this->alarm_start_on->EditAttrs["class"] = "form-control";
			$this->alarm_start_on->EditCustomAttributes = "";
			$this->alarm_start_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->alarm_start_on->CurrentValue, 11));
			$this->alarm_start_on->PlaceHolder = ew_RemoveHtml($this->alarm_start_on->FldCaption());

			// alarm_close_on
			// alarm_close

			$this->alarm_close->EditAttrs["class"] = "form-control";
			$this->alarm_close->EditCustomAttributes = "";
			$this->alarm_close->EditValue = $this->alarm_close->Options(TRUE);

			// alarm_admin_id
			// alarm_call_parents

			$this->alarm_call_parents->EditCustomAttributes = "";
			$this->alarm_call_parents->EditValue = $this->alarm_call_parents->Options(FALSE);

			// alarm_call_emergency
			$this->alarm_call_emergency->EditCustomAttributes = "";
			$this->alarm_call_emergency->EditValue = $this->alarm_call_emergency->Options(FALSE);

			// alarm_note
			$this->alarm_note->EditAttrs["class"] = "form-control";
			$this->alarm_note->EditCustomAttributes = "";
			$this->alarm_note->EditValue = ew_HtmlEncode($this->alarm_note->CurrentValue);
			$this->alarm_note->PlaceHolder = ew_RemoveHtml($this->alarm_note->FldCaption());

			// alarm_last_update
			// Edit refer script
			// alarm_type_id

			$this->alarm_type_id->LinkCustomAttributes = "";
			$this->alarm_type_id->HrefValue = "";

			// alarm_user_id
			$this->alarm_user_id->LinkCustomAttributes = "";
			$this->alarm_user_id->HrefValue = "";

			// alarm_product_id
			$this->alarm_product_id->LinkCustomAttributes = "";
			$this->alarm_product_id->HrefValue = "";

			// alarm_datetime
			$this->alarm_datetime->LinkCustomAttributes = "";
			$this->alarm_datetime->HrefValue = "";

			// alarm_start_on
			$this->alarm_start_on->LinkCustomAttributes = "";
			$this->alarm_start_on->HrefValue = "";

			// alarm_close_on
			$this->alarm_close_on->LinkCustomAttributes = "";
			$this->alarm_close_on->HrefValue = "";

			// alarm_close
			$this->alarm_close->LinkCustomAttributes = "";
			$this->alarm_close->HrefValue = "";

			// alarm_admin_id
			$this->alarm_admin_id->LinkCustomAttributes = "";
			$this->alarm_admin_id->HrefValue = "";

			// alarm_call_parents
			$this->alarm_call_parents->LinkCustomAttributes = "";
			$this->alarm_call_parents->HrefValue = "";

			// alarm_call_emergency
			$this->alarm_call_emergency->LinkCustomAttributes = "";
			$this->alarm_call_emergency->HrefValue = "";

			// alarm_note
			$this->alarm_note->LinkCustomAttributes = "";
			$this->alarm_note->HrefValue = "";

			// alarm_last_update
			$this->alarm_last_update->LinkCustomAttributes = "";
			$this->alarm_last_update->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->alarm_type_id->FldIsDetailKey && !is_null($this->alarm_type_id->FormValue) && $this->alarm_type_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->alarm_type_id->FldCaption(), $this->alarm_type_id->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->alarm_user_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->alarm_user_id->FldErrMsg());
		}
		if (!$this->alarm_product_id->FldIsDetailKey && !is_null($this->alarm_product_id->FormValue) && $this->alarm_product_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->alarm_product_id->FldCaption(), $this->alarm_product_id->ReqErrMsg));
		}
		if (!$this->alarm_datetime->FldIsDetailKey && !is_null($this->alarm_datetime->FormValue) && $this->alarm_datetime->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->alarm_datetime->FldCaption(), $this->alarm_datetime->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->alarm_datetime->FormValue)) {
			ew_AddMessage($gsFormError, $this->alarm_datetime->FldErrMsg());
		}
		if (!$this->alarm_start_on->FldIsDetailKey && !is_null($this->alarm_start_on->FormValue) && $this->alarm_start_on->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->alarm_start_on->FldCaption(), $this->alarm_start_on->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->alarm_start_on->FormValue)) {
			ew_AddMessage($gsFormError, $this->alarm_start_on->FldErrMsg());
		}
		if (!$this->alarm_close->FldIsDetailKey && !is_null($this->alarm_close->FormValue) && $this->alarm_close->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->alarm_close->FldCaption(), $this->alarm_close->ReqErrMsg));
		}
		if ($this->alarm_call_parents->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->alarm_call_parents->FldCaption(), $this->alarm_call_parents->ReqErrMsg));
		}
		if ($this->alarm_call_emergency->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->alarm_call_emergency->FldCaption(), $this->alarm_call_emergency->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// alarm_type_id
		$this->alarm_type_id->SetDbValueDef($rsnew, $this->alarm_type_id->CurrentValue, 0, FALSE);

		// alarm_user_id
		$this->alarm_user_id->SetDbValueDef($rsnew, $this->alarm_user_id->CurrentValue, NULL, FALSE);

		// alarm_product_id
		$this->alarm_product_id->SetDbValueDef($rsnew, $this->alarm_product_id->CurrentValue, 0, FALSE);

		// alarm_datetime
		$this->alarm_datetime->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->alarm_datetime->CurrentValue, 11), ew_CurrentDate(), FALSE);

		// alarm_start_on
		$this->alarm_start_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->alarm_start_on->CurrentValue, 11), NULL, FALSE);

		// alarm_close_on
		$this->alarm_close_on->SetDbValueDef($rsnew, ew_CurrentDateTime(), ew_CurrentDate());
		$rsnew['alarm_close_on'] = &$this->alarm_close_on->DbValue;

		// alarm_close
		$this->alarm_close->SetDbValueDef($rsnew, $this->alarm_close->CurrentValue, 0, FALSE);

		// alarm_admin_id
		$this->alarm_admin_id->SetDbValueDef($rsnew, CurrentUserID(), 0);
		$rsnew['alarm_admin_id'] = &$this->alarm_admin_id->DbValue;

		// alarm_call_parents
		$this->alarm_call_parents->SetDbValueDef($rsnew, $this->alarm_call_parents->CurrentValue, 0, FALSE);

		// alarm_call_emergency
		$this->alarm_call_emergency->SetDbValueDef($rsnew, $this->alarm_call_emergency->CurrentValue, 0, FALSE);

		// alarm_note
		$this->alarm_note->SetDbValueDef($rsnew, $this->alarm_note->CurrentValue, NULL, FALSE);

		// alarm_last_update
		$this->alarm_last_update->SetDbValueDef($rsnew, ew_CurrentDateTime(), NULL);
		$rsnew['alarm_last_update'] = &$this->alarm_last_update->DbValue;

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->alarm_id->setDbValue($conn->Insert_ID());
				$rsnew['alarm_id'] = $this->alarm_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setQueryStringValue($_GET["fk_user_id"]);
					$this->alarm_user_id->setQueryStringValue($GLOBALS["telecare_user"]->user_id->QueryStringValue);
					$this->alarm_user_id->setSessionValue($this->alarm_user_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setFormValue($_POST["fk_user_id"]);
					$this->alarm_user_id->setFormValue($GLOBALS["telecare_user"]->user_id->FormValue);
					$this->alarm_user_id->setSessionValue($this->alarm_user_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "telecare_user") {
				if ($this->alarm_user_id->QueryStringValue == "") $this->alarm_user_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_alarmlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_alarm_add)) $telecare_alarm_add = new ctelecare_alarm_add();

// Page init
$telecare_alarm_add->Page_Init();

// Page main
$telecare_alarm_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_alarm_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_alarmadd = new ew_Form("ftelecare_alarmadd", "add");

// Validate form
ftelecare_alarmadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_alarm_type_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_type_id->FldCaption(), $telecare_alarm->alarm_type_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_user_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_user_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_alarm_product_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_product_id->FldCaption(), $telecare_alarm->alarm_product_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_datetime");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_datetime->FldCaption(), $telecare_alarm->alarm_datetime->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_datetime");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_datetime->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_alarm_start_on");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_start_on->FldCaption(), $telecare_alarm->alarm_start_on->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_start_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_alarm->alarm_start_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_alarm_close");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_close->FldCaption(), $telecare_alarm->alarm_close->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_call_parents");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_call_parents->FldCaption(), $telecare_alarm->alarm_call_parents->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_alarm_call_emergency");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_alarm->alarm_call_emergency->FldCaption(), $telecare_alarm->alarm_call_emergency->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_alarmadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_alarmadd.ValidateRequired = true;
<?php } else { ?>
ftelecare_alarmadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_alarmadd.Lists["x_alarm_type_id"] = {"LinkField":"x_alarm_type_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_alarm_type_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmadd.Lists["x_alarm_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmadd.Lists["x_alarm_product_id"] = {"LinkField":"x_product_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_product_type_name","x_product_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmadd.Lists["x_alarm_close"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmadd.Lists["x_alarm_close"].Options = <?php echo json_encode($telecare_alarm->alarm_close->Options()) ?>;
ftelecare_alarmadd.Lists["x_alarm_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmadd.Lists["x_alarm_call_parents"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmadd.Lists["x_alarm_call_parents"].Options = <?php echo json_encode($telecare_alarm->alarm_call_parents->Options()) ?>;
ftelecare_alarmadd.Lists["x_alarm_call_emergency"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_alarmadd.Lists["x_alarm_call_emergency"].Options = <?php echo json_encode($telecare_alarm->alarm_call_emergency->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_alarm_add->ShowPageHeader(); ?>
<?php
$telecare_alarm_add->ShowMessage();
?>
<form name="ftelecare_alarmadd" id="ftelecare_alarmadd" class="<?php echo $telecare_alarm_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_alarm_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_alarm_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_alarm">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($telecare_alarm->alarm_type_id->Visible) { // alarm_type_id ?>
	<div id="r_alarm_type_id" class="form-group">
		<label id="elh_telecare_alarm_alarm_type_id" for="x_alarm_type_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_type_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_type_id->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_type_id">
<select data-table="telecare_alarm" data-field="x_alarm_type_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_type_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_type_id->DisplayValueSeparator) : $telecare_alarm->alarm_type_id->DisplayValueSeparator) ?>" id="x_alarm_type_id" name="x_alarm_type_id"<?php echo $telecare_alarm->alarm_type_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_type_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_type_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_type_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_type_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_type_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_type_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_type_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `alarm_type_id`, `alarm_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_alarm_type`";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_type_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_type_id->LookupFilters += array("f0" => "`alarm_type_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_type_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_type_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_alarm_type_id" id="s_x_alarm_type_id" value="<?php echo $telecare_alarm->alarm_type_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_alarm->alarm_type_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_user_id->Visible) { // alarm_user_id ?>
	<div id="r_alarm_user_id" class="form-group">
		<label id="elh_telecare_alarm_alarm_user_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_user_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_user_id->CellAttributes() ?>>
<?php if ($telecare_alarm->alarm_user_id->getSessionValue() <> "") { ?>
<span id="el_telecare_alarm_alarm_user_id">
<span<?php echo $telecare_alarm->alarm_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_alarm->alarm_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_alarm_user_id" name="x_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_telecare_alarm_alarm_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_alarm->alarm_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_alarm->alarm_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x_alarm_user_id" style="white-space: nowrap; z-index: 8970">
	<input type="text" name="sv_x_alarm_user_id" id="sv_x_alarm_user_id" value="<?php echo $telecare_alarm->alarm_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->getPlaceHolder()) ?>"<?php echo $telecare_alarm->alarm_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_alarm" data-field="x_alarm_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_user_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_user_id->DisplayValueSeparator) : $telecare_alarm->alarm_user_id->DisplayValueSeparator) ?>" name="x_alarm_user_id" id="x_alarm_user_id" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->alarm_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x_alarm_user_id" id="q_x_alarm_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_alarmadd.CreateAutoSuggest({"id":"x_alarm_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php echo $telecare_alarm->alarm_user_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_product_id->Visible) { // alarm_product_id ?>
	<div id="r_alarm_product_id" class="form-group">
		<label id="elh_telecare_alarm_alarm_product_id" for="x_alarm_product_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_product_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_product_id->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_product_id">
<select data-table="telecare_alarm" data-field="x_alarm_product_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_product_id->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_product_id->DisplayValueSeparator) : $telecare_alarm->alarm_product_id->DisplayValueSeparator) ?>" id="x_alarm_product_id" name="x_alarm_product_id"<?php echo $telecare_alarm->alarm_product_id->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_product_id->EditValue)) {
	$arwrk = $telecare_alarm->alarm_product_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_product_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_product_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_product_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_product_id->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_product_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `product_id`, `product_type_name` AS `DispFld`, `product_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM telecare_product INNER JOIN telecare_product_type ON telecare_product_type.product_type_id = telecare_product.product_type_id";
		$sWhereWrk = "";
		break;
}
$telecare_alarm->alarm_product_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_alarm->alarm_product_id->LookupFilters += array("f0" => "telecare_product.product_id = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_alarm->Lookup_Selecting($telecare_alarm->alarm_product_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_alarm->alarm_product_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_alarm_product_id" id="s_x_alarm_product_id" value="<?php echo $telecare_alarm->alarm_product_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_alarm->alarm_product_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_datetime->Visible) { // alarm_datetime ?>
	<div id="r_alarm_datetime" class="form-group">
		<label id="elh_telecare_alarm_alarm_datetime" for="x_alarm_datetime" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_datetime->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_datetime->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_datetime">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_datetime" data-format="11" name="x_alarm_datetime" id="x_alarm_datetime" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_datetime->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_datetime->EditValue ?>"<?php echo $telecare_alarm->alarm_datetime->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_datetime->ReadOnly && !$telecare_alarm->alarm_datetime->Disabled && !isset($telecare_alarm->alarm_datetime->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_datetime->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmadd", "x_alarm_datetime", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<?php echo $telecare_alarm->alarm_datetime->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_start_on->Visible) { // alarm_start_on ?>
	<div id="r_alarm_start_on" class="form-group">
		<label id="elh_telecare_alarm_alarm_start_on" for="x_alarm_start_on" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_start_on->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_start_on->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_start_on">
<input type="text" data-table="telecare_alarm" data-field="x_alarm_start_on" data-format="11" name="x_alarm_start_on" id="x_alarm_start_on" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_alarm->alarm_start_on->EditValue ?>"<?php echo $telecare_alarm->alarm_start_on->EditAttributes() ?>>
<?php if (!$telecare_alarm->alarm_start_on->ReadOnly && !$telecare_alarm->alarm_start_on->Disabled && !isset($telecare_alarm->alarm_start_on->EditAttrs["readonly"]) && !isset($telecare_alarm->alarm_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_alarmadd", "x_alarm_start_on", "%d/%m/%Y %H:%M:%S");
</script>
<?php } ?>
</span>
<?php echo $telecare_alarm->alarm_start_on->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_close->Visible) { // alarm_close ?>
	<div id="r_alarm_close" class="form-group">
		<label id="elh_telecare_alarm_alarm_close" for="x_alarm_close" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_close->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_close->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_close">
<select data-table="telecare_alarm" data-field="x_alarm_close" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_close->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_close->DisplayValueSeparator) : $telecare_alarm->alarm_close->DisplayValueSeparator) ?>" id="x_alarm_close" name="x_alarm_close"<?php echo $telecare_alarm->alarm_close->EditAttributes() ?>>
<?php
if (is_array($telecare_alarm->alarm_close->EditValue)) {
	$arwrk = $telecare_alarm->alarm_close->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_alarm->alarm_close->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_alarm->alarm_close->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_close->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_close->CurrentValue) ?>" selected><?php echo $telecare_alarm->alarm_close->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php echo $telecare_alarm->alarm_close->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_parents->Visible) { // alarm_call_parents ?>
	<div id="r_alarm_call_parents" class="form-group">
		<label id="elh_telecare_alarm_alarm_call_parents" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_call_parents->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_call_parents->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_call_parents">
<div id="tp_x_alarm_call_parents" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_parents->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_parents->DisplayValueSeparator) : $telecare_alarm->alarm_call_parents->DisplayValueSeparator) ?>" name="x_alarm_call_parents" id="x_alarm_call_parents" value="{value}"<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>></div>
<div id="dsl_x_alarm_call_parents" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_parents->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_parents->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x_alarm_call_parents" id="x_alarm_call_parents_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_parents->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_parents" name="x_alarm_call_parents" id="x_alarm_call_parents_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_parents->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_parents->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_parents->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_alarm->alarm_call_parents->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_call_emergency->Visible) { // alarm_call_emergency ?>
	<div id="r_alarm_call_emergency" class="form-group">
		<label id="elh_telecare_alarm_alarm_call_emergency" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_call_emergency->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_call_emergency->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_call_emergency">
<div id="tp_x_alarm_call_emergency" class="ewTemplate"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ? json_encode($telecare_alarm->alarm_call_emergency->DisplayValueSeparator) : $telecare_alarm->alarm_call_emergency->DisplayValueSeparator) ?>" name="x_alarm_call_emergency" id="x_alarm_call_emergency" value="{value}"<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>></div>
<div id="dsl_x_alarm_call_emergency" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_alarm->alarm_call_emergency->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_alarm->alarm_call_emergency->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x_alarm_call_emergency" id="x_alarm_call_emergency_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_alarm->alarm_call_emergency->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_alarm" data-field="x_alarm_call_emergency" name="x_alarm_call_emergency" id="x_alarm_call_emergency_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_alarm->alarm_call_emergency->CurrentValue) ?>" checked<?php echo $telecare_alarm->alarm_call_emergency->EditAttributes() ?>><?php echo $telecare_alarm->alarm_call_emergency->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_alarm->alarm_call_emergency->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_alarm->alarm_note->Visible) { // alarm_note ?>
	<div id="r_alarm_note" class="form-group">
		<label id="elh_telecare_alarm_alarm_note" for="x_alarm_note" class="col-sm-2 control-label ewLabel"><?php echo $telecare_alarm->alarm_note->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_alarm->alarm_note->CellAttributes() ?>>
<span id="el_telecare_alarm_alarm_note">
<textarea data-table="telecare_alarm" data-field="x_alarm_note" name="x_alarm_note" id="x_alarm_note" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($telecare_alarm->alarm_note->getPlaceHolder()) ?>"<?php echo $telecare_alarm->alarm_note->EditAttributes() ?>><?php echo $telecare_alarm->alarm_note->EditValue ?></textarea>
</span>
<?php echo $telecare_alarm->alarm_note->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_alarm_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_alarmadd.Init();
</script>
<?php
$telecare_alarm_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_alarm_add->Page_Terminate();
?>
