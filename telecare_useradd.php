<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_callgridcls.php" ?>
<?php include_once "telecare_alarmgridcls.php" ?>
<?php include_once "telecare_prescriptiongridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_user_add = NULL; // Initialize page object first

class ctelecare_user_add extends ctelecare_user {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_user';

	// Page object name
	var $PageObjName = 'telecare_user_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
    var $AuditTrailOnAdd = TRUE;
    var $AuditTrailOnEdit = FALSE;
    var $AuditTrailOnDelete = FALSE;
    var $AuditTrailOnView = FALSE;
    var $AuditTrailOnViewData = FALSE;
    var $AuditTrailOnSearch = FALSE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_user)
		if (!isset($GLOBALS["telecare_user"]) || get_class($GLOBALS["telecare_user"]) == "ctelecare_user") {
			$GLOBALS["telecare_user"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_user"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_user', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_userlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'telecare_call'
			if (@$_POST["grid"] == "ftelecare_callgrid") {
				if (!isset($GLOBALS["telecare_call_grid"])) $GLOBALS["telecare_call_grid"] = new ctelecare_call_grid;
				$GLOBALS["telecare_call_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'telecare_alarm'
			if (@$_POST["grid"] == "ftelecare_alarmgrid") {
				if (!isset($GLOBALS["telecare_alarm_grid"])) $GLOBALS["telecare_alarm_grid"] = new ctelecare_alarm_grid;
				$GLOBALS["telecare_alarm_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}

			// Process auto fill for detail table 'telecare_prescription'
			if (@$_POST["grid"] == "ftelecare_prescriptiongrid") {
				if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;
				$GLOBALS["telecare_prescription_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_user;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_user);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;
	var $MultiPages; // Multi pages object

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["user_id"] != "") {
				$this->user_id->setQueryStringValue($_GET["user_id"]);
				$this->setKey("user_id", $this->user_id->CurrentValue); // Set up key
			} else {
				$this->setKey("user_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Set up detail parameters
		$this->SetUpDetailParms();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_userlist.php"); // No matching record, return to list
				}

				// Set up detail parameters
				$this->SetUpDetailParms();
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					if ($this->getCurrentDetailTable() <> "") // Master/detail add
						$sReturnUrl = $this->GetDetailUrl();
					else
						$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_userview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values

					// Set up detail parameters
					$this->SetUpDetailParms();
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->user_surname->CurrentValue = NULL;
		$this->user_surname->OldValue = $this->user_surname->CurrentValue;
		$this->user_name->CurrentValue = NULL;
		$this->user_name->OldValue = $this->user_name->CurrentValue;
		$this->user_gender->CurrentValue = 1;
		$this->user_born->CurrentValue = NULL;
		$this->user_born->OldValue = $this->user_born->CurrentValue;
		$this->user_modules->CurrentValue = NULL;
		$this->user_modules->OldValue = $this->user_modules->CurrentValue;
		$this->user_doctor_id->CurrentValue = NULL;
		$this->user_doctor_id->OldValue = $this->user_doctor_id->CurrentValue;
		$this->user_is_active->CurrentValue = 1;
		$this->user_medical_history->CurrentValue = NULL;
		$this->user_medical_history->OldValue = $this->user_medical_history->CurrentValue;
		$this->user_address->CurrentValue = NULL;
		$this->user_address->OldValue = $this->user_address->CurrentValue;
		$this->user_region_id->CurrentValue = NULL;
		$this->user_region_id->OldValue = $this->user_region_id->CurrentValue;
		$this->user_province_id->CurrentValue = NULL;
		$this->user_province_id->OldValue = $this->user_province_id->CurrentValue;
		$this->user_city_id->CurrentValue = NULL;
		$this->user_city_id->OldValue = $this->user_city_id->CurrentValue;
		$this->user_email->CurrentValue = NULL;
		$this->user_email->OldValue = $this->user_email->CurrentValue;
		$this->user_phone->CurrentValue = NULL;
		$this->user_phone->OldValue = $this->user_phone->CurrentValue;
		$this->user_language->CurrentValue = 1;
		$this->user_username->CurrentValue = NULL;
		$this->user_username->OldValue = $this->user_username->CurrentValue;
		$this->user_password->CurrentValue = NULL;
		$this->user_password->OldValue = $this->user_password->CurrentValue;
		$this->user_latitude->CurrentValue = NULL;
		$this->user_latitude->OldValue = $this->user_latitude->CurrentValue;
		$this->user_longitude->CurrentValue = NULL;
		$this->user_longitude->OldValue = $this->user_longitude->CurrentValue;
		$this->user_last_update->CurrentValue = NULL;
		$this->user_last_update->OldValue = $this->user_last_update->CurrentValue;
		$this->user_admin_id->CurrentValue = NULL;
		$this->user_admin_id->OldValue = $this->user_admin_id->CurrentValue;
		$this->user_level->CurrentValue = NULL;
		$this->user_level->OldValue = $this->user_level->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->user_surname->FldIsDetailKey) {
			$this->user_surname->setFormValue($objForm->GetValue("x_user_surname"));
		}
		if (!$this->user_name->FldIsDetailKey) {
			$this->user_name->setFormValue($objForm->GetValue("x_user_name"));
		}
		if (!$this->user_gender->FldIsDetailKey) {
			$this->user_gender->setFormValue($objForm->GetValue("x_user_gender"));
		}
		if (!$this->user_born->FldIsDetailKey) {
			$this->user_born->setFormValue($objForm->GetValue("x_user_born"));
			$this->user_born->CurrentValue = ew_UnFormatDateTime($this->user_born->CurrentValue, 7);
		}
		if (!$this->user_modules->FldIsDetailKey) {
			$this->user_modules->setFormValue($objForm->GetValue("x_user_modules"));
		}
		if (!$this->user_doctor_id->FldIsDetailKey) {
			$this->user_doctor_id->setFormValue($objForm->GetValue("x_user_doctor_id"));
		}
		if (!$this->user_is_active->FldIsDetailKey) {
			$this->user_is_active->setFormValue($objForm->GetValue("x_user_is_active"));
		}
		if (!$this->user_medical_history->FldIsDetailKey) {
			$this->user_medical_history->setFormValue($objForm->GetValue("x_user_medical_history"));
		}
		if (!$this->user_address->FldIsDetailKey) {
			$this->user_address->setFormValue($objForm->GetValue("x_user_address"));
		}
		if (!$this->user_region_id->FldIsDetailKey) {
			$this->user_region_id->setFormValue($objForm->GetValue("x_user_region_id"));
		}
		if (!$this->user_province_id->FldIsDetailKey) {
			$this->user_province_id->setFormValue($objForm->GetValue("x_user_province_id"));
		}
		if (!$this->user_city_id->FldIsDetailKey) {
			$this->user_city_id->setFormValue($objForm->GetValue("x_user_city_id"));
		}
		if (!$this->user_email->FldIsDetailKey) {
			$this->user_email->setFormValue($objForm->GetValue("x_user_email"));
		}
		if (!$this->user_phone->FldIsDetailKey) {
			$this->user_phone->setFormValue($objForm->GetValue("x_user_phone"));
		}
		if (!$this->user_language->FldIsDetailKey) {
			$this->user_language->setFormValue($objForm->GetValue("x_user_language"));
		}
		if (!$this->user_username->FldIsDetailKey) {
			$this->user_username->setFormValue($objForm->GetValue("x_user_username"));
		}
		if (!$this->user_password->FldIsDetailKey) {
			$this->user_password->setFormValue($objForm->GetValue("x_user_password"));
		}
		if (!$this->user_latitude->FldIsDetailKey) {
			$this->user_latitude->setFormValue($objForm->GetValue("x_user_latitude"));
		}
		if (!$this->user_longitude->FldIsDetailKey) {
			$this->user_longitude->setFormValue($objForm->GetValue("x_user_longitude"));
		}
		if (!$this->user_last_update->FldIsDetailKey) {
			$this->user_last_update->setFormValue($objForm->GetValue("x_user_last_update"));
			$this->user_last_update->CurrentValue = ew_UnFormatDateTime($this->user_last_update->CurrentValue, 7);
		}
		if (!$this->user_admin_id->FldIsDetailKey) {
			$this->user_admin_id->setFormValue($objForm->GetValue("x_user_admin_id"));
		}
		if (!$this->user_level->FldIsDetailKey) {
			$this->user_level->setFormValue($objForm->GetValue("x_user_level"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->user_surname->CurrentValue = $this->user_surname->FormValue;
		$this->user_name->CurrentValue = $this->user_name->FormValue;
		$this->user_gender->CurrentValue = $this->user_gender->FormValue;
		$this->user_born->CurrentValue = $this->user_born->FormValue;
		$this->user_born->CurrentValue = ew_UnFormatDateTime($this->user_born->CurrentValue, 7);
		$this->user_modules->CurrentValue = $this->user_modules->FormValue;
		$this->user_doctor_id->CurrentValue = $this->user_doctor_id->FormValue;
		$this->user_is_active->CurrentValue = $this->user_is_active->FormValue;
		$this->user_medical_history->CurrentValue = $this->user_medical_history->FormValue;
		$this->user_address->CurrentValue = $this->user_address->FormValue;
		$this->user_region_id->CurrentValue = $this->user_region_id->FormValue;
		$this->user_province_id->CurrentValue = $this->user_province_id->FormValue;
		$this->user_city_id->CurrentValue = $this->user_city_id->FormValue;
		$this->user_email->CurrentValue = $this->user_email->FormValue;
		$this->user_phone->CurrentValue = $this->user_phone->FormValue;
		$this->user_language->CurrentValue = $this->user_language->FormValue;
		$this->user_username->CurrentValue = $this->user_username->FormValue;
		$this->user_password->CurrentValue = $this->user_password->FormValue;
		$this->user_latitude->CurrentValue = $this->user_latitude->FormValue;
		$this->user_longitude->CurrentValue = $this->user_longitude->FormValue;
		$this->user_last_update->CurrentValue = $this->user_last_update->FormValue;
		$this->user_last_update->CurrentValue = ew_UnFormatDateTime($this->user_last_update->CurrentValue, 7);
		$this->user_admin_id->CurrentValue = $this->user_admin_id->FormValue;
		$this->user_level->CurrentValue = $this->user_level->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->user_id->setDbValue($rs->fields('user_id'));
		$this->user_surname->setDbValue($rs->fields('user_surname'));
		$this->user_name->setDbValue($rs->fields('user_name'));
		$this->user_gender->setDbValue($rs->fields('user_gender'));
		$this->user_born->setDbValue($rs->fields('user_born'));
		$this->user_modules->setDbValue($rs->fields('user_modules'));
		$this->user_doctor_id->setDbValue($rs->fields('user_doctor_id'));
		$this->user_is_active->setDbValue($rs->fields('user_is_active'));
		$this->user_medical_history->setDbValue($rs->fields('user_medical_history'));
		$this->user_address->setDbValue($rs->fields('user_address'));
		$this->user_region_id->setDbValue($rs->fields('user_region_id'));
		$this->user_province_id->setDbValue($rs->fields('user_province_id'));
		$this->user_city_id->setDbValue($rs->fields('user_city_id'));
		$this->user_email->setDbValue($rs->fields('user_email'));
		$this->user_phone->setDbValue($rs->fields('user_phone'));
		$this->user_language->setDbValue($rs->fields('user_language'));
		$this->user_username->setDbValue($rs->fields('user_username'));
		$this->user_password->setDbValue($rs->fields('user_password'));
		$this->user_latitude->setDbValue($rs->fields('user_latitude'));
		$this->user_longitude->setDbValue($rs->fields('user_longitude'));
		$this->user_last_update->setDbValue($rs->fields('user_last_update'));
		$this->user_admin_id->setDbValue($rs->fields('user_admin_id'));
		$this->user_level->setDbValue($rs->fields('user_level'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->user_id->DbValue = $row['user_id'];
		$this->user_surname->DbValue = $row['user_surname'];
		$this->user_name->DbValue = $row['user_name'];
		$this->user_gender->DbValue = $row['user_gender'];
		$this->user_born->DbValue = $row['user_born'];
		$this->user_modules->DbValue = $row['user_modules'];
		$this->user_doctor_id->DbValue = $row['user_doctor_id'];
		$this->user_is_active->DbValue = $row['user_is_active'];
		$this->user_medical_history->DbValue = $row['user_medical_history'];
		$this->user_address->DbValue = $row['user_address'];
		$this->user_region_id->DbValue = $row['user_region_id'];
		$this->user_province_id->DbValue = $row['user_province_id'];
		$this->user_city_id->DbValue = $row['user_city_id'];
		$this->user_email->DbValue = $row['user_email'];
		$this->user_phone->DbValue = $row['user_phone'];
		$this->user_language->DbValue = $row['user_language'];
		$this->user_username->DbValue = $row['user_username'];
		$this->user_password->DbValue = $row['user_password'];
		$this->user_latitude->DbValue = $row['user_latitude'];
		$this->user_longitude->DbValue = $row['user_longitude'];
		$this->user_last_update->DbValue = $row['user_last_update'];
		$this->user_admin_id->DbValue = $row['user_admin_id'];
		$this->user_level->DbValue = $row['user_level'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("user_id")) <> "")
			$this->user_id->CurrentValue = $this->getKey("user_id"); // user_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// user_id
		// user_surname
		// user_name
		// user_gender
		// user_born
		// user_modules
		// user_doctor_id
		// user_is_active
		// user_medical_history
		// user_address
		// user_region_id
		// user_province_id
		// user_city_id
		// user_email
		// user_phone
		// user_language
		// user_username
		// user_password
		// user_latitude
		// user_longitude
		// user_last_update
		// user_admin_id
		// user_level

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// user_id
		$this->user_id->ViewValue = $this->user_id->CurrentValue;
		$this->user_id->ViewCustomAttributes = "";

		// user_surname
		$this->user_surname->ViewValue = $this->user_surname->CurrentValue;
		$this->user_surname->ViewCustomAttributes = "";

		// user_name
		$this->user_name->ViewValue = $this->user_name->CurrentValue;
		$this->user_name->ViewCustomAttributes = "";

		// user_gender
		if (strval($this->user_gender->CurrentValue) <> "") {
			$this->user_gender->ViewValue = $this->user_gender->OptionCaption($this->user_gender->CurrentValue);
		} else {
			$this->user_gender->ViewValue = NULL;
		}
		$this->user_gender->ViewCustomAttributes = "";

		// user_born
		$this->user_born->ViewValue = $this->user_born->CurrentValue;
		$this->user_born->ViewValue = ew_FormatDateTime($this->user_born->ViewValue, 7);
		$this->user_born->ViewCustomAttributes = "";

		// user_modules
		if (strval($this->user_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->user_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->user_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->user_modules->ViewValue .= $this->user_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->user_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->user_modules->ViewValue = $this->user_modules->CurrentValue;
			}
		} else {
			$this->user_modules->ViewValue = NULL;
		}
		$this->user_modules->ViewCustomAttributes = "";

		// user_doctor_id
		if (strval($this->user_doctor_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = " `admin_level` = 2 ";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_doctor_id->ViewValue = $this->user_doctor_id->CurrentValue;
			}
		} else {
			$this->user_doctor_id->ViewValue = NULL;
		}
		$this->user_doctor_id->ViewCustomAttributes = "";

		// user_is_active
		$this->user_is_active->ViewValue = $this->user_is_active->CurrentValue;
		$this->user_is_active->ViewCustomAttributes = "";

		// user_medical_history
		$this->user_medical_history->ViewValue = $this->user_medical_history->CurrentValue;
		$this->user_medical_history->ViewCustomAttributes = "";

		// user_address
		$this->user_address->ViewValue = $this->user_address->CurrentValue;
		$this->user_address->ViewCustomAttributes = "";

		// user_region_id
		if (strval($this->user_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->user_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `region_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_region_id->ViewValue = $this->user_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_region_id->ViewValue = $this->user_region_id->CurrentValue;
			}
		} else {
			$this->user_region_id->ViewValue = NULL;
		}
		$this->user_region_id->ViewCustomAttributes = "";

		// user_province_id
		if (strval($this->user_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->user_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `province_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_province_id->ViewValue = $this->user_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_province_id->ViewValue = $this->user_province_id->CurrentValue;
			}
		} else {
			$this->user_province_id->ViewValue = NULL;
		}
		$this->user_province_id->ViewCustomAttributes = "";

		// user_city_id
		if (strval($this->user_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->user_city_id->ViewValue = $this->user_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->user_city_id->ViewValue = $this->user_city_id->CurrentValue;
			}
		} else {
			$this->user_city_id->ViewValue = NULL;
		}
		$this->user_city_id->ViewCustomAttributes = "";

		// user_email
		$this->user_email->ViewValue = $this->user_email->CurrentValue;
		$this->user_email->ViewCustomAttributes = "";

		// user_phone
		$this->user_phone->ViewValue = $this->user_phone->CurrentValue;
		$this->user_phone->ViewCustomAttributes = "";

		// user_language
		$this->user_language->ViewValue = $this->user_language->CurrentValue;
		$this->user_language->ViewCustomAttributes = "";

		// user_username
		$this->user_username->ViewValue = $this->user_username->CurrentValue;
		$this->user_username->ViewCustomAttributes = "";

		// user_password
		$this->user_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->user_password->ViewCustomAttributes = "";

		// user_latitude
		$this->user_latitude->ViewValue = $this->user_latitude->CurrentValue;
		$this->user_latitude->ViewCustomAttributes = "";

		// user_longitude
		$this->user_longitude->ViewValue = $this->user_longitude->CurrentValue;
		$this->user_longitude->ViewCustomAttributes = "";

		// user_last_update
		$this->user_last_update->ViewValue = $this->user_last_update->CurrentValue;
		$this->user_last_update->ViewValue = ew_FormatDateTime($this->user_last_update->ViewValue, 7);
		$this->user_last_update->ViewCustomAttributes = "";

		// user_admin_id
		$this->user_admin_id->ViewValue = $this->user_admin_id->CurrentValue;
		$this->user_admin_id->ViewCustomAttributes = "";

		// user_level
		$this->user_level->ViewValue = $this->user_level->CurrentValue;
		$this->user_level->ViewCustomAttributes = "";

			// user_surname
			$this->user_surname->LinkCustomAttributes = "";
			$this->user_surname->HrefValue = "";
			$this->user_surname->TooltipValue = "";

			// user_name
			$this->user_name->LinkCustomAttributes = "";
			$this->user_name->HrefValue = "";
			$this->user_name->TooltipValue = "";

			// user_gender
			$this->user_gender->LinkCustomAttributes = "";
			$this->user_gender->HrefValue = "";
			$this->user_gender->TooltipValue = "";

			// user_born
			$this->user_born->LinkCustomAttributes = "";
			$this->user_born->HrefValue = "";
			$this->user_born->TooltipValue = "";

			// user_modules
			$this->user_modules->LinkCustomAttributes = "";
			$this->user_modules->HrefValue = "";
			$this->user_modules->TooltipValue = "";

			// user_doctor_id
			$this->user_doctor_id->LinkCustomAttributes = "";
			$this->user_doctor_id->HrefValue = "";
			$this->user_doctor_id->TooltipValue = "";

			// user_is_active
			$this->user_is_active->LinkCustomAttributes = "";
			$this->user_is_active->HrefValue = "";
			$this->user_is_active->TooltipValue = "";

			// user_medical_history
			$this->user_medical_history->LinkCustomAttributes = "";
			$this->user_medical_history->HrefValue = "";
			$this->user_medical_history->TooltipValue = "";

			// user_address
			$this->user_address->LinkCustomAttributes = "";
			$this->user_address->HrefValue = "";
			$this->user_address->TooltipValue = "";

			// user_region_id
			$this->user_region_id->LinkCustomAttributes = "";
			$this->user_region_id->HrefValue = "";
			$this->user_region_id->TooltipValue = "";

			// user_province_id
			$this->user_province_id->LinkCustomAttributes = "";
			$this->user_province_id->HrefValue = "";
			$this->user_province_id->TooltipValue = "";

			// user_city_id
			$this->user_city_id->LinkCustomAttributes = "";
			$this->user_city_id->HrefValue = "";
			$this->user_city_id->TooltipValue = "";

			// user_email
			$this->user_email->LinkCustomAttributes = "";
			$this->user_email->HrefValue = "";
			$this->user_email->TooltipValue = "";

			// user_phone
			$this->user_phone->LinkCustomAttributes = "";
			$this->user_phone->HrefValue = "";
			$this->user_phone->TooltipValue = "";

			// user_language
			$this->user_language->LinkCustomAttributes = "";
			$this->user_language->HrefValue = "";
			$this->user_language->TooltipValue = "";

			// user_username
			$this->user_username->LinkCustomAttributes = "";
			$this->user_username->HrefValue = "";
			$this->user_username->TooltipValue = "";

			// user_password
			$this->user_password->LinkCustomAttributes = "";
			$this->user_password->HrefValue = "";
			$this->user_password->TooltipValue = "";

			// user_latitude
			$this->user_latitude->LinkCustomAttributes = "";
			$this->user_latitude->HrefValue = "";
			$this->user_latitude->TooltipValue = "";

			// user_longitude
			$this->user_longitude->LinkCustomAttributes = "";
			$this->user_longitude->HrefValue = "";
			$this->user_longitude->TooltipValue = "";

			// user_last_update
			$this->user_last_update->LinkCustomAttributes = "";
			$this->user_last_update->HrefValue = "";
			$this->user_last_update->TooltipValue = "";

			// user_admin_id
			$this->user_admin_id->LinkCustomAttributes = "";
			$this->user_admin_id->HrefValue = "";
			$this->user_admin_id->TooltipValue = "";

			// user_level
			$this->user_level->LinkCustomAttributes = "";
			$this->user_level->HrefValue = "";
			$this->user_level->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// user_surname
			$this->user_surname->EditAttrs["class"] = "form-control";
			$this->user_surname->EditCustomAttributes = "";
			$this->user_surname->EditValue = ew_HtmlEncode($this->user_surname->CurrentValue);
			$this->user_surname->PlaceHolder = ew_RemoveHtml($this->user_surname->FldCaption());

			// user_name
			$this->user_name->EditAttrs["class"] = "form-control";
			$this->user_name->EditCustomAttributes = "";
			$this->user_name->EditValue = ew_HtmlEncode($this->user_name->CurrentValue);
			$this->user_name->PlaceHolder = ew_RemoveHtml($this->user_name->FldCaption());

			// user_gender
			$this->user_gender->EditCustomAttributes = "";
			$this->user_gender->EditValue = $this->user_gender->Options(FALSE);

			// user_born
			$this->user_born->EditAttrs["class"] = "form-control";
			$this->user_born->EditCustomAttributes = "";
			$this->user_born->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->user_born->CurrentValue, 7));
			$this->user_born->PlaceHolder = ew_RemoveHtml($this->user_born->FldCaption());

			// user_modules
			$this->user_modules->EditCustomAttributes = "";
			if (trim(strval($this->user_modules->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$arwrk = explode(",", $this->user_modules->CurrentValue);
				$sFilterWrk = "";
				foreach ($arwrk as $wrk) {
					if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
					$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
				}
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_modules, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->user_modules->EditValue = $arwrk;

			// user_doctor_id
			$this->user_doctor_id->EditAttrs["class"] = "form-control";
			$this->user_doctor_id->EditCustomAttributes = "";
			if (trim(strval($this->user_doctor_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->user_doctor_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			$lookuptblfilter = " `admin_level` = 2 ";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_user"]->UserIDAllow("add")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->user_doctor_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_doctor_id->EditValue = $arwrk;

			// user_is_active
			$this->user_is_active->EditAttrs["class"] = "form-control";
			$this->user_is_active->EditCustomAttributes = "";
			$this->user_is_active->EditValue = ew_HtmlEncode($this->user_is_active->CurrentValue);
			$this->user_is_active->PlaceHolder = ew_RemoveHtml($this->user_is_active->FldCaption());

			// user_medical_history
			$this->user_medical_history->EditAttrs["class"] = "form-control";
			$this->user_medical_history->EditCustomAttributes = "";
			$this->user_medical_history->EditValue = ew_HtmlEncode($this->user_medical_history->CurrentValue);
			$this->user_medical_history->PlaceHolder = ew_RemoveHtml($this->user_medical_history->FldCaption());

			// user_address
			$this->user_address->EditAttrs["class"] = "form-control";
			$this->user_address->EditCustomAttributes = "";
			$this->user_address->EditValue = ew_HtmlEncode($this->user_address->CurrentValue);
			$this->user_address->PlaceHolder = ew_RemoveHtml($this->user_address->FldCaption());

			// user_region_id
			$this->user_region_id->EditAttrs["class"] = "form-control";
			$this->user_region_id->EditCustomAttributes = "";
			if (trim(strval($this->user_region_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->user_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_region_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `region_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_region_id->EditValue = $arwrk;

			// user_province_id
			$this->user_province_id->EditAttrs["class"] = "form-control";
			$this->user_province_id->EditCustomAttributes = "";
			if (trim(strval($this->user_province_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->user_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_province_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `province_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_province_id->EditValue = $arwrk;

			// user_city_id
			$this->user_city_id->EditAttrs["class"] = "form-control";
			$this->user_city_id->EditCustomAttributes = "";
			if (trim(strval($this->user_city_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->user_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->user_city_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `city_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->user_city_id->EditValue = $arwrk;

			// user_email
			$this->user_email->EditAttrs["class"] = "form-control";
			$this->user_email->EditCustomAttributes = "";
			$this->user_email->EditValue = ew_HtmlEncode($this->user_email->CurrentValue);
			$this->user_email->PlaceHolder = ew_RemoveHtml($this->user_email->FldCaption());

			// user_phone
			$this->user_phone->EditAttrs["class"] = "form-control";
			$this->user_phone->EditCustomAttributes = "";
			$this->user_phone->EditValue = ew_HtmlEncode($this->user_phone->CurrentValue);
			$this->user_phone->PlaceHolder = ew_RemoveHtml($this->user_phone->FldCaption());

			// user_language
			$this->user_language->EditAttrs["class"] = "form-control";
			$this->user_language->EditCustomAttributes = "";
			$this->user_language->EditValue = ew_HtmlEncode($this->user_language->CurrentValue);
			$this->user_language->PlaceHolder = ew_RemoveHtml($this->user_language->FldCaption());

			// user_username
			$this->user_username->EditAttrs["class"] = "form-control";
			$this->user_username->EditCustomAttributes = "";
			$this->user_username->EditValue = ew_HtmlEncode($this->user_username->CurrentValue);
			$this->user_username->PlaceHolder = ew_RemoveHtml($this->user_username->FldCaption());

			// user_password
			$this->user_password->EditAttrs["class"] = "form-control";
			$this->user_password->EditCustomAttributes = "";
			$this->user_password->EditValue = ew_HtmlEncode($this->user_password->CurrentValue);
			$this->user_password->PlaceHolder = ew_RemoveHtml($this->user_password->FldCaption());

			// user_latitude
			$this->user_latitude->EditAttrs["class"] = "form-control";
			$this->user_latitude->EditCustomAttributes = "";
			$this->user_latitude->EditValue = ew_HtmlEncode($this->user_latitude->CurrentValue);
			$this->user_latitude->PlaceHolder = ew_RemoveHtml($this->user_latitude->FldCaption());

			// user_longitude
			$this->user_longitude->EditAttrs["class"] = "form-control";
			$this->user_longitude->EditCustomAttributes = "";
			$this->user_longitude->EditValue = ew_HtmlEncode($this->user_longitude->CurrentValue);
			$this->user_longitude->PlaceHolder = ew_RemoveHtml($this->user_longitude->FldCaption());

			// user_last_update
			$this->user_last_update->EditAttrs["class"] = "form-control";
			$this->user_last_update->EditCustomAttributes = "";
			$this->user_last_update->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->user_last_update->CurrentValue, 7));
			$this->user_last_update->PlaceHolder = ew_RemoveHtml($this->user_last_update->FldCaption());

			// user_admin_id
			// user_level

			$this->user_level->EditAttrs["class"] = "form-control";
			$this->user_level->EditCustomAttributes = "";
			$this->user_level->EditValue = ew_HtmlEncode($this->user_level->CurrentValue);
			$this->user_level->PlaceHolder = ew_RemoveHtml($this->user_level->FldCaption());

			// Edit refer script
			// user_surname

			$this->user_surname->LinkCustomAttributes = "";
			$this->user_surname->HrefValue = "";

			// user_name
			$this->user_name->LinkCustomAttributes = "";
			$this->user_name->HrefValue = "";

			// user_gender
			$this->user_gender->LinkCustomAttributes = "";
			$this->user_gender->HrefValue = "";

			// user_born
			$this->user_born->LinkCustomAttributes = "";
			$this->user_born->HrefValue = "";

			// user_modules
			$this->user_modules->LinkCustomAttributes = "";
			$this->user_modules->HrefValue = "";

			// user_doctor_id
			$this->user_doctor_id->LinkCustomAttributes = "";
			$this->user_doctor_id->HrefValue = "";

			// user_is_active
			$this->user_is_active->LinkCustomAttributes = "";
			$this->user_is_active->HrefValue = "";

			// user_medical_history
			$this->user_medical_history->LinkCustomAttributes = "";
			$this->user_medical_history->HrefValue = "";

			// user_address
			$this->user_address->LinkCustomAttributes = "";
			$this->user_address->HrefValue = "";

			// user_region_id
			$this->user_region_id->LinkCustomAttributes = "";
			$this->user_region_id->HrefValue = "";

			// user_province_id
			$this->user_province_id->LinkCustomAttributes = "";
			$this->user_province_id->HrefValue = "";

			// user_city_id
			$this->user_city_id->LinkCustomAttributes = "";
			$this->user_city_id->HrefValue = "";

			// user_email
			$this->user_email->LinkCustomAttributes = "";
			$this->user_email->HrefValue = "";

			// user_phone
			$this->user_phone->LinkCustomAttributes = "";
			$this->user_phone->HrefValue = "";

			// user_language
			$this->user_language->LinkCustomAttributes = "";
			$this->user_language->HrefValue = "";

			// user_username
			$this->user_username->LinkCustomAttributes = "";
			$this->user_username->HrefValue = "";

			// user_password
			$this->user_password->LinkCustomAttributes = "";
			$this->user_password->HrefValue = "";

			// user_latitude
			$this->user_latitude->LinkCustomAttributes = "";
			$this->user_latitude->HrefValue = "";

			// user_longitude
			$this->user_longitude->LinkCustomAttributes = "";
			$this->user_longitude->HrefValue = "";

			// user_last_update
			$this->user_last_update->LinkCustomAttributes = "";
			$this->user_last_update->HrefValue = "";

			// user_admin_id
			$this->user_admin_id->LinkCustomAttributes = "";
			$this->user_admin_id->HrefValue = "";

			// user_level
			$this->user_level->LinkCustomAttributes = "";
			$this->user_level->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->user_surname->FldIsDetailKey && !is_null($this->user_surname->FormValue) && $this->user_surname->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_surname->FldCaption(), $this->user_surname->ReqErrMsg));
		}
		if (!$this->user_name->FldIsDetailKey && !is_null($this->user_name->FormValue) && $this->user_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_name->FldCaption(), $this->user_name->ReqErrMsg));
		}
		if ($this->user_gender->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_gender->FldCaption(), $this->user_gender->ReqErrMsg));
		}
		if (!ew_CheckEuroDate($this->user_born->FormValue)) {
			ew_AddMessage($gsFormError, $this->user_born->FldErrMsg());
		}
		if ($this->user_modules->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_modules->FldCaption(), $this->user_modules->ReqErrMsg));
		}
		if (!$this->user_doctor_id->FldIsDetailKey && !is_null($this->user_doctor_id->FormValue) && $this->user_doctor_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_doctor_id->FldCaption(), $this->user_doctor_id->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->user_is_active->FormValue)) {
			ew_AddMessage($gsFormError, $this->user_is_active->FldErrMsg());
		}
		if (!$this->user_medical_history->FldIsDetailKey && !is_null($this->user_medical_history->FormValue) && $this->user_medical_history->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_medical_history->FldCaption(), $this->user_medical_history->ReqErrMsg));
		}
		if (!$this->user_address->FldIsDetailKey && !is_null($this->user_address->FormValue) && $this->user_address->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_address->FldCaption(), $this->user_address->ReqErrMsg));
		}
		if (!$this->user_region_id->FldIsDetailKey && !is_null($this->user_region_id->FormValue) && $this->user_region_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_region_id->FldCaption(), $this->user_region_id->ReqErrMsg));
		}
		if (!$this->user_province_id->FldIsDetailKey && !is_null($this->user_province_id->FormValue) && $this->user_province_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_province_id->FldCaption(), $this->user_province_id->ReqErrMsg));
		}
		if (!$this->user_city_id->FldIsDetailKey && !is_null($this->user_city_id->FormValue) && $this->user_city_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_city_id->FldCaption(), $this->user_city_id->ReqErrMsg));
		}
		if (!$this->user_email->FldIsDetailKey && !is_null($this->user_email->FormValue) && $this->user_email->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_email->FldCaption(), $this->user_email->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->user_email->FormValue)) {
			ew_AddMessage($gsFormError, $this->user_email->FldErrMsg());
		}
		if (!ew_CheckInteger($this->user_language->FormValue)) {
			ew_AddMessage($gsFormError, $this->user_language->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->user_last_update->FormValue)) {
			ew_AddMessage($gsFormError, $this->user_last_update->FldErrMsg());
		}
		if (!$this->user_level->FldIsDetailKey && !is_null($this->user_level->FormValue) && $this->user_level->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->user_level->FldCaption(), $this->user_level->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->user_level->FormValue)) {
			ew_AddMessage($gsFormError, $this->user_level->FldErrMsg());
		}

		// Validate detail grid
		$DetailTblVar = explode(",", $this->getCurrentDetailTable());
		if (in_array("telecare_call", $DetailTblVar) && $GLOBALS["telecare_call"]->DetailAdd) {
			if (!isset($GLOBALS["telecare_call_grid"])) $GLOBALS["telecare_call_grid"] = new ctelecare_call_grid(); // get detail page object
			$GLOBALS["telecare_call_grid"]->ValidateGridForm();
		}
		if (in_array("telecare_alarm", $DetailTblVar) && $GLOBALS["telecare_alarm"]->DetailAdd) {
			if (!isset($GLOBALS["telecare_alarm_grid"])) $GLOBALS["telecare_alarm_grid"] = new ctelecare_alarm_grid(); // get detail page object
			$GLOBALS["telecare_alarm_grid"]->ValidateGridForm();
		}
		if (in_array("telecare_prescription", $DetailTblVar) && $GLOBALS["telecare_prescription"]->DetailAdd) {
			if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid(); // get detail page object
			$GLOBALS["telecare_prescription_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Begin transaction
		if ($this->getCurrentDetailTable() <> "")
			$conn->BeginTrans();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// user_surname
		$this->user_surname->SetDbValueDef($rsnew, $this->user_surname->CurrentValue, NULL, FALSE);

		// user_name
		$this->user_name->SetDbValueDef($rsnew, $this->user_name->CurrentValue, NULL, FALSE);

		// user_gender
		$this->user_gender->SetDbValueDef($rsnew, $this->user_gender->CurrentValue, NULL, strval($this->user_gender->CurrentValue) == "");

		// user_born
		$this->user_born->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->user_born->CurrentValue, 7), NULL, FALSE);

		// user_modules
		$this->user_modules->SetDbValueDef($rsnew, $this->user_modules->CurrentValue, NULL, FALSE);

		// user_doctor_id
		$this->user_doctor_id->SetDbValueDef($rsnew, $this->user_doctor_id->CurrentValue, NULL, FALSE);

		// user_is_active
		$this->user_is_active->SetDbValueDef($rsnew, $this->user_is_active->CurrentValue, NULL, strval($this->user_is_active->CurrentValue) == "");

		// user_medical_history
		$this->user_medical_history->SetDbValueDef($rsnew, $this->user_medical_history->CurrentValue, NULL, FALSE);

		// user_address
		$this->user_address->SetDbValueDef($rsnew, $this->user_address->CurrentValue, NULL, FALSE);

		// user_region_id
		$this->user_region_id->SetDbValueDef($rsnew, $this->user_region_id->CurrentValue, NULL, FALSE);

		// user_province_id
		$this->user_province_id->SetDbValueDef($rsnew, $this->user_province_id->CurrentValue, NULL, FALSE);

		// user_city_id
		$this->user_city_id->SetDbValueDef($rsnew, $this->user_city_id->CurrentValue, NULL, FALSE);

		// user_email
		$this->user_email->SetDbValueDef($rsnew, $this->user_email->CurrentValue, NULL, FALSE);

		// user_phone
		$this->user_phone->SetDbValueDef($rsnew, $this->user_phone->CurrentValue, NULL, FALSE);

		// user_language
		$this->user_language->SetDbValueDef($rsnew, $this->user_language->CurrentValue, NULL, strval($this->user_language->CurrentValue) == "");

		// user_username
		$this->user_username->SetDbValueDef($rsnew, $this->user_username->CurrentValue, NULL, FALSE);

		// user_password
		$this->user_password->SetDbValueDef($rsnew, $this->user_password->CurrentValue, NULL, FALSE);

		// user_latitude
		$this->user_latitude->SetDbValueDef($rsnew, $this->user_latitude->CurrentValue, NULL, FALSE);

		// user_longitude
		$this->user_longitude->SetDbValueDef($rsnew, $this->user_longitude->CurrentValue, NULL, FALSE);

		// user_last_update
		$this->user_last_update->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->user_last_update->CurrentValue, 7), NULL, FALSE);

		// user_admin_id
		$this->user_admin_id->SetDbValueDef($rsnew, CurrentUserID(), NULL);
		$rsnew['user_admin_id'] = &$this->user_admin_id->DbValue;

		// user_level
		$this->user_level->SetDbValueDef($rsnew, $this->user_level->CurrentValue, 0, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->user_id->setDbValue($conn->Insert_ID());
				$rsnew['user_id'] = $this->user_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}

		// Add detail records
		if ($AddRow) {
			$DetailTblVar = explode(",", $this->getCurrentDetailTable());
			if (in_array("telecare_call", $DetailTblVar) && $GLOBALS["telecare_call"]->DetailAdd) {
				$GLOBALS["telecare_call"]->call_user_id->setSessionValue($this->user_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["telecare_call_grid"])) $GLOBALS["telecare_call_grid"] = new ctelecare_call_grid(); // Get detail page object
				$AddRow = $GLOBALS["telecare_call_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["telecare_call"]->call_user_id->setSessionValue(""); // Clear master key if insert failed
			}
			if (in_array("telecare_alarm", $DetailTblVar) && $GLOBALS["telecare_alarm"]->DetailAdd) {
				$GLOBALS["telecare_alarm"]->alarm_user_id->setSessionValue($this->user_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["telecare_alarm_grid"])) $GLOBALS["telecare_alarm_grid"] = new ctelecare_alarm_grid(); // Get detail page object
				$AddRow = $GLOBALS["telecare_alarm_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["telecare_alarm"]->alarm_user_id->setSessionValue(""); // Clear master key if insert failed
			}
			if (in_array("telecare_prescription", $DetailTblVar) && $GLOBALS["telecare_prescription"]->DetailAdd) {
				$GLOBALS["telecare_prescription"]->prescription_user_id->setSessionValue($this->user_id->CurrentValue); // Set master key
				if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid(); // Get detail page object
				$AddRow = $GLOBALS["telecare_prescription_grid"]->GridInsert();
				if (!$AddRow)
					$GLOBALS["telecare_prescription"]->prescription_user_id->setSessionValue(""); // Clear master key if insert failed
			}
		}

		// Commit/Rollback transaction
		if ($this->getCurrentDetailTable() <> "") {
			if ($AddRow) {
				$conn->CommitTrans(); // Commit transaction
			} else {
				$conn->RollbackTrans(); // Rollback transaction
			}
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
			$this->WriteAuditTrailOnAdd($rsnew);
		}
		return $AddRow;
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("telecare_call", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_call_grid"]))
					$GLOBALS["telecare_call_grid"] = new ctelecare_call_grid;
				if ($GLOBALS["telecare_call_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["telecare_call_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["telecare_call_grid"]->CurrentMode = "add";
					$GLOBALS["telecare_call_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["telecare_call_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_call_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_call_grid"]->call_user_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_call_grid"]->call_user_id->CurrentValue = $this->user_id->CurrentValue;
					$GLOBALS["telecare_call_grid"]->call_user_id->setSessionValue($GLOBALS["telecare_call_grid"]->call_user_id->CurrentValue);
				}
			}
			if (in_array("telecare_alarm", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_alarm_grid"]))
					$GLOBALS["telecare_alarm_grid"] = new ctelecare_alarm_grid;
				if ($GLOBALS["telecare_alarm_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["telecare_alarm_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["telecare_alarm_grid"]->CurrentMode = "add";
					$GLOBALS["telecare_alarm_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["telecare_alarm_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_alarm_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_alarm_grid"]->alarm_user_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_alarm_grid"]->alarm_user_id->CurrentValue = $this->user_id->CurrentValue;
					$GLOBALS["telecare_alarm_grid"]->alarm_user_id->setSessionValue($GLOBALS["telecare_alarm_grid"]->alarm_user_id->CurrentValue);
				}
			}
			if (in_array("telecare_prescription", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_prescription_grid"]))
					$GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;
				if ($GLOBALS["telecare_prescription_grid"]->DetailAdd) {
					if ($this->CopyRecord)
						$GLOBALS["telecare_prescription_grid"]->CurrentMode = "copy";
					else
						$GLOBALS["telecare_prescription_grid"]->CurrentMode = "add";
					$GLOBALS["telecare_prescription_grid"]->CurrentAction = "gridadd";

					// Save current master table to detail table
					$GLOBALS["telecare_prescription_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_prescription_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_prescription_grid"]->prescription_user_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_prescription_grid"]->prescription_user_id->CurrentValue = $this->user_id->CurrentValue;
					$GLOBALS["telecare_prescription_grid"]->prescription_user_id->setSessionValue($GLOBALS["telecare_prescription_grid"]->prescription_user_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_userlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$pages->Add(4);
		$this->MultiPages = $pages;
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'telecare_user';
		$usr = CurrentUserID();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (add page)
	function WriteAuditTrailOnAdd(&$rs) {
		global $Language;
		if (!$this->AuditTrailOnAdd) return;
		$table = 'telecare_user';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['user_id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
		$usr = CurrentUserID();
		foreach (array_keys($rs) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldHtmlTag == "PASSWORD") {
					$newvalue = $Language->Phrase("PasswordMask"); // Password Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$newvalue = $rs[$fldname];
					else
						$newvalue = "[MEMO]"; // Memo Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$newvalue = "[XML]"; // XML Field
				} else {
					$newvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $usr, "A", $table, $fldname, $key, "", $newvalue);
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_user_add)) $telecare_user_add = new ctelecare_user_add();

// Page init
$telecare_user_add->Page_Init();

// Page main
$telecare_user_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_user_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_useradd = new ew_Form("ftelecare_useradd", "add");

// Validate form
ftelecare_useradd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_user_surname");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_surname->FldCaption(), $telecare_user->user_surname->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_name->FldCaption(), $telecare_user->user_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_gender");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_gender->FldCaption(), $telecare_user->user_gender->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_born");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_user->user_born->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_user_modules[]");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_modules->FldCaption(), $telecare_user->user_modules->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_doctor_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_doctor_id->FldCaption(), $telecare_user->user_doctor_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_is_active");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_user->user_is_active->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_user_medical_history");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_medical_history->FldCaption(), $telecare_user->user_medical_history->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_address");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_address->FldCaption(), $telecare_user->user_address->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_region_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_region_id->FldCaption(), $telecare_user->user_region_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_province_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_province_id->FldCaption(), $telecare_user->user_province_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_city_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_city_id->FldCaption(), $telecare_user->user_city_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_email");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_email->FldCaption(), $telecare_user->user_email->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_email");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_user->user_email->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_user_language");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_user->user_language->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_user_last_update");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_user->user_last_update->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_user_level");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_user->user_level->FldCaption(), $telecare_user->user_level->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_user_level");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_user->user_level->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_useradd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_useradd.ValidateRequired = true;
<?php } else { ?>
ftelecare_useradd.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_useradd.MultiPage = new ew_MultiPage("ftelecare_useradd");

// Dynamic selection lists
ftelecare_useradd.Lists["x_user_gender"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_useradd.Lists["x_user_gender"].Options = <?php echo json_encode($telecare_user->user_gender->Options()) ?>;
ftelecare_useradd.Lists["x_user_modules[]"] = {"LinkField":"x_invalidity_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_invalidity_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_useradd.Lists["x_user_doctor_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_useradd.Lists["x_user_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_user_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_useradd.Lists["x_user_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":["x_user_region_id"],"ChildFields":["x_user_city_id"],"FilterFields":["x_province_region_id"],"Options":[],"Template":""};
ftelecare_useradd.Lists["x_user_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":["x_user_province_id"],"ChildFields":[],"FilterFields":["x_city_province_id"],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_user_add->ShowPageHeader(); ?>
<?php
$telecare_user_add->ShowMessage();
?>
<form name="ftelecare_useradd" id="ftelecare_useradd" class="<?php echo $telecare_user_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_user_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_user_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_user">
<input type="hidden" name="a_add" id="a_add" value="A">
<div class="ewMultiPage">
<div class="tabbable" id="telecare_user_add">
	<ul class="nav<?php echo $telecare_user_add->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_user_add->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_user1" data-toggle="tab"><?php echo $telecare_user->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_user_add->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_user2" data-toggle="tab"><?php echo $telecare_user->PageCaption(2) ?></a></li>
		<li<?php echo $telecare_user_add->MultiPages->TabStyle("3") ?>><a href="#tab_telecare_user3" data-toggle="tab"><?php echo $telecare_user->PageCaption(3) ?></a></li>
		<li<?php echo $telecare_user_add->MultiPages->TabStyle("4") ?>><a href="#tab_telecare_user4" data-toggle="tab"><?php echo $telecare_user->PageCaption(4) ?></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane<?php echo $telecare_user_add->MultiPages->PageStyle("1") ?>" id="tab_telecare_user1">
<div>
<?php if ($telecare_user->user_surname->Visible) { // user_surname ?>
	<div id="r_user_surname" class="form-group">
		<label id="elh_telecare_user_user_surname" for="x_user_surname" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_surname->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_surname->CellAttributes() ?>>
<span id="el_telecare_user_user_surname">
<input type="text" data-table="telecare_user" data-field="x_user_surname" data-page="1" name="x_user_surname" id="x_user_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_surname->EditValue ?>"<?php echo $telecare_user->user_surname->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_surname->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_name->Visible) { // user_name ?>
	<div id="r_user_name" class="form-group">
		<label id="elh_telecare_user_user_name" for="x_user_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_name->CellAttributes() ?>>
<span id="el_telecare_user_user_name">
<input type="text" data-table="telecare_user" data-field="x_user_name" data-page="1" name="x_user_name" id="x_user_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_name->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_name->EditValue ?>"<?php echo $telecare_user->user_name->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_gender->Visible) { // user_gender ?>
	<div id="r_user_gender" class="form-group">
		<label id="elh_telecare_user_user_gender" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_gender->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_gender->CellAttributes() ?>>
<span id="el_telecare_user_user_gender">
<div id="tp_x_user_gender" class="ewTemplate"><input type="radio" data-table="telecare_user" data-field="x_user_gender" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_gender->DisplayValueSeparator) ? json_encode($telecare_user->user_gender->DisplayValueSeparator) : $telecare_user->user_gender->DisplayValueSeparator) ?>" name="x_user_gender" id="x_user_gender" value="{value}"<?php echo $telecare_user->user_gender->EditAttributes() ?>></div>
<div id="dsl_x_user_gender" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_user->user_gender->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_user->user_gender->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_user" data-field="x_user_gender" data-page="1" name="x_user_gender" id="x_user_gender_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_user->user_gender->EditAttributes() ?>><?php echo $telecare_user->user_gender->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_gender->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_user" data-field="x_user_gender" data-page="1" name="x_user_gender" id="x_user_gender_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_user->user_gender->CurrentValue) ?>" checked<?php echo $telecare_user->user_gender->EditAttributes() ?>><?php echo $telecare_user->user_gender->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_user->user_gender->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_born->Visible) { // user_born ?>
	<div id="r_user_born" class="form-group">
		<label id="elh_telecare_user_user_born" for="x_user_born" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_born->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_born->CellAttributes() ?>>
<span id="el_telecare_user_user_born">
<input type="text" data-table="telecare_user" data-field="x_user_born" data-page="1" data-format="7" name="x_user_born" id="x_user_born" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_born->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_born->EditValue ?>"<?php echo $telecare_user->user_born->EditAttributes() ?>>
<?php if (!$telecare_user->user_born->ReadOnly && !$telecare_user->user_born->Disabled && !isset($telecare_user->user_born->EditAttrs["readonly"]) && !isset($telecare_user->user_born->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_useradd", "x_user_born", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_user->user_born->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_modules->Visible) { // user_modules ?>
	<div id="r_user_modules" class="form-group">
		<label id="elh_telecare_user_user_modules" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_modules->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_modules->CellAttributes() ?>>
<span id="el_telecare_user_user_modules">
<div id="tp_x_user_modules" class="ewTemplate"><input type="checkbox" data-table="telecare_user" data-field="x_user_modules" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_modules->DisplayValueSeparator) ? json_encode($telecare_user->user_modules->DisplayValueSeparator) : $telecare_user->user_modules->DisplayValueSeparator) ?>" name="x_user_modules[]" id="x_user_modules[]" value="{value}"<?php echo $telecare_user->user_modules->EditAttributes() ?>></div>
<div id="dsl_x_user_modules" data-repeatcolumn="10" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_user->user_modules->EditValue;
if (is_array($arwrk)) {
	$armultiwrk = (strval($telecare_user->user_modules->CurrentValue) <> "") ? explode(",", strval($telecare_user->user_modules->CurrentValue)) : array();
	$cnt = count($armultiwrk);
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = "";
		for ($ari = 0; $ari < $cnt; $ari++) {
			if (ew_SameStr($arwrk[$rowcntwrk][0], $armultiwrk[$ari]) && !is_null($armultiwrk[$ari])) {
				$armultiwrk[$ari] = NULL; // Marked for removal
				$selwrk = " checked";
				if ($selwrk <> "") $emptywrk = FALSE;
				break;
			}
		}
		if ($selwrk <> "") {
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 10, 1) ?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_user" data-field="x_user_modules" data-page="1" name="x_user_modules[]" id="x_user_modules_<?php echo $rowcntwrk ?>[]" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_user->user_modules->EditAttributes() ?>><?php echo $telecare_user->user_modules->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 10, 2) ?>
<?php
		}
	}
	for ($ari = 0; $ari < $cnt; $ari++) {
		if (!is_null($armultiwrk[$ari])) {
?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_user" data-field="x_user_modules" data-page="1" name="x_user_modules[]" value="<?php echo ew_HtmlEncode($armultiwrk[$ari]) ?>" checked<?php echo $telecare_user->user_modules->EditAttributes() ?>><?php echo $armultiwrk[$ari] ?></label>
<?php
		}
	}
}
?>
</div></div>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
}
$telecare_user->user_modules->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_modules->LookupFilters += array("f0" => "`invalidity_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_modules, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
if ($sSqlWrk <> "") $telecare_user->user_modules->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_modules" id="s_x_user_modules" value="<?php echo $telecare_user->user_modules->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_user->user_modules->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_doctor_id->Visible) { // user_doctor_id ?>
	<div id="r_user_doctor_id" class="form-group">
		<label id="elh_telecare_user_user_doctor_id" for="x_user_doctor_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_doctor_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_doctor_id->CellAttributes() ?>>
<span id="el_telecare_user_user_doctor_id">
<select data-table="telecare_user" data-field="x_user_doctor_id" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_doctor_id->DisplayValueSeparator) ? json_encode($telecare_user->user_doctor_id->DisplayValueSeparator) : $telecare_user->user_doctor_id->DisplayValueSeparator) ?>" id="x_user_doctor_id" name="x_user_doctor_id"<?php echo $telecare_user->user_doctor_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_doctor_id->EditValue)) {
	$arwrk = $telecare_user->user_doctor_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_doctor_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_doctor_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_doctor_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_doctor_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_doctor_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = " `admin_level` = 2 ";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_user"]->UserIDAllow("add")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_user->user_doctor_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_doctor_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_doctor_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_user->user_doctor_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_doctor_id" id="s_x_user_doctor_id" value="<?php echo $telecare_user->user_doctor_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_user->user_doctor_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_is_active->Visible) { // user_is_active ?>
	<div id="r_user_is_active" class="form-group">
		<label id="elh_telecare_user_user_is_active" for="x_user_is_active" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_is_active->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_is_active->CellAttributes() ?>>
<span id="el_telecare_user_user_is_active">
<input type="text" data-table="telecare_user" data-field="x_user_is_active" data-page="1" name="x_user_is_active" id="x_user_is_active" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_is_active->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_is_active->EditValue ?>"<?php echo $telecare_user->user_is_active->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_is_active->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_medical_history->Visible) { // user_medical_history ?>
	<div id="r_user_medical_history" class="form-group">
		<label id="elh_telecare_user_user_medical_history" for="x_user_medical_history" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_medical_history->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_medical_history->CellAttributes() ?>>
<span id="el_telecare_user_user_medical_history">
<textarea data-table="telecare_user" data-field="x_user_medical_history" data-page="1" name="x_user_medical_history" id="x_user_medical_history" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_medical_history->getPlaceHolder()) ?>"<?php echo $telecare_user->user_medical_history->EditAttributes() ?>><?php echo $telecare_user->user_medical_history->EditValue ?></textarea>
</span>
<?php echo $telecare_user->user_medical_history->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_level->Visible) { // user_level ?>
	<div id="r_user_level" class="form-group">
		<label id="elh_telecare_user_user_level" for="x_user_level" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_level->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_level->CellAttributes() ?>>
<span id="el_telecare_user_user_level">
<input type="text" data-table="telecare_user" data-field="x_user_level" data-page="1" name="x_user_level" id="x_user_level" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_level->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_level->EditValue ?>"<?php echo $telecare_user->user_level->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_level->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_user_add->MultiPages->PageStyle("2") ?>" id="tab_telecare_user2">
<div>
<?php if ($telecare_user->user_address->Visible) { // user_address ?>
	<div id="r_user_address" class="form-group">
		<label id="elh_telecare_user_user_address" for="x_user_address" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_address->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_address->CellAttributes() ?>>
<span id="el_telecare_user_user_address">
<input type="text" data-table="telecare_user" data-field="x_user_address" data-page="2" name="x_user_address" id="x_user_address" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_address->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_address->EditValue ?>"<?php echo $telecare_user->user_address->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_address->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_region_id->Visible) { // user_region_id ?>
	<div id="r_user_region_id" class="form-group">
		<label id="elh_telecare_user_user_region_id" for="x_user_region_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_region_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_region_id->CellAttributes() ?>>
<span id="el_telecare_user_user_region_id">
<?php $telecare_user->user_region_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_user->user_region_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_user" data-field="x_user_region_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_region_id->DisplayValueSeparator) ? json_encode($telecare_user->user_region_id->DisplayValueSeparator) : $telecare_user->user_region_id->DisplayValueSeparator) ?>" id="x_user_region_id" name="x_user_region_id"<?php echo $telecare_user->user_region_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_region_id->EditValue)) {
	$arwrk = $telecare_user->user_region_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_region_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_region_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_region_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_region_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_region_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
}
$telecare_user->user_region_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_region_id->LookupFilters += array("f0" => "`region_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_region_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `region_name` ASC";
if ($sSqlWrk <> "") $telecare_user->user_region_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_region_id" id="s_x_user_region_id" value="<?php echo $telecare_user->user_region_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_user->user_region_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_province_id->Visible) { // user_province_id ?>
	<div id="r_user_province_id" class="form-group">
		<label id="elh_telecare_user_user_province_id" for="x_user_province_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_province_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_province_id->CellAttributes() ?>>
<span id="el_telecare_user_user_province_id">
<?php $telecare_user->user_province_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_user->user_province_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_user" data-field="x_user_province_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_province_id->DisplayValueSeparator) ? json_encode($telecare_user->user_province_id->DisplayValueSeparator) : $telecare_user->user_province_id->DisplayValueSeparator) ?>" id="x_user_province_id" name="x_user_province_id"<?php echo $telecare_user->user_province_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_province_id->EditValue)) {
	$arwrk = $telecare_user->user_province_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_province_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_province_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_province_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_province_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_province_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_user->user_province_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_province_id->LookupFilters += array("f0" => "`province_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_user->user_province_id->LookupFilters += array("f1" => "`province_region_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_province_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `province_name` ASC";
if ($sSqlWrk <> "") $telecare_user->user_province_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_province_id" id="s_x_user_province_id" value="<?php echo $telecare_user->user_province_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_user->user_province_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_city_id->Visible) { // user_city_id ?>
	<div id="r_user_city_id" class="form-group">
		<label id="elh_telecare_user_user_city_id" for="x_user_city_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_city_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_city_id->CellAttributes() ?>>
<span id="el_telecare_user_user_city_id">
<select data-table="telecare_user" data-field="x_user_city_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_user->user_city_id->DisplayValueSeparator) ? json_encode($telecare_user->user_city_id->DisplayValueSeparator) : $telecare_user->user_city_id->DisplayValueSeparator) ?>" id="x_user_city_id" name="x_user_city_id"<?php echo $telecare_user->user_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_user->user_city_id->EditValue)) {
	$arwrk = $telecare_user->user_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_user->user_city_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_user->user_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_user->user_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_user->user_city_id->CurrentValue) ?>" selected><?php echo $telecare_user->user_city_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_user->user_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_user->user_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_user->user_city_id->LookupFilters += array("f1" => "`city_province_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_user->Lookup_Selecting($telecare_user->user_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `city_name` ASC";
if ($sSqlWrk <> "") $telecare_user->user_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_user_city_id" id="s_x_user_city_id" value="<?php echo $telecare_user->user_city_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_user->user_city_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_email->Visible) { // user_email ?>
	<div id="r_user_email" class="form-group">
		<label id="elh_telecare_user_user_email" for="x_user_email" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_email->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_email->CellAttributes() ?>>
<span id="el_telecare_user_user_email">
<input type="text" data-table="telecare_user" data-field="x_user_email" data-page="2" name="x_user_email" id="x_user_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_email->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_email->EditValue ?>"<?php echo $telecare_user->user_email->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_phone->Visible) { // user_phone ?>
	<div id="r_user_phone" class="form-group">
		<label id="elh_telecare_user_user_phone" for="x_user_phone" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_phone->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_phone->CellAttributes() ?>>
<span id="el_telecare_user_user_phone">
<input type="text" data-table="telecare_user" data-field="x_user_phone" data-page="2" name="x_user_phone" id="x_user_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_phone->EditValue ?>"<?php echo $telecare_user->user_phone->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_phone->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_language->Visible) { // user_language ?>
	<div id="r_user_language" class="form-group">
		<label id="elh_telecare_user_user_language" for="x_user_language" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_language->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_language->CellAttributes() ?>>
<span id="el_telecare_user_user_language">
<input type="text" data-table="telecare_user" data-field="x_user_language" data-page="2" name="x_user_language" id="x_user_language" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_language->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_language->EditValue ?>"<?php echo $telecare_user->user_language->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_language->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_user_add->MultiPages->PageStyle("3") ?>" id="tab_telecare_user3">
<div>
<?php if ($telecare_user->user_username->Visible) { // user_username ?>
	<div id="r_user_username" class="form-group">
		<label id="elh_telecare_user_user_username" for="x_user_username" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_username->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_username->CellAttributes() ?>>
<span id="el_telecare_user_user_username">
<input type="text" data-table="telecare_user" data-field="x_user_username" data-page="3" name="x_user_username" id="x_user_username" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_username->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_username->EditValue ?>"<?php echo $telecare_user->user_username->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_username->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_password->Visible) { // user_password ?>
	<div id="r_user_password" class="form-group">
		<label id="elh_telecare_user_user_password" for="x_user_password" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_password->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_password->CellAttributes() ?>>
<span id="el_telecare_user_user_password">
<input type="password" data-field="x_user_password" name="x_user_password" id="x_user_password" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_password->getPlaceHolder()) ?>"<?php echo $telecare_user->user_password->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_password->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_user_add->MultiPages->PageStyle("4") ?>" id="tab_telecare_user4">
<div>
<?php if ($telecare_user->user_latitude->Visible) { // user_latitude ?>
	<div id="r_user_latitude" class="form-group">
		<label id="elh_telecare_user_user_latitude" for="x_user_latitude" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_latitude->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_latitude->CellAttributes() ?>>
<span id="el_telecare_user_user_latitude">
<input type="text" data-table="telecare_user" data-field="x_user_latitude" data-page="4" name="x_user_latitude" id="x_user_latitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_latitude->EditValue ?>"<?php echo $telecare_user->user_latitude->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_latitude->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_longitude->Visible) { // user_longitude ?>
	<div id="r_user_longitude" class="form-group">
		<label id="elh_telecare_user_user_longitude" for="x_user_longitude" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_longitude->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_longitude->CellAttributes() ?>>
<span id="el_telecare_user_user_longitude">
<input type="text" data-table="telecare_user" data-field="x_user_longitude" data-page="4" name="x_user_longitude" id="x_user_longitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_longitude->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_longitude->EditValue ?>"<?php echo $telecare_user->user_longitude->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_longitude->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_user->user_last_update->Visible) { // user_last_update ?>
	<div id="r_user_last_update" class="form-group">
		<label id="elh_telecare_user_user_last_update" for="x_user_last_update" class="col-sm-2 control-label ewLabel"><?php echo $telecare_user->user_last_update->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_user->user_last_update->CellAttributes() ?>>
<span id="el_telecare_user_user_last_update">
<input type="text" data-table="telecare_user" data-field="x_user_last_update" data-page="4" data-format="7" name="x_user_last_update" id="x_user_last_update" placeholder="<?php echo ew_HtmlEncode($telecare_user->user_last_update->getPlaceHolder()) ?>" value="<?php echo $telecare_user->user_last_update->EditValue ?>"<?php echo $telecare_user->user_last_update->EditAttributes() ?>>
</span>
<?php echo $telecare_user->user_last_update->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
	</div>
</div>
</div>
<?php
	if (in_array("telecare_call", explode(",", $telecare_user->getCurrentDetailTable())) && $telecare_call->DetailAdd) {
?>
<?php if ($telecare_user->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_call", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_callgrid.php" ?>
<?php } ?>
<?php
	if (in_array("telecare_alarm", explode(",", $telecare_user->getCurrentDetailTable())) && $telecare_alarm->DetailAdd) {
?>
<?php if ($telecare_user->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_alarm", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_alarmgrid.php" ?>
<?php } ?>
<?php
	if (in_array("telecare_prescription", explode(",", $telecare_user->getCurrentDetailTable())) && $telecare_prescription->DetailAdd) {
?>
<?php if ($telecare_user->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_prescription", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_prescriptiongrid.php" ?>
<?php } ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_user_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_useradd.Init();
</script>
<?php
$telecare_user_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_user_add->Page_Terminate();
?>
