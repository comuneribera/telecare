<?php

// apotech_id
// apotech_admin_id
// apotech_surname
// apotech_name
// apotech_company_name
// apotech_company_phone
// apotech_company_email
// apotech_latitude
// apotech_longitude

?>
<?php if ($telecare_apotech->Visible) { ?>
<!-- <h4 class="ewMasterCaption"><?php echo $telecare_apotech->TableCaption() ?></h4> -->
<table id="tbl_telecare_apotechmaster" class="table table-bordered table-striped ewViewTable">
	<tbody>
<?php if ($telecare_apotech->apotech_id->Visible) { // apotech_id ?>
		<tr id="r_apotech_id">
			<td><?php echo $telecare_apotech->apotech_id->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_id->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_id">
<span<?php echo $telecare_apotech->apotech_id->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_admin_id->Visible) { // apotech_admin_id ?>
		<tr id="r_apotech_admin_id">
			<td><?php echo $telecare_apotech->apotech_admin_id->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_admin_id->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_admin_id">
<span<?php echo $telecare_apotech->apotech_admin_id->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_admin_id->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_surname->Visible) { // apotech_surname ?>
		<tr id="r_apotech_surname">
			<td><?php echo $telecare_apotech->apotech_surname->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_surname->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_surname">
<span<?php echo $telecare_apotech->apotech_surname->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_surname->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_name->Visible) { // apotech_name ?>
		<tr id="r_apotech_name">
			<td><?php echo $telecare_apotech->apotech_name->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_name->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_name">
<span<?php echo $telecare_apotech->apotech_name->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_name->Visible) { // apotech_company_name ?>
		<tr id="r_apotech_company_name">
			<td><?php echo $telecare_apotech->apotech_company_name->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_company_name->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_name">
<span<?php echo $telecare_apotech->apotech_company_name->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_name->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_phone->Visible) { // apotech_company_phone ?>
		<tr id="r_apotech_company_phone">
			<td><?php echo $telecare_apotech->apotech_company_phone->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_company_phone->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_phone">
<span<?php echo $telecare_apotech->apotech_company_phone->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_phone->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_email->Visible) { // apotech_company_email ?>
		<tr id="r_apotech_company_email">
			<td><?php echo $telecare_apotech->apotech_company_email->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_company_email->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_email">
<span<?php echo $telecare_apotech->apotech_company_email->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_email->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_latitude->Visible) { // apotech_latitude ?>
		<tr id="r_apotech_latitude">
			<td><?php echo $telecare_apotech->apotech_latitude->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_latitude->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_latitude">
<span<?php echo $telecare_apotech->apotech_latitude->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_latitude->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
<?php if ($telecare_apotech->apotech_longitude->Visible) { // apotech_longitude ?>
		<tr id="r_apotech_longitude">
			<td><?php echo $telecare_apotech->apotech_longitude->FldCaption() ?></td>
			<td<?php echo $telecare_apotech->apotech_longitude->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_longitude">
<span<?php echo $telecare_apotech->apotech_longitude->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_longitude->ListViewValue() ?></span>
</span>
</td>
		</tr>
<?php } ?>
	</tbody>
</table>
<?php } ?>
