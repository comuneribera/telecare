<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_admin_add = NULL; // Initialize page object first

class ctelecare_admin_add extends ctelecare_admin {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_admin';

	// Page object name
	var $PageObjName = 'telecare_admin_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_admin)
		if (!isset($GLOBALS["telecare_admin"]) || get_class($GLOBALS["telecare_admin"]) == "ctelecare_admin") {
			$GLOBALS["telecare_admin"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_admin"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_admin', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_adminlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
			if (strval($Security->CurrentUserID()) == "") {
				$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
				$this->Page_Terminate(ew_GetUrl("telecare_adminlist.php"));
			}
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_admin;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_admin);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;
	var $MultiPages; // Multi pages object

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["admin_id"] != "") {
				$this->admin_id->setQueryStringValue($_GET["admin_id"]);
				$this->setKey("admin_id", $this->admin_id->CurrentValue); // Set up key
			} else {
				$this->setKey("admin_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_adminlist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_adminview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->admin_company_logo_file->Upload->Index = $objForm->Index;
		$this->admin_company_logo_file->Upload->UploadFile();
		$this->admin_company_logo_file->CurrentValue = $this->admin_company_logo_file->Upload->FileName;
		$this->admin_company_logo_size->CurrentValue = $this->admin_company_logo_file->Upload->FileSize;
		$this->admin_company_logo_width->CurrentValue = $this->admin_company_logo_file->Upload->ImageWidth;
		$this->admin_company_logo_height->CurrentValue = $this->admin_company_logo_file->Upload->ImageHeight;
	}

	// Load default values
	function LoadDefaultValues() {
		$this->admin_name->CurrentValue = NULL;
		$this->admin_name->OldValue = $this->admin_name->CurrentValue;
		$this->admin_surname->CurrentValue = NULL;
		$this->admin_surname->OldValue = $this->admin_surname->CurrentValue;
		$this->admin_gender->CurrentValue = 1;
		$this->admin_email->CurrentValue = NULL;
		$this->admin_email->OldValue = $this->admin_email->CurrentValue;
		$this->admin_mobile->CurrentValue = NULL;
		$this->admin_mobile->OldValue = $this->admin_mobile->CurrentValue;
		$this->admin_phone->CurrentValue = NULL;
		$this->admin_phone->OldValue = $this->admin_phone->CurrentValue;
		$this->admin_modules->CurrentValue = NULL;
		$this->admin_modules->OldValue = $this->admin_modules->CurrentValue;
		$this->admin_language->CurrentValue = 0;
		$this->admin_is_active->CurrentValue = 1;
		$this->admin_company_name->CurrentValue = NULL;
		$this->admin_company_name->OldValue = $this->admin_company_name->CurrentValue;
		$this->admin_company_vat->CurrentValue = NULL;
		$this->admin_company_vat->OldValue = $this->admin_company_vat->CurrentValue;
		$this->admin_company_logo_file->Upload->DbValue = NULL;
		$this->admin_company_logo_file->OldValue = $this->admin_company_logo_file->Upload->DbValue;
		$this->admin_company_logo_file->CurrentValue = NULL; // Clear file related field
		$this->admin_company_address->CurrentValue = NULL;
		$this->admin_company_address->OldValue = $this->admin_company_address->CurrentValue;
		$this->admin_company_region_id->CurrentValue = NULL;
		$this->admin_company_region_id->OldValue = $this->admin_company_region_id->CurrentValue;
		$this->admin_company_province_id->CurrentValue = NULL;
		$this->admin_company_province_id->OldValue = $this->admin_company_province_id->CurrentValue;
		$this->admin_company_city_id->CurrentValue = NULL;
		$this->admin_company_city_id->OldValue = $this->admin_company_city_id->CurrentValue;
		$this->admin_company_phone->CurrentValue = NULL;
		$this->admin_company_phone->OldValue = $this->admin_company_phone->CurrentValue;
		$this->admin_company_email->CurrentValue = NULL;
		$this->admin_company_email->OldValue = $this->admin_company_email->CurrentValue;
		$this->admin_company_web->CurrentValue = NULL;
		$this->admin_company_web->OldValue = $this->admin_company_web->CurrentValue;
		$this->admin_contract->CurrentValue = NULL;
		$this->admin_contract->OldValue = $this->admin_contract->CurrentValue;
		$this->admin_expire->CurrentValue = NULL;
		$this->admin_expire->OldValue = $this->admin_expire->CurrentValue;
		$this->admin_company_facebook->CurrentValue = NULL;
		$this->admin_company_facebook->OldValue = $this->admin_company_facebook->CurrentValue;
		$this->admin_company_twitter->CurrentValue = NULL;
		$this->admin_company_twitter->OldValue = $this->admin_company_twitter->CurrentValue;
		$this->admin_company_googleplus->CurrentValue = NULL;
		$this->admin_company_googleplus->OldValue = $this->admin_company_googleplus->CurrentValue;
		$this->admin_company_linkedin->CurrentValue = NULL;
		$this->admin_company_linkedin->OldValue = $this->admin_company_linkedin->CurrentValue;
		$this->admin_link_playstore->CurrentValue = NULL;
		$this->admin_link_playstore->OldValue = $this->admin_link_playstore->CurrentValue;
		$this->admin_link_appstore->CurrentValue = NULL;
		$this->admin_link_appstore->OldValue = $this->admin_link_appstore->CurrentValue;
		$this->admin_link_windowsstore->CurrentValue = NULL;
		$this->admin_link_windowsstore->OldValue = $this->admin_link_windowsstore->CurrentValue;
		$this->admin_level->CurrentValue = NULL;
		$this->admin_level->OldValue = $this->admin_level->CurrentValue;
		$this->admin_username->CurrentValue = NULL;
		$this->admin_username->OldValue = $this->admin_username->CurrentValue;
		$this->admin_password->CurrentValue = NULL;
		$this->admin_password->OldValue = $this->admin_password->CurrentValue;
		$this->admin_last_update->CurrentValue = NULL;
		$this->admin_last_update->OldValue = $this->admin_last_update->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->admin_name->FldIsDetailKey) {
			$this->admin_name->setFormValue($objForm->GetValue("x_admin_name"));
		}
		if (!$this->admin_surname->FldIsDetailKey) {
			$this->admin_surname->setFormValue($objForm->GetValue("x_admin_surname"));
		}
		if (!$this->admin_gender->FldIsDetailKey) {
			$this->admin_gender->setFormValue($objForm->GetValue("x_admin_gender"));
		}
		if (!$this->admin_email->FldIsDetailKey) {
			$this->admin_email->setFormValue($objForm->GetValue("x_admin_email"));
		}
		if (!$this->admin_mobile->FldIsDetailKey) {
			$this->admin_mobile->setFormValue($objForm->GetValue("x_admin_mobile"));
		}
		if (!$this->admin_phone->FldIsDetailKey) {
			$this->admin_phone->setFormValue($objForm->GetValue("x_admin_phone"));
		}
		if (!$this->admin_modules->FldIsDetailKey) {
			$this->admin_modules->setFormValue($objForm->GetValue("x_admin_modules"));
		}
		if (!$this->admin_language->FldIsDetailKey) {
			$this->admin_language->setFormValue($objForm->GetValue("x_admin_language"));
		}
		if (!$this->admin_is_active->FldIsDetailKey) {
			$this->admin_is_active->setFormValue($objForm->GetValue("x_admin_is_active"));
		}
		if (!$this->admin_company_name->FldIsDetailKey) {
			$this->admin_company_name->setFormValue($objForm->GetValue("x_admin_company_name"));
		}
		if (!$this->admin_company_vat->FldIsDetailKey) {
			$this->admin_company_vat->setFormValue($objForm->GetValue("x_admin_company_vat"));
		}
		if (!$this->admin_company_address->FldIsDetailKey) {
			$this->admin_company_address->setFormValue($objForm->GetValue("x_admin_company_address"));
		}
		if (!$this->admin_company_region_id->FldIsDetailKey) {
			$this->admin_company_region_id->setFormValue($objForm->GetValue("x_admin_company_region_id"));
		}
		if (!$this->admin_company_province_id->FldIsDetailKey) {
			$this->admin_company_province_id->setFormValue($objForm->GetValue("x_admin_company_province_id"));
		}
		if (!$this->admin_company_city_id->FldIsDetailKey) {
			$this->admin_company_city_id->setFormValue($objForm->GetValue("x_admin_company_city_id"));
		}
		if (!$this->admin_company_phone->FldIsDetailKey) {
			$this->admin_company_phone->setFormValue($objForm->GetValue("x_admin_company_phone"));
		}
		if (!$this->admin_company_email->FldIsDetailKey) {
			$this->admin_company_email->setFormValue($objForm->GetValue("x_admin_company_email"));
		}
		if (!$this->admin_company_web->FldIsDetailKey) {
			$this->admin_company_web->setFormValue($objForm->GetValue("x_admin_company_web"));
		}
		if (!$this->admin_contract->FldIsDetailKey) {
			$this->admin_contract->setFormValue($objForm->GetValue("x_admin_contract"));
			$this->admin_contract->CurrentValue = ew_UnFormatDateTime($this->admin_contract->CurrentValue, 7);
		}
		if (!$this->admin_expire->FldIsDetailKey) {
			$this->admin_expire->setFormValue($objForm->GetValue("x_admin_expire"));
			$this->admin_expire->CurrentValue = ew_UnFormatDateTime($this->admin_expire->CurrentValue, 7);
		}
		if (!$this->admin_company_facebook->FldIsDetailKey) {
			$this->admin_company_facebook->setFormValue($objForm->GetValue("x_admin_company_facebook"));
		}
		if (!$this->admin_company_twitter->FldIsDetailKey) {
			$this->admin_company_twitter->setFormValue($objForm->GetValue("x_admin_company_twitter"));
		}
		if (!$this->admin_company_googleplus->FldIsDetailKey) {
			$this->admin_company_googleplus->setFormValue($objForm->GetValue("x_admin_company_googleplus"));
		}
		if (!$this->admin_company_linkedin->FldIsDetailKey) {
			$this->admin_company_linkedin->setFormValue($objForm->GetValue("x_admin_company_linkedin"));
		}
		if (!$this->admin_link_playstore->FldIsDetailKey) {
			$this->admin_link_playstore->setFormValue($objForm->GetValue("x_admin_link_playstore"));
		}
		if (!$this->admin_link_appstore->FldIsDetailKey) {
			$this->admin_link_appstore->setFormValue($objForm->GetValue("x_admin_link_appstore"));
		}
		if (!$this->admin_link_windowsstore->FldIsDetailKey) {
			$this->admin_link_windowsstore->setFormValue($objForm->GetValue("x_admin_link_windowsstore"));
		}
		if (!$this->admin_level->FldIsDetailKey) {
			$this->admin_level->setFormValue($objForm->GetValue("x_admin_level"));
		}
		if (!$this->admin_username->FldIsDetailKey) {
			$this->admin_username->setFormValue($objForm->GetValue("x_admin_username"));
		}
		if (!$this->admin_password->FldIsDetailKey) {
			$this->admin_password->setFormValue($objForm->GetValue("x_admin_password"));
		}
		if (!$this->admin_last_update->FldIsDetailKey) {
			$this->admin_last_update->setFormValue($objForm->GetValue("x_admin_last_update"));
			$this->admin_last_update->CurrentValue = ew_UnFormatDateTime($this->admin_last_update->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->admin_name->CurrentValue = $this->admin_name->FormValue;
		$this->admin_surname->CurrentValue = $this->admin_surname->FormValue;
		$this->admin_gender->CurrentValue = $this->admin_gender->FormValue;
		$this->admin_email->CurrentValue = $this->admin_email->FormValue;
		$this->admin_mobile->CurrentValue = $this->admin_mobile->FormValue;
		$this->admin_phone->CurrentValue = $this->admin_phone->FormValue;
		$this->admin_modules->CurrentValue = $this->admin_modules->FormValue;
		$this->admin_language->CurrentValue = $this->admin_language->FormValue;
		$this->admin_is_active->CurrentValue = $this->admin_is_active->FormValue;
		$this->admin_company_name->CurrentValue = $this->admin_company_name->FormValue;
		$this->admin_company_vat->CurrentValue = $this->admin_company_vat->FormValue;
		$this->admin_company_address->CurrentValue = $this->admin_company_address->FormValue;
		$this->admin_company_region_id->CurrentValue = $this->admin_company_region_id->FormValue;
		$this->admin_company_province_id->CurrentValue = $this->admin_company_province_id->FormValue;
		$this->admin_company_city_id->CurrentValue = $this->admin_company_city_id->FormValue;
		$this->admin_company_phone->CurrentValue = $this->admin_company_phone->FormValue;
		$this->admin_company_email->CurrentValue = $this->admin_company_email->FormValue;
		$this->admin_company_web->CurrentValue = $this->admin_company_web->FormValue;
		$this->admin_contract->CurrentValue = $this->admin_contract->FormValue;
		$this->admin_contract->CurrentValue = ew_UnFormatDateTime($this->admin_contract->CurrentValue, 7);
		$this->admin_expire->CurrentValue = $this->admin_expire->FormValue;
		$this->admin_expire->CurrentValue = ew_UnFormatDateTime($this->admin_expire->CurrentValue, 7);
		$this->admin_company_facebook->CurrentValue = $this->admin_company_facebook->FormValue;
		$this->admin_company_twitter->CurrentValue = $this->admin_company_twitter->FormValue;
		$this->admin_company_googleplus->CurrentValue = $this->admin_company_googleplus->FormValue;
		$this->admin_company_linkedin->CurrentValue = $this->admin_company_linkedin->FormValue;
		$this->admin_link_playstore->CurrentValue = $this->admin_link_playstore->FormValue;
		$this->admin_link_appstore->CurrentValue = $this->admin_link_appstore->FormValue;
		$this->admin_link_windowsstore->CurrentValue = $this->admin_link_windowsstore->FormValue;
		$this->admin_level->CurrentValue = $this->admin_level->FormValue;
		$this->admin_username->CurrentValue = $this->admin_username->FormValue;
		$this->admin_password->CurrentValue = $this->admin_password->FormValue;
		$this->admin_last_update->CurrentValue = $this->admin_last_update->FormValue;
		$this->admin_last_update->CurrentValue = ew_UnFormatDateTime($this->admin_last_update->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}

		// Check if valid user id
		if ($res) {
			$res = $this->ShowOptionLink('add');
			if (!$res) {
				$sUserIdMsg = $Language->Phrase("NoPermission");
				$this->setFailureMessage($sUserIdMsg);
			}
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->admin_id->setDbValue($rs->fields('admin_id'));
		$this->admin_parent_id->setDbValue($rs->fields('admin_parent_id'));
		$this->admin_name->setDbValue($rs->fields('admin_name'));
		$this->admin_surname->setDbValue($rs->fields('admin_surname'));
		$this->admin_gender->setDbValue($rs->fields('admin_gender'));
		$this->admin_email->setDbValue($rs->fields('admin_email'));
		$this->admin_mobile->setDbValue($rs->fields('admin_mobile'));
		$this->admin_phone->setDbValue($rs->fields('admin_phone'));
		$this->admin_modules->setDbValue($rs->fields('admin_modules'));
		$this->admin_language->setDbValue($rs->fields('admin_language'));
		$this->admin_is_active->setDbValue($rs->fields('admin_is_active'));
		$this->admin_company_name->setDbValue($rs->fields('admin_company_name'));
		$this->admin_company_vat->setDbValue($rs->fields('admin_company_vat'));
		$this->admin_company_logo_file->Upload->DbValue = $rs->fields('admin_company_logo_file');
		$this->admin_company_logo_file->CurrentValue = $this->admin_company_logo_file->Upload->DbValue;
		$this->admin_company_logo_width->setDbValue($rs->fields('admin_company_logo_width'));
		$this->admin_company_logo_height->setDbValue($rs->fields('admin_company_logo_height'));
		$this->admin_company_logo_size->setDbValue($rs->fields('admin_company_logo_size'));
		$this->admin_company_address->setDbValue($rs->fields('admin_company_address'));
		$this->admin_company_region_id->setDbValue($rs->fields('admin_company_region_id'));
		$this->admin_company_province_id->setDbValue($rs->fields('admin_company_province_id'));
		$this->admin_company_city_id->setDbValue($rs->fields('admin_company_city_id'));
		$this->admin_company_phone->setDbValue($rs->fields('admin_company_phone'));
		$this->admin_company_email->setDbValue($rs->fields('admin_company_email'));
		$this->admin_company_web->setDbValue($rs->fields('admin_company_web'));
		$this->admin_contract->setDbValue($rs->fields('admin_contract'));
		$this->admin_expire->setDbValue($rs->fields('admin_expire'));
		$this->admin_company_facebook->setDbValue($rs->fields('admin_company_facebook'));
		$this->admin_company_twitter->setDbValue($rs->fields('admin_company_twitter'));
		$this->admin_company_googleplus->setDbValue($rs->fields('admin_company_googleplus'));
		$this->admin_company_linkedin->setDbValue($rs->fields('admin_company_linkedin'));
		$this->admin_link_playstore->setDbValue($rs->fields('admin_link_playstore'));
		$this->admin_link_appstore->setDbValue($rs->fields('admin_link_appstore'));
		$this->admin_link_windowsstore->setDbValue($rs->fields('admin_link_windowsstore'));
		$this->admin_level->setDbValue($rs->fields('admin_level'));
		$this->admin_username->setDbValue($rs->fields('admin_username'));
		$this->admin_password->setDbValue($rs->fields('admin_password'));
		$this->admin_company_autohority->setDbValue($rs->fields('admin_company_autohority'));
		$this->admin_auth_token->setDbValue($rs->fields('admin_auth_token'));
		$this->admin_last_update->setDbValue($rs->fields('admin_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->admin_id->DbValue = $row['admin_id'];
		$this->admin_parent_id->DbValue = $row['admin_parent_id'];
		$this->admin_name->DbValue = $row['admin_name'];
		$this->admin_surname->DbValue = $row['admin_surname'];
		$this->admin_gender->DbValue = $row['admin_gender'];
		$this->admin_email->DbValue = $row['admin_email'];
		$this->admin_mobile->DbValue = $row['admin_mobile'];
		$this->admin_phone->DbValue = $row['admin_phone'];
		$this->admin_modules->DbValue = $row['admin_modules'];
		$this->admin_language->DbValue = $row['admin_language'];
		$this->admin_is_active->DbValue = $row['admin_is_active'];
		$this->admin_company_name->DbValue = $row['admin_company_name'];
		$this->admin_company_vat->DbValue = $row['admin_company_vat'];
		$this->admin_company_logo_file->Upload->DbValue = $row['admin_company_logo_file'];
		$this->admin_company_logo_width->DbValue = $row['admin_company_logo_width'];
		$this->admin_company_logo_height->DbValue = $row['admin_company_logo_height'];
		$this->admin_company_logo_size->DbValue = $row['admin_company_logo_size'];
		$this->admin_company_address->DbValue = $row['admin_company_address'];
		$this->admin_company_region_id->DbValue = $row['admin_company_region_id'];
		$this->admin_company_province_id->DbValue = $row['admin_company_province_id'];
		$this->admin_company_city_id->DbValue = $row['admin_company_city_id'];
		$this->admin_company_phone->DbValue = $row['admin_company_phone'];
		$this->admin_company_email->DbValue = $row['admin_company_email'];
		$this->admin_company_web->DbValue = $row['admin_company_web'];
		$this->admin_contract->DbValue = $row['admin_contract'];
		$this->admin_expire->DbValue = $row['admin_expire'];
		$this->admin_company_facebook->DbValue = $row['admin_company_facebook'];
		$this->admin_company_twitter->DbValue = $row['admin_company_twitter'];
		$this->admin_company_googleplus->DbValue = $row['admin_company_googleplus'];
		$this->admin_company_linkedin->DbValue = $row['admin_company_linkedin'];
		$this->admin_link_playstore->DbValue = $row['admin_link_playstore'];
		$this->admin_link_appstore->DbValue = $row['admin_link_appstore'];
		$this->admin_link_windowsstore->DbValue = $row['admin_link_windowsstore'];
		$this->admin_level->DbValue = $row['admin_level'];
		$this->admin_username->DbValue = $row['admin_username'];
		$this->admin_password->DbValue = $row['admin_password'];
		$this->admin_company_autohority->DbValue = $row['admin_company_autohority'];
		$this->admin_auth_token->DbValue = $row['admin_auth_token'];
		$this->admin_last_update->DbValue = $row['admin_last_update'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("admin_id")) <> "")
			$this->admin_id->CurrentValue = $this->getKey("admin_id"); // admin_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// admin_id
		// admin_parent_id
		// admin_name
		// admin_surname
		// admin_gender
		// admin_email
		// admin_mobile
		// admin_phone
		// admin_modules
		// admin_language
		// admin_is_active
		// admin_company_name
		// admin_company_vat
		// admin_company_logo_file
		// admin_company_logo_width
		// admin_company_logo_height
		// admin_company_logo_size
		// admin_company_address
		// admin_company_region_id
		// admin_company_province_id
		// admin_company_city_id
		// admin_company_phone
		// admin_company_email
		// admin_company_web
		// admin_contract
		// admin_expire
		// admin_company_facebook
		// admin_company_twitter
		// admin_company_googleplus
		// admin_company_linkedin
		// admin_link_playstore
		// admin_link_appstore
		// admin_link_windowsstore
		// admin_level
		// admin_username
		// admin_password
		// admin_company_autohority
		// admin_auth_token
		// admin_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// admin_id
		$this->admin_id->ViewValue = $this->admin_id->CurrentValue;
		$this->admin_id->ViewCustomAttributes = "";

		// admin_name
		$this->admin_name->ViewValue = $this->admin_name->CurrentValue;
		$this->admin_name->ViewCustomAttributes = "";

		// admin_surname
		$this->admin_surname->ViewValue = $this->admin_surname->CurrentValue;
		$this->admin_surname->ViewCustomAttributes = "";

		// admin_gender
		if (strval($this->admin_gender->CurrentValue) <> "") {
			$this->admin_gender->ViewValue = $this->admin_gender->OptionCaption($this->admin_gender->CurrentValue);
		} else {
			$this->admin_gender->ViewValue = NULL;
		}
		$this->admin_gender->ViewCustomAttributes = "";

		// admin_email
		$this->admin_email->ViewValue = $this->admin_email->CurrentValue;
		$this->admin_email->ViewCustomAttributes = "";

		// admin_mobile
		$this->admin_mobile->ViewValue = $this->admin_mobile->CurrentValue;
		$this->admin_mobile->ViewCustomAttributes = "";

		// admin_phone
		$this->admin_phone->ViewValue = $this->admin_phone->CurrentValue;
		$this->admin_phone->ViewCustomAttributes = "";

		// admin_modules
		if (strval($this->admin_modules->CurrentValue) <> "") {
			$arwrk = explode(",", $this->admin_modules->CurrentValue);
			$sFilterWrk = "";
			foreach ($arwrk as $wrk) {
				if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
				$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
			}
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_modules, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->admin_modules->ViewValue = "";
				$ari = 0;
				while (!$rswrk->EOF) {
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$this->admin_modules->ViewValue .= $this->admin_modules->DisplayValue($arwrk);
					$rswrk->MoveNext();
					if (!$rswrk->EOF) $this->admin_modules->ViewValue .= ew_ViewOptionSeparator($ari); // Separate Options
					$ari++;
				}
				$rswrk->Close();
			} else {
				$this->admin_modules->ViewValue = $this->admin_modules->CurrentValue;
			}
		} else {
			$this->admin_modules->ViewValue = NULL;
		}
		$this->admin_modules->ViewCustomAttributes = "";

		// admin_language
		if (strval($this->admin_language->CurrentValue) <> "") {
			$this->admin_language->ViewValue = $this->admin_language->OptionCaption($this->admin_language->CurrentValue);
		} else {
			$this->admin_language->ViewValue = NULL;
		}
		$this->admin_language->ViewCustomAttributes = "";

		// admin_is_active
		if (strval($this->admin_is_active->CurrentValue) <> "") {
			$this->admin_is_active->ViewValue = $this->admin_is_active->OptionCaption($this->admin_is_active->CurrentValue);
		} else {
			$this->admin_is_active->ViewValue = NULL;
		}
		$this->admin_is_active->ViewCustomAttributes = "";

		// admin_company_name
		$this->admin_company_name->ViewValue = $this->admin_company_name->CurrentValue;
		$this->admin_company_name->ViewCustomAttributes = "";

		// admin_company_vat
		$this->admin_company_vat->ViewValue = $this->admin_company_vat->CurrentValue;
		$this->admin_company_vat->ViewCustomAttributes = "";

		// admin_company_logo_file
		if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
			$this->admin_company_logo_file->ImageWidth = 100;
			$this->admin_company_logo_file->ImageHeight = 0;
			$this->admin_company_logo_file->ImageAlt = $this->admin_company_logo_file->FldAlt();
			$this->admin_company_logo_file->ViewValue = $this->admin_company_logo_file->Upload->DbValue;
		} else {
			$this->admin_company_logo_file->ViewValue = "";
		}
		$this->admin_company_logo_file->ViewCustomAttributes = "";

		// admin_company_address
		$this->admin_company_address->ViewValue = $this->admin_company_address->CurrentValue;
		$this->admin_company_address->ViewCustomAttributes = "";

		// admin_company_region_id
		if (strval($this->admin_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->admin_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_region_id->ViewValue = $this->admin_company_region_id->CurrentValue;
			}
		} else {
			$this->admin_company_region_id->ViewValue = NULL;
		}
		$this->admin_company_region_id->ViewCustomAttributes = "";

		// admin_company_province_id
		if (strval($this->admin_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->admin_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_province_id->ViewValue = $this->admin_company_province_id->CurrentValue;
			}
		} else {
			$this->admin_company_province_id->ViewValue = NULL;
		}
		$this->admin_company_province_id->ViewCustomAttributes = "";

		// admin_company_city_id
		if (strval($this->admin_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->admin_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->admin_company_city_id->ViewValue = $this->admin_company_city_id->CurrentValue;
			}
		} else {
			$this->admin_company_city_id->ViewValue = NULL;
		}
		$this->admin_company_city_id->ViewCustomAttributes = "";

		// admin_company_phone
		$this->admin_company_phone->ViewValue = $this->admin_company_phone->CurrentValue;
		$this->admin_company_phone->ViewCustomAttributes = "";

		// admin_company_email
		$this->admin_company_email->ViewValue = $this->admin_company_email->CurrentValue;
		$this->admin_company_email->ViewCustomAttributes = "";

		// admin_company_web
		$this->admin_company_web->ViewValue = $this->admin_company_web->CurrentValue;
		$this->admin_company_web->ViewCustomAttributes = "";

		// admin_contract
		$this->admin_contract->ViewValue = $this->admin_contract->CurrentValue;
		$this->admin_contract->ViewValue = ew_FormatDateTime($this->admin_contract->ViewValue, 7);
		$this->admin_contract->ViewCustomAttributes = "";

		// admin_expire
		$this->admin_expire->ViewValue = $this->admin_expire->CurrentValue;
		$this->admin_expire->ViewValue = ew_FormatDateTime($this->admin_expire->ViewValue, 7);
		$this->admin_expire->ViewCustomAttributes = "";

		// admin_company_facebook
		$this->admin_company_facebook->ViewValue = $this->admin_company_facebook->CurrentValue;
		$this->admin_company_facebook->ViewCustomAttributes = "";

		// admin_company_twitter
		$this->admin_company_twitter->ViewValue = $this->admin_company_twitter->CurrentValue;
		$this->admin_company_twitter->ViewCustomAttributes = "";

		// admin_company_googleplus
		$this->admin_company_googleplus->ViewValue = $this->admin_company_googleplus->CurrentValue;
		$this->admin_company_googleplus->ViewCustomAttributes = "";

		// admin_company_linkedin
		$this->admin_company_linkedin->ViewValue = $this->admin_company_linkedin->CurrentValue;
		$this->admin_company_linkedin->ViewCustomAttributes = "";

		// admin_link_playstore
		$this->admin_link_playstore->ViewValue = $this->admin_link_playstore->CurrentValue;
		$this->admin_link_playstore->ViewCustomAttributes = "";

		// admin_link_appstore
		$this->admin_link_appstore->ViewValue = $this->admin_link_appstore->CurrentValue;
		$this->admin_link_appstore->ViewCustomAttributes = "";

		// admin_link_windowsstore
		$this->admin_link_windowsstore->ViewValue = $this->admin_link_windowsstore->CurrentValue;
		$this->admin_link_windowsstore->ViewCustomAttributes = "";

		// admin_level
		if ($Security->CanAdmin()) { // System admin
		if (strval($this->admin_level->CurrentValue) <> "") {
			$this->admin_level->ViewValue = $this->admin_level->OptionCaption($this->admin_level->CurrentValue);
		} else {
			$this->admin_level->ViewValue = NULL;
		}
		} else {
			$this->admin_level->ViewValue = $Language->Phrase("PasswordMask");
		}
		$this->admin_level->ViewCustomAttributes = "";

		// admin_username
		$this->admin_username->ViewValue = $this->admin_username->CurrentValue;
		$this->admin_username->ViewCustomAttributes = "";

		// admin_password
		$this->admin_password->ViewValue = $Language->Phrase("PasswordMask");
		$this->admin_password->ViewCustomAttributes = "";

		// admin_last_update
		$this->admin_last_update->ViewValue = $this->admin_last_update->CurrentValue;
		$this->admin_last_update->ViewValue = ew_FormatDateTime($this->admin_last_update->ViewValue, 7);
		$this->admin_last_update->ViewCustomAttributes = "";

			// admin_name
			$this->admin_name->LinkCustomAttributes = "";
			$this->admin_name->HrefValue = "";
			$this->admin_name->TooltipValue = "";

			// admin_surname
			$this->admin_surname->LinkCustomAttributes = "";
			$this->admin_surname->HrefValue = "";
			$this->admin_surname->TooltipValue = "";

			// admin_gender
			$this->admin_gender->LinkCustomAttributes = "";
			$this->admin_gender->HrefValue = "";
			$this->admin_gender->TooltipValue = "";

			// admin_email
			$this->admin_email->LinkCustomAttributes = "";
			$this->admin_email->HrefValue = "";
			$this->admin_email->TooltipValue = "";

			// admin_mobile
			$this->admin_mobile->LinkCustomAttributes = "";
			$this->admin_mobile->HrefValue = "";
			$this->admin_mobile->TooltipValue = "";

			// admin_phone
			$this->admin_phone->LinkCustomAttributes = "";
			$this->admin_phone->HrefValue = "";
			$this->admin_phone->TooltipValue = "";

			// admin_modules
			$this->admin_modules->LinkCustomAttributes = "";
			$this->admin_modules->HrefValue = "";
			$this->admin_modules->TooltipValue = "";

			// admin_language
			$this->admin_language->LinkCustomAttributes = "";
			$this->admin_language->HrefValue = "";
			$this->admin_language->TooltipValue = "";

			// admin_is_active
			$this->admin_is_active->LinkCustomAttributes = "";
			$this->admin_is_active->HrefValue = "";
			$this->admin_is_active->TooltipValue = "";

			// admin_company_name
			$this->admin_company_name->LinkCustomAttributes = "";
			$this->admin_company_name->HrefValue = "";
			$this->admin_company_name->TooltipValue = "";

			// admin_company_vat
			$this->admin_company_vat->LinkCustomAttributes = "";
			$this->admin_company_vat->HrefValue = "";
			$this->admin_company_vat->TooltipValue = "";

			// admin_company_logo_file
			$this->admin_company_logo_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
				$this->admin_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->admin_company_logo_file, $this->admin_company_logo_file->Upload->DbValue); // Add prefix/suffix
				$this->admin_company_logo_file->LinkAttrs["target"] = ""; // Add target
				if ($this->Export <> "") $this->admin_company_logo_file->HrefValue = ew_ConvertFullUrl($this->admin_company_logo_file->HrefValue);
			} else {
				$this->admin_company_logo_file->HrefValue = "";
			}
			$this->admin_company_logo_file->HrefValue2 = $this->admin_company_logo_file->UploadPath . $this->admin_company_logo_file->Upload->DbValue;
			$this->admin_company_logo_file->TooltipValue = "";
			if ($this->admin_company_logo_file->UseColorbox) {
				$this->admin_company_logo_file->LinkAttrs["title"] = $Language->Phrase("ViewImageGallery");
				$this->admin_company_logo_file->LinkAttrs["data-rel"] = "telecare_admin_x_admin_company_logo_file";

				//$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox ewTooltip img-thumbnail";
				//$this->admin_company_logo_file->LinkAttrs["data-placement"] = "bottom";
				//$this->admin_company_logo_file->LinkAttrs["data-container"] = "body";

				$this->admin_company_logo_file->LinkAttrs["class"] = "ewLightbox img-thumbnail";
			}

			// admin_company_address
			$this->admin_company_address->LinkCustomAttributes = "";
			$this->admin_company_address->HrefValue = "";
			$this->admin_company_address->TooltipValue = "";

			// admin_company_region_id
			$this->admin_company_region_id->LinkCustomAttributes = "";
			$this->admin_company_region_id->HrefValue = "";
			$this->admin_company_region_id->TooltipValue = "";

			// admin_company_province_id
			$this->admin_company_province_id->LinkCustomAttributes = "";
			$this->admin_company_province_id->HrefValue = "";
			$this->admin_company_province_id->TooltipValue = "";

			// admin_company_city_id
			$this->admin_company_city_id->LinkCustomAttributes = "";
			$this->admin_company_city_id->HrefValue = "";
			$this->admin_company_city_id->TooltipValue = "";

			// admin_company_phone
			$this->admin_company_phone->LinkCustomAttributes = "";
			$this->admin_company_phone->HrefValue = "";
			$this->admin_company_phone->TooltipValue = "";

			// admin_company_email
			$this->admin_company_email->LinkCustomAttributes = "";
			$this->admin_company_email->HrefValue = "";
			$this->admin_company_email->TooltipValue = "";

			// admin_company_web
			$this->admin_company_web->LinkCustomAttributes = "";
			$this->admin_company_web->HrefValue = "";
			$this->admin_company_web->TooltipValue = "";

			// admin_contract
			$this->admin_contract->LinkCustomAttributes = "";
			$this->admin_contract->HrefValue = "";
			$this->admin_contract->TooltipValue = "";

			// admin_expire
			$this->admin_expire->LinkCustomAttributes = "";
			$this->admin_expire->HrefValue = "";
			$this->admin_expire->TooltipValue = "";

			// admin_company_facebook
			$this->admin_company_facebook->LinkCustomAttributes = "";
			$this->admin_company_facebook->HrefValue = "";
			$this->admin_company_facebook->TooltipValue = "";

			// admin_company_twitter
			$this->admin_company_twitter->LinkCustomAttributes = "";
			$this->admin_company_twitter->HrefValue = "";
			$this->admin_company_twitter->TooltipValue = "";

			// admin_company_googleplus
			$this->admin_company_googleplus->LinkCustomAttributes = "";
			$this->admin_company_googleplus->HrefValue = "";
			$this->admin_company_googleplus->TooltipValue = "";

			// admin_company_linkedin
			$this->admin_company_linkedin->LinkCustomAttributes = "";
			$this->admin_company_linkedin->HrefValue = "";
			$this->admin_company_linkedin->TooltipValue = "";

			// admin_link_playstore
			$this->admin_link_playstore->LinkCustomAttributes = "";
			$this->admin_link_playstore->HrefValue = "";
			$this->admin_link_playstore->TooltipValue = "";

			// admin_link_appstore
			$this->admin_link_appstore->LinkCustomAttributes = "";
			$this->admin_link_appstore->HrefValue = "";
			$this->admin_link_appstore->TooltipValue = "";

			// admin_link_windowsstore
			$this->admin_link_windowsstore->LinkCustomAttributes = "";
			$this->admin_link_windowsstore->HrefValue = "";
			$this->admin_link_windowsstore->TooltipValue = "";

			// admin_level
			$this->admin_level->LinkCustomAttributes = "";
			$this->admin_level->HrefValue = "";
			$this->admin_level->TooltipValue = "";

			// admin_username
			$this->admin_username->LinkCustomAttributes = "";
			$this->admin_username->HrefValue = "";
			$this->admin_username->TooltipValue = "";

			// admin_password
			$this->admin_password->LinkCustomAttributes = "";
			$this->admin_password->HrefValue = "";
			$this->admin_password->TooltipValue = "";

			// admin_last_update
			$this->admin_last_update->LinkCustomAttributes = "";
			$this->admin_last_update->HrefValue = "";
			$this->admin_last_update->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// admin_name
			$this->admin_name->EditAttrs["class"] = "form-control";
			$this->admin_name->EditCustomAttributes = "";
			$this->admin_name->EditValue = ew_HtmlEncode($this->admin_name->CurrentValue);
			$this->admin_name->PlaceHolder = ew_RemoveHtml($this->admin_name->FldCaption());

			// admin_surname
			$this->admin_surname->EditAttrs["class"] = "form-control";
			$this->admin_surname->EditCustomAttributes = "";
			$this->admin_surname->EditValue = ew_HtmlEncode($this->admin_surname->CurrentValue);
			$this->admin_surname->PlaceHolder = ew_RemoveHtml($this->admin_surname->FldCaption());

			// admin_gender
			$this->admin_gender->EditCustomAttributes = "";
			$this->admin_gender->EditValue = $this->admin_gender->Options(FALSE);

			// admin_email
			$this->admin_email->EditAttrs["class"] = "form-control";
			$this->admin_email->EditCustomAttributes = "";
			$this->admin_email->EditValue = ew_HtmlEncode($this->admin_email->CurrentValue);
			$this->admin_email->PlaceHolder = ew_RemoveHtml($this->admin_email->FldCaption());

			// admin_mobile
			$this->admin_mobile->EditAttrs["class"] = "form-control";
			$this->admin_mobile->EditCustomAttributes = "";
			$this->admin_mobile->EditValue = ew_HtmlEncode($this->admin_mobile->CurrentValue);
			$this->admin_mobile->PlaceHolder = ew_RemoveHtml($this->admin_mobile->FldCaption());

			// admin_phone
			$this->admin_phone->EditAttrs["class"] = "form-control";
			$this->admin_phone->EditCustomAttributes = "";
			$this->admin_phone->EditValue = ew_HtmlEncode($this->admin_phone->CurrentValue);
			$this->admin_phone->PlaceHolder = ew_RemoveHtml($this->admin_phone->FldCaption());

			// admin_modules
			$this->admin_modules->EditCustomAttributes = "";
			if (trim(strval($this->admin_modules->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$arwrk = explode(",", $this->admin_modules->CurrentValue);
				$sFilterWrk = "";
				foreach ($arwrk as $wrk) {
					if ($sFilterWrk <> "") $sFilterWrk .= " OR ";
					$sFilterWrk .= "`invalidity_id`" . ew_SearchString("=", trim($wrk), EW_DATATYPE_NUMBER, "");
				}
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_invalidity`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_modules, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			$this->admin_modules->EditValue = $arwrk;

			// admin_language
			$this->admin_language->EditCustomAttributes = "";
			$this->admin_language->EditValue = $this->admin_language->Options(FALSE);

			// admin_is_active
			$this->admin_is_active->EditCustomAttributes = "";
			$this->admin_is_active->EditValue = $this->admin_is_active->Options(FALSE);

			// admin_company_name
			$this->admin_company_name->EditAttrs["class"] = "form-control";
			$this->admin_company_name->EditCustomAttributes = "";
			$this->admin_company_name->EditValue = ew_HtmlEncode($this->admin_company_name->CurrentValue);
			$this->admin_company_name->PlaceHolder = ew_RemoveHtml($this->admin_company_name->FldCaption());

			// admin_company_vat
			$this->admin_company_vat->EditAttrs["class"] = "form-control";
			$this->admin_company_vat->EditCustomAttributes = "";
			$this->admin_company_vat->EditValue = ew_HtmlEncode($this->admin_company_vat->CurrentValue);
			$this->admin_company_vat->PlaceHolder = ew_RemoveHtml($this->admin_company_vat->FldCaption());

			// admin_company_logo_file
			$this->admin_company_logo_file->EditAttrs["class"] = "form-control";
			$this->admin_company_logo_file->EditCustomAttributes = "";
			if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
				$this->admin_company_logo_file->ImageWidth = 100;
				$this->admin_company_logo_file->ImageHeight = 0;
				$this->admin_company_logo_file->ImageAlt = $this->admin_company_logo_file->FldAlt();
				$this->admin_company_logo_file->EditValue = $this->admin_company_logo_file->Upload->DbValue;
			} else {
				$this->admin_company_logo_file->EditValue = "";
			}
			if (!ew_Empty($this->admin_company_logo_file->CurrentValue))
				$this->admin_company_logo_file->Upload->FileName = $this->admin_company_logo_file->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->admin_company_logo_file);

			// admin_company_address
			$this->admin_company_address->EditAttrs["class"] = "form-control";
			$this->admin_company_address->EditCustomAttributes = "";
			$this->admin_company_address->EditValue = ew_HtmlEncode($this->admin_company_address->CurrentValue);
			$this->admin_company_address->PlaceHolder = ew_RemoveHtml($this->admin_company_address->FldCaption());

			// admin_company_region_id
			$this->admin_company_region_id->EditAttrs["class"] = "form-control";
			$this->admin_company_region_id->EditCustomAttributes = "";
			if (trim(strval($this->admin_company_region_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->admin_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->admin_company_region_id->EditValue = $arwrk;

			// admin_company_province_id
			$this->admin_company_province_id->EditAttrs["class"] = "form-control";
			$this->admin_company_province_id->EditCustomAttributes = "";
			if (trim(strval($this->admin_company_province_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->admin_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->admin_company_province_id->EditValue = $arwrk;

			// admin_company_city_id
			$this->admin_company_city_id->EditAttrs["class"] = "form-control";
			$this->admin_company_city_id->EditCustomAttributes = "";
			if (trim(strval($this->admin_company_city_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->admin_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->admin_company_city_id->EditValue = $arwrk;

			// admin_company_phone
			$this->admin_company_phone->EditAttrs["class"] = "form-control";
			$this->admin_company_phone->EditCustomAttributes = "";
			$this->admin_company_phone->EditValue = ew_HtmlEncode($this->admin_company_phone->CurrentValue);
			$this->admin_company_phone->PlaceHolder = ew_RemoveHtml($this->admin_company_phone->FldCaption());

			// admin_company_email
			$this->admin_company_email->EditAttrs["class"] = "form-control";
			$this->admin_company_email->EditCustomAttributes = "";
			$this->admin_company_email->EditValue = ew_HtmlEncode($this->admin_company_email->CurrentValue);
			$this->admin_company_email->PlaceHolder = ew_RemoveHtml($this->admin_company_email->FldCaption());

			// admin_company_web
			$this->admin_company_web->EditAttrs["class"] = "form-control";
			$this->admin_company_web->EditCustomAttributes = "";
			$this->admin_company_web->EditValue = ew_HtmlEncode($this->admin_company_web->CurrentValue);
			$this->admin_company_web->PlaceHolder = ew_RemoveHtml($this->admin_company_web->FldCaption());

			// admin_contract
			$this->admin_contract->EditAttrs["class"] = "form-control";
			$this->admin_contract->EditCustomAttributes = "";
			$this->admin_contract->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->admin_contract->CurrentValue, 7));
			$this->admin_contract->PlaceHolder = ew_RemoveHtml($this->admin_contract->FldCaption());

			// admin_expire
			$this->admin_expire->EditAttrs["class"] = "form-control";
			$this->admin_expire->EditCustomAttributes = "";
			$this->admin_expire->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->admin_expire->CurrentValue, 7));
			$this->admin_expire->PlaceHolder = ew_RemoveHtml($this->admin_expire->FldCaption());

			// admin_company_facebook
			$this->admin_company_facebook->EditAttrs["class"] = "form-control";
			$this->admin_company_facebook->EditCustomAttributes = "";
			$this->admin_company_facebook->EditValue = ew_HtmlEncode($this->admin_company_facebook->CurrentValue);
			$this->admin_company_facebook->PlaceHolder = ew_RemoveHtml($this->admin_company_facebook->FldCaption());

			// admin_company_twitter
			$this->admin_company_twitter->EditAttrs["class"] = "form-control";
			$this->admin_company_twitter->EditCustomAttributes = "";
			$this->admin_company_twitter->EditValue = ew_HtmlEncode($this->admin_company_twitter->CurrentValue);
			$this->admin_company_twitter->PlaceHolder = ew_RemoveHtml($this->admin_company_twitter->FldCaption());

			// admin_company_googleplus
			$this->admin_company_googleplus->EditAttrs["class"] = "form-control";
			$this->admin_company_googleplus->EditCustomAttributes = "";
			$this->admin_company_googleplus->EditValue = ew_HtmlEncode($this->admin_company_googleplus->CurrentValue);
			$this->admin_company_googleplus->PlaceHolder = ew_RemoveHtml($this->admin_company_googleplus->FldCaption());

			// admin_company_linkedin
			$this->admin_company_linkedin->EditAttrs["class"] = "form-control";
			$this->admin_company_linkedin->EditCustomAttributes = "";
			$this->admin_company_linkedin->EditValue = ew_HtmlEncode($this->admin_company_linkedin->CurrentValue);
			$this->admin_company_linkedin->PlaceHolder = ew_RemoveHtml($this->admin_company_linkedin->FldCaption());

			// admin_link_playstore
			$this->admin_link_playstore->EditAttrs["class"] = "form-control";
			$this->admin_link_playstore->EditCustomAttributes = "";
			$this->admin_link_playstore->EditValue = ew_HtmlEncode($this->admin_link_playstore->CurrentValue);
			$this->admin_link_playstore->PlaceHolder = ew_RemoveHtml($this->admin_link_playstore->FldCaption());

			// admin_link_appstore
			$this->admin_link_appstore->EditAttrs["class"] = "form-control";
			$this->admin_link_appstore->EditCustomAttributes = "";
			$this->admin_link_appstore->EditValue = ew_HtmlEncode($this->admin_link_appstore->CurrentValue);
			$this->admin_link_appstore->PlaceHolder = ew_RemoveHtml($this->admin_link_appstore->FldCaption());

			// admin_link_windowsstore
			$this->admin_link_windowsstore->EditAttrs["class"] = "form-control";
			$this->admin_link_windowsstore->EditCustomAttributes = "";
			$this->admin_link_windowsstore->EditValue = ew_HtmlEncode($this->admin_link_windowsstore->CurrentValue);
			$this->admin_link_windowsstore->PlaceHolder = ew_RemoveHtml($this->admin_link_windowsstore->FldCaption());

			// admin_level
			$this->admin_level->EditAttrs["class"] = "form-control";
			$this->admin_level->EditCustomAttributes = "";
			if (!$Security->CanAdmin()) { // System admin
				$this->admin_level->EditValue = $Language->Phrase("PasswordMask");
			} else {
			$this->admin_level->EditValue = $this->admin_level->Options(TRUE);
			}

			// admin_username
			$this->admin_username->EditAttrs["class"] = "form-control";
			$this->admin_username->EditCustomAttributes = "";
			$this->admin_username->EditValue = ew_HtmlEncode($this->admin_username->CurrentValue);
			$this->admin_username->PlaceHolder = ew_RemoveHtml($this->admin_username->FldCaption());

			// admin_password
			$this->admin_password->EditAttrs["class"] = "form-control ewPasswordStrength";
			$this->admin_password->EditCustomAttributes = "";
			$this->admin_password->EditValue = ew_HtmlEncode($this->admin_password->CurrentValue);
			$this->admin_password->PlaceHolder = ew_RemoveHtml($this->admin_password->FldCaption());

			// admin_last_update
			// Edit refer script
			// admin_name

			$this->admin_name->LinkCustomAttributes = "";
			$this->admin_name->HrefValue = "";

			// admin_surname
			$this->admin_surname->LinkCustomAttributes = "";
			$this->admin_surname->HrefValue = "";

			// admin_gender
			$this->admin_gender->LinkCustomAttributes = "";
			$this->admin_gender->HrefValue = "";

			// admin_email
			$this->admin_email->LinkCustomAttributes = "";
			$this->admin_email->HrefValue = "";

			// admin_mobile
			$this->admin_mobile->LinkCustomAttributes = "";
			$this->admin_mobile->HrefValue = "";

			// admin_phone
			$this->admin_phone->LinkCustomAttributes = "";
			$this->admin_phone->HrefValue = "";

			// admin_modules
			$this->admin_modules->LinkCustomAttributes = "";
			$this->admin_modules->HrefValue = "";

			// admin_language
			$this->admin_language->LinkCustomAttributes = "";
			$this->admin_language->HrefValue = "";

			// admin_is_active
			$this->admin_is_active->LinkCustomAttributes = "";
			$this->admin_is_active->HrefValue = "";

			// admin_company_name
			$this->admin_company_name->LinkCustomAttributes = "";
			$this->admin_company_name->HrefValue = "";

			// admin_company_vat
			$this->admin_company_vat->LinkCustomAttributes = "";
			$this->admin_company_vat->HrefValue = "";

			// admin_company_logo_file
			$this->admin_company_logo_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->admin_company_logo_file->Upload->DbValue)) {
				$this->admin_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->admin_company_logo_file, $this->admin_company_logo_file->Upload->DbValue); // Add prefix/suffix
				$this->admin_company_logo_file->LinkAttrs["target"] = ""; // Add target
				if ($this->Export <> "") $this->admin_company_logo_file->HrefValue = ew_ConvertFullUrl($this->admin_company_logo_file->HrefValue);
			} else {
				$this->admin_company_logo_file->HrefValue = "";
			}
			$this->admin_company_logo_file->HrefValue2 = $this->admin_company_logo_file->UploadPath . $this->admin_company_logo_file->Upload->DbValue;

			// admin_company_address
			$this->admin_company_address->LinkCustomAttributes = "";
			$this->admin_company_address->HrefValue = "";

			// admin_company_region_id
			$this->admin_company_region_id->LinkCustomAttributes = "";
			$this->admin_company_region_id->HrefValue = "";

			// admin_company_province_id
			$this->admin_company_province_id->LinkCustomAttributes = "";
			$this->admin_company_province_id->HrefValue = "";

			// admin_company_city_id
			$this->admin_company_city_id->LinkCustomAttributes = "";
			$this->admin_company_city_id->HrefValue = "";

			// admin_company_phone
			$this->admin_company_phone->LinkCustomAttributes = "";
			$this->admin_company_phone->HrefValue = "";

			// admin_company_email
			$this->admin_company_email->LinkCustomAttributes = "";
			$this->admin_company_email->HrefValue = "";

			// admin_company_web
			$this->admin_company_web->LinkCustomAttributes = "";
			$this->admin_company_web->HrefValue = "";

			// admin_contract
			$this->admin_contract->LinkCustomAttributes = "";
			$this->admin_contract->HrefValue = "";

			// admin_expire
			$this->admin_expire->LinkCustomAttributes = "";
			$this->admin_expire->HrefValue = "";

			// admin_company_facebook
			$this->admin_company_facebook->LinkCustomAttributes = "";
			$this->admin_company_facebook->HrefValue = "";

			// admin_company_twitter
			$this->admin_company_twitter->LinkCustomAttributes = "";
			$this->admin_company_twitter->HrefValue = "";

			// admin_company_googleplus
			$this->admin_company_googleplus->LinkCustomAttributes = "";
			$this->admin_company_googleplus->HrefValue = "";

			// admin_company_linkedin
			$this->admin_company_linkedin->LinkCustomAttributes = "";
			$this->admin_company_linkedin->HrefValue = "";

			// admin_link_playstore
			$this->admin_link_playstore->LinkCustomAttributes = "";
			$this->admin_link_playstore->HrefValue = "";

			// admin_link_appstore
			$this->admin_link_appstore->LinkCustomAttributes = "";
			$this->admin_link_appstore->HrefValue = "";

			// admin_link_windowsstore
			$this->admin_link_windowsstore->LinkCustomAttributes = "";
			$this->admin_link_windowsstore->HrefValue = "";

			// admin_level
			$this->admin_level->LinkCustomAttributes = "";
			$this->admin_level->HrefValue = "";

			// admin_username
			$this->admin_username->LinkCustomAttributes = "";
			$this->admin_username->HrefValue = "";

			// admin_password
			$this->admin_password->LinkCustomAttributes = "";
			$this->admin_password->HrefValue = "";

			// admin_last_update
			$this->admin_last_update->LinkCustomAttributes = "";
			$this->admin_last_update->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->admin_email->FldIsDetailKey && !is_null($this->admin_email->FormValue) && $this->admin_email->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_email->FldCaption(), $this->admin_email->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->admin_email->FormValue)) {
			ew_AddMessage($gsFormError, $this->admin_email->FldErrMsg());
		}
		if (!$this->admin_phone->FldIsDetailKey && !is_null($this->admin_phone->FormValue) && $this->admin_phone->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_phone->FldCaption(), $this->admin_phone->ReqErrMsg));
		}
		if ($this->admin_modules->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_modules->FldCaption(), $this->admin_modules->ReqErrMsg));
		}
		if ($this->admin_is_active->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_is_active->FldCaption(), $this->admin_is_active->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->admin_company_vat->FormValue)) {
			ew_AddMessage($gsFormError, $this->admin_company_vat->FldErrMsg());
		}
		if (!$this->admin_company_region_id->FldIsDetailKey && !is_null($this->admin_company_region_id->FormValue) && $this->admin_company_region_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_company_region_id->FldCaption(), $this->admin_company_region_id->ReqErrMsg));
		}
		if (!$this->admin_company_province_id->FldIsDetailKey && !is_null($this->admin_company_province_id->FormValue) && $this->admin_company_province_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_company_province_id->FldCaption(), $this->admin_company_province_id->ReqErrMsg));
		}
		if (!$this->admin_company_city_id->FldIsDetailKey && !is_null($this->admin_company_city_id->FormValue) && $this->admin_company_city_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_company_city_id->FldCaption(), $this->admin_company_city_id->ReqErrMsg));
		}
		if (!$this->admin_company_phone->FldIsDetailKey && !is_null($this->admin_company_phone->FormValue) && $this->admin_company_phone->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_company_phone->FldCaption(), $this->admin_company_phone->ReqErrMsg));
		}
		if (!$this->admin_company_email->FldIsDetailKey && !is_null($this->admin_company_email->FormValue) && $this->admin_company_email->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_company_email->FldCaption(), $this->admin_company_email->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->admin_company_email->FormValue)) {
			ew_AddMessage($gsFormError, $this->admin_company_email->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->admin_contract->FormValue)) {
			ew_AddMessage($gsFormError, $this->admin_contract->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->admin_expire->FormValue)) {
			ew_AddMessage($gsFormError, $this->admin_expire->FldErrMsg());
		}
		if (!$this->admin_level->FldIsDetailKey && !is_null($this->admin_level->FormValue) && $this->admin_level->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->admin_level->FldCaption(), $this->admin_level->ReqErrMsg));
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		if ($this->admin_username->CurrentValue <> "") { // Check field with unique index
			$sFilter = "(admin_username = '" . ew_AdjustSql($this->admin_username->CurrentValue, $this->DBID) . "')";
			$rsChk = $this->LoadRs($sFilter);
			if ($rsChk && !$rsChk->EOF) {
				$sIdxErrMsg = str_replace("%f", $this->admin_username->FldCaption(), $Language->Phrase("DupIndex"));
				$sIdxErrMsg = str_replace("%v", $this->admin_username->CurrentValue, $sIdxErrMsg);
				$this->setFailureMessage($sIdxErrMsg);
				$rsChk->Close();
				return FALSE;
			}
		}
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// admin_name
		$this->admin_name->SetDbValueDef($rsnew, $this->admin_name->CurrentValue, NULL, FALSE);

		// admin_surname
		$this->admin_surname->SetDbValueDef($rsnew, $this->admin_surname->CurrentValue, NULL, FALSE);

		// admin_gender
		$this->admin_gender->SetDbValueDef($rsnew, $this->admin_gender->CurrentValue, NULL, strval($this->admin_gender->CurrentValue) == "");

		// admin_email
		$this->admin_email->SetDbValueDef($rsnew, $this->admin_email->CurrentValue, NULL, FALSE);

		// admin_mobile
		$this->admin_mobile->SetDbValueDef($rsnew, $this->admin_mobile->CurrentValue, NULL, FALSE);

		// admin_phone
		$this->admin_phone->SetDbValueDef($rsnew, $this->admin_phone->CurrentValue, NULL, FALSE);

		// admin_modules
		$this->admin_modules->SetDbValueDef($rsnew, $this->admin_modules->CurrentValue, NULL, FALSE);

		// admin_language
		$this->admin_language->SetDbValueDef($rsnew, $this->admin_language->CurrentValue, NULL, strval($this->admin_language->CurrentValue) == "");

		// admin_is_active
		$this->admin_is_active->SetDbValueDef($rsnew, $this->admin_is_active->CurrentValue, NULL, FALSE);

		// admin_company_name
		$this->admin_company_name->SetDbValueDef($rsnew, $this->admin_company_name->CurrentValue, NULL, FALSE);

		// admin_company_vat
		$this->admin_company_vat->SetDbValueDef($rsnew, $this->admin_company_vat->CurrentValue, NULL, FALSE);

		// admin_company_logo_file
		if ($this->admin_company_logo_file->Visible && !$this->admin_company_logo_file->Upload->KeepFile) {
			$this->admin_company_logo_file->Upload->DbValue = ""; // No need to delete old file
			if ($this->admin_company_logo_file->Upload->FileName == "") {
				$rsnew['admin_company_logo_file'] = NULL;
			} else {
				$rsnew['admin_company_logo_file'] = $this->admin_company_logo_file->Upload->FileName;
			}
			$this->admin_company_logo_size->SetDbValueDef($rsnew, $this->admin_company_logo_file->Upload->FileSize, NULL, FALSE);
			if (!ew_Empty($this->admin_company_logo_file->Upload->Value)) { // Get resize dimensions
				$TmpValue = $this->admin_company_logo_file->Upload->Value;
				$this->admin_company_logo_file->Upload->Resize($this->admin_company_logo_file->ImageWidth, $this->admin_company_logo_file->ImageHeight);
				$this->admin_company_logo_file->Upload->Value = $TmpValue;
			}
			$this->admin_company_logo_width->SetDbValueDef($rsnew, $this->admin_company_logo_file->Upload->ImageWidth, NULL, FALSE);
			$this->admin_company_logo_height->SetDbValueDef($rsnew, $this->admin_company_logo_file->Upload->ImageHeight, NULL, FALSE);
		}

		// admin_company_address
		$this->admin_company_address->SetDbValueDef($rsnew, $this->admin_company_address->CurrentValue, NULL, FALSE);

		// admin_company_region_id
		$this->admin_company_region_id->SetDbValueDef($rsnew, $this->admin_company_region_id->CurrentValue, NULL, FALSE);

		// admin_company_province_id
		$this->admin_company_province_id->SetDbValueDef($rsnew, $this->admin_company_province_id->CurrentValue, NULL, FALSE);

		// admin_company_city_id
		$this->admin_company_city_id->SetDbValueDef($rsnew, $this->admin_company_city_id->CurrentValue, NULL, FALSE);

		// admin_company_phone
		$this->admin_company_phone->SetDbValueDef($rsnew, $this->admin_company_phone->CurrentValue, NULL, FALSE);

		// admin_company_email
		$this->admin_company_email->SetDbValueDef($rsnew, $this->admin_company_email->CurrentValue, NULL, FALSE);

		// admin_company_web
		$this->admin_company_web->SetDbValueDef($rsnew, $this->admin_company_web->CurrentValue, NULL, FALSE);

		// admin_contract
		$this->admin_contract->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->admin_contract->CurrentValue, 7), NULL, FALSE);

		// admin_expire
		$this->admin_expire->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->admin_expire->CurrentValue, 7), NULL, FALSE);

		// admin_company_facebook
		$this->admin_company_facebook->SetDbValueDef($rsnew, $this->admin_company_facebook->CurrentValue, NULL, FALSE);

		// admin_company_twitter
		$this->admin_company_twitter->SetDbValueDef($rsnew, $this->admin_company_twitter->CurrentValue, NULL, FALSE);

		// admin_company_googleplus
		$this->admin_company_googleplus->SetDbValueDef($rsnew, $this->admin_company_googleplus->CurrentValue, NULL, FALSE);

		// admin_company_linkedin
		$this->admin_company_linkedin->SetDbValueDef($rsnew, $this->admin_company_linkedin->CurrentValue, NULL, FALSE);

		// admin_link_playstore
		$this->admin_link_playstore->SetDbValueDef($rsnew, $this->admin_link_playstore->CurrentValue, NULL, FALSE);

		// admin_link_appstore
		$this->admin_link_appstore->SetDbValueDef($rsnew, $this->admin_link_appstore->CurrentValue, NULL, FALSE);

		// admin_link_windowsstore
		$this->admin_link_windowsstore->SetDbValueDef($rsnew, $this->admin_link_windowsstore->CurrentValue, NULL, FALSE);

		// admin_level
		if ($Security->CanAdmin()) { // System admin
		$this->admin_level->SetDbValueDef($rsnew, $this->admin_level->CurrentValue, 0, strval($this->admin_level->CurrentValue) == "");
		}

		// admin_username
		$this->admin_username->SetDbValueDef($rsnew, $this->admin_username->CurrentValue, NULL, FALSE);

		// admin_password
		$this->admin_password->SetDbValueDef($rsnew, $this->admin_password->CurrentValue, NULL, FALSE);

		// admin_last_update
		$this->admin_last_update->SetDbValueDef($rsnew, ew_CurrentDateTime(), NULL);
		$rsnew['admin_last_update'] = &$this->admin_last_update->DbValue;

		// admin_id
		// admin_parent_id

		if (!$Security->IsAdmin() && $Security->IsLoggedIn()) { // Non system admin
			$rsnew['admin_parent_id'] = CurrentUserID();
		}
		if ($this->admin_company_logo_file->Visible && !$this->admin_company_logo_file->Upload->KeepFile) {
			if (!ew_Empty($this->admin_company_logo_file->Upload->Value)) {
				if ($this->admin_company_logo_file->Upload->FileName == $this->admin_company_logo_file->Upload->DbValue) { // Overwrite if same file name
					$this->admin_company_logo_file->Upload->DbValue = ""; // No need to delete any more
				} else {
					$rsnew['admin_company_logo_file'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->admin_company_logo_file->UploadPath), $rsnew['admin_company_logo_file']); // Get new file name
				}
			}
		}

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->admin_id->setDbValue($conn->Insert_ID());
				$rsnew['admin_id'] = $this->admin_id->DbValue;
				if ($this->admin_company_logo_file->Visible && !$this->admin_company_logo_file->Upload->KeepFile) {
					if (!ew_Empty($this->admin_company_logo_file->Upload->Value)) {
						$this->admin_company_logo_file->Upload->SaveToFile($this->admin_company_logo_file->UploadPath, $rsnew['admin_company_logo_file'], TRUE);
					}
					if ($this->admin_company_logo_file->Upload->DbValue <> "")
						@unlink(ew_UploadPathEx(TRUE, $this->admin_company_logo_file->OldUploadPath) . $this->admin_company_logo_file->Upload->DbValue);
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// admin_company_logo_file
		ew_CleanUploadTempPath($this->admin_company_logo_file, $this->admin_company_logo_file->Upload->Index);
		return $AddRow;
	}

	// Show link optionally based on User ID
	function ShowOptionLink($id = "") {
		global $Security;
		if ($Security->IsLoggedIn() && !$Security->IsAdmin() && !$this->UserIDAllow($id))
			return $Security->IsValidUserID($this->admin_id->CurrentValue);
		return TRUE;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_adminlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$pages->Add(4);
		$this->MultiPages = $pages;
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_admin_add)) $telecare_admin_add = new ctelecare_admin_add();

// Page init
$telecare_admin_add->Page_Init();

// Page main
$telecare_admin_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_admin_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_adminadd = new ew_Form("ftelecare_adminadd", "add");

// Validate form
ftelecare_adminadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_admin_email");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_email->FldCaption(), $telecare_admin->admin_email->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_email");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_email->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_admin_phone");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_phone->FldCaption(), $telecare_admin->admin_phone->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_modules[]");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_modules->FldCaption(), $telecare_admin->admin_modules->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_is_active");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_is_active->FldCaption(), $telecare_admin->admin_is_active->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_company_vat");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_company_vat->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_admin_company_region_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_company_region_id->FldCaption(), $telecare_admin->admin_company_region_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_company_province_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_company_province_id->FldCaption(), $telecare_admin->admin_company_province_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_company_city_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_company_city_id->FldCaption(), $telecare_admin->admin_company_city_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_company_phone");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_company_phone->FldCaption(), $telecare_admin->admin_company_phone->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_company_email");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_company_email->FldCaption(), $telecare_admin->admin_company_email->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_admin_company_email");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_company_email->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_admin_contract");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_contract->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_admin_expire");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_admin->admin_expire->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_admin_level");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_admin->admin_level->FldCaption(), $telecare_admin->admin_level->ReqErrMsg)) ?>");
			if ($(fobj.x_admin_password).hasClass("ewPasswordStrength") && !$(fobj.x_admin_password).data("validated"))
				return this.OnError(fobj.x_admin_password, ewLanguage.Phrase("PasswordTooSimple"));

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_adminadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_adminadd.ValidateRequired = true;
<?php } else { ?>
ftelecare_adminadd.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_adminadd.MultiPage = new ew_MultiPage("ftelecare_adminadd");

// Dynamic selection lists
ftelecare_adminadd.Lists["x_admin_gender"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_gender"].Options = <?php echo json_encode($telecare_admin->admin_gender->Options()) ?>;
ftelecare_adminadd.Lists["x_admin_modules[]"] = {"LinkField":"x_invalidity_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_invalidity_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_language"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_language"].Options = <?php echo json_encode($telecare_admin->admin_language->Options()) ?>;
ftelecare_adminadd.Lists["x_admin_is_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_is_active"].Options = <?php echo json_encode($telecare_admin->admin_is_active->Options()) ?>;
ftelecare_adminadd.Lists["x_admin_company_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_admin_company_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_company_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":["x_admin_company_region_id"],"ChildFields":["x_admin_company_city_id"],"FilterFields":["x_province_region_id"],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_company_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":["x_admin_company_province_id"],"ChildFields":[],"FilterFields":["x_city_province_id"],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_level"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_adminadd.Lists["x_admin_level"].Options = <?php echo json_encode($telecare_admin->admin_level->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_admin_add->ShowPageHeader(); ?>
<?php
$telecare_admin_add->ShowMessage();
?>
<form name="ftelecare_adminadd" id="ftelecare_adminadd" class="<?php echo $telecare_admin_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_admin_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_admin_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_admin">
<input type="hidden" name="a_add" id="a_add" value="A">
<!-- Fields to prevent google autofill -->
<input class="hidden" type="text" name="<?php echo ew_Encrypt(ew_Random()) ?>">
<input class="hidden" type="password" name="<?php echo ew_Encrypt(ew_Random()) ?>">
<div class="ewMultiPage">
<div class="tabbable" id="telecare_admin_add">
	<ul class="nav<?php echo $telecare_admin_add->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_admin_add->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_admin1" data-toggle="tab"><?php echo $telecare_admin->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_admin_add->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_admin2" data-toggle="tab"><?php echo $telecare_admin->PageCaption(2) ?></a></li>
		<li<?php echo $telecare_admin_add->MultiPages->TabStyle("3") ?>><a href="#tab_telecare_admin3" data-toggle="tab"><?php echo $telecare_admin->PageCaption(3) ?></a></li>
		<li<?php echo $telecare_admin_add->MultiPages->TabStyle("4") ?>><a href="#tab_telecare_admin4" data-toggle="tab"><?php echo $telecare_admin->PageCaption(4) ?></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane<?php echo $telecare_admin_add->MultiPages->PageStyle("1") ?>" id="tab_telecare_admin1">
<div>
<?php if ($telecare_admin->admin_name->Visible) { // admin_name ?>
	<div id="r_admin_name" class="form-group">
		<label id="elh_telecare_admin_admin_name" for="x_admin_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_name->CellAttributes() ?>>
<span id="el_telecare_admin_admin_name">
<input type="text" data-table="telecare_admin" data-field="x_admin_name" data-page="1" name="x_admin_name" id="x_admin_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_name->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_name->EditValue ?>"<?php echo $telecare_admin->admin_name->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_surname->Visible) { // admin_surname ?>
	<div id="r_admin_surname" class="form-group">
		<label id="elh_telecare_admin_admin_surname" for="x_admin_surname" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_surname->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_surname->CellAttributes() ?>>
<span id="el_telecare_admin_admin_surname">
<input type="text" data-table="telecare_admin" data-field="x_admin_surname" data-page="1" name="x_admin_surname" id="x_admin_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_surname->EditValue ?>"<?php echo $telecare_admin->admin_surname->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_surname->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_gender->Visible) { // admin_gender ?>
	<div id="r_admin_gender" class="form-group">
		<label id="elh_telecare_admin_admin_gender" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_gender->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_gender->CellAttributes() ?>>
<span id="el_telecare_admin_admin_gender">
<div id="tp_x_admin_gender" class="ewTemplate"><input type="radio" data-table="telecare_admin" data-field="x_admin_gender" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_gender->DisplayValueSeparator) ? json_encode($telecare_admin->admin_gender->DisplayValueSeparator) : $telecare_admin->admin_gender->DisplayValueSeparator) ?>" name="x_admin_gender" id="x_admin_gender" value="{value}"<?php echo $telecare_admin->admin_gender->EditAttributes() ?>></div>
<div id="dsl_x_admin_gender" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_gender->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_admin->admin_gender->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_gender" data-page="1" name="x_admin_gender" id="x_admin_gender_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_gender->EditAttributes() ?>><?php echo $telecare_admin->admin_gender->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_gender->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_gender" data-page="1" name="x_admin_gender" id="x_admin_gender_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_admin->admin_gender->CurrentValue) ?>" checked<?php echo $telecare_admin->admin_gender->EditAttributes() ?>><?php echo $telecare_admin->admin_gender->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_admin->admin_gender->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_email->Visible) { // admin_email ?>
	<div id="r_admin_email" class="form-group">
		<label id="elh_telecare_admin_admin_email" for="x_admin_email" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_email->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_email->CellAttributes() ?>>
<span id="el_telecare_admin_admin_email">
<input type="text" data-table="telecare_admin" data-field="x_admin_email" data-page="1" name="x_admin_email" id="x_admin_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_email->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_email->EditValue ?>"<?php echo $telecare_admin->admin_email->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_mobile->Visible) { // admin_mobile ?>
	<div id="r_admin_mobile" class="form-group">
		<label id="elh_telecare_admin_admin_mobile" for="x_admin_mobile" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_mobile->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_mobile->CellAttributes() ?>>
<span id="el_telecare_admin_admin_mobile">
<input type="text" data-table="telecare_admin" data-field="x_admin_mobile" data-page="1" name="x_admin_mobile" id="x_admin_mobile" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_mobile->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_mobile->EditValue ?>"<?php echo $telecare_admin->admin_mobile->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_mobile->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_phone->Visible) { // admin_phone ?>
	<div id="r_admin_phone" class="form-group">
		<label id="elh_telecare_admin_admin_phone" for="x_admin_phone" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_phone->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_phone->CellAttributes() ?>>
<span id="el_telecare_admin_admin_phone">
<input type="text" data-table="telecare_admin" data-field="x_admin_phone" data-page="1" name="x_admin_phone" id="x_admin_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_phone->EditValue ?>"<?php echo $telecare_admin->admin_phone->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_phone->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_modules->Visible) { // admin_modules ?>
	<div id="r_admin_modules" class="form-group">
		<label id="elh_telecare_admin_admin_modules" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_modules->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_modules->CellAttributes() ?>>
<span id="el_telecare_admin_admin_modules">
<div id="tp_x_admin_modules" class="ewTemplate"><input type="checkbox" data-table="telecare_admin" data-field="x_admin_modules" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_modules->DisplayValueSeparator) ? json_encode($telecare_admin->admin_modules->DisplayValueSeparator) : $telecare_admin->admin_modules->DisplayValueSeparator) ?>" name="x_admin_modules[]" id="x_admin_modules[]" value="{value}"<?php echo $telecare_admin->admin_modules->EditAttributes() ?>></div>
<div id="dsl_x_admin_modules" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_modules->EditValue;
if (is_array($arwrk)) {
	$armultiwrk = (strval($telecare_admin->admin_modules->CurrentValue) <> "") ? explode(",", strval($telecare_admin->admin_modules->CurrentValue)) : array();
	$cnt = count($armultiwrk);
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = "";
		for ($ari = 0; $ari < $cnt; $ari++) {
			if (ew_SameStr($arwrk[$rowcntwrk][0], $armultiwrk[$ari]) && !is_null($armultiwrk[$ari])) {
				$armultiwrk[$ari] = NULL; // Marked for removal
				$selwrk = " checked";
				if ($selwrk <> "") $emptywrk = FALSE;
				break;
			}
		}
		if ($selwrk <> "") {
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_admin" data-field="x_admin_modules" data-page="1" name="x_admin_modules[]" id="x_admin_modules_<?php echo $rowcntwrk ?>[]" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_modules->EditAttributes() ?>><?php echo $telecare_admin->admin_modules->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
		}
	}
	for ($ari = 0; $ari < $cnt; $ari++) {
		if (!is_null($armultiwrk[$ari])) {
?>
<label class="checkbox-inline"><input type="checkbox" data-table="telecare_admin" data-field="x_admin_modules" data-page="1" name="x_admin_modules[]" value="<?php echo ew_HtmlEncode($armultiwrk[$ari]) ?>" checked<?php echo $telecare_admin->admin_modules->EditAttributes() ?>><?php echo $armultiwrk[$ari] ?></label>
<?php
		}
	}
}
?>
</div></div>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `invalidity_id`, `invalidity_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_invalidity`";
		$sWhereWrk = "";
		break;
}
$telecare_admin->admin_modules->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_modules->LookupFilters += array("f0" => "`invalidity_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_modules, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `invalidity_name` ASC";
if ($sSqlWrk <> "") $telecare_admin->admin_modules->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_modules" id="s_x_admin_modules" value="<?php echo $telecare_admin->admin_modules->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_admin->admin_modules->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_language->Visible) { // admin_language ?>
	<div id="r_admin_language" class="form-group">
		<label id="elh_telecare_admin_admin_language" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_language->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_language->CellAttributes() ?>>
<span id="el_telecare_admin_admin_language">
<div id="tp_x_admin_language" class="ewTemplate"><input type="radio" data-table="telecare_admin" data-field="x_admin_language" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_language->DisplayValueSeparator) ? json_encode($telecare_admin->admin_language->DisplayValueSeparator) : $telecare_admin->admin_language->DisplayValueSeparator) ?>" name="x_admin_language" id="x_admin_language" value="{value}"<?php echo $telecare_admin->admin_language->EditAttributes() ?>></div>
<div id="dsl_x_admin_language" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_language->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_admin->admin_language->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_language" data-page="1" name="x_admin_language" id="x_admin_language_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_language->EditAttributes() ?>><?php echo $telecare_admin->admin_language->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_language->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_language" data-page="1" name="x_admin_language" id="x_admin_language_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_admin->admin_language->CurrentValue) ?>" checked<?php echo $telecare_admin->admin_language->EditAttributes() ?>><?php echo $telecare_admin->admin_language->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_admin->admin_language->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_is_active->Visible) { // admin_is_active ?>
	<div id="r_admin_is_active" class="form-group">
		<label id="elh_telecare_admin_admin_is_active" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_is_active->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_is_active->CellAttributes() ?>>
<span id="el_telecare_admin_admin_is_active">
<div id="tp_x_admin_is_active" class="ewTemplate"><input type="radio" data-table="telecare_admin" data-field="x_admin_is_active" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_is_active->DisplayValueSeparator) ? json_encode($telecare_admin->admin_is_active->DisplayValueSeparator) : $telecare_admin->admin_is_active->DisplayValueSeparator) ?>" name="x_admin_is_active" id="x_admin_is_active" value="{value}"<?php echo $telecare_admin->admin_is_active->EditAttributes() ?>></div>
<div id="dsl_x_admin_is_active" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_admin->admin_is_active->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_admin->admin_is_active->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_is_active" data-page="1" name="x_admin_is_active" id="x_admin_is_active_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_admin->admin_is_active->EditAttributes() ?>><?php echo $telecare_admin->admin_is_active->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_is_active->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_admin" data-field="x_admin_is_active" data-page="1" name="x_admin_is_active" id="x_admin_is_active_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_admin->admin_is_active->CurrentValue) ?>" checked<?php echo $telecare_admin->admin_is_active->EditAttributes() ?>><?php echo $telecare_admin->admin_is_active->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_admin->admin_is_active->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_admin_add->MultiPages->PageStyle("2") ?>" id="tab_telecare_admin2">
<div>
<?php if ($telecare_admin->admin_company_name->Visible) { // admin_company_name ?>
	<div id="r_admin_company_name" class="form-group">
		<label id="elh_telecare_admin_admin_company_name" for="x_admin_company_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_name->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_name">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_name" data-page="2" name="x_admin_company_name" id="x_admin_company_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_name->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_name->EditValue ?>"<?php echo $telecare_admin->admin_company_name->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_vat->Visible) { // admin_company_vat ?>
	<div id="r_admin_company_vat" class="form-group">
		<label id="elh_telecare_admin_admin_company_vat" for="x_admin_company_vat" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_vat->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_vat->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_vat">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_vat" data-page="2" name="x_admin_company_vat" id="x_admin_company_vat" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_vat->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_vat->EditValue ?>"<?php echo $telecare_admin->admin_company_vat->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_vat->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_logo_file->Visible) { // admin_company_logo_file ?>
	<div id="r_admin_company_logo_file" class="form-group">
		<label id="elh_telecare_admin_admin_company_logo_file" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_logo_file->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_logo_file->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_logo_file">
<div id="fd_x_admin_company_logo_file">
<span title="<?php echo $telecare_admin->admin_company_logo_file->FldTitle() ? $telecare_admin->admin_company_logo_file->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($telecare_admin->admin_company_logo_file->ReadOnly || $telecare_admin->admin_company_logo_file->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="telecare_admin" data-field="x_admin_company_logo_file" data-page="2" name="x_admin_company_logo_file" id="x_admin_company_logo_file"<?php echo $telecare_admin->admin_company_logo_file->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_admin_company_logo_file" id= "fn_x_admin_company_logo_file" value="<?php echo $telecare_admin->admin_company_logo_file->Upload->FileName ?>">
<input type="hidden" name="fa_x_admin_company_logo_file" id= "fa_x_admin_company_logo_file" value="0">
<input type="hidden" name="fs_x_admin_company_logo_file" id= "fs_x_admin_company_logo_file" value="255">
<input type="hidden" name="fx_x_admin_company_logo_file" id= "fx_x_admin_company_logo_file" value="<?php echo $telecare_admin->admin_company_logo_file->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_admin_company_logo_file" id= "fm_x_admin_company_logo_file" value="<?php echo $telecare_admin->admin_company_logo_file->UploadMaxFileSize ?>">
</div>
<table id="ft_x_admin_company_logo_file" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $telecare_admin->admin_company_logo_file->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_address->Visible) { // admin_company_address ?>
	<div id="r_admin_company_address" class="form-group">
		<label id="elh_telecare_admin_admin_company_address" for="x_admin_company_address" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_address->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_address->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_address">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_address" data-page="2" name="x_admin_company_address" id="x_admin_company_address" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_address->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_address->EditValue ?>"<?php echo $telecare_admin->admin_company_address->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_address->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_region_id->Visible) { // admin_company_region_id ?>
	<div id="r_admin_company_region_id" class="form-group">
		<label id="elh_telecare_admin_admin_company_region_id" for="x_admin_company_region_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_region_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_region_id->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_region_id">
<?php $telecare_admin->admin_company_region_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_admin->admin_company_region_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_admin" data-field="x_admin_company_region_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_company_region_id->DisplayValueSeparator) ? json_encode($telecare_admin->admin_company_region_id->DisplayValueSeparator) : $telecare_admin->admin_company_region_id->DisplayValueSeparator) ?>" id="x_admin_company_region_id" name="x_admin_company_region_id"<?php echo $telecare_admin->admin_company_region_id->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_company_region_id->EditValue)) {
	$arwrk = $telecare_admin->admin_company_region_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_company_region_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_company_region_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_company_region_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_company_region_id->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_company_region_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
}
$telecare_admin->admin_company_region_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_company_region_id->LookupFilters += array("f0" => "`region_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_company_region_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_admin->admin_company_region_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_company_region_id" id="s_x_admin_company_region_id" value="<?php echo $telecare_admin->admin_company_region_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_admin->admin_company_region_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_province_id->Visible) { // admin_company_province_id ?>
	<div id="r_admin_company_province_id" class="form-group">
		<label id="elh_telecare_admin_admin_company_province_id" for="x_admin_company_province_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_province_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_province_id->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_province_id">
<?php $telecare_admin->admin_company_province_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_admin->admin_company_province_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_admin" data-field="x_admin_company_province_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_company_province_id->DisplayValueSeparator) ? json_encode($telecare_admin->admin_company_province_id->DisplayValueSeparator) : $telecare_admin->admin_company_province_id->DisplayValueSeparator) ?>" id="x_admin_company_province_id" name="x_admin_company_province_id"<?php echo $telecare_admin->admin_company_province_id->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_company_province_id->EditValue)) {
	$arwrk = $telecare_admin->admin_company_province_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_company_province_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_company_province_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_company_province_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_company_province_id->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_company_province_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_admin->admin_company_province_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_company_province_id->LookupFilters += array("f0" => "`province_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_admin->admin_company_province_id->LookupFilters += array("f1" => "`province_region_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_company_province_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_admin->admin_company_province_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_company_province_id" id="s_x_admin_company_province_id" value="<?php echo $telecare_admin->admin_company_province_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_admin->admin_company_province_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_city_id->Visible) { // admin_company_city_id ?>
	<div id="r_admin_company_city_id" class="form-group">
		<label id="elh_telecare_admin_admin_company_city_id" for="x_admin_company_city_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_city_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_city_id->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_city_id">
<select data-table="telecare_admin" data-field="x_admin_company_city_id" data-page="2" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_company_city_id->DisplayValueSeparator) ? json_encode($telecare_admin->admin_company_city_id->DisplayValueSeparator) : $telecare_admin->admin_company_city_id->DisplayValueSeparator) ?>" id="x_admin_company_city_id" name="x_admin_company_city_id"<?php echo $telecare_admin->admin_company_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_company_city_id->EditValue)) {
	$arwrk = $telecare_admin->admin_company_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_company_city_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_company_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_company_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_company_city_id->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_company_city_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_admin->admin_company_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_admin->admin_company_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_admin->admin_company_city_id->LookupFilters += array("f1" => "`city_province_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_admin->Lookup_Selecting($telecare_admin->admin_company_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_admin->admin_company_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_admin_company_city_id" id="s_x_admin_company_city_id" value="<?php echo $telecare_admin->admin_company_city_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_admin->admin_company_city_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_phone->Visible) { // admin_company_phone ?>
	<div id="r_admin_company_phone" class="form-group">
		<label id="elh_telecare_admin_admin_company_phone" for="x_admin_company_phone" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_phone->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_phone->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_phone">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_phone" data-page="2" name="x_admin_company_phone" id="x_admin_company_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_phone->EditValue ?>"<?php echo $telecare_admin->admin_company_phone->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_phone->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_email->Visible) { // admin_company_email ?>
	<div id="r_admin_company_email" class="form-group">
		<label id="elh_telecare_admin_admin_company_email" for="x_admin_company_email" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_email->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_email->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_email">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_email" data-page="2" name="x_admin_company_email" id="x_admin_company_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_email->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_email->EditValue ?>"<?php echo $telecare_admin->admin_company_email->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_web->Visible) { // admin_company_web ?>
	<div id="r_admin_company_web" class="form-group">
		<label id="elh_telecare_admin_admin_company_web" for="x_admin_company_web" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_web->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_web->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_web">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_web" data-page="2" name="x_admin_company_web" id="x_admin_company_web" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_web->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_web->EditValue ?>"<?php echo $telecare_admin->admin_company_web->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_web->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_contract->Visible) { // admin_contract ?>
	<div id="r_admin_contract" class="form-group">
		<label id="elh_telecare_admin_admin_contract" for="x_admin_contract" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_contract->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_contract->CellAttributes() ?>>
<span id="el_telecare_admin_admin_contract">
<input type="text" data-table="telecare_admin" data-field="x_admin_contract" data-page="2" data-format="7" name="x_admin_contract" id="x_admin_contract" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_contract->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_contract->EditValue ?>"<?php echo $telecare_admin->admin_contract->EditAttributes() ?>>
<?php if (!$telecare_admin->admin_contract->ReadOnly && !$telecare_admin->admin_contract->Disabled && !isset($telecare_admin->admin_contract->EditAttrs["readonly"]) && !isset($telecare_admin->admin_contract->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_adminadd", "x_admin_contract", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_admin->admin_contract->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_expire->Visible) { // admin_expire ?>
	<div id="r_admin_expire" class="form-group">
		<label id="elh_telecare_admin_admin_expire" for="x_admin_expire" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_expire->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_expire->CellAttributes() ?>>
<span id="el_telecare_admin_admin_expire">
<input type="text" data-table="telecare_admin" data-field="x_admin_expire" data-page="2" data-format="7" name="x_admin_expire" id="x_admin_expire" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_expire->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_expire->EditValue ?>"<?php echo $telecare_admin->admin_expire->EditAttributes() ?>>
<?php if (!$telecare_admin->admin_expire->ReadOnly && !$telecare_admin->admin_expire->Disabled && !isset($telecare_admin->admin_expire->EditAttrs["readonly"]) && !isset($telecare_admin->admin_expire->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_adminadd", "x_admin_expire", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_admin->admin_expire->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_admin_add->MultiPages->PageStyle("3") ?>" id="tab_telecare_admin3">
<div>
<?php if ($telecare_admin->admin_company_facebook->Visible) { // admin_company_facebook ?>
	<div id="r_admin_company_facebook" class="form-group">
		<label id="elh_telecare_admin_admin_company_facebook" for="x_admin_company_facebook" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_facebook->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_facebook->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_facebook">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_facebook" data-page="3" name="x_admin_company_facebook" id="x_admin_company_facebook" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_facebook->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_facebook->EditValue ?>"<?php echo $telecare_admin->admin_company_facebook->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_facebook->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_twitter->Visible) { // admin_company_twitter ?>
	<div id="r_admin_company_twitter" class="form-group">
		<label id="elh_telecare_admin_admin_company_twitter" for="x_admin_company_twitter" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_twitter->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_twitter->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_twitter">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_twitter" data-page="3" name="x_admin_company_twitter" id="x_admin_company_twitter" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_twitter->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_twitter->EditValue ?>"<?php echo $telecare_admin->admin_company_twitter->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_twitter->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_googleplus->Visible) { // admin_company_googleplus ?>
	<div id="r_admin_company_googleplus" class="form-group">
		<label id="elh_telecare_admin_admin_company_googleplus" for="x_admin_company_googleplus" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_googleplus->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_googleplus->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_googleplus">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_googleplus" data-page="3" name="x_admin_company_googleplus" id="x_admin_company_googleplus" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_googleplus->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_googleplus->EditValue ?>"<?php echo $telecare_admin->admin_company_googleplus->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_googleplus->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_company_linkedin->Visible) { // admin_company_linkedin ?>
	<div id="r_admin_company_linkedin" class="form-group">
		<label id="elh_telecare_admin_admin_company_linkedin" for="x_admin_company_linkedin" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_company_linkedin->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_company_linkedin->CellAttributes() ?>>
<span id="el_telecare_admin_admin_company_linkedin">
<input type="text" data-table="telecare_admin" data-field="x_admin_company_linkedin" data-page="3" name="x_admin_company_linkedin" id="x_admin_company_linkedin" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_company_linkedin->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_company_linkedin->EditValue ?>"<?php echo $telecare_admin->admin_company_linkedin->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_company_linkedin->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_link_playstore->Visible) { // admin_link_playstore ?>
	<div id="r_admin_link_playstore" class="form-group">
		<label id="elh_telecare_admin_admin_link_playstore" for="x_admin_link_playstore" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_link_playstore->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_link_playstore->CellAttributes() ?>>
<span id="el_telecare_admin_admin_link_playstore">
<input type="text" data-table="telecare_admin" data-field="x_admin_link_playstore" data-page="3" name="x_admin_link_playstore" id="x_admin_link_playstore" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_link_playstore->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_link_playstore->EditValue ?>"<?php echo $telecare_admin->admin_link_playstore->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_link_playstore->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_link_appstore->Visible) { // admin_link_appstore ?>
	<div id="r_admin_link_appstore" class="form-group">
		<label id="elh_telecare_admin_admin_link_appstore" for="x_admin_link_appstore" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_link_appstore->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_link_appstore->CellAttributes() ?>>
<span id="el_telecare_admin_admin_link_appstore">
<input type="text" data-table="telecare_admin" data-field="x_admin_link_appstore" data-page="3" name="x_admin_link_appstore" id="x_admin_link_appstore" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_link_appstore->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_link_appstore->EditValue ?>"<?php echo $telecare_admin->admin_link_appstore->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_link_appstore->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_link_windowsstore->Visible) { // admin_link_windowsstore ?>
	<div id="r_admin_link_windowsstore" class="form-group">
		<label id="elh_telecare_admin_admin_link_windowsstore" for="x_admin_link_windowsstore" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_link_windowsstore->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_link_windowsstore->CellAttributes() ?>>
<span id="el_telecare_admin_admin_link_windowsstore">
<input type="text" data-table="telecare_admin" data-field="x_admin_link_windowsstore" data-page="3" name="x_admin_link_windowsstore" id="x_admin_link_windowsstore" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_link_windowsstore->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_link_windowsstore->EditValue ?>"<?php echo $telecare_admin->admin_link_windowsstore->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_link_windowsstore->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_admin_add->MultiPages->PageStyle("4") ?>" id="tab_telecare_admin4">
<div>
<?php if ($telecare_admin->admin_level->Visible) { // admin_level ?>
	<div id="r_admin_level" class="form-group">
		<label id="elh_telecare_admin_admin_level" for="x_admin_level" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_level->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_level->CellAttributes() ?>>
<?php if (!$Security->IsAdmin() && $Security->IsLoggedIn()) { // Non system admin ?>
<span id="el_telecare_admin_admin_level">
<p class="form-control-static"><?php echo $telecare_admin->admin_level->EditValue ?></p>
</span>
<?php } else { ?>
<span id="el_telecare_admin_admin_level">
<select data-table="telecare_admin" data-field="x_admin_level" data-page="4" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_admin->admin_level->DisplayValueSeparator) ? json_encode($telecare_admin->admin_level->DisplayValueSeparator) : $telecare_admin->admin_level->DisplayValueSeparator) ?>" id="x_admin_level" name="x_admin_level"<?php echo $telecare_admin->admin_level->EditAttributes() ?>>
<?php
if (is_array($telecare_admin->admin_level->EditValue)) {
	$arwrk = $telecare_admin->admin_level->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_admin->admin_level->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_admin->admin_level->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_admin->admin_level->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_admin->admin_level->CurrentValue) ?>" selected><?php echo $telecare_admin->admin_level->CurrentValue ?></option>
<?php
    }
}
?>
</select>
</span>
<?php } ?>
<?php echo $telecare_admin->admin_level->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_username->Visible) { // admin_username ?>
	<div id="r_admin_username" class="form-group">
		<label id="elh_telecare_admin_admin_username" for="x_admin_username" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_username->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_username->CellAttributes() ?>>
<span id="el_telecare_admin_admin_username">
<input type="text" data-table="telecare_admin" data-field="x_admin_username" data-page="4" name="x_admin_username" id="x_admin_username" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_username->getPlaceHolder()) ?>" value="<?php echo $telecare_admin->admin_username->EditValue ?>"<?php echo $telecare_admin->admin_username->EditAttributes() ?>>
</span>
<?php echo $telecare_admin->admin_username->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_admin->admin_password->Visible) { // admin_password ?>
	<div id="r_admin_password" class="form-group">
		<label id="elh_telecare_admin_admin_password" for="x_admin_password" class="col-sm-2 control-label ewLabel"><?php echo $telecare_admin->admin_password->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_admin->admin_password->CellAttributes() ?>>
<span id="el_telecare_admin_admin_password">
<div class="input-group" id="ig_x_admin_password">
<input type="password" data-password-strength="pst_x_admin_password" data-password-generated="pgt_x_admin_password" data-table="telecare_admin" data-field="x_admin_password" data-page="4" name="x_admin_password" id="x_admin_password" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_admin->admin_password->getPlaceHolder()) ?>"<?php echo $telecare_admin->admin_password->EditAttributes() ?>>
<span class="input-group-btn">
	<button type="button" class="btn btn-default ewPasswordGenerator" title="<?php echo ew_HtmlTitle($Language->Phrase("GeneratePassword")) ?>" data-password-field="x_admin_password" data-password-confirm="c_admin_password" data-password-strength="pst_x_admin_password" data-password-generated="pgt_x_admin_password"><?php echo $Language->Phrase("GeneratePassword") ?></button>
</span>
</div>
<span class="help-block" id="pgt_x_admin_password" style="display: none;"></span>
<div class="progress ewPasswordStrengthBar" id="pst_x_admin_password" style="display: none;">
	<div class="progress-bar" role="progressbar"></div>
</div>
</span>
<?php echo $telecare_admin->admin_password->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
	</div>
</div>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_admin_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_adminadd.Init();
</script>
<?php
$telecare_admin_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_admin_add->Page_Terminate();
?>
