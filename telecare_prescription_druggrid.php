<?php include_once "telecare_admininfo.php" ?>
<?php

// Create page object
if (!isset($telecare_prescription_drug_grid)) $telecare_prescription_drug_grid = new ctelecare_prescription_drug_grid();

// Page init
$telecare_prescription_drug_grid->Page_Init();

// Page main
$telecare_prescription_drug_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_prescription_drug_grid->Page_Render();
?>
<?php if ($telecare_prescription_drug->Export == "") { ?>
<script type="text/javascript">

// Form object
var ftelecare_prescription_druggrid = new ew_Form("ftelecare_prescription_druggrid", "grid");
ftelecare_prescription_druggrid.FormKeyCountName = '<?php echo $telecare_prescription_drug_grid->FormKeyCountName ?>';

// Validate form
ftelecare_prescription_druggrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_prescription_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription_drug->prescription_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_prescription_number");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_prescription_drug->prescription_number->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
ftelecare_prescription_druggrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "prescription_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "drug_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "prescription_number", false)) return false;
	return true;
}

// Form_CustomValidate event
ftelecare_prescription_druggrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_prescription_druggrid.ValidateRequired = true;
<?php } else { ?>
ftelecare_prescription_druggrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_prescription_druggrid.Lists["x_drug_id"] = {"LinkField":"x_drug_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_drug_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<?php } ?>
<?php
if ($telecare_prescription_drug->CurrentAction == "gridadd") {
	if ($telecare_prescription_drug->CurrentMode == "copy") {
		$bSelectLimit = $telecare_prescription_drug_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$telecare_prescription_drug_grid->TotalRecs = $telecare_prescription_drug->SelectRecordCount();
			$telecare_prescription_drug_grid->Recordset = $telecare_prescription_drug_grid->LoadRecordset($telecare_prescription_drug_grid->StartRec-1, $telecare_prescription_drug_grid->DisplayRecs);
		} else {
			if ($telecare_prescription_drug_grid->Recordset = $telecare_prescription_drug_grid->LoadRecordset())
				$telecare_prescription_drug_grid->TotalRecs = $telecare_prescription_drug_grid->Recordset->RecordCount();
		}
		$telecare_prescription_drug_grid->StartRec = 1;
		$telecare_prescription_drug_grid->DisplayRecs = $telecare_prescription_drug_grid->TotalRecs;
	} else {
		$telecare_prescription_drug->CurrentFilter = "0=1";
		$telecare_prescription_drug_grid->StartRec = 1;
		$telecare_prescription_drug_grid->DisplayRecs = $telecare_prescription_drug->GridAddRowCount;
	}
	$telecare_prescription_drug_grid->TotalRecs = $telecare_prescription_drug_grid->DisplayRecs;
	$telecare_prescription_drug_grid->StopRec = $telecare_prescription_drug_grid->DisplayRecs;
} else {
	$bSelectLimit = $telecare_prescription_drug_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($telecare_prescription_drug_grid->TotalRecs <= 0)
			$telecare_prescription_drug_grid->TotalRecs = $telecare_prescription_drug->SelectRecordCount();
	} else {
		if (!$telecare_prescription_drug_grid->Recordset && ($telecare_prescription_drug_grid->Recordset = $telecare_prescription_drug_grid->LoadRecordset()))
			$telecare_prescription_drug_grid->TotalRecs = $telecare_prescription_drug_grid->Recordset->RecordCount();
	}
	$telecare_prescription_drug_grid->StartRec = 1;
	$telecare_prescription_drug_grid->DisplayRecs = $telecare_prescription_drug_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$telecare_prescription_drug_grid->Recordset = $telecare_prescription_drug_grid->LoadRecordset($telecare_prescription_drug_grid->StartRec-1, $telecare_prescription_drug_grid->DisplayRecs);

	// Set no record found message
	if ($telecare_prescription_drug->CurrentAction == "" && $telecare_prescription_drug_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$telecare_prescription_drug_grid->setWarningMessage($Language->Phrase("NoPermission"));
		if ($telecare_prescription_drug_grid->SearchWhere == "0=101")
			$telecare_prescription_drug_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$telecare_prescription_drug_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$telecare_prescription_drug_grid->RenderOtherOptions();
?>
<?php $telecare_prescription_drug_grid->ShowPageHeader(); ?>
<?php
$telecare_prescription_drug_grid->ShowMessage();
?>
<?php if ($telecare_prescription_drug_grid->TotalRecs > 0 || $telecare_prescription_drug->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="ftelecare_prescription_druggrid" class="ewForm form-inline">
<?php if ($telecare_prescription_drug_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($telecare_prescription_drug_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_telecare_prescription_drug" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_telecare_prescription_druggrid" class="table ewTable">
<?php echo $telecare_prescription_drug->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$telecare_prescription_drug_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$telecare_prescription_drug_grid->RenderListOptions();

// Render list options (header, left)
$telecare_prescription_drug_grid->ListOptions->Render("header", "left");
?>
<?php if ($telecare_prescription_drug->prescription_id->Visible) { // prescription_id ?>
	<?php if ($telecare_prescription_drug->SortUrl($telecare_prescription_drug->prescription_id) == "") { ?>
		<th data-name="prescription_id"><div id="elh_telecare_prescription_drug_prescription_id" class="telecare_prescription_drug_prescription_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->prescription_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_id"><div><div id="elh_telecare_prescription_drug_prescription_id" class="telecare_prescription_drug_prescription_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->prescription_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription_drug->prescription_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription_drug->prescription_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription_drug->drug_id->Visible) { // drug_id ?>
	<?php if ($telecare_prescription_drug->SortUrl($telecare_prescription_drug->drug_id) == "") { ?>
		<th data-name="drug_id"><div id="elh_telecare_prescription_drug_drug_id" class="telecare_prescription_drug_drug_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->drug_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="drug_id"><div><div id="elh_telecare_prescription_drug_drug_id" class="telecare_prescription_drug_drug_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->drug_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription_drug->drug_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription_drug->drug_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription_drug->prescription_drug_id->Visible) { // prescription_drug_id ?>
	<?php if ($telecare_prescription_drug->SortUrl($telecare_prescription_drug->prescription_drug_id) == "") { ?>
		<th data-name="prescription_drug_id"><div id="elh_telecare_prescription_drug_prescription_drug_id" class="telecare_prescription_drug_prescription_drug_id"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->prescription_drug_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_drug_id"><div><div id="elh_telecare_prescription_drug_prescription_drug_id" class="telecare_prescription_drug_prescription_drug_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->prescription_drug_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription_drug->prescription_drug_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription_drug->prescription_drug_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_prescription_drug->prescription_number->Visible) { // prescription_number ?>
	<?php if ($telecare_prescription_drug->SortUrl($telecare_prescription_drug->prescription_number) == "") { ?>
		<th data-name="prescription_number"><div id="elh_telecare_prescription_drug_prescription_number" class="telecare_prescription_drug_prescription_number"><div class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->prescription_number->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="prescription_number"><div><div id="elh_telecare_prescription_drug_prescription_number" class="telecare_prescription_drug_prescription_number">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_prescription_drug->prescription_number->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_prescription_drug->prescription_number->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_prescription_drug->prescription_number->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$telecare_prescription_drug_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$telecare_prescription_drug_grid->StartRec = 1;
$telecare_prescription_drug_grid->StopRec = $telecare_prescription_drug_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($telecare_prescription_drug_grid->FormKeyCountName) && ($telecare_prescription_drug->CurrentAction == "gridadd" || $telecare_prescription_drug->CurrentAction == "gridedit" || $telecare_prescription_drug->CurrentAction == "F")) {
		$telecare_prescription_drug_grid->KeyCount = $objForm->GetValue($telecare_prescription_drug_grid->FormKeyCountName);
		$telecare_prescription_drug_grid->StopRec = $telecare_prescription_drug_grid->StartRec + $telecare_prescription_drug_grid->KeyCount - 1;
	}
}
$telecare_prescription_drug_grid->RecCnt = $telecare_prescription_drug_grid->StartRec - 1;
if ($telecare_prescription_drug_grid->Recordset && !$telecare_prescription_drug_grid->Recordset->EOF) {
	$telecare_prescription_drug_grid->Recordset->MoveFirst();
	$bSelectLimit = $telecare_prescription_drug_grid->UseSelectLimit;
	if (!$bSelectLimit && $telecare_prescription_drug_grid->StartRec > 1)
		$telecare_prescription_drug_grid->Recordset->Move($telecare_prescription_drug_grid->StartRec - 1);
} elseif (!$telecare_prescription_drug->AllowAddDeleteRow && $telecare_prescription_drug_grid->StopRec == 0) {
	$telecare_prescription_drug_grid->StopRec = $telecare_prescription_drug->GridAddRowCount;
}

// Initialize aggregate
$telecare_prescription_drug->RowType = EW_ROWTYPE_AGGREGATEINIT;
$telecare_prescription_drug->ResetAttrs();
$telecare_prescription_drug_grid->RenderRow();
if ($telecare_prescription_drug->CurrentAction == "gridadd")
	$telecare_prescription_drug_grid->RowIndex = 0;
if ($telecare_prescription_drug->CurrentAction == "gridedit")
	$telecare_prescription_drug_grid->RowIndex = 0;
while ($telecare_prescription_drug_grid->RecCnt < $telecare_prescription_drug_grid->StopRec) {
	$telecare_prescription_drug_grid->RecCnt++;
	if (intval($telecare_prescription_drug_grid->RecCnt) >= intval($telecare_prescription_drug_grid->StartRec)) {
		$telecare_prescription_drug_grid->RowCnt++;
		if ($telecare_prescription_drug->CurrentAction == "gridadd" || $telecare_prescription_drug->CurrentAction == "gridedit" || $telecare_prescription_drug->CurrentAction == "F") {
			$telecare_prescription_drug_grid->RowIndex++;
			$objForm->Index = $telecare_prescription_drug_grid->RowIndex;
			if ($objForm->HasValue($telecare_prescription_drug_grid->FormActionName))
				$telecare_prescription_drug_grid->RowAction = strval($objForm->GetValue($telecare_prescription_drug_grid->FormActionName));
			elseif ($telecare_prescription_drug->CurrentAction == "gridadd")
				$telecare_prescription_drug_grid->RowAction = "insert";
			else
				$telecare_prescription_drug_grid->RowAction = "";
		}

		// Set up key count
		$telecare_prescription_drug_grid->KeyCount = $telecare_prescription_drug_grid->RowIndex;

		// Init row class and style
		$telecare_prescription_drug->ResetAttrs();
		$telecare_prescription_drug->CssClass = "";
		if ($telecare_prescription_drug->CurrentAction == "gridadd") {
			if ($telecare_prescription_drug->CurrentMode == "copy") {
				$telecare_prescription_drug_grid->LoadRowValues($telecare_prescription_drug_grid->Recordset); // Load row values
				$telecare_prescription_drug_grid->SetRecordKey($telecare_prescription_drug_grid->RowOldKey, $telecare_prescription_drug_grid->Recordset); // Set old record key
			} else {
				$telecare_prescription_drug_grid->LoadDefaultValues(); // Load default values
				$telecare_prescription_drug_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$telecare_prescription_drug_grid->LoadRowValues($telecare_prescription_drug_grid->Recordset); // Load row values
		}
		$telecare_prescription_drug->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($telecare_prescription_drug->CurrentAction == "gridadd") // Grid add
			$telecare_prescription_drug->RowType = EW_ROWTYPE_ADD; // Render add
		if ($telecare_prescription_drug->CurrentAction == "gridadd" && $telecare_prescription_drug->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$telecare_prescription_drug_grid->RestoreCurrentRowFormValues($telecare_prescription_drug_grid->RowIndex); // Restore form values
		if ($telecare_prescription_drug->CurrentAction == "gridedit") { // Grid edit
			if ($telecare_prescription_drug->EventCancelled) {
				$telecare_prescription_drug_grid->RestoreCurrentRowFormValues($telecare_prescription_drug_grid->RowIndex); // Restore form values
			}
			if ($telecare_prescription_drug_grid->RowAction == "insert")
				$telecare_prescription_drug->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$telecare_prescription_drug->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($telecare_prescription_drug->CurrentAction == "gridedit" && ($telecare_prescription_drug->RowType == EW_ROWTYPE_EDIT || $telecare_prescription_drug->RowType == EW_ROWTYPE_ADD) && $telecare_prescription_drug->EventCancelled) // Update failed
			$telecare_prescription_drug_grid->RestoreCurrentRowFormValues($telecare_prescription_drug_grid->RowIndex); // Restore form values
		if ($telecare_prescription_drug->RowType == EW_ROWTYPE_EDIT) // Edit row
			$telecare_prescription_drug_grid->EditRowCnt++;
		if ($telecare_prescription_drug->CurrentAction == "F") // Confirm row
			$telecare_prescription_drug_grid->RestoreCurrentRowFormValues($telecare_prescription_drug_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$telecare_prescription_drug->RowAttrs = array_merge($telecare_prescription_drug->RowAttrs, array('data-rowindex'=>$telecare_prescription_drug_grid->RowCnt, 'id'=>'r' . $telecare_prescription_drug_grid->RowCnt . '_telecare_prescription_drug', 'data-rowtype'=>$telecare_prescription_drug->RowType));

		// Render row
		$telecare_prescription_drug_grid->RenderRow();

		// Render list options
		$telecare_prescription_drug_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($telecare_prescription_drug_grid->RowAction <> "delete" && $telecare_prescription_drug_grid->RowAction <> "insertdelete" && !($telecare_prescription_drug_grid->RowAction == "insert" && $telecare_prescription_drug->CurrentAction == "F" && $telecare_prescription_drug_grid->EmptyRow())) {
?>
	<tr<?php echo $telecare_prescription_drug->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_prescription_drug_grid->ListOptions->Render("body", "left", $telecare_prescription_drug_grid->RowCnt);
?>
	<?php if ($telecare_prescription_drug->prescription_id->Visible) { // prescription_id ?>
		<td data-name="prescription_id"<?php echo $telecare_prescription_drug->prescription_id->CellAttributes() ?>>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($telecare_prescription_drug->prescription_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_id" class="form-group telecare_prescription_drug_prescription_id">
<span<?php echo $telecare_prescription_drug->prescription_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_id" class="form-group telecare_prescription_drug_prescription_id">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_id->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_id->EditAttributes() ?>>
</span>
<?php } ?>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($telecare_prescription_drug->prescription_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_id" class="form-group telecare_prescription_drug_prescription_id">
<span<?php echo $telecare_prescription_drug->prescription_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_id" class="form-group telecare_prescription_drug_prescription_id">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_id->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_id->EditAttributes() ?>>
</span>
<?php } ?>
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_id" class="telecare_prescription_drug_prescription_id">
<span<?php echo $telecare_prescription_drug->prescription_id->ViewAttributes() ?>>
<?php echo $telecare_prescription_drug->prescription_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $telecare_prescription_drug_grid->PageObjName . "_row_" . $telecare_prescription_drug_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($telecare_prescription_drug->drug_id->Visible) { // drug_id ?>
		<td data-name="drug_id"<?php echo $telecare_prescription_drug->drug_id->CellAttributes() ?>>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_drug_id" class="form-group telecare_prescription_drug_drug_id">
<select data-table="telecare_prescription_drug" data-field="x_drug_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription_drug->drug_id->DisplayValueSeparator) ? json_encode($telecare_prescription_drug->drug_id->DisplayValueSeparator) : $telecare_prescription_drug->drug_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id"<?php echo $telecare_prescription_drug->drug_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription_drug->drug_id->EditValue)) {
	$arwrk = $telecare_prescription_drug->drug_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription_drug->drug_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription_drug->drug_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription_drug->drug_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->CurrentValue) ?>" selected><?php echo $telecare_prescription_drug->drug_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription_drug->drug_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription_drug->drug_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription_drug->drug_id->LookupFilters += array("f0" => "`drug_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription_drug->Lookup_Selecting($telecare_prescription_drug->drug_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `drug_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription_drug->drug_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="s_x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo $telecare_prescription_drug->drug_id->LookupFilterQuery() ?>">
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_drug_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_drug_id" class="form-group telecare_prescription_drug_drug_id">
<select data-table="telecare_prescription_drug" data-field="x_drug_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription_drug->drug_id->DisplayValueSeparator) ? json_encode($telecare_prescription_drug->drug_id->DisplayValueSeparator) : $telecare_prescription_drug->drug_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id"<?php echo $telecare_prescription_drug->drug_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription_drug->drug_id->EditValue)) {
	$arwrk = $telecare_prescription_drug->drug_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription_drug->drug_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription_drug->drug_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription_drug->drug_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->CurrentValue) ?>" selected><?php echo $telecare_prescription_drug->drug_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription_drug->drug_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription_drug->drug_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription_drug->drug_id->LookupFilters += array("f0" => "`drug_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription_drug->Lookup_Selecting($telecare_prescription_drug->drug_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `drug_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription_drug->drug_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="s_x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo $telecare_prescription_drug->drug_id->LookupFilterQuery() ?>">
</span>
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_drug_id" class="telecare_prescription_drug_drug_id">
<span<?php echo $telecare_prescription_drug->drug_id->ViewAttributes() ?>>
<?php echo $telecare_prescription_drug->drug_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_drug_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription_drug->prescription_drug_id->Visible) { // prescription_drug_id ?>
		<td data-name="prescription_drug_id"<?php echo $telecare_prescription_drug->prescription_drug_id->CellAttributes() ?>>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_drug_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_drug_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_drug_id" class="form-group telecare_prescription_drug_prescription_drug_id">
<span<?php echo $telecare_prescription_drug->prescription_drug_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_drug_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_drug_id->CurrentValue) ?>">
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_drug_id" class="telecare_prescription_drug_prescription_drug_id">
<span<?php echo $telecare_prescription_drug->prescription_drug_id->ViewAttributes() ?>>
<?php echo $telecare_prescription_drug->prescription_drug_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_drug_id->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_drug_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_drug_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_prescription_drug->prescription_number->Visible) { // prescription_number ?>
		<td data-name="prescription_number"<?php echo $telecare_prescription_drug->prescription_number->CellAttributes() ?>>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_number" class="form-group telecare_prescription_drug_prescription_number">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_number->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_number->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->OldValue) ?>">
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_number" class="form-group telecare_prescription_drug_prescription_number">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_number->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_number->EditAttributes() ?>>
</span>
<?php } ?>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_prescription_drug_grid->RowCnt ?>_telecare_prescription_drug_prescription_number" class="telecare_prescription_drug_prescription_number">
<span<?php echo $telecare_prescription_drug->prescription_number->ViewAttributes() ?>>
<?php echo $telecare_prescription_drug->prescription_number->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->FormValue) ?>">
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_prescription_drug_grid->ListOptions->Render("body", "right", $telecare_prescription_drug_grid->RowCnt);
?>
	</tr>
<?php if ($telecare_prescription_drug->RowType == EW_ROWTYPE_ADD || $telecare_prescription_drug->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
ftelecare_prescription_druggrid.UpdateOpts(<?php echo $telecare_prescription_drug_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($telecare_prescription_drug->CurrentAction <> "gridadd" || $telecare_prescription_drug->CurrentMode == "copy")
		if (!$telecare_prescription_drug_grid->Recordset->EOF) $telecare_prescription_drug_grid->Recordset->MoveNext();
}
?>
<?php
	if ($telecare_prescription_drug->CurrentMode == "add" || $telecare_prescription_drug->CurrentMode == "copy" || $telecare_prescription_drug->CurrentMode == "edit") {
		$telecare_prescription_drug_grid->RowIndex = '$rowindex$';
		$telecare_prescription_drug_grid->LoadDefaultValues();

		// Set row properties
		$telecare_prescription_drug->ResetAttrs();
		$telecare_prescription_drug->RowAttrs = array_merge($telecare_prescription_drug->RowAttrs, array('data-rowindex'=>$telecare_prescription_drug_grid->RowIndex, 'id'=>'r0_telecare_prescription_drug', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($telecare_prescription_drug->RowAttrs["class"], "ewTemplate");
		$telecare_prescription_drug->RowType = EW_ROWTYPE_ADD;

		// Render row
		$telecare_prescription_drug_grid->RenderRow();

		// Render list options
		$telecare_prescription_drug_grid->RenderListOptions();
		$telecare_prescription_drug_grid->StartRowCnt = 0;
?>
	<tr<?php echo $telecare_prescription_drug->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_prescription_drug_grid->ListOptions->Render("body", "left", $telecare_prescription_drug_grid->RowIndex);
?>
	<?php if ($telecare_prescription_drug->prescription_id->Visible) { // prescription_id ?>
		<td data-name="prescription_id">
<?php if ($telecare_prescription_drug->CurrentAction <> "F") { ?>
<?php if ($telecare_prescription_drug->prescription_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_telecare_prescription_drug_prescription_id" class="form-group telecare_prescription_drug_prescription_id">
<span<?php echo $telecare_prescription_drug->prescription_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_drug_prescription_id" class="form-group telecare_prescription_drug_prescription_id">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_id->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_id->EditAttributes() ?>>
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_drug_prescription_id" class="form-group telecare_prescription_drug_prescription_id">
<span<?php echo $telecare_prescription_drug->prescription_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription_drug->drug_id->Visible) { // drug_id ?>
		<td data-name="drug_id">
<?php if ($telecare_prescription_drug->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_prescription_drug_drug_id" class="form-group telecare_prescription_drug_drug_id">
<select data-table="telecare_prescription_drug" data-field="x_drug_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_prescription_drug->drug_id->DisplayValueSeparator) ? json_encode($telecare_prescription_drug->drug_id->DisplayValueSeparator) : $telecare_prescription_drug->drug_id->DisplayValueSeparator) ?>" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id"<?php echo $telecare_prescription_drug->drug_id->EditAttributes() ?>>
<?php
if (is_array($telecare_prescription_drug->drug_id->EditValue)) {
	$arwrk = $telecare_prescription_drug->drug_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_prescription_drug->drug_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_prescription_drug->drug_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_prescription_drug->drug_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->CurrentValue) ?>" selected><?php echo $telecare_prescription_drug->drug_id->CurrentValue ?></option>
<?php
    }
}
if (@$emptywrk) $telecare_prescription_drug->drug_id->OldValue = "";
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `drug_id`, `drug_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_drug`";
		$sWhereWrk = "";
		break;
}
$telecare_prescription_drug->drug_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_prescription_drug->drug_id->LookupFilters += array("f0" => "`drug_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_prescription_drug->Lookup_Selecting($telecare_prescription_drug->drug_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `drug_name` ASC";
if ($sSqlWrk <> "") $telecare_prescription_drug->drug_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="s_x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo $telecare_prescription_drug->drug_id->LookupFilterQuery() ?>">
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_drug_drug_id" class="form-group telecare_prescription_drug_drug_id">
<span<?php echo $telecare_prescription_drug->drug_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->drug_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_drug_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->drug_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription_drug->prescription_drug_id->Visible) { // prescription_drug_id ?>
		<td data-name="prescription_drug_id">
<?php if ($telecare_prescription_drug->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_drug_prescription_drug_id" class="form-group telecare_prescription_drug_prescription_drug_id">
<span<?php echo $telecare_prescription_drug->prescription_drug_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_drug_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_drug_id" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_drug_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_drug_id" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_drug_id" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_drug_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_prescription_drug->prescription_number->Visible) { // prescription_number ?>
		<td data-name="prescription_number">
<?php if ($telecare_prescription_drug->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_prescription_drug_prescription_number" class="form-group telecare_prescription_drug_prescription_number">
<input type="text" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->getPlaceHolder()) ?>" value="<?php echo $telecare_prescription_drug->prescription_number->EditValue ?>"<?php echo $telecare_prescription_drug->prescription_number->EditAttributes() ?>>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_prescription_drug_prescription_number" class="form-group telecare_prescription_drug_prescription_number">
<span<?php echo $telecare_prescription_drug->prescription_number->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_prescription_drug->prescription_number->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="x<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_prescription_drug" data-field="x_prescription_number" name="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" id="o<?php echo $telecare_prescription_drug_grid->RowIndex ?>_prescription_number" value="<?php echo ew_HtmlEncode($telecare_prescription_drug->prescription_number->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_prescription_drug_grid->ListOptions->Render("body", "right", $telecare_prescription_drug_grid->RowCnt);
?>
<script type="text/javascript">
ftelecare_prescription_druggrid.UpdateOpts(<?php echo $telecare_prescription_drug_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($telecare_prescription_drug->CurrentMode == "add" || $telecare_prescription_drug->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $telecare_prescription_drug_grid->FormKeyCountName ?>" id="<?php echo $telecare_prescription_drug_grid->FormKeyCountName ?>" value="<?php echo $telecare_prescription_drug_grid->KeyCount ?>">
<?php echo $telecare_prescription_drug_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_prescription_drug->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $telecare_prescription_drug_grid->FormKeyCountName ?>" id="<?php echo $telecare_prescription_drug_grid->FormKeyCountName ?>" value="<?php echo $telecare_prescription_drug_grid->KeyCount ?>">
<?php echo $telecare_prescription_drug_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_prescription_drug->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="ftelecare_prescription_druggrid">
</div>
<?php

// Close recordset
if ($telecare_prescription_drug_grid->Recordset)
	$telecare_prescription_drug_grid->Recordset->Close();
?>
<?php if ($telecare_prescription_drug_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($telecare_prescription_drug_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($telecare_prescription_drug_grid->TotalRecs == 0 && $telecare_prescription_drug->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_prescription_drug_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($telecare_prescription_drug->Export == "") { ?>
<script type="text/javascript">
ftelecare_prescription_druggrid.Init();
</script>
<?php } ?>
<?php
$telecare_prescription_drug_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$telecare_prescription_drug_grid->Page_Terminate();
?>
