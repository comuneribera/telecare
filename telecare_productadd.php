<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_productinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_product_add = NULL; // Initialize page object first

class ctelecare_product_add extends ctelecare_product {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_product';

	// Page object name
	var $PageObjName = 'telecare_product_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_product)
		if (!isset($GLOBALS["telecare_product"]) || get_class($GLOBALS["telecare_product"]) == "ctelecare_product") {
			$GLOBALS["telecare_product"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_product"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_product', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_productlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_product;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_product);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["product_id"] != "") {
				$this->product_id->setQueryStringValue($_GET["product_id"]);
				$this->setKey("product_id", $this->product_id->CurrentValue); // Set up key
			} else {
				$this->setKey("product_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_productlist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_productview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->product_attach_file->Upload->Index = $objForm->Index;
		$this->product_attach_file->Upload->UploadFile();
		$this->product_attach_file->CurrentValue = $this->product_attach_file->Upload->FileName;
		$this->product_attach_type->CurrentValue = $this->product_attach_file->Upload->ContentType;
		$this->product_attach_size->CurrentValue = $this->product_attach_file->Upload->FileSize;
	}

	// Load default values
	function LoadDefaultValues() {
		$this->product_admin_id->CurrentValue = NULL;
		$this->product_admin_id->OldValue = $this->product_admin_id->CurrentValue;
		$this->product_type_id->CurrentValue = NULL;
		$this->product_type_id->OldValue = $this->product_type_id->CurrentValue;
		$this->product_user_id->CurrentValue = NULL;
		$this->product_user_id->OldValue = $this->product_user_id->CurrentValue;
		$this->product_name->CurrentValue = NULL;
		$this->product_name->OldValue = $this->product_name->CurrentValue;
		$this->product_installation_date->CurrentValue = NULL;
		$this->product_installation_date->OldValue = $this->product_installation_date->CurrentValue;
		$this->product_installation_account_id->CurrentValue = NULL;
		$this->product_installation_account_id->OldValue = $this->product_installation_account_id->CurrentValue;
		$this->product_attach_file->Upload->DbValue = NULL;
		$this->product_attach_file->OldValue = $this->product_attach_file->Upload->DbValue;
		$this->product_attach_file->CurrentValue = NULL; // Clear file related field
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->product_admin_id->FldIsDetailKey) {
			$this->product_admin_id->setFormValue($objForm->GetValue("x_product_admin_id"));
		}
		if (!$this->product_type_id->FldIsDetailKey) {
			$this->product_type_id->setFormValue($objForm->GetValue("x_product_type_id"));
		}
		if (!$this->product_user_id->FldIsDetailKey) {
			$this->product_user_id->setFormValue($objForm->GetValue("x_product_user_id"));
		}
		if (!$this->product_name->FldIsDetailKey) {
			$this->product_name->setFormValue($objForm->GetValue("x_product_name"));
		}
		if (!$this->product_installation_date->FldIsDetailKey) {
			$this->product_installation_date->setFormValue($objForm->GetValue("x_product_installation_date"));
			$this->product_installation_date->CurrentValue = ew_UnFormatDateTime($this->product_installation_date->CurrentValue, 7);
		}
		if (!$this->product_installation_account_id->FldIsDetailKey) {
			$this->product_installation_account_id->setFormValue($objForm->GetValue("x_product_installation_account_id"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->product_admin_id->CurrentValue = $this->product_admin_id->FormValue;
		$this->product_type_id->CurrentValue = $this->product_type_id->FormValue;
		$this->product_user_id->CurrentValue = $this->product_user_id->FormValue;
		$this->product_name->CurrentValue = $this->product_name->FormValue;
		$this->product_installation_date->CurrentValue = $this->product_installation_date->FormValue;
		$this->product_installation_date->CurrentValue = ew_UnFormatDateTime($this->product_installation_date->CurrentValue, 7);
		$this->product_installation_account_id->CurrentValue = $this->product_installation_account_id->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->product_id->setDbValue($rs->fields('product_id'));
		$this->product_admin_id->setDbValue($rs->fields('product_admin_id'));
		$this->product_type_id->setDbValue($rs->fields('product_type_id'));
		$this->product_user_id->setDbValue($rs->fields('product_user_id'));
		$this->product_name->setDbValue($rs->fields('product_name'));
		$this->product_installation_date->setDbValue($rs->fields('product_installation_date'));
		$this->product_installation_account_id->setDbValue($rs->fields('product_installation_account_id'));
		$this->product_attach_file->Upload->DbValue = $rs->fields('product_attach_file');
		$this->product_attach_file->CurrentValue = $this->product_attach_file->Upload->DbValue;
		$this->product_attach_type->setDbValue($rs->fields('product_attach_type'));
		$this->product_attach_size->setDbValue($rs->fields('product_attach_size'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->product_id->DbValue = $row['product_id'];
		$this->product_admin_id->DbValue = $row['product_admin_id'];
		$this->product_type_id->DbValue = $row['product_type_id'];
		$this->product_user_id->DbValue = $row['product_user_id'];
		$this->product_name->DbValue = $row['product_name'];
		$this->product_installation_date->DbValue = $row['product_installation_date'];
		$this->product_installation_account_id->DbValue = $row['product_installation_account_id'];
		$this->product_attach_file->Upload->DbValue = $row['product_attach_file'];
		$this->product_attach_type->DbValue = $row['product_attach_type'];
		$this->product_attach_size->DbValue = $row['product_attach_size'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("product_id")) <> "")
			$this->product_id->CurrentValue = $this->getKey("product_id"); // product_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// product_id
		// product_admin_id
		// product_type_id
		// product_user_id
		// product_name
		// product_installation_date
		// product_installation_account_id
		// product_attach_file
		// product_attach_type
		// product_attach_size

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// product_id
		$this->product_id->ViewValue = $this->product_id->CurrentValue;
		$this->product_id->ViewCustomAttributes = "";

		// product_admin_id
		$this->product_admin_id->ViewValue = $this->product_admin_id->CurrentValue;
		$this->product_admin_id->ViewCustomAttributes = "";

		// product_type_id
		if (strval($this->product_type_id->CurrentValue) <> "") {
			$sFilterWrk = "`product_type_id`" . ew_SearchString("=", $this->product_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_type_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `product_type_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->product_type_id->ViewValue = $this->product_type_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_type_id->ViewValue = $this->product_type_id->CurrentValue;
			}
		} else {
			$this->product_type_id->ViewValue = NULL;
		}
		$this->product_type_id->ViewCustomAttributes = "";

		// product_user_id
		if (strval($this->product_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->product_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->product_user_id->ViewValue = $this->product_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_user_id->ViewValue = $this->product_user_id->CurrentValue;
			}
		} else {
			$this->product_user_id->ViewValue = NULL;
		}
		$this->product_user_id->ViewCustomAttributes = "";

		// product_name
		$this->product_name->ViewValue = $this->product_name->CurrentValue;
		$this->product_name->ViewCustomAttributes = "";

		// product_installation_date
		$this->product_installation_date->ViewValue = $this->product_installation_date->CurrentValue;
		$this->product_installation_date->ViewValue = ew_FormatDateTime($this->product_installation_date->ViewValue, 7);
		$this->product_installation_date->ViewCustomAttributes = "";

		// product_installation_account_id
		if (strval($this->product_installation_account_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->product_installation_account_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		$lookuptblfilter = "`admin_level` = 4";
		ew_AddFilter($sWhereWrk, $lookuptblfilter);
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->product_installation_account_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
		$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->product_installation_account_id->ViewValue = $this->product_installation_account_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->product_installation_account_id->ViewValue = $this->product_installation_account_id->CurrentValue;
			}
		} else {
			$this->product_installation_account_id->ViewValue = NULL;
		}
		$this->product_installation_account_id->ViewCustomAttributes = "";

		// product_attach_file
		if (!ew_Empty($this->product_attach_file->Upload->DbValue)) {
			$this->product_attach_file->ViewValue = $this->product_attach_file->Upload->DbValue;
		} else {
			$this->product_attach_file->ViewValue = "";
		}
		$this->product_attach_file->ViewCustomAttributes = "";

			// product_admin_id
			$this->product_admin_id->LinkCustomAttributes = "";
			$this->product_admin_id->HrefValue = "";
			$this->product_admin_id->TooltipValue = "";

			// product_type_id
			$this->product_type_id->LinkCustomAttributes = "";
			$this->product_type_id->HrefValue = "";
			$this->product_type_id->TooltipValue = "";

			// product_user_id
			$this->product_user_id->LinkCustomAttributes = "";
			$this->product_user_id->HrefValue = "";
			$this->product_user_id->TooltipValue = "";

			// product_name
			$this->product_name->LinkCustomAttributes = "";
			$this->product_name->HrefValue = "";
			$this->product_name->TooltipValue = "";

			// product_installation_date
			$this->product_installation_date->LinkCustomAttributes = "";
			$this->product_installation_date->HrefValue = "";
			$this->product_installation_date->TooltipValue = "";

			// product_installation_account_id
			$this->product_installation_account_id->LinkCustomAttributes = "";
			$this->product_installation_account_id->HrefValue = "";
			$this->product_installation_account_id->TooltipValue = "";

			// product_attach_file
			$this->product_attach_file->LinkCustomAttributes = "";
			$this->product_attach_file->HrefValue = "";
			$this->product_attach_file->HrefValue2 = $this->product_attach_file->UploadPath . $this->product_attach_file->Upload->DbValue;
			$this->product_attach_file->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// product_admin_id
			// product_type_id

			$this->product_type_id->EditAttrs["class"] = "form-control";
			$this->product_type_id->EditCustomAttributes = "";
			if (trim(strval($this->product_type_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`product_type_id`" . ew_SearchString("=", $this->product_type_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_product_type`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_product_type`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->product_type_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `product_type_name` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->product_type_id->EditValue = $arwrk;

			// product_user_id
			$this->product_user_id->EditAttrs["class"] = "form-control";
			$this->product_user_id->EditCustomAttributes = "";
			if (trim(strval($this->product_user_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->product_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->product_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `user_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->product_user_id->EditValue = $arwrk;

			// product_name
			$this->product_name->EditAttrs["class"] = "form-control";
			$this->product_name->EditCustomAttributes = "";
			$this->product_name->EditValue = ew_HtmlEncode($this->product_name->CurrentValue);
			$this->product_name->PlaceHolder = ew_RemoveHtml($this->product_name->FldCaption());

			// product_installation_date
			$this->product_installation_date->EditAttrs["class"] = "form-control";
			$this->product_installation_date->EditCustomAttributes = "";
			$this->product_installation_date->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->product_installation_date->CurrentValue, 7));
			$this->product_installation_date->PlaceHolder = ew_RemoveHtml($this->product_installation_date->FldCaption());

			// product_installation_account_id
			$this->product_installation_account_id->EditAttrs["class"] = "form-control";
			$this->product_installation_account_id->EditCustomAttributes = "";
			if (trim(strval($this->product_installation_account_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->product_installation_account_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			$lookuptblfilter = "`admin_level` = 4";
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_product"]->UserIDAllow("add")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->product_installation_account_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$sSqlWrk .= " ORDER BY `admin_surname` ASC";
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->product_installation_account_id->EditValue = $arwrk;

			// product_attach_file
			$this->product_attach_file->EditAttrs["class"] = "form-control";
			$this->product_attach_file->EditCustomAttributes = "";
			if (!ew_Empty($this->product_attach_file->Upload->DbValue)) {
				$this->product_attach_file->EditValue = $this->product_attach_file->Upload->DbValue;
			} else {
				$this->product_attach_file->EditValue = "";
			}
			if (!ew_Empty($this->product_attach_file->CurrentValue))
				$this->product_attach_file->Upload->FileName = $this->product_attach_file->CurrentValue;
			if (($this->CurrentAction == "I" || $this->CurrentAction == "C") && !$this->EventCancelled) ew_RenderUploadField($this->product_attach_file);

			// Edit refer script
			// product_admin_id

			$this->product_admin_id->LinkCustomAttributes = "";
			$this->product_admin_id->HrefValue = "";

			// product_type_id
			$this->product_type_id->LinkCustomAttributes = "";
			$this->product_type_id->HrefValue = "";

			// product_user_id
			$this->product_user_id->LinkCustomAttributes = "";
			$this->product_user_id->HrefValue = "";

			// product_name
			$this->product_name->LinkCustomAttributes = "";
			$this->product_name->HrefValue = "";

			// product_installation_date
			$this->product_installation_date->LinkCustomAttributes = "";
			$this->product_installation_date->HrefValue = "";

			// product_installation_account_id
			$this->product_installation_account_id->LinkCustomAttributes = "";
			$this->product_installation_account_id->HrefValue = "";

			// product_attach_file
			$this->product_attach_file->LinkCustomAttributes = "";
			$this->product_attach_file->HrefValue = "";
			$this->product_attach_file->HrefValue2 = $this->product_attach_file->UploadPath . $this->product_attach_file->Upload->DbValue;
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckEuroDate($this->product_installation_date->FormValue)) {
			ew_AddMessage($gsFormError, $this->product_installation_date->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// product_admin_id
		$this->product_admin_id->SetDbValueDef($rsnew, CurrentUserID(), NULL);
		$rsnew['product_admin_id'] = &$this->product_admin_id->DbValue;

		// product_type_id
		$this->product_type_id->SetDbValueDef($rsnew, $this->product_type_id->CurrentValue, NULL, FALSE);

		// product_user_id
		$this->product_user_id->SetDbValueDef($rsnew, $this->product_user_id->CurrentValue, NULL, FALSE);

		// product_name
		$this->product_name->SetDbValueDef($rsnew, $this->product_name->CurrentValue, NULL, FALSE);

		// product_installation_date
		$this->product_installation_date->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->product_installation_date->CurrentValue, 7), NULL, FALSE);

		// product_installation_account_id
		$this->product_installation_account_id->SetDbValueDef($rsnew, $this->product_installation_account_id->CurrentValue, NULL, FALSE);

		// product_attach_file
		if ($this->product_attach_file->Visible && !$this->product_attach_file->Upload->KeepFile) {
			$this->product_attach_file->Upload->DbValue = ""; // No need to delete old file
			if ($this->product_attach_file->Upload->FileName == "") {
				$rsnew['product_attach_file'] = NULL;
			} else {
				$rsnew['product_attach_file'] = $this->product_attach_file->Upload->FileName;
			}
			$this->product_attach_type->SetDbValueDef($rsnew, trim($this->product_attach_file->Upload->ContentType), NULL, FALSE);
			$this->product_attach_size->SetDbValueDef($rsnew, $this->product_attach_file->Upload->FileSize, NULL, FALSE);
		}
		if ($this->product_attach_file->Visible && !$this->product_attach_file->Upload->KeepFile) {
			$OldFiles = explode(EW_MULTIPLE_UPLOAD_SEPARATOR, $this->product_attach_file->Upload->DbValue);
			if (!ew_Empty($this->product_attach_file->Upload->FileName)) {
				$NewFiles = explode(EW_MULTIPLE_UPLOAD_SEPARATOR, $this->product_attach_file->Upload->FileName);
				$FileCount = count($NewFiles);
				for ($i = 0; $i < $FileCount; $i++) {
					$fldvar = ($this->product_attach_file->Upload->Index < 0) ? $this->product_attach_file->FldVar : substr($this->product_attach_file->FldVar, 0, 1) . $this->product_attach_file->Upload->Index . substr($this->product_attach_file->FldVar, 1);
					if ($NewFiles[$i] <> "") {
						$file = $NewFiles[$i];
						if (file_exists(ew_UploadTempPath($fldvar, $this->product_attach_file->TblVar) . EW_PATH_DELIMITER . $file)) {
							if (!in_array($file, $OldFiles)) {
								$file1 = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->product_attach_file->UploadPath), $file); // Get new file name
								if ($file1 <> $file) { // Rename temp file
									while (file_exists(ew_UploadTempPath($fldvar, $this->product_attach_file->TblVar) . EW_PATH_DELIMITER . $file1)) // Make sure did not clash with existing upload file
										$file1 = ew_UniqueFilename(ew_UploadPathEx(TRUE, $this->product_attach_file->UploadPath), $file1, TRUE); // Use indexed name
									rename(ew_UploadTempPath($fldvar, $this->product_attach_file->TblVar) . EW_PATH_DELIMITER . $file, ew_UploadTempPath($fldvar, $this->product_attach_file->TblVar) . EW_PATH_DELIMITER . $file1);
									$NewFiles[$i] = $file1;
								}
							}
						}
					}
				}
				$this->product_attach_file->Upload->FileName = implode(EW_MULTIPLE_UPLOAD_SEPARATOR, $NewFiles);
				$rsnew['product_attach_file'] = $this->product_attach_file->Upload->FileName;
			} else {
				$NewFiles = array();
			}
		}

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->product_id->setDbValue($conn->Insert_ID());
				$rsnew['product_id'] = $this->product_id->DbValue;
				if ($this->product_attach_file->Visible && !$this->product_attach_file->Upload->KeepFile) {
					$OldFiles = explode(EW_MULTIPLE_UPLOAD_SEPARATOR, $this->product_attach_file->Upload->DbValue);
					if (!ew_Empty($this->product_attach_file->Upload->FileName)) {
						$NewFiles = explode(EW_MULTIPLE_UPLOAD_SEPARATOR, $this->product_attach_file->Upload->FileName);
						$NewFiles2 = explode(EW_MULTIPLE_UPLOAD_SEPARATOR, $rsnew['product_attach_file']);
						$FileCount = count($NewFiles);
						for ($i = 0; $i < $FileCount; $i++) {
							$fldvar = ($this->product_attach_file->Upload->Index < 0) ? $this->product_attach_file->FldVar : substr($this->product_attach_file->FldVar, 0, 1) . $this->product_attach_file->Upload->Index . substr($this->product_attach_file->FldVar, 1);
							if ($NewFiles[$i] <> "") {
								$file = ew_UploadTempPath($fldvar, $this->product_attach_file->TblVar) . EW_PATH_DELIMITER . $NewFiles[$i];
								if (file_exists($file)) {
									$this->product_attach_file->Upload->SaveToFile($this->product_attach_file->UploadPath, (@$NewFiles2[$i] <> "") ? $NewFiles2[$i] : $NewFiles[$i], TRUE, $i); // Just replace
								}
							}
						}
					} else {
						$NewFiles = array();
					}
					$FileCount = count($OldFiles);
					for ($i = 0; $i < $FileCount; $i++) {
						if ($OldFiles[$i] <> "" && !in_array($OldFiles[$i], $NewFiles))
							@unlink(ew_UploadPathEx(TRUE, $this->product_attach_file->OldUploadPath) . $OldFiles[$i]);
					}
				}
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}

		// product_attach_file
		ew_CleanUploadTempPath($this->product_attach_file, $this->product_attach_file->Upload->Index);
		return $AddRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_productlist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_product_add)) $telecare_product_add = new ctelecare_product_add();

// Page init
$telecare_product_add->Page_Init();

// Page main
$telecare_product_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_product_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_productadd = new ew_Form("ftelecare_productadd", "add");

// Validate form
ftelecare_productadd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_product_installation_date");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_product->product_installation_date->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_productadd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_productadd.ValidateRequired = true;
<?php } else { ?>
ftelecare_productadd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_productadd.Lists["x_product_type_id"] = {"LinkField":"x_product_type_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_product_type_name","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_productadd.Lists["x_product_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_productadd.Lists["x_product_installation_account_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_product_add->ShowPageHeader(); ?>
<?php
$telecare_product_add->ShowMessage();
?>
<form name="ftelecare_productadd" id="ftelecare_productadd" class="<?php echo $telecare_product_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_product_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_product_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_product">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($telecare_product->product_type_id->Visible) { // product_type_id ?>
	<div id="r_product_type_id" class="form-group">
		<label id="elh_telecare_product_product_type_id" for="x_product_type_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_product->product_type_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_product->product_type_id->CellAttributes() ?>>
<span id="el_telecare_product_product_type_id">
<select data-table="telecare_product" data-field="x_product_type_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_product->product_type_id->DisplayValueSeparator) ? json_encode($telecare_product->product_type_id->DisplayValueSeparator) : $telecare_product->product_type_id->DisplayValueSeparator) ?>" id="x_product_type_id" name="x_product_type_id"<?php echo $telecare_product->product_type_id->EditAttributes() ?>>
<?php
if (is_array($telecare_product->product_type_id->EditValue)) {
	$arwrk = $telecare_product->product_type_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_product->product_type_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_product->product_type_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_product->product_type_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_product->product_type_id->CurrentValue) ?>" selected><?php echo $telecare_product->product_type_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `product_type_id`, `product_type_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_product_type`";
		$sWhereWrk = "";
		break;
}
$telecare_product->product_type_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_product->product_type_id->LookupFilters += array("f0" => "`product_type_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_product->Lookup_Selecting($telecare_product->product_type_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `product_type_name` ASC";
if ($sSqlWrk <> "") $telecare_product->product_type_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_product_type_id" id="s_x_product_type_id" value="<?php echo $telecare_product->product_type_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_product->product_type_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_product->product_user_id->Visible) { // product_user_id ?>
	<div id="r_product_user_id" class="form-group">
		<label id="elh_telecare_product_product_user_id" for="x_product_user_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_product->product_user_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_product->product_user_id->CellAttributes() ?>>
<span id="el_telecare_product_product_user_id">
<select data-table="telecare_product" data-field="x_product_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_product->product_user_id->DisplayValueSeparator) ? json_encode($telecare_product->product_user_id->DisplayValueSeparator) : $telecare_product->product_user_id->DisplayValueSeparator) ?>" id="x_product_user_id" name="x_product_user_id"<?php echo $telecare_product->product_user_id->EditAttributes() ?>>
<?php
if (is_array($telecare_product->product_user_id->EditValue)) {
	$arwrk = $telecare_product->product_user_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_product->product_user_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_product->product_user_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_product->product_user_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_product->product_user_id->CurrentValue) ?>" selected><?php echo $telecare_product->product_user_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
		$sWhereWrk = "";
		break;
}
$telecare_product->product_user_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_product->product_user_id->LookupFilters += array("f0" => "`user_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_product->Lookup_Selecting($telecare_product->product_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `user_surname` ASC";
if ($sSqlWrk <> "") $telecare_product->product_user_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_product_user_id" id="s_x_product_user_id" value="<?php echo $telecare_product->product_user_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_product->product_user_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_product->product_name->Visible) { // product_name ?>
	<div id="r_product_name" class="form-group">
		<label id="elh_telecare_product_product_name" for="x_product_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_product->product_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_product->product_name->CellAttributes() ?>>
<span id="el_telecare_product_product_name">
<input type="text" data-table="telecare_product" data-field="x_product_name" name="x_product_name" id="x_product_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_product->product_name->getPlaceHolder()) ?>" value="<?php echo $telecare_product->product_name->EditValue ?>"<?php echo $telecare_product->product_name->EditAttributes() ?>>
</span>
<?php echo $telecare_product->product_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_product->product_installation_date->Visible) { // product_installation_date ?>
	<div id="r_product_installation_date" class="form-group">
		<label id="elh_telecare_product_product_installation_date" for="x_product_installation_date" class="col-sm-2 control-label ewLabel"><?php echo $telecare_product->product_installation_date->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_product->product_installation_date->CellAttributes() ?>>
<span id="el_telecare_product_product_installation_date">
<input type="text" data-table="telecare_product" data-field="x_product_installation_date" data-format="7" name="x_product_installation_date" id="x_product_installation_date" placeholder="<?php echo ew_HtmlEncode($telecare_product->product_installation_date->getPlaceHolder()) ?>" value="<?php echo $telecare_product->product_installation_date->EditValue ?>"<?php echo $telecare_product->product_installation_date->EditAttributes() ?>>
<?php if (!$telecare_product->product_installation_date->ReadOnly && !$telecare_product->product_installation_date->Disabled && !isset($telecare_product->product_installation_date->EditAttrs["readonly"]) && !isset($telecare_product->product_installation_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_productadd", "x_product_installation_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_product->product_installation_date->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_product->product_installation_account_id->Visible) { // product_installation_account_id ?>
	<div id="r_product_installation_account_id" class="form-group">
		<label id="elh_telecare_product_product_installation_account_id" for="x_product_installation_account_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_product->product_installation_account_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_product->product_installation_account_id->CellAttributes() ?>>
<span id="el_telecare_product_product_installation_account_id">
<select data-table="telecare_product" data-field="x_product_installation_account_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_product->product_installation_account_id->DisplayValueSeparator) ? json_encode($telecare_product->product_installation_account_id->DisplayValueSeparator) : $telecare_product->product_installation_account_id->DisplayValueSeparator) ?>" id="x_product_installation_account_id" name="x_product_installation_account_id"<?php echo $telecare_product->product_installation_account_id->EditAttributes() ?>>
<?php
if (is_array($telecare_product->product_installation_account_id->EditValue)) {
	$arwrk = $telecare_product->product_installation_account_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_product->product_installation_account_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_product->product_installation_account_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_product->product_installation_account_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_product->product_installation_account_id->CurrentValue) ?>" selected><?php echo $telecare_product->product_installation_account_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
$lookuptblfilter = "`admin_level` = 4";
ew_AddFilter($sWhereWrk, $lookuptblfilter);
if (!$GLOBALS["telecare_product"]->UserIDAllow("add")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_product->product_installation_account_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_product->product_installation_account_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_product->Lookup_Selecting($telecare_product->product_installation_account_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " ORDER BY `admin_surname` ASC";
if ($sSqlWrk <> "") $telecare_product->product_installation_account_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_product_installation_account_id" id="s_x_product_installation_account_id" value="<?php echo $telecare_product->product_installation_account_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_product->product_installation_account_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_product->product_attach_file->Visible) { // product_attach_file ?>
	<div id="r_product_attach_file" class="form-group">
		<label id="elh_telecare_product_product_attach_file" class="col-sm-2 control-label ewLabel"><?php echo $telecare_product->product_attach_file->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_product->product_attach_file->CellAttributes() ?>>
<span id="el_telecare_product_product_attach_file">
<div id="fd_x_product_attach_file">
<span title="<?php echo $telecare_product->product_attach_file->FldTitle() ? $telecare_product->product_attach_file->FldTitle() : $Language->Phrase("ChooseFiles") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($telecare_product->product_attach_file->ReadOnly || $telecare_product->product_attach_file->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="telecare_product" data-field="x_product_attach_file" name="x_product_attach_file" id="x_product_attach_file" multiple="multiple"<?php echo $telecare_product->product_attach_file->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_product_attach_file" id= "fn_x_product_attach_file" value="<?php echo $telecare_product->product_attach_file->Upload->FileName ?>">
<input type="hidden" name="fa_x_product_attach_file" id= "fa_x_product_attach_file" value="0">
<input type="hidden" name="fs_x_product_attach_file" id= "fs_x_product_attach_file" value="255">
<input type="hidden" name="fx_x_product_attach_file" id= "fx_x_product_attach_file" value="<?php echo $telecare_product->product_attach_file->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_product_attach_file" id= "fm_x_product_attach_file" value="<?php echo $telecare_product->product_attach_file->UploadMaxFileSize ?>">
<input type="hidden" name="fc_x_product_attach_file" id= "fc_x_product_attach_file" value="<?php echo $telecare_product->product_attach_file->UploadMaxFileCount ?>">
</div>
<table id="ft_x_product_attach_file" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $telecare_product->product_attach_file->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_product_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_productadd.Init();
</script>
<?php
$telecare_product_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_product_add->Page_Terminate();
?>
