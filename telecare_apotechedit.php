<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_apotechinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_prescriptiongridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_apotech_edit = NULL; // Initialize page object first

class ctelecare_apotech_edit extends ctelecare_apotech {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_apotech';

	// Page object name
	var $PageObjName = 'telecare_apotech_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_apotech)
		if (!isset($GLOBALS["telecare_apotech"]) || get_class($GLOBALS["telecare_apotech"]) == "ctelecare_apotech") {
			$GLOBALS["telecare_apotech"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_apotech"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_apotech', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_apotechlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->apotech_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Set up multi page object
		$this->SetupMultiPages();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'telecare_prescription'
			if (@$_POST["grid"] == "ftelecare_prescriptiongrid") {
				if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;
				$GLOBALS["telecare_prescription_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_apotech;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_apotech);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;
	var $MultiPages; // Multi pages object

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["apotech_id"] <> "") {
			$this->apotech_id->setQueryStringValue($_GET["apotech_id"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values

			// Set up detail parameters
			$this->SetUpDetailParms();
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->apotech_id->CurrentValue == "")
			$this->Page_Terminate("telecare_apotechlist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_apotechlist.php"); // No matching record, return to list
				}

				// Set up detail parameters
				$this->SetUpDetailParms();
				break;
			Case "U": // Update
				if ($this->getCurrentDetailTable() <> "") // Master/detail edit
					$sReturnUrl = $this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=" . $this->getCurrentDetailTable()); // Master/Detail view page
				else
					$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed

					// Set up detail parameters
					$this->SetUpDetailParms();
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
		$this->apotech_company_logo_file->Upload->Index = $objForm->Index;
		$this->apotech_company_logo_file->Upload->UploadFile();
		$this->apotech_company_logo_file->CurrentValue = $this->apotech_company_logo_file->Upload->FileName;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		$this->GetUploadFiles(); // Get upload files
		if (!$this->apotech_id->FldIsDetailKey)
			$this->apotech_id->setFormValue($objForm->GetValue("x_apotech_id"));
		if (!$this->apotech_admin_id->FldIsDetailKey) {
			$this->apotech_admin_id->setFormValue($objForm->GetValue("x_apotech_admin_id"));
		}
		if (!$this->apotech_surname->FldIsDetailKey) {
			$this->apotech_surname->setFormValue($objForm->GetValue("x_apotech_surname"));
		}
		if (!$this->apotech_name->FldIsDetailKey) {
			$this->apotech_name->setFormValue($objForm->GetValue("x_apotech_name"));
		}
		if (!$this->apotech_email->FldIsDetailKey) {
			$this->apotech_email->setFormValue($objForm->GetValue("x_apotech_email"));
		}
		if (!$this->apotech_mobile->FldIsDetailKey) {
			$this->apotech_mobile->setFormValue($objForm->GetValue("x_apotech_mobile"));
		}
		if (!$this->apotech_phone->FldIsDetailKey) {
			$this->apotech_phone->setFormValue($objForm->GetValue("x_apotech_phone"));
		}
		if (!$this->apotech_is_active->FldIsDetailKey) {
			$this->apotech_is_active->setFormValue($objForm->GetValue("x_apotech_is_active"));
		}
		if (!$this->apotech_last_update->FldIsDetailKey) {
			$this->apotech_last_update->setFormValue($objForm->GetValue("x_apotech_last_update"));
			$this->apotech_last_update->CurrentValue = ew_UnFormatDateTime($this->apotech_last_update->CurrentValue, 7);
		}
		if (!$this->apotech_company_name->FldIsDetailKey) {
			$this->apotech_company_name->setFormValue($objForm->GetValue("x_apotech_company_name"));
		}
		if (!$this->apotech_company_address->FldIsDetailKey) {
			$this->apotech_company_address->setFormValue($objForm->GetValue("x_apotech_company_address"));
		}
		if (!$this->apotech_company_phone->FldIsDetailKey) {
			$this->apotech_company_phone->setFormValue($objForm->GetValue("x_apotech_company_phone"));
		}
		if (!$this->apotech_company_email->FldIsDetailKey) {
			$this->apotech_company_email->setFormValue($objForm->GetValue("x_apotech_company_email"));
		}
		if (!$this->apotech_company_web->FldIsDetailKey) {
			$this->apotech_company_web->setFormValue($objForm->GetValue("x_apotech_company_web"));
		}
		if (!$this->apotech_latitude->FldIsDetailKey) {
			$this->apotech_latitude->setFormValue($objForm->GetValue("x_apotech_latitude"));
		}
		if (!$this->apotech_longitude->FldIsDetailKey) {
			$this->apotech_longitude->setFormValue($objForm->GetValue("x_apotech_longitude"));
		}
		if (!$this->apotech_company_region_id->FldIsDetailKey) {
			$this->apotech_company_region_id->setFormValue($objForm->GetValue("x_apotech_company_region_id"));
		}
		if (!$this->apotech_company_province_id->FldIsDetailKey) {
			$this->apotech_company_province_id->setFormValue($objForm->GetValue("x_apotech_company_province_id"));
		}
		if (!$this->apotech_company_city_id->FldIsDetailKey) {
			$this->apotech_company_city_id->setFormValue($objForm->GetValue("x_apotech_company_city_id"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->apotech_id->CurrentValue = $this->apotech_id->FormValue;
		$this->apotech_admin_id->CurrentValue = $this->apotech_admin_id->FormValue;
		$this->apotech_surname->CurrentValue = $this->apotech_surname->FormValue;
		$this->apotech_name->CurrentValue = $this->apotech_name->FormValue;
		$this->apotech_email->CurrentValue = $this->apotech_email->FormValue;
		$this->apotech_mobile->CurrentValue = $this->apotech_mobile->FormValue;
		$this->apotech_phone->CurrentValue = $this->apotech_phone->FormValue;
		$this->apotech_is_active->CurrentValue = $this->apotech_is_active->FormValue;
		$this->apotech_last_update->CurrentValue = $this->apotech_last_update->FormValue;
		$this->apotech_last_update->CurrentValue = ew_UnFormatDateTime($this->apotech_last_update->CurrentValue, 7);
		$this->apotech_company_name->CurrentValue = $this->apotech_company_name->FormValue;
		$this->apotech_company_address->CurrentValue = $this->apotech_company_address->FormValue;
		$this->apotech_company_phone->CurrentValue = $this->apotech_company_phone->FormValue;
		$this->apotech_company_email->CurrentValue = $this->apotech_company_email->FormValue;
		$this->apotech_company_web->CurrentValue = $this->apotech_company_web->FormValue;
		$this->apotech_latitude->CurrentValue = $this->apotech_latitude->FormValue;
		$this->apotech_longitude->CurrentValue = $this->apotech_longitude->FormValue;
		$this->apotech_company_region_id->CurrentValue = $this->apotech_company_region_id->FormValue;
		$this->apotech_company_province_id->CurrentValue = $this->apotech_company_province_id->FormValue;
		$this->apotech_company_city_id->CurrentValue = $this->apotech_company_city_id->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->apotech_id->setDbValue($rs->fields('apotech_id'));
		$this->apotech_admin_id->setDbValue($rs->fields('apotech_admin_id'));
		$this->apotech_surname->setDbValue($rs->fields('apotech_surname'));
		$this->apotech_name->setDbValue($rs->fields('apotech_name'));
		$this->apotech_email->setDbValue($rs->fields('apotech_email'));
		$this->apotech_mobile->setDbValue($rs->fields('apotech_mobile'));
		$this->apotech_phone->setDbValue($rs->fields('apotech_phone'));
		$this->apotech_is_active->setDbValue($rs->fields('apotech_is_active'));
		$this->apotech_last_update->setDbValue($rs->fields('apotech_last_update'));
		$this->apotech_company_name->setDbValue($rs->fields('apotech_company_name'));
		$this->apotech_company_logo_file->Upload->DbValue = $rs->fields('apotech_company_logo_file');
		$this->apotech_company_logo_file->CurrentValue = $this->apotech_company_logo_file->Upload->DbValue;
		$this->apotech_company_logo_width->setDbValue($rs->fields('apotech_company_logo_width'));
		$this->apotech_company_logo_height->setDbValue($rs->fields('apotech_company_logo_height'));
		$this->apotech_company_logo_size->setDbValue($rs->fields('apotech_company_logo_size'));
		$this->apotech_company_address->setDbValue($rs->fields('apotech_company_address'));
		$this->apotech_company_phone->setDbValue($rs->fields('apotech_company_phone'));
		$this->apotech_company_email->setDbValue($rs->fields('apotech_company_email'));
		$this->apotech_company_web->setDbValue($rs->fields('apotech_company_web'));
		$this->apotech_latitude->setDbValue($rs->fields('apotech_latitude'));
		$this->apotech_longitude->setDbValue($rs->fields('apotech_longitude'));
		$this->apotech_company_region_id->setDbValue($rs->fields('apotech_company_region_id'));
		$this->apotech_company_province_id->setDbValue($rs->fields('apotech_company_province_id'));
		$this->apotech_company_city_id->setDbValue($rs->fields('apotech_company_city_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->apotech_id->DbValue = $row['apotech_id'];
		$this->apotech_admin_id->DbValue = $row['apotech_admin_id'];
		$this->apotech_surname->DbValue = $row['apotech_surname'];
		$this->apotech_name->DbValue = $row['apotech_name'];
		$this->apotech_email->DbValue = $row['apotech_email'];
		$this->apotech_mobile->DbValue = $row['apotech_mobile'];
		$this->apotech_phone->DbValue = $row['apotech_phone'];
		$this->apotech_is_active->DbValue = $row['apotech_is_active'];
		$this->apotech_last_update->DbValue = $row['apotech_last_update'];
		$this->apotech_company_name->DbValue = $row['apotech_company_name'];
		$this->apotech_company_logo_file->Upload->DbValue = $row['apotech_company_logo_file'];
		$this->apotech_company_logo_width->DbValue = $row['apotech_company_logo_width'];
		$this->apotech_company_logo_height->DbValue = $row['apotech_company_logo_height'];
		$this->apotech_company_logo_size->DbValue = $row['apotech_company_logo_size'];
		$this->apotech_company_address->DbValue = $row['apotech_company_address'];
		$this->apotech_company_phone->DbValue = $row['apotech_company_phone'];
		$this->apotech_company_email->DbValue = $row['apotech_company_email'];
		$this->apotech_company_web->DbValue = $row['apotech_company_web'];
		$this->apotech_latitude->DbValue = $row['apotech_latitude'];
		$this->apotech_longitude->DbValue = $row['apotech_longitude'];
		$this->apotech_company_region_id->DbValue = $row['apotech_company_region_id'];
		$this->apotech_company_province_id->DbValue = $row['apotech_company_province_id'];
		$this->apotech_company_city_id->DbValue = $row['apotech_company_city_id'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// apotech_id
		// apotech_admin_id
		// apotech_surname
		// apotech_name
		// apotech_email
		// apotech_mobile
		// apotech_phone
		// apotech_is_active
		// apotech_last_update
		// apotech_company_name
		// apotech_company_logo_file
		// apotech_company_logo_width
		// apotech_company_logo_height
		// apotech_company_logo_size
		// apotech_company_address
		// apotech_company_phone
		// apotech_company_email
		// apotech_company_web
		// apotech_latitude
		// apotech_longitude
		// apotech_company_region_id
		// apotech_company_province_id
		// apotech_company_city_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// apotech_id
		$this->apotech_id->ViewValue = $this->apotech_id->CurrentValue;
		$this->apotech_id->ViewCustomAttributes = "";

		// apotech_admin_id
		if (strval($this->apotech_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->apotech_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->CurrentValue;
			}
		} else {
			$this->apotech_admin_id->ViewValue = NULL;
		}
		$this->apotech_admin_id->ViewCustomAttributes = "";

		// apotech_surname
		$this->apotech_surname->ViewValue = $this->apotech_surname->CurrentValue;
		$this->apotech_surname->ViewCustomAttributes = "";

		// apotech_name
		$this->apotech_name->ViewValue = $this->apotech_name->CurrentValue;
		$this->apotech_name->ViewCustomAttributes = "";

		// apotech_email
		$this->apotech_email->ViewValue = $this->apotech_email->CurrentValue;
		$this->apotech_email->ViewCustomAttributes = "";

		// apotech_mobile
		$this->apotech_mobile->ViewValue = $this->apotech_mobile->CurrentValue;
		$this->apotech_mobile->ViewCustomAttributes = "";

		// apotech_phone
		$this->apotech_phone->ViewValue = $this->apotech_phone->CurrentValue;
		$this->apotech_phone->ViewCustomAttributes = "";

		// apotech_is_active
		if (strval($this->apotech_is_active->CurrentValue) <> "") {
			$this->apotech_is_active->ViewValue = $this->apotech_is_active->OptionCaption($this->apotech_is_active->CurrentValue);
		} else {
			$this->apotech_is_active->ViewValue = NULL;
		}
		$this->apotech_is_active->ViewCustomAttributes = "";

		// apotech_last_update
		$this->apotech_last_update->ViewValue = $this->apotech_last_update->CurrentValue;
		$this->apotech_last_update->ViewValue = ew_FormatDateTime($this->apotech_last_update->ViewValue, 7);
		$this->apotech_last_update->ViewCustomAttributes = "";

		// apotech_company_name
		$this->apotech_company_name->ViewValue = $this->apotech_company_name->CurrentValue;
		$this->apotech_company_name->ViewCustomAttributes = "";

		// apotech_company_logo_file
		if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
			$this->apotech_company_logo_file->ImageWidth = 100;
			$this->apotech_company_logo_file->ImageHeight = 0;
			$this->apotech_company_logo_file->ImageAlt = $this->apotech_company_logo_file->FldAlt();
			$this->apotech_company_logo_file->ViewValue = $this->apotech_company_logo_file->Upload->DbValue;
		} else {
			$this->apotech_company_logo_file->ViewValue = "";
		}
		$this->apotech_company_logo_file->ViewCustomAttributes = "";

		// apotech_company_logo_width
		$this->apotech_company_logo_width->ViewValue = $this->apotech_company_logo_width->CurrentValue;
		$this->apotech_company_logo_width->ViewCustomAttributes = "";

		// apotech_company_logo_height
		$this->apotech_company_logo_height->ViewValue = $this->apotech_company_logo_height->CurrentValue;
		$this->apotech_company_logo_height->ViewCustomAttributes = "";

		// apotech_company_logo_size
		$this->apotech_company_logo_size->ViewValue = $this->apotech_company_logo_size->CurrentValue;
		$this->apotech_company_logo_size->ViewCustomAttributes = "";

		// apotech_company_address
		$this->apotech_company_address->ViewValue = $this->apotech_company_address->CurrentValue;
		$this->apotech_company_address->ViewCustomAttributes = "";

		// apotech_company_phone
		$this->apotech_company_phone->ViewValue = $this->apotech_company_phone->CurrentValue;
		$this->apotech_company_phone->ViewCustomAttributes = "";

		// apotech_company_email
		$this->apotech_company_email->ViewValue = $this->apotech_company_email->CurrentValue;
		$this->apotech_company_email->ViewCustomAttributes = "";

		// apotech_company_web
		$this->apotech_company_web->ViewValue = $this->apotech_company_web->CurrentValue;
		$this->apotech_company_web->ViewCustomAttributes = "";

		// apotech_latitude
		$this->apotech_latitude->ViewValue = $this->apotech_latitude->CurrentValue;
		$this->apotech_latitude->ViewCustomAttributes = "";

		// apotech_longitude
		$this->apotech_longitude->ViewValue = $this->apotech_longitude->CurrentValue;
		$this->apotech_longitude->ViewCustomAttributes = "";

		// apotech_company_region_id
		if (strval($this->apotech_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->apotech_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->CurrentValue;
			}
		} else {
			$this->apotech_company_region_id->ViewValue = NULL;
		}
		$this->apotech_company_region_id->ViewCustomAttributes = "";

		// apotech_company_province_id
		if (strval($this->apotech_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->apotech_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->CurrentValue;
			}
		} else {
			$this->apotech_company_province_id->ViewValue = NULL;
		}
		$this->apotech_company_province_id->ViewCustomAttributes = "";

		// apotech_company_city_id
		if (strval($this->apotech_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->apotech_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->CurrentValue;
			}
		} else {
			$this->apotech_company_city_id->ViewValue = NULL;
		}
		$this->apotech_company_city_id->ViewCustomAttributes = "";

			// apotech_id
			$this->apotech_id->LinkCustomAttributes = "";
			$this->apotech_id->HrefValue = "";
			$this->apotech_id->TooltipValue = "";

			// apotech_admin_id
			$this->apotech_admin_id->LinkCustomAttributes = "";
			$this->apotech_admin_id->HrefValue = "";
			$this->apotech_admin_id->TooltipValue = "";

			// apotech_surname
			$this->apotech_surname->LinkCustomAttributes = "";
			$this->apotech_surname->HrefValue = "";
			$this->apotech_surname->TooltipValue = "";

			// apotech_name
			$this->apotech_name->LinkCustomAttributes = "";
			$this->apotech_name->HrefValue = "";
			$this->apotech_name->TooltipValue = "";

			// apotech_email
			$this->apotech_email->LinkCustomAttributes = "";
			$this->apotech_email->HrefValue = "";
			$this->apotech_email->TooltipValue = "";

			// apotech_mobile
			$this->apotech_mobile->LinkCustomAttributes = "";
			$this->apotech_mobile->HrefValue = "";
			$this->apotech_mobile->TooltipValue = "";

			// apotech_phone
			$this->apotech_phone->LinkCustomAttributes = "";
			$this->apotech_phone->HrefValue = "";
			$this->apotech_phone->TooltipValue = "";

			// apotech_is_active
			$this->apotech_is_active->LinkCustomAttributes = "";
			$this->apotech_is_active->HrefValue = "";
			$this->apotech_is_active->TooltipValue = "";

			// apotech_last_update
			$this->apotech_last_update->LinkCustomAttributes = "";
			$this->apotech_last_update->HrefValue = "";
			$this->apotech_last_update->TooltipValue = "";

			// apotech_company_name
			$this->apotech_company_name->LinkCustomAttributes = "";
			$this->apotech_company_name->HrefValue = "";
			$this->apotech_company_name->TooltipValue = "";

			// apotech_company_logo_file
			$this->apotech_company_logo_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
				$this->apotech_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->apotech_company_logo_file, $this->apotech_company_logo_file->Upload->DbValue); // Add prefix/suffix
				$this->apotech_company_logo_file->LinkAttrs["target"] = ""; // Add target
				if ($this->Export <> "") $this->apotech_company_logo_file->HrefValue = ew_ConvertFullUrl($this->apotech_company_logo_file->HrefValue);
			} else {
				$this->apotech_company_logo_file->HrefValue = "";
			}
			$this->apotech_company_logo_file->HrefValue2 = $this->apotech_company_logo_file->UploadPath . $this->apotech_company_logo_file->Upload->DbValue;
			$this->apotech_company_logo_file->TooltipValue = "";
			if ($this->apotech_company_logo_file->UseColorbox) {
				$this->apotech_company_logo_file->LinkAttrs["title"] = $Language->Phrase("ViewImageGallery");
				$this->apotech_company_logo_file->LinkAttrs["data-rel"] = "telecare_apotech_x_apotech_company_logo_file";

				//$this->apotech_company_logo_file->LinkAttrs["class"] = "ewLightbox ewTooltip img-thumbnail";
				//$this->apotech_company_logo_file->LinkAttrs["data-placement"] = "bottom";
				//$this->apotech_company_logo_file->LinkAttrs["data-container"] = "body";

				$this->apotech_company_logo_file->LinkAttrs["class"] = "ewLightbox img-thumbnail";
			}

			// apotech_company_address
			$this->apotech_company_address->LinkCustomAttributes = "";
			$this->apotech_company_address->HrefValue = "";
			$this->apotech_company_address->TooltipValue = "";

			// apotech_company_phone
			$this->apotech_company_phone->LinkCustomAttributes = "";
			$this->apotech_company_phone->HrefValue = "";
			$this->apotech_company_phone->TooltipValue = "";

			// apotech_company_email
			$this->apotech_company_email->LinkCustomAttributes = "";
			$this->apotech_company_email->HrefValue = "";
			$this->apotech_company_email->TooltipValue = "";

			// apotech_company_web
			$this->apotech_company_web->LinkCustomAttributes = "";
			$this->apotech_company_web->HrefValue = "";
			$this->apotech_company_web->TooltipValue = "";

			// apotech_latitude
			$this->apotech_latitude->LinkCustomAttributes = "";
			$this->apotech_latitude->HrefValue = "";
			$this->apotech_latitude->TooltipValue = "";

			// apotech_longitude
			$this->apotech_longitude->LinkCustomAttributes = "";
			$this->apotech_longitude->HrefValue = "";
			$this->apotech_longitude->TooltipValue = "";

			// apotech_company_region_id
			$this->apotech_company_region_id->LinkCustomAttributes = "";
			$this->apotech_company_region_id->HrefValue = "";
			$this->apotech_company_region_id->TooltipValue = "";

			// apotech_company_province_id
			$this->apotech_company_province_id->LinkCustomAttributes = "";
			$this->apotech_company_province_id->HrefValue = "";
			$this->apotech_company_province_id->TooltipValue = "";

			// apotech_company_city_id
			$this->apotech_company_city_id->LinkCustomAttributes = "";
			$this->apotech_company_city_id->HrefValue = "";
			$this->apotech_company_city_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// apotech_id
			$this->apotech_id->EditAttrs["class"] = "form-control";
			$this->apotech_id->EditCustomAttributes = "";
			$this->apotech_id->EditValue = $this->apotech_id->CurrentValue;
			$this->apotech_id->ViewCustomAttributes = "";

			// apotech_admin_id
			$this->apotech_admin_id->EditAttrs["class"] = "form-control";
			$this->apotech_admin_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_admin_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->apotech_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_admin`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			if (!$GLOBALS["telecare_apotech"]->UserIDAllow("edit")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
			$this->Lookup_Selecting($this->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_admin_id->EditValue = $arwrk;

			// apotech_surname
			$this->apotech_surname->EditAttrs["class"] = "form-control";
			$this->apotech_surname->EditCustomAttributes = "";
			$this->apotech_surname->EditValue = ew_HtmlEncode($this->apotech_surname->CurrentValue);
			$this->apotech_surname->PlaceHolder = ew_RemoveHtml($this->apotech_surname->FldCaption());

			// apotech_name
			$this->apotech_name->EditAttrs["class"] = "form-control";
			$this->apotech_name->EditCustomAttributes = "";
			$this->apotech_name->EditValue = ew_HtmlEncode($this->apotech_name->CurrentValue);
			$this->apotech_name->PlaceHolder = ew_RemoveHtml($this->apotech_name->FldCaption());

			// apotech_email
			$this->apotech_email->EditAttrs["class"] = "form-control";
			$this->apotech_email->EditCustomAttributes = "";
			$this->apotech_email->EditValue = ew_HtmlEncode($this->apotech_email->CurrentValue);
			$this->apotech_email->PlaceHolder = ew_RemoveHtml($this->apotech_email->FldCaption());

			// apotech_mobile
			$this->apotech_mobile->EditAttrs["class"] = "form-control";
			$this->apotech_mobile->EditCustomAttributes = "";
			$this->apotech_mobile->EditValue = ew_HtmlEncode($this->apotech_mobile->CurrentValue);
			$this->apotech_mobile->PlaceHolder = ew_RemoveHtml($this->apotech_mobile->FldCaption());

			// apotech_phone
			$this->apotech_phone->EditAttrs["class"] = "form-control";
			$this->apotech_phone->EditCustomAttributes = "";
			$this->apotech_phone->EditValue = ew_HtmlEncode($this->apotech_phone->CurrentValue);
			$this->apotech_phone->PlaceHolder = ew_RemoveHtml($this->apotech_phone->FldCaption());

			// apotech_is_active
			$this->apotech_is_active->EditCustomAttributes = "";
			$this->apotech_is_active->EditValue = $this->apotech_is_active->Options(FALSE);

			// apotech_last_update
			// apotech_company_name

			$this->apotech_company_name->EditAttrs["class"] = "form-control";
			$this->apotech_company_name->EditCustomAttributes = "";
			$this->apotech_company_name->EditValue = ew_HtmlEncode($this->apotech_company_name->CurrentValue);
			$this->apotech_company_name->PlaceHolder = ew_RemoveHtml($this->apotech_company_name->FldCaption());

			// apotech_company_logo_file
			$this->apotech_company_logo_file->EditAttrs["class"] = "form-control";
			$this->apotech_company_logo_file->EditCustomAttributes = "";
			if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
				$this->apotech_company_logo_file->ImageWidth = 100;
				$this->apotech_company_logo_file->ImageHeight = 0;
				$this->apotech_company_logo_file->ImageAlt = $this->apotech_company_logo_file->FldAlt();
				$this->apotech_company_logo_file->EditValue = $this->apotech_company_logo_file->Upload->DbValue;
			} else {
				$this->apotech_company_logo_file->EditValue = "";
			}
			if (!ew_Empty($this->apotech_company_logo_file->CurrentValue))
				$this->apotech_company_logo_file->Upload->FileName = $this->apotech_company_logo_file->CurrentValue;
			if ($this->CurrentAction == "I" && !$this->EventCancelled) ew_RenderUploadField($this->apotech_company_logo_file);

			// apotech_company_address
			$this->apotech_company_address->EditAttrs["class"] = "form-control";
			$this->apotech_company_address->EditCustomAttributes = "";
			$this->apotech_company_address->EditValue = ew_HtmlEncode($this->apotech_company_address->CurrentValue);
			$this->apotech_company_address->PlaceHolder = ew_RemoveHtml($this->apotech_company_address->FldCaption());

			// apotech_company_phone
			$this->apotech_company_phone->EditAttrs["class"] = "form-control";
			$this->apotech_company_phone->EditCustomAttributes = "";
			$this->apotech_company_phone->EditValue = ew_HtmlEncode($this->apotech_company_phone->CurrentValue);
			$this->apotech_company_phone->PlaceHolder = ew_RemoveHtml($this->apotech_company_phone->FldCaption());

			// apotech_company_email
			$this->apotech_company_email->EditAttrs["class"] = "form-control";
			$this->apotech_company_email->EditCustomAttributes = "";
			$this->apotech_company_email->EditValue = ew_HtmlEncode($this->apotech_company_email->CurrentValue);
			$this->apotech_company_email->PlaceHolder = ew_RemoveHtml($this->apotech_company_email->FldCaption());

			// apotech_company_web
			$this->apotech_company_web->EditAttrs["class"] = "form-control";
			$this->apotech_company_web->EditCustomAttributes = "";
			$this->apotech_company_web->EditValue = ew_HtmlEncode($this->apotech_company_web->CurrentValue);
			$this->apotech_company_web->PlaceHolder = ew_RemoveHtml($this->apotech_company_web->FldCaption());

			// apotech_latitude
			$this->apotech_latitude->EditAttrs["class"] = "form-control";
			$this->apotech_latitude->EditCustomAttributes = "";
			$this->apotech_latitude->EditValue = ew_HtmlEncode($this->apotech_latitude->CurrentValue);
			$this->apotech_latitude->PlaceHolder = ew_RemoveHtml($this->apotech_latitude->FldCaption());

			// apotech_longitude
			$this->apotech_longitude->EditAttrs["class"] = "form-control";
			$this->apotech_longitude->EditCustomAttributes = "";
			$this->apotech_longitude->EditValue = ew_HtmlEncode($this->apotech_longitude->CurrentValue);
			$this->apotech_longitude->PlaceHolder = ew_RemoveHtml($this->apotech_longitude->FldCaption());

			// apotech_company_region_id
			$this->apotech_company_region_id->EditAttrs["class"] = "form-control";
			$this->apotech_company_region_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_company_region_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->apotech_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_region`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_company_region_id->EditValue = $arwrk;

			// apotech_company_province_id
			$this->apotech_company_province_id->EditAttrs["class"] = "form-control";
			$this->apotech_company_province_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_company_province_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->apotech_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `province_region_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_province`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_company_province_id->EditValue = $arwrk;

			// apotech_company_city_id
			$this->apotech_company_city_id->EditAttrs["class"] = "form-control";
			$this->apotech_company_city_id->EditCustomAttributes = "";
			if (trim(strval($this->apotech_company_city_id->CurrentValue)) == "") {
				$sFilterWrk = "0=1";
			} else {
				$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->apotech_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			}
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, `city_province_id` AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `telecare_city`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->apotech_company_city_id->EditValue = $arwrk;

			// Edit refer script
			// apotech_id

			$this->apotech_id->LinkCustomAttributes = "";
			$this->apotech_id->HrefValue = "";

			// apotech_admin_id
			$this->apotech_admin_id->LinkCustomAttributes = "";
			$this->apotech_admin_id->HrefValue = "";

			// apotech_surname
			$this->apotech_surname->LinkCustomAttributes = "";
			$this->apotech_surname->HrefValue = "";

			// apotech_name
			$this->apotech_name->LinkCustomAttributes = "";
			$this->apotech_name->HrefValue = "";

			// apotech_email
			$this->apotech_email->LinkCustomAttributes = "";
			$this->apotech_email->HrefValue = "";

			// apotech_mobile
			$this->apotech_mobile->LinkCustomAttributes = "";
			$this->apotech_mobile->HrefValue = "";

			// apotech_phone
			$this->apotech_phone->LinkCustomAttributes = "";
			$this->apotech_phone->HrefValue = "";

			// apotech_is_active
			$this->apotech_is_active->LinkCustomAttributes = "";
			$this->apotech_is_active->HrefValue = "";

			// apotech_last_update
			$this->apotech_last_update->LinkCustomAttributes = "";
			$this->apotech_last_update->HrefValue = "";

			// apotech_company_name
			$this->apotech_company_name->LinkCustomAttributes = "";
			$this->apotech_company_name->HrefValue = "";

			// apotech_company_logo_file
			$this->apotech_company_logo_file->LinkCustomAttributes = "";
			if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
				$this->apotech_company_logo_file->HrefValue = ew_GetFileUploadUrl($this->apotech_company_logo_file, $this->apotech_company_logo_file->Upload->DbValue); // Add prefix/suffix
				$this->apotech_company_logo_file->LinkAttrs["target"] = ""; // Add target
				if ($this->Export <> "") $this->apotech_company_logo_file->HrefValue = ew_ConvertFullUrl($this->apotech_company_logo_file->HrefValue);
			} else {
				$this->apotech_company_logo_file->HrefValue = "";
			}
			$this->apotech_company_logo_file->HrefValue2 = $this->apotech_company_logo_file->UploadPath . $this->apotech_company_logo_file->Upload->DbValue;

			// apotech_company_address
			$this->apotech_company_address->LinkCustomAttributes = "";
			$this->apotech_company_address->HrefValue = "";

			// apotech_company_phone
			$this->apotech_company_phone->LinkCustomAttributes = "";
			$this->apotech_company_phone->HrefValue = "";

			// apotech_company_email
			$this->apotech_company_email->LinkCustomAttributes = "";
			$this->apotech_company_email->HrefValue = "";

			// apotech_company_web
			$this->apotech_company_web->LinkCustomAttributes = "";
			$this->apotech_company_web->HrefValue = "";

			// apotech_latitude
			$this->apotech_latitude->LinkCustomAttributes = "";
			$this->apotech_latitude->HrefValue = "";

			// apotech_longitude
			$this->apotech_longitude->LinkCustomAttributes = "";
			$this->apotech_longitude->HrefValue = "";

			// apotech_company_region_id
			$this->apotech_company_region_id->LinkCustomAttributes = "";
			$this->apotech_company_region_id->HrefValue = "";

			// apotech_company_province_id
			$this->apotech_company_province_id->LinkCustomAttributes = "";
			$this->apotech_company_province_id->HrefValue = "";

			// apotech_company_city_id
			$this->apotech_company_city_id->LinkCustomAttributes = "";
			$this->apotech_company_city_id->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->apotech_admin_id->FldIsDetailKey && !is_null($this->apotech_admin_id->FormValue) && $this->apotech_admin_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_admin_id->FldCaption(), $this->apotech_admin_id->ReqErrMsg));
		}
		if (!$this->apotech_surname->FldIsDetailKey && !is_null($this->apotech_surname->FormValue) && $this->apotech_surname->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_surname->FldCaption(), $this->apotech_surname->ReqErrMsg));
		}
		if (!$this->apotech_name->FldIsDetailKey && !is_null($this->apotech_name->FormValue) && $this->apotech_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_name->FldCaption(), $this->apotech_name->ReqErrMsg));
		}
		if (!$this->apotech_email->FldIsDetailKey && !is_null($this->apotech_email->FormValue) && $this->apotech_email->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_email->FldCaption(), $this->apotech_email->ReqErrMsg));
		}
		if (!ew_CheckEmail($this->apotech_email->FormValue)) {
			ew_AddMessage($gsFormError, $this->apotech_email->FldErrMsg());
		}
		if (!$this->apotech_phone->FldIsDetailKey && !is_null($this->apotech_phone->FormValue) && $this->apotech_phone->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_phone->FldCaption(), $this->apotech_phone->ReqErrMsg));
		}
		if (!$this->apotech_company_region_id->FldIsDetailKey && !is_null($this->apotech_company_region_id->FormValue) && $this->apotech_company_region_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_company_region_id->FldCaption(), $this->apotech_company_region_id->ReqErrMsg));
		}
		if (!$this->apotech_company_province_id->FldIsDetailKey && !is_null($this->apotech_company_province_id->FormValue) && $this->apotech_company_province_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_company_province_id->FldCaption(), $this->apotech_company_province_id->ReqErrMsg));
		}
		if (!$this->apotech_company_city_id->FldIsDetailKey && !is_null($this->apotech_company_city_id->FormValue) && $this->apotech_company_city_id->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->apotech_company_city_id->FldCaption(), $this->apotech_company_city_id->ReqErrMsg));
		}

		// Validate detail grid
		$DetailTblVar = explode(",", $this->getCurrentDetailTable());
		if (in_array("telecare_prescription", $DetailTblVar) && $GLOBALS["telecare_prescription"]->DetailEdit) {
			if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid(); // get detail page object
			$GLOBALS["telecare_prescription_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Begin transaction
			if ($this->getCurrentDetailTable() <> "")
				$conn->BeginTrans();

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// apotech_admin_id
			$this->apotech_admin_id->SetDbValueDef($rsnew, $this->apotech_admin_id->CurrentValue, 0, $this->apotech_admin_id->ReadOnly);

			// apotech_surname
			$this->apotech_surname->SetDbValueDef($rsnew, $this->apotech_surname->CurrentValue, NULL, $this->apotech_surname->ReadOnly);

			// apotech_name
			$this->apotech_name->SetDbValueDef($rsnew, $this->apotech_name->CurrentValue, NULL, $this->apotech_name->ReadOnly);

			// apotech_email
			$this->apotech_email->SetDbValueDef($rsnew, $this->apotech_email->CurrentValue, NULL, $this->apotech_email->ReadOnly);

			// apotech_mobile
			$this->apotech_mobile->SetDbValueDef($rsnew, $this->apotech_mobile->CurrentValue, NULL, $this->apotech_mobile->ReadOnly);

			// apotech_phone
			$this->apotech_phone->SetDbValueDef($rsnew, $this->apotech_phone->CurrentValue, NULL, $this->apotech_phone->ReadOnly);

			// apotech_is_active
			$this->apotech_is_active->SetDbValueDef($rsnew, $this->apotech_is_active->CurrentValue, NULL, $this->apotech_is_active->ReadOnly);

			// apotech_last_update
			$this->apotech_last_update->SetDbValueDef($rsnew, ew_CurrentDateTime(), NULL);
			$rsnew['apotech_last_update'] = &$this->apotech_last_update->DbValue;

			// apotech_company_name
			$this->apotech_company_name->SetDbValueDef($rsnew, $this->apotech_company_name->CurrentValue, NULL, $this->apotech_company_name->ReadOnly);

			// apotech_company_logo_file
			if ($this->apotech_company_logo_file->Visible && !$this->apotech_company_logo_file->ReadOnly && !$this->apotech_company_logo_file->Upload->KeepFile) {
				$this->apotech_company_logo_file->Upload->DbValue = $rsold['apotech_company_logo_file']; // Get original value
				if ($this->apotech_company_logo_file->Upload->FileName == "") {
					$rsnew['apotech_company_logo_file'] = NULL;
				} else {
					$rsnew['apotech_company_logo_file'] = $this->apotech_company_logo_file->Upload->FileName;
				}
			}

			// apotech_company_address
			$this->apotech_company_address->SetDbValueDef($rsnew, $this->apotech_company_address->CurrentValue, NULL, $this->apotech_company_address->ReadOnly);

			// apotech_company_phone
			$this->apotech_company_phone->SetDbValueDef($rsnew, $this->apotech_company_phone->CurrentValue, NULL, $this->apotech_company_phone->ReadOnly);

			// apotech_company_email
			$this->apotech_company_email->SetDbValueDef($rsnew, $this->apotech_company_email->CurrentValue, NULL, $this->apotech_company_email->ReadOnly);

			// apotech_company_web
			$this->apotech_company_web->SetDbValueDef($rsnew, $this->apotech_company_web->CurrentValue, NULL, $this->apotech_company_web->ReadOnly);

			// apotech_latitude
			$this->apotech_latitude->SetDbValueDef($rsnew, $this->apotech_latitude->CurrentValue, NULL, $this->apotech_latitude->ReadOnly);

			// apotech_longitude
			$this->apotech_longitude->SetDbValueDef($rsnew, $this->apotech_longitude->CurrentValue, NULL, $this->apotech_longitude->ReadOnly);

			// apotech_company_region_id
			$this->apotech_company_region_id->SetDbValueDef($rsnew, $this->apotech_company_region_id->CurrentValue, NULL, $this->apotech_company_region_id->ReadOnly);

			// apotech_company_province_id
			$this->apotech_company_province_id->SetDbValueDef($rsnew, $this->apotech_company_province_id->CurrentValue, NULL, $this->apotech_company_province_id->ReadOnly);

			// apotech_company_city_id
			$this->apotech_company_city_id->SetDbValueDef($rsnew, $this->apotech_company_city_id->CurrentValue, NULL, $this->apotech_company_city_id->ReadOnly);
			if ($this->apotech_company_logo_file->Visible && !$this->apotech_company_logo_file->Upload->KeepFile) {
				if (!ew_Empty($this->apotech_company_logo_file->Upload->Value)) {
					if ($this->apotech_company_logo_file->Upload->FileName == $this->apotech_company_logo_file->Upload->DbValue) { // Overwrite if same file name
						$this->apotech_company_logo_file->Upload->DbValue = ""; // No need to delete any more
					} else {
						$rsnew['apotech_company_logo_file'] = ew_UploadFileNameEx(ew_UploadPathEx(TRUE, $this->apotech_company_logo_file->UploadPath), $rsnew['apotech_company_logo_file']); // Get new file name
					}
				}
			}

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
					if ($this->apotech_company_logo_file->Visible && !$this->apotech_company_logo_file->Upload->KeepFile) {
						if (!ew_Empty($this->apotech_company_logo_file->Upload->Value)) {
							$this->apotech_company_logo_file->Upload->SaveToFile($this->apotech_company_logo_file->UploadPath, $rsnew['apotech_company_logo_file'], TRUE);
						}
						if ($this->apotech_company_logo_file->Upload->DbValue <> "")
							@unlink(ew_UploadPathEx(TRUE, $this->apotech_company_logo_file->OldUploadPath) . $this->apotech_company_logo_file->Upload->DbValue);
					}
				}

				// Update detail records
				if ($EditRow) {
					$DetailTblVar = explode(",", $this->getCurrentDetailTable());
					if (in_array("telecare_prescription", $DetailTblVar) && $GLOBALS["telecare_prescription"]->DetailEdit) {
						if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid(); // Get detail page object
						$EditRow = $GLOBALS["telecare_prescription_grid"]->GridUpdate();
					}
				}

				// Commit/Rollback transaction
				if ($this->getCurrentDetailTable() <> "") {
					if ($EditRow) {
						$conn->CommitTrans(); // Commit transaction
					} else {
						$conn->RollbackTrans(); // Rollback transaction
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();

		// apotech_company_logo_file
		ew_CleanUploadTempPath($this->apotech_company_logo_file, $this->apotech_company_logo_file->Upload->Index);
		return $EditRow;
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			$DetailTblVar = explode(",", $sDetailTblVar);
			if (in_array("telecare_prescription", $DetailTblVar)) {
				if (!isset($GLOBALS["telecare_prescription_grid"]))
					$GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;
				if ($GLOBALS["telecare_prescription_grid"]->DetailEdit) {
					$GLOBALS["telecare_prescription_grid"]->CurrentMode = "edit";
					$GLOBALS["telecare_prescription_grid"]->CurrentAction = "gridedit";

					// Save current master table to detail table
					$GLOBALS["telecare_prescription_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["telecare_prescription_grid"]->setStartRecordNumber(1);
					$GLOBALS["telecare_prescription_grid"]->prescription_drugstore_id->FldIsDetailKey = TRUE;
					$GLOBALS["telecare_prescription_grid"]->prescription_drugstore_id->CurrentValue = $this->apotech_id->CurrentValue;
					$GLOBALS["telecare_prescription_grid"]->prescription_drugstore_id->setSessionValue($GLOBALS["telecare_prescription_grid"]->prescription_drugstore_id->CurrentValue);
				}
			}
		}
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_apotechlist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Set up multi pages
	function SetupMultiPages() {
		$pages = new cSubPages();
		$pages->Style = "tabs";
		$pages->Add(0);
		$pages->Add(1);
		$pages->Add(2);
		$pages->Add(3);
		$this->MultiPages = $pages;
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_apotech_edit)) $telecare_apotech_edit = new ctelecare_apotech_edit();

// Page init
$telecare_apotech_edit->Page_Init();

// Page main
$telecare_apotech_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_apotech_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = ftelecare_apotechedit = new ew_Form("ftelecare_apotechedit", "edit");

// Validate form
ftelecare_apotechedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_apotech_admin_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_admin_id->FldCaption(), $telecare_apotech->apotech_admin_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_apotech_surname");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_surname->FldCaption(), $telecare_apotech->apotech_surname->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_apotech_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_name->FldCaption(), $telecare_apotech->apotech_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_apotech_email");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_email->FldCaption(), $telecare_apotech->apotech_email->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_apotech_email");
			if (elm && !ew_CheckEmail(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_apotech->apotech_email->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_apotech_phone");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_phone->FldCaption(), $telecare_apotech->apotech_phone->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_apotech_company_region_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_company_region_id->FldCaption(), $telecare_apotech->apotech_company_region_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_apotech_company_province_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_company_province_id->FldCaption(), $telecare_apotech->apotech_company_province_id->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_apotech_company_city_id");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_apotech->apotech_company_city_id->FldCaption(), $telecare_apotech->apotech_company_city_id->ReqErrMsg)) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_apotechedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_apotechedit.ValidateRequired = true;
<?php } else { ?>
ftelecare_apotechedit.ValidateRequired = false; 
<?php } ?>

// Multi-Page
ftelecare_apotechedit.MultiPage = new ew_MultiPage("ftelecare_apotechedit");

// Dynamic selection lists
ftelecare_apotechedit.Lists["x_apotech_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_apotechedit.Lists["x_apotech_is_active"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_apotechedit.Lists["x_apotech_is_active"].Options = <?php echo json_encode($telecare_apotech->apotech_is_active->Options()) ?>;
ftelecare_apotechedit.Lists["x_apotech_company_region_id"] = {"LinkField":"x_region_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_region_name","","",""],"ParentFields":[],"ChildFields":["x_apotech_company_province_id"],"FilterFields":[],"Options":[],"Template":""};
ftelecare_apotechedit.Lists["x_apotech_company_province_id"] = {"LinkField":"x_province_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_province_name","","",""],"ParentFields":["x_apotech_company_region_id"],"ChildFields":["x_apotech_company_city_id"],"FilterFields":["x_province_region_id"],"Options":[],"Template":""};
ftelecare_apotechedit.Lists["x_apotech_company_city_id"] = {"LinkField":"x_city_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_city_name","","",""],"ParentFields":["x_apotech_company_province_id"],"ChildFields":[],"FilterFields":["x_city_province_id"],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_apotech_edit->ShowPageHeader(); ?>
<?php
$telecare_apotech_edit->ShowMessage();
?>
<form name="ftelecare_apotechedit" id="ftelecare_apotechedit" class="<?php echo $telecare_apotech_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_apotech_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_apotech_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_apotech">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<div class="ewMultiPage">
<div class="tabbable" id="telecare_apotech_edit">
	<ul class="nav<?php echo $telecare_apotech_edit->MultiPages->NavStyle() ?>">
		<li<?php echo $telecare_apotech_edit->MultiPages->TabStyle("1") ?>><a href="#tab_telecare_apotech1" data-toggle="tab"><?php echo $telecare_apotech->PageCaption(1) ?></a></li>
		<li<?php echo $telecare_apotech_edit->MultiPages->TabStyle("2") ?>><a href="#tab_telecare_apotech2" data-toggle="tab"><?php echo $telecare_apotech->PageCaption(2) ?></a></li>
		<li<?php echo $telecare_apotech_edit->MultiPages->TabStyle("3") ?>><a href="#tab_telecare_apotech3" data-toggle="tab"><?php echo $telecare_apotech->PageCaption(3) ?></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane<?php echo $telecare_apotech_edit->MultiPages->PageStyle("1") ?>" id="tab_telecare_apotech1">
<div>
<?php if ($telecare_apotech->apotech_id->Visible) { // apotech_id ?>
	<div id="r_apotech_id" class="form-group">
		<label id="elh_telecare_apotech_apotech_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_id->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_id">
<span<?php echo $telecare_apotech->apotech_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_apotech->apotech_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_apotech" data-field="x_apotech_id" data-page="1" name="x_apotech_id" id="x_apotech_id" value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_id->CurrentValue) ?>">
<?php echo $telecare_apotech->apotech_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_admin_id->Visible) { // apotech_admin_id ?>
	<div id="r_apotech_admin_id" class="form-group">
		<label id="elh_telecare_apotech_apotech_admin_id" for="x_apotech_admin_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_admin_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_admin_id->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_admin_id">
<select data-table="telecare_apotech" data-field="x_apotech_admin_id" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_admin_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_admin_id->DisplayValueSeparator) : $telecare_apotech->apotech_admin_id->DisplayValueSeparator) ?>" id="x_apotech_admin_id" name="x_apotech_admin_id"<?php echo $telecare_apotech->apotech_admin_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_admin_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_admin_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_admin_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_admin_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_admin_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_admin_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_admin_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
		$sWhereWrk = "";
		break;
}
if (!$GLOBALS["telecare_apotech"]->UserIDAllow("edit")) $sWhereWrk = $GLOBALS["telecare_admin"]->AddUserIDFilter($sWhereWrk);
$telecare_apotech->apotech_admin_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_admin_id->LookupFilters += array("f0" => "`admin_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_admin_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_admin_id" id="s_x_apotech_admin_id" value="<?php echo $telecare_apotech->apotech_admin_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_apotech->apotech_admin_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_surname->Visible) { // apotech_surname ?>
	<div id="r_apotech_surname" class="form-group">
		<label id="elh_telecare_apotech_apotech_surname" for="x_apotech_surname" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_surname->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_surname->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_surname">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_surname" data-page="1" name="x_apotech_surname" id="x_apotech_surname" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_surname->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_surname->EditValue ?>"<?php echo $telecare_apotech->apotech_surname->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_surname->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_name->Visible) { // apotech_name ?>
	<div id="r_apotech_name" class="form-group">
		<label id="elh_telecare_apotech_apotech_name" for="x_apotech_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_name->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_name">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_name" data-page="1" name="x_apotech_name" id="x_apotech_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_name->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_name->EditValue ?>"<?php echo $telecare_apotech->apotech_name->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_email->Visible) { // apotech_email ?>
	<div id="r_apotech_email" class="form-group">
		<label id="elh_telecare_apotech_apotech_email" for="x_apotech_email" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_email->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_email->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_email">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_email" data-page="1" name="x_apotech_email" id="x_apotech_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_email->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_email->EditValue ?>"<?php echo $telecare_apotech->apotech_email->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_mobile->Visible) { // apotech_mobile ?>
	<div id="r_apotech_mobile" class="form-group">
		<label id="elh_telecare_apotech_apotech_mobile" for="x_apotech_mobile" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_mobile->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_mobile->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_mobile">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_mobile" data-page="1" name="x_apotech_mobile" id="x_apotech_mobile" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_mobile->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_mobile->EditValue ?>"<?php echo $telecare_apotech->apotech_mobile->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_mobile->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_phone->Visible) { // apotech_phone ?>
	<div id="r_apotech_phone" class="form-group">
		<label id="elh_telecare_apotech_apotech_phone" for="x_apotech_phone" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_phone->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_phone->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_phone">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_phone" data-page="1" name="x_apotech_phone" id="x_apotech_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_phone->EditValue ?>"<?php echo $telecare_apotech->apotech_phone->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_phone->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_is_active->Visible) { // apotech_is_active ?>
	<div id="r_apotech_is_active" class="form-group">
		<label id="elh_telecare_apotech_apotech_is_active" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_is_active->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_is_active->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_is_active">
<div id="tp_x_apotech_is_active" class="ewTemplate"><input type="radio" data-table="telecare_apotech" data-field="x_apotech_is_active" data-page="1" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_is_active->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_is_active->DisplayValueSeparator) : $telecare_apotech->apotech_is_active->DisplayValueSeparator) ?>" name="x_apotech_is_active" id="x_apotech_is_active" value="{value}"<?php echo $telecare_apotech->apotech_is_active->EditAttributes() ?>></div>
<div id="dsl_x_apotech_is_active" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_apotech->apotech_is_active->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_apotech->apotech_is_active->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_apotech" data-field="x_apotech_is_active" data-page="1" name="x_apotech_is_active" id="x_apotech_is_active_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_apotech->apotech_is_active->EditAttributes() ?>><?php echo $telecare_apotech->apotech_is_active->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_is_active->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_apotech" data-field="x_apotech_is_active" data-page="1" name="x_apotech_is_active" id="x_apotech_is_active_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_is_active->CurrentValue) ?>" checked<?php echo $telecare_apotech->apotech_is_active->EditAttributes() ?>><?php echo $telecare_apotech->apotech_is_active->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_apotech->apotech_is_active->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_apotech_edit->MultiPages->PageStyle("2") ?>" id="tab_telecare_apotech2">
<div>
<?php if ($telecare_apotech->apotech_company_name->Visible) { // apotech_company_name ?>
	<div id="r_apotech_company_name" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_name" for="x_apotech_company_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_name->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_name->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_name">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_name" data-page="2" name="x_apotech_company_name" id="x_apotech_company_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_name->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_name->EditValue ?>"<?php echo $telecare_apotech->apotech_company_name->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_company_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_logo_file->Visible) { // apotech_company_logo_file ?>
	<div id="r_apotech_company_logo_file" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_logo_file" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_logo_file->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_logo_file->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_logo_file">
<div id="fd_x_apotech_company_logo_file">
<span title="<?php echo $telecare_apotech->apotech_company_logo_file->FldTitle() ? $telecare_apotech->apotech_company_logo_file->FldTitle() : $Language->Phrase("ChooseFile") ?>" class="btn btn-default btn-sm fileinput-button ewTooltip<?php if ($telecare_apotech->apotech_company_logo_file->ReadOnly || $telecare_apotech->apotech_company_logo_file->Disabled) echo " hide"; ?>">
	<span><?php echo $Language->Phrase("ChooseFileBtn") ?></span>
	<input type="file" title=" " data-table="telecare_apotech" data-field="x_apotech_company_logo_file" data-page="2" name="x_apotech_company_logo_file" id="x_apotech_company_logo_file"<?php echo $telecare_apotech->apotech_company_logo_file->EditAttributes() ?>>
</span>
<input type="hidden" name="fn_x_apotech_company_logo_file" id= "fn_x_apotech_company_logo_file" value="<?php echo $telecare_apotech->apotech_company_logo_file->Upload->FileName ?>">
<?php if (@$_POST["fa_x_apotech_company_logo_file"] == "0") { ?>
<input type="hidden" name="fa_x_apotech_company_logo_file" id= "fa_x_apotech_company_logo_file" value="0">
<?php } else { ?>
<input type="hidden" name="fa_x_apotech_company_logo_file" id= "fa_x_apotech_company_logo_file" value="1">
<?php } ?>
<input type="hidden" name="fs_x_apotech_company_logo_file" id= "fs_x_apotech_company_logo_file" value="255">
<input type="hidden" name="fx_x_apotech_company_logo_file" id= "fx_x_apotech_company_logo_file" value="<?php echo $telecare_apotech->apotech_company_logo_file->UploadAllowedFileExt ?>">
<input type="hidden" name="fm_x_apotech_company_logo_file" id= "fm_x_apotech_company_logo_file" value="<?php echo $telecare_apotech->apotech_company_logo_file->UploadMaxFileSize ?>">
</div>
<table id="ft_x_apotech_company_logo_file" class="table table-condensed pull-left ewUploadTable"><tbody class="files"></tbody></table>
</span>
<?php echo $telecare_apotech->apotech_company_logo_file->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_address->Visible) { // apotech_company_address ?>
	<div id="r_apotech_company_address" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_address" for="x_apotech_company_address" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_address->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_address->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_address">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_address" data-page="2" name="x_apotech_company_address" id="x_apotech_company_address" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_address->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_address->EditValue ?>"<?php echo $telecare_apotech->apotech_company_address->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_company_address->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_phone->Visible) { // apotech_company_phone ?>
	<div id="r_apotech_company_phone" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_phone" for="x_apotech_company_phone" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_phone->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_phone->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_phone">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_phone" data-page="2" name="x_apotech_company_phone" id="x_apotech_company_phone" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_phone->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_phone->EditValue ?>"<?php echo $telecare_apotech->apotech_company_phone->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_company_phone->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_email->Visible) { // apotech_company_email ?>
	<div id="r_apotech_company_email" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_email" for="x_apotech_company_email" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_email->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_email->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_email">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_email" data-page="2" name="x_apotech_company_email" id="x_apotech_company_email" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_email->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_email->EditValue ?>"<?php echo $telecare_apotech->apotech_company_email->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_company_email->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_web->Visible) { // apotech_company_web ?>
	<div id="r_apotech_company_web" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_web" for="x_apotech_company_web" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_web->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_web->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_web">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_company_web" data-page="2" name="x_apotech_company_web" id="x_apotech_company_web" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_web->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_company_web->EditValue ?>"<?php echo $telecare_apotech->apotech_company_web->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_company_web->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
		<div class="tab-pane<?php echo $telecare_apotech_edit->MultiPages->PageStyle("3") ?>" id="tab_telecare_apotech3">
<div>
<?php if ($telecare_apotech->apotech_latitude->Visible) { // apotech_latitude ?>
	<div id="r_apotech_latitude" class="form-group">
		<label id="elh_telecare_apotech_apotech_latitude" for="x_apotech_latitude" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_latitude->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_latitude->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_latitude">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_latitude" data-page="3" name="x_apotech_latitude" id="x_apotech_latitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_latitude->EditValue ?>"<?php echo $telecare_apotech->apotech_latitude->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_latitude->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_longitude->Visible) { // apotech_longitude ?>
	<div id="r_apotech_longitude" class="form-group">
		<label id="elh_telecare_apotech_apotech_longitude" for="x_apotech_longitude" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_longitude->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_longitude->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_longitude">
<input type="text" data-table="telecare_apotech" data-field="x_apotech_longitude" data-page="3" name="x_apotech_longitude" id="x_apotech_longitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_apotech->apotech_longitude->getPlaceHolder()) ?>" value="<?php echo $telecare_apotech->apotech_longitude->EditValue ?>"<?php echo $telecare_apotech->apotech_longitude->EditAttributes() ?>>
</span>
<?php echo $telecare_apotech->apotech_longitude->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_region_id->Visible) { // apotech_company_region_id ?>
	<div id="r_apotech_company_region_id" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_region_id" for="x_apotech_company_region_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_region_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_region_id->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_region_id">
<?php $telecare_apotech->apotech_company_region_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_apotech->apotech_company_region_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_apotech" data-field="x_apotech_company_region_id" data-page="3" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_company_region_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_company_region_id->DisplayValueSeparator) : $telecare_apotech->apotech_company_region_id->DisplayValueSeparator) ?>" id="x_apotech_company_region_id" name="x_apotech_company_region_id"<?php echo $telecare_apotech->apotech_company_region_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_company_region_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_company_region_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_company_region_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_company_region_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_company_region_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_region_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_company_region_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
	default:
		$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
		$sWhereWrk = "";
		break;
}
$telecare_apotech->apotech_company_region_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_company_region_id->LookupFilters += array("f0" => "`region_id` = {filter_value}", "t0" => "3", "fn0" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_company_region_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_company_region_id" id="s_x_apotech_company_region_id" value="<?php echo $telecare_apotech->apotech_company_region_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_apotech->apotech_company_region_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_province_id->Visible) { // apotech_company_province_id ?>
	<div id="r_apotech_company_province_id" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_province_id" for="x_apotech_company_province_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_province_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_province_id->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_province_id">
<?php $telecare_apotech->apotech_company_province_id->EditAttrs["onchange"] = "ew_UpdateOpt.call(this); " . @$telecare_apotech->apotech_company_province_id->EditAttrs["onchange"]; ?>
<select data-table="telecare_apotech" data-field="x_apotech_company_province_id" data-page="3" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_company_province_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_company_province_id->DisplayValueSeparator) : $telecare_apotech->apotech_company_province_id->DisplayValueSeparator) ?>" id="x_apotech_company_province_id" name="x_apotech_company_province_id"<?php echo $telecare_apotech->apotech_company_province_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_company_province_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_company_province_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_company_province_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_company_province_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_company_province_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_province_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_company_province_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_apotech->apotech_company_province_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_company_province_id->LookupFilters += array("f0" => "`province_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_apotech->apotech_company_province_id->LookupFilters += array("f1" => "`province_region_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_company_province_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_company_province_id" id="s_x_apotech_company_province_id" value="<?php echo $telecare_apotech->apotech_company_province_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_apotech->apotech_company_province_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_city_id->Visible) { // apotech_company_city_id ?>
	<div id="r_apotech_company_city_id" class="form-group">
		<label id="elh_telecare_apotech_apotech_company_city_id" for="x_apotech_company_city_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_apotech->apotech_company_city_id->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_apotech->apotech_company_city_id->CellAttributes() ?>>
<span id="el_telecare_apotech_apotech_company_city_id">
<select data-table="telecare_apotech" data-field="x_apotech_company_city_id" data-page="3" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_apotech->apotech_company_city_id->DisplayValueSeparator) ? json_encode($telecare_apotech->apotech_company_city_id->DisplayValueSeparator) : $telecare_apotech->apotech_company_city_id->DisplayValueSeparator) ?>" id="x_apotech_company_city_id" name="x_apotech_company_city_id"<?php echo $telecare_apotech->apotech_company_city_id->EditAttributes() ?>>
<?php
if (is_array($telecare_apotech->apotech_company_city_id->EditValue)) {
	$arwrk = $telecare_apotech->apotech_company_city_id->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = ew_SameStr($telecare_apotech->apotech_company_city_id->CurrentValue, $arwrk[$rowcntwrk][0]) ? " selected" : "";
		if ($selwrk <> "") $emptywrk = FALSE;		
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $telecare_apotech->apotech_company_city_id->DisplayValue($arwrk[$rowcntwrk]) ?>
</option>
<?php
	}
	if ($emptywrk && strval($telecare_apotech->apotech_company_city_id->CurrentValue) <> "") {
?>
<option value="<?php echo ew_HtmlEncode($telecare_apotech->apotech_company_city_id->CurrentValue) ?>" selected><?php echo $telecare_apotech->apotech_company_city_id->CurrentValue ?></option>
<?php
    }
}
?>
</select>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
	default:
		$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
		$sWhereWrk = "{filter}";
		break;
}
$telecare_apotech->apotech_company_city_id->LookupFilters = array("s" => $sSqlWrk, "d" => "");
$telecare_apotech->apotech_company_city_id->LookupFilters += array("f0" => "`city_id` = {filter_value}", "t0" => "21", "fn0" => "");
$telecare_apotech->apotech_company_city_id->LookupFilters += array("f1" => "`city_province_id` IN ({filter_value})", "t1" => "3", "fn1" => "");
$sSqlWrk = "";
$telecare_apotech->Lookup_Selecting($telecare_apotech->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
if ($sSqlWrk <> "") $telecare_apotech->apotech_company_city_id->LookupFilters["s"] .= $sSqlWrk;
?>
<input type="hidden" name="s_x_apotech_company_city_id" id="s_x_apotech_company_city_id" value="<?php echo $telecare_apotech->apotech_company_city_id->LookupFilterQuery() ?>">
</span>
<?php echo $telecare_apotech->apotech_company_city_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
		</div>
	</div>
</div>
</div>
<?php
	if (in_array("telecare_prescription", explode(",", $telecare_apotech->getCurrentDetailTable())) && $telecare_prescription->DetailEdit) {
?>
<?php if ($telecare_apotech->getCurrentDetailTable() <> "") { ?>
<h4 class="ewDetailCaption"><?php echo $Language->TablePhrase("telecare_prescription", "TblCaption") ?></h4>
<?php } ?>
<?php include_once "telecare_prescriptiongrid.php" ?>
<?php } ?>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_apotech_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_apotechedit.Init();
</script>
<?php
$telecare_apotech_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_apotech_edit->Page_Terminate();
?>
