<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_callinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_userinfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_call_add = NULL; // Initialize page object first

class ctelecare_call_add extends ctelecare_call {

	// Page ID
	var $PageID = 'add';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_call';

	// Page object name
	var $PageObjName = 'telecare_call_add';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_call)
		if (!isset($GLOBALS["telecare_call"]) || get_class($GLOBALS["telecare_call"]) == "ctelecare_call") {
			$GLOBALS["telecare_call"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_call"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Table object (telecare_user)
		if (!isset($GLOBALS['telecare_user'])) $GLOBALS['telecare_user'] = new ctelecare_user();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'add', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_call', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanAdd()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_calllist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_call;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_call);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewAddForm";
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $Priv = 0;
	var $OldRecordset;
	var $CopyRecord;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Set up master/detail parameters
		$this->SetUpMasterParms();

		// Process form if post back
		if (@$_POST["a_add"] <> "") {
			$this->CurrentAction = $_POST["a_add"]; // Get form action
			$this->CopyRecord = $this->LoadOldRecord(); // Load old recordset
			$this->LoadFormValues(); // Load form values
		} else { // Not post back

			// Load key values from QueryString
			$this->CopyRecord = TRUE;
			if (@$_GET["call_id"] != "") {
				$this->call_id->setQueryStringValue($_GET["call_id"]);
				$this->setKey("call_id", $this->call_id->CurrentValue); // Set up key
			} else {
				$this->setKey("call_id", ""); // Clear key
				$this->CopyRecord = FALSE;
			}
			if ($this->CopyRecord) {
				$this->CurrentAction = "C"; // Copy record
			} else {
				$this->CurrentAction = "I"; // Display blank record
				$this->LoadDefaultValues(); // Load default values
			}
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Validate form if post back
		if (@$_POST["a_add"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = "I"; // Form error, reset action
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues(); // Restore form values
				$this->setFailureMessage($gsFormError);
			}
		}

		// Perform action based on action code
		switch ($this->CurrentAction) {
			case "I": // Blank record, no action required
				break;
			case "C": // Copy an existing record
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_calllist.php"); // No matching record, return to list
				}
				break;
			case "A": // Add new record
				$this->SendEmail = TRUE; // Send email on add success
				if ($this->AddRow($this->OldRecordset)) { // Add successful
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up success message
					$sReturnUrl = $this->getReturnUrl();
					if (ew_GetPageName($sReturnUrl) == "telecare_callview.php")
						$sReturnUrl = $this->GetViewUrl(); // View paging, return to view page with keyurl directly
					$this->Page_Terminate($sReturnUrl); // Clean up and return
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Add failed, restore form values
				}
		}

		// Render row based on row type
		$this->RowType = EW_ROWTYPE_ADD; // Render add type

		// Render row
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load default values
	function LoadDefaultValues() {
		$this->call_user_id->CurrentValue = NULL;
		$this->call_user_id->OldValue = $this->call_user_id->CurrentValue;
		$this->call_admin_id->CurrentValue = NULL;
		$this->call_admin_id->OldValue = $this->call_admin_id->CurrentValue;
		$this->call_requested_date->CurrentValue = NULL;
		$this->call_requested_date->OldValue = $this->call_requested_date->CurrentValue;
		$this->call_start_on->CurrentValue = NULL;
		$this->call_start_on->OldValue = $this->call_start_on->CurrentValue;
		$this->call_close_on->CurrentValue = NULL;
		$this->call_close_on->OldValue = $this->call_close_on->CurrentValue;
		$this->call_user_note->CurrentValue = NULL;
		$this->call_user_note->OldValue = $this->call_user_note->CurrentValue;
		$this->call_admin_note->CurrentValue = NULL;
		$this->call_admin_note->OldValue = $this->call_admin_note->CurrentValue;
		$this->call_closed->CurrentValue = 0;
		$this->call_last_update->CurrentValue = NULL;
		$this->call_last_update->OldValue = $this->call_last_update->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->call_user_id->FldIsDetailKey) {
			$this->call_user_id->setFormValue($objForm->GetValue("x_call_user_id"));
		}
		if (!$this->call_admin_id->FldIsDetailKey) {
			$this->call_admin_id->setFormValue($objForm->GetValue("x_call_admin_id"));
		}
		if (!$this->call_requested_date->FldIsDetailKey) {
			$this->call_requested_date->setFormValue($objForm->GetValue("x_call_requested_date"));
			$this->call_requested_date->CurrentValue = ew_UnFormatDateTime($this->call_requested_date->CurrentValue, 7);
		}
		if (!$this->call_start_on->FldIsDetailKey) {
			$this->call_start_on->setFormValue($objForm->GetValue("x_call_start_on"));
			$this->call_start_on->CurrentValue = ew_UnFormatDateTime($this->call_start_on->CurrentValue, 7);
		}
		if (!$this->call_close_on->FldIsDetailKey) {
			$this->call_close_on->setFormValue($objForm->GetValue("x_call_close_on"));
			$this->call_close_on->CurrentValue = ew_UnFormatDateTime($this->call_close_on->CurrentValue, 7);
		}
		if (!$this->call_user_note->FldIsDetailKey) {
			$this->call_user_note->setFormValue($objForm->GetValue("x_call_user_note"));
		}
		if (!$this->call_admin_note->FldIsDetailKey) {
			$this->call_admin_note->setFormValue($objForm->GetValue("x_call_admin_note"));
		}
		if (!$this->call_closed->FldIsDetailKey) {
			$this->call_closed->setFormValue($objForm->GetValue("x_call_closed"));
		}
		if (!$this->call_last_update->FldIsDetailKey) {
			$this->call_last_update->setFormValue($objForm->GetValue("x_call_last_update"));
			$this->call_last_update->CurrentValue = ew_UnFormatDateTime($this->call_last_update->CurrentValue, 7);
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadOldRecord();
		$this->call_user_id->CurrentValue = $this->call_user_id->FormValue;
		$this->call_admin_id->CurrentValue = $this->call_admin_id->FormValue;
		$this->call_requested_date->CurrentValue = $this->call_requested_date->FormValue;
		$this->call_requested_date->CurrentValue = ew_UnFormatDateTime($this->call_requested_date->CurrentValue, 7);
		$this->call_start_on->CurrentValue = $this->call_start_on->FormValue;
		$this->call_start_on->CurrentValue = ew_UnFormatDateTime($this->call_start_on->CurrentValue, 7);
		$this->call_close_on->CurrentValue = $this->call_close_on->FormValue;
		$this->call_close_on->CurrentValue = ew_UnFormatDateTime($this->call_close_on->CurrentValue, 7);
		$this->call_user_note->CurrentValue = $this->call_user_note->FormValue;
		$this->call_admin_note->CurrentValue = $this->call_admin_note->FormValue;
		$this->call_closed->CurrentValue = $this->call_closed->FormValue;
		$this->call_last_update->CurrentValue = $this->call_last_update->FormValue;
		$this->call_last_update->CurrentValue = ew_UnFormatDateTime($this->call_last_update->CurrentValue, 7);
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->call_id->setDbValue($rs->fields('call_id'));
		$this->call_user_id->setDbValue($rs->fields('call_user_id'));
		$this->call_admin_id->setDbValue($rs->fields('call_admin_id'));
		$this->call_requested_date->setDbValue($rs->fields('call_requested_date'));
		$this->call_start_on->setDbValue($rs->fields('call_start_on'));
		$this->call_close_on->setDbValue($rs->fields('call_close_on'));
		$this->call_user_note->setDbValue($rs->fields('call_user_note'));
		$this->call_admin_note->setDbValue($rs->fields('call_admin_note'));
		$this->call_closed->setDbValue($rs->fields('call_closed'));
		$this->call_last_update->setDbValue($rs->fields('call_last_update'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->call_id->DbValue = $row['call_id'];
		$this->call_user_id->DbValue = $row['call_user_id'];
		$this->call_admin_id->DbValue = $row['call_admin_id'];
		$this->call_requested_date->DbValue = $row['call_requested_date'];
		$this->call_start_on->DbValue = $row['call_start_on'];
		$this->call_close_on->DbValue = $row['call_close_on'];
		$this->call_user_note->DbValue = $row['call_user_note'];
		$this->call_admin_note->DbValue = $row['call_admin_note'];
		$this->call_closed->DbValue = $row['call_closed'];
		$this->call_last_update->DbValue = $row['call_last_update'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("call_id")) <> "")
			$this->call_id->CurrentValue = $this->getKey("call_id"); // call_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// call_id
		// call_user_id
		// call_admin_id
		// call_requested_date
		// call_start_on
		// call_close_on
		// call_user_note
		// call_admin_note
		// call_closed
		// call_last_update

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// call_id
		$this->call_id->ViewValue = $this->call_id->CurrentValue;
		$this->call_id->ViewCustomAttributes = "";

		// call_user_id
		$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
		if (strval($this->call_user_id->CurrentValue) <> "") {
			$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->call_user_id->ViewValue = $this->call_user_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
			}
		} else {
			$this->call_user_id->ViewValue = NULL;
		}
		$this->call_user_id->ViewCustomAttributes = "";

		// call_admin_id
		if (strval($this->call_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->call_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, `admin_company_name` AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->call_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$arwrk[3] = $rswrk->fields('Disp3Fld');
				$this->call_admin_id->ViewValue = $this->call_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->call_admin_id->ViewValue = $this->call_admin_id->CurrentValue;
			}
		} else {
			$this->call_admin_id->ViewValue = NULL;
		}
		$this->call_admin_id->ViewCustomAttributes = "";

		// call_requested_date
		$this->call_requested_date->ViewValue = $this->call_requested_date->CurrentValue;
		$this->call_requested_date->ViewValue = ew_FormatDateTime($this->call_requested_date->ViewValue, 7);
		$this->call_requested_date->ViewCustomAttributes = "";

		// call_start_on
		$this->call_start_on->ViewValue = $this->call_start_on->CurrentValue;
		$this->call_start_on->ViewValue = ew_FormatDateTime($this->call_start_on->ViewValue, 7);
		$this->call_start_on->ViewCustomAttributes = "";

		// call_close_on
		$this->call_close_on->ViewValue = $this->call_close_on->CurrentValue;
		$this->call_close_on->ViewValue = ew_FormatDateTime($this->call_close_on->ViewValue, 7);
		$this->call_close_on->ViewCustomAttributes = "";

		// call_user_note
		$this->call_user_note->ViewValue = $this->call_user_note->CurrentValue;
		$this->call_user_note->ViewCustomAttributes = "";

		// call_admin_note
		$this->call_admin_note->ViewValue = $this->call_admin_note->CurrentValue;
		$this->call_admin_note->ViewCustomAttributes = "";

		// call_closed
		if (strval($this->call_closed->CurrentValue) <> "") {
			$this->call_closed->ViewValue = $this->call_closed->OptionCaption($this->call_closed->CurrentValue);
		} else {
			$this->call_closed->ViewValue = NULL;
		}
		$this->call_closed->ViewCustomAttributes = "";

		// call_last_update
		$this->call_last_update->ViewValue = $this->call_last_update->CurrentValue;
		$this->call_last_update->ViewValue = ew_FormatDateTime($this->call_last_update->ViewValue, 7);
		$this->call_last_update->ViewCustomAttributes = "";

			// call_user_id
			$this->call_user_id->LinkCustomAttributes = "";
			$this->call_user_id->HrefValue = "";
			$this->call_user_id->TooltipValue = "";

			// call_admin_id
			$this->call_admin_id->LinkCustomAttributes = "";
			$this->call_admin_id->HrefValue = "";
			$this->call_admin_id->TooltipValue = "";

			// call_requested_date
			$this->call_requested_date->LinkCustomAttributes = "";
			$this->call_requested_date->HrefValue = "";
			$this->call_requested_date->TooltipValue = "";

			// call_start_on
			$this->call_start_on->LinkCustomAttributes = "";
			$this->call_start_on->HrefValue = "";
			$this->call_start_on->TooltipValue = "";

			// call_close_on
			$this->call_close_on->LinkCustomAttributes = "";
			$this->call_close_on->HrefValue = "";
			$this->call_close_on->TooltipValue = "";

			// call_user_note
			$this->call_user_note->LinkCustomAttributes = "";
			$this->call_user_note->HrefValue = "";
			$this->call_user_note->TooltipValue = "";

			// call_admin_note
			$this->call_admin_note->LinkCustomAttributes = "";
			$this->call_admin_note->HrefValue = "";
			$this->call_admin_note->TooltipValue = "";

			// call_closed
			$this->call_closed->LinkCustomAttributes = "";
			$this->call_closed->HrefValue = "";
			$this->call_closed->TooltipValue = "";

			// call_last_update
			$this->call_last_update->LinkCustomAttributes = "";
			$this->call_last_update->HrefValue = "";
			$this->call_last_update->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// call_user_id
			$this->call_user_id->EditAttrs["class"] = "form-control";
			$this->call_user_id->EditCustomAttributes = "";
			if ($this->call_user_id->getSessionValue() <> "") {
				$this->call_user_id->CurrentValue = $this->call_user_id->getSessionValue();
			$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
			if (strval($this->call_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = $rswrk->fields('DispFld');
					$arwrk[2] = $rswrk->fields('Disp2Fld');
					$this->call_user_id->ViewValue = $this->call_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->call_user_id->ViewValue = $this->call_user_id->CurrentValue;
				}
			} else {
				$this->call_user_id->ViewValue = NULL;
			}
			$this->call_user_id->ViewCustomAttributes = "";
			} else {
			$this->call_user_id->EditValue = ew_HtmlEncode($this->call_user_id->CurrentValue);
			if (strval($this->call_user_id->CurrentValue) <> "") {
				$sFilterWrk = "`user_id`" . ew_SearchString("=", $this->call_user_id->CurrentValue, EW_DATATYPE_NUMBER, "");
			switch (@$gsLanguage) {
				case "it":
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
				default:
					$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_user`";
					$sWhereWrk = "";
					break;
			}
			ew_AddFilter($sWhereWrk, $sFilterWrk);
			$this->Lookup_Selecting($this->call_user_id, $sWhereWrk); // Call Lookup selecting
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = Conn()->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$arwrk = array();
					$arwrk[1] = ew_HtmlEncode($rswrk->fields('DispFld'));
					$arwrk[2] = ew_HtmlEncode($rswrk->fields('Disp2Fld'));
					$this->call_user_id->EditValue = $this->call_user_id->DisplayValue($arwrk);
					$rswrk->Close();
				} else {
					$this->call_user_id->EditValue = ew_HtmlEncode($this->call_user_id->CurrentValue);
				}
			} else {
				$this->call_user_id->EditValue = NULL;
			}
			$this->call_user_id->PlaceHolder = ew_RemoveHtml($this->call_user_id->FldCaption());
			}

			// call_admin_id
			// call_requested_date

			$this->call_requested_date->EditAttrs["class"] = "form-control";
			$this->call_requested_date->EditCustomAttributes = "";
			$this->call_requested_date->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_requested_date->CurrentValue, 7));
			$this->call_requested_date->PlaceHolder = ew_RemoveHtml($this->call_requested_date->FldCaption());

			// call_start_on
			$this->call_start_on->EditAttrs["class"] = "form-control";
			$this->call_start_on->EditCustomAttributes = "";
			$this->call_start_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_start_on->CurrentValue, 7));
			$this->call_start_on->PlaceHolder = ew_RemoveHtml($this->call_start_on->FldCaption());

			// call_close_on
			$this->call_close_on->EditAttrs["class"] = "form-control";
			$this->call_close_on->EditCustomAttributes = "";
			$this->call_close_on->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->call_close_on->CurrentValue, 7));
			$this->call_close_on->PlaceHolder = ew_RemoveHtml($this->call_close_on->FldCaption());

			// call_user_note
			$this->call_user_note->EditAttrs["class"] = "form-control";
			$this->call_user_note->EditCustomAttributes = "";
			$this->call_user_note->EditValue = ew_HtmlEncode($this->call_user_note->CurrentValue);
			$this->call_user_note->PlaceHolder = ew_RemoveHtml($this->call_user_note->FldCaption());

			// call_admin_note
			$this->call_admin_note->EditAttrs["class"] = "form-control";
			$this->call_admin_note->EditCustomAttributes = "";
			$this->call_admin_note->EditValue = ew_HtmlEncode($this->call_admin_note->CurrentValue);
			$this->call_admin_note->PlaceHolder = ew_RemoveHtml($this->call_admin_note->FldCaption());

			// call_closed
			$this->call_closed->EditCustomAttributes = "";
			$this->call_closed->EditValue = $this->call_closed->Options(FALSE);

			// call_last_update
			// Edit refer script
			// call_user_id

			$this->call_user_id->LinkCustomAttributes = "";
			$this->call_user_id->HrefValue = "";

			// call_admin_id
			$this->call_admin_id->LinkCustomAttributes = "";
			$this->call_admin_id->HrefValue = "";

			// call_requested_date
			$this->call_requested_date->LinkCustomAttributes = "";
			$this->call_requested_date->HrefValue = "";

			// call_start_on
			$this->call_start_on->LinkCustomAttributes = "";
			$this->call_start_on->HrefValue = "";

			// call_close_on
			$this->call_close_on->LinkCustomAttributes = "";
			$this->call_close_on->HrefValue = "";

			// call_user_note
			$this->call_user_note->LinkCustomAttributes = "";
			$this->call_user_note->HrefValue = "";

			// call_admin_note
			$this->call_admin_note->LinkCustomAttributes = "";
			$this->call_admin_note->HrefValue = "";

			// call_closed
			$this->call_closed->LinkCustomAttributes = "";
			$this->call_closed->HrefValue = "";

			// call_last_update
			$this->call_last_update->LinkCustomAttributes = "";
			$this->call_last_update->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!ew_CheckInteger($this->call_user_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_user_id->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->call_requested_date->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_requested_date->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->call_start_on->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_start_on->FldErrMsg());
		}
		if (!ew_CheckEuroDate($this->call_close_on->FormValue)) {
			ew_AddMessage($gsFormError, $this->call_close_on->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $Language, $Security;
		$conn = &$this->Connection();

		// Load db values from rsold
		if ($rsold) {
			$this->LoadDbValues($rsold);
		}
		$rsnew = array();

		// call_user_id
		$this->call_user_id->SetDbValueDef($rsnew, $this->call_user_id->CurrentValue, NULL, FALSE);

		// call_admin_id
		$this->call_admin_id->SetDbValueDef($rsnew, CurrentUserID(), NULL);
		$rsnew['call_admin_id'] = &$this->call_admin_id->DbValue;

		// call_requested_date
		$this->call_requested_date->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_requested_date->CurrentValue, 7), NULL, FALSE);

		// call_start_on
		$this->call_start_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_start_on->CurrentValue, 7), NULL, FALSE);

		// call_close_on
		$this->call_close_on->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->call_close_on->CurrentValue, 7), NULL, FALSE);

		// call_user_note
		$this->call_user_note->SetDbValueDef($rsnew, $this->call_user_note->CurrentValue, NULL, FALSE);

		// call_admin_note
		$this->call_admin_note->SetDbValueDef($rsnew, $this->call_admin_note->CurrentValue, NULL, FALSE);

		// call_closed
		$this->call_closed->SetDbValueDef($rsnew, $this->call_closed->CurrentValue, NULL, strval($this->call_closed->CurrentValue) == "");

		// call_last_update
		$this->call_last_update->SetDbValueDef($rsnew, ew_CurrentDateTime(), NULL);
		$rsnew['call_last_update'] = &$this->call_last_update->DbValue;

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {

				// Get insert id if necessary
				$this->call_id->setDbValue($conn->Insert_ID());
				$rsnew['call_id'] = $this->call_id->DbValue;
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
		}
		return $AddRow;
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_GET["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setQueryStringValue($_GET["fk_user_id"]);
					$this->call_user_id->setQueryStringValue($GLOBALS["telecare_user"]->user_id->QueryStringValue);
					$this->call_user_id->setSessionValue($this->call_user_id->QueryStringValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		} elseif (isset($_POST[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_POST[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "telecare_user") {
				$bValidMaster = TRUE;
				if (@$_POST["fk_user_id"] <> "") {
					$GLOBALS["telecare_user"]->user_id->setFormValue($_POST["fk_user_id"]);
					$this->call_user_id->setFormValue($GLOBALS["telecare_user"]->user_id->FormValue);
					$this->call_user_id->setSessionValue($this->call_user_id->FormValue);
					if (!is_numeric($GLOBALS["telecare_user"]->user_id->FormValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "telecare_user") {
				if ($this->call_user_id->QueryStringValue == "") $this->call_user_id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); // Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_calllist.php", "", $this->TableVar, TRUE);
		$PageId = ($this->CurrentAction == "C") ? "Copy" : "Add";
		$Breadcrumb->Add("add", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_call_add)) $telecare_call_add = new ctelecare_call_add();

// Page init
$telecare_call_add->Page_Init();

// Page main
$telecare_call_add->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_call_add->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "add";
var CurrentForm = ftelecare_calladd = new ew_Form("ftelecare_calladd", "add");

// Validate form
ftelecare_calladd.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_call_user_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_user_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_call_requested_date");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_requested_date->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_call_start_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_start_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_call_close_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_close_on->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_calladd.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_calladd.ValidateRequired = true;
<?php } else { ?>
ftelecare_calladd.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_calladd.Lists["x_call_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_calladd.Lists["x_call_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_calladd.Lists["x_call_closed"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_calladd.Lists["x_call_closed"].Options = <?php echo json_encode($telecare_call->call_closed->Options()) ?>;

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_call_add->ShowPageHeader(); ?>
<?php
$telecare_call_add->ShowMessage();
?>
<form name="ftelecare_calladd" id="ftelecare_calladd" class="<?php echo $telecare_call_add->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_call_add->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_call_add->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_call">
<input type="hidden" name="a_add" id="a_add" value="A">
<div>
<?php if ($telecare_call->call_user_id->Visible) { // call_user_id ?>
	<div id="r_call_user_id" class="form-group">
		<label id="elh_telecare_call_call_user_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_call->call_user_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_call->call_user_id->CellAttributes() ?>>
<?php if ($telecare_call->call_user_id->getSessionValue() <> "") { ?>
<span id="el_telecare_call_call_user_id">
<span<?php echo $telecare_call->call_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x_call_user_id" name="x_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el_telecare_call_call_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_call->call_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_call->call_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x_call_user_id" style="white-space: nowrap; z-index: 8980">
	<input type="text" name="sv_x_call_user_id" id="sv_x_call_user_id" value="<?php echo $telecare_call->call_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>"<?php echo $telecare_call->call_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_user_id->DisplayValueSeparator) ? json_encode($telecare_call->call_user_id->DisplayValueSeparator) : $telecare_call->call_user_id->DisplayValueSeparator) ?>" name="x_call_user_id" id="x_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_call->Lookup_Selecting($telecare_call->call_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x_call_user_id" id="q_x_call_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_calladd.CreateAutoSuggest({"id":"x_call_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php echo $telecare_call->call_user_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_call->call_requested_date->Visible) { // call_requested_date ?>
	<div id="r_call_requested_date" class="form-group">
		<label id="elh_telecare_call_call_requested_date" for="x_call_requested_date" class="col-sm-2 control-label ewLabel"><?php echo $telecare_call->call_requested_date->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_call->call_requested_date->CellAttributes() ?>>
<span id="el_telecare_call_call_requested_date">
<input type="text" data-table="telecare_call" data-field="x_call_requested_date" data-format="7" name="x_call_requested_date" id="x_call_requested_date" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_requested_date->EditValue ?>"<?php echo $telecare_call->call_requested_date->EditAttributes() ?>>
<?php if (!$telecare_call->call_requested_date->ReadOnly && !$telecare_call->call_requested_date->Disabled && !isset($telecare_call->call_requested_date->EditAttrs["readonly"]) && !isset($telecare_call->call_requested_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_calladd", "x_call_requested_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_call->call_requested_date->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_call->call_start_on->Visible) { // call_start_on ?>
	<div id="r_call_start_on" class="form-group">
		<label id="elh_telecare_call_call_start_on" for="x_call_start_on" class="col-sm-2 control-label ewLabel"><?php echo $telecare_call->call_start_on->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_call->call_start_on->CellAttributes() ?>>
<span id="el_telecare_call_call_start_on">
<input type="text" data-table="telecare_call" data-field="x_call_start_on" data-format="7" name="x_call_start_on" id="x_call_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_start_on->EditValue ?>"<?php echo $telecare_call->call_start_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_start_on->ReadOnly && !$telecare_call->call_start_on->Disabled && !isset($telecare_call->call_start_on->EditAttrs["readonly"]) && !isset($telecare_call->call_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_calladd", "x_call_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_call->call_start_on->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_call->call_close_on->Visible) { // call_close_on ?>
	<div id="r_call_close_on" class="form-group">
		<label id="elh_telecare_call_call_close_on" for="x_call_close_on" class="col-sm-2 control-label ewLabel"><?php echo $telecare_call->call_close_on->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_call->call_close_on->CellAttributes() ?>>
<span id="el_telecare_call_call_close_on">
<input type="text" data-table="telecare_call" data-field="x_call_close_on" data-format="7" name="x_call_close_on" id="x_call_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_close_on->EditValue ?>"<?php echo $telecare_call->call_close_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_close_on->ReadOnly && !$telecare_call->call_close_on->Disabled && !isset($telecare_call->call_close_on->EditAttrs["readonly"]) && !isset($telecare_call->call_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_calladd", "x_call_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php echo $telecare_call->call_close_on->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_call->call_user_note->Visible) { // call_user_note ?>
	<div id="r_call_user_note" class="form-group">
		<label id="elh_telecare_call_call_user_note" for="x_call_user_note" class="col-sm-2 control-label ewLabel"><?php echo $telecare_call->call_user_note->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_call->call_user_note->CellAttributes() ?>>
<span id="el_telecare_call_call_user_note">
<textarea data-table="telecare_call" data-field="x_call_user_note" name="x_call_user_note" id="x_call_user_note" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_note->getPlaceHolder()) ?>"<?php echo $telecare_call->call_user_note->EditAttributes() ?>><?php echo $telecare_call->call_user_note->EditValue ?></textarea>
</span>
<?php echo $telecare_call->call_user_note->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_call->call_admin_note->Visible) { // call_admin_note ?>
	<div id="r_call_admin_note" class="form-group">
		<label id="elh_telecare_call_call_admin_note" for="x_call_admin_note" class="col-sm-2 control-label ewLabel"><?php echo $telecare_call->call_admin_note->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_call->call_admin_note->CellAttributes() ?>>
<span id="el_telecare_call_call_admin_note">
<textarea data-table="telecare_call" data-field="x_call_admin_note" name="x_call_admin_note" id="x_call_admin_note" cols="35" rows="4" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_admin_note->getPlaceHolder()) ?>"<?php echo $telecare_call->call_admin_note->EditAttributes() ?>><?php echo $telecare_call->call_admin_note->EditValue ?></textarea>
</span>
<?php echo $telecare_call->call_admin_note->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_call->call_closed->Visible) { // call_closed ?>
	<div id="r_call_closed" class="form-group">
		<label id="elh_telecare_call_call_closed" class="col-sm-2 control-label ewLabel"><?php echo $telecare_call->call_closed->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_call->call_closed->CellAttributes() ?>>
<span id="el_telecare_call_call_closed">
<div id="tp_x_call_closed" class="ewTemplate"><input type="radio" data-table="telecare_call" data-field="x_call_closed" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_closed->DisplayValueSeparator) ? json_encode($telecare_call->call_closed->DisplayValueSeparator) : $telecare_call->call_closed->DisplayValueSeparator) ?>" name="x_call_closed" id="x_call_closed" value="{value}"<?php echo $telecare_call->call_closed->EditAttributes() ?>></div>
<div id="dsl_x_call_closed" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_call->call_closed->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_call->call_closed->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x_call_closed" id="x_call_closed_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_call->call_closed->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x_call_closed" id="x_call_closed_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->CurrentValue) ?>" checked<?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->CurrentValue ?></label>
<?php
    }
}
?>
</div></div>
</span>
<?php echo $telecare_call->call_closed->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("AddBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_call_add->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_calladd.Init();
</script>
<?php
$telecare_call_add->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_call_add->Page_Terminate();
?>
