<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_apotechinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_apotech_delete = NULL; // Initialize page object first

class ctelecare_apotech_delete extends ctelecare_apotech {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_apotech';

	// Page object name
	var $PageObjName = 'telecare_apotech_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_apotech)
		if (!isset($GLOBALS["telecare_apotech"]) || get_class($GLOBALS["telecare_apotech"]) == "ctelecare_apotech") {
			$GLOBALS["telecare_apotech"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_apotech"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_apotech', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanDelete()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_apotechlist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->apotech_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_apotech;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_apotech);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter = "";
	var $DbDetailFilter = "";
	var $StartRec;
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("telecare_apotechlist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in telecare_apotech class, telecare_apotechinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->apotech_id->setDbValue($rs->fields('apotech_id'));
		$this->apotech_admin_id->setDbValue($rs->fields('apotech_admin_id'));
		$this->apotech_surname->setDbValue($rs->fields('apotech_surname'));
		$this->apotech_name->setDbValue($rs->fields('apotech_name'));
		$this->apotech_email->setDbValue($rs->fields('apotech_email'));
		$this->apotech_mobile->setDbValue($rs->fields('apotech_mobile'));
		$this->apotech_phone->setDbValue($rs->fields('apotech_phone'));
		$this->apotech_is_active->setDbValue($rs->fields('apotech_is_active'));
		$this->apotech_last_update->setDbValue($rs->fields('apotech_last_update'));
		$this->apotech_company_name->setDbValue($rs->fields('apotech_company_name'));
		$this->apotech_company_logo_file->Upload->DbValue = $rs->fields('apotech_company_logo_file');
		$this->apotech_company_logo_file->CurrentValue = $this->apotech_company_logo_file->Upload->DbValue;
		$this->apotech_company_logo_width->setDbValue($rs->fields('apotech_company_logo_width'));
		$this->apotech_company_logo_height->setDbValue($rs->fields('apotech_company_logo_height'));
		$this->apotech_company_logo_size->setDbValue($rs->fields('apotech_company_logo_size'));
		$this->apotech_company_address->setDbValue($rs->fields('apotech_company_address'));
		$this->apotech_company_phone->setDbValue($rs->fields('apotech_company_phone'));
		$this->apotech_company_email->setDbValue($rs->fields('apotech_company_email'));
		$this->apotech_company_web->setDbValue($rs->fields('apotech_company_web'));
		$this->apotech_latitude->setDbValue($rs->fields('apotech_latitude'));
		$this->apotech_longitude->setDbValue($rs->fields('apotech_longitude'));
		$this->apotech_company_region_id->setDbValue($rs->fields('apotech_company_region_id'));
		$this->apotech_company_province_id->setDbValue($rs->fields('apotech_company_province_id'));
		$this->apotech_company_city_id->setDbValue($rs->fields('apotech_company_city_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->apotech_id->DbValue = $row['apotech_id'];
		$this->apotech_admin_id->DbValue = $row['apotech_admin_id'];
		$this->apotech_surname->DbValue = $row['apotech_surname'];
		$this->apotech_name->DbValue = $row['apotech_name'];
		$this->apotech_email->DbValue = $row['apotech_email'];
		$this->apotech_mobile->DbValue = $row['apotech_mobile'];
		$this->apotech_phone->DbValue = $row['apotech_phone'];
		$this->apotech_is_active->DbValue = $row['apotech_is_active'];
		$this->apotech_last_update->DbValue = $row['apotech_last_update'];
		$this->apotech_company_name->DbValue = $row['apotech_company_name'];
		$this->apotech_company_logo_file->Upload->DbValue = $row['apotech_company_logo_file'];
		$this->apotech_company_logo_width->DbValue = $row['apotech_company_logo_width'];
		$this->apotech_company_logo_height->DbValue = $row['apotech_company_logo_height'];
		$this->apotech_company_logo_size->DbValue = $row['apotech_company_logo_size'];
		$this->apotech_company_address->DbValue = $row['apotech_company_address'];
		$this->apotech_company_phone->DbValue = $row['apotech_company_phone'];
		$this->apotech_company_email->DbValue = $row['apotech_company_email'];
		$this->apotech_company_web->DbValue = $row['apotech_company_web'];
		$this->apotech_latitude->DbValue = $row['apotech_latitude'];
		$this->apotech_longitude->DbValue = $row['apotech_longitude'];
		$this->apotech_company_region_id->DbValue = $row['apotech_company_region_id'];
		$this->apotech_company_province_id->DbValue = $row['apotech_company_province_id'];
		$this->apotech_company_city_id->DbValue = $row['apotech_company_city_id'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// apotech_id
		// apotech_admin_id
		// apotech_surname
		// apotech_name
		// apotech_email
		// apotech_mobile
		// apotech_phone
		// apotech_is_active
		// apotech_last_update
		// apotech_company_name
		// apotech_company_logo_file
		// apotech_company_logo_width
		// apotech_company_logo_height
		// apotech_company_logo_size
		// apotech_company_address
		// apotech_company_phone
		// apotech_company_email
		// apotech_company_web
		// apotech_latitude
		// apotech_longitude
		// apotech_company_region_id
		// apotech_company_province_id
		// apotech_company_city_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// apotech_id
		$this->apotech_id->ViewValue = $this->apotech_id->CurrentValue;
		$this->apotech_id->ViewCustomAttributes = "";

		// apotech_admin_id
		if (strval($this->apotech_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->apotech_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->CurrentValue;
			}
		} else {
			$this->apotech_admin_id->ViewValue = NULL;
		}
		$this->apotech_admin_id->ViewCustomAttributes = "";

		// apotech_surname
		$this->apotech_surname->ViewValue = $this->apotech_surname->CurrentValue;
		$this->apotech_surname->ViewCustomAttributes = "";

		// apotech_name
		$this->apotech_name->ViewValue = $this->apotech_name->CurrentValue;
		$this->apotech_name->ViewCustomAttributes = "";

		// apotech_email
		$this->apotech_email->ViewValue = $this->apotech_email->CurrentValue;
		$this->apotech_email->ViewCustomAttributes = "";

		// apotech_mobile
		$this->apotech_mobile->ViewValue = $this->apotech_mobile->CurrentValue;
		$this->apotech_mobile->ViewCustomAttributes = "";

		// apotech_phone
		$this->apotech_phone->ViewValue = $this->apotech_phone->CurrentValue;
		$this->apotech_phone->ViewCustomAttributes = "";

		// apotech_is_active
		if (strval($this->apotech_is_active->CurrentValue) <> "") {
			$this->apotech_is_active->ViewValue = $this->apotech_is_active->OptionCaption($this->apotech_is_active->CurrentValue);
		} else {
			$this->apotech_is_active->ViewValue = NULL;
		}
		$this->apotech_is_active->ViewCustomAttributes = "";

		// apotech_last_update
		$this->apotech_last_update->ViewValue = $this->apotech_last_update->CurrentValue;
		$this->apotech_last_update->ViewValue = ew_FormatDateTime($this->apotech_last_update->ViewValue, 7);
		$this->apotech_last_update->ViewCustomAttributes = "";

		// apotech_company_name
		$this->apotech_company_name->ViewValue = $this->apotech_company_name->CurrentValue;
		$this->apotech_company_name->ViewCustomAttributes = "";

		// apotech_company_logo_file
		if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
			$this->apotech_company_logo_file->ImageWidth = 100;
			$this->apotech_company_logo_file->ImageHeight = 0;
			$this->apotech_company_logo_file->ImageAlt = $this->apotech_company_logo_file->FldAlt();
			$this->apotech_company_logo_file->ViewValue = $this->apotech_company_logo_file->Upload->DbValue;
		} else {
			$this->apotech_company_logo_file->ViewValue = "";
		}
		$this->apotech_company_logo_file->ViewCustomAttributes = "";

		// apotech_company_logo_width
		$this->apotech_company_logo_width->ViewValue = $this->apotech_company_logo_width->CurrentValue;
		$this->apotech_company_logo_width->ViewCustomAttributes = "";

		// apotech_company_logo_height
		$this->apotech_company_logo_height->ViewValue = $this->apotech_company_logo_height->CurrentValue;
		$this->apotech_company_logo_height->ViewCustomAttributes = "";

		// apotech_company_logo_size
		$this->apotech_company_logo_size->ViewValue = $this->apotech_company_logo_size->CurrentValue;
		$this->apotech_company_logo_size->ViewCustomAttributes = "";

		// apotech_company_address
		$this->apotech_company_address->ViewValue = $this->apotech_company_address->CurrentValue;
		$this->apotech_company_address->ViewCustomAttributes = "";

		// apotech_company_phone
		$this->apotech_company_phone->ViewValue = $this->apotech_company_phone->CurrentValue;
		$this->apotech_company_phone->ViewCustomAttributes = "";

		// apotech_company_email
		$this->apotech_company_email->ViewValue = $this->apotech_company_email->CurrentValue;
		$this->apotech_company_email->ViewCustomAttributes = "";

		// apotech_company_web
		$this->apotech_company_web->ViewValue = $this->apotech_company_web->CurrentValue;
		$this->apotech_company_web->ViewCustomAttributes = "";

		// apotech_latitude
		$this->apotech_latitude->ViewValue = $this->apotech_latitude->CurrentValue;
		$this->apotech_latitude->ViewCustomAttributes = "";

		// apotech_longitude
		$this->apotech_longitude->ViewValue = $this->apotech_longitude->CurrentValue;
		$this->apotech_longitude->ViewCustomAttributes = "";

		// apotech_company_region_id
		if (strval($this->apotech_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->apotech_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->CurrentValue;
			}
		} else {
			$this->apotech_company_region_id->ViewValue = NULL;
		}
		$this->apotech_company_region_id->ViewCustomAttributes = "";

		// apotech_company_province_id
		if (strval($this->apotech_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->apotech_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->CurrentValue;
			}
		} else {
			$this->apotech_company_province_id->ViewValue = NULL;
		}
		$this->apotech_company_province_id->ViewCustomAttributes = "";

		// apotech_company_city_id
		if (strval($this->apotech_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->apotech_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->CurrentValue;
			}
		} else {
			$this->apotech_company_city_id->ViewValue = NULL;
		}
		$this->apotech_company_city_id->ViewCustomAttributes = "";

			// apotech_id
			$this->apotech_id->LinkCustomAttributes = "";
			$this->apotech_id->HrefValue = "";
			$this->apotech_id->TooltipValue = "";

			// apotech_admin_id
			$this->apotech_admin_id->LinkCustomAttributes = "";
			$this->apotech_admin_id->HrefValue = "";
			$this->apotech_admin_id->TooltipValue = "";

			// apotech_surname
			$this->apotech_surname->LinkCustomAttributes = "";
			$this->apotech_surname->HrefValue = "";
			$this->apotech_surname->TooltipValue = "";

			// apotech_name
			$this->apotech_name->LinkCustomAttributes = "";
			$this->apotech_name->HrefValue = "";
			$this->apotech_name->TooltipValue = "";

			// apotech_company_name
			$this->apotech_company_name->LinkCustomAttributes = "";
			$this->apotech_company_name->HrefValue = "";
			$this->apotech_company_name->TooltipValue = "";

			// apotech_company_phone
			$this->apotech_company_phone->LinkCustomAttributes = "";
			$this->apotech_company_phone->HrefValue = "";
			$this->apotech_company_phone->TooltipValue = "";

			// apotech_company_email
			$this->apotech_company_email->LinkCustomAttributes = "";
			$this->apotech_company_email->HrefValue = "";
			$this->apotech_company_email->TooltipValue = "";

			// apotech_latitude
			$this->apotech_latitude->LinkCustomAttributes = "";
			$this->apotech_latitude->HrefValue = "";
			$this->apotech_latitude->TooltipValue = "";

			// apotech_longitude
			$this->apotech_longitude->LinkCustomAttributes = "";
			$this->apotech_longitude->HrefValue = "";
			$this->apotech_longitude->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $Language, $Security;
		if (!$Security->CanDelete()) {
			$this->setFailureMessage($Language->Phrase("NoDeletePermission")); // No delete permission
			return FALSE;
		}
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$rows = ($rs) ? $rs->GetRows() : array();
		$conn->BeginTrans();

		// Clone old rows
		$rsold = $rows;
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['apotech_id'];
				$this->LoadDbValues($row);
				@unlink(ew_UploadPathEx(TRUE, $this->apotech_company_logo_file->OldUploadPath) . $row['apotech_company_logo_file']);
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_apotechlist.php", "", $this->TableVar, TRUE);
		$PageId = "delete";
		$Breadcrumb->Add("delete", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_apotech_delete)) $telecare_apotech_delete = new ctelecare_apotech_delete();

// Page init
$telecare_apotech_delete->Page_Init();

// Page main
$telecare_apotech_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_apotech_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "delete";
var CurrentForm = ftelecare_apotechdelete = new ew_Form("ftelecare_apotechdelete", "delete");

// Form_CustomValidate event
ftelecare_apotechdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_apotechdelete.ValidateRequired = true;
<?php } else { ?>
ftelecare_apotechdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_apotechdelete.Lists["x_apotech_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($telecare_apotech_delete->Recordset = $telecare_apotech_delete->LoadRecordset())
	$telecare_apotech_deleteTotalRecs = $telecare_apotech_delete->Recordset->RecordCount(); // Get record count
if ($telecare_apotech_deleteTotalRecs <= 0) { // No record found, exit
	if ($telecare_apotech_delete->Recordset)
		$telecare_apotech_delete->Recordset->Close();
	$telecare_apotech_delete->Page_Terminate("telecare_apotechlist.php"); // Return to list
}
?>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_apotech_delete->ShowPageHeader(); ?>
<?php
$telecare_apotech_delete->ShowMessage();
?>
<form name="ftelecare_apotechdelete" id="ftelecare_apotechdelete" class="form-inline ewForm ewDeleteForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_apotech_delete->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_apotech_delete->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_apotech">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($telecare_apotech_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<div class="ewGrid">
<div class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table class="table ewTable">
<?php echo $telecare_apotech->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($telecare_apotech->apotech_id->Visible) { // apotech_id ?>
		<th><span id="elh_telecare_apotech_apotech_id" class="telecare_apotech_apotech_id"><?php echo $telecare_apotech->apotech_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_admin_id->Visible) { // apotech_admin_id ?>
		<th><span id="elh_telecare_apotech_apotech_admin_id" class="telecare_apotech_apotech_admin_id"><?php echo $telecare_apotech->apotech_admin_id->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_surname->Visible) { // apotech_surname ?>
		<th><span id="elh_telecare_apotech_apotech_surname" class="telecare_apotech_apotech_surname"><?php echo $telecare_apotech->apotech_surname->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_name->Visible) { // apotech_name ?>
		<th><span id="elh_telecare_apotech_apotech_name" class="telecare_apotech_apotech_name"><?php echo $telecare_apotech->apotech_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_name->Visible) { // apotech_company_name ?>
		<th><span id="elh_telecare_apotech_apotech_company_name" class="telecare_apotech_apotech_company_name"><?php echo $telecare_apotech->apotech_company_name->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_phone->Visible) { // apotech_company_phone ?>
		<th><span id="elh_telecare_apotech_apotech_company_phone" class="telecare_apotech_apotech_company_phone"><?php echo $telecare_apotech->apotech_company_phone->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_email->Visible) { // apotech_company_email ?>
		<th><span id="elh_telecare_apotech_apotech_company_email" class="telecare_apotech_apotech_company_email"><?php echo $telecare_apotech->apotech_company_email->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_latitude->Visible) { // apotech_latitude ?>
		<th><span id="elh_telecare_apotech_apotech_latitude" class="telecare_apotech_apotech_latitude"><?php echo $telecare_apotech->apotech_latitude->FldCaption() ?></span></th>
<?php } ?>
<?php if ($telecare_apotech->apotech_longitude->Visible) { // apotech_longitude ?>
		<th><span id="elh_telecare_apotech_apotech_longitude" class="telecare_apotech_apotech_longitude"><?php echo $telecare_apotech->apotech_longitude->FldCaption() ?></span></th>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$telecare_apotech_delete->RecCnt = 0;
$i = 0;
while (!$telecare_apotech_delete->Recordset->EOF) {
	$telecare_apotech_delete->RecCnt++;
	$telecare_apotech_delete->RowCnt++;

	// Set row properties
	$telecare_apotech->ResetAttrs();
	$telecare_apotech->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$telecare_apotech_delete->LoadRowValues($telecare_apotech_delete->Recordset);

	// Render row
	$telecare_apotech_delete->RenderRow();
?>
	<tr<?php echo $telecare_apotech->RowAttributes() ?>>
<?php if ($telecare_apotech->apotech_id->Visible) { // apotech_id ?>
		<td<?php echo $telecare_apotech->apotech_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_id" class="telecare_apotech_apotech_id">
<span<?php echo $telecare_apotech->apotech_id->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_admin_id->Visible) { // apotech_admin_id ?>
		<td<?php echo $telecare_apotech->apotech_admin_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_admin_id" class="telecare_apotech_apotech_admin_id">
<span<?php echo $telecare_apotech->apotech_admin_id->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_admin_id->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_surname->Visible) { // apotech_surname ?>
		<td<?php echo $telecare_apotech->apotech_surname->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_surname" class="telecare_apotech_apotech_surname">
<span<?php echo $telecare_apotech->apotech_surname->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_surname->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_name->Visible) { // apotech_name ?>
		<td<?php echo $telecare_apotech->apotech_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_name" class="telecare_apotech_apotech_name">
<span<?php echo $telecare_apotech->apotech_name->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_name->Visible) { // apotech_company_name ?>
		<td<?php echo $telecare_apotech->apotech_company_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_company_name" class="telecare_apotech_apotech_company_name">
<span<?php echo $telecare_apotech->apotech_company_name->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_phone->Visible) { // apotech_company_phone ?>
		<td<?php echo $telecare_apotech->apotech_company_phone->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_company_phone" class="telecare_apotech_apotech_company_phone">
<span<?php echo $telecare_apotech->apotech_company_phone->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_phone->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_company_email->Visible) { // apotech_company_email ?>
		<td<?php echo $telecare_apotech->apotech_company_email->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_company_email" class="telecare_apotech_apotech_company_email">
<span<?php echo $telecare_apotech->apotech_company_email->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_email->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_latitude->Visible) { // apotech_latitude ?>
		<td<?php echo $telecare_apotech->apotech_latitude->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_latitude" class="telecare_apotech_apotech_latitude">
<span<?php echo $telecare_apotech->apotech_latitude->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_latitude->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($telecare_apotech->apotech_longitude->Visible) { // apotech_longitude ?>
		<td<?php echo $telecare_apotech->apotech_longitude->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_delete->RowCnt ?>_telecare_apotech_apotech_longitude" class="telecare_apotech_apotech_longitude">
<span<?php echo $telecare_apotech->apotech_longitude->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_longitude->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$telecare_apotech_delete->Recordset->MoveNext();
}
$telecare_apotech_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</div>
<div>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_apotech_delete->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
</div>
</form>
<script type="text/javascript">
ftelecare_apotechdelete.Init();
</script>
<?php
$telecare_apotech_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_apotech_delete->Page_Terminate();
?>
