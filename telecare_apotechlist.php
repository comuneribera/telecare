<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_apotechinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "telecare_prescriptiongridcls.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_apotech_list = NULL; // Initialize page object first

class ctelecare_apotech_list extends ctelecare_apotech {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_apotech';

	// Page object name
	var $PageObjName = 'telecare_apotech_list';

	// Grid form hidden field names
	var $FormName = 'ftelecare_apotechlist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Custom export
	var $ExportExcelCustom = FALSE;
	var $ExportWordCustom = FALSE;
	var $ExportPdfCustom = FALSE;
	var $ExportEmailCustom = FALSE;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_apotech)
		if (!isset($GLOBALS["telecare_apotech"]) || get_class($GLOBALS["telecare_apotech"]) == "ctelecare_apotech") {
			$GLOBALS["telecare_apotech"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_apotech"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "telecare_apotechadd.php?" . EW_TABLE_SHOW_DETAIL . "=";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "telecare_apotechdelete.php";
		$this->MultiUpdateUrl = "telecare_apotechupdate.php";

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_apotech', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "div";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "div";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "div";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "div";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";

		// Filter options
		$this->FilterOptions = new cListOptions();
		$this->FilterOptions->Tag = "div";
		$this->FilterOptions->TagClassName = "ewFilterOption ftelecare_apotechlistsrch";

		// List actions
		$this->ListActions = new cListActions();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanList()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			$this->Page_Terminate(ew_GetUrl("index.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Get export parameters
		$custom = "";
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
			$custom = @$_GET["custom"];
		} elseif (@$_POST["export"] <> "") {
			$this->Export = $_POST["export"];
			$custom = @$_POST["custom"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
			$custom = @$_POST["custom"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get custom export parameters
		if ($this->Export <> "" && $custom <> "") {
			$this->CustomExport = $this->Export;
			$this->Export = "print";
		}
		$gsCustomExport = $this->CustomExport;
		$gsExport = $this->Export; // Get export parameter, used in header

		// Update Export URLs
		if (defined("EW_USE_PHPEXCEL"))
			$this->ExportExcelCustom = FALSE;
		if ($this->ExportExcelCustom)
			$this->ExportExcelUrl .= "&amp;custom=1";
		if (defined("EW_USE_PHPWORD"))
			$this->ExportWordCustom = FALSE;
		if ($this->ExportWordCustom)
			$this->ExportWordUrl .= "&amp;custom=1";
		if ($this->ExportPdfCustom)
			$this->ExportPdfUrl .= "&amp;custom=1";
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->apotech_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {

			// Process auto fill for detail table 'telecare_prescription'
			if (@$_POST["grid"] == "ftelecare_prescriptiongrid") {
				if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;
				$GLOBALS["telecare_prescription_grid"]->Page_Init();
				$this->Page_Terminate();
				exit();
			}
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();

		// Setup other options
		$this->SetupOtherOptions();

		// Set up custom action (compatible with old version)
		foreach ($this->CustomActions as $name => $action)
			$this->ListActions->Add($name, $action);

		// Show checkbox column if multiple action
		foreach ($this->ListActions->Items as $listaction) {
			if ($listaction->Select == EW_ACTION_MULTIPLE && $listaction->Allow) {
				$this->ListOptions->Items["checkbox"]->Visible = TRUE;
				break;
			}
		}
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_apotech;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_apotech);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $SearchOptions; // Search options
	var $OtherOptions = array(); // Other options
	var $FilterOptions; // Filter options
	var $ListActions; // List actions
	var $SelectedCount = 0;
	var $SelectedIndex = 0;
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $DefaultSearchWhere = ""; // Default search WHERE clause
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $MultiColumnClass;
	var $MultiColumnEditClass = "col-sm-12";
	var $MultiColumnCnt = 12;
	var $MultiColumnEditCnt = 12;
	var $GridCnt = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $DetailPages;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process list action first
			if ($this->ProcessListAction()) // Ajax request
				$this->Page_Terminate();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			if ($this->Export == "")
				$this->SetupBreadcrumb();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide options
			if ($this->Export <> "" || $this->CurrentAction <> "") {
				$this->ExportOptions->HideAllOptions();
				$this->FilterOptions->HideAllOptions();
			}

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Get default search criteria
			ew_AddFilter($this->DefaultSearchWhere, $this->BasicSearchWhere(TRUE));
			ew_AddFilter($this->DefaultSearchWhere, $this->AdvancedSearchWhere(TRUE));

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values

			// Restore filter list
			$this->RestoreFilterList();
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset / export
			if (($this->Export <> "" || $this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall") && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		if (!$Security->CanList())
			$sFilter = "(0=1)"; // Filter all records
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if ($this->CustomExport == "" && in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			$this->Page_Terminate(); // Terminate response
			exit();
		}

		// Load record count first
		if (!$this->IsAddOrEdit()) {
			$bSelectLimit = $this->UseSelectLimit;
			if ($bSelectLimit) {
				$this->TotalRecs = $this->SelectRecordCount();
			} else {
				if ($this->Recordset = $this->LoadRecordset())
					$this->TotalRecs = $this->Recordset->RecordCount();
			}
		}

		// Search options
		$this->SetupSearchOptions();
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue($this->FormKeyName));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->apotech_id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->apotech_id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Get list of filters
	function GetFilterList() {

		// Initialize
		$sFilterList = "";
		$sFilterList = ew_Concat($sFilterList, $this->apotech_id->AdvancedSearch->ToJSON(), ","); // Field apotech_id
		$sFilterList = ew_Concat($sFilterList, $this->apotech_admin_id->AdvancedSearch->ToJSON(), ","); // Field apotech_admin_id
		$sFilterList = ew_Concat($sFilterList, $this->apotech_surname->AdvancedSearch->ToJSON(), ","); // Field apotech_surname
		$sFilterList = ew_Concat($sFilterList, $this->apotech_name->AdvancedSearch->ToJSON(), ","); // Field apotech_name
		$sFilterList = ew_Concat($sFilterList, $this->apotech_email->AdvancedSearch->ToJSON(), ","); // Field apotech_email
		$sFilterList = ew_Concat($sFilterList, $this->apotech_mobile->AdvancedSearch->ToJSON(), ","); // Field apotech_mobile
		$sFilterList = ew_Concat($sFilterList, $this->apotech_phone->AdvancedSearch->ToJSON(), ","); // Field apotech_phone
		$sFilterList = ew_Concat($sFilterList, $this->apotech_is_active->AdvancedSearch->ToJSON(), ","); // Field apotech_is_active
		$sFilterList = ew_Concat($sFilterList, $this->apotech_last_update->AdvancedSearch->ToJSON(), ","); // Field apotech_last_update
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_name->AdvancedSearch->ToJSON(), ","); // Field apotech_company_name
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_logo_file->AdvancedSearch->ToJSON(), ","); // Field apotech_company_logo_file
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_address->AdvancedSearch->ToJSON(), ","); // Field apotech_company_address
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_phone->AdvancedSearch->ToJSON(), ","); // Field apotech_company_phone
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_email->AdvancedSearch->ToJSON(), ","); // Field apotech_company_email
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_web->AdvancedSearch->ToJSON(), ","); // Field apotech_company_web
		$sFilterList = ew_Concat($sFilterList, $this->apotech_latitude->AdvancedSearch->ToJSON(), ","); // Field apotech_latitude
		$sFilterList = ew_Concat($sFilterList, $this->apotech_longitude->AdvancedSearch->ToJSON(), ","); // Field apotech_longitude
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_region_id->AdvancedSearch->ToJSON(), ","); // Field apotech_company_region_id
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_province_id->AdvancedSearch->ToJSON(), ","); // Field apotech_company_province_id
		$sFilterList = ew_Concat($sFilterList, $this->apotech_company_city_id->AdvancedSearch->ToJSON(), ","); // Field apotech_company_city_id
		if ($this->BasicSearch->Keyword <> "") {
			$sWrk = "\"" . EW_TABLE_BASIC_SEARCH . "\":\"" . ew_JsEncode2($this->BasicSearch->Keyword) . "\",\"" . EW_TABLE_BASIC_SEARCH_TYPE . "\":\"" . ew_JsEncode2($this->BasicSearch->Type) . "\"";
			$sFilterList = ew_Concat($sFilterList, $sWrk, ",");
		}

		// Return filter list in json
		return ($sFilterList <> "") ? "{" . $sFilterList . "}" : "null";
	}

	// Restore list of filters
	function RestoreFilterList() {

		// Return if not reset filter
		if (@$_POST["cmd"] <> "resetfilter")
			return FALSE;
		$filter = json_decode(ew_StripSlashes(@$_POST["filter"]), TRUE);
		$this->Command = "search";

		// Field apotech_id
		$this->apotech_id->AdvancedSearch->SearchValue = @$filter["x_apotech_id"];
		$this->apotech_id->AdvancedSearch->SearchOperator = @$filter["z_apotech_id"];
		$this->apotech_id->AdvancedSearch->SearchCondition = @$filter["v_apotech_id"];
		$this->apotech_id->AdvancedSearch->SearchValue2 = @$filter["y_apotech_id"];
		$this->apotech_id->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_id"];
		$this->apotech_id->AdvancedSearch->Save();

		// Field apotech_admin_id
		$this->apotech_admin_id->AdvancedSearch->SearchValue = @$filter["x_apotech_admin_id"];
		$this->apotech_admin_id->AdvancedSearch->SearchOperator = @$filter["z_apotech_admin_id"];
		$this->apotech_admin_id->AdvancedSearch->SearchCondition = @$filter["v_apotech_admin_id"];
		$this->apotech_admin_id->AdvancedSearch->SearchValue2 = @$filter["y_apotech_admin_id"];
		$this->apotech_admin_id->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_admin_id"];
		$this->apotech_admin_id->AdvancedSearch->Save();

		// Field apotech_surname
		$this->apotech_surname->AdvancedSearch->SearchValue = @$filter["x_apotech_surname"];
		$this->apotech_surname->AdvancedSearch->SearchOperator = @$filter["z_apotech_surname"];
		$this->apotech_surname->AdvancedSearch->SearchCondition = @$filter["v_apotech_surname"];
		$this->apotech_surname->AdvancedSearch->SearchValue2 = @$filter["y_apotech_surname"];
		$this->apotech_surname->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_surname"];
		$this->apotech_surname->AdvancedSearch->Save();

		// Field apotech_name
		$this->apotech_name->AdvancedSearch->SearchValue = @$filter["x_apotech_name"];
		$this->apotech_name->AdvancedSearch->SearchOperator = @$filter["z_apotech_name"];
		$this->apotech_name->AdvancedSearch->SearchCondition = @$filter["v_apotech_name"];
		$this->apotech_name->AdvancedSearch->SearchValue2 = @$filter["y_apotech_name"];
		$this->apotech_name->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_name"];
		$this->apotech_name->AdvancedSearch->Save();

		// Field apotech_email
		$this->apotech_email->AdvancedSearch->SearchValue = @$filter["x_apotech_email"];
		$this->apotech_email->AdvancedSearch->SearchOperator = @$filter["z_apotech_email"];
		$this->apotech_email->AdvancedSearch->SearchCondition = @$filter["v_apotech_email"];
		$this->apotech_email->AdvancedSearch->SearchValue2 = @$filter["y_apotech_email"];
		$this->apotech_email->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_email"];
		$this->apotech_email->AdvancedSearch->Save();

		// Field apotech_mobile
		$this->apotech_mobile->AdvancedSearch->SearchValue = @$filter["x_apotech_mobile"];
		$this->apotech_mobile->AdvancedSearch->SearchOperator = @$filter["z_apotech_mobile"];
		$this->apotech_mobile->AdvancedSearch->SearchCondition = @$filter["v_apotech_mobile"];
		$this->apotech_mobile->AdvancedSearch->SearchValue2 = @$filter["y_apotech_mobile"];
		$this->apotech_mobile->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_mobile"];
		$this->apotech_mobile->AdvancedSearch->Save();

		// Field apotech_phone
		$this->apotech_phone->AdvancedSearch->SearchValue = @$filter["x_apotech_phone"];
		$this->apotech_phone->AdvancedSearch->SearchOperator = @$filter["z_apotech_phone"];
		$this->apotech_phone->AdvancedSearch->SearchCondition = @$filter["v_apotech_phone"];
		$this->apotech_phone->AdvancedSearch->SearchValue2 = @$filter["y_apotech_phone"];
		$this->apotech_phone->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_phone"];
		$this->apotech_phone->AdvancedSearch->Save();

		// Field apotech_is_active
		$this->apotech_is_active->AdvancedSearch->SearchValue = @$filter["x_apotech_is_active"];
		$this->apotech_is_active->AdvancedSearch->SearchOperator = @$filter["z_apotech_is_active"];
		$this->apotech_is_active->AdvancedSearch->SearchCondition = @$filter["v_apotech_is_active"];
		$this->apotech_is_active->AdvancedSearch->SearchValue2 = @$filter["y_apotech_is_active"];
		$this->apotech_is_active->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_is_active"];
		$this->apotech_is_active->AdvancedSearch->Save();

		// Field apotech_last_update
		$this->apotech_last_update->AdvancedSearch->SearchValue = @$filter["x_apotech_last_update"];
		$this->apotech_last_update->AdvancedSearch->SearchOperator = @$filter["z_apotech_last_update"];
		$this->apotech_last_update->AdvancedSearch->SearchCondition = @$filter["v_apotech_last_update"];
		$this->apotech_last_update->AdvancedSearch->SearchValue2 = @$filter["y_apotech_last_update"];
		$this->apotech_last_update->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_last_update"];
		$this->apotech_last_update->AdvancedSearch->Save();

		// Field apotech_company_name
		$this->apotech_company_name->AdvancedSearch->SearchValue = @$filter["x_apotech_company_name"];
		$this->apotech_company_name->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_name"];
		$this->apotech_company_name->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_name"];
		$this->apotech_company_name->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_name"];
		$this->apotech_company_name->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_name"];
		$this->apotech_company_name->AdvancedSearch->Save();

		// Field apotech_company_logo_file
		$this->apotech_company_logo_file->AdvancedSearch->SearchValue = @$filter["x_apotech_company_logo_file"];
		$this->apotech_company_logo_file->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_logo_file"];
		$this->apotech_company_logo_file->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_logo_file"];
		$this->apotech_company_logo_file->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_logo_file"];
		$this->apotech_company_logo_file->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_logo_file"];
		$this->apotech_company_logo_file->AdvancedSearch->Save();

		// Field apotech_company_address
		$this->apotech_company_address->AdvancedSearch->SearchValue = @$filter["x_apotech_company_address"];
		$this->apotech_company_address->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_address"];
		$this->apotech_company_address->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_address"];
		$this->apotech_company_address->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_address"];
		$this->apotech_company_address->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_address"];
		$this->apotech_company_address->AdvancedSearch->Save();

		// Field apotech_company_phone
		$this->apotech_company_phone->AdvancedSearch->SearchValue = @$filter["x_apotech_company_phone"];
		$this->apotech_company_phone->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_phone"];
		$this->apotech_company_phone->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_phone"];
		$this->apotech_company_phone->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_phone"];
		$this->apotech_company_phone->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_phone"];
		$this->apotech_company_phone->AdvancedSearch->Save();

		// Field apotech_company_email
		$this->apotech_company_email->AdvancedSearch->SearchValue = @$filter["x_apotech_company_email"];
		$this->apotech_company_email->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_email"];
		$this->apotech_company_email->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_email"];
		$this->apotech_company_email->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_email"];
		$this->apotech_company_email->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_email"];
		$this->apotech_company_email->AdvancedSearch->Save();

		// Field apotech_company_web
		$this->apotech_company_web->AdvancedSearch->SearchValue = @$filter["x_apotech_company_web"];
		$this->apotech_company_web->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_web"];
		$this->apotech_company_web->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_web"];
		$this->apotech_company_web->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_web"];
		$this->apotech_company_web->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_web"];
		$this->apotech_company_web->AdvancedSearch->Save();

		// Field apotech_latitude
		$this->apotech_latitude->AdvancedSearch->SearchValue = @$filter["x_apotech_latitude"];
		$this->apotech_latitude->AdvancedSearch->SearchOperator = @$filter["z_apotech_latitude"];
		$this->apotech_latitude->AdvancedSearch->SearchCondition = @$filter["v_apotech_latitude"];
		$this->apotech_latitude->AdvancedSearch->SearchValue2 = @$filter["y_apotech_latitude"];
		$this->apotech_latitude->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_latitude"];
		$this->apotech_latitude->AdvancedSearch->Save();

		// Field apotech_longitude
		$this->apotech_longitude->AdvancedSearch->SearchValue = @$filter["x_apotech_longitude"];
		$this->apotech_longitude->AdvancedSearch->SearchOperator = @$filter["z_apotech_longitude"];
		$this->apotech_longitude->AdvancedSearch->SearchCondition = @$filter["v_apotech_longitude"];
		$this->apotech_longitude->AdvancedSearch->SearchValue2 = @$filter["y_apotech_longitude"];
		$this->apotech_longitude->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_longitude"];
		$this->apotech_longitude->AdvancedSearch->Save();

		// Field apotech_company_region_id
		$this->apotech_company_region_id->AdvancedSearch->SearchValue = @$filter["x_apotech_company_region_id"];
		$this->apotech_company_region_id->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_region_id"];
		$this->apotech_company_region_id->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_region_id"];
		$this->apotech_company_region_id->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_region_id"];
		$this->apotech_company_region_id->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_region_id"];
		$this->apotech_company_region_id->AdvancedSearch->Save();

		// Field apotech_company_province_id
		$this->apotech_company_province_id->AdvancedSearch->SearchValue = @$filter["x_apotech_company_province_id"];
		$this->apotech_company_province_id->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_province_id"];
		$this->apotech_company_province_id->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_province_id"];
		$this->apotech_company_province_id->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_province_id"];
		$this->apotech_company_province_id->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_province_id"];
		$this->apotech_company_province_id->AdvancedSearch->Save();

		// Field apotech_company_city_id
		$this->apotech_company_city_id->AdvancedSearch->SearchValue = @$filter["x_apotech_company_city_id"];
		$this->apotech_company_city_id->AdvancedSearch->SearchOperator = @$filter["z_apotech_company_city_id"];
		$this->apotech_company_city_id->AdvancedSearch->SearchCondition = @$filter["v_apotech_company_city_id"];
		$this->apotech_company_city_id->AdvancedSearch->SearchValue2 = @$filter["y_apotech_company_city_id"];
		$this->apotech_company_city_id->AdvancedSearch->SearchOperator2 = @$filter["w_apotech_company_city_id"];
		$this->apotech_company_city_id->AdvancedSearch->Save();
		$this->BasicSearch->setKeyword(@$filter[EW_TABLE_BASIC_SEARCH]);
		$this->BasicSearch->setType(@$filter[EW_TABLE_BASIC_SEARCH_TYPE]);
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere($Default = FALSE) {
		global $Security;
		$sWhere = "";
		if (!$Security->CanSearch()) return "";
		$this->BuildSearchSql($sWhere, $this->apotech_id, $Default, FALSE); // apotech_id
		$this->BuildSearchSql($sWhere, $this->apotech_admin_id, $Default, FALSE); // apotech_admin_id
		$this->BuildSearchSql($sWhere, $this->apotech_surname, $Default, FALSE); // apotech_surname
		$this->BuildSearchSql($sWhere, $this->apotech_name, $Default, FALSE); // apotech_name
		$this->BuildSearchSql($sWhere, $this->apotech_email, $Default, FALSE); // apotech_email
		$this->BuildSearchSql($sWhere, $this->apotech_mobile, $Default, FALSE); // apotech_mobile
		$this->BuildSearchSql($sWhere, $this->apotech_phone, $Default, FALSE); // apotech_phone
		$this->BuildSearchSql($sWhere, $this->apotech_is_active, $Default, FALSE); // apotech_is_active
		$this->BuildSearchSql($sWhere, $this->apotech_last_update, $Default, FALSE); // apotech_last_update
		$this->BuildSearchSql($sWhere, $this->apotech_company_name, $Default, FALSE); // apotech_company_name
		$this->BuildSearchSql($sWhere, $this->apotech_company_logo_file, $Default, FALSE); // apotech_company_logo_file
		$this->BuildSearchSql($sWhere, $this->apotech_company_address, $Default, FALSE); // apotech_company_address
		$this->BuildSearchSql($sWhere, $this->apotech_company_phone, $Default, FALSE); // apotech_company_phone
		$this->BuildSearchSql($sWhere, $this->apotech_company_email, $Default, FALSE); // apotech_company_email
		$this->BuildSearchSql($sWhere, $this->apotech_company_web, $Default, FALSE); // apotech_company_web
		$this->BuildSearchSql($sWhere, $this->apotech_latitude, $Default, FALSE); // apotech_latitude
		$this->BuildSearchSql($sWhere, $this->apotech_longitude, $Default, FALSE); // apotech_longitude
		$this->BuildSearchSql($sWhere, $this->apotech_company_region_id, $Default, FALSE); // apotech_company_region_id
		$this->BuildSearchSql($sWhere, $this->apotech_company_province_id, $Default, FALSE); // apotech_company_province_id
		$this->BuildSearchSql($sWhere, $this->apotech_company_city_id, $Default, FALSE); // apotech_company_city_id

		// Set up search parm
		if (!$Default && $sWhere <> "") {
			$this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->apotech_id->AdvancedSearch->Save(); // apotech_id
			$this->apotech_admin_id->AdvancedSearch->Save(); // apotech_admin_id
			$this->apotech_surname->AdvancedSearch->Save(); // apotech_surname
			$this->apotech_name->AdvancedSearch->Save(); // apotech_name
			$this->apotech_email->AdvancedSearch->Save(); // apotech_email
			$this->apotech_mobile->AdvancedSearch->Save(); // apotech_mobile
			$this->apotech_phone->AdvancedSearch->Save(); // apotech_phone
			$this->apotech_is_active->AdvancedSearch->Save(); // apotech_is_active
			$this->apotech_last_update->AdvancedSearch->Save(); // apotech_last_update
			$this->apotech_company_name->AdvancedSearch->Save(); // apotech_company_name
			$this->apotech_company_logo_file->AdvancedSearch->Save(); // apotech_company_logo_file
			$this->apotech_company_address->AdvancedSearch->Save(); // apotech_company_address
			$this->apotech_company_phone->AdvancedSearch->Save(); // apotech_company_phone
			$this->apotech_company_email->AdvancedSearch->Save(); // apotech_company_email
			$this->apotech_company_web->AdvancedSearch->Save(); // apotech_company_web
			$this->apotech_latitude->AdvancedSearch->Save(); // apotech_latitude
			$this->apotech_longitude->AdvancedSearch->Save(); // apotech_longitude
			$this->apotech_company_region_id->AdvancedSearch->Save(); // apotech_company_region_id
			$this->apotech_company_province_id->AdvancedSearch->Save(); // apotech_company_province_id
			$this->apotech_company_city_id->AdvancedSearch->Save(); // apotech_company_city_id
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $Default, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = ($Default) ? $Fld->AdvancedSearch->SearchValueDefault : $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = ($Default) ? $Fld->AdvancedSearch->SearchOperatorDefault : $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = ($Default) ? $Fld->AdvancedSearch->SearchConditionDefault : $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = ($Default) ? $Fld->AdvancedSearch->SearchValue2Default : $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = ($Default) ? $Fld->AdvancedSearch->SearchOperator2Default : $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1 || $FldOpr <> "LIKE" ||
			($FldOpr2 <> "LIKE" && $FldVal2 <> ""))
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal, $this->DBID) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2, $this->DBID) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2, $this->DBID);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Return basic search SQL
	function BasicSearchSQL($arKeywords, $type) {
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_surname, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_name, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_email, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_mobile, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_phone, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_is_active, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_company_name, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_company_logo_file, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_company_address, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_company_phone, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_company_email, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_company_web, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_latitude, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_longitude, $arKeywords, $type);
		$this->BuildBasicSearchSQL($sWhere, $this->apotech_company_city_id, $arKeywords, $type);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $arKeywords, $type) {
		$sDefCond = ($type == "OR") ? "OR" : "AND";
		$arSQL = array(); // Array for SQL parts
		$arCond = array(); // Array for search conditions
		$cnt = count($arKeywords);
		$j = 0; // Number of SQL parts
		for ($i = 0; $i < $cnt; $i++) {
			$Keyword = $arKeywords[$i];
			$Keyword = trim($Keyword);
			if (EW_BASIC_SEARCH_IGNORE_PATTERN <> "") {
				$Keyword = preg_replace(EW_BASIC_SEARCH_IGNORE_PATTERN, "\\", $Keyword);
				$ar = explode("\\", $Keyword);
			} else {
				$ar = array($Keyword);
			}
			foreach ($ar as $Keyword) {
				if ($Keyword <> "") {
					$sWrk = "";
					if ($Keyword == "OR" && $type == "") {
						if ($j > 0)
							$arCond[$j-1] = "OR";
					} elseif ($Keyword == EW_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NULL";
					} elseif ($Keyword == EW_NOT_NULL_VALUE) {
						$sWrk = $Fld->FldExpression . " IS NOT NULL";
					} elseif ($Fld->FldIsVirtual && $Fld->FldVirtualSearch) {
						$sWrk = $Fld->FldVirtualExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					} elseif ($Fld->FldDataType != EW_DATATYPE_NUMBER || is_numeric($Keyword)) {
						$sWrk = $Fld->FldBasicSearchExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING, $this->DBID), $this->DBID);
					}
					if ($sWrk <> "") {
						$arSQL[$j] = $sWrk;
						$arCond[$j] = $sDefCond;
						$j += 1;
					}
				}
			}
		}
		$cnt = count($arSQL);
		$bQuoted = FALSE;
		$sSql = "";
		if ($cnt > 0) {
			for ($i = 0; $i < $cnt-1; $i++) {
				if ($arCond[$i] == "OR") {
					if (!$bQuoted) $sSql .= "(";
					$bQuoted = TRUE;
				}
				$sSql .= $arSQL[$i];
				if ($bQuoted && $arCond[$i] <> "OR") {
					$sSql .= ")";
					$bQuoted = FALSE;
				}
				$sSql .= " " . $arCond[$i] . " ";
			}
			$sSql .= $arSQL[$cnt-1];
			if ($bQuoted)
				$sSql .= ")";
		}
		if ($sSql <> "") {
			if ($Where <> "") $Where .= " OR ";
			$Where .=  "(" . $sSql . ")";
		}
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere($Default = FALSE) {
		global $Security;
		$sSearchStr = "";
		if (!$Security->CanSearch()) return "";
		$sSearchKeyword = ($Default) ? $this->BasicSearch->KeywordDefault : $this->BasicSearch->Keyword;
		$sSearchType = ($Default) ? $this->BasicSearch->TypeDefault : $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				$ar = array();

				// Match quoted keywords (i.e.: "...")
				if (preg_match_all('/"([^"]*)"/i', $sSearch, $matches, PREG_SET_ORDER)) {
					foreach ($matches as $match) {
						$p = strpos($sSearch, $match[0]);
						$str = substr($sSearch, 0, $p);
						$sSearch = substr($sSearch, $p + strlen($match[0]));
						if (strlen(trim($str)) > 0)
							$ar = array_merge($ar, explode(" ", trim($str)));
						$ar[] = $match[1]; // Save quoted keyword
					}
				}

				// Match individual keywords
				if (strlen(trim($sSearch)) > 0)
					$ar = array_merge($ar, explode(" ", trim($sSearch)));

				// Search keyword in any fields
				if (($sSearchType == "OR" || $sSearchType == "AND") && $this->BasicSearch->BasicSearchAnyFields) {
					foreach ($ar as $sKeyword) {
						if ($sKeyword <> "") {
							if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
							$sSearchStr .= "(" . $this->BasicSearchSQL(array($sKeyword), $sSearchType) . ")";
						}
					}
				} else {
					$sSearchStr = $this->BasicSearchSQL($ar, $sSearchType);
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL(array($sSearch), $sSearchType);
			}
			if (!$Default) $this->Command = "search";
		}
		if (!$Default && $this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		if ($this->apotech_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_admin_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_surname->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_name->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_email->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_mobile->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_phone->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_is_active->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_last_update->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_name->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_logo_file->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_address->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_phone->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_email->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_web->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_latitude->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_longitude->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_region_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_province_id->AdvancedSearch->IssetSession())
			return TRUE;
		if ($this->apotech_company_city_id->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->apotech_id->AdvancedSearch->UnsetSession();
		$this->apotech_admin_id->AdvancedSearch->UnsetSession();
		$this->apotech_surname->AdvancedSearch->UnsetSession();
		$this->apotech_name->AdvancedSearch->UnsetSession();
		$this->apotech_email->AdvancedSearch->UnsetSession();
		$this->apotech_mobile->AdvancedSearch->UnsetSession();
		$this->apotech_phone->AdvancedSearch->UnsetSession();
		$this->apotech_is_active->AdvancedSearch->UnsetSession();
		$this->apotech_last_update->AdvancedSearch->UnsetSession();
		$this->apotech_company_name->AdvancedSearch->UnsetSession();
		$this->apotech_company_logo_file->AdvancedSearch->UnsetSession();
		$this->apotech_company_address->AdvancedSearch->UnsetSession();
		$this->apotech_company_phone->AdvancedSearch->UnsetSession();
		$this->apotech_company_email->AdvancedSearch->UnsetSession();
		$this->apotech_company_web->AdvancedSearch->UnsetSession();
		$this->apotech_latitude->AdvancedSearch->UnsetSession();
		$this->apotech_longitude->AdvancedSearch->UnsetSession();
		$this->apotech_company_region_id->AdvancedSearch->UnsetSession();
		$this->apotech_company_province_id->AdvancedSearch->UnsetSession();
		$this->apotech_company_city_id->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();

		// Restore advanced search values
		$this->apotech_id->AdvancedSearch->Load();
		$this->apotech_admin_id->AdvancedSearch->Load();
		$this->apotech_surname->AdvancedSearch->Load();
		$this->apotech_name->AdvancedSearch->Load();
		$this->apotech_email->AdvancedSearch->Load();
		$this->apotech_mobile->AdvancedSearch->Load();
		$this->apotech_phone->AdvancedSearch->Load();
		$this->apotech_is_active->AdvancedSearch->Load();
		$this->apotech_last_update->AdvancedSearch->Load();
		$this->apotech_company_name->AdvancedSearch->Load();
		$this->apotech_company_logo_file->AdvancedSearch->Load();
		$this->apotech_company_address->AdvancedSearch->Load();
		$this->apotech_company_phone->AdvancedSearch->Load();
		$this->apotech_company_email->AdvancedSearch->Load();
		$this->apotech_company_web->AdvancedSearch->Load();
		$this->apotech_latitude->AdvancedSearch->Load();
		$this->apotech_longitude->AdvancedSearch->Load();
		$this->apotech_company_region_id->AdvancedSearch->Load();
		$this->apotech_company_province_id->AdvancedSearch->Load();
		$this->apotech_company_city_id->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->apotech_id); // apotech_id
			$this->UpdateSort($this->apotech_admin_id); // apotech_admin_id
			$this->UpdateSort($this->apotech_surname); // apotech_surname
			$this->UpdateSort($this->apotech_name); // apotech_name
			$this->UpdateSort($this->apotech_company_name); // apotech_company_name
			$this->UpdateSort($this->apotech_company_phone); // apotech_company_phone
			$this->UpdateSort($this->apotech_company_email); // apotech_company_email
			$this->UpdateSort($this->apotech_latitude); // apotech_latitude
			$this->UpdateSort($this->apotech_longitude); // apotech_longitude
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->getSqlOrderBy() <> "") {
				$sOrderBy = $this->getSqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->apotech_id->setSort("");
				$this->apotech_admin_id->setSort("");
				$this->apotech_surname->setSort("");
				$this->apotech_name->setSort("");
				$this->apotech_company_name->setSort("");
				$this->apotech_company_phone->setSort("");
				$this->apotech_company_email->setSort("");
				$this->apotech_latitude->setSort("");
				$this->apotech_longitude->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanView();
		$item->OnLeft = TRUE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanEdit();
		$item->OnLeft = TRUE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanAdd();
		$item->OnLeft = TRUE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->CanDelete();
		$item->OnLeft = TRUE;

		// "detail_telecare_prescription"
		$item = &$this->ListOptions->Add("detail_telecare_prescription");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->AllowList(CurrentProjectID() . 'telecare_prescription') && !$this->ShowMultipleDetails;
		$item->OnLeft = TRUE;
		$item->ShowInButtonGroup = FALSE;
		if (!isset($GLOBALS["telecare_prescription_grid"])) $GLOBALS["telecare_prescription_grid"] = new ctelecare_prescription_grid;

		// Multiple details
		if ($this->ShowMultipleDetails) {
			$item = &$this->ListOptions->Add("details");
			$item->CssStyle = "white-space: nowrap;";
			$item->Visible = $this->ShowMultipleDetails;
			$item->OnLeft = TRUE;
			$item->ShowInButtonGroup = FALSE;
		}

		// Set up detail pages
		$pages = new cSubPages();
		$pages->Add("telecare_prescription");
		$this->DetailPages = $pages;

		// List actions
		$item = &$this->ListOptions->Add("listactions");
		$item->CssStyle = "white-space: nowrap;";
		$item->OnLeft = TRUE;
		$item->Visible = FALSE;
		$item->ShowInButtonGroup = FALSE;
		$item->ShowInDropDown = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = TRUE;
		$item->Header = "<input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\">";
		$item->MoveTo(0);
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseImageAndText = TRUE;
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = TRUE;
		if ($this->ListOptions->UseButtonGroup && ew_IsMobile())
			$this->ListOptions->UseDropDownButton = TRUE;
		$this->ListOptions->ButtonClass = "btn-sm"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$this->SetupListOptionsExt();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if ($Security->CanView())
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" title=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->CanEdit()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" title=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if ($Security->CanAdd()) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" title=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->CanDelete())
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " title=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// Set up list action buttons
		$oListOpt = &$this->ListOptions->GetItem("listactions");
		if ($oListOpt) {
			$body = "";
			$links = array();
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_SINGLE && $listaction->Allow) {
					$action = $listaction->Action;
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode(str_replace(" ewIcon", "", $listaction->Icon)) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\"></span> " : "";
					$links[] = "<li><a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . $listaction->Caption . "</a></li>";
					if (count($links) == 1) // Single button
						$body = "<a class=\"ewAction ewListAction\" data-action=\"" . ew_HtmlEncode($action) . "\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({key:" . $this->KeyToJson() . "}," . $listaction->ToJson(TRUE) . "));return false;\">" . $Language->Phrase("ListActionButton") . "</a>";
				}
			}
			if (count($links) > 1) { // More than one buttons, use dropdown
				$body = "<button class=\"dropdown-toggle btn btn-default btn-sm ewActions\" title=\"" . ew_HtmlTitle($Language->Phrase("ListActionButton")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("ListActionButton") . "<b class=\"caret\"></b></button>";
				$content = "";
				foreach ($links as $link)
					$content .= "<li>" . $link . "</li>";
				$body .= "<ul class=\"dropdown-menu" . ($oListOpt->OnLeft ? "" : " dropdown-menu-right") . "\">". $content . "</ul>";
				$body = "<div class=\"btn-group\">" . $body . "</div>";
			}
			if (count($links) > 0) {
				$oListOpt->Body = $body;
				$oListOpt->Visible = TRUE;
			}
		}
		$DetailViewTblVar = "";
		$DetailCopyTblVar = "";
		$DetailEditTblVar = "";

		// "detail_telecare_prescription"
		$oListOpt = &$this->ListOptions->Items["detail_telecare_prescription"];
		if ($Security->AllowList(CurrentProjectID() . 'telecare_prescription')) {
			$body = $Language->Phrase("DetailLink") . $Language->TablePhrase("telecare_prescription", "TblCaption");
			$body = "<a class=\"btn btn-default btn-sm ewRowLink ewDetail\" data-action=\"list\" href=\"" . ew_HtmlEncode("telecare_prescriptionlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_apotech&fk_apotech_id=" . urlencode(strval($this->apotech_id->CurrentValue)) . "") . "\">" . $body . "</a>";
			$links = "";
			if ($GLOBALS["telecare_prescription_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_prescription')) {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
				if ($DetailViewTblVar <> "") $DetailViewTblVar .= ",";
				$DetailViewTblVar .= "telecare_prescription";
			}
			if ($GLOBALS["telecare_prescription_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_prescription')) {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
				if ($DetailEditTblVar <> "") $DetailEditTblVar .= ",";
				$DetailEditTblVar .= "telecare_prescription";
			}
			if ($GLOBALS["telecare_prescription_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_prescription')) {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription")) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
				if ($DetailCopyTblVar <> "") $DetailCopyTblVar .= ",";
				$DetailCopyTblVar .= "telecare_prescription";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewDetail\" data-toggle=\"dropdown\"><b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu\">". $links . "</ul>";
			}
			$body = "<div class=\"btn-group\">" . $body . "</div>";
			$oListOpt->Body = $body;
			if ($this->ShowMultipleDetails) $oListOpt->Visible = FALSE;
		}
		if ($this->ShowMultipleDetails) {
			$body = $Language->Phrase("MultipleMasterDetails");
			$body = "<div class=\"btn-group\">";
			$links = "";
			if ($DetailViewTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailView\" data-action=\"view\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" href=\"" . ew_HtmlEncode($this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailViewTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailViewLink")) . "</a></li>";
			}
			if ($DetailEditTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailEdit\" data-action=\"edit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" href=\"" . ew_HtmlEncode($this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailEditTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailEditLink")) . "</a></li>";
			}
			if ($DetailCopyTblVar <> "") {
				$links .= "<li><a class=\"ewRowLink ewDetailCopy\" data-action=\"add\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" href=\"" . ew_HtmlEncode($this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=" . $DetailCopyTblVar)) . "\">" . ew_HtmlImageAndText($Language->Phrase("MasterDetailCopyLink")) . "</a></li>";
			}
			if ($links <> "") {
				$body .= "<button class=\"dropdown-toggle btn btn-default btn-sm ewMasterDetail\" title=\"" . ew_HtmlTitle($Language->Phrase("MultipleMasterDetails")) . "\" data-toggle=\"dropdown\">" . $Language->Phrase("MultipleMasterDetails") . "<b class=\"caret\"></b></button>";
				$body .= "<ul class=\"dropdown-menu ewMenu\">". $links . "</ul>";
			}
			$body .= "</div>";

			// Multiple details
			$oListOpt = &$this->ListOptions->Items["details"];
			$oListOpt->Body = $body;
		}

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->apotech_id->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event);'>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddLink")) . "\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "" && $Security->CanAdd());
		$option = $options["detail"];
		$DetailTableLink = "";
		$item = &$option->Add("detailadd_telecare_prescription");
		$caption = $Language->Phrase("Add") . "&nbsp;" . $this->TableCaption() . "/" . $GLOBALS["telecare_prescription"]->TableCaption();
		$item->Body = "<a class=\"ewDetailAddGroup ewDetailAdd\" title=\"" . ew_HtmlTitle($caption) . "\" data-caption=\"" . ew_HtmlTitle($caption) . "\" href=\"" . ew_HtmlEncode($this->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "\">" . $caption . "</a>";
		$item->Visible = ($GLOBALS["telecare_prescription"]->DetailAdd && $Security->AllowAdd(CurrentProjectID() . 'telecare_prescription') && $Security->CanAdd());
		if ($item->Visible) {
			if ($DetailTableLink <> "") $DetailTableLink .= ",";
			$DetailTableLink .= "telecare_prescription";
		}

		// Add multiple details
		if ($this->ShowMultipleDetails) {
			$item = &$option->Add("detailsadd");
			$item->Body = "<a class=\"ewDetailAddGroup ewDetailAdd\" title=\"" . ew_HtmlTitle($Language->Phrase("AddMasterDetailLink")) . "\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("AddMasterDetailLink")) . "\" href=\"" . ew_HtmlEncode($this->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=" . $DetailTableLink) . "\">" . $Language->Phrase("AddMasterDetailLink") . "</a>";
			$item->Visible = ($DetailTableLink <> "" && $Security->CanAdd());

			// Hide single master/detail items
			$ar = explode(",", $DetailTableLink);
			$cnt = count($ar);
			for ($i = 0; $i < $cnt; $i++) {
				if ($item = &$option->GetItem("detailadd_" . $ar[$i]))
					$item->Visible = FALSE;
			}
		}
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseImageAndText = TRUE;
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-sm"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");

		// Filter button
		$item = &$this->FilterOptions->Add("savecurrentfilter");
		$item->Body = "<a class=\"ewSaveFilter\" data-form=\"ftelecare_apotechlistsrch\" href=\"#\">" . $Language->Phrase("SaveCurrentFilter") . "</a>";
		$item->Visible = TRUE;
		$item = &$this->FilterOptions->Add("deletefilter");
		$item->Body = "<a class=\"ewDeleteFilter\" data-form=\"ftelecare_apotechlistsrch\" href=\"#\">" . $Language->Phrase("DeleteFilter") . "</a>";
		$item->Visible = TRUE;
		$this->FilterOptions->UseDropDownButton = TRUE;
		$this->FilterOptions->UseButtonGroup = !$this->FilterOptions->UseDropDownButton;
		$this->FilterOptions->DropDownButtonPhrase = $Language->Phrase("Filters");

		// Add group option item
		$item = &$this->FilterOptions->Add($this->FilterOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
			$option = &$options["action"];

			// Set up list action buttons
			foreach ($this->ListActions->Items as $listaction) {
				if ($listaction->Select == EW_ACTION_MULTIPLE) {
					$item = &$option->Add("custom_" . $listaction->Action);
					$caption = $listaction->Caption;
					$icon = ($listaction->Icon <> "") ? "<span class=\"" . ew_HtmlEncode($listaction->Icon) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\"></span> " : $caption;
					$item->Body = "<a class=\"ewAction ewListAction\" title=\"" . ew_HtmlEncode($caption) . "\" data-caption=\"" . ew_HtmlEncode($caption) . "\" href=\"\" onclick=\"ew_SubmitAction(event,jQuery.extend({f:document.ftelecare_apotechlist}," . $listaction->ToJson(TRUE) . "));return false;\">" . $icon . "</a>";
					$item->Visible = $listaction->Allow;
				}
			}

			// Hide grid edit and other options
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$option->HideAllOptions();
			}
	}

	// Process list action
	function ProcessListAction() {
		global $Language, $Security;
		$userlist = "";
		$user = "";
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {

			// Check permission first
			$ActionCaption = $UserAction;
			if (array_key_exists($UserAction, $this->ListActions->Items)) {
				$ActionCaption = $this->ListActions->Items[$UserAction]->Caption;
				if (!$this->ListActions->Items[$UserAction]->Allow) {
					$errmsg = str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionNotAllowed"));
					if (@$_POST["ajax"] == $UserAction) // Ajax
						echo "<p class=\"text-danger\">" . $errmsg . "</p>";
					else
						$this->setFailureMessage($errmsg);
					return FALSE;
				}
			}
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$this->CurrentAction = $UserAction;

			// Call row action event
			if ($rs && !$rs->EOF) {
				$conn->BeginTrans();
				$this->SelectedCount = $rs->RecordCount();
				$this->SelectedIndex = 0;
				while (!$rs->EOF) {
					$this->SelectedIndex++;
					$row = $rs->fields;
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
					$rs->MoveNext();
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $ActionCaption, $Language->Phrase("CustomActionFailed")));
					}
				}
			}
			if ($rs)
				$rs->Close();
			if (@$_POST["ajax"] == $UserAction) { // Ajax
				if ($this->getSuccessMessage() <> "") {
					echo "<p class=\"text-success\">" . $this->getSuccessMessage() . "</p>";
					$this->ClearSuccessMessage(); // Clear message
				}
				if ($this->getFailureMessage() <> "") {
					echo "<p class=\"text-danger\">" . $this->getFailureMessage() . "</p>";
					$this->ClearFailureMessage(); // Clear message
				}
				return TRUE;
			}
		}
		return FALSE; // Not ajax request
	}

	// Set up search options
	function SetupSearchOptions() {
		global $Language;
		$this->SearchOptions = new cListOptions();
		$this->SearchOptions->Tag = "div";
		$this->SearchOptions->TagClassName = "ewSearchOption";

		// Search button
		$item = &$this->SearchOptions->Add("searchtoggle");
		$SearchToggleClass = ($this->SearchWhere <> "") ? " active" : " active";
		$item->Body = "<button type=\"button\" class=\"btn btn-default ewSearchToggle" . $SearchToggleClass . "\" title=\"" . $Language->Phrase("SearchPanel") . "\" data-caption=\"" . $Language->Phrase("SearchPanel") . "\" data-toggle=\"button\" data-form=\"ftelecare_apotechlistsrch\">" . $Language->Phrase("SearchBtn") . "</button>";
		$item->Visible = TRUE;

		// Show all button
		$item = &$this->SearchOptions->Add("showall");
		$item->Body = "<a class=\"btn btn-default ewShowAll\" title=\"" . $Language->Phrase("ShowAll") . "\" data-caption=\"" . $Language->Phrase("ShowAll") . "\" href=\"" . $this->PageUrl() . "cmd=reset\">" . $Language->Phrase("ShowAllBtn") . "</a>";
		$item->Visible = ($this->SearchWhere <> $this->DefaultSearchWhere && $this->SearchWhere <> "0=101");

		// Advanced search button
		$item = &$this->SearchOptions->Add("advancedsearch");
		if (ew_IsMobile())
			$item->Body = "<a class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" href=\"telecare_apotechsrch.php\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		else
			$item->Body = "<button type=\"button\" class=\"btn btn-default ewAdvancedSearch\" title=\"" . $Language->Phrase("AdvancedSearch") . "\" data-caption=\"" . $Language->Phrase("AdvancedSearch") . "\" onclick=\"ew_SearchDialogShow({lnk:this,url:'telecare_apotechsrch.php'});\">" . $Language->Phrase("AdvancedSearchBtn") . "</a>";
		$item->Visible = TRUE;

		// Button group for search
		$this->SearchOptions->UseDropDownButton = FALSE;
		$this->SearchOptions->UseImageAndText = TRUE;
		$this->SearchOptions->UseButtonGroup = TRUE;
		$this->SearchOptions->DropDownButtonPhrase = $Language->Phrase("ButtonSearch");

		// Add group option item
		$item = &$this->SearchOptions->Add($this->SearchOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;

		// Hide search options
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->SearchOptions->HideAllOptions();
		global $Security;
		if (!$Security->CanSearch()) {
			$this->SearchOptions->HideAllOptions();
			$this->FilterOptions->HideAllOptions();
		}
	}

	function SetupListOptionsExt() {
		global $Security, $Language;

		// Hide detail items for dropdown if necessary
		$this->ListOptions->HideDetailItemsForDropDown();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
		$links = "";
		$btngrps = "";
		$sSqlWrk = "`prescription_drugstore_id`=" . ew_AdjustSql($this->apotech_id->CurrentValue, $this->DBID) . "";

		// Column "detail_telecare_prescription"
		if ($this->DetailPages->Items["telecare_prescription"]->Visible) {
			$link = "";
			$option = &$this->ListOptions->Items["detail_telecare_prescription"];
			$url = "telecare_prescriptionpreview.php?t=telecare_apotech&f=" . ew_Encrypt($sSqlWrk);
			$btngrp = "<div data-table=\"telecare_prescription\" data-url=\"" . $url . "\" class=\"btn-group\">";
			if ($Security->AllowList(CurrentProjectID() . 'telecare_prescription')) {			
				$label = $Language->TablePhrase("telecare_prescription", "TblCaption");
				$link = "<li><a href=\"#\" data-toggle=\"tab\" data-table=\"telecare_prescription\" data-url=\"" . $url . "\">" . $label . "</a></li>";			
				$links .= $link;
				$detaillnk = ew_JsEncode3("telecare_prescriptionlist.php?" . EW_TABLE_SHOW_MASTER . "=telecare_apotech&fk_apotech_id=" . urlencode(strval($this->apotech_id->CurrentValue)) . "");
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . $Language->TablePhrase("telecare_prescription", "TblCaption") . "\" onclick=\"window.location='" . $detaillnk . "'\">" . $Language->Phrase("MasterDetailListLink") . "</button>";
			}
			if ($GLOBALS["telecare_prescription_grid"]->DetailView && $Security->CanView() && $Security->AllowView(CurrentProjectID() . 'telecare_prescription'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailViewLink")) . "\" onclick=\"window.location='" . $this->GetViewUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "'\">" . $Language->Phrase("MasterDetailViewLink") . "</button>";
			if ($GLOBALS["telecare_prescription_grid"]->DetailEdit && $Security->CanEdit() && $Security->AllowEdit(CurrentProjectID() . 'telecare_prescription'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailEditLink")) . "\" onclick=\"window.location='" . $this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "'\">" . $Language->Phrase("MasterDetailEditLink") . "</button>";
			if ($GLOBALS["telecare_prescription_grid"]->DetailAdd && $Security->CanAdd() && $Security->AllowAdd(CurrentProjectID() . 'telecare_prescription'))
				$btngrp .= "<button type=\"button\" class=\"btn btn-default btn-sm\" title=\"" . ew_HtmlTitle($Language->Phrase("MasterDetailCopyLink")) . "\" onclick=\"window.location='" . $this->GetCopyUrl(EW_TABLE_SHOW_DETAIL . "=telecare_prescription") . "'\">" . $Language->Phrase("MasterDetailCopyLink") . "</button>";
			$btngrp .= "</div>";
			if ($link <> "") {
				$btngrps .= $btngrp;
				$option->Body .= "<div class=\"hide ewPreview\">" . $link . $btngrp . "</div>";
			}
		}

		// Hide detail items if necessary
		$this->ListOptions->HideDetailItemsForDropDown();

		// Column "preview"
		$option = &$this->ListOptions->GetItem("preview");
		if (!$option) { // Add preview column
			$option = &$this->ListOptions->Add("preview");
			$option->OnLeft = TRUE;
			if ($option->OnLeft) {
				$option->MoveTo($this->ListOptions->ItemPos("checkbox") + 1);
			} else {
				$option->MoveTo($this->ListOptions->ItemPos("checkbox"));
			}
			$option->Visible = !($this->Export <> "" || $this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit");
			$option->ShowInDropDown = FALSE;
			$option->ShowInButtonGroup = FALSE;
		}
		if ($option) {
			$option->Body = "<span class=\"ewPreviewRowBtn icon-expand\"></span>";
			$option->Body .= "<div class=\"hide ewPreview\">" . $links . $btngrps . "</div>";
			if ($option->Visible) $option->Visible = $link <> "";
		}

		// Column "details" (Multiple details)
		$option = &$this->ListOptions->GetItem("details");
		if ($option) {
			$option->Body .= "<div class=\"hide ewPreview\">" . $links . $btngrps . "</div>";
			if ($option->Visible) $option->Visible = $links <> "";
		}
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// apotech_id

		$this->apotech_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_id"]);
		if ($this->apotech_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_id->AdvancedSearch->SearchOperator = @$_GET["z_apotech_id"];

		// apotech_admin_id
		$this->apotech_admin_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_admin_id"]);
		if ($this->apotech_admin_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_admin_id->AdvancedSearch->SearchOperator = @$_GET["z_apotech_admin_id"];

		// apotech_surname
		$this->apotech_surname->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_surname"]);
		if ($this->apotech_surname->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_surname->AdvancedSearch->SearchOperator = @$_GET["z_apotech_surname"];

		// apotech_name
		$this->apotech_name->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_name"]);
		if ($this->apotech_name->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_name->AdvancedSearch->SearchOperator = @$_GET["z_apotech_name"];

		// apotech_email
		$this->apotech_email->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_email"]);
		if ($this->apotech_email->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_email->AdvancedSearch->SearchOperator = @$_GET["z_apotech_email"];

		// apotech_mobile
		$this->apotech_mobile->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_mobile"]);
		if ($this->apotech_mobile->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_mobile->AdvancedSearch->SearchOperator = @$_GET["z_apotech_mobile"];

		// apotech_phone
		$this->apotech_phone->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_phone"]);
		if ($this->apotech_phone->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_phone->AdvancedSearch->SearchOperator = @$_GET["z_apotech_phone"];

		// apotech_is_active
		$this->apotech_is_active->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_is_active"]);
		if ($this->apotech_is_active->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_is_active->AdvancedSearch->SearchOperator = @$_GET["z_apotech_is_active"];

		// apotech_last_update
		$this->apotech_last_update->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_last_update"]);
		if ($this->apotech_last_update->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_last_update->AdvancedSearch->SearchOperator = @$_GET["z_apotech_last_update"];

		// apotech_company_name
		$this->apotech_company_name->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_name"]);
		if ($this->apotech_company_name->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_name->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_name"];

		// apotech_company_logo_file
		$this->apotech_company_logo_file->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_logo_file"]);
		if ($this->apotech_company_logo_file->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_logo_file->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_logo_file"];

		// apotech_company_address
		$this->apotech_company_address->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_address"]);
		if ($this->apotech_company_address->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_address->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_address"];

		// apotech_company_phone
		$this->apotech_company_phone->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_phone"]);
		if ($this->apotech_company_phone->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_phone->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_phone"];

		// apotech_company_email
		$this->apotech_company_email->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_email"]);
		if ($this->apotech_company_email->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_email->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_email"];

		// apotech_company_web
		$this->apotech_company_web->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_web"]);
		if ($this->apotech_company_web->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_web->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_web"];

		// apotech_latitude
		$this->apotech_latitude->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_latitude"]);
		if ($this->apotech_latitude->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_latitude->AdvancedSearch->SearchOperator = @$_GET["z_apotech_latitude"];

		// apotech_longitude
		$this->apotech_longitude->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_longitude"]);
		if ($this->apotech_longitude->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_longitude->AdvancedSearch->SearchOperator = @$_GET["z_apotech_longitude"];

		// apotech_company_region_id
		$this->apotech_company_region_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_region_id"]);
		if ($this->apotech_company_region_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_region_id->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_region_id"];

		// apotech_company_province_id
		$this->apotech_company_province_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_province_id"]);
		if ($this->apotech_company_province_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_province_id->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_province_id"];

		// apotech_company_city_id
		$this->apotech_company_city_id->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_apotech_company_city_id"]);
		if ($this->apotech_company_city_id->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->apotech_company_city_id->AdvancedSearch->SearchOperator = @$_GET["z_apotech_company_city_id"];
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {

		// Load List page SQL
		$sSql = $this->SelectSQL();
		$conn = &$this->Connection();

		// Load recordset
		$dbtype = ew_GetConnectionType($this->DBID);
		if ($this->UseSelectLimit) {
			$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
			if ($dbtype == "MSSQL") {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset, array("_hasOrderBy" => trim($this->getOrderBy()) || trim($this->getSessionOrderBy())));
			} else {
				$rs = $conn->SelectLimit($sSql, $rowcnt, $offset);
			}
			$conn->raiseErrorFn = '';
		} else {
			$rs = ew_LoadRecordset($sSql, $conn);
		}

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->apotech_id->setDbValue($rs->fields('apotech_id'));
		$this->apotech_admin_id->setDbValue($rs->fields('apotech_admin_id'));
		$this->apotech_surname->setDbValue($rs->fields('apotech_surname'));
		$this->apotech_name->setDbValue($rs->fields('apotech_name'));
		$this->apotech_email->setDbValue($rs->fields('apotech_email'));
		$this->apotech_mobile->setDbValue($rs->fields('apotech_mobile'));
		$this->apotech_phone->setDbValue($rs->fields('apotech_phone'));
		$this->apotech_is_active->setDbValue($rs->fields('apotech_is_active'));
		$this->apotech_last_update->setDbValue($rs->fields('apotech_last_update'));
		$this->apotech_company_name->setDbValue($rs->fields('apotech_company_name'));
		$this->apotech_company_logo_file->Upload->DbValue = $rs->fields('apotech_company_logo_file');
		$this->apotech_company_logo_file->CurrentValue = $this->apotech_company_logo_file->Upload->DbValue;
		$this->apotech_company_logo_width->setDbValue($rs->fields('apotech_company_logo_width'));
		$this->apotech_company_logo_height->setDbValue($rs->fields('apotech_company_logo_height'));
		$this->apotech_company_logo_size->setDbValue($rs->fields('apotech_company_logo_size'));
		$this->apotech_company_address->setDbValue($rs->fields('apotech_company_address'));
		$this->apotech_company_phone->setDbValue($rs->fields('apotech_company_phone'));
		$this->apotech_company_email->setDbValue($rs->fields('apotech_company_email'));
		$this->apotech_company_web->setDbValue($rs->fields('apotech_company_web'));
		$this->apotech_latitude->setDbValue($rs->fields('apotech_latitude'));
		$this->apotech_longitude->setDbValue($rs->fields('apotech_longitude'));
		$this->apotech_company_region_id->setDbValue($rs->fields('apotech_company_region_id'));
		$this->apotech_company_province_id->setDbValue($rs->fields('apotech_company_province_id'));
		$this->apotech_company_city_id->setDbValue($rs->fields('apotech_company_city_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->apotech_id->DbValue = $row['apotech_id'];
		$this->apotech_admin_id->DbValue = $row['apotech_admin_id'];
		$this->apotech_surname->DbValue = $row['apotech_surname'];
		$this->apotech_name->DbValue = $row['apotech_name'];
		$this->apotech_email->DbValue = $row['apotech_email'];
		$this->apotech_mobile->DbValue = $row['apotech_mobile'];
		$this->apotech_phone->DbValue = $row['apotech_phone'];
		$this->apotech_is_active->DbValue = $row['apotech_is_active'];
		$this->apotech_last_update->DbValue = $row['apotech_last_update'];
		$this->apotech_company_name->DbValue = $row['apotech_company_name'];
		$this->apotech_company_logo_file->Upload->DbValue = $row['apotech_company_logo_file'];
		$this->apotech_company_logo_width->DbValue = $row['apotech_company_logo_width'];
		$this->apotech_company_logo_height->DbValue = $row['apotech_company_logo_height'];
		$this->apotech_company_logo_size->DbValue = $row['apotech_company_logo_size'];
		$this->apotech_company_address->DbValue = $row['apotech_company_address'];
		$this->apotech_company_phone->DbValue = $row['apotech_company_phone'];
		$this->apotech_company_email->DbValue = $row['apotech_company_email'];
		$this->apotech_company_web->DbValue = $row['apotech_company_web'];
		$this->apotech_latitude->DbValue = $row['apotech_latitude'];
		$this->apotech_longitude->DbValue = $row['apotech_longitude'];
		$this->apotech_company_region_id->DbValue = $row['apotech_company_region_id'];
		$this->apotech_company_province_id->DbValue = $row['apotech_company_province_id'];
		$this->apotech_company_city_id->DbValue = $row['apotech_company_city_id'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("apotech_id")) <> "")
			$this->apotech_id->CurrentValue = $this->getKey("apotech_id"); // apotech_id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$conn = &$this->Connection();
			$this->OldRecordset = ew_LoadRecordset($sSql, $conn);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// apotech_id
		// apotech_admin_id
		// apotech_surname
		// apotech_name
		// apotech_email
		// apotech_mobile
		// apotech_phone
		// apotech_is_active
		// apotech_last_update
		// apotech_company_name
		// apotech_company_logo_file
		// apotech_company_logo_width
		// apotech_company_logo_height
		// apotech_company_logo_size
		// apotech_company_address
		// apotech_company_phone
		// apotech_company_email
		// apotech_company_web
		// apotech_latitude
		// apotech_longitude
		// apotech_company_region_id
		// apotech_company_province_id
		// apotech_company_city_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// apotech_id
		$this->apotech_id->ViewValue = $this->apotech_id->CurrentValue;
		$this->apotech_id->ViewCustomAttributes = "";

		// apotech_admin_id
		if (strval($this->apotech_admin_id->CurrentValue) <> "") {
			$sFilterWrk = "`admin_id`" . ew_SearchString("=", $this->apotech_admin_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `admin_id`, `admin_surname` AS `DispFld`, `admin_name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_admin`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_admin_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$arwrk[2] = $rswrk->fields('Disp2Fld');
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_admin_id->ViewValue = $this->apotech_admin_id->CurrentValue;
			}
		} else {
			$this->apotech_admin_id->ViewValue = NULL;
		}
		$this->apotech_admin_id->ViewCustomAttributes = "";

		// apotech_surname
		$this->apotech_surname->ViewValue = $this->apotech_surname->CurrentValue;
		$this->apotech_surname->ViewCustomAttributes = "";

		// apotech_name
		$this->apotech_name->ViewValue = $this->apotech_name->CurrentValue;
		$this->apotech_name->ViewCustomAttributes = "";

		// apotech_email
		$this->apotech_email->ViewValue = $this->apotech_email->CurrentValue;
		$this->apotech_email->ViewCustomAttributes = "";

		// apotech_mobile
		$this->apotech_mobile->ViewValue = $this->apotech_mobile->CurrentValue;
		$this->apotech_mobile->ViewCustomAttributes = "";

		// apotech_phone
		$this->apotech_phone->ViewValue = $this->apotech_phone->CurrentValue;
		$this->apotech_phone->ViewCustomAttributes = "";

		// apotech_is_active
		if (strval($this->apotech_is_active->CurrentValue) <> "") {
			$this->apotech_is_active->ViewValue = $this->apotech_is_active->OptionCaption($this->apotech_is_active->CurrentValue);
		} else {
			$this->apotech_is_active->ViewValue = NULL;
		}
		$this->apotech_is_active->ViewCustomAttributes = "";

		// apotech_last_update
		$this->apotech_last_update->ViewValue = $this->apotech_last_update->CurrentValue;
		$this->apotech_last_update->ViewValue = ew_FormatDateTime($this->apotech_last_update->ViewValue, 7);
		$this->apotech_last_update->ViewCustomAttributes = "";

		// apotech_company_name
		$this->apotech_company_name->ViewValue = $this->apotech_company_name->CurrentValue;
		$this->apotech_company_name->ViewCustomAttributes = "";

		// apotech_company_logo_file
		if (!ew_Empty($this->apotech_company_logo_file->Upload->DbValue)) {
			$this->apotech_company_logo_file->ImageWidth = 100;
			$this->apotech_company_logo_file->ImageHeight = 0;
			$this->apotech_company_logo_file->ImageAlt = $this->apotech_company_logo_file->FldAlt();
			$this->apotech_company_logo_file->ViewValue = $this->apotech_company_logo_file->Upload->DbValue;
		} else {
			$this->apotech_company_logo_file->ViewValue = "";
		}
		$this->apotech_company_logo_file->ViewCustomAttributes = "";

		// apotech_company_logo_width
		$this->apotech_company_logo_width->ViewValue = $this->apotech_company_logo_width->CurrentValue;
		$this->apotech_company_logo_width->ViewCustomAttributes = "";

		// apotech_company_logo_height
		$this->apotech_company_logo_height->ViewValue = $this->apotech_company_logo_height->CurrentValue;
		$this->apotech_company_logo_height->ViewCustomAttributes = "";

		// apotech_company_logo_size
		$this->apotech_company_logo_size->ViewValue = $this->apotech_company_logo_size->CurrentValue;
		$this->apotech_company_logo_size->ViewCustomAttributes = "";

		// apotech_company_address
		$this->apotech_company_address->ViewValue = $this->apotech_company_address->CurrentValue;
		$this->apotech_company_address->ViewCustomAttributes = "";

		// apotech_company_phone
		$this->apotech_company_phone->ViewValue = $this->apotech_company_phone->CurrentValue;
		$this->apotech_company_phone->ViewCustomAttributes = "";

		// apotech_company_email
		$this->apotech_company_email->ViewValue = $this->apotech_company_email->CurrentValue;
		$this->apotech_company_email->ViewCustomAttributes = "";

		// apotech_company_web
		$this->apotech_company_web->ViewValue = $this->apotech_company_web->CurrentValue;
		$this->apotech_company_web->ViewCustomAttributes = "";

		// apotech_latitude
		$this->apotech_latitude->ViewValue = $this->apotech_latitude->CurrentValue;
		$this->apotech_latitude->ViewCustomAttributes = "";

		// apotech_longitude
		$this->apotech_longitude->ViewValue = $this->apotech_longitude->CurrentValue;
		$this->apotech_longitude->ViewCustomAttributes = "";

		// apotech_company_region_id
		if (strval($this->apotech_company_region_id->CurrentValue) <> "") {
			$sFilterWrk = "`region_id`" . ew_SearchString("=", $this->apotech_company_region_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `region_id`, `region_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_region`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_region_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_region_id->ViewValue = $this->apotech_company_region_id->CurrentValue;
			}
		} else {
			$this->apotech_company_region_id->ViewValue = NULL;
		}
		$this->apotech_company_region_id->ViewCustomAttributes = "";

		// apotech_company_province_id
		if (strval($this->apotech_company_province_id->CurrentValue) <> "") {
			$sFilterWrk = "`province_id`" . ew_SearchString("=", $this->apotech_company_province_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `province_id`, `province_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_province`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_province_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_province_id->ViewValue = $this->apotech_company_province_id->CurrentValue;
			}
		} else {
			$this->apotech_company_province_id->ViewValue = NULL;
		}
		$this->apotech_company_province_id->ViewCustomAttributes = "";

		// apotech_company_city_id
		if (strval($this->apotech_company_city_id->CurrentValue) <> "") {
			$sFilterWrk = "`city_id`" . ew_SearchString("=", $this->apotech_company_city_id->CurrentValue, EW_DATATYPE_NUMBER, "");
		switch (@$gsLanguage) {
			case "it":
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
			default:
				$sSqlWrk = "SELECT `city_id`, `city_name` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `telecare_city`";
				$sWhereWrk = "";
				break;
		}
		ew_AddFilter($sWhereWrk, $sFilterWrk);
		$this->Lookup_Selecting($this->apotech_company_city_id, $sWhereWrk); // Call Lookup selecting
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = Conn()->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$arwrk = array();
				$arwrk[1] = $rswrk->fields('DispFld');
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->DisplayValue($arwrk);
				$rswrk->Close();
			} else {
				$this->apotech_company_city_id->ViewValue = $this->apotech_company_city_id->CurrentValue;
			}
		} else {
			$this->apotech_company_city_id->ViewValue = NULL;
		}
		$this->apotech_company_city_id->ViewCustomAttributes = "";

			// apotech_id
			$this->apotech_id->LinkCustomAttributes = "";
			$this->apotech_id->HrefValue = "";
			$this->apotech_id->TooltipValue = "";

			// apotech_admin_id
			$this->apotech_admin_id->LinkCustomAttributes = "";
			$this->apotech_admin_id->HrefValue = "";
			$this->apotech_admin_id->TooltipValue = "";

			// apotech_surname
			$this->apotech_surname->LinkCustomAttributes = "";
			$this->apotech_surname->HrefValue = "";
			$this->apotech_surname->TooltipValue = "";

			// apotech_name
			$this->apotech_name->LinkCustomAttributes = "";
			$this->apotech_name->HrefValue = "";
			$this->apotech_name->TooltipValue = "";

			// apotech_company_name
			$this->apotech_company_name->LinkCustomAttributes = "";
			$this->apotech_company_name->HrefValue = "";
			$this->apotech_company_name->TooltipValue = "";

			// apotech_company_phone
			$this->apotech_company_phone->LinkCustomAttributes = "";
			$this->apotech_company_phone->HrefValue = "";
			$this->apotech_company_phone->TooltipValue = "";

			// apotech_company_email
			$this->apotech_company_email->LinkCustomAttributes = "";
			$this->apotech_company_email->HrefValue = "";
			$this->apotech_company_email->TooltipValue = "";

			// apotech_latitude
			$this->apotech_latitude->LinkCustomAttributes = "";
			$this->apotech_latitude->HrefValue = "";
			$this->apotech_latitude->TooltipValue = "";

			// apotech_longitude
			$this->apotech_longitude->LinkCustomAttributes = "";
			$this->apotech_longitude->HrefValue = "";
			$this->apotech_longitude->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->apotech_id->AdvancedSearch->Load();
		$this->apotech_admin_id->AdvancedSearch->Load();
		$this->apotech_surname->AdvancedSearch->Load();
		$this->apotech_name->AdvancedSearch->Load();
		$this->apotech_email->AdvancedSearch->Load();
		$this->apotech_mobile->AdvancedSearch->Load();
		$this->apotech_phone->AdvancedSearch->Load();
		$this->apotech_is_active->AdvancedSearch->Load();
		$this->apotech_last_update->AdvancedSearch->Load();
		$this->apotech_company_name->AdvancedSearch->Load();
		$this->apotech_company_logo_file->AdvancedSearch->Load();
		$this->apotech_company_address->AdvancedSearch->Load();
		$this->apotech_company_phone->AdvancedSearch->Load();
		$this->apotech_company_email->AdvancedSearch->Load();
		$this->apotech_company_web->AdvancedSearch->Load();
		$this->apotech_latitude->AdvancedSearch->Load();
		$this->apotech_longitude->AdvancedSearch->Load();
		$this->apotech_company_region_id->AdvancedSearch->Load();
		$this->apotech_company_province_id->AdvancedSearch->Load();
		$this->apotech_company_city_id->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\" class=\"ewExportLink ewPrint\" title=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("PrinterFriendlyText")) . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\" class=\"ewExportLink ewExcel\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToExcelText")) . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = TRUE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\" class=\"ewExportLink ewWord\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToWordText")) . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\" class=\"ewExportLink ewHtml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToHtmlText")) . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\" class=\"ewExportLink ewXml\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToXmlText")) . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\" class=\"ewExportLink ewCsv\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToCsvText")) . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\" class=\"ewExportLink ewPdf\" title=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\" data-caption=\"" . ew_HtmlEncode($Language->Phrase("ExportToPDFText")) . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$url = "";
		$item->Body = "<button id=\"emf_telecare_apotech\" class=\"ewExportLink ewEmail\" title=\"" . $Language->Phrase("ExportToEmailText") . "\" data-caption=\"" . $Language->Phrase("ExportToEmailText") . "\" onclick=\"ew_EmailDialogShow({lnk:'emf_telecare_apotech',hdr:ewLanguage.Phrase('ExportToEmailText'),f:document.ftelecare_apotechlist,sel:false" . $url . "});\">" . $Language->Phrase("ExportToEmail") . "</button>";
		$item->Visible = FALSE;

		// Drop down button for export
		$this->ExportOptions->UseButtonGroup = TRUE;
		$this->ExportOptions->UseImageAndText = TRUE;
		$this->ExportOptions->UseDropDownButton = FALSE;
		if ($this->ExportOptions->UseButtonGroup && ew_IsMobile())
			$this->ExportOptions->UseDropDownButton = TRUE;
		$this->ExportOptions->DropDownButtonPhrase = $Language->Phrase("ButtonExport");

		// Add group option item
		$item = &$this->ExportOptions->Add($this->ExportOptions->GroupOptionName);
		$item->Body = "";
		$item->Visible = FALSE;
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = $this->UseSelectLimit;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if (!$this->Recordset)
				$this->Recordset = $this->LoadRecordset();
			$rs = &$this->Recordset;
			if ($rs)
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$this->ExportDoc = ew_ExportDocument($this, "h");
		$Doc = &$this->ExportDoc;
		if ($bSelectLimit) {
			$this->StartRec = 1;
			$this->StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {

			//$this->StartRec = $this->StartRec;
			//$this->StopRec = $this->StopRec;

		}

		// Call Page Exporting server event
		$this->ExportDoc->ExportCustom = !$this->Page_Exporting();
		$ParentTable = "";
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$Doc->Text .= $sHeader;
		$this->ExportDocument($Doc, $rs, $this->StartRec, $this->StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$Doc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Call Page Exported server event
		$this->Page_Exported();

		// Export header and footer
		$Doc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$Doc->Export();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", $this->TableVar, $url, "", $this->TableVar, TRUE);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}

	// Page Exporting event
	// $this->ExportDoc = export document object
	function Page_Exporting() {

		//$this->ExportDoc->Text = "my header"; // Export header
		//return FALSE; // Return FALSE to skip default export and use Row_Export event

		return TRUE; // Return TRUE to use default export and skip Row_Export event
	}

	// Row Export event
	// $this->ExportDoc = export document object
	function Row_Export($rs) {

	    //$this->ExportDoc->Text .= "my content"; // Build HTML with field value: $rs["MyField"] or $this->MyField->ViewValue
	}

	// Page Exported event
	// $this->ExportDoc = export document object
	function Page_Exported() {

		//$this->ExportDoc->Text .= "my footer"; // Export footer
		//echo $this->ExportDoc->Text;

	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_apotech_list)) $telecare_apotech_list = new ctelecare_apotech_list();

// Page init
$telecare_apotech_list->Page_Init();

// Page main
$telecare_apotech_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_apotech_list->Page_Render();
?>
<?php include_once "header.php" ?>
<?php if ($telecare_apotech->Export == "") { ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "list";
var CurrentForm = ftelecare_apotechlist = new ew_Form("ftelecare_apotechlist", "list");
ftelecare_apotechlist.FormKeyCountName = '<?php echo $telecare_apotech_list->FormKeyCountName ?>';

// Form_CustomValidate event
ftelecare_apotechlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_apotechlist.ValidateRequired = true;
<?php } else { ?>
ftelecare_apotechlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_apotechlist.Lists["x_apotech_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};

// Form object for search
var CurrentSearchForm = ftelecare_apotechlistsrch = new ew_Form("ftelecare_apotechlistsrch");
</script>
<style type="text/css">
.ewTablePreviewRow { /* main table preview row color */
	background-color: #FFFFFF; /* preview row color */
}
.ewTablePreviewRow .ewGrid {
	display: table;
}
.ewTablePreviewRow .ewGrid .ewTable {
	width: auto;
}
</style>
<div id="ewPreview" class="hide"><ul class="nav nav-tabs"></ul><div class="tab-content"><div class="tab-pane fade"></div></div></div>
<script type="text/javascript" src="phpjs/ewpreview.min.js"></script>
<script type="text/javascript">
var EW_PREVIEW_PLACEMENT = EW_CSS_FLIP ? "left" : "right";
var EW_PREVIEW_SINGLE_ROW = false;
var EW_PREVIEW_OVERLAY = false;
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if ($telecare_apotech->Export == "") { ?>
<div class="ewToolbar">
<?php if ($telecare_apotech->Export == "") { ?>
<?php $Breadcrumb->Render(); ?>
<?php } ?>
<?php if ($telecare_apotech_list->TotalRecs > 0 && $telecare_apotech_list->ExportOptions->Visible()) { ?>
<?php $telecare_apotech_list->ExportOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_apotech_list->SearchOptions->Visible()) { ?>
<?php $telecare_apotech_list->SearchOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_apotech_list->FilterOptions->Visible()) { ?>
<?php $telecare_apotech_list->FilterOptions->Render("body") ?>
<?php } ?>
<?php if ($telecare_apotech->Export == "") { ?>
<?php echo $Language->SelectionForm(); ?>
<?php } ?>
<div class="clearfix"></div>
</div>
<?php } ?>
<?php
	$bSelectLimit = $telecare_apotech_list->UseSelectLimit;
	if ($bSelectLimit) {
		if ($telecare_apotech_list->TotalRecs <= 0)
			$telecare_apotech_list->TotalRecs = $telecare_apotech->SelectRecordCount();
	} else {
		if (!$telecare_apotech_list->Recordset && ($telecare_apotech_list->Recordset = $telecare_apotech_list->LoadRecordset()))
			$telecare_apotech_list->TotalRecs = $telecare_apotech_list->Recordset->RecordCount();
	}
	$telecare_apotech_list->StartRec = 1;
	if ($telecare_apotech_list->DisplayRecs <= 0 || ($telecare_apotech->Export <> "" && $telecare_apotech->ExportAll)) // Display all records
		$telecare_apotech_list->DisplayRecs = $telecare_apotech_list->TotalRecs;
	if (!($telecare_apotech->Export <> "" && $telecare_apotech->ExportAll))
		$telecare_apotech_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$telecare_apotech_list->Recordset = $telecare_apotech_list->LoadRecordset($telecare_apotech_list->StartRec-1, $telecare_apotech_list->DisplayRecs);

	// Set no record found message
	if ($telecare_apotech->CurrentAction == "" && $telecare_apotech_list->TotalRecs == 0) {
		if (!$Security->CanList())
			$telecare_apotech_list->setWarningMessage($Language->Phrase("NoPermission"));
		if ($telecare_apotech_list->SearchWhere == "0=101")
			$telecare_apotech_list->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$telecare_apotech_list->setWarningMessage($Language->Phrase("NoRecord"));
	}
$telecare_apotech_list->RenderOtherOptions();
?>
<?php if ($Security->CanSearch()) { ?>
<?php if ($telecare_apotech->Export == "" && $telecare_apotech->CurrentAction == "") { ?>
<form name="ftelecare_apotechlistsrch" id="ftelecare_apotechlistsrch" class="form-inline ewForm" action="<?php echo ew_CurrentPage() ?>">
<?php $SearchPanelClass = ($telecare_apotech_list->SearchWhere <> "") ? " in" : " in"; ?>
<div id="ftelecare_apotechlistsrch_SearchPanel" class="ewSearchPanel collapse<?php echo $SearchPanelClass ?>">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="telecare_apotech">
	<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="ewQuickSearch input-group">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="form-control" value="<?php echo ew_HtmlEncode($telecare_apotech_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo ew_HtmlEncode($Language->Phrase("Search")) ?>">
	<input type="hidden" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" id="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="<?php echo ew_HtmlEncode($telecare_apotech_list->BasicSearch->getType()) ?>">
	<div class="input-group-btn">
		<button type="button" data-toggle="dropdown" class="btn btn-default"><span id="searchtype"><?php echo $telecare_apotech_list->BasicSearch->getTypeNameShort() ?></span><span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li<?php if ($telecare_apotech_list->BasicSearch->getType() == "") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this)"><?php echo $Language->Phrase("QuickSearchAuto") ?></a></li>
			<li<?php if ($telecare_apotech_list->BasicSearch->getType() == "=") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'=')"><?php echo $Language->Phrase("QuickSearchExact") ?></a></li>
			<li<?php if ($telecare_apotech_list->BasicSearch->getType() == "AND") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'AND')"><?php echo $Language->Phrase("QuickSearchAll") ?></a></li>
			<li<?php if ($telecare_apotech_list->BasicSearch->getType() == "OR") echo " class=\"active\""; ?>><a href="javascript:void(0);" onclick="ew_SetSearchType(this,'OR')"><?php echo $Language->Phrase("QuickSearchAny") ?></a></li>
		</ul>
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
</div>
	</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $telecare_apotech_list->ShowPageHeader(); ?>
<?php
$telecare_apotech_list->ShowMessage();
?>
<?php if ($telecare_apotech_list->TotalRecs > 0 || $telecare_apotech->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<?php if ($telecare_apotech->Export == "") { ?>
<div class="panel-heading ewGridUpperPanel">
<?php if ($telecare_apotech->CurrentAction <> "gridadd" && $telecare_apotech->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="form-inline ewForm ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_apotech_list->Pager)) $telecare_apotech_list->Pager = new cPrevNextPager($telecare_apotech_list->StartRec, $telecare_apotech_list->DisplayRecs, $telecare_apotech_list->TotalRecs) ?>
<?php if ($telecare_apotech_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_apotech_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_apotech_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_apotech_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_apotech_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_apotech_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_apotech_list->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
<form name="ftelecare_apotechlist" id="ftelecare_apotechlist" class="form-inline ewForm ewListForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_apotech_list->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_apotech_list->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_apotech">
<div id="gmp_telecare_apotech" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<?php if ($telecare_apotech_list->TotalRecs > 0) { ?>
<table id="tbl_telecare_apotechlist" class="table ewTable">
<?php echo $telecare_apotech->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$telecare_apotech_list->RowType = EW_ROWTYPE_HEADER;

// Render list options
$telecare_apotech_list->RenderListOptions();

// Render list options (header, left)
$telecare_apotech_list->ListOptions->Render("header", "left");
?>
<?php if ($telecare_apotech->apotech_id->Visible) { // apotech_id ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_id) == "") { ?>
		<th data-name="apotech_id"><div id="elh_telecare_apotech_apotech_id" class="telecare_apotech_apotech_id"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_id) ?>',1);"><div id="elh_telecare_apotech_apotech_id" class="telecare_apotech_apotech_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_admin_id->Visible) { // apotech_admin_id ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_admin_id) == "") { ?>
		<th data-name="apotech_admin_id"><div id="elh_telecare_apotech_apotech_admin_id" class="telecare_apotech_apotech_admin_id"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_admin_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_admin_id"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_admin_id) ?>',1);"><div id="elh_telecare_apotech_apotech_admin_id" class="telecare_apotech_apotech_admin_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_admin_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_admin_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_admin_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_surname->Visible) { // apotech_surname ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_surname) == "") { ?>
		<th data-name="apotech_surname"><div id="elh_telecare_apotech_apotech_surname" class="telecare_apotech_apotech_surname"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_surname->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_surname"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_surname) ?>',1);"><div id="elh_telecare_apotech_apotech_surname" class="telecare_apotech_apotech_surname">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_surname->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_surname->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_surname->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_name->Visible) { // apotech_name ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_name) == "") { ?>
		<th data-name="apotech_name"><div id="elh_telecare_apotech_apotech_name" class="telecare_apotech_apotech_name"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_name"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_name) ?>',1);"><div id="elh_telecare_apotech_apotech_name" class="telecare_apotech_apotech_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_name->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_company_name->Visible) { // apotech_company_name ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_company_name) == "") { ?>
		<th data-name="apotech_company_name"><div id="elh_telecare_apotech_apotech_company_name" class="telecare_apotech_apotech_company_name"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_company_name->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_company_name"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_company_name) ?>',1);"><div id="elh_telecare_apotech_apotech_company_name" class="telecare_apotech_apotech_company_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_company_name->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_company_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_company_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_company_phone->Visible) { // apotech_company_phone ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_company_phone) == "") { ?>
		<th data-name="apotech_company_phone"><div id="elh_telecare_apotech_apotech_company_phone" class="telecare_apotech_apotech_company_phone"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_company_phone->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_company_phone"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_company_phone) ?>',1);"><div id="elh_telecare_apotech_apotech_company_phone" class="telecare_apotech_apotech_company_phone">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_company_phone->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_company_phone->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_company_phone->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_company_email->Visible) { // apotech_company_email ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_company_email) == "") { ?>
		<th data-name="apotech_company_email"><div id="elh_telecare_apotech_apotech_company_email" class="telecare_apotech_apotech_company_email"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_company_email->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_company_email"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_company_email) ?>',1);"><div id="elh_telecare_apotech_apotech_company_email" class="telecare_apotech_apotech_company_email">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_company_email->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_company_email->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_company_email->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_latitude->Visible) { // apotech_latitude ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_latitude) == "") { ?>
		<th data-name="apotech_latitude"><div id="elh_telecare_apotech_apotech_latitude" class="telecare_apotech_apotech_latitude"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_latitude->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_latitude"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_latitude) ?>',1);"><div id="elh_telecare_apotech_apotech_latitude" class="telecare_apotech_apotech_latitude">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_latitude->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_latitude->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_latitude->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_apotech->apotech_longitude->Visible) { // apotech_longitude ?>
	<?php if ($telecare_apotech->SortUrl($telecare_apotech->apotech_longitude) == "") { ?>
		<th data-name="apotech_longitude"><div id="elh_telecare_apotech_apotech_longitude" class="telecare_apotech_apotech_longitude"><div class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_longitude->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="apotech_longitude"><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $telecare_apotech->SortUrl($telecare_apotech->apotech_longitude) ?>',1);"><div id="elh_telecare_apotech_apotech_longitude" class="telecare_apotech_apotech_longitude">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_apotech->apotech_longitude->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($telecare_apotech->apotech_longitude->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_apotech->apotech_longitude->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$telecare_apotech_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($telecare_apotech->ExportAll && $telecare_apotech->Export <> "") {
	$telecare_apotech_list->StopRec = $telecare_apotech_list->TotalRecs;
} else {

	// Set the last record to display
	if ($telecare_apotech_list->TotalRecs > $telecare_apotech_list->StartRec + $telecare_apotech_list->DisplayRecs - 1)
		$telecare_apotech_list->StopRec = $telecare_apotech_list->StartRec + $telecare_apotech_list->DisplayRecs - 1;
	else
		$telecare_apotech_list->StopRec = $telecare_apotech_list->TotalRecs;
}
$telecare_apotech_list->RecCnt = $telecare_apotech_list->StartRec - 1;
if ($telecare_apotech_list->Recordset && !$telecare_apotech_list->Recordset->EOF) {
	$telecare_apotech_list->Recordset->MoveFirst();
	$bSelectLimit = $telecare_apotech_list->UseSelectLimit;
	if (!$bSelectLimit && $telecare_apotech_list->StartRec > 1)
		$telecare_apotech_list->Recordset->Move($telecare_apotech_list->StartRec - 1);
} elseif (!$telecare_apotech->AllowAddDeleteRow && $telecare_apotech_list->StopRec == 0) {
	$telecare_apotech_list->StopRec = $telecare_apotech->GridAddRowCount;
}

// Initialize aggregate
$telecare_apotech->RowType = EW_ROWTYPE_AGGREGATEINIT;
$telecare_apotech->ResetAttrs();
$telecare_apotech_list->RenderRow();
while ($telecare_apotech_list->RecCnt < $telecare_apotech_list->StopRec) {
	$telecare_apotech_list->RecCnt++;
	if (intval($telecare_apotech_list->RecCnt) >= intval($telecare_apotech_list->StartRec)) {
		$telecare_apotech_list->RowCnt++;

		// Set up key count
		$telecare_apotech_list->KeyCount = $telecare_apotech_list->RowIndex;

		// Init row class and style
		$telecare_apotech->ResetAttrs();
		$telecare_apotech->CssClass = "";
		if ($telecare_apotech->CurrentAction == "gridadd") {
		} else {
			$telecare_apotech_list->LoadRowValues($telecare_apotech_list->Recordset); // Load row values
		}
		$telecare_apotech->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$telecare_apotech->RowAttrs = array_merge($telecare_apotech->RowAttrs, array('data-rowindex'=>$telecare_apotech_list->RowCnt, 'id'=>'r' . $telecare_apotech_list->RowCnt . '_telecare_apotech', 'data-rowtype'=>$telecare_apotech->RowType));

		// Render row
		$telecare_apotech_list->RenderRow();

		// Render list options
		$telecare_apotech_list->RenderListOptions();
?>
	<tr<?php echo $telecare_apotech->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_apotech_list->ListOptions->Render("body", "left", $telecare_apotech_list->RowCnt);
?>
	<?php if ($telecare_apotech->apotech_id->Visible) { // apotech_id ?>
		<td data-name="apotech_id"<?php echo $telecare_apotech->apotech_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_id" class="telecare_apotech_apotech_id">
<span<?php echo $telecare_apotech->apotech_id->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_id->ListViewValue() ?></span>
</span>
<a id="<?php echo $telecare_apotech_list->PageObjName . "_row_" . $telecare_apotech_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_admin_id->Visible) { // apotech_admin_id ?>
		<td data-name="apotech_admin_id"<?php echo $telecare_apotech->apotech_admin_id->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_admin_id" class="telecare_apotech_apotech_admin_id">
<span<?php echo $telecare_apotech->apotech_admin_id->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_admin_id->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_surname->Visible) { // apotech_surname ?>
		<td data-name="apotech_surname"<?php echo $telecare_apotech->apotech_surname->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_surname" class="telecare_apotech_apotech_surname">
<span<?php echo $telecare_apotech->apotech_surname->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_surname->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_name->Visible) { // apotech_name ?>
		<td data-name="apotech_name"<?php echo $telecare_apotech->apotech_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_name" class="telecare_apotech_apotech_name">
<span<?php echo $telecare_apotech->apotech_name->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_name->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_company_name->Visible) { // apotech_company_name ?>
		<td data-name="apotech_company_name"<?php echo $telecare_apotech->apotech_company_name->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_company_name" class="telecare_apotech_apotech_company_name">
<span<?php echo $telecare_apotech->apotech_company_name->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_name->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_company_phone->Visible) { // apotech_company_phone ?>
		<td data-name="apotech_company_phone"<?php echo $telecare_apotech->apotech_company_phone->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_company_phone" class="telecare_apotech_apotech_company_phone">
<span<?php echo $telecare_apotech->apotech_company_phone->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_phone->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_company_email->Visible) { // apotech_company_email ?>
		<td data-name="apotech_company_email"<?php echo $telecare_apotech->apotech_company_email->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_company_email" class="telecare_apotech_apotech_company_email">
<span<?php echo $telecare_apotech->apotech_company_email->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_company_email->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_latitude->Visible) { // apotech_latitude ?>
		<td data-name="apotech_latitude"<?php echo $telecare_apotech->apotech_latitude->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_latitude" class="telecare_apotech_apotech_latitude">
<span<?php echo $telecare_apotech->apotech_latitude->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_latitude->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
	<?php if ($telecare_apotech->apotech_longitude->Visible) { // apotech_longitude ?>
		<td data-name="apotech_longitude"<?php echo $telecare_apotech->apotech_longitude->CellAttributes() ?>>
<span id="el<?php echo $telecare_apotech_list->RowCnt ?>_telecare_apotech_apotech_longitude" class="telecare_apotech_apotech_longitude">
<span<?php echo $telecare_apotech->apotech_longitude->ViewAttributes() ?>>
<?php echo $telecare_apotech->apotech_longitude->ListViewValue() ?></span>
</span>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_apotech_list->ListOptions->Render("body", "right", $telecare_apotech_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($telecare_apotech->CurrentAction <> "gridadd")
		$telecare_apotech_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($telecare_apotech->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($telecare_apotech_list->Recordset)
	$telecare_apotech_list->Recordset->Close();
?>
<?php if ($telecare_apotech->Export == "") { ?>
<div class="panel-footer ewGridLowerPanel">
<?php if ($telecare_apotech->CurrentAction <> "gridadd" && $telecare_apotech->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-inline ewPagerForm" action="<?php echo ew_CurrentPage() ?>">
<?php if (!isset($telecare_apotech_list->Pager)) $telecare_apotech_list->Pager = new cPrevNextPager($telecare_apotech_list->StartRec, $telecare_apotech_list->DisplayRecs, $telecare_apotech_list->TotalRecs) ?>
<?php if ($telecare_apotech_list->Pager->RecordCount > 0) { ?>
<div class="ewPager">
<span><?php echo $Language->Phrase("Page") ?>&nbsp;</span>
<div class="ewPrevNext"><div class="input-group">
<div class="input-group-btn">
<!--first page button-->
	<?php if ($telecare_apotech_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerFirst") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->FirstButton->Start ?>"><span class="icon-first ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerFirst") ?>"><span class="icon-first ewIcon"></span></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($telecare_apotech_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerPrevious") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->PrevButton->Start ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerPrevious") ?>"><span class="icon-prev ewIcon"></span></a>
	<?php } ?>
</div>
<!--current page number-->
	<input class="form-control input-sm" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $telecare_apotech_list->Pager->CurrentPage ?>">
<div class="input-group-btn">
<!--next page button-->
	<?php if ($telecare_apotech_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerNext") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->NextButton->Start ?>"><span class="icon-next ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerNext") ?>"><span class="icon-next ewIcon"></span></a>
	<?php } ?>
<!--last page button-->
	<?php if ($telecare_apotech_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-default btn-sm" title="<?php echo $Language->Phrase("PagerLast") ?>" href="<?php echo $telecare_apotech_list->PageUrl() ?>start=<?php echo $telecare_apotech_list->Pager->LastButton->Start ?>"><span class="icon-last ewIcon"></span></a>
	<?php } else { ?>
	<a class="btn btn-default btn-sm disabled" title="<?php echo $Language->Phrase("PagerLast") ?>"><span class="icon-last ewIcon"></span></a>
	<?php } ?>
</div>
</div>
</div>
<span>&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->PageCount ?></span>
</div>
<div class="ewPager ewRec">
	<span><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $telecare_apotech_list->Pager->RecordCount ?></span>
</div>
<?php } ?>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_apotech_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if ($telecare_apotech_list->TotalRecs == 0 && $telecare_apotech->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_apotech_list->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($telecare_apotech->Export == "") { ?>
<script type="text/javascript">
ftelecare_apotechlistsrch.Init();
ftelecare_apotechlistsrch.FilterList = <?php echo $telecare_apotech_list->GetFilterList() ?>;
ftelecare_apotechlist.Init();
</script>
<?php } ?>
<?php
$telecare_apotech_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($telecare_apotech->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$telecare_apotech_list->Page_Terminate();
?>
