<!-- Begin Main Menu -->
<?php $RootMenu = new cMenu(EW_MENUBAR_ID) ?>
<?php

// Generate all menu items
$RootMenu->IsRoot = TRUE;
$RootMenu->AddMenuItem(5, "mi_telecare_alarm", $Language->MenuPhrase("5", "MenuText"), "telecare_alarmlist.php?cmd=resetall", -1, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_alarm'), FALSE);
$RootMenu->AddMenuItem(17, "mi_telecare_alarm_type", $Language->MenuPhrase("17", "MenuText"), "telecare_alarm_typelist.php", 5, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_alarm_type'), FALSE);
$RootMenu->AddMenuItem(55, "mci_Assistiti", $Language->MenuPhrase("55", "MenuText"), "", -1, "", IsLoggedIn(), FALSE, TRUE);
$RootMenu->AddMenuItem(23, "mi_telecare_doctor_user", $Language->MenuPhrase("23", "MenuText"), "telecare_doctor_userlist.php", 55, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_doctor_user'), FALSE);
$RootMenu->AddMenuItem(2, "mi_telecare_user", $Language->MenuPhrase("2", "MenuText"), "telecare_userlist.php", 55, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_user'), FALSE);
$RootMenu->AddMenuItem(6, "mi_telecare_call", $Language->MenuPhrase("6", "MenuText"), "telecare_calllist.php?cmd=resetall", 2, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_call'), FALSE);
$RootMenu->AddMenuItem(16, "mi_telecare_invalidity", $Language->MenuPhrase("16", "MenuText"), "telecare_invaliditylist.php", 2, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_invalidity'), FALSE);
$RootMenu->AddMenuItem(3, "mi_telecare_product", $Language->MenuPhrase("3", "MenuText"), "telecare_productlist.php", -1, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_product'), FALSE);
$RootMenu->AddMenuItem(4, "mi_telecare_product_type", $Language->MenuPhrase("4", "MenuText"), "telecare_product_typelist.php", 3, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_product_type'), FALSE);
$RootMenu->AddMenuItem(7, "mi_telecare_data", $Language->MenuPhrase("7", "MenuText"), "telecare_datalist.php", -1, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_data'), FALSE);
$RootMenu->AddMenuItem(56, "mci_Ricette_Mediche", $Language->MenuPhrase("56", "MenuText"), "", -1, "", IsLoggedIn(), FALSE, TRUE);
$RootMenu->AddMenuItem(22, "mi_telecare_prescription_drugstore", $Language->MenuPhrase("22", "MenuText"), "telecare_prescription_drugstorelist.php", 56, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_prescription_drugstore'), FALSE);
$RootMenu->AddMenuItem(9, "mi_telecare_prescription", $Language->MenuPhrase("9", "MenuText"), "telecare_prescriptionlist.php?cmd=resetall", 56, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_prescription'), FALSE);
$RootMenu->AddMenuItem(1, "mi_telecare_admin", $Language->MenuPhrase("1", "MenuText"), "telecare_adminlist.php", -1, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_admin'), FALSE);
$RootMenu->AddMenuItem(43, "mci_Impostazioni", $Language->MenuPhrase("43", "MenuText"), "", -1, "", IsLoggedIn(), FALSE, TRUE);
$RootMenu->AddMenuItem(8, "mi_telecare_drug", $Language->MenuPhrase("8", "MenuText"), "telecare_druglist.php", 43, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_drug'), FALSE);
$RootMenu->AddMenuItem(11, "mi_telecare_procedure", $Language->MenuPhrase("11", "MenuText"), "telecare_procedurelist.php", 43, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}telecare_procedure'), FALSE);
$RootMenu->AddMenuItem(18, "mi_audittrail", $Language->MenuPhrase("18", "MenuText"), "audittraillist.php", 43, "", AllowListMenu('{5100C3CA-F0DF-438E-B9AF-D8484F72A633}audittrail'), FALSE);
$RootMenu->AddMenuItem(-1, "mi_logout", $Language->Phrase("Logout"), "logout.php", -1, "", IsLoggedIn());
$RootMenu->AddMenuItem(-1, "mi_login", $Language->Phrase("Login"), "login.php", -1, "", !IsLoggedIn() && substr(@$_SERVER["URL"], -1 * strlen("login.php")) <> "login.php");
$RootMenu->Render();
?>
<!-- End Main Menu -->
