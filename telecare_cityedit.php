<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg12.php" ?>
<?php include_once ((EW_USE_ADODB) ? "adodb5/adodb.inc.php" : "ewmysql12.php") ?>
<?php include_once "phpfn12.php" ?>
<?php include_once "telecare_cityinfo.php" ?>
<?php include_once "telecare_admininfo.php" ?>
<?php include_once "userfn12.php" ?>
<?php

//
// Page class
//

$telecare_city_edit = NULL; // Initialize page object first

class ctelecare_city_edit extends ctelecare_city {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{5100C3CA-F0DF-438E-B9AF-D8484F72A633}";

	// Table name
	var $TableName = 'telecare_city';

	// Page object name
	var $PageObjName = 'telecare_city_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Methods to clear message
	function ClearMessage() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
	}

	function ClearFailureMessage() {
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
	}

	function ClearSuccessMessage() {
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
	}

	function ClearWarningMessage() {
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	function ClearMessages() {
		$_SESSION[EW_SESSION_MESSAGE] = "";
		$_SESSION[EW_SESSION_FAILURE_MESSAGE] = "";
		$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = "";
		$_SESSION[EW_SESSION_WARNING_MESSAGE] = "";
	}

	// Show message
	function ShowMessage() {
		$hidden = TRUE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-info ewInfo\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-danger ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}
	var $Token = "";
	var $TokenTimeout = 0;
	var $CheckToken = EW_CHECK_TOKEN;
	var $CheckTokenFn = "ew_CheckToken";
	var $CreateTokenFn = "ew_CreateToken";

	// Valid Post
	function ValidPost() {
		if (!$this->CheckToken || !ew_IsHttpPost())
			return TRUE;
		if (!isset($_POST[EW_TOKEN_NAME]))
			return FALSE;
		$fn = $this->CheckTokenFn;
		if (is_callable($fn))
			return $fn($_POST[EW_TOKEN_NAME], $this->TokenTimeout);
		return FALSE;
	}

	// Create Token
	function CreateToken() {
		global $gsToken;
		if ($this->CheckToken) {
			$fn = $this->CreateTokenFn;
			if ($this->Token == "" && is_callable($fn)) // Create token
				$this->Token = $fn();
			$gsToken = $this->Token; // Save to global variable
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		global $UserTable, $UserTableConn;
		$GLOBALS["Page"] = &$this;
		$this->TokenTimeout = ew_SessionTimeoutTime();

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (telecare_city)
		if (!isset($GLOBALS["telecare_city"]) || get_class($GLOBALS["telecare_city"]) == "ctelecare_city") {
			$GLOBALS["telecare_city"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["telecare_city"];
		}

		// Table object (telecare_admin)
		if (!isset($GLOBALS['telecare_admin'])) $GLOBALS['telecare_admin'] = new ctelecare_admin();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'telecare_city', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect($this->DBID);

		// User table object (telecare_admin)
		if (!isset($UserTable)) {
			$UserTable = new ctelecare_admin();
			$UserTableConn = Conn($UserTable->DBID);
		}
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsCustomExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loading();
		$Security->LoadCurrentUserLevel($this->ProjectID . $this->TableName);
		if ($Security->IsLoggedIn()) $Security->TablePermission_Loaded();
		if (!$Security->CanEdit()) {
			$Security->SaveLastUrl();
			$this->setFailureMessage($Language->Phrase("NoPermission")); // Set no permission
			if ($Security->CanList())
				$this->Page_Terminate(ew_GetUrl("telecare_citylist.php"));
			else
				$this->Page_Terminate(ew_GetUrl("login.php"));
		}
		if ($Security->IsLoggedIn()) {
			$Security->UserID_Loading();
			$Security->LoadUserID();
			$Security->UserID_Loaded();
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up current action
		$this->city_id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Check token
		if (!$this->ValidPost()) {
			echo $Language->Phrase("InvalidPostRequest");
			$this->Page_Terminate();
			exit();
		}

		// Process auto fill
		if (@$_POST["ajax"] == "autofill") {
			$results = $this->GetAutoFill(@$_POST["name"], @$_POST["q"]);
			if ($results) {

				// Clean output buffer
				if (!EW_DEBUG_ENABLED && ob_get_length())
					ob_end_clean();
				echo $results;
				$this->Page_Terminate();
				exit();
			}
		}

		// Create Token
		$this->CreateToken();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $gsExportFile, $gTmpImages;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();

		// Export
		global $EW_EXPORT, $telecare_city;
		if ($this->CustomExport <> "" && $this->CustomExport == $this->Export && array_key_exists($this->CustomExport, $EW_EXPORT)) {
				$sContent = ob_get_contents();
			if ($gsExportFile == "") $gsExportFile = $this->TableVar;
			$class = $EW_EXPORT[$this->CustomExport];
			if (class_exists($class)) {
				$doc = new $class($telecare_city);
				$doc->Text = $sContent;
				if ($this->Export == "email")
					echo $this->ExportEmail($doc->Text);
				else
					$doc->Export();
				ew_DeleteTmpImages(); // Delete temp images
				exit();
			}
		}
		$this->Page_Redirecting($url);

		 // Close connection
		ew_CloseConn();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $FormClassName = "form-horizontal ewForm ewEditForm";
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["city_id"] <> "") {
			$this->city_id->setQueryStringValue($_GET["city_id"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->city_id->CurrentValue == "")
			$this->Page_Terminate("telecare_citylist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("telecare_citylist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$sReturnUrl = $this->getReturnUrl();
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} elseif ($this->getFailureMessage() == $Language->Phrase("NoRecord")) {
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm, $Language;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->city_id->FldIsDetailKey)
			$this->city_id->setFormValue($objForm->GetValue("x_city_id"));
		if (!$this->city_name->FldIsDetailKey) {
			$this->city_name->setFormValue($objForm->GetValue("x_city_name"));
		}
		if (!$this->city_slug->FldIsDetailKey) {
			$this->city_slug->setFormValue($objForm->GetValue("x_city_slug"));
		}
		if (!$this->city_latitude->FldIsDetailKey) {
			$this->city_latitude->setFormValue($objForm->GetValue("x_city_latitude"));
		}
		if (!$this->city_longitude->FldIsDetailKey) {
			$this->city_longitude->setFormValue($objForm->GetValue("x_city_longitude"));
		}
		if (!$this->city_istat_code->FldIsDetailKey) {
			$this->city_istat_code->setFormValue($objForm->GetValue("x_city_istat_code"));
		}
		if (!$this->city_istat_alphanumeric_code->FldIsDetailKey) {
			$this->city_istat_alphanumeric_code->setFormValue($objForm->GetValue("x_city_istat_alphanumeric_code"));
		}
		if (!$this->city_province_istat_code->FldIsDetailKey) {
			$this->city_province_istat_code->setFormValue($objForm->GetValue("x_city_province_istat_code"));
		}
		if (!$this->city_administrative_center->FldIsDetailKey) {
			$this->city_administrative_center->setFormValue($objForm->GetValue("x_city_administrative_center"));
		}
		if (!$this->city_region_administrative_center->FldIsDetailKey) {
			$this->city_region_administrative_center->setFormValue($objForm->GetValue("x_city_region_administrative_center"));
		}
		if (!$this->city_province_id->FldIsDetailKey) {
			$this->city_province_id->setFormValue($objForm->GetValue("x_city_province_id"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->city_id->CurrentValue = $this->city_id->FormValue;
		$this->city_name->CurrentValue = $this->city_name->FormValue;
		$this->city_slug->CurrentValue = $this->city_slug->FormValue;
		$this->city_latitude->CurrentValue = $this->city_latitude->FormValue;
		$this->city_longitude->CurrentValue = $this->city_longitude->FormValue;
		$this->city_istat_code->CurrentValue = $this->city_istat_code->FormValue;
		$this->city_istat_alphanumeric_code->CurrentValue = $this->city_istat_alphanumeric_code->FormValue;
		$this->city_province_istat_code->CurrentValue = $this->city_province_istat_code->FormValue;
		$this->city_administrative_center->CurrentValue = $this->city_administrative_center->FormValue;
		$this->city_region_administrative_center->CurrentValue = $this->city_region_administrative_center->FormValue;
		$this->city_province_id->CurrentValue = $this->city_province_id->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn = &$this->Connection();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql, $conn);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->city_id->setDbValue($rs->fields('city_id'));
		$this->city_name->setDbValue($rs->fields('city_name'));
		$this->city_slug->setDbValue($rs->fields('city_slug'));
		$this->city_latitude->setDbValue($rs->fields('city_latitude'));
		$this->city_longitude->setDbValue($rs->fields('city_longitude'));
		$this->city_istat_code->setDbValue($rs->fields('city_istat_code'));
		$this->city_istat_alphanumeric_code->setDbValue($rs->fields('city_istat_alphanumeric_code'));
		$this->city_province_istat_code->setDbValue($rs->fields('city_province_istat_code'));
		$this->city_administrative_center->setDbValue($rs->fields('city_administrative_center'));
		$this->city_region_administrative_center->setDbValue($rs->fields('city_region_administrative_center'));
		$this->city_province_id->setDbValue($rs->fields('city_province_id'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->city_id->DbValue = $row['city_id'];
		$this->city_name->DbValue = $row['city_name'];
		$this->city_slug->DbValue = $row['city_slug'];
		$this->city_latitude->DbValue = $row['city_latitude'];
		$this->city_longitude->DbValue = $row['city_longitude'];
		$this->city_istat_code->DbValue = $row['city_istat_code'];
		$this->city_istat_alphanumeric_code->DbValue = $row['city_istat_alphanumeric_code'];
		$this->city_province_istat_code->DbValue = $row['city_province_istat_code'];
		$this->city_administrative_center->DbValue = $row['city_administrative_center'];
		$this->city_region_administrative_center->DbValue = $row['city_region_administrative_center'];
		$this->city_province_id->DbValue = $row['city_province_id'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $Security, $Language, $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// city_id
		// city_name
		// city_slug
		// city_latitude
		// city_longitude
		// city_istat_code
		// city_istat_alphanumeric_code
		// city_province_istat_code
		// city_administrative_center
		// city_region_administrative_center
		// city_province_id

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

		// city_id
		$this->city_id->ViewValue = $this->city_id->CurrentValue;
		$this->city_id->ViewCustomAttributes = "";

		// city_name
		$this->city_name->ViewValue = $this->city_name->CurrentValue;
		$this->city_name->ViewCustomAttributes = "";

		// city_slug
		$this->city_slug->ViewValue = $this->city_slug->CurrentValue;
		$this->city_slug->ViewCustomAttributes = "";

		// city_latitude
		$this->city_latitude->ViewValue = $this->city_latitude->CurrentValue;
		$this->city_latitude->ViewCustomAttributes = "";

		// city_longitude
		$this->city_longitude->ViewValue = $this->city_longitude->CurrentValue;
		$this->city_longitude->ViewCustomAttributes = "";

		// city_istat_code
		$this->city_istat_code->ViewValue = $this->city_istat_code->CurrentValue;
		$this->city_istat_code->ViewCustomAttributes = "";

		// city_istat_alphanumeric_code
		$this->city_istat_alphanumeric_code->ViewValue = $this->city_istat_alphanumeric_code->CurrentValue;
		$this->city_istat_alphanumeric_code->ViewCustomAttributes = "";

		// city_province_istat_code
		$this->city_province_istat_code->ViewValue = $this->city_province_istat_code->CurrentValue;
		$this->city_province_istat_code->ViewCustomAttributes = "";

		// city_administrative_center
		$this->city_administrative_center->ViewValue = $this->city_administrative_center->CurrentValue;
		$this->city_administrative_center->ViewCustomAttributes = "";

		// city_region_administrative_center
		$this->city_region_administrative_center->ViewValue = $this->city_region_administrative_center->CurrentValue;
		$this->city_region_administrative_center->ViewCustomAttributes = "";

		// city_province_id
		$this->city_province_id->ViewValue = $this->city_province_id->CurrentValue;
		$this->city_province_id->ViewCustomAttributes = "";

			// city_id
			$this->city_id->LinkCustomAttributes = "";
			$this->city_id->HrefValue = "";
			$this->city_id->TooltipValue = "";

			// city_name
			$this->city_name->LinkCustomAttributes = "";
			$this->city_name->HrefValue = "";
			$this->city_name->TooltipValue = "";

			// city_slug
			$this->city_slug->LinkCustomAttributes = "";
			$this->city_slug->HrefValue = "";
			$this->city_slug->TooltipValue = "";

			// city_latitude
			$this->city_latitude->LinkCustomAttributes = "";
			$this->city_latitude->HrefValue = "";
			$this->city_latitude->TooltipValue = "";

			// city_longitude
			$this->city_longitude->LinkCustomAttributes = "";
			$this->city_longitude->HrefValue = "";
			$this->city_longitude->TooltipValue = "";

			// city_istat_code
			$this->city_istat_code->LinkCustomAttributes = "";
			$this->city_istat_code->HrefValue = "";
			$this->city_istat_code->TooltipValue = "";

			// city_istat_alphanumeric_code
			$this->city_istat_alphanumeric_code->LinkCustomAttributes = "";
			$this->city_istat_alphanumeric_code->HrefValue = "";
			$this->city_istat_alphanumeric_code->TooltipValue = "";

			// city_province_istat_code
			$this->city_province_istat_code->LinkCustomAttributes = "";
			$this->city_province_istat_code->HrefValue = "";
			$this->city_province_istat_code->TooltipValue = "";

			// city_administrative_center
			$this->city_administrative_center->LinkCustomAttributes = "";
			$this->city_administrative_center->HrefValue = "";
			$this->city_administrative_center->TooltipValue = "";

			// city_region_administrative_center
			$this->city_region_administrative_center->LinkCustomAttributes = "";
			$this->city_region_administrative_center->HrefValue = "";
			$this->city_region_administrative_center->TooltipValue = "";

			// city_province_id
			$this->city_province_id->LinkCustomAttributes = "";
			$this->city_province_id->HrefValue = "";
			$this->city_province_id->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// city_id
			$this->city_id->EditAttrs["class"] = "form-control";
			$this->city_id->EditCustomAttributes = "";
			$this->city_id->EditValue = $this->city_id->CurrentValue;
			$this->city_id->ViewCustomAttributes = "";

			// city_name
			$this->city_name->EditAttrs["class"] = "form-control";
			$this->city_name->EditCustomAttributes = "";
			$this->city_name->EditValue = ew_HtmlEncode($this->city_name->CurrentValue);
			$this->city_name->PlaceHolder = ew_RemoveHtml($this->city_name->FldCaption());

			// city_slug
			$this->city_slug->EditAttrs["class"] = "form-control";
			$this->city_slug->EditCustomAttributes = "";
			$this->city_slug->EditValue = ew_HtmlEncode($this->city_slug->CurrentValue);
			$this->city_slug->PlaceHolder = ew_RemoveHtml($this->city_slug->FldCaption());

			// city_latitude
			$this->city_latitude->EditAttrs["class"] = "form-control";
			$this->city_latitude->EditCustomAttributes = "";
			$this->city_latitude->EditValue = ew_HtmlEncode($this->city_latitude->CurrentValue);
			$this->city_latitude->PlaceHolder = ew_RemoveHtml($this->city_latitude->FldCaption());

			// city_longitude
			$this->city_longitude->EditAttrs["class"] = "form-control";
			$this->city_longitude->EditCustomAttributes = "";
			$this->city_longitude->EditValue = ew_HtmlEncode($this->city_longitude->CurrentValue);
			$this->city_longitude->PlaceHolder = ew_RemoveHtml($this->city_longitude->FldCaption());

			// city_istat_code
			$this->city_istat_code->EditAttrs["class"] = "form-control";
			$this->city_istat_code->EditCustomAttributes = "";
			$this->city_istat_code->EditValue = ew_HtmlEncode($this->city_istat_code->CurrentValue);
			$this->city_istat_code->PlaceHolder = ew_RemoveHtml($this->city_istat_code->FldCaption());

			// city_istat_alphanumeric_code
			$this->city_istat_alphanumeric_code->EditAttrs["class"] = "form-control";
			$this->city_istat_alphanumeric_code->EditCustomAttributes = "";
			$this->city_istat_alphanumeric_code->EditValue = ew_HtmlEncode($this->city_istat_alphanumeric_code->CurrentValue);
			$this->city_istat_alphanumeric_code->PlaceHolder = ew_RemoveHtml($this->city_istat_alphanumeric_code->FldCaption());

			// city_province_istat_code
			$this->city_province_istat_code->EditAttrs["class"] = "form-control";
			$this->city_province_istat_code->EditCustomAttributes = "";
			$this->city_province_istat_code->EditValue = ew_HtmlEncode($this->city_province_istat_code->CurrentValue);
			$this->city_province_istat_code->PlaceHolder = ew_RemoveHtml($this->city_province_istat_code->FldCaption());

			// city_administrative_center
			$this->city_administrative_center->EditAttrs["class"] = "form-control";
			$this->city_administrative_center->EditCustomAttributes = "";
			$this->city_administrative_center->EditValue = ew_HtmlEncode($this->city_administrative_center->CurrentValue);
			$this->city_administrative_center->PlaceHolder = ew_RemoveHtml($this->city_administrative_center->FldCaption());

			// city_region_administrative_center
			$this->city_region_administrative_center->EditAttrs["class"] = "form-control";
			$this->city_region_administrative_center->EditCustomAttributes = "";
			$this->city_region_administrative_center->EditValue = ew_HtmlEncode($this->city_region_administrative_center->CurrentValue);
			$this->city_region_administrative_center->PlaceHolder = ew_RemoveHtml($this->city_region_administrative_center->FldCaption());

			// city_province_id
			$this->city_province_id->EditAttrs["class"] = "form-control";
			$this->city_province_id->EditCustomAttributes = "";
			$this->city_province_id->EditValue = ew_HtmlEncode($this->city_province_id->CurrentValue);
			$this->city_province_id->PlaceHolder = ew_RemoveHtml($this->city_province_id->FldCaption());

			// Edit refer script
			// city_id

			$this->city_id->LinkCustomAttributes = "";
			$this->city_id->HrefValue = "";

			// city_name
			$this->city_name->LinkCustomAttributes = "";
			$this->city_name->HrefValue = "";

			// city_slug
			$this->city_slug->LinkCustomAttributes = "";
			$this->city_slug->HrefValue = "";

			// city_latitude
			$this->city_latitude->LinkCustomAttributes = "";
			$this->city_latitude->HrefValue = "";

			// city_longitude
			$this->city_longitude->LinkCustomAttributes = "";
			$this->city_longitude->HrefValue = "";

			// city_istat_code
			$this->city_istat_code->LinkCustomAttributes = "";
			$this->city_istat_code->HrefValue = "";

			// city_istat_alphanumeric_code
			$this->city_istat_alphanumeric_code->LinkCustomAttributes = "";
			$this->city_istat_alphanumeric_code->HrefValue = "";

			// city_province_istat_code
			$this->city_province_istat_code->LinkCustomAttributes = "";
			$this->city_province_istat_code->HrefValue = "";

			// city_administrative_center
			$this->city_administrative_center->LinkCustomAttributes = "";
			$this->city_administrative_center->HrefValue = "";

			// city_region_administrative_center
			$this->city_region_administrative_center->LinkCustomAttributes = "";
			$this->city_region_administrative_center->HrefValue = "";

			// city_province_id
			$this->city_province_id->LinkCustomAttributes = "";
			$this->city_province_id->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->city_name->FldIsDetailKey && !is_null($this->city_name->FormValue) && $this->city_name->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_name->FldCaption(), $this->city_name->ReqErrMsg));
		}
		if (!$this->city_slug->FldIsDetailKey && !is_null($this->city_slug->FormValue) && $this->city_slug->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_slug->FldCaption(), $this->city_slug->ReqErrMsg));
		}
		if (!$this->city_latitude->FldIsDetailKey && !is_null($this->city_latitude->FormValue) && $this->city_latitude->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_latitude->FldCaption(), $this->city_latitude->ReqErrMsg));
		}
		if (!$this->city_longitude->FldIsDetailKey && !is_null($this->city_longitude->FormValue) && $this->city_longitude->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_longitude->FldCaption(), $this->city_longitude->ReqErrMsg));
		}
		if (!$this->city_istat_code->FldIsDetailKey && !is_null($this->city_istat_code->FormValue) && $this->city_istat_code->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_istat_code->FldCaption(), $this->city_istat_code->ReqErrMsg));
		}
		if (!$this->city_istat_alphanumeric_code->FldIsDetailKey && !is_null($this->city_istat_alphanumeric_code->FormValue) && $this->city_istat_alphanumeric_code->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_istat_alphanumeric_code->FldCaption(), $this->city_istat_alphanumeric_code->ReqErrMsg));
		}
		if (!$this->city_province_istat_code->FldIsDetailKey && !is_null($this->city_province_istat_code->FormValue) && $this->city_province_istat_code->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_province_istat_code->FldCaption(), $this->city_province_istat_code->ReqErrMsg));
		}
		if (!$this->city_administrative_center->FldIsDetailKey && !is_null($this->city_administrative_center->FormValue) && $this->city_administrative_center->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_administrative_center->FldCaption(), $this->city_administrative_center->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->city_administrative_center->FormValue)) {
			ew_AddMessage($gsFormError, $this->city_administrative_center->FldErrMsg());
		}
		if (!$this->city_region_administrative_center->FldIsDetailKey && !is_null($this->city_region_administrative_center->FormValue) && $this->city_region_administrative_center->FormValue == "") {
			ew_AddMessage($gsFormError, str_replace("%s", $this->city_region_administrative_center->FldCaption(), $this->city_region_administrative_center->ReqErrMsg));
		}
		if (!ew_CheckInteger($this->city_region_administrative_center->FormValue)) {
			ew_AddMessage($gsFormError, $this->city_region_administrative_center->FldErrMsg());
		}
		if (!ew_CheckInteger($this->city_province_id->FormValue)) {
			ew_AddMessage($gsFormError, $this->city_province_id->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $Security, $Language;
		$sFilter = $this->KeyFilter();
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$conn = &$this->Connection();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// city_name
			$this->city_name->SetDbValueDef($rsnew, $this->city_name->CurrentValue, "", $this->city_name->ReadOnly);

			// city_slug
			$this->city_slug->SetDbValueDef($rsnew, $this->city_slug->CurrentValue, "", $this->city_slug->ReadOnly);

			// city_latitude
			$this->city_latitude->SetDbValueDef($rsnew, $this->city_latitude->CurrentValue, "", $this->city_latitude->ReadOnly);

			// city_longitude
			$this->city_longitude->SetDbValueDef($rsnew, $this->city_longitude->CurrentValue, "", $this->city_longitude->ReadOnly);

			// city_istat_code
			$this->city_istat_code->SetDbValueDef($rsnew, $this->city_istat_code->CurrentValue, "", $this->city_istat_code->ReadOnly);

			// city_istat_alphanumeric_code
			$this->city_istat_alphanumeric_code->SetDbValueDef($rsnew, $this->city_istat_alphanumeric_code->CurrentValue, "", $this->city_istat_alphanumeric_code->ReadOnly);

			// city_province_istat_code
			$this->city_province_istat_code->SetDbValueDef($rsnew, $this->city_province_istat_code->CurrentValue, "", $this->city_province_istat_code->ReadOnly);

			// city_administrative_center
			$this->city_administrative_center->SetDbValueDef($rsnew, $this->city_administrative_center->CurrentValue, 0, $this->city_administrative_center->ReadOnly);

			// city_region_administrative_center
			$this->city_region_administrative_center->SetDbValueDef($rsnew, $this->city_region_administrative_center->CurrentValue, 0, $this->city_region_administrative_center->ReadOnly);

			// city_province_id
			$this->city_province_id->SetDbValueDef($rsnew, $this->city_province_id->CurrentValue, NULL, $this->city_province_id->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = $GLOBALS["EW_ERROR_FN"];
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$url = substr(ew_CurrentUrl(), strrpos(ew_CurrentUrl(), "/")+1);
		$Breadcrumb->Add("list", $this->TableVar, "telecare_citylist.php", "", $this->TableVar, TRUE);
		$PageId = "edit";
		$Breadcrumb->Add("edit", $PageId, $url);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(TRUE) ?>
<?php

// Create page object
if (!isset($telecare_city_edit)) $telecare_city_edit = new ctelecare_city_edit();

// Page init
$telecare_city_edit->Page_Init();

// Page main
$telecare_city_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_city_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Form object
var CurrentPageID = EW_PAGE_ID = "edit";
var CurrentForm = ftelecare_cityedit = new ew_Form("ftelecare_cityedit", "edit");

// Validate form
ftelecare_cityedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_city_name");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_name->FldCaption(), $telecare_city->city_name->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_slug");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_slug->FldCaption(), $telecare_city->city_slug->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_latitude");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_latitude->FldCaption(), $telecare_city->city_latitude->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_longitude");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_longitude->FldCaption(), $telecare_city->city_longitude->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_istat_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_istat_code->FldCaption(), $telecare_city->city_istat_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_istat_alphanumeric_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_istat_alphanumeric_code->FldCaption(), $telecare_city->city_istat_alphanumeric_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_province_istat_code");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_province_istat_code->FldCaption(), $telecare_city->city_province_istat_code->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_administrative_center");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_administrative_center->FldCaption(), $telecare_city->city_administrative_center->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_administrative_center");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_city->city_administrative_center->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_city_region_administrative_center");
			if (elm && !ew_IsHidden(elm) && !ew_HasValue(elm))
				return this.OnError(elm, "<?php echo ew_JsEncode2(str_replace("%s", $telecare_city->city_region_administrative_center->FldCaption(), $telecare_city->city_region_administrative_center->ReqErrMsg)) ?>");
			elm = this.GetElements("x" + infix + "_city_region_administrative_center");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_city->city_region_administrative_center->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_city_province_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_city->city_province_id->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
ftelecare_cityedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_cityedit.ValidateRequired = true;
<?php } else { ?>
ftelecare_cityedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<div class="ewToolbar">
<?php $Breadcrumb->Render(); ?>
<?php echo $Language->SelectionForm(); ?>
<div class="clearfix"></div>
</div>
<?php $telecare_city_edit->ShowPageHeader(); ?>
<?php
$telecare_city_edit->ShowMessage();
?>
<form name="ftelecare_cityedit" id="ftelecare_cityedit" class="<?php echo $telecare_city_edit->FormClassName ?>" action="<?php echo ew_CurrentPage() ?>" method="post">
<?php if ($telecare_city_edit->CheckToken) { ?>
<input type="hidden" name="<?php echo EW_TOKEN_NAME ?>" value="<?php echo $telecare_city_edit->Token ?>">
<?php } ?>
<input type="hidden" name="t" value="telecare_city">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<div>
<?php if ($telecare_city->city_id->Visible) { // city_id ?>
	<div id="r_city_id" class="form-group">
		<label id="elh_telecare_city_city_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_id->CellAttributes() ?>>
<span id="el_telecare_city_city_id">
<span<?php echo $telecare_city->city_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_city->city_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_city" data-field="x_city_id" name="x_city_id" id="x_city_id" value="<?php echo ew_HtmlEncode($telecare_city->city_id->CurrentValue) ?>">
<?php echo $telecare_city->city_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_name->Visible) { // city_name ?>
	<div id="r_city_name" class="form-group">
		<label id="elh_telecare_city_city_name" for="x_city_name" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_name->CellAttributes() ?>>
<span id="el_telecare_city_city_name">
<input type="text" data-table="telecare_city" data-field="x_city_name" name="x_city_name" id="x_city_name" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_name->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_name->EditValue ?>"<?php echo $telecare_city->city_name->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_name->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_slug->Visible) { // city_slug ?>
	<div id="r_city_slug" class="form-group">
		<label id="elh_telecare_city_city_slug" for="x_city_slug" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_slug->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_slug->CellAttributes() ?>>
<span id="el_telecare_city_city_slug">
<input type="text" data-table="telecare_city" data-field="x_city_slug" name="x_city_slug" id="x_city_slug" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_slug->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_slug->EditValue ?>"<?php echo $telecare_city->city_slug->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_slug->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_latitude->Visible) { // city_latitude ?>
	<div id="r_city_latitude" class="form-group">
		<label id="elh_telecare_city_city_latitude" for="x_city_latitude" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_latitude->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_latitude->CellAttributes() ?>>
<span id="el_telecare_city_city_latitude">
<input type="text" data-table="telecare_city" data-field="x_city_latitude" name="x_city_latitude" id="x_city_latitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_latitude->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_latitude->EditValue ?>"<?php echo $telecare_city->city_latitude->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_latitude->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_longitude->Visible) { // city_longitude ?>
	<div id="r_city_longitude" class="form-group">
		<label id="elh_telecare_city_city_longitude" for="x_city_longitude" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_longitude->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_longitude->CellAttributes() ?>>
<span id="el_telecare_city_city_longitude">
<input type="text" data-table="telecare_city" data-field="x_city_longitude" name="x_city_longitude" id="x_city_longitude" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_longitude->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_longitude->EditValue ?>"<?php echo $telecare_city->city_longitude->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_longitude->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_istat_code->Visible) { // city_istat_code ?>
	<div id="r_city_istat_code" class="form-group">
		<label id="elh_telecare_city_city_istat_code" for="x_city_istat_code" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_istat_code->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_istat_code->CellAttributes() ?>>
<span id="el_telecare_city_city_istat_code">
<input type="text" data-table="telecare_city" data-field="x_city_istat_code" name="x_city_istat_code" id="x_city_istat_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_istat_code->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_istat_code->EditValue ?>"<?php echo $telecare_city->city_istat_code->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_istat_code->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_istat_alphanumeric_code->Visible) { // city_istat_alphanumeric_code ?>
	<div id="r_city_istat_alphanumeric_code" class="form-group">
		<label id="elh_telecare_city_city_istat_alphanumeric_code" for="x_city_istat_alphanumeric_code" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_istat_alphanumeric_code->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_istat_alphanumeric_code->CellAttributes() ?>>
<span id="el_telecare_city_city_istat_alphanumeric_code">
<input type="text" data-table="telecare_city" data-field="x_city_istat_alphanumeric_code" name="x_city_istat_alphanumeric_code" id="x_city_istat_alphanumeric_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_istat_alphanumeric_code->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_istat_alphanumeric_code->EditValue ?>"<?php echo $telecare_city->city_istat_alphanumeric_code->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_istat_alphanumeric_code->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_province_istat_code->Visible) { // city_province_istat_code ?>
	<div id="r_city_province_istat_code" class="form-group">
		<label id="elh_telecare_city_city_province_istat_code" for="x_city_province_istat_code" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_province_istat_code->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_province_istat_code->CellAttributes() ?>>
<span id="el_telecare_city_city_province_istat_code">
<input type="text" data-table="telecare_city" data-field="x_city_province_istat_code" name="x_city_province_istat_code" id="x_city_province_istat_code" size="30" maxlength="255" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_province_istat_code->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_province_istat_code->EditValue ?>"<?php echo $telecare_city->city_province_istat_code->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_province_istat_code->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_administrative_center->Visible) { // city_administrative_center ?>
	<div id="r_city_administrative_center" class="form-group">
		<label id="elh_telecare_city_city_administrative_center" for="x_city_administrative_center" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_administrative_center->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_administrative_center->CellAttributes() ?>>
<span id="el_telecare_city_city_administrative_center">
<input type="text" data-table="telecare_city" data-field="x_city_administrative_center" name="x_city_administrative_center" id="x_city_administrative_center" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_administrative_center->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_administrative_center->EditValue ?>"<?php echo $telecare_city->city_administrative_center->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_administrative_center->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_region_administrative_center->Visible) { // city_region_administrative_center ?>
	<div id="r_city_region_administrative_center" class="form-group">
		<label id="elh_telecare_city_city_region_administrative_center" for="x_city_region_administrative_center" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_region_administrative_center->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_region_administrative_center->CellAttributes() ?>>
<span id="el_telecare_city_city_region_administrative_center">
<input type="text" data-table="telecare_city" data-field="x_city_region_administrative_center" name="x_city_region_administrative_center" id="x_city_region_administrative_center" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_region_administrative_center->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_region_administrative_center->EditValue ?>"<?php echo $telecare_city->city_region_administrative_center->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_region_administrative_center->CustomMsg ?></div></div>
	</div>
<?php } ?>
<?php if ($telecare_city->city_province_id->Visible) { // city_province_id ?>
	<div id="r_city_province_id" class="form-group">
		<label id="elh_telecare_city_city_province_id" for="x_city_province_id" class="col-sm-2 control-label ewLabel"><?php echo $telecare_city->city_province_id->FldCaption() ?></label>
		<div class="col-sm-10"><div<?php echo $telecare_city->city_province_id->CellAttributes() ?>>
<span id="el_telecare_city_city_province_id">
<input type="text" data-table="telecare_city" data-field="x_city_province_id" name="x_city_province_id" id="x_city_province_id" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_city->city_province_id->getPlaceHolder()) ?>" value="<?php echo $telecare_city->city_province_id->EditValue ?>"<?php echo $telecare_city->city_province_id->EditAttributes() ?>>
</span>
<?php echo $telecare_city->city_province_id->CustomMsg ?></div></div>
	</div>
<?php } ?>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("SaveBtn") ?></button>
<button class="btn btn-default ewButton" name="btnCancel" id="btnCancel" type="button" data-href="<?php echo $telecare_city_edit->getReturnUrl() ?>"><?php echo $Language->Phrase("CancelBtn") ?></button>
	</div>
</div>
</form>
<script type="text/javascript">
ftelecare_cityedit.Init();
</script>
<?php
$telecare_city_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$telecare_city_edit->Page_Terminate();
?>
