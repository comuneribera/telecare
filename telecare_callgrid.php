<?php include_once "telecare_admininfo.php" ?>
<?php

// Create page object
if (!isset($telecare_call_grid)) $telecare_call_grid = new ctelecare_call_grid();

// Page init
$telecare_call_grid->Page_Init();

// Page main
$telecare_call_grid->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$telecare_call_grid->Page_Render();
?>
<?php if ($telecare_call->Export == "") { ?>
<script type="text/javascript">

// Form object
var ftelecare_callgrid = new ew_Form("ftelecare_callgrid", "grid");
ftelecare_callgrid.FormKeyCountName = '<?php echo $telecare_call_grid->FormKeyCountName ?>';

// Validate form
ftelecare_callgrid.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
		var checkrow = (gridinsert) ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
			elm = this.GetElements("x" + infix + "_call_user_id");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_user_id->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_call_requested_date");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_requested_date->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_call_start_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_start_on->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_call_close_on");
			if (elm && !ew_CheckEuroDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($telecare_call->call_close_on->FldErrMsg()) ?>");

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
ftelecare_callgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "call_user_id", false)) return false;
	if (ew_ValueChanged(fobj, infix, "call_requested_date", false)) return false;
	if (ew_ValueChanged(fobj, infix, "call_start_on", false)) return false;
	if (ew_ValueChanged(fobj, infix, "call_close_on", false)) return false;
	if (ew_ValueChanged(fobj, infix, "call_closed", false)) return false;
	return true;
}

// Form_CustomValidate event
ftelecare_callgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
ftelecare_callgrid.ValidateRequired = true;
<?php } else { ?>
ftelecare_callgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
ftelecare_callgrid.Lists["x_call_user_id"] = {"LinkField":"x_user_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_user_surname","x_user_name","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_callgrid.Lists["x_call_admin_id"] = {"LinkField":"x_admin_id","Ajax":true,"AutoFill":false,"DisplayFields":["x_admin_surname","x_admin_name","x_admin_company_name",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_callgrid.Lists["x_call_closed"] = {"LinkField":"","Ajax":null,"AutoFill":false,"DisplayFields":["","","",""],"ParentFields":[],"ChildFields":[],"FilterFields":[],"Options":[],"Template":""};
ftelecare_callgrid.Lists["x_call_closed"].Options = <?php echo json_encode($telecare_call->call_closed->Options()) ?>;

// Form object for search
</script>
<?php } ?>
<?php
if ($telecare_call->CurrentAction == "gridadd") {
	if ($telecare_call->CurrentMode == "copy") {
		$bSelectLimit = $telecare_call_grid->UseSelectLimit;
		if ($bSelectLimit) {
			$telecare_call_grid->TotalRecs = $telecare_call->SelectRecordCount();
			$telecare_call_grid->Recordset = $telecare_call_grid->LoadRecordset($telecare_call_grid->StartRec-1, $telecare_call_grid->DisplayRecs);
		} else {
			if ($telecare_call_grid->Recordset = $telecare_call_grid->LoadRecordset())
				$telecare_call_grid->TotalRecs = $telecare_call_grid->Recordset->RecordCount();
		}
		$telecare_call_grid->StartRec = 1;
		$telecare_call_grid->DisplayRecs = $telecare_call_grid->TotalRecs;
	} else {
		$telecare_call->CurrentFilter = "0=1";
		$telecare_call_grid->StartRec = 1;
		$telecare_call_grid->DisplayRecs = $telecare_call->GridAddRowCount;
	}
	$telecare_call_grid->TotalRecs = $telecare_call_grid->DisplayRecs;
	$telecare_call_grid->StopRec = $telecare_call_grid->DisplayRecs;
} else {
	$bSelectLimit = $telecare_call_grid->UseSelectLimit;
	if ($bSelectLimit) {
		if ($telecare_call_grid->TotalRecs <= 0)
			$telecare_call_grid->TotalRecs = $telecare_call->SelectRecordCount();
	} else {
		if (!$telecare_call_grid->Recordset && ($telecare_call_grid->Recordset = $telecare_call_grid->LoadRecordset()))
			$telecare_call_grid->TotalRecs = $telecare_call_grid->Recordset->RecordCount();
	}
	$telecare_call_grid->StartRec = 1;
	$telecare_call_grid->DisplayRecs = $telecare_call_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$telecare_call_grid->Recordset = $telecare_call_grid->LoadRecordset($telecare_call_grid->StartRec-1, $telecare_call_grid->DisplayRecs);

	// Set no record found message
	if ($telecare_call->CurrentAction == "" && $telecare_call_grid->TotalRecs == 0) {
		if (!$Security->CanList())
			$telecare_call_grid->setWarningMessage($Language->Phrase("NoPermission"));
		if ($telecare_call_grid->SearchWhere == "0=101")
			$telecare_call_grid->setWarningMessage($Language->Phrase("EnterSearchCriteria"));
		else
			$telecare_call_grid->setWarningMessage($Language->Phrase("NoRecord"));
	}
}
$telecare_call_grid->RenderOtherOptions();
?>
<?php $telecare_call_grid->ShowPageHeader(); ?>
<?php
$telecare_call_grid->ShowMessage();
?>
<?php if ($telecare_call_grid->TotalRecs > 0 || $telecare_call->CurrentAction <> "") { ?>
<div class="panel panel-default ewGrid">
<div id="ftelecare_callgrid" class="ewForm form-inline">
<?php if ($telecare_call_grid->ShowOtherOptions) { ?>
<div class="panel-heading ewGridUpperPanel">
<?php
	foreach ($telecare_call_grid->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<div id="gmp_telecare_call" class="<?php if (ew_IsResponsiveLayout()) { echo "table-responsive "; } ?>ewGridMiddlePanel">
<table id="tbl_telecare_callgrid" class="table ewTable">
<?php echo $telecare_call->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Header row
$telecare_call_grid->RowType = EW_ROWTYPE_HEADER;

// Render list options
$telecare_call_grid->RenderListOptions();

// Render list options (header, left)
$telecare_call_grid->ListOptions->Render("header", "left");
?>
<?php if ($telecare_call->call_id->Visible) { // call_id ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_id) == "") { ?>
		<th data-name="call_id"><div id="elh_telecare_call_call_id" class="telecare_call_call_id"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_id"><div><div id="elh_telecare_call_call_id" class="telecare_call_call_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_call->call_user_id->Visible) { // call_user_id ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_user_id) == "") { ?>
		<th data-name="call_user_id"><div id="elh_telecare_call_call_user_id" class="telecare_call_call_user_id"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_user_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_user_id"><div><div id="elh_telecare_call_call_user_id" class="telecare_call_call_user_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_user_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_user_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_user_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_call->call_admin_id->Visible) { // call_admin_id ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_admin_id) == "") { ?>
		<th data-name="call_admin_id"><div id="elh_telecare_call_call_admin_id" class="telecare_call_call_admin_id"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_admin_id->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_admin_id"><div><div id="elh_telecare_call_call_admin_id" class="telecare_call_call_admin_id">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_admin_id->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_admin_id->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_admin_id->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_call->call_requested_date->Visible) { // call_requested_date ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_requested_date) == "") { ?>
		<th data-name="call_requested_date"><div id="elh_telecare_call_call_requested_date" class="telecare_call_call_requested_date"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_requested_date->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_requested_date"><div><div id="elh_telecare_call_call_requested_date" class="telecare_call_call_requested_date">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_requested_date->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_requested_date->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_requested_date->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_call->call_start_on->Visible) { // call_start_on ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_start_on) == "") { ?>
		<th data-name="call_start_on"><div id="elh_telecare_call_call_start_on" class="telecare_call_call_start_on"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_start_on->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_start_on"><div><div id="elh_telecare_call_call_start_on" class="telecare_call_call_start_on">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_start_on->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_start_on->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_start_on->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_call->call_close_on->Visible) { // call_close_on ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_close_on) == "") { ?>
		<th data-name="call_close_on"><div id="elh_telecare_call_call_close_on" class="telecare_call_call_close_on"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_close_on->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_close_on"><div><div id="elh_telecare_call_call_close_on" class="telecare_call_call_close_on">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_close_on->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_close_on->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_close_on->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_call->call_closed->Visible) { // call_closed ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_closed) == "") { ?>
		<th data-name="call_closed"><div id="elh_telecare_call_call_closed" class="telecare_call_call_closed"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_closed->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_closed"><div><div id="elh_telecare_call_call_closed" class="telecare_call_call_closed">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_closed->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_closed->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_closed->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php if ($telecare_call->call_last_update->Visible) { // call_last_update ?>
	<?php if ($telecare_call->SortUrl($telecare_call->call_last_update) == "") { ?>
		<th data-name="call_last_update"><div id="elh_telecare_call_call_last_update" class="telecare_call_call_last_update"><div class="ewTableHeaderCaption"><?php echo $telecare_call->call_last_update->FldCaption() ?></div></div></th>
	<?php } else { ?>
		<th data-name="call_last_update"><div><div id="elh_telecare_call_call_last_update" class="telecare_call_call_last_update">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $telecare_call->call_last_update->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($telecare_call->call_last_update->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($telecare_call->call_last_update->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></th>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$telecare_call_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$telecare_call_grid->StartRec = 1;
$telecare_call_grid->StopRec = $telecare_call_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue($telecare_call_grid->FormKeyCountName) && ($telecare_call->CurrentAction == "gridadd" || $telecare_call->CurrentAction == "gridedit" || $telecare_call->CurrentAction == "F")) {
		$telecare_call_grid->KeyCount = $objForm->GetValue($telecare_call_grid->FormKeyCountName);
		$telecare_call_grid->StopRec = $telecare_call_grid->StartRec + $telecare_call_grid->KeyCount - 1;
	}
}
$telecare_call_grid->RecCnt = $telecare_call_grid->StartRec - 1;
if ($telecare_call_grid->Recordset && !$telecare_call_grid->Recordset->EOF) {
	$telecare_call_grid->Recordset->MoveFirst();
	$bSelectLimit = $telecare_call_grid->UseSelectLimit;
	if (!$bSelectLimit && $telecare_call_grid->StartRec > 1)
		$telecare_call_grid->Recordset->Move($telecare_call_grid->StartRec - 1);
} elseif (!$telecare_call->AllowAddDeleteRow && $telecare_call_grid->StopRec == 0) {
	$telecare_call_grid->StopRec = $telecare_call->GridAddRowCount;
}

// Initialize aggregate
$telecare_call->RowType = EW_ROWTYPE_AGGREGATEINIT;
$telecare_call->ResetAttrs();
$telecare_call_grid->RenderRow();
if ($telecare_call->CurrentAction == "gridadd")
	$telecare_call_grid->RowIndex = 0;
if ($telecare_call->CurrentAction == "gridedit")
	$telecare_call_grid->RowIndex = 0;
while ($telecare_call_grid->RecCnt < $telecare_call_grid->StopRec) {
	$telecare_call_grid->RecCnt++;
	if (intval($telecare_call_grid->RecCnt) >= intval($telecare_call_grid->StartRec)) {
		$telecare_call_grid->RowCnt++;
		if ($telecare_call->CurrentAction == "gridadd" || $telecare_call->CurrentAction == "gridedit" || $telecare_call->CurrentAction == "F") {
			$telecare_call_grid->RowIndex++;
			$objForm->Index = $telecare_call_grid->RowIndex;
			if ($objForm->HasValue($telecare_call_grid->FormActionName))
				$telecare_call_grid->RowAction = strval($objForm->GetValue($telecare_call_grid->FormActionName));
			elseif ($telecare_call->CurrentAction == "gridadd")
				$telecare_call_grid->RowAction = "insert";
			else
				$telecare_call_grid->RowAction = "";
		}

		// Set up key count
		$telecare_call_grid->KeyCount = $telecare_call_grid->RowIndex;

		// Init row class and style
		$telecare_call->ResetAttrs();
		$telecare_call->CssClass = "";
		if ($telecare_call->CurrentAction == "gridadd") {
			if ($telecare_call->CurrentMode == "copy") {
				$telecare_call_grid->LoadRowValues($telecare_call_grid->Recordset); // Load row values
				$telecare_call_grid->SetRecordKey($telecare_call_grid->RowOldKey, $telecare_call_grid->Recordset); // Set old record key
			} else {
				$telecare_call_grid->LoadDefaultValues(); // Load default values
				$telecare_call_grid->RowOldKey = ""; // Clear old key value
			}
		} else {
			$telecare_call_grid->LoadRowValues($telecare_call_grid->Recordset); // Load row values
		}
		$telecare_call->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($telecare_call->CurrentAction == "gridadd") // Grid add
			$telecare_call->RowType = EW_ROWTYPE_ADD; // Render add
		if ($telecare_call->CurrentAction == "gridadd" && $telecare_call->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$telecare_call_grid->RestoreCurrentRowFormValues($telecare_call_grid->RowIndex); // Restore form values
		if ($telecare_call->CurrentAction == "gridedit") { // Grid edit
			if ($telecare_call->EventCancelled) {
				$telecare_call_grid->RestoreCurrentRowFormValues($telecare_call_grid->RowIndex); // Restore form values
			}
			if ($telecare_call_grid->RowAction == "insert")
				$telecare_call->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$telecare_call->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($telecare_call->CurrentAction == "gridedit" && ($telecare_call->RowType == EW_ROWTYPE_EDIT || $telecare_call->RowType == EW_ROWTYPE_ADD) && $telecare_call->EventCancelled) // Update failed
			$telecare_call_grid->RestoreCurrentRowFormValues($telecare_call_grid->RowIndex); // Restore form values
		if ($telecare_call->RowType == EW_ROWTYPE_EDIT) // Edit row
			$telecare_call_grid->EditRowCnt++;
		if ($telecare_call->CurrentAction == "F") // Confirm row
			$telecare_call_grid->RestoreCurrentRowFormValues($telecare_call_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$telecare_call->RowAttrs = array_merge($telecare_call->RowAttrs, array('data-rowindex'=>$telecare_call_grid->RowCnt, 'id'=>'r' . $telecare_call_grid->RowCnt . '_telecare_call', 'data-rowtype'=>$telecare_call->RowType));

		// Render row
		$telecare_call_grid->RenderRow();

		// Render list options
		$telecare_call_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($telecare_call_grid->RowAction <> "delete" && $telecare_call_grid->RowAction <> "insertdelete" && !($telecare_call_grid->RowAction == "insert" && $telecare_call->CurrentAction == "F" && $telecare_call_grid->EmptyRow())) {
?>
	<tr<?php echo $telecare_call->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_call_grid->ListOptions->Render("body", "left", $telecare_call_grid->RowCnt);
?>
	<?php if ($telecare_call->call_id->Visible) { // call_id ?>
		<td data-name="call_id"<?php echo $telecare_call->call_id->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_id" value="<?php echo ew_HtmlEncode($telecare_call->call_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_id" class="form-group telecare_call_call_id">
<span<?php echo $telecare_call->call_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_id->EditValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_id" value="<?php echo ew_HtmlEncode($telecare_call->call_id->CurrentValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_id" class="telecare_call_call_id">
<span<?php echo $telecare_call->call_id->ViewAttributes() ?>>
<?php echo $telecare_call->call_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_id" value="<?php echo ew_HtmlEncode($telecare_call->call_id->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_id" value="<?php echo ew_HtmlEncode($telecare_call->call_id->OldValue) ?>">
<?php } ?>
<a id="<?php echo $telecare_call_grid->PageObjName . "_row_" . $telecare_call_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($telecare_call->call_user_id->Visible) { // call_user_id ?>
		<td data-name="call_user_id"<?php echo $telecare_call->call_user_id->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($telecare_call->call_user_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_user_id" class="form-group telecare_call_call_user_id">
<span<?php echo $telecare_call->call_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_user_id" class="form-group telecare_call_call_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_call->call_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_call->call_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" style="white-space: nowrap; z-index: <?php echo (9000 - $telecare_call_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="sv_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo $telecare_call->call_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>"<?php echo $telecare_call->call_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_user_id->DisplayValueSeparator) ? json_encode($telecare_call->call_user_id->DisplayValueSeparator) : $telecare_call->call_user_id->DisplayValueSeparator) ?>" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_call->Lookup_Selecting($telecare_call->call_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="q_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_callgrid.CreateAutoSuggest({"id":"x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($telecare_call->call_user_id->getSessionValue() <> "") { ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_user_id" class="form-group telecare_call_call_user_id">
<span<?php echo $telecare_call->call_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_user_id" class="form-group telecare_call_call_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_call->call_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_call->call_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" style="white-space: nowrap; z-index: <?php echo (9000 - $telecare_call_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="sv_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo $telecare_call->call_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>"<?php echo $telecare_call->call_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_user_id->DisplayValueSeparator) ? json_encode($telecare_call->call_user_id->DisplayValueSeparator) : $telecare_call->call_user_id->DisplayValueSeparator) ?>" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_call->Lookup_Selecting($telecare_call->call_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="q_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_callgrid.CreateAutoSuggest({"id":"x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_user_id" class="telecare_call_call_user_id">
<span<?php echo $telecare_call->call_user_id->ViewAttributes() ?>>
<?php echo $telecare_call->call_user_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_call->call_admin_id->Visible) { // call_admin_id ?>
		<td data-name="call_admin_id"<?php echo $telecare_call->call_admin_id->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_admin_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" value="<?php echo ew_HtmlEncode($telecare_call->call_admin_id->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_admin_id" class="telecare_call_call_admin_id">
<span<?php echo $telecare_call->call_admin_id->ViewAttributes() ?>>
<?php echo $telecare_call->call_admin_id->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_admin_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" value="<?php echo ew_HtmlEncode($telecare_call->call_admin_id->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_admin_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" value="<?php echo ew_HtmlEncode($telecare_call->call_admin_id->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_call->call_requested_date->Visible) { // call_requested_date ?>
		<td data-name="call_requested_date"<?php echo $telecare_call->call_requested_date->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_requested_date" class="form-group telecare_call_call_requested_date">
<input type="text" data-table="telecare_call" data-field="x_call_requested_date" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_requested_date->EditValue ?>"<?php echo $telecare_call->call_requested_date->EditAttributes() ?>>
<?php if (!$telecare_call->call_requested_date->ReadOnly && !$telecare_call->call_requested_date->Disabled && !isset($telecare_call->call_requested_date->EditAttrs["readonly"]) && !isset($telecare_call->call_requested_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_requested_date" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" value="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_requested_date" class="form-group telecare_call_call_requested_date">
<input type="text" data-table="telecare_call" data-field="x_call_requested_date" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_requested_date->EditValue ?>"<?php echo $telecare_call->call_requested_date->EditAttributes() ?>>
<?php if (!$telecare_call->call_requested_date->ReadOnly && !$telecare_call->call_requested_date->Disabled && !isset($telecare_call->call_requested_date->EditAttrs["readonly"]) && !isset($telecare_call->call_requested_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_requested_date" class="telecare_call_call_requested_date">
<span<?php echo $telecare_call->call_requested_date->ViewAttributes() ?>>
<?php echo $telecare_call->call_requested_date->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_requested_date" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" value="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_requested_date" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" value="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_call->call_start_on->Visible) { // call_start_on ?>
		<td data-name="call_start_on"<?php echo $telecare_call->call_start_on->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_start_on" class="form-group telecare_call_call_start_on">
<input type="text" data-table="telecare_call" data-field="x_call_start_on" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_start_on->EditValue ?>"<?php echo $telecare_call->call_start_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_start_on->ReadOnly && !$telecare_call->call_start_on->Disabled && !isset($telecare_call->call_start_on->EditAttrs["readonly"]) && !isset($telecare_call->call_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_start_on" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" value="<?php echo ew_HtmlEncode($telecare_call->call_start_on->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_start_on" class="form-group telecare_call_call_start_on">
<input type="text" data-table="telecare_call" data-field="x_call_start_on" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_start_on->EditValue ?>"<?php echo $telecare_call->call_start_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_start_on->ReadOnly && !$telecare_call->call_start_on->Disabled && !isset($telecare_call->call_start_on->EditAttrs["readonly"]) && !isset($telecare_call->call_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_start_on" class="telecare_call_call_start_on">
<span<?php echo $telecare_call->call_start_on->ViewAttributes() ?>>
<?php echo $telecare_call->call_start_on->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_start_on" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" value="<?php echo ew_HtmlEncode($telecare_call->call_start_on->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_start_on" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" value="<?php echo ew_HtmlEncode($telecare_call->call_start_on->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_call->call_close_on->Visible) { // call_close_on ?>
		<td data-name="call_close_on"<?php echo $telecare_call->call_close_on->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_close_on" class="form-group telecare_call_call_close_on">
<input type="text" data-table="telecare_call" data-field="x_call_close_on" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_close_on->EditValue ?>"<?php echo $telecare_call->call_close_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_close_on->ReadOnly && !$telecare_call->call_close_on->Disabled && !isset($telecare_call->call_close_on->EditAttrs["readonly"]) && !isset($telecare_call->call_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_close_on" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" value="<?php echo ew_HtmlEncode($telecare_call->call_close_on->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_close_on" class="form-group telecare_call_call_close_on">
<input type="text" data-table="telecare_call" data-field="x_call_close_on" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_close_on->EditValue ?>"<?php echo $telecare_call->call_close_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_close_on->ReadOnly && !$telecare_call->call_close_on->Disabled && !isset($telecare_call->call_close_on->EditAttrs["readonly"]) && !isset($telecare_call->call_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_close_on" class="telecare_call_call_close_on">
<span<?php echo $telecare_call->call_close_on->ViewAttributes() ?>>
<?php echo $telecare_call->call_close_on->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_close_on" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" value="<?php echo ew_HtmlEncode($telecare_call->call_close_on->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_close_on" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" value="<?php echo ew_HtmlEncode($telecare_call->call_close_on->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_call->call_closed->Visible) { // call_closed ?>
		<td data-name="call_closed"<?php echo $telecare_call->call_closed->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_closed" class="form-group telecare_call_call_closed">
<div id="tp_x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" class="ewTemplate"><input type="radio" data-table="telecare_call" data-field="x_call_closed" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_closed->DisplayValueSeparator) ? json_encode($telecare_call->call_closed->DisplayValueSeparator) : $telecare_call->call_closed->DisplayValueSeparator) ?>" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="{value}"<?php echo $telecare_call->call_closed->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_call->call_closed->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_call->call_closed->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_call->call_closed->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->CurrentValue) ?>" checked<?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_call->call_closed->OldValue = "";
?>
</div></div>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_closed" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_closed" class="form-group telecare_call_call_closed">
<div id="tp_x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" class="ewTemplate"><input type="radio" data-table="telecare_call" data-field="x_call_closed" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_closed->DisplayValueSeparator) ? json_encode($telecare_call->call_closed->DisplayValueSeparator) : $telecare_call->call_closed->DisplayValueSeparator) ?>" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="{value}"<?php echo $telecare_call->call_closed->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_call->call_closed->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_call->call_closed->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_call->call_closed->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->CurrentValue) ?>" checked<?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_call->call_closed->OldValue = "";
?>
</div></div>
</span>
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_closed" class="telecare_call_call_closed">
<span<?php echo $telecare_call->call_closed->ViewAttributes() ?>>
<?php echo $telecare_call->call_closed->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_closed" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
	<?php if ($telecare_call->call_last_update->Visible) { // call_last_update ?>
		<td data-name="call_last_update"<?php echo $telecare_call->call_last_update->CellAttributes() ?>>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_last_update" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" value="<?php echo ew_HtmlEncode($telecare_call->call_last_update->OldValue) ?>">
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($telecare_call->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span id="el<?php echo $telecare_call_grid->RowCnt ?>_telecare_call_call_last_update" class="telecare_call_call_last_update">
<span<?php echo $telecare_call->call_last_update->ViewAttributes() ?>>
<?php echo $telecare_call->call_last_update->ListViewValue() ?></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_last_update" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" value="<?php echo ew_HtmlEncode($telecare_call->call_last_update->FormValue) ?>">
<input type="hidden" data-table="telecare_call" data-field="x_call_last_update" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" value="<?php echo ew_HtmlEncode($telecare_call->call_last_update->OldValue) ?>">
<?php } ?>
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_call_grid->ListOptions->Render("body", "right", $telecare_call_grid->RowCnt);
?>
	</tr>
<?php if ($telecare_call->RowType == EW_ROWTYPE_ADD || $telecare_call->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
ftelecare_callgrid.UpdateOpts(<?php echo $telecare_call_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($telecare_call->CurrentAction <> "gridadd" || $telecare_call->CurrentMode == "copy")
		if (!$telecare_call_grid->Recordset->EOF) $telecare_call_grid->Recordset->MoveNext();
}
?>
<?php
	if ($telecare_call->CurrentMode == "add" || $telecare_call->CurrentMode == "copy" || $telecare_call->CurrentMode == "edit") {
		$telecare_call_grid->RowIndex = '$rowindex$';
		$telecare_call_grid->LoadDefaultValues();

		// Set row properties
		$telecare_call->ResetAttrs();
		$telecare_call->RowAttrs = array_merge($telecare_call->RowAttrs, array('data-rowindex'=>$telecare_call_grid->RowIndex, 'id'=>'r0_telecare_call', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($telecare_call->RowAttrs["class"], "ewTemplate");
		$telecare_call->RowType = EW_ROWTYPE_ADD;

		// Render row
		$telecare_call_grid->RenderRow();

		// Render list options
		$telecare_call_grid->RenderListOptions();
		$telecare_call_grid->StartRowCnt = 0;
?>
	<tr<?php echo $telecare_call->RowAttributes() ?>>
<?php

// Render list options (body, left)
$telecare_call_grid->ListOptions->Render("body", "left", $telecare_call_grid->RowIndex);
?>
	<?php if ($telecare_call->call_id->Visible) { // call_id ?>
		<td data-name="call_id">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_id" class="form-group telecare_call_call_id">
<span<?php echo $telecare_call->call_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_id" value="<?php echo ew_HtmlEncode($telecare_call->call_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_id" value="<?php echo ew_HtmlEncode($telecare_call->call_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_call->call_user_id->Visible) { // call_user_id ?>
		<td data-name="call_user_id">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<?php if ($telecare_call->call_user_id->getSessionValue() <> "") { ?>
<span id="el$rowindex$_telecare_call_call_user_id" class="form-group telecare_call_call_user_id">
<span<?php echo $telecare_call->call_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>">
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_user_id" class="form-group telecare_call_call_user_id">
<?php
$wrkonchange = trim(" " . @$telecare_call->call_user_id->EditAttrs["onchange"]);
if ($wrkonchange <> "") $wrkonchange = " onchange=\"" . ew_JsEncode2($wrkonchange) . "\"";
$telecare_call->call_user_id->EditAttrs["onchange"] = "";
?>
<span id="as_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" style="white-space: nowrap; z-index: <?php echo (9000 - $telecare_call_grid->RowCnt * 10) ?>">
	<input type="text" name="sv_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="sv_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo $telecare_call->call_user_id->EditValue ?>" size="30" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>" data-placeholder="<?php echo ew_HtmlEncode($telecare_call->call_user_id->getPlaceHolder()) ?>"<?php echo $telecare_call->call_user_id->EditAttributes() ?>>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_user_id->DisplayValueSeparator) ? json_encode($telecare_call->call_user_id->DisplayValueSeparator) : $telecare_call->call_user_id->DisplayValueSeparator) ?>" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->CurrentValue) ?>"<?php echo $wrkonchange ?>>
<?php
switch (@$gsLanguage) {
	case "it":
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
	default:
		$sSqlWrk = "SELECT `user_id`, `user_surname` AS `DispFld`, `user_name` AS `Disp2Fld` FROM `telecare_user`";
		$sWhereWrk = "`user_surname` LIKE '%{query_value}%' OR `user_name` LIKE '%{query_value}%' OR CONCAT(`user_surname`,'" . ew_ValueSeparator(1, $Page->call_user_id) . "',`user_name`) LIKE '{query_value}%'";
		break;
}
$telecare_call->Lookup_Selecting($telecare_call->call_user_id, $sWhereWrk); // Call Lookup selecting
if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
$sSqlWrk .= " LIMIT " . EW_AUTO_SUGGEST_MAX_ENTRIES;
?>
<input type="hidden" name="q_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="q_x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="s=<?php echo ew_Encrypt($sSqlWrk) ?>&d=">
<script type="text/javascript">
ftelecare_callgrid.CreateAutoSuggest({"id":"x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id","forceSelect":false});
</script>
</span>
<?php } ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_user_id" class="form-group telecare_call_call_user_id">
<span<?php echo $telecare_call->call_user_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_user_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_user_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_user_id" value="<?php echo ew_HtmlEncode($telecare_call->call_user_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_call->call_admin_id->Visible) { // call_admin_id ?>
		<td data-name="call_admin_id">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_admin_id" class="form-group telecare_call_call_admin_id">
<span<?php echo $telecare_call->call_admin_id->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_admin_id->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_admin_id" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" value="<?php echo ew_HtmlEncode($telecare_call->call_admin_id->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_admin_id" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_admin_id" value="<?php echo ew_HtmlEncode($telecare_call->call_admin_id->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_call->call_requested_date->Visible) { // call_requested_date ?>
		<td data-name="call_requested_date">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_call_call_requested_date" class="form-group telecare_call_call_requested_date">
<input type="text" data-table="telecare_call" data-field="x_call_requested_date" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_requested_date->EditValue ?>"<?php echo $telecare_call->call_requested_date->EditAttributes() ?>>
<?php if (!$telecare_call->call_requested_date->ReadOnly && !$telecare_call->call_requested_date->Disabled && !isset($telecare_call->call_requested_date->EditAttrs["readonly"]) && !isset($telecare_call->call_requested_date->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_requested_date" class="form-group telecare_call_call_requested_date">
<span<?php echo $telecare_call->call_requested_date->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_requested_date->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_requested_date" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" value="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_requested_date" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_requested_date" value="<?php echo ew_HtmlEncode($telecare_call->call_requested_date->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_call->call_start_on->Visible) { // call_start_on ?>
		<td data-name="call_start_on">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_call_call_start_on" class="form-group telecare_call_call_start_on">
<input type="text" data-table="telecare_call" data-field="x_call_start_on" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_start_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_start_on->EditValue ?>"<?php echo $telecare_call->call_start_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_start_on->ReadOnly && !$telecare_call->call_start_on->Disabled && !isset($telecare_call->call_start_on->EditAttrs["readonly"]) && !isset($telecare_call->call_start_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_start_on" class="form-group telecare_call_call_start_on">
<span<?php echo $telecare_call->call_start_on->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_start_on->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_start_on" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" value="<?php echo ew_HtmlEncode($telecare_call->call_start_on->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_start_on" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_start_on" value="<?php echo ew_HtmlEncode($telecare_call->call_start_on->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_call->call_close_on->Visible) { // call_close_on ?>
		<td data-name="call_close_on">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_call_call_close_on" class="form-group telecare_call_call_close_on">
<input type="text" data-table="telecare_call" data-field="x_call_close_on" data-format="7" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" placeholder="<?php echo ew_HtmlEncode($telecare_call->call_close_on->getPlaceHolder()) ?>" value="<?php echo $telecare_call->call_close_on->EditValue ?>"<?php echo $telecare_call->call_close_on->EditAttributes() ?>>
<?php if (!$telecare_call->call_close_on->ReadOnly && !$telecare_call->call_close_on->Disabled && !isset($telecare_call->call_close_on->EditAttrs["readonly"]) && !isset($telecare_call->call_close_on->EditAttrs["disabled"])) { ?>
<script type="text/javascript">
ew_CreateCalendar("ftelecare_callgrid", "x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on", "%d/%m/%Y");
</script>
<?php } ?>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_close_on" class="form-group telecare_call_call_close_on">
<span<?php echo $telecare_call->call_close_on->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_close_on->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_close_on" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" value="<?php echo ew_HtmlEncode($telecare_call->call_close_on->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_close_on" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_close_on" value="<?php echo ew_HtmlEncode($telecare_call->call_close_on->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_call->call_closed->Visible) { // call_closed ?>
		<td data-name="call_closed">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<span id="el$rowindex$_telecare_call_call_closed" class="form-group telecare_call_call_closed">
<div id="tp_x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" class="ewTemplate"><input type="radio" data-table="telecare_call" data-field="x_call_closed" data-value-separator="<?php echo ew_HtmlEncode(is_array($telecare_call->call_closed->DisplayValueSeparator) ? json_encode($telecare_call->call_closed->DisplayValueSeparator) : $telecare_call->call_closed->DisplayValueSeparator) ?>" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="{value}"<?php echo $telecare_call->call_closed->EditAttributes() ?>></div>
<div id="dsl_x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" data-repeatcolumn="5" class="ewItemList" style="display: none;"><div>
<?php
$arwrk = $telecare_call->call_closed->EditValue;
if (is_array($arwrk)) {
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($telecare_call->call_closed->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " checked" : "";
		if ($selwrk <> "")
			$emptywrk = FALSE;
?>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 1) ?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed_<?php echo $rowcntwrk ?>" value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?><?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->DisplayValue($arwrk[$rowcntwrk]) ?></label>
<?php echo ew_RepeatColumnTable($rowswrk, $rowcntwrk, 5, 2) ?>
<?php
	}
	if ($emptywrk && strval($telecare_call->call_closed->CurrentValue) <> "") {
?>
<label class="radio-inline"><input type="radio" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed_<?php echo $rowswrk ?>" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->CurrentValue) ?>" checked<?php echo $telecare_call->call_closed->EditAttributes() ?>><?php echo $telecare_call->call_closed->CurrentValue ?></label>
<?php
    }
}
if (@$emptywrk) $telecare_call->call_closed->OldValue = "";
?>
</div></div>
</span>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_closed" class="form-group telecare_call_call_closed">
<span<?php echo $telecare_call->call_closed->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_closed->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_closed" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_closed" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_closed" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_closed" value="<?php echo ew_HtmlEncode($telecare_call->call_closed->OldValue) ?>">
</td>
	<?php } ?>
	<?php if ($telecare_call->call_last_update->Visible) { // call_last_update ?>
		<td data-name="call_last_update">
<?php if ($telecare_call->CurrentAction <> "F") { ?>
<?php } else { ?>
<span id="el$rowindex$_telecare_call_call_last_update" class="form-group telecare_call_call_last_update">
<span<?php echo $telecare_call->call_last_update->ViewAttributes() ?>>
<p class="form-control-static"><?php echo $telecare_call->call_last_update->ViewValue ?></p></span>
</span>
<input type="hidden" data-table="telecare_call" data-field="x_call_last_update" name="x<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" id="x<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" value="<?php echo ew_HtmlEncode($telecare_call->call_last_update->FormValue) ?>">
<?php } ?>
<input type="hidden" data-table="telecare_call" data-field="x_call_last_update" name="o<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" id="o<?php echo $telecare_call_grid->RowIndex ?>_call_last_update" value="<?php echo ew_HtmlEncode($telecare_call->call_last_update->OldValue) ?>">
</td>
	<?php } ?>
<?php

// Render list options (body, right)
$telecare_call_grid->ListOptions->Render("body", "right", $telecare_call_grid->RowCnt);
?>
<script type="text/javascript">
ftelecare_callgrid.UpdateOpts(<?php echo $telecare_call_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($telecare_call->CurrentMode == "add" || $telecare_call->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="<?php echo $telecare_call_grid->FormKeyCountName ?>" id="<?php echo $telecare_call_grid->FormKeyCountName ?>" value="<?php echo $telecare_call_grid->KeyCount ?>">
<?php echo $telecare_call_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_call->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="<?php echo $telecare_call_grid->FormKeyCountName ?>" id="<?php echo $telecare_call_grid->FormKeyCountName ?>" value="<?php echo $telecare_call_grid->KeyCount ?>">
<?php echo $telecare_call_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($telecare_call->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" value="ftelecare_callgrid">
</div>
<?php

// Close recordset
if ($telecare_call_grid->Recordset)
	$telecare_call_grid->Recordset->Close();
?>
<?php if ($telecare_call_grid->ShowOtherOptions) { ?>
<div class="panel-footer ewGridLowerPanel">
<?php
	foreach ($telecare_call_grid->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
<div class="clearfix"></div>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if ($telecare_call_grid->TotalRecs == 0 && $telecare_call->CurrentAction == "") { // Show other options ?>
<div class="ewListOtherOptions">
<?php
	foreach ($telecare_call_grid->OtherOptions as &$option) {
		$option->ButtonClass = "";
		$option->Render("body", "");
	}
?>
</div>
<div class="clearfix"></div>
<?php } ?>
<?php if ($telecare_call->Export == "") { ?>
<script type="text/javascript">
ftelecare_callgrid.Init();
</script>
<?php } ?>
<?php
$telecare_call_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$telecare_call_grid->Page_Terminate();
?>
